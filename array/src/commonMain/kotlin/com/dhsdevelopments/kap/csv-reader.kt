package com.dhsdevelopments.kap.csv

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.mpbignum.BigInt
import com.dhsdevelopments.mpbignum.of

class CsvParseException(msg: String, val line: Int, val col: Int) : Exception("Error at ${line + 1}:${col + 1}: $msg")

fun interface CsvValueConverter {
    fun parse(s: String): APLValue?
}

fun readCsv(source: CharacterProvider): APLValue {
    return CsvReader().read(source)
}

class CsvReader(val separator: Char = ',', val quoteChar: Char? = '\"', val trim: Boolean = true, val valueConverter: CsvValueConverter? = null) {
    fun read(source: CharacterProvider): APLValue {
        val rows = readRows(source)

        if (rows.isEmpty()) {
            return APLArrayImpl.make(dimensionsOfSize(0, 0)) {
                throw Exception("Attempt to read a value when initialising empty array")
            }
        }

        val width = rows.maxValueBy { it.size }
        return APLArrayImpl.make(dimensionsOfSize(rows.size, width)) { index ->
            val rowIndex = index / width
            val colIndex = index % width
            val row = rows[rowIndex]
            if (colIndex >= row.size) {
                APLNullValue
            } else {
                row[colIndex]
            }
        }
    }

    private fun readRows(source: CharacterProvider): List<List<APLValue>> {
        val rows = ArrayList<List<APLValue>>()
        var lineNumber = 0
        while (true) {
            var line = source.nextLine() ?: break
            lineNumber++
            val fields = ArrayList<APLValue>()
            var pos = 0

            fun atEol() = pos >= line.length

            fun isCsvWhitespace(ch: Char) = ch != separator && ch.isWhitespace()

            fun skipWhitespace() {
                while (!atEol() && isCsvWhitespace(line[pos])) {
                    pos++
                }
            }

            fun readQuotedField(): String {
                val buf2 = StringBuilder()
                loop@ while (true) {
                    while (atEol()) {
                        line = source.nextLine() ?: throw CsvParseException(
                            "End of file in the middle of string",
                            lineNumber - 1,
                            pos)
                        lineNumber++
                        pos = 0
                        buf2.append("\n")
                    }
                    val ch = line[pos++]
                    when {
                        quoteChar != null && ch == quoteChar -> {
                            if (atEol() || line[pos] != '"') {
                                break@loop
                            } else {
                                buf2.append("\"")
                                pos++
                            }
                        }
                        quoteChar != null && ch == '\\' -> {
                            if (atEol()) {
                                throw CsvParseException("Unterminated string", lineNumber - 1, pos)
                            } else {
                                buf2.append(line[pos++])
                            }
                        }
                        else -> buf2.append(ch)
                    }
                }
                return buf2.toString()
            }

            fun readUnquotedField(initial: Char): String {
                val buf = StringBuilder()
                buf.append(initial)
                var heldWhitespace = StringBuilder()
                loop@ while (!atEol()) {
                    val ch = line[pos]
                    when {
                        ch == separator -> {
                            if (!trim) {
                                buf.append(heldWhitespace)
                            }
                            break@loop
                        }
                        isCsvWhitespace(ch) -> heldWhitespace.append(ch)
                        else -> {
                            buf.append(heldWhitespace)
                            heldWhitespace = StringBuilder()
                            buf.append(ch)
                        }
                    }
                    pos++
                }
                return buf.toString()
            }

            if (trim) {
                skipWhitespace()
            }
            if (!atEol()) {
                while (true) {
                    if (trim) {
                        skipWhitespace()
                    }
                    if (atEol()) break
                    val ch = line[pos++]
                    val field = when {
                        quoteChar != null && ch == quoteChar -> stringToAplValue(readQuotedField())
                        ch == separator -> {
                            pos--
                            APLString.EMPTY_STRING
                        }
                        else -> stringToAplValue(readUnquotedField(ch))
                    }
                    fields.add(field)
                    // At this point the next character must be either a comma or we're at the end of the line
                    if (trim) {
                        skipWhitespace()
                    }
                    if (!atEol()) {
                        val ch2 = line[pos++]
                        if (ch2 != separator) {
                            throw CsvParseException("Syntax error in CSV file", lineNumber - 1, pos)
                        }
                    }
                }
                rows.add(fields)
            }
        }
        return rows
    }

    private fun stringToAplValue(string: String): APLValue {
        if (valueConverter != null) {
            val v = valueConverter.parse(string)
            if (v != null) {
                return v
            }
        }
        return when {
            PATTERN_INTEGER.matches(string) -> BigInt.of(string).makeAPLNumberWithReduction()
            PATTERN_FLOAT1.matches(string) -> string.toDouble().makeAPLNumber()
            PATTERN_FLOAT2.matches(string) -> string.toDouble().makeAPLNumber()
            else -> APLString.make(string)
        }
    }

    companion object {
        private val PATTERN_INTEGER = "^-?[0-9]+$".toRegex()
        private val PATTERN_FLOAT1 = "^-?[0-9]+\\.[0-9]*$".toRegex()
        private val PATTERN_FLOAT2 = "^-?[0-9]*\\.[0-9]+$".toRegex()
    }
}
