package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.complex.Complex
import com.dhsdevelopments.mpbignum.Rational
import com.dhsdevelopments.mpbignum.make
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class APLComplexTest : APLTest() {
    @Test
    fun addComplex() {
        val result = parseAPLExpression("2J3+3J4")
        assertEquals(Complex(5.0, 7.0), result.ensureNumber().asComplex())
    }

    @Test
    fun addIntAndComplex() {
        val result = parseAPLExpression("6+3J4")
        assertEquals(Complex(9.0, 4.0), result.ensureNumber().asComplex())
    }

    @Test
    fun addComplexAndInt() {
        val result = parseAPLExpression("3J4+6")
        assertEquals(Complex(9.0, 4.0), result.ensureNumber().asComplex())
    }

    @Test
    fun multiplyComplex() {
        val result = parseAPLExpression("×⌻⍨ 0 1 0j1 1j1 10j21 ¯2j6 2j¯6 ¯2j¯6 1.2j3.2 1e8j1e3")
        assertDimension(dimensionsOfSize(10, 10), result)
        assertArrayContent(
            arrayOf(
                NearDouble(0.0), NearDouble(0.0), NearDouble(0.0), NearDouble(0.0), NearDouble(0.0), NearDouble(0.0), NearDouble(0.0),
                NearDouble(0.0), NearDouble(0.0), NearDouble(0.0), NearDouble(0.0), NearDouble(1.0),
                NearComplex(Complex(0, 1)), NearComplex(Complex(1, 1)), NearComplex(Complex(10, 21)), NearComplex(Complex(-2, 6)),
                NearComplex(Complex(2, -6)), NearComplex(Complex(-2, -6)), NearComplex(Complex(1.2, 3.2)), NearComplex(Complex(100000000, 1000)),
                NearDouble(0.0), NearComplex(Complex(0, 1)), NearDouble(-1.0), NearComplex(Complex(-1, 1)), NearComplex(Complex(-21, 10)),
                NearComplex(Complex(-6, -2)), NearComplex(Complex(6, 2)), NearComplex(Complex(6, -2)), NearComplex(Complex(-3.2, 1.2)),
                NearComplex(Complex(-1000, 100000000)), NearDouble(0.0), NearComplex(Complex(1, 1)), NearComplex(Complex(-1, 1)),
                NearComplex(Complex(0, 2)), NearComplex(Complex(-11, 31)), NearComplex(Complex(-8, 4)), NearComplex(Complex(8, -4)),
                NearComplex(Complex(4, -8)), NearComplex(Complex(-2.0, 4.4)), NearComplex(Complex(99999000, 100001000)), NearDouble(0.0),
                NearComplex(Complex(10, 21)), NearComplex(Complex(-21, 10)), NearComplex(Complex(-11, 31)), NearComplex(Complex(-341, 420)),
                NearComplex(Complex(-146, 18)), NearComplex(Complex(146, -18)), NearComplex(Complex(106, -102)), NearComplex(Complex(-55.2, 57.2)),
                NearComplex(Complex(999979000, 2100010000)), NearDouble(0.0), NearComplex(Complex(-2, 6)), NearComplex(Complex(-6, -2)),
                NearComplex(Complex(-8, 4)), NearComplex(Complex(-146, 18)), NearComplex(Complex(-32, -24)), NearComplex(Complex(32, 24)),
                NearDouble(40.0), NearComplex(Complex(-21.6, 0.8)), NearComplex(Complex(-200006000, 599998000)), NearDouble(0.0),
                NearComplex(Complex(2, -6)), NearComplex(Complex(6, 2)), NearComplex(Complex(8, -4)), NearComplex(Complex(146, -18)),
                NearComplex(Complex(32, 24)), NearComplex(Complex(-32, -24)), NearDouble(-40.0), NearComplex(Complex(21.6, -0.8)),
                NearComplex(Complex(200006000, -599998000)), NearDouble(0.0), NearComplex(Complex(-2, -6)), NearComplex(Complex(6, -2)),
                NearComplex(Complex(4, -8)), NearComplex(Complex(106, -102)), NearDouble(40.0), NearDouble(-40.0),
                NearComplex(Complex(-32, 24)), NearComplex(Complex(16.8, -13.6)), NearComplex(Complex(-199994000, -600002000)), NearDouble(0.0),
                NearComplex(Complex(1.2, 3.2)), NearComplex(Complex(-3.2, 1.2)), NearComplex(Complex(-2, 4.4)), NearComplex(Complex(-55.2, 57.2)),
                NearComplex(Complex(-21.6, 0.8)), NearComplex(Complex(21.6, -0.8)), NearComplex(Complex(16.8, -13.6)), NearComplex(Complex(-8.8, 7.68)),
                NearComplex(Complex(119996800, 320001200)), NearDouble(0.0), NearComplex(Complex(100000000, 1000)), NearComplex(Complex(-1000, 100000000)),
                NearComplex(Complex(99999000, 100001000)), NearComplex(Complex(999979000, 2100010000)), NearComplex(Complex(-200006000, 599998000)),
                NearComplex(Complex(200006000, -599998000)), NearComplex(Complex(-199994000, -600002000)), NearComplex(Complex(119996800, 320001200)),
                NearComplex(Complex(9.999999999E15, 2E11), -12, -8)),
            result)
    }

    @Test
    fun complexSign() {
        val result = parseAPLExpression("× 0j1 ¯2j6 2j¯6 ¯2j¯6")
        assert1DArray(
            arrayOf(
                NearComplex(Complex(0, 1)),
                NearComplex(Complex(-0.316227766, 0.9486832981)),
                NearComplex(Complex(0.316227766, -0.9486832981)),
                NearComplex(Complex(-0.316227766, -0.9486832981))),
            result)
    }

    @Test
    fun convertIntToComplex() {
        val result = parseAPLExpression("1+3")
        assertEquals(Complex(4.0, 0.0), result.ensureNumber().asComplex())
    }

    @Test
    fun convertFloatToComplex() {
        val result = parseAPLExpression("1.0+3.0")
        assertEquals(Complex(4.0, 0.0), result.ensureNumber().asComplex())
    }

    @Test
    fun complexConjugate() {
        val result = parseAPLExpression("+5j¯2")
        assertEquals(Complex(5.0, 2.0), result.ensureNumber().asComplex())
    }

    @Test
    fun convertComplexToIntSuccess() {
        val result = parseAPLExpression("2J3-1J3")
        assertEquals(1, result.ensureNumber().asInt())
    }

    @Test
    fun convertComplexToDoubleSuccess() {
        val result = parseAPLExpression("2J3-1J3")
        assertEquals(1.0, result.ensureNumber().asDouble())
    }

    @Test
    fun negateComplexTest() {
        val result = parseAPLExpression("-2J3")
        assertEquals(Complex(-2.0, -3.0), result.ensureNumber().asComplex())
    }

    @Test
    fun exptTest() {
        parseAPLExpression("⋆1J2").let { result ->
            //#C(-1.1312043837568135d0 2.4717266720048188d0)
            assertComplexWithRange(Pair(-1.131205, -1.131204), Pair(2.471725, 2.471727), result)
        }
        parseAPLExpression("2j3⋆2j2").let { result ->
            //#C(-0.32932274563575575d0 -1.7909296731087332d0)
            assertComplexWithRange(Pair(-0.329323, -0.329321), Pair(-1.790930, -1.790928), result)
        }
    }

    @Test
    fun compareComplexEqual() {
        assertSimpleNumber(1, parseAPLExpression("3j4=3j4"))
        assertSimpleNumber(0, parseAPLExpression("3j4=6j7"))
        assertSimpleNumber(0, parseAPLExpression("1j2=1j3"))
        assertSimpleNumber(0, parseAPLExpression("1j2=3j2"))
        assertSimpleNumber(1, parseAPLExpression("0j0=0"))
        assertSimpleNumber(1, parseAPLExpression("3j0=3"))
        assertSimpleNumber(0, parseAPLExpression("3j4=3"))
    }

    @Test
    fun compareComplexNotEqual() {
        assertSimpleNumber(0, parseAPLExpression("3j4≠3j4"))
        assertSimpleNumber(1, parseAPLExpression("3j4≠6j7"))
        assertSimpleNumber(1, parseAPLExpression("1j2≠1j3"))
        assertSimpleNumber(1, parseAPLExpression("1j2≠3j2"))
        assertSimpleNumber(0, parseAPLExpression("0j0≠0"))
        assertSimpleNumber(0, parseAPLExpression("3j0≠3"))
        assertSimpleNumber(1, parseAPLExpression("3j4≠3"))
    }

    @Test
    fun realpartLong() {
        parseAPLExpression("math:re ¯1 0 1 10 12345987").let { result ->
            assert1DArray(arrayOf(-1, 0, 1, 10, 12345987), result)
        }
    }

    @Test
    fun realpartDouble() {
        parseAPLExpression("math:re ¯1.5 0.0 1.5 10.5 12345987.5").let { result ->
            assert1DArray(arrayOf(InnerDouble(-1.5), InnerDouble(0.0), InnerDouble(1.5), InnerDouble(10.5), InnerDouble(12345987.5)), result)
        }
    }

    @Test
    fun realpartComplex() {
        parseAPLExpression("math:re 4j8 ¯3j5 3j¯5 ¯100j¯331").let { result ->
            assert1DArray(arrayOf(InnerDouble(4.0), InnerDouble(-3.0), InnerDouble(3.0), InnerDouble(-100.0)), result)
        }
    }

    @Test
    fun realpartBigintRational() {
        parseAPLExpression("math:re (int:asBigint 1234) (int:asBigint 1234567890123456789012345678901234567890) 1r3").let { result ->
            assert1DArray(
                arrayOf(
                    InnerBigIntOrLong("1234"),
                    InnerBigIntOrLong("1234567890123456789012345678901234567890"),
                    InnerRational(Rational.make(1, 3))),
                result)
        }
    }

    @Test
    fun realpartMixed() {
        parseAPLExpression("math:re 0 1 1.5").let { result ->
            assert1DArray(arrayOf(0, 1, InnerDouble(1.5)), result)
        }
    }

    @Test
    fun realpartMixedWithComplex() {
        parseAPLExpression("math:re 0 1 1.5 9.5j2.5").let { result ->
            assert1DArray(arrayOf(0, 1, InnerDouble(1.5), InnerDouble(9.5)), result)
        }
    }

    @Test
    fun imagpartLong() {
        parseAPLExpression("math:im ¯1 0 1 998").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0), result)
        }
    }

    @Test
    fun imagpartDouble() {
        parseAPLExpression("math:im ¯1.5 0.0 1.5 10.5 1111.1").let { result ->
            assert1DArray(arrayOf(InnerDouble(0.0), InnerDouble(0.0), InnerDouble(0.0), InnerDouble(0.0), InnerDouble(0.0)), result)
        }
    }

    @Test
    fun imagpartComplex() {
        parseAPLExpression("math:im 5j2.5 ¯5.5j4.5 9j¯4 ¯3j¯4.5").let { result ->
            assert1DArray(arrayOf(InnerDouble(2.5), InnerDouble(4.5), InnerDouble(-4.0), InnerDouble(-4.5)), result)
        }
    }

    @Test
    fun imagpartBigintRational() {
        parseAPLExpression("math:im (int:asBigint 1234) (int:asBigint 1234567890123456789012345678901234567890) 1r3").let { result ->
            assert1DArray(
                arrayOf(
                    InnerBigIntOrLong(0),
                    InnerBigIntOrLong(0),
                    InnerBigIntOrLong(0)),
                result)
        }
    }

    @Test
    fun imagpartMixed() {
        parseAPLExpression("math:im 0 1 1.5").let { result ->
            assert1DArray(arrayOf(0, 0, InnerDouble(0.0)), result)
        }
    }

    @Test
    fun imagpartMixedWithComplex() {
        parseAPLExpression("math:im 0 1 1.5 6j7").let { result ->
            assert1DArray(arrayOf(0, 0, InnerDouble(0.0), InnerDouble(7.0)), result)
        }
    }

    @Test
    fun realpartOnCharacterShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("math:re \"foo\"")
        }
    }

    @Test
    fun imagpartOnCharacterShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("math:im \"foo\"")
        }
    }

    @Test
    fun realpartOnNullShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("math:re null null")
        }
    }

    @Test
    fun imagpartOnNullShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("math:im null null")
        }
    }
}
