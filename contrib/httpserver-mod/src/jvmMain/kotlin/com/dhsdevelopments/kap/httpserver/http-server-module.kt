package com.dhsdevelopments.kap.httpserver

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.makeAPLNumberAsBoolean
import org.eclipse.jetty.server.*
import org.eclipse.jetty.util.Callback
import java.nio.ByteBuffer
import java.util.concurrent.CopyOnWriteArrayList

class HttpServerModule : KapModule {
    override val name get() = "http-server"

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("httpserver")
        engine.registerFunction(ns.internAndExport("start"), StartServerFunction(this))
        engine.registerFunction(ns.internAndExport("addHandler"), AddHandlerFunction(this))
    }

    var server: Server? = null
    val handlers = CopyOnWriteArrayList<RegisteredHandler>()

    fun registerHandler(regexpString: String, handler: APLFunction) {
        handlers.add(RegisteredHandler(regexpString, handler))
    }

    override fun close() {
        server?.stop()
    }
}

class RegisteredHandler(regexpString: String, val handler: APLFunction) {
    val regexp = regexpString.toRegex()
}

class StartServerFunction(val module: HttpServerModule) : APLFunctionDescriptor {
    inner class StartServerFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            if (module.server != null) {
                throwAPLException(APLEvalException("Server is already started", pos))
            }

            val args = KeywordList.parse(a, pos)
            val port = args.lookupInteger("port", pos) ?: 8080

            val server = Server()
            server.handler = RootHandler(module, context.engine)
            val connector = ServerConnector(server)
            connector.port = port
            server.addConnector(connector)
            server.start()
            module.server = server

            context.engine.standardOutput.writeString("Server started\n")

            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = StartServerFunctionImpl(instantiation)
}

class AddHandlerFunction(val module: HttpServerModule) : APLFunctionDescriptor {
    inner class AddHandlerFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(2, 2, pos) {
        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val (path, handlerFn) = args
            val h = handlerFn.unwrapDeferredValue()
            if (h !is LambdaValue) {
                throwAPLException(APLIllegalArgumentException("Handler must be a lambda value, got: ${h.kapClass.name}", pos))
            }
            module.registerHandler(path.toStringValue(pos), h.makeClosure())
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = AddHandlerFunctionImpl(instantiation)
}

private class RootHandler(val module: HttpServerModule, val engine: Engine) : Handler.Abstract() {
    override fun handle(request: Request, response: Response, callback: Callback): Boolean {
        val path = request.httpURI.decodedPath
        module.handlers.forEach { h ->
            val res = h.regexp.matchEntire(path)
            if (res != null) {
                engine.backgroundDispatcher.start {
                    callHandlerAndSendHttpResponse(h, res, request, response, callback)
                }
                return true
            }
        }
        return false
    }

    private fun callHandlerAndSendHttpResponse(
        h: RegisteredHandler,
        matchResult: MatchResult,
        request: Request,
        response: Response,
        callback: Callback
    ) {
        engine.withThreadLocalAssigned {
            val context = RuntimeContext(engine)
            try {
                val groups = matchResult.groups
                val pathMatches = APLArrayImpl(dimensionsOfSize(groups.size - 1), Array(groups.size - 1) { i ->
                    APLString.make(groups.get(i + 1)?.value ?: "")
                })

                val headerList = ArrayList<Pair<APLValue.APLValueKey, APLValue>>()
                request.headers.forEach { field ->
                    headerList.add(Pair(APLString.make(field.lowerCaseName).makeTypeQualifiedKey(), APLString.make(field.value)))
                }
                val headers = APLMap(ImmutableMap2.makeFromContent(headerList))

                val resultParamsList = ArrayList<Pair<APLValue.APLValueKey, APLValue>>()
                Request.extractQueryParameters(request)?.forEach { field ->
                    resultParamsList.add(Pair(APLString.make(field.name).makeTypeQualifiedKey(), APLString.make(field.value)))
                }
                val paramsArg = APLMap(ImmutableMap2.makeFromContent(resultParamsList))

                val handlerArgument = APLMap(
                    ImmutableMap2.makeFromContent(
                        listOf(
                            mkKeyword("url").makeTypeQualifiedKey() to APLString.make(request.httpURI.asString()),
                            mkKeyword("path").makeTypeQualifiedKey() to APLString.make(request.httpURI.decodedPath),
                            mkKeyword("query").makeTypeQualifiedKey() to APLString.make(request.httpURI.query ?: ""),
                            mkKeyword("queryFields").makeTypeQualifiedKey() to paramsArg,
                            mkKeyword("port").makeTypeQualifiedKey() to request.httpURI.port.makeAPLNumber(),
                            mkKeyword("pathMatches").makeTypeQualifiedKey() to pathMatches,
                            mkKeyword("headers").makeTypeQualifiedKey() to headers,
                            mkKeyword("secure").makeTypeQualifiedKey() to request.isSecure.makeAPLNumberAsBoolean(),
                            mkKeyword("length").makeTypeQualifiedKey() to request.length.makeAPLNumber())))

                val evalRes = h.handler.eval1Arg(context, handlerArgument, null).collapse()
                processResult(engine, evalRes, response, callback)
            } catch (e: Exception) {
                callback.failed(e)
            }
        }
    }

    private fun processResult(engine: Engine, result: APLValue, response: Response, callback: Callback) {
        val mapResult = if (result is APLMap) result else null
        response.status = lookupInt(engine, mapResult, "status", 200)
        val headersArg = lookupValue(engine, "headers", mapResult)
        if (headersArg != null && headersArg is APLMap) {
            headersArg.content.forEach { (key, value) ->
                response.headers.add(key.value.toStringValue(), value.toStringValue())
            }
        } else {
            response.headers.add("Content-Type", "text/plain; charset=utf-8")
        }
        val outputValue = if (mapResult == null) {
            result
        } else {
            lookupValue(engine, "data", mapResult) ?: APLNullValue
        }
        val array = outputValue.asByteArray()
        response.headers.add("Content-Length", array.size.toLong())
        response.write(true, ByteBuffer.wrap(array), Callback.NOOP)
        callback.succeeded()
    }

    private fun lookupInt(engine: Engine, map: APLMap?, name: String, defaultValue: Int): Int {
        val value = lookupValue(engine, name, map)
        if (value != null) {
            return value.ensureNumber().asInt()
        }
        return defaultValue
    }

    private fun lookupValue(engine: Engine, name: String, map: APLMap?): APLValue? {
        val sym = engine.keywordNamespace.internSymbol(name)
        val value = map?.content?.get(APLSymbol(sym).makeTypeQualifiedKey())
        return value
    }

    private fun mkKeyword(name: String): APLSymbol {
        return APLSymbol(engine.keywordNamespace.internSymbol(name))
    }
}
