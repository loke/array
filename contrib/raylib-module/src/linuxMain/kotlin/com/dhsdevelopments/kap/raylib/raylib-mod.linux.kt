package com.dhsdevelopments.kap.raylib

import com.dhsdevelopments.kap.*

actual fun parseDefaultRaylibStructs(): List<RaylibApiRoot> {
    throw ModuleInitFailedException("Linux implementation does not have a default function descriptor file")
}

class LinuxRaylibModuleBuilder : KapModuleBuilder {
    override val name get() = "raylib"
    override val description get() = "Use the raylib library for Kap"

    override fun canBeInstantiated() = true

    override fun configurationParameters(): List<ParameterInfo> {
        return listOf(ParameterInfo("file", "Path to the JSON function descriptor files. Comma separated.", ParameterInfo.ParameterType.STRING, true))
    }

    override fun makeModuleWithParams(params: Map<String, Any>): KapModule {
        val path = params["file"] ?: throw ModuleInitFailedException("Missing parameter 'file'")
        if (path !is String) throw ModuleInitFailedException("Parameter 'file' should be a string")
        val mod = RaylibModule()
        mod.jsonFiles = parseJsonFiles(path)
        return mod
    }

    private fun parseJsonFiles(paths: String): List<RaylibApiRoot> {
        val resultList = ArrayList<RaylibApiRoot>()
        paths.split(",").map { it.trim() }.forEach { f ->
            try {
                openInputCharFile(f).use { input ->
                    val s = input.lines().joinToString("\n")
                    resultList.add(parseRaylibFile(s))
                }
            } catch (e: MPFileException) {
                throw ModuleBuilderInitFailedException("Error loading file ${f}: ${e.message}")
            }
        }
        return resultList
    }
}
