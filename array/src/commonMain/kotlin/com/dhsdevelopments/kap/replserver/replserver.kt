package com.dhsdevelopments.kap.replserver

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.json.*
import com.dhsdevelopments.kap.mpthread.MPLock
import com.dhsdevelopments.kap.mpthread.startThread
import com.dhsdevelopments.kap.mpthread.withLocked
import com.dhsdevelopments.kap.net.NetworkApi
import com.dhsdevelopments.kap.net.TcpConnection
import com.dhsdevelopments.kap.net.TcpServerSocket

class ReplServerDisconnectedException(message: String) : Exception("Repl server disconnected: ${message}")

interface ReplServerCommandHandler {
    fun handleCommand(doc: JsonObject, replServerConnection: ReplServerConnection<*>)
}

abstract class ReplServer(val engine: Engine) {
    abstract fun start()
    abstract fun stop()
}

abstract class ReplServerConnection<T : ReplServer>(val replServer: T) {
    private val handlers = mapOf("syms" to SymbolsHandler())

    fun processCommand(doc: JsonElement) {
        if (doc !is JsonObject) {
            throw ReplServerDisconnectedException("Invalid object type: ${doc::class.simpleName}")
        }
        val cmd = doc.lookup("cmd") ?: throw ReplServerDisconnectedException("No cmd field in input: ${doc}")
        if (cmd !is JsonString) {
            throw ReplServerDisconnectedException("cmd is not a string, got: ${cmd}")
        }
        val handler = handlers[cmd.value] ?: throw ReplServerDisconnectedException("Unknown command: ${cmd.value}")
        handler.handleCommand(doc, this)
    }

    abstract fun writeResponse(content: String)
    abstract fun close()
}

class TcpReplServer(private val port: Int, private val nwapi: NetworkApi, engine: Engine) : ReplServer(engine) {
    private val lock = MPLock()
    private var state = State.INIT
    private var socket: TcpServerSocket? = null
    private val connections = HashSet<TcpReplServerConnection>()

    fun state() = lock.withLocked { state }

    override fun start() {
        if (state() != State.INIT) {
            throw IllegalStateException("State is not INIT. Got: ${state.name}")
        }
        val host = "::1"
        val s = nwapi.startListener(port, host)
        lock.withLocked {
            socket = s
            state = State.RUNNING
        }
        startThread {
            connectionLoop(s)
        }
        engine.standardOutput.writeString("TcpServer started: host=${host} port=${port}\n")
    }

    override fun stop() {
        val connectionsCopy = lock.withLocked {
            if (state != State.RUNNING) {
                KapLogger.w { "Attempting to stop server when state = ${state}" }
                return
            }
            state = State.STOPPING
            HashSet(connections)
        }
        connectionsCopy.forEach { conn ->
            try {
                conn.close()
            } catch (e: MPFileException) {
                KapLogger.w(e) { "Exception when closing repl connection" }
            }
        }
        val socketCopy = socket
        if (socketCopy != null) {
            socketCopy.close()
            socket = null
            state = State.STOPPED
        }
    }

    private fun connectionLoop(socket: TcpServerSocket) {
        while (state == State.RUNNING) {
            val conn = socket.accept()
            val tcpReplServerConnection = TcpReplServerConnection(this, conn)
            lock.withLocked {
                connections.add(tcpReplServerConnection)
            }
            startThread {
                tcpReplServerConnection.clientLoop()
            }
        }
        socket.close()
    }

    enum class State {
        INIT,
        RUNNING,
        STOPPING,
        STOPPED
    }
}

class TcpReplServerConnection(replServer: TcpReplServer, val conn: TcpConnection) : ReplServerConnection<TcpReplServer>(replServer) {
    private val input = ByteToCharacterProvider(conn.byteProvider)
    private val output = CharacterToByteConsumer(conn.byteConsumer)

    fun clientLoop() {
        try {
            val jsonDecoder = makeJsonDecoder()
            while (replServer.state() == TcpReplServer.State.RUNNING) {
                val s = input.nextLine() ?: break
                val doc = jsonDecoder.decodeJsonFromString(s)
                processCommand(doc)
            }
        } catch (ex: ReplServerDisconnectedException) {
            KapLogger.e(ex) { "Protocol error: ${ex.message}" }
        } finally {
            conn.close()
        }
    }

    override fun writeResponse(content: String) {
        if (content.contains("\n")) {
            throw ReplServerDisconnectedException("Response contained a newline: ${content}")
        }
        output.writeString("${content}\n")
    }

    override fun close() {
        conn.close()
    }
}

class SymbolsHandler : ReplServerCommandHandler {
    override fun handleCommand(doc: JsonObject, replServerConnection: ReplServerConnection<*>) {
        val prefix = lookupArgString(doc.lookup("args"), "prefix")
        val result = ArrayList<String>()
        replServerConnection.replServer.engine.iterateSymbolsInImports { sym ->
            val name = sym.symbolName
            if (name.length > 1 && (prefix == null || name.startsWith(prefix))) {
                result.add(name)
            }
            false
        }
        val buf = StringBuilder()
        buf.append("[")
        var first = true
        result.map { sym ->
            if (first) {
                first = false
            } else {
                buf.append(",")
            }
            buf.append("{\"name\":")
            buf.append(makeJsonString(sym))
            buf.append("}")
        }
        buf.append("]")
        replServerConnection.writeResponse(buf.toString())
    }

    private fun lookupArgString(args: JsonElement?, field: String): String? {
        if (args == null) {
            return null
        }
        val fieldValue = (args as JsonObject).lookup(field) ?: return null
        return (fieldValue as JsonString).value
    }
}
