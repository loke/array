package com.dhsdevelopments.kap

import com.dhsdevelopments.mpbignum.BigInt
import java.lang.ref.WeakReference
import java.math.BigInteger
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock
import java.util.regex.PatternSyntaxException
import kotlin.concurrent.schedule
import kotlin.concurrent.withLock

actual fun sleepMillis(engine: Engine, time: Long) {
    require(time >= 0)
    val nd = engine.nativeData as JvmNativeData
    val stopTime = System.currentTimeMillis() + time
    nd.breakNotifierLock.withLock {
        while (true) {
            val currTime = System.currentTimeMillis()
            if (nd.breakState || currTime >= stopTime) {
                break
            }
            nd.breakNotifierCondition.await(stopTime - currTime, TimeUnit.MILLISECONDS)
        }
    }
    engine.checkInterrupted()
}

actual fun sleepMillisCallback(engine: Engine, time: Long, pos: Position, callback: () -> Either<APLValue, Exception>) {
    val jvmNative = engine.nativeData as JvmNativeData
    val pub = jvmNative.findAsyncJobPublisher(pos)
    val job = AsyncJob(callback)
    jvmNative.timer.schedule(time) {
        pub(job)
    }
}

actual fun Double.formatDouble() = this.toString()

actual fun toRegexpWithException(string: String, options: Set<RegexOption>): Regex {
    return try {
        string.toRegex(options)
    } catch (e: PatternSyntaxException) {
        throw RegexpParseException("Error parsing regexp: \"${string}\"", e)
    }
}

actual fun numCores(): Int {
    return Runtime.getRuntime().availableProcessors()
}

class JvmMPThreadPoolExecutor(val maxNumParallel: Int) : MPThreadPoolExecutor {
    private val executor = Executors.newFixedThreadPool(maxNumParallel)
    override val numThreads get() = maxNumParallel

    override fun <T : Any> start(fn: () -> T): JvmTExecutorTask<T> {
        val future = executor.submit<T>(fn)
        return JvmTExecutorTask(future)
    }

    override fun close() {
        executor.shutdown()
    }

    inner class JvmTExecutorTask<T : Any>(val future: Future<out T>) : BackgroundTask<T> {
        override fun await(): T {
            return future.get()
        }
    }
}

actual fun makeBackgroundDispatcher(numThreads: Int): MPThreadPoolExecutor {
    return JvmMPThreadPoolExecutor(numThreads)
}

class JvmWeakReference<T : Any>(ref: T) : MPWeakReference<T> {
    private val instance = WeakReference(ref)

    override val value: T? get() = instance.get()
}

actual fun <T : Any> MPWeakReference.Companion.make(ref: T): MPWeakReference<T> {
    return JvmWeakReference(ref)
}

class AsyncJob(val callback: () -> Either<APLValue, Exception>)

class JvmNativeData : NativeData {
    val timer = Timer()
    var asyncJobPublisher: ((AsyncJob) -> Unit)? = null
    var keyboardInput: KeyboardInput = KeyboardInputJvm()

    val breakNotifierLock = ReentrantLock()
    val breakNotifierCondition: Condition = breakNotifierLock.newCondition()
    var breakState = false

    override fun close() {
        timer.cancel()
        keyboardInput.close()
    }

    fun findAsyncJobPublisher(pos: Position?): (AsyncJob) -> Unit {
        return asyncJobPublisher
            ?: throwAPLException(KapInternalError("Engine not configured to handle handle async calls", pos))
    }
}

actual fun makeNativeData(): NativeData = JvmNativeData()

@Suppress("NOTHING_TO_INLINE")
actual inline fun nativeUpdateBreakPending(engine: Engine, state: Boolean) {
    val nd = engine.nativeData as JvmNativeData
    nd.breakNotifierLock.withLock {
        nd.breakState = state
        nd.breakNotifierCondition.signalAll()
    }
}

@Suppress("NOTHING_TO_INLINE")
actual inline fun nativeBreakPending(engine: Engine): Boolean = false

actual fun findInstallPath(): String? {
    return System.getProperty("kap.installPath")
}

actual val brokenNegativeZero: Boolean = false

fun BigInteger.makeAPLNumber() = BigInt.of(this)
