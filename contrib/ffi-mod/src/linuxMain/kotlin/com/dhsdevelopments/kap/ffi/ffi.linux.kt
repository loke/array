package com.dhsdevelopments.kap.ffi

import kotlinx.cinterop.*
import libffi.*
import platform.posix.*

@OptIn(ExperimentalForeignApi::class)
class LinuxFfiManager : FfiManager {
    override fun loadLibrary(file: String): FfiLibrary {
        val handle = dlopen(file, RTLD_LAZY) ?: throw LoadLibraryFailedException("Failed to load ${file}: ${strerror(errno)}")
        return LinuxFfiLibrary(file, handle)
    }

    override fun allocBytes(size: Long): FfiBuffer {
        return LinuxFfiBuffer(size)
    }

    override fun defineStruct(vararg fields: InputArgType<*>): FfiStructDefinition {
        return LinuxFfiStructDefinition(fields.map { v -> v as LinuxInputArgType<*> })
    }

    override fun close() {
    }

    override fun makeInputArgChar() = LinuxInputArgTypeByte
    override fun makeInputArgInt() = LinuxInputArgTypeInt
    override fun makeInputArgLong() = LinuxInputArgTypeLong
    override fun makeInputArgFloat() = LinuxInputArgTypeFloat
    override fun makeInputArgDouble() = LinuxInputArgTypeDouble
    override fun makeInputArgString() = LinuxInputArgTypeString
    override fun makeInputArgPtr() = LinuxInputArgTypePtr
    override fun makeInputArgBoolean() = LinuxInputArgTypeBoolean
    override fun makeInputArgTypeVoid(): InputArgType<FfiManager.VoidWrapper> = LinuxInputArgTypeVoid

    override fun makeInputArgStruct(structDefinition: FfiStructDefinition) = LinuxInputArgStruct(structDefinition as LinuxFfiStructDefinition)
    override fun <T> makeInputArgPointerToArg(type: InputArgType<T>) = LinuxInputArgPointerToArg(type as LinuxInputArgType<T>)

    override fun makeNullPtr() = LinuxFfiPointer(null)

    companion object {
        object LinuxInputArgTypeByte : LinuxInputArgType<Byte>(ffi_type_uint8.ptr) {
            override fun convertToNative(placement: NativePlacement, value: Any): CPointer<CPointed> = placement.alloc(value as Byte).ptr

            override fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any) {
                val v = ptr.reinterpret<ByteVar>()
                v.pointed.value = value as Byte
            }

            override fun readValueAtOffset(ptr: COpaquePointer) = ptr.reinterpret<ByteVar>().pointed.value
        }

        object LinuxInputArgTypeInt : LinuxInputArgType<Int>(ffi_type_sint32.ptr) {
            override fun convertToNative(placement: NativePlacement, value: Any): CPointer<CPointed> = placement.alloc(value as Int).ptr

            override fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any) {
                val v = ptr.reinterpret<IntVar>()
                v.pointed.value = value as Int
            }

            override fun readValueAtOffset(ptr: COpaquePointer) = ptr.reinterpret<IntVar>().pointed.value
        }

        object LinuxInputArgTypeLong : LinuxInputArgType<Long>(ffi_type_sint64.ptr) {
            override fun convertToNative(placement: NativePlacement, value: Any): CPointer<CPointed> = placement.alloc(value as Long).ptr

            override fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any) {
                val v = ptr.reinterpret<LongVar>()
                v.pointed.value = value as Long
            }

            override fun readValueAtOffset(ptr: COpaquePointer) = ptr.reinterpret<LongVar>().pointed.value
        }

        object LinuxInputArgTypeFloat : LinuxInputArgType<Float>(ffi_type_float.ptr) {
            override fun convertToNative(placement: NativePlacement, value: Any): CPointer<CPointed> = placement.alloc(value as Float).ptr

            override fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any) {
                val v = ptr.reinterpret<FloatVar>()
                v.pointed.value = value as Float
            }

            override fun readValueAtOffset(ptr: COpaquePointer) = ptr.reinterpret<FloatVar>().pointed.value
        }

        object LinuxInputArgTypeDouble : LinuxInputArgType<Double>(ffi_type_double.ptr) {
            override fun convertToNative(placement: NativePlacement, value: Any): CPointer<CPointed> = placement.alloc(value as Double).ptr

            override fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any) {
                val v = ptr.reinterpret<DoubleVar>()
                v.pointed.value = value as Double
            }

            override fun readValueAtOffset(ptr: COpaquePointer) = ptr.reinterpret<DoubleVar>().pointed.value
        }

        object LinuxInputArgTypeBoolean : LinuxInputArgType<Boolean>(ffi_type_uint8.ptr) {
            override fun convertToNative(placement: NativePlacement, value: Any): CPointer<CPointed> =
                placement.alloc(if (value as Boolean) 1.toByte() else 0.toByte()).ptr

            override fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any) {
                val v = ptr.reinterpret<ByteVar>()
                v.pointed.value = if (value as Boolean) 1.toByte() else 0.toByte()
            }

            override fun readValueAtOffset(ptr: COpaquePointer) = ptr.reinterpret<ByteVar>().pointed.value != 0.toByte()
        }

        object LinuxInputArgTypeString : LinuxInputArgType<String?>(ffi_type_pointer.ptr) {
            override fun convertToNative(placement: NativePlacement, value: Any): CPointer<CPointed> {
                val buf = writeToNewBuffer(value, placement)
                val argWrapper = placement.alloc<CArrayPointerVar<ByteVar>>()
                argWrapper.value = buf
                return argWrapper.ptr
            }

            private fun writeToNewBuffer(value: Any, placement: NativePlacement): CArrayPointer<ByteVar> {
                val bytes = (value as String).encodeToByteArray()
                val buf = placement.allocArray<ByteVar>(bytes.size + 1)
                for (i in bytes.indices) {
                    buf[i] = bytes[i]
                }
                buf[bytes.size] = 0
                return buf
            }

            override fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any) {
                val v = ptr.reinterpret<COpaquePointerVar>()
                v.pointed.value = writeToNewBuffer(value as String, placement)
            }

            override fun readValueAtOffset(ptr: COpaquePointer): String {
                val v = ptr.reinterpret<CPointerVar<ByteVar>>()
                return v.pointed.value!!.toKString()
            }
        }

        object LinuxInputArgTypePtr : LinuxInputArgType<FfiPointer>(ffi_type_pointer.ptr) {
            override fun convertToNative(placement: NativePlacement, value: Any): CPointer<CPointed> {
                val arg = placement.alloc<COpaquePointerVar>()
                arg.value = (value as LinuxFfiPointer).ptr
                return arg.ptr
            }

            override fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any) {
                val v = ptr.reinterpret<COpaquePointerVar>()
                v.pointed.value = (value as LinuxFfiPointer).ptr
            }

            override fun readValueAtOffset(ptr: COpaquePointer): FfiPointer {
                val v = ptr.reinterpret<CPointerVar<COpaquePointerVar>>()
                return LinuxFfiPointer(v.pointed.value?.reinterpret())
            }
        }

        object LinuxInputArgTypeVoid : LinuxInputArgType<FfiManager.VoidWrapper>(ffi_type_void.ptr) {
            override fun convertToNative(placement: NativePlacement, value: Any) = error("No value to read from void")

            //            override fun convertToKotlin(rc: ffi_argVar) = FfiManager.VoidWrapper
            override fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any) = error("No value to read from void")
            override fun readValueAtOffset(ptr: COpaquePointer) = FfiManager.VoidWrapper
        }
    }
}

@OptIn(ExperimentalForeignApi::class)
class LinuxInputArgPointerToArg<T>(val innerType: LinuxInputArgType<T>) : LinuxInputArgType<T>(ffi_type_pointer.ptr) {
    override fun convertToNative(placement: NativePlacement, value: Any): COpaquePointer {
        val ptr = innerType.convertToNative(placement, value)
        val buf = placement.alloc<COpaquePointerVar>()
        buf.value = ptr
        return buf.ptr
    }

    override fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any) {
        TODO("not implemented")
    }

    override fun readValueAtOffset(ptr: COpaquePointer): T {
        val ptrToValue = ptr.reinterpret<CPointerVar<CPointed>>().pointed.value ?: throw GenericFfiException("Pointer is null")
        return innerType.readValueAtOffset(ptrToValue.reinterpret())
    }
}

@OptIn(ExperimentalForeignApi::class)
class LinuxInputArgStruct(val structDefinition: LinuxFfiStructDefinition) : LinuxInputArgType<StructResult>(structDefinition.ffiType.ptr) {
    override fun convertToNative(placement: NativePlacement, value: Any): COpaquePointer {
        val v0 = value as LinuxStructResult
        require(structDefinition.fields.size == v0.size())
        val res = placement.allocArray<ByteVar>(structDefinition.size)
        writeStructAtPtr(placement, res, v0)
        return res
    }

    override fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any) {
        writeStructAtPtr(placement, ptr, value as LinuxStructResult)
    }

    override fun readValueAtOffset(ptr: COpaquePointer): StructResult {
        return readStructAtPtr(ptr)
    }

    private fun writeStructAtPtr(placement: NativePlacement, ptr: COpaquePointer, value: LinuxStructResult) {
        structDefinition.fields.forEachIndexed { i, field ->
            val addr: COpaquePointer = ptr.rawValue.plus(structDefinition.offsets[i]).toLong().toCPointer()!!
            field.writeValueAtOffset(placement, addr, value.elements[i])
        }
    }

    fun readStructAtPtr(ptr: COpaquePointer): StructResult {
        val elements = Array(structDefinition.fields.size) { i ->
            val field = structDefinition.fields[i]
            val addr: COpaquePointer = ptr.rawValue.plus(structDefinition.offsets[i]).toLong().toCPointer()!!
            field.readValueAtOffset(addr) ?: throw GenericFfiException("Reading nulls from a struct is not supported")
        }
        return LinuxStructResult(elements)
    }

    class LinuxStructResult(val elements: Array<Any>) : StructResult {
        override fun getAtIndex(index: Int) = elements[index]
        override fun size() = elements.size
    }
}

@ExperimentalForeignApi
class LinuxFfiStructDefinition(val fields: List<LinuxInputArgType<*>>) : FfiStructDefinition {
    val ffiType: ffi_type
    val offsets: LongArray

    override val size get() = ffiType.size.toLong()

    init {
        ffiType = nativeHeap.alloc<ffi_type>()
        ffiType.size = 0UL
        ffiType.alignment = 0U
        ffiType.type = FFI_TYPE_STRUCT.toUShort()

        val elements = nativeHeap.allocArray<CPointerVar<ffi_type>>(fields.size + 1)
        fields.forEachIndexed { i, field ->
            elements[i] = field.type
        }
        elements[fields.size] = null
        ffiType.elements = elements

        var offsetsRet: LongArray? = null
        memScoped {
            val cif = alloc<ffi_cif>()
            ffi_prep_cif(cif.ptr, FFI_DEFAULT_ABI, 0.toUInt(), ffiType.ptr, null).let { res ->
                if (res != FFI_OK) {
                    throw GenericFfiException("Error preparing ffi type")
                }
            }

            val sizes = allocArray<size_tVar>(fields.size)
            ffi_get_struct_offsets(FFI_DEFAULT_ABI, ffiType.ptr, sizes).let { res ->
                if (res != FFI_OK) {
                    throw GenericFfiException("Error getting offsets")
                }
            }

            offsetsRet = LongArray(fields.size) { i ->
                sizes[i].toLong()
            }
        }
        offsets = offsetsRet!!
    }

    override fun derivePtr(base: FfiPointer, index: Int): FfiPointer {
        val basePtr = (base as LinuxFfiPointer).ptr ?: throw GenericFfiException("Attempt to derive a struct pointer from null")
        return LinuxFfiPointer(basePtr.rawValue.plus(offsets[index]).toLong().toCPointer())
    }

    override fun makeStructResult(values: Array<Any>): StructResult {
        return LinuxInputArgStruct.LinuxStructResult(values)
    }
}

@OptIn(ExperimentalForeignApi::class)
abstract class LinuxInputArgType<T>(val type: CPointer<ffi_type>) : InputArgType<T> {
    abstract fun convertToNative(placement: NativePlacement, value: Any): COpaquePointer
    abstract fun writeValueAtOffset(placement: NativePlacement, ptr: COpaquePointer, value: Any)
    abstract fun readValueAtOffset(ptr: COpaquePointer): T
}

actual fun ffiManager(): FfiManager {
    return LinuxFfiManager()
}

@OptIn(ExperimentalForeignApi::class)
class LinuxFfiLibrary(override val name: String, val handle: CPointer<out CPointed>) : FfiLibrary {
    override fun <R> findFunction(name: String, returnType: InputArgType<R>, argTypes: List<InputArgType<*>>): FfiFunctionDescriptor<R>? {
        val fnPtr = dlsym(handle, name) ?: return null

        returnType as LinuxInputArgType<R>
        val cif = nativeHeap.alloc<ffi_cif>()
        val nativeArgTypeList = nativeHeap.allocArray<CPointerVar<ffi_type>>(argTypes.size)
        argTypes.forEachIndexed { i, type ->
            nativeArgTypeList[i] = (type as LinuxInputArgType).type
        }
        ffi_prep_cif(cif.ptr, FFI_DEFAULT_ABI, argTypes.size.toUInt(), returnType.type, nativeArgTypeList).let { res ->
            if (res != FFI_OK) {
                throw GenericFfiException("Error preparing cif value: ${res}")
            }
        }
        return LinuxFfiFunctionDescriptor(returnType, argTypes.map { v -> v as LinuxInputArgType<*> }, fnPtr, cif)
    }
}

@OptIn(ExperimentalForeignApi::class)
class LinuxFfiFunctionDescriptor<R>(
    override val returnType: LinuxInputArgType<R>,
    override val argTypes: List<LinuxInputArgType<*>>,
    val fnPtr: CPointer<out CPointed>,
    cifReference: ffi_cif
) : FfiFunctionDescriptor<R> {

    private val cif = cifReference

    override fun call(args: List<Any>, outputParams: Set<Int>?): FfiFunctionDescriptor.CallResult<R> {
        memScoped {
            require(args.size == argTypes.size)
            val rc = alloc(returnType.type.pointed.size.toLong(), returnType.type.pointed.alignment.toInt()).reinterpret<ByteVar>()
            val nativeArgsList = allocArray<COpaquePointerVar>(args.size)
            argTypes.forEachIndexed { i, type ->
                nativeArgsList[i] = type.convertToNative(this, args[i])
            }
            ffi_call(cif.ptr, fnPtr.reinterpret(), rc.ptr, nativeArgsList)
            return convertReturnArgs(rc.ptr, outputParams, nativeArgsList)
//            return returnType.convertToKotlin(rc)
        }
    }

    private fun convertReturnArgs(
        rc: COpaquePointer,
        outputParams: Set<Int>?,
        nativeArgsList: CArrayPointer<COpaquePointerVar>): FfiFunctionDescriptor.CallResult<R> {
        val outputs = if (outputParams == null) {
            emptyList()
        } else {
            argTypes.mapIndexed { i, argType ->
                if (outputParams.contains(i)) {
                    argType.readValueAtOffset(nativeArgsList[i]!!)
                } else {
                    null
                }
            }
        }
        return FfiFunctionDescriptor.CallResult(returnType.readValueAtOffset(rc), outputs)
    }
}

@OptIn(ExperimentalForeignApi::class)
class LinuxFfiBuffer(size: Long) : FfiBuffer {
    private val ptr = nativeHeap.allocArray<ByteVar>(size)

    override fun pointerToOffset(offset: Long): FfiPointer {
        return LinuxFfiPointer(ptr.plus(offset)!!.reinterpret())
    }

    override fun close() {
        nativeHeap.free(ptr)
    }
}

@OptIn(ExperimentalForeignApi::class)
class LinuxFfiPointer(val ptr: CPointer<CPointed>?) : FfiPointer {
    override fun readLongAtOffset(index: Long): Long {
        return ptr.rawValue.plus(index).toLong().toCPointer<LongVar>()!!.pointed.value
    }

    override fun writeLongAtOffset(index: Long, value: Long) {
        ptr.rawValue.plus(index).toLong().toCPointer<LongVar>()!!.pointed.value = value
    }

    override fun readByteAtOffset(index: Long): Byte {
        return ptr.rawValue.plus(index).toLong().toCPointer<ByteVar>()!!.pointed.value
    }

    override fun writeByteAtOffset(index: Long, value: Byte) {
        ptr.rawValue.plus(index).toLong().toCPointer<ByteVar>()!!.pointed.value = value
    }

    override fun copyBytesToArray(offset: Long, buf: ByteArray, bufPosition: Int, size: Int) {
        if (ptr == null) {
            throw GenericFfiException("Attempt to copy data from null pointer")
        }
        val src = ptr.reinterpret<ByteVar>()
        for (i in 0 until size) {
            buf[bufPosition + i] = src[offset + i]
        }
    }

    override fun copyBytesFromArray(offset: Long, buf: ByteArray, bufPosition: Int, size: Int) {
        if (ptr == null) {
            throw GenericFfiException("Attempt to copy data from null pointer")
        }
        val dest = ptr.reinterpret<ByteVar>()
        for (i in 0 until size) {
            dest[offset + i] = buf[bufPosition + i]
        }
    }
}
