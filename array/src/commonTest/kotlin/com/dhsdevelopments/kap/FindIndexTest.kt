package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals

class FindIndexTest : APLTest() {
    @Test
    fun singleIndex() {
        parseAPLExpression("30 ⍳ 10 20 30 40").let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(1, 1, 0, 1), result)
        }
    }

    @Test
    fun multiIndexes() {
        parseAPLExpression("10 60 ⍳ 10 20 30 40 50 60").let { result ->
            assertDimension(dimensionsOfSize(6), result)
            assertArrayContent(arrayOf(0, 2, 2, 2, 2, 1), result)
        }
    }

    @Test
    fun indexTestPlain() {
        assertSimpleNumber(1, parseAPLExpression("10 20 30 40 50 ⍳ 20"))
    }

    @Test
    fun indexTestMultiArg() {
        parseAPLExpression("10 20 30 40 50 60 70 80 90 ⍳ 10 50").let { result ->
            assertDimension(dimensionsOfSize(2), result)
            assertArrayContent(arrayOf(0, 4), result)
        }
    }

    @Test
    fun indexNoIndexFound() {
        assertSimpleNumber(4, parseAPLExpression("10 20 30 40 ⍳ 1"))
    }

    @Test
    fun indexNotFoundMultiArg() {
        parseAPLExpression("10 20 30 40 ⍳ 1 2").let { result ->
            assertDimension(dimensionsOfSize(2), result)
            assertArrayContent(arrayOf(4, 4), result)
        }
    }

    @Test
    fun indexWithMultiDimensionalLeftArg() {
        parseAPLExpression("9 6 1 2 0 ⍳ (2 2 ⍴ ⍳4)").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(4, 2, 3, 5), result)
        }
    }

    @Test
    fun indexWithOptimisation0() {
        val (result, out) = parseAPLExpressionWithOutput(
            """
            |a ⇐ { io:print ⍵ }
            |(a¨ 10 11 12 13 14 15 16) ⍳ 12
            """.trimMargin())
        assertSimpleNumber(2, result)
        assertEquals("101112", out)
    }

    @Test
    fun indexWithOptimisation1() {
        val (result, out) = parseAPLExpressionWithOutput(
            """
            |a ⇐ { io:print ⍵ }
            |(a¨ 10 11 12 13 14 15 16) ⍳ 11 12
            """.trimMargin())
        assertDimension(dimensionsOfSize(2), result)
        assertArrayContent(arrayOf(1, 2), result)
        assertEquals("1011101112", out)
    }

    @Test
    fun indexWithStringArrayRef() {
        parseAPLExpression("foo ← \"jkc\" ⋄ \"abc\" ⍳ foo[2]").let { result ->
            assertSimpleNumber(2, result)
        }
    }

    @Test
    fun indexWithString() {
        parseAPLExpression("\"abc\" ⍳ @c").let { result ->
            assertSimpleNumber(2, result)
        }
    }

    @Test
    fun indexWithDifferentTypes0() {
        parseAPLExpression("\"abc\" ⍳ 0").let { result ->
            assertSimpleNumber(3, result)
        }
    }

    @Test
    fun indexWithSubArrays() {
        parseAPLExpression("(1 2 3) (4 5 6) ⍳ (⊂4 5 6)").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun indexOfWithTypeDifferentTypes0() {
        parseAPLExpression("2 1.0 1 ⍳ 1.0 1 4").let { result ->
            assert1DArray(arrayOf(1, 1, 3), result)
        }
    }

    @Test
    fun indexOfWithTypeDifferentTypes1() {
        parseAPLExpression("2 1 1.0 ⍳ 1.0 1 4").let { result ->
            assert1DArray(arrayOf(1, 1, 3), result)
        }
    }

    @Test
    fun negativeZeroIndexOf0() {
        parseAPLExpression("2 ¯0.0 0.0 ⍳ ¯0.0 0.0 111").let { result ->
            assert1DArray(arrayOf(1, 1, 3), result)
        }
    }

    @Test
    fun negativeZeroIndexOf1() {
        parseAPLExpression("2 0.0 ¯0.0 ⍳ ¯0.0 0.0 111").let { result ->
            assert1DArray(arrayOf(1, 1, 3), result)
        }
    }

    @Test
    fun multiDimensionalIndexOf0() {
        parseAPLExpression("(3 3 ⍴ ⍳9) ⍳ 3").let { result ->
            assertEnclosed(Inner1D(arrayOf(1, 0)), result)
        }
    }

    @Test
    fun multiDimensionalIndexOf1() {
        parseAPLExpression("(3 3 ⍴ ⍳9) ⍳ 3 0").let { result ->
            assert1DArray(arrayOf(Inner1D(arrayOf(1, 0)), Inner1D(arrayOf(0, 0))), result)
        }
    }

    @Test
    fun multiDimensionalIndexOf3D() {
        parseAPLExpression("(3 3 3 ⍴ ⍳100) ⍳ 3 15").let { result ->
            assert1DArray(arrayOf(Inner1D(arrayOf(0, 1, 0)), Inner1D(arrayOf(1, 2, 0))), result)
        }
    }

    @Test
    fun multiDimensionalLeftAndRightArgs() {
        parseAPLExpression("(4 4 ⍴ ⍳100) ⍳ (3 3 ⍴ 5+⍳9)").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(
                arrayOf(
                    Inner1D(arrayOf(1, 1)),
                    Inner1D(arrayOf(1, 2)),
                    Inner1D(arrayOf(1, 3)),
                    Inner1D(arrayOf(2, 0)),
                    Inner1D(arrayOf(2, 1)),
                    Inner1D(arrayOf(2, 2)),
                    Inner1D(arrayOf(2, 3)),
                    Inner1D(arrayOf(3, 0)),
                    Inner1D(arrayOf(3, 1))),
                result)
        }
    }

    @Test
    fun multiDimensionalMissingElementScalarRightArg() {
        parseAPLExpression("(3 3 ⍴ ⍳9) ⍳ 100").let { result ->
            assertAPLNil(result)
        }
    }

    @Test
    fun multiDimensionalMissingElementArrayRightArg() {
        parseAPLExpression("(3 3 ⍴ ⍳9) ⍳ 100 200").let { result ->
            assert1DArray(arrayOf(InnerAPLNil(), InnerAPLNil()), result)
        }
    }

    @Test
    fun multiDimensionalMissingElementArrayRightArgOneArgumentNotFound() {
        parseAPLExpression("(3 3 ⍴ ⍳9) ⍳ 1 200").let { result ->
            assert1DArray(arrayOf(Inner1D(arrayOf(0, 1)), InnerAPLNil()), result)
        }
    }
}
