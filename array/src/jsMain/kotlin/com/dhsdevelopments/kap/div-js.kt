package com.dhsdevelopments.kap

actual fun sleepMillis(engine: Engine, time: Long) {
    engine.jsNativeData().sleepWithBreakCheck(time)
}

actual fun sleepMillisCallback(engine: Engine, time: Long, pos: Position, callback: () -> Either<APLValue, Exception>) {
    setTimeout({ callback() }, time.toDouble())
}

fun Engine.jsNativeData(): JsNativeData {
    return nativeData as JsNativeData
}

private val NON_SCIENTIFIC_INTEGER_REGEX = "^-?[0-9]+$".toRegex()

actual fun Double.formatDouble(): String {
    return if (this.equals(-0.0)) {
        "-0.0"
    } else {
        val s = this.toString()
        if (NON_SCIENTIFIC_INTEGER_REGEX.matches(s)) {
            "${s}.0"
        } else {
            s
        }
    }
}

actual fun toRegexpWithException(string: String, options: Set<RegexOption>): Regex {
    return try {
        string.toRegex(options)
    } catch (e: Throwable) {
        throw RegexpParseException("Error parsing regexp: \"${string}\"", e)
    }
}

actual fun numCores() = 1

actual fun makeBackgroundDispatcher(numThreads: Int): MPThreadPoolExecutor {
    return SingleThreadedThreadPoolExecutor()
}

class JsWeakReference<T : Any>(ref: T) : MPWeakReference<T> {
    private val instance: dynamic

    init {
        @Suppress("UNUSED_VARIABLE")
        val inst = ref
        instance = js("new WeakRef(inst)")
    }

    override val value: T?
        get() {
            @Suppress("UNUSED_VARIABLE")
            val inst = instance
            val v = js("var a = inst.deref(); if(a) { return a; } else { return null; }")
            return v as T?
        }
}

actual fun <T : Any> MPWeakReference.Companion.make(ref: T): MPWeakReference<T> {
    return JsWeakReference(ref)
}

actual fun makeTimerHandler(engine: Engine): TimerHandler? = null

actual fun getEnvValue(name: String): String? = null
actual val getEnvValueSupported: Boolean get() = false

class JsNativeData : NativeData {
    lateinit var sendMessageFn: (dynamic) -> Unit
    val jsTransferQueue: ServerSideJsTransferQueue?
    var httpManager: HttpManager? = null
    var keyboardInput: KeyboardInput? = null

    init {
        jsTransferQueue = ServerSideJsTransferQueue.make()
    }

    fun sleepWithBreakCheck(time: Long) {
        val queue = jsTransferQueue ?: throw SleepNotSupportedException()
        queue.waitForUpdate(time)
    }

    override fun close() {}
}


actual fun makeNativeData(): NativeData = JsNativeData()

@Suppress("NOTHING_TO_INLINE")
actual inline fun nativeUpdateBreakPending(engine: Engine, state: Boolean) {
    engine.jsNativeData().jsTransferQueue?.updateBreakPending(state)
}

@Suppress("NOTHING_TO_INLINE")
actual inline fun nativeBreakPending(engine: Engine): Boolean {
    val jsTransferQueue = engine.jsNativeData().jsTransferQueue ?: return false
    return jsTransferQueue.currentState() == JsTransferQueue.STATE_BREAK
}

actual fun findInstallPath(): String? = null

actual val brokenNegativeZero: Boolean get() = false
