package com.dhsdevelopments.kap

import org.khronos.webgl.Int32Array
import org.khronos.webgl.Uint8Array
import org.khronos.webgl.get
import org.khronos.webgl.set
import kotlin.js.Promise
import kotlin.math.min

abstract class JsTransferQueue(
    val controlBufferData: SharedArrayBuffer,
    val contentBufferData: SharedArrayBuffer,
) {
    val controlArray: Int32Array
    val contentArray: Uint8Array

    init {
        @Suppress("UNUSED_VARIABLE")
        val buf = controlBufferData
        val a = js("new Int32Array(buf)")

        @Suppress("UNUSED_VARIABLE")
        val newState = STATE_NONE
        js("Atomics.store(a, newState, 0)")
        controlArray = a

        @Suppress("UNUSED_VARIABLE")
        val contentBuffer = contentBufferData
        contentArray = js("new Uint8Array(contentBuffer)")
    }

    fun sharedBuffers(): dynamic {
        return jsObject2(
            "controlBufferData" to controlBufferData,
            "contentBufferData" to contentBufferData)
    }

    fun currentState(): Int {
        @Suppress("UNUSED_VARIABLE")
        val b = controlArray
        return js("Atomics.load(b, 0)") as Int
    }

    protected fun ensureStateValue(index: Int, expectedState: Int) {
        val i = index

        @Suppress("UnusedVariable")
        val b = controlArray
        val s = js("Atomics.load(b, i)") as Int
        require(s == expectedState) { "State in position ${i} was ${s}, expected: ${expectedState}" }
    }

    protected fun updateStateValue(index: Int, newState: Int) {
        @Suppress("UnusedVariable")
        val i = index

        @Suppress("UnusedVariable")
        val b = controlArray

        @Suppress("UnusedVariable")
        val s = newState
        js("Atomics.store(b, i, s); Atomics.notify(b, i);")
    }

    fun encodeLong(value: Long, dest: Uint8Array, offset: Int) {
        for (i in 0 until 8) {
            dest[offset + i] = ((value shr ((7 - i) * 8)) and 0xFFL).toByte()
        }
    }

    fun decodeLong(src: Uint8Array, offset: Int): Long {
        var res = 0L
        for (i in 0 until 8) {
            res = res or ((src[offset + i].toLong() and 0xFFL) shl ((7 - i) * 8))
        }
        return res
    }

    companion object {
        const val CONTENT_BUFFER_SIZE = 1024
        const val STATE_NONE = 0
        const val STATE_BREAK = 1
        const val STATE_HTTP_RESULT = 2
        const val STATE_READ_LINE_RESULT = 3
    }
}

class ServerSideJsTransferQueue(
    controlBufferData: SharedArrayBuffer,
    contentBufferData: SharedArrayBuffer
) : JsTransferQueue(
    controlBufferData,
    contentBufferData
) {
    /**
     * Server side: Wait until state change.
     *
     * @param time maximum wait time in milliseconds
     * @return the state after the wait completed
     */
    fun waitForUpdate(time: Long? = null): Int {
        val waitTime = time?.toDouble() ?: Double.POSITIVE_INFINITY
        require(waitTime >= 0)

        @Suppress("UNUSED_VARIABLE")
        val b = controlArray

        js("Atomics.wait(b, 0, 0, waitTime)")

        return js("Atomics.load(b, 0)") as Int
    }

    fun updateBreakPending(state: Boolean) {
        @Suppress("ASSIGNED_BUT_NEVER_ACCESSED_VARIABLE")
        val newState: Int
        val expectedState: Int
        if (state) {
            newState = STATE_BREAK
            expectedState = STATE_NONE
        } else {
            newState = STATE_NONE
            expectedState = STATE_BREAK
        }

        @Suppress("UNUSED_VARIABLE")
        val b = controlArray
        val prevState = js("Atomics.compareExchange(b, 0, expectedState, newState)") as Int
        if (prevState == expectedState) {
            js("Atomics.notify(b, 0)")
        }
    }

    fun waitForStateValueServerSide(index: Int, expectedState: Int, newState: Int) {
        val i = index

        @Suppress("UnusedVariable")
        val b = controlArray
        val e = expectedState
        val s = newState
        while (true) {
            val currState = js("Atomics.load(b, i)")
            when {
                currState == newState -> break
                currState != expectedState -> error("Got state at ${i}: ${currState}, expected: ${e}, newState: ${s}")
            }
            js("Atomics.wait(b, i, currState);")
        }
    }

    fun waitForStateOrBreak(expectedState: Int): Int {
        @Suppress("UnusedVariable")
        val b = controlArray
        while (true) {
            val currState = js("Atomics.load(b, 0)")
            if (currState == expectedState || currState == STATE_BREAK) {
                return currState
            }
            js("Atomics.wait(b, 0, currState)")
        }
    }

    fun readArray(): ByteArray {
        waitForStateValueServerSide(1, 0, 1)
        val size = decodeLong(contentArray, 0).toInt()
        val resultBuf = ByteArray(size)
        var p = 0
        var currBlock = 1
        while (true) {
            val remaining = size - p
            for (i in 0 until min(CONTENT_BUFFER_SIZE, remaining)) {
                resultBuf[p++] = contentArray[8 + i]
            }
            updateStateValue(2, currBlock)
            if (p < size) {
                val newBlockIndex = currBlock + 1
                waitForStateValueServerSide(1, currBlock, newBlockIndex)
                currBlock = newBlockIndex
            } else {
                waitForStateValueServerSide(1, currBlock, 0)
                updateStateValue(2, 0)
                break
            }
        }
        return resultBuf
    }

    companion object {
        fun make(): ServerSideJsTransferQueue? {
            if (crossOriginIsolatedAndDefined()) {
                val controlBuffer = SharedArrayBuffer(4 * 3)
                val contentBuffer = SharedArrayBuffer(CONTENT_BUFFER_SIZE + 8)
                return ServerSideJsTransferQueue(controlBuffer, contentBuffer)
            } else {
                return null
            }
        }
    }
}

class ClientSideJsTransferQueue(
    controlBufferData: SharedArrayBuffer,
    contentBufferData: SharedArrayBuffer
) : JsTransferQueue(
    controlBufferData,
    contentBufferData
) {
    var isUpdating = false
    val pendingUpdates = ArrayList<PendingUpdate>()

    /**
     * Client side: Update the current state
     */
    fun updateState(newState: Int) {
        fun performUpdate() {
            @Suppress("UnusedVariable")
            val b = controlArray
            val s = newState
            val oldState = js("Atomics.load(b, 0)") as Int
            if (oldState == s) {
                return
            }
            val prevValue = js("Atomics.compareExchange(b, 0, oldState, s)") as Int
            if (prevValue == oldState) {
                js("Atomics.notify(b, 0)")
                if (pendingUpdates.isEmpty()) {
                    isUpdating = false
                } else {
                    pendingUpdates.removeFirst().updateState(this)
                }
            } else {
                val waitAsyncRes = js("Atomics.waitAsync(b, 0, prevValue)")
                if (!waitAsyncRes.async as Boolean) {
                    error("Result was not async")
                }
                val promise = waitAsyncRes.value as Promise<String>
                promise.then {
                    performUpdate()
                }
            }
        }

        if (isUpdating) {
            val update = PendingUpdate { queue ->
                performUpdate()
            }
            pendingUpdates.add(update)
        } else {
            isUpdating = true
            performUpdate()
        }
    }

    fun waitAndUpdateStateValue(index: Int, expectedState: Int, newState: Int, fn: () -> Unit) {
        val i = index

        @Suppress("UnusedVariable")
        val b = controlArray
        val e = expectedState
        val s = newState

        val oldValue = js("Atomics.compareExchange(b, i, e, s)") as Int
        if (oldValue == expectedState) {
            js("Atomics.notify(b, i)")
            fn()
        } else {
            val waitAsyncRes = js("Atomics.waitAsync(b, i, oldValue)")
            if (!(waitAsyncRes.async as Boolean)) {
                error("Result was not async")
            }
            val promise = waitAsyncRes.value as Promise<String>
            promise.then {
                waitAndUpdateStateValue(i, e, s, fn)
            }
        }
    }

    fun waitForUpdateClientSide(index: Int, expectedState: Int, newState: Int, fn: (Int) -> Unit) {
        val i = index

        @Suppress("UnusedVariable")
        val b = controlArray
        val e = expectedState
        val currState = js("Atomics.load(b, i)")
        when (currState) {
            newState -> {
                fn(currState)
            }
            e -> {
                val waitAsyncRes = js("Atomics.waitAsync(b, i, e)")
                if (!waitAsyncRes.async as Boolean) {
                    error("Result was not async")
                }
                val promise = waitAsyncRes.value as Promise<String>
                promise.then {
                    waitForUpdateClientSide(i, expectedState, newState, fn)
                }
            }
            else -> {
                error("Invalid state at index: ${i}. Expected: ${e}, newState: ${newState}, currState: ${currState}")
            }
        }
    }

    private fun sendArrayInt(buf: ByteArray, currIndex: Int, completionFunction: () -> Unit) {
        // Client: Sender
        // Server: Receiver
        //
        // index 1: updated by the client when data is ready in the output buffer
        // index 2: updated by the server when a block has been consumed
        //
        // Initial state: index 1 and 2 = 0
        // Client: Write size into first 8 bytes of contentArray
        //         ensure index 2 = 0   ←─────────┐
        //         write first block into buffer  │
        //         set index 1 = 1                │
        //         wait for index 2 = 1 ──────────┘ All indexes +1 in next loop
        //         When completed: set index 1 = 0
        // Server: Wait for index 1, transition from 0 to 1 ←───────┐
        //         Read the data                                    │
        //         set index 2 = 1 ─────────────────────────────────┘ Increase indexes
        //         When completed: set index 2 to 0

        ensureStateValue(2, currIndex)

        encodeLong(buf.size.toLong(), contentArray, 0)

        val p = currIndex * CONTENT_BUFFER_SIZE
        repeat(min(buf.size - p, CONTENT_BUFFER_SIZE)) { i ->
            contentArray[8 + i] = buf[p + i]
        }

        val newIndex = currIndex + 1
        updateStateValue(1, newIndex)

        waitForUpdateClientSide(2, currIndex, newIndex) {
            if (newIndex * CONTENT_BUFFER_SIZE < buf.size) {
                sendArrayInt(buf, newIndex, completionFunction)
            } else {
                updateStateValue(1, 0)
                completionFunction()
            }
        }
    }

    fun sendArray(buf: ByteArray, completionFunction: () -> Unit) {
        sendArrayInt(buf, 0, completionFunction)
    }

    companion object {
        fun make(sharedBuffers: dynamic): ClientSideJsTransferQueue {
            require(crossOriginIsolatedAndDefined())
            val controlBuffer = sharedBuffers["controlBufferData"] as SharedArrayBuffer
            val contentBuffer = sharedBuffers["contentBufferData"] as SharedArrayBuffer
            return ClientSideJsTransferQueue(controlBuffer, contentBuffer)
        }
    }

    fun interface PendingUpdate {
        fun updateState(queue: ClientSideJsTransferQueue)
    }
}
