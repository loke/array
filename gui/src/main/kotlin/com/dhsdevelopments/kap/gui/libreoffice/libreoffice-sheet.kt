package com.dhsdevelopments.kap.gui.libreoffice

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.dates.APLTimestamp
import kotlinx.datetime.Instant
import org.odftoolkit.odfdom.doc.OdfDocument
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument
import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileInputStream
import java.nio.ByteBuffer

class ReadLibreofficeXMLFileParseError(message: String, cause: Throwable? = null) : Exception(message, cause)

@Throws(ReadLibreofficeXMLFileParseError::class)
fun readLibreofficeXMLFileFromByteBuffer(buffer: ByteBuffer): APLValue {
    val doc = ByteArrayInputStream(buffer.array()).use { inStream ->
        OdfDocument.loadDocument(inStream)
    }
    return parseLibreofficeDocumentToAPLValue(doc)
}

@Throws(ReadLibreofficeXMLFileParseError::class)
fun readLibreofficeXMLFile(file: File): APLValue {
    val doc = FileInputStream(file).use { inStream ->
        OdfDocument.loadDocument(inStream)
    }
    return parseLibreofficeDocumentToAPLValue(doc)
}

@Throws(ReadLibreofficeXMLFileParseError::class)
fun parseLibreofficeDocumentToAPLValue(doc: OdfDocument): APLValue {
    if (doc !is OdfSpreadsheetDocument) {
        throw ReadLibreofficeXMLFileParseError("Document is not a spreadsheet")
    }

    if (doc.spreadsheetTables.isEmpty()) {
        throw ReadLibreofficeXMLFileParseError("Document does not contain any tables")
    }
    if (doc.spreadsheetTables.size > 1) {
        KapLogger.w { "Content contains multiple tables, returning the first" }
    }
    val table = doc.spreadsheetTables[0]
    val rowCount = table.rowCount
    val columnCount = table.columnCount
    val content = Array<APLValue>(rowCount * columnCount) { i ->
        val cell = table.getCellByPosition(i % columnCount, i / columnCount)
        when (cell.valueType) {
            "float" -> APLDouble(cell.doubleValue)
            "date" -> APLTimestamp(Instant.fromEpochMilliseconds(cell.dateValue.toInstant().toEpochMilli()))
            "string" -> APLString.make(cell.stringValue)
            "currency" -> APLDouble(cell.currencyValue)
            "boolean" -> if (cell.booleanValue) APLLONG_1 else APLLONG_0
            "null" -> APLLONG_0
            else -> APLLONG_0
        }
    }
    return APLArrayImpl(dimensionsOfSize(rowCount, columnCount), content)
}
