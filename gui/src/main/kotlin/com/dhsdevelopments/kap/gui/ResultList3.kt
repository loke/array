package com.dhsdevelopments.kap.gui

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.gui.styledarea.*
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuItem
import javafx.scene.input.MouseEvent
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.text.TextFlow
import org.fxmisc.flowless.VirtualizedScrollPane
import org.fxmisc.richtext.model.SegmentOpsBase
import org.fxmisc.richtext.model.StyledSegment
import org.fxmisc.richtext.model.TextOps
import java.util.*
import java.util.concurrent.LinkedTransferQueue
import java.util.function.BiConsumer
import java.util.function.Function

class REPLSourceLocation(
    private val text: String,
    val parent: ResultList3,
    val tag: InputLogTag
) : SourceLocation {
    override fun sourceText() = text
    override fun open() = makeStringCharacterProvider(text)
}

class InputLogTag

class ResultList3(val client: Client) {
    private val styledOps = CodeSegmentOps()
    private val styledArea: ROStyledArea
    private val scrollArea: VirtualizedScrollPane<ROStyledArea>

    private val history = ArrayList<String>()
    private var historyPos = 0
    private var pendingInput: ROStyledArea.InputText? = null

    private val resultRefHistory = ArrayList<TextStyle.ClearableStyleContent>()

    init {
        val applyParagraphStyle = BiConsumer<TextFlow, ParStyle> { text, parStyle ->
            if (parStyle.type == ParStyle.ParStyleType.INDENT) {
                text.border =
                    Border(BorderStroke(Color.TRANSPARENT, BorderStrokeStyle.NONE, CornerRadii.EMPTY, BorderWidths(5.0, 5.0, 5.0, 30.0)))
//            } else if(parStyle.type == ParStyle.ParStyleType.TIMING) {
//                text.textAlignment = TextAlignment.RIGHT
            }
        }
        val nodeFactory = Function<StyledSegment<EditorContent, TextStyle>, Node> { segment ->
            segment.segment.createNode(client.renderContext, segment.style)
        }

        styledArea = ROStyledArea(client, applyParagraphStyle, styledOps, nodeFactory, client.config)
        styledArea.id = "replPanel"
        if (client.config.settings().displayWelcomeText) {
            val welcomeText =
                """
                |Welcome to Kap.
                |
                |Use the prefix key ${client.config.settings().keyPrefix} to insert special characters.
                |Please use the keyboard help window (select Window, Keyboard) to see the keyboard layout.
                |Use the configuration screen (select File, Settings) to change the prefix key.
                """.trimMargin()
            styledArea.insertInitialText(welcomeText)
        }
        styledArea.setCommandListener(ResultListCommandListener())
        styledArea.setPromptedInputCommandListener(PromptedInputCommandListener())
        initStyledAreaContextMenu()

        val historyListener = ResultHistoryListener()
        styledArea.addHistoryListener(historyListener)

        styledArea.isWrapText = false

        scrollArea = VirtualizedScrollPane(styledArea)

        styledArea.onMouseMoved = EventHandler { event ->
            processMouseMoved(event)
        }

        styledArea.onMouseExited = EventHandler {
            clearHighlightedStyles()
            prevHighlightedStyleName = null
        }
    }

    private var prevHighlightedStyleName: String? = null

    private fun processMouseMoved(event: MouseEvent) {
        styledArea.hit(event.x, event.y).characterIndex.ifPresent { index ->
            val style = styledArea.document.getStyleAtPosition(index)
            if (prevHighlightedStyleName != style.cssClassName) {
                clearHighlightedStyles()

                if (style.cssClassName != null) {
                    val nodes = styledArea.lookupAll(".${style.cssClassName}")
                    nodes.forEach { node ->
                        val styles = node.styleClass
                        if (!styles.contains(HIGHLIGHTED_STYLE_NAME)) {
                            styles.add(HIGHLIGHTED_STYLE_NAME)
                        }
                    }
                }
                prevHighlightedStyleName = style.cssClassName
            }
        }
    }

    private fun clearHighlightedStyles() {
        if (prevHighlightedStyleName != null) {
            styledArea.lookupAll(".${prevHighlightedStyleName}").forEach { node ->
                node.styleClass.remove(HIGHLIGHTED_STYLE_NAME)
            }
        }
    }

    private fun initStyledAreaContextMenu() {
        val m = ContextMenu(MenuItem("Selection").apply { isDisable = true })
        styledArea.contextMenu = m
        styledArea.onContextMenuRequested = EventHandler { event ->
            repeat(m.items.size - 1) {
                m.items.removeLast()
            }
            styledArea.hit(event.x, event.y).characterIndex.ifPresent { index ->
                val style = styledArea.document.getStyleAtPosition(index)
                val rootValue = style.contentRefs?.rootValue
                if (rootValue != null) {
                    m.items.add(MenuItem("Copy as string").apply {
                        onAction = EventHandler { findRootValueFromStyle(style)?.let { value -> copyValueAsString(value) } }
                    })
                    m.items.add(MenuItem("Copy as code").apply {
                        onAction = EventHandler { findRootValueFromStyle(style)?.let { value -> copyValueAsCode(value) } }
                    })
                    m.items.add(MenuItem("Copy as HTML").apply {
                        onAction = EventHandler { findRootValueFromStyle(style)?.let { value -> copyValueAsHtml(value) } }
                    })
                    client.objectViewerRegistry.findEditorProviders(rootValue).forEach { p ->
                        m.items.add(MenuItem(p.menuTitle).apply {
                            onAction = EventHandler { p.perform(client, rootValue, null) }
                        })
                    }
                }
                val innerValuePtr = style.innerValuePtr
                if (innerValuePtr != null) {
                    val contentRefs = style.contentRefs
                    if (contentRefs != null) {
                        m.items.add(MenuItem("Show full value").apply {
                            onAction = EventHandler {
                                val innerValue = contentRefs.values[innerValuePtr]
                                ValuePopup.displayValueInPopup(client, innerValue)
                            }
                        })
                    }
                }
            }
        }
    }

    fun requestFocus() {
        styledArea.requestFocus()
    }

    fun getNode() = scrollArea

    fun addResult(v: EvalExpressionResult) {
        val supp = TextStyle.ClearableStyleContent(v.formattedEvalResult.result)
        @Suppress("KotlinConstantConditions")
        if (MAX_REF_HISTORY_SIZE > 0) {
            resultRefHistory.add(supp)
        }
        while (resultRefHistory.size > MAX_REF_HISTORY_SIZE) {
            val oldEntry = resultRefHistory.removeFirst()
            oldEntry.clear()
        }
        styledArea.appendExpressionResultEnd(v, clearableStyleContent = supp)
    }

    // Need to suppress error warning here because of https://youtrack.jetbrains.com/issue/KTIJ-20744
    @Suppress("USELESS_IS_CHECK")
    fun addExceptionResult(e: Exception) {
        val (message, details) = if (e is APLGenericException) {
            Pair(e.formattedError(), e.extendedDescription)
        } else {
            Pair("Exception from Kap engine: ${e.message}", null)
        }
        styledArea.appendErrorMessage(message, details)
    }

    fun addOutput(text: String) {
        styledArea.appendOutputEnd(text)
    }

    fun addErrorOutput(text: String) {
        styledArea.appendErrorMessage(text)
    }

    private fun addInput(text: String): InputLogTag {
        val tag = InputLogTag()
        styledArea.appendTextEnd(text + "\n", TextStyle(client.config, TextStyle.Type.LOG_INPUT), ParStyle(ParStyle.ParStyleType.INDENT, tag = tag))
        return tag
    }

    fun updateStyle(tag: InputLogTag, startLine: Int, startCol: Int, endLine: Int, endCol: Int, textStyle: TextStyle) {
        styledArea.withUpdateEnabled {
            val startIndex = indexOfTag(tag)
            if (startIndex != null) {
                styledArea.setStyleForRange(startIndex + startLine, startCol, startIndex + endLine, endCol, textStyle)
            }
        }
    }

    private fun indexOfTag(tag: Any): Int? {
        var foundIndex: Int? = null
        for (i in styledArea.paragraphs.size - 1 downTo 0) {
            val paragraph = styledArea.paragraphs[i]
            if (paragraph.paragraphStyle.tag === tag) {
                foundIndex = i
            } else {
                if (foundIndex != null) {
                    break
                }
            }
        }
        return foundIndex
    }

    fun updateInputStatePrompted(prompt: String) {
        styledArea.updateInputStatePrompted(prompt)
    }

    fun updateInputStateRepl() {
        styledArea.updateInputStateRepl()
    }

    fun updateInputStateDisabled() {
        styledArea.updateInputStateDisabled()
    }

    fun showBottomParagraphAtTop() {
        styledArea.showBottomParagraphAtTop()
    }

    fun clearLog() {
        styledArea.clearLog()
    }

    inner class ResultHistoryListener : HistoryListener {
        override fun prevHistory() {
            if (historyPos > 0) {
                if (historyPos == history.size) {
                    pendingInput = styledArea.currentInput()
                }
                historyPos--
                styledArea.replaceInputText(history[historyPos])
                styledArea.showBottomParagraphAtTop()
            }
        }

        override fun nextHistory() {
            when {
                historyPos < history.size - 1 -> {
                    historyPos++
                    styledArea.replaceInputText(history[historyPos])
                }
                historyPos == history.size - 1 -> {
                    historyPos++
                    val p = pendingInput
                    if (p == null) {
                        styledArea.replaceInputText("")
                    } else {
                        styledArea.replaceInputTextAndMoveCursor(p)
                    }
                    pendingInput = null
                    styledArea.showBottomParagraphAtTop()
                }
            }
        }
    }

    companion object {
        private const val MAX_REF_HISTORY_SIZE = 10
        private const val HIGHLIGHTED_STYLE_NAME = "kap-value-inner-highlighted"

        private fun findRootValueFromStyle(style: TextStyle): APLValue? {
            return style.contentRefs?.rootValue
        }
    }

    class CodeSegmentOps : SegmentOpsBase<EditorContent, TextStyle>(EditorContent.makeBlank()), TextOps<EditorContent, TextStyle> {
        override fun length(seg: EditorContent): Int {
            return seg.length()
        }

        override fun joinSeg(currentSeg: EditorContent, nextSeg: EditorContent): Optional<EditorContent> {
            return currentSeg.joinSegment(nextSeg)
        }

        override fun realGetText(seg: EditorContent): String? {
            return seg.realGetText()
        }

        override fun realCharAt(seg: EditorContent, index: Int): Char {
            return seg.realCharAt(index)
        }

        override fun realSubSequence(seg: EditorContent, start: Int, end: Int): EditorContent {
            return seg.realSubsequence(start, end)
        }

        override fun create(text: String): EditorContent {
            return EditorContent.makeString(text)
        }
    }

    inner class ResultListCommandListener : CommandListener {
        override fun valid(text: String): Boolean {
            return text.trim().isNotBlank() && !client.calculationQueue.isActive()
        }

        override fun handle(text: String) {
            if (text.isNotBlank()) {
                if (history.isEmpty() || history.last() != text) {
                    history.add(text)
                }
                historyPos = history.size
                pendingInput = null
                val tag = addInput(text)

                val cmgr = client.engine.commandManager
                val cmd = cmgr.processPrefix(text)
                if (cmd != null) {
                    updateInputStateDisabled()
                    client.calculationQueue.pushJobToQueue { engine ->
                        try {
                            cmgr.processCommandString(cmd)
                        } catch (e: CommandException) {
                            Platform.runLater {
                                addErrorOutput("Error: ${e.message}")
                            }
                        } finally {
                            Platform.runLater {
                                updateInputStateRepl()
                            }
                        }
                    }
                } else {
                    val source = REPLSourceLocation(text, this@ResultList3, tag)
                    client.evalSource(source)
                }
            }
        }
    }

    val promptedInputQueue = LinkedTransferQueue<String>()

    inner class PromptedInputCommandListener : CommandListener {
        override fun valid(text: String) = true

        override fun handle(text: String) {
            updateInputStateDisabled()
            promptedInputQueue.add(text)
        }
    }
}
