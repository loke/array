package com.dhsdevelopments.kap.mpthread

import com.dhsdevelopments.kap.Either
import kotlin.test.*

class ThreadTest {
    @Test
    fun simpleThread() {
        if (!backendSupportsThreading) return

        var result = 0
        val thread = startThread {
            val buf = StringBuilder()
            repeat(10000) { i ->
                buf.append("x")
            }
            result = buf.toString().length
            "abc"
        }
        thread.join()
        assertTrue(thread.isStopped())
        val threadRes = thread.completionData()
        assertTrue(threadRes is Either.Left)
        assertEquals("abc", threadRes.value)
        assertEquals(10000, result)
    }

    // Linux threadlocal doesn't work at the moment
    @Ignore
    @Test
    fun mpthreadLocal() {
        if (!backendSupportsThreading) return

        val threadLocalString = makeMPThreadLocal<String>()
        threadLocalString.value = "a"
        val threads = (0 until 10).map { i ->
            startThread {
                threadLocalString.value = "thread:${i}"
                threadLocalString.value
            }
        }
        threads.forEach { th -> th.join() }
        threads.forEachIndexed { i, th ->
            val completionData = th.completionData()
            if (completionData is Either.Right) {
                completionData.value.printStackTrace()
            }
            assertIs<Either.Left<String>>(completionData)
            assertEquals("thread:${i}", completionData.value)
        }
        assertEquals("a", threadLocalString.value)
    }
}
