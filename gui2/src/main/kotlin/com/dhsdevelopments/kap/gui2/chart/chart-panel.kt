package com.dhsdevelopments.kap.gui2.chart

import org.jfree.chart.ChartPanel
import org.jfree.chart.JFreeChart
import java.awt.BorderLayout
import javax.swing.*

fun displayChartPanel(chart: JFreeChart) {
    val chartPanel = ChartPanel(chart)

    val mainPanel = JPanel().apply {
        layout = BorderLayout()
        add(makeMenu(chartPanel), BorderLayout.NORTH)
        add(chartPanel, BorderLayout.CENTER)
    }

    JFrame("Chart").apply {
        setSize(800, 600)
        contentPane = mainPanel
        setLocationRelativeTo(null)
        defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        isVisible = true
    }
}

private fun makeMenu(chartPanel: ChartPanel): JMenuBar {
    val menu = JMenuBar()
    val fileMenu = JMenu("File").apply {
        add(JMenuItem("Export").apply { addActionListener { exportImage(chartPanel) } })
    }
    menu.add(fileMenu)
    return menu
}

private fun exportImage(chartPanel: ChartPanel) {
    chartPanel.doSaveAs()
}
