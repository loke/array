package com.dhsdevelopments.kap.sql

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.KotlinObjectWrappedValue
import com.dhsdevelopments.kap.builtins.makeAPLNumberAsBoolean
import com.dhsdevelopments.kap.dates.APLTimestamp
import com.dhsdevelopments.mpbignum.BigInt
import com.dhsdevelopments.mpbignum.Rational
import com.dhsdevelopments.mpbignum.make
import kotlinx.datetime.Instant
import java.io.ByteArrayInputStream
import java.math.BigDecimal
import java.math.BigInteger
import java.sql.*

private inline fun <T> withOpenTransaction(conn: Connection, fn: () -> T): T {
    var finished = false
    try {
        val res = fn()
        finished = true
        return res
    } finally {
        if (finished) {
            conn.commit()
        } else {
            conn.rollback()
        }
    }
}

private inline fun <T> withSQLExceptions(pos: Position, fn: () -> T): T {
    return try {
        fn()
    } catch (e: SQLException) {
        throwAPLException(SQLAPLException("Exception from database engine: ${e.message}", pos, e))
    }
}

class SQLAPLException(message: String, pos: Position? = null, cause: Throwable? = null) : APLEvalException(message, pos, cause)

class SQLConnectionValue(conn: Connection, val description: String) : KotlinObjectWrappedValue<Connection>(conn) {
    override fun formatted(style: FormatStyle) = "Connection(${description})"
}

class SQLPreparedStatementValue(statement: PreparedStatement) : KotlinObjectWrappedValue<PreparedStatement>(statement) {
    override fun formatted(style: FormatStyle) = "PreparedStatement($value)"
}

private fun ensureSQLConnectionValue(a: APLValue, pos: Position? = null): SQLConnectionValue {
    if (a !is SQLConnectionValue) {
        throwAPLException(APLIllegalArgumentException("Value is not a valid SQL connection", pos))
    }
    return a
}

private inline fun <T> withSQLConnection(a: APLValue, pos: Position, fn: (Connection) -> T): T {
    return withSQLExceptions(pos) {
        fn(ensureSQLConnectionValue(a, pos).value)
    }
}

private inline fun <T> withPreparedStatement(a: APLValue, pos: Position, fn: (PreparedStatement) -> T): T {
    return withSQLExceptions(pos) {
        fn(ensurePreparedStatementValue(a, pos).value)
    }
}

private fun ensurePreparedStatementValue(a: APLValue, pos: Position? = null): SQLPreparedStatementValue {
    if (a !is SQLPreparedStatementValue) {
        throwAPLException(APLIllegalArgumentException("Value is not a valid prepared statement", pos))
    }
    return a
}

class SQLConnectFunction : APLFunctionDescriptor {
    class SQLConnectFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val a0 = a.listify()
            return withSQLExceptions(pos) {
                val connectionUrl = a0.listElement(0, pos).toStringValue(pos)
                val (username, password) = when (a0.listSize()) {
                    1 -> Pair(null, null)
                    2 -> Pair(a0.listElement(1, pos).toStringValue(pos), null)
                    3 -> Pair(a0.listElement(1, pos).toStringValue(pos), a0.listElement(2, pos).toStringValue(pos))
                    else -> throwAPLException(IllegalArgumentNumException(3, a0.listSize(), pos, minArgs = 1))
                }
                val conn = DriverManager.getConnection(connectionUrl, username, password)
                conn.autoCommit = false
                SQLConnectionValue(conn, "url=${connectionUrl}")
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = SQLConnectFunctionImpl(instantiation)
}

private fun parseEntry(value: Any?, colIndex: Int, pos: Position): APLValue {
    return when (value) {
        null -> APLNilValue
        is Byte -> value.toLong().makeAPLNumber()
        is Short -> value.toLong().makeAPLNumber()
        is Int -> value.toLong().makeAPLNumber()
        is Long -> value.makeAPLNumber()
        is Float -> value.toDouble().makeAPLNumber()
        is Double -> value.makeAPLNumber()
        is Boolean -> value.makeAPLNumberAsBoolean()
        is ByteArray -> APLArrayByte(dimensionsOfSize(value.size), value)
        is Blob -> readBlob(value, colIndex, pos)
        is Clob -> readClob(value, colIndex, pos)
        is BigInteger -> BigInt.of(value).makeAPLNumberWithReduction()
        is BigDecimal -> makeAPLNumberFromBigDecimal(value)
        is Char -> APLChar(value.code)
        is String -> APLString.make(value)
        is Timestamp -> APLTimestamp(Instant.fromEpochMilliseconds(value.time))
        is Time -> APLTimestamp(Instant.fromEpochMilliseconds(value.time))
        is Date -> APLTimestamp(Instant.fromEpochMilliseconds(value.time))
        else -> throwAPLException(SQLAPLException("Cannot convert value ${value} to an APL Value (column ${colIndex + 1} in result)", pos))
    }
}

private fun readBlob(value: Blob, index: Int, pos: Position): APLArrayByte {
    val size = value.length()
    if (size > Integer.MAX_VALUE) {
        throwAPLException(KapOverflowException("Size of value in column ${index + 1} too large", pos))
    }
    val b = value.getBytes(1, size.toInt())
    return APLArrayByte(dimensionsOfSize(size.toInt()), b)
}

private fun readClob(value: Clob, index: Int, pos: Position): APLString {
    val size = value.length()
    if (size > Integer.MAX_VALUE) {
        throwAPLException(KapOverflowException("Size of value in column ${index + 1} too large", pos))
    }
    val s = value.getSubString(1, size.toInt())
    return APLString.make(s)
}

private fun makeAPLNumberFromBigDecimal(value: BigDecimal): APLNumber {
    val scale = value.scale()
    return if (scale <= 0) {
        BigInt.of(value.toBigInteger()).makeAPLNumberWithReduction()
    } else {
        Rational.make(BigInt.of(value.movePointRight(scale).toBigInteger()), BigInt.of(BigInteger("10").pow(scale))).makeAPLNumber()
    }
}

private fun resultSetToValue(result: ResultSet, pos: Position): APLValue {
    val metaData = result.metaData
    val colCount = metaData.columnCount
    val resultData = ArrayList<APLValue>()
    while (result.next()) {
        repeat(colCount) { colIndex ->
            resultData.add(parseEntry(result.getObject(colIndex + 1), colIndex, pos))
        }
    }
    val colNames = ArrayList<AxisLabel?>()
    var hasNames = false
    repeat(colCount) { i ->
        val name = metaData.getColumnLabel(i + 1)
        val res = if (name != null && name.isNotBlank()) {
            hasNames = true
            name
        } else {
            null
        }
        colNames.add(if (res == null) null else AxisLabel(res))
    }
    val queryContentResult = APLArrayList(dimensionsOfSize(resultData.size / colCount, colCount), resultData)
    return if (hasNames) {
        val metadata = object : APLValueMetadata {
            override val labels = DimensionLabels(listOf(null, colNames))
        }
        MetadataOverrideArray(queryContentResult, metadata)
    } else {
        queryContentResult
    }
}

class SQLQueryFunction : APLFunctionDescriptor {
    class SQLQueryFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            withSQLConnection(a, pos) {
                val query = b.toStringValue(pos)
                val conn = ensureSQLConnectionValue(a, pos).value
                return withOpenTransaction(conn) {
                    conn.createStatement().use { statement ->
                        @Suppress("SqlSourceToSinkFlow")
                        statement.executeQuery(query).use { result ->
                            resultSetToValue(result, pos)
                        }
                    }
                }
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = SQLQueryFunctionImpl(instantiation)
}

class SQLUpdateFunction : APLFunctionDescriptor {
    class SQLUpdateFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            withSQLConnection(a, pos) { conn ->
                val query = b.toStringValue(pos)
                return withOpenTransaction(conn) {
                    conn.createStatement().use { statement ->
                        @Suppress("SqlSourceToSinkFlow")
                        val result = statement.executeUpdate(query)
                        result.makeAPLNumber()
                    }
                }
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = SQLUpdateFunctionImpl(instantiation)
}

class SQLPrepareFunction : APLFunctionDescriptor {
    class SQLPrepareFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            return withSQLConnection(a, pos) { conn ->
                @Suppress("SqlSourceToSinkFlow")
                val statement = conn.prepareStatement(b.toStringValue(pos))
                SQLPreparedStatementValue(statement)
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = SQLPrepareFunctionImpl(instantiation)
}

private fun updatePreparedStatementCol(statement: PreparedStatement, index: Int, value: APLValue, pos: Position) {
    when (val v = value.collapse()) {
        is APLLong -> statement.setLong(index, v.value)
        is APLChar -> statement.setString(index, v.asString())
        is APLNilValue -> statement.setNull(index, Types.NULL)
        is APLTimestamp -> statement.setTimestamp(index, Timestamp(v.time.toEpochMilliseconds()))
        else -> {
            if (v.rank == 1) {
                val stringValue = v.toStringValueOrNull()
                if (stringValue != null) {
                    statement.setString(index, stringValue)
                } else {
                    val bytes = v.asByteArray(pos)
                    statement.setBlob(index, ByteArrayInputStream(bytes))
                }
            } else {
                throwAPLException(
                    SQLAPLException(
                        "Value cannot be used in an SQL prepared statement: ${value.formatted(FormatStyle.PLAIN)}",
                        pos))
            }
        }
    }
}

class SQLPreparedUpdateFunction : APLFunctionDescriptor {
    class SQLPreparedUpdateFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            withPreparedStatement(a, pos) { statement ->
                val result = ArrayList<APLValue>()
                withOpenTransaction(statement.connection) {
                    val bDimensions = b.dimensions
                    bDimensions.multipliers()
                    when (bDimensions.size) {
                        1 -> {
                            repeat(bDimensions[0]) { colIndex ->
                                updatePreparedStatementCol(statement, colIndex + 1, b.valueAt(colIndex), pos)
                            }
                            statement.executeUpdate()
                        }
                        2 -> {
                            repeat(bDimensions[0]) { rowIndex ->
                                repeat(bDimensions[1]) { colIndex ->
                                    updatePreparedStatementCol(
                                        statement,
                                        colIndex + 1,
                                        b.valueAt(bDimensions.indexFromPosition(intArrayOf(rowIndex, colIndex), pos = pos)),
                                        pos)
                                }
                                val res = statement.executeUpdate()
                                result.add(res.makeAPLNumber())
                            }
                        }
                        else -> throwAPLException(SQLAPLException("Right value must be rank 1 or 2", pos))
                    }
                }
                return APLArrayList(dimensionsOfSize(result.size), result)
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = SQLPreparedUpdateFunctionImpl(instantiation)
}

class SQLPreparedQueryFunction : APLFunctionDescriptor {
    class SQLPreparedQueryFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            withPreparedStatement(a, pos) { statement ->
                withOpenTransaction(statement.connection) {
                    val bCollapsed = b.collapse()
                    val bDimensions = bCollapsed.dimensions
                    if (bDimensions.size != 1) {
                        throwAPLException(SQLAPLException("Right argument to function must be a rank-1 array", pos))
                    }
                    repeat(bDimensions[0]) { colIndex ->
                        updatePreparedStatementCol(statement, colIndex + 1, bCollapsed.valueAt(colIndex), pos)
                    }
                    statement.executeQuery().use { result ->
                        return resultSetToValue(result, pos)
                    }
                }
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = SQLPreparedQueryFunctionImpl(instantiation)
}

class SQLModule : KapModule {
    override val name get() = "sql"

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("sql")
        engine.registerFunction(ns.internAndExport("connect"), SQLConnectFunction())
        engine.registerFunction(ns.internAndExport("query"), SQLQueryFunction())
        engine.registerFunction(ns.internAndExport("update"), SQLUpdateFunction())
        engine.registerFunction(ns.internAndExport("prepare"), SQLPrepareFunction())
        engine.registerFunction(ns.internAndExport("updatePrepared"), SQLPreparedUpdateFunction())
        engine.registerFunction(ns.internAndExport("queryPrepared"), SQLPreparedQueryFunction())

        engine.registerClosableHandler(object : ClosableHandler<SQLConnectionValue> {
            override fun close(value: SQLConnectionValue) {
                value.value.close()
            }
        })
        engine.registerClosableHandler(object : ClosableHandler<SQLPreparedStatementValue> {
            override fun close(value: SQLPreparedStatementValue) {
                value.value.close()
            }
        })
    }
}
