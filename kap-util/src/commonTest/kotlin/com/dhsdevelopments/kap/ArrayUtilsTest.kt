package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class ArrayUtilsTest {
    @Test
    fun testRestSimple() {
        val a = listOf(1, 2, 3, 4)
        assertEquals(listOf(2, 3, 4), a.rest())
    }

    @Test
    fun testRestSingleElement() {
        val a = listOf(1)
        assertEquals(emptyList(), a.rest())
    }

    @Test
    fun testRestOnEmptyArrayShouldFail() {
        val a = ArrayList<Int>()
        assertFailsWith<IllegalStateException> {
            a.rest()
        }
    }
}
