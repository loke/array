package com.dhsdevelopments.kap.gui.report2

import com.dhsdevelopments.kap.KapLogger
import com.dhsdevelopments.kap.gui.arrayedit.ArrayEditor
import com.dhsdevelopments.kap.unless
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.scene.control.ContextMenu
import javafx.scene.control.Menu
import javafx.scene.control.MenuItem
import javafx.scene.control.SeparatorMenuItem
import javafx.scene.input.KeyCharacterCombination
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import org.controlsfx.control.spreadsheet.GridBase
import org.controlsfx.control.spreadsheet.SpreadsheetCell
import org.controlsfx.control.spreadsheet.SpreadsheetView

class ReportSpreadsheetView : SpreadsheetView() {
    lateinit var reportClient: ReportClient

    init {
        stylesheets.add("/array/gui/report2/report2.css")
        updateGridBase()
    }

    override fun getSpreadsheetViewContextMenu(): ContextMenu {
        val menu = super.getSpreadsheetViewContextMenu()
        menu.items.apply {
            add(MenuItem("Insert variable").apply {
                onAction = EventHandler { insertVariableSelected() }
                accelerator = KeyCodeCombination(KeyCode.E, KeyCharacterCombination.SHORTCUT_DOWN)
            })
            add(SeparatorMenuItem())
            add(MenuItem("Set span").apply {
                onAction = EventHandler { setColAndRowSpanSelected() }
            })
            add(MenuItem("Reset span").apply {
                onAction = EventHandler { resetSpanSelected() }
            })
            add(SeparatorMenuItem())
            add(Menu("Text Style").apply {
                items.add(MenuItem("Bold").apply {
                    onAction = EventHandler { updateTextStyleForCell(StyleableCellType.LabelTextStyle.BOLD) }
                    accelerator = KeyCodeCombination(KeyCode.B, KeyCharacterCombination.SHORTCUT_DOWN)
                })
                items.add(MenuItem("Italics").apply {
                    onAction = EventHandler { updateTextStyleForCell(StyleableCellType.LabelTextStyle.ITALICS) }
                    accelerator = KeyCodeCombination(KeyCode.I, KeyCharacterCombination.SHORTCUT_DOWN)
                })
            })
            add(Menu("Colour").apply {
                items.add(MenuItem("Red", ColourBox(1.0, 0.0, 0.0)))
                items.add(MenuItem("Green", ColourBox(0.0, 1.0, 0.0)))
                items.add(MenuItem("Blue", ColourBox(0.0, 0.0, 1.0)))
                items.add(MenuItem("Yellow", ColourBox(1.0, 1.0, 0.0)))
                items.add(MenuItem("Magenta", ColourBox(1.0, 0.0, 1.0)))
                items.add(MenuItem("Cyan", ColourBox(0.0, 1.0, 1.0)))
            })
        }
        return menu
    }

    private fun updateTextStyleForCell(style: StyleableCellType.LabelTextStyle) {
        selectionModel.selectedCells.forEach { cellPosition ->
            val row = cellPosition.row
            val col = cellPosition.column
            val cell = grid.rows[row][col]
            val cellType = cell.cellType
            if (cellType is StyleableCellType) {
                cellType.invertStyle(cell, style)
            }
        }
    }

    private fun setColAndRowSpanSelected() {
        val selection = ArrayEditor.SelectedArea.computeSelectedArea(selectionModel.selectedCells)
        if (selection != null) {
            setSpanFromSelection(selection)
        }
    }

    fun setSpanFromSelection(selection: ArrayEditor.SelectedArea) {
        if (!verifySpanAllowed(selection)) {
            return
        }

        // Update the span for the cell in the top left of the selection
//        grid.spanColumn(selection.width, selection.row, selection.column)
//        grid.spanRow(selection.height, selection.row, selection.column)
        if (selection.width > 1 || selection.height > 1) {
            val grid = reportClient.table.grid
            var cell: SpreadsheetCell? = null
            repeat(selection.height) { y ->
                val row = grid.rows[selection.row + y]
                repeat(selection.width) { x ->
                    val v2 = if (x == 0 && y == 0) {
                        val v = row[selection.column + x]
                        v.rowSpan = selection.height
                        v.columnSpan = selection.width
                        cell = v
                        v
                    } else {
                        cell!!
                    }
                    row[selection.column + x] = v2
                }
                grid.rows[selection.row + y] = row
            }
        }
    }

    private fun verifySpanAllowed(selection: ArrayEditor.SelectedArea): Boolean {
        if (selection.width == 0 || selection.height == 0) {
            KapLogger.d { "Empty selection" }
            return false
        }

        // Ensure that all cells in selected area are field cells, except for the top-left one which may be
        // any cell of type CellSpannable
        repeat(selection.height) { y ->
            val row = grid.rows[y + selection.row]
            repeat(selection.width) { x ->
                val cell = row[x + selection.column]
                if (!(cell.cellType is LabelCellType || (x == 0 && y == 0 && cell.cellType is CellSpannable))) {
                    KapLogger.w { "Non-label value found at position: ${y + selection.row},${x + selection.column}" }
                    return false
                }
                if (cell.rowSpan != 1 || cell.columnSpan != 1) {
                    KapLogger.w { "Cannot set span for cells which already has a span set: ${y + selection.row},${x + selection.column}" }
                    return false
                }
            }
        }

        return true
    }

    private fun resetSpanSelected() {
        val selection = ArrayEditor.SelectedArea.computeSelectedArea(selectionModel.selectedCells)
        if (selection == null || selection.width == 0 || selection.height == 0) {
            return
        }
        val cell = grid.rows[selection.row][selection.column]
        val oldColSpan = cell.columnSpan
        val oldRowSpan = cell.rowSpan
        grid.spanColumn(1, selection.row, selection.column)
        grid.spanRow(1, selection.row, selection.column)
        repeat(oldRowSpan) { y ->
            val rowIndex = y + selection.row
            val row = grid.rows[rowIndex]
            repeat(oldColSpan) { x ->
                val colIndex = x + selection.column
                unless(x == 0 && y == 0) {
                    row[colIndex] = LabelCellType.createCell(LabelCellContent(""), rowIndex, colIndex)
                }
                grid.rows[rowIndex] = row
            }
        }
    }

    private fun insertVariableSelected() {
        InsertVariable.open(reportClient)
    }

    private fun updateGridBase() {
        val numRows = 100
        val numCols = 40
        val gridBase = GridBase(numRows, numCols)
        val rows = FXCollections.observableArrayList<ObservableList<SpreadsheetCell>>()
        repeat(numRows) { rowIndex ->
            val row = FXCollections.observableArrayList<SpreadsheetCell>()
            repeat(numCols) { colIndex ->
                row.add(LabelCellType.createCell(LabelCellContent(""), rowIndex, colIndex))
            }
            rows.add(row)
        }
        gridBase.setRows(rows)
        grid = gridBase
    }

    fun initClient(clientRef: ReportClient) {
        reportClient = clientRef
    }
}
