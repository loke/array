package com.dhsdevelopments.kap.gui.chart

import com.dhsdevelopments.kap.chart.Plot2DParams
import javafx.collections.ObservableList
import javafx.scene.Scene
import javafx.scene.chart.LineChart
import javafx.scene.chart.NumberAxis
import javafx.scene.chart.XYChart
import javafx.scene.layout.BorderPane
import javafx.stage.Stage

class PlotPanel(var params: Plot2DParams, dataset: ObservableList<XYChart.Series<Number, Number>>) : BorderPane() {
    private val chart: LineChart<Number, Number>

    init {
        val xAxis = NumberAxis()
        xAxis.isForceZeroInRange = false
        chart = LineChart(xAxis, NumberAxis())
        chart.createSymbols = false
        center = chart
        chart.data = dataset
    }

    companion object {
        fun open(params: Plot2DParams, dataset: ObservableList<XYChart.Series<Number, Number>>) {
            val plotPanel = PlotPanel(params, dataset)
            val stage = Stage()
            val scene = Scene(plotPanel)
            stage.scene = scene
            stage.show()
        }
    }
}
