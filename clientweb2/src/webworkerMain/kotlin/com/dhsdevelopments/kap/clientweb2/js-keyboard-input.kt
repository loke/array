package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.*

class JsKeyboardInput(val engine: Engine) : KeyboardInput {
    override fun readString(prompt: String): String? {
        val jsTransferQueue = engine.jsNativeData().jsTransferQueue
        require(jsTransferQueue != null) { "Attempt to read string when jsTransferQueue is null" }

        val msg = jsObject2(
            "messageType" to READ_LINE_TYPE,
            "prompt" to prompt)
        engine.jsNativeData().sendMessageFn(msg)

        val newState = jsTransferQueue.waitForStateOrBreak(JsTransferQueue.STATE_READ_LINE_RESULT)
        if (newState == JsTransferQueue.STATE_BREAK) {
            engine.checkInterrupted()
        }
        require(newState == JsTransferQueue.STATE_READ_LINE_RESULT) { "Illegal state value. Got: ${newState}" }
        val dec = jsTransferQueue.readArray()
        return dec.decodeToString()
    }
}
