package com.dhsdevelopments.kap.gui2

import com.dhsdevelopments.kap.KapLogger
import com.dhsdevelopments.kap.gui2.report.ReportFrame
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.Font
import java.awt.event.ActionEvent
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.*

class Main {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SwingUtilities.invokeLater {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())
                val client = Gui2Client()
                client.openReplFrame()
            }
        }
    }
}

class Gui2Client {
    val computeQueue = ComputeQueue()
    private val font: Font
    private val frame: JFrame

    init {
        font = Font.createFont(Font.TRUETYPE_FONT, this::class.java.getResourceAsStream("fonts/iosevka-fixed-regular.ttf")).deriveFont(18.0f)

        frame = JFrame("Test frame")
        frame.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        frame.addWindowListener(object : WindowAdapter() {
            override fun windowClosed(e: WindowEvent) {
                KapLogger.d { "window closed" }
                computeQueue.stop()
            }
        })
        val scrollPane = JScrollPane(ReplPanel(computeQueue, font)).apply {
            preferredSize = Dimension(800, 800)
        }
        frame.contentPane.layout = BorderLayout()
        frame.contentPane.add(scrollPane, BorderLayout.CENTER)
        frame.contentPane.add(createMenu(), BorderLayout.NORTH)
        frame.pack()
    }

    fun openReplFrame() {
        frame.isVisible = true
    }

    private fun createMenu(): JMenuBar {
        val menuBar = JMenuBar()

        val fileMenu = JMenu("File").apply {
            add(QuitAction())
        }
        menuBar.add(fileMenu)

        val windowMenu = JMenu("Window").apply {
            add(OpenReportToolAction())
        }
        menuBar.add(windowMenu)

        return menuBar
    }

    private inner class QuitAction : AbstractAction("Quit") {
        override fun actionPerformed(e: ActionEvent?) {
            frame.dispose()
        }
    }

    private inner class OpenReportToolAction : AbstractAction("Report editor") {
        override fun actionPerformed(e: ActionEvent?) {
            val frame = ReportFrame(this@Gui2Client)
            frame.defaultCloseOperation = WindowConstants.DISPOSE_ON_CLOSE
            frame.pack()
            frame.isVisible = true
        }
    }
}
