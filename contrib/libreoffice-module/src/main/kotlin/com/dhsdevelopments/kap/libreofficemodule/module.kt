package com.dhsdevelopments.kap.libreofficemodule

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.KotlinObjectWrappedValue
import com.dhsdevelopments.kap.builtins.ResizedArrayImpls
import com.sun.star.container.XIndexAccess
import com.sun.star.frame.XComponentLoader
import com.sun.star.sheet.*
import com.sun.star.table.CellContentType
import com.sun.star.table.XCell
import com.sun.star.text.XText
import com.sun.star.uno.UnoRuntime

// To be able to load libreoffice, add this to the runtime configuration:
// -classpath $Classpath$:/usr/lib64/libreoffice/program/classes/juh.jar:/usr/lib64/libreoffice/program/classes/ridl.jar:/usr/lib64/libreoffice/program/classes/libreoffice.jar:/usr/lib64/libreoffice/program/classes/jurt.jar:/usr/lib64/libreoffice/program/classes/java_uno.jar
class LibreofficeModule : KapModule {
    override val name get() = "libreoffice"

    private var componentLoader: XComponentLoader? = null

    fun findComponentLoader(): XComponentLoader {
        initConnection()
        return componentLoader ?: error("Component loader is null")
    }

    override fun init(engine: Engine) {
        try {
            Class.forName("com.sun.star.comp.helper.Bootstrap")
        } catch (_: ClassNotFoundException) {
            throw ModuleInitSkipped("Libreoffice jar files not on classpath")
        }

        val ns = engine.makeNamespace("libreoffice")

        engine.classManager.registerSupplementarySystemClass(ns.internSymbol("doc"), LibreofficeDocKapClass)

        engine.registerFunction(ns.internAndExport("link"), LinkSpreadsheetFunction(this))
        engine.registerFunction(ns.internAndExport("sheetContent"), ContentFunction())

    }

    private fun initConnection() {
        val context = com.sun.star.comp.helper.Bootstrap.bootstrap()
        println("Got context: ${context}")
        val s = context.serviceManager.availableServiceNames.joinToString(", ")
        println("services: ${s}")
        val desktop = context.serviceManager.createInstanceWithContext("com.sun.star.frame.Desktop", context)
        val componentLoader = UnoRuntime.queryInterface(XComponentLoader::class.java, desktop)
        this.componentLoader = componentLoader
    }
}

object LibreofficeDocKapClass : ModuleClass() {
    override val name get() = "libreoffice-doc"
}


class LibreofficeDocValue(value: XSpreadsheetDocument) : KotlinObjectWrappedValue<XSpreadsheetDocument>(value) {
    override val kapClass get() = LibreofficeDocKapClass

    override fun formatted(style: FormatStyle) = "spreadsheet-document"

    companion object {
        fun ensureValue(v: APLValue, pos: Position? = null): XSpreadsheetDocument {
            val v0 = v.unwrapDeferredValue()
            if (v0 !is LibreofficeDocValue) {
                throwAPLException(APLIllegalArgumentException("Argument is not a libreoffice document", pos))
            }
            return v0.value
        }
    }
}

class LinkSpreadsheetFunction(val module: LibreofficeModule) : APLFunctionDescriptor {
    inner class LinkSpreadsheetFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val loader = module.findComponentLoader()
            println("Got loader: ${loader}")

            val component = loader.loadComponentFromURL("private:factory/scalc", "_blank", 0, emptyArray())
            val spreadsheetDoc = UnoRuntime.queryInterface(XSpreadsheetDocument::class.java, component)

            return LibreofficeDocValue(spreadsheetDoc)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = LinkSpreadsheetFunctionImpl(instantiation)
}

class ContentFunction : APLFunctionDescriptor {
    class ContentFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val sheetIndex = b.ensureNumber(pos).asInt(pos)
            val doc = LibreofficeDocValue.ensureValue(a, pos)
            val sheets = doc.sheets
            val indexAccess = UnoRuntime.queryInterface(XIndexAccess::class.java, sheets)
            if (sheetIndex < 0 || sheetIndex >= indexAccess.count) {
                throwAPLException(
                    APLIllegalArgumentException(
                        "Sheet index must be greater or equal to 0 and less than ${indexAccess.count}, got: ${sheetIndex}",
                        pos))
            }
            val sheet = UnoRuntime.queryInterface(XSpreadsheet::class.java, indexAccess.getByIndex(sheetIndex))

            // (libreoffice:link 0) libreoffice:sheetContent 0
            //
            // s ← libreoffice:link 0
            // s libreoffice:sheetContent 0

            val cellRangesQuery = UnoRuntime.queryInterface(XCellRangesQuery::class.java, sheet)
            val cells = cellRangesQuery.queryContentCells((CellFlags.VALUE or CellFlags.DATETIME or CellFlags.STRING or CellFlags.FORMULA).toShort())
            val r = cells.rangeAddresses
            if (r.size == 0) {
                return ResizedArrayImpls.makeResizedArray(dimensionsOfSize(0, 0), APLNullValue)
            }

            val w = r.maxOf { c -> c.EndColumn } + 1
            val h = r.maxOf { c -> c.EndRow } + 1

            val content = arrayOfNulls<APLValue>(w * h)
            val e = cells.cells.createEnumeration()
            while (e.hasMoreElements()) {
                val cell = UnoRuntime.queryInterface(XCell::class.java, e.nextElement())
                val v = when (cell.type) {
                    CellContentType.VALUE -> {
                        APLDouble(cell.value)
                    }
                    CellContentType.TEXT -> {
                        val text = UnoRuntime.queryInterface(XText::class.java, cell)
                        APLString.make(text.string)
                    }
                    CellContentType.FORMULA -> {
                        APLDouble(cell.value)
                    }
                    else -> null
                }
                if (v != null) {
                    val addr = UnoRuntime.queryInterface(XCellAddressable::class.java, cell).cellAddress
                    content[addr.Row * w + addr.Column] = v
                }
            }
            return APLArrayWithDefault(dimensionsOfSize(h, w), content)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ContentFunctionImpl(instantiation)
}

private class APLArrayWithDefault(override val dimensions: Dimensions, val content: Array<APLValue?>) : APLArray() {
    override fun valueAt(p: Int) = content[p] ?: APLNullValue
}
