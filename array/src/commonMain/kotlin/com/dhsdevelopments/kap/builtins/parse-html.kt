package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.htmlconverter.htmlTableToArray
import com.fleeksoft.ksoup.Ksoup

class HtmlParserException(pos: Position?) : APLEvalException("No table found in HTML content", pos)

class FromHtmlTableFunction : APLFunctionDescriptor {
    class FromHtmlTableFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            return parseTableInString(a, 0)
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val n = a.ensureNumber(pos).asInt(pos)
            return parseTableInString(b, n)
        }

        private fun parseTableInString(a: APLValue, tableIndex: Int): APLValue {
            val s = a.toStringValue(pos)
            val doc = Ksoup.parse(s)
            val res = htmlTableToArray(doc, tableIndex)
            if (res == null) {
                throwAPLException(HtmlParserException(pos))
            }
            return res
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FromHtmlTableFunctionImpl(instantiation)
}
