package com.dhsdevelopments.kap.gui2.report

import com.dhsdevelopments.kap.gui2.Gui2Client
import java.awt.event.ActionEvent
import javax.swing.AbstractAction
import javax.swing.JMenuItem
import javax.swing.JPopupMenu
import javax.swing.JTable
import javax.swing.event.ChangeEvent
import javax.swing.event.ListSelectionEvent
import javax.swing.event.TableColumnModelEvent
import javax.swing.event.TableColumnModelListener

class ReportTable(val owner: ReportFrame, val client: Gui2Client) : JTable(ReportTableModel(), ReportColumnModel()) {
    val reportTableModel get() = model as ReportTableModel

    init {
        columnModel.addColumnModelListener(ReportColumnModelListener())
        autoCreateColumnsFromModel = true
        createDefaultColumnsFromModel()
        columnSelectionAllowed = true
        setDefaultRenderer(ReportCell::class.java, ReportCellRenderer())
        autoResizeMode = AUTO_RESIZE_OFF

        componentPopupMenu = JPopupMenu().apply {
            add(JMenuItem(AddDataAction()))
        }
    }

    inner class AddDataAction : AbstractAction("Add variable") {
        override fun actionPerformed(e: ActionEvent?) {
            val rowIndex = selectedRow.let { row -> if (row == -1) 0 else row }
            val colIndex = selectedColumn.let { col -> if (col == -1) 0 else col }
            val dialog = InsertVariableDialog(owner, rowIndex, colIndex)
            dialog.isVisible = true
        }
    }

    class ReportColumnModelListener : TableColumnModelListener {
        override fun columnAdded(e: TableColumnModelEvent?) {}
        override fun columnRemoved(e: TableColumnModelEvent?) {}
        override fun columnMoved(e: TableColumnModelEvent?) {}
        override fun columnMarginChanged(e: ChangeEvent?) {}
        override fun columnSelectionChanged(e: ListSelectionEvent?) {}
    }
}
