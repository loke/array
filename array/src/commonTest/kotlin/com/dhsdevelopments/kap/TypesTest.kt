package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals

class TypesTest : APLTest() {
    @Test
    fun testInteger() {
        testResultType("typeof 10", SystemClass.INTEGER)
        testResultType("typeof ¯10", SystemClass.INTEGER)
        testResultType("typeof 100", SystemClass.INTEGER)
        testResultType("typeof 0", SystemClass.INTEGER)
    }

    @Test
    fun testDouble() {
        testResultType("typeof 1.2", SystemClass.FLOAT)
        testResultType("typeof 10.", SystemClass.FLOAT)
        testResultType("typeof 1.0", SystemClass.FLOAT)
        testResultType("typeof 0.0", SystemClass.FLOAT)
        testResultType("typeof 100.0", SystemClass.FLOAT)
        testResultType("typeof ¯1.0", SystemClass.FLOAT)
        testResultType("typeof ¯1.2", SystemClass.FLOAT)
        testResultType("typeof ¯1.", SystemClass.FLOAT)
        testResultType("typeof 1+1.2", SystemClass.FLOAT)
    }

    @Test
    fun testComplex() {
        testResultType("typeof 1J2", SystemClass.COMPLEX)
        testResultType("typeof 1.2J2.4", SystemClass.COMPLEX)
        testResultType("typeof 100+1J2", SystemClass.COMPLEX)
    }

    @Test
    fun testBigint() {
        testResultType("typeof 2⋆500", SystemClass.INTEGER)
    }

    @Test
    fun testRational() {
        testResultType("typeof 1÷5", SystemClass.RATIONAL)
    }

    @Test
    fun testChar() {
        testResultType("typeof 0 ⌷ \"foo\"", SystemClass.CHAR)
        testResultType("typeof 0 0 ⌷ 2 2 ⍴ \"foox\"", SystemClass.CHAR)
    }

    @Test
    fun testArray() {
        testResultType("typeof 0⍴1", SystemClass.ARRAY)
        testResultType("typeof 1 2 3 4 5 6", SystemClass.ARRAY)
        testResultType("typeof ⍳100", SystemClass.ARRAY)
        testResultType("typeof (⍳100) × ⍳100", SystemClass.ARRAY)
        testResultType("typeof ⍬", SystemClass.ARRAY)
    }

    @Test
    fun testSymbol() {
        testResultType("typeof 'foo", SystemClass.SYMBOL)
    }

    @Test
    fun testLambdaFunction() {
        testResultType("typeof λ { ⍺+⍵+1 }", SystemClass.LAMBDA_FN)
    }

    @Test
    fun testList() {
        testResultType("typeof (1;2;3)", SystemClass.LIST)
    }

    @Test
    fun testMaps() {
        testResultType("typeof map \"a\" 2", SystemClass.MAP)
    }

    @Test
    fun specialisedTypeFromAddLong() {
        parseAPLExpression("1 2 + 3 4", collapse = false).let { result ->
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun specialisedTypeFromAddSingleValueLeftLong() {
        parseAPLExpression("1 + 3 4", collapse = false).let { result ->
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun specialisedTypeFromSingleValueRightAddLong() {
        parseAPLExpression("1 2 + 3", collapse = false).let { result ->
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun specialisedTypeFromAddDouble() {
        parseAPLExpression("1.1 2.0 + 3.9 4.8", collapse = false).let { result ->
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun specialisedTypeFromSingleValueLeftAddDouble() {
        parseAPLExpression("1.1 + 3.9 4.8", collapse = false).let { result ->
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun specialisedTypeFromSingleValueRightAddDouble() {
        parseAPLExpression("1.1 2.0 + 3.9", collapse = false).let { result ->
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun specialisedTypeAddLongAndDoubleArrays() {
        parseAPLExpression("1.1 2.0 + 10 11", collapse = false).let { result ->
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun specialisedTypeAddDoubleAndLongArrays() {
        parseAPLExpression("1 2 + 10.0 11.3", collapse = false).let { result ->
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun specialisedTypeAddSingleValueDoubleLongArray() {
        parseAPLExpression("1.1 + 10 11", collapse = false).let { result ->
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun specialisedTypeAddLongArraysSingleValueDouble() {
        parseAPLExpression("10 11 + 1.1", collapse = false).let { result ->
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun specialisedTypeAddSingleValueLongDoubleArray() {
        parseAPLExpression("1 + 10.0 11.3", collapse = false).let { result ->
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun specialisedTypeAddDoubleArraySingleValueLong() {
        parseAPLExpression("1.1 2.0 + 1", collapse = false).let { result ->
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    private fun testResultType(expression: String, expectedResultClass: KapClass) {
        val engine = makeEngine()
        val result = engine.parseAndEval(StringSourceLocation(expression))
        val expectedSym = engine.classManager.nameForClass(expectedResultClass)
        assertSymbolNameCoreNamespace(engine, expectedSym.symbolName, result)
    }
}
