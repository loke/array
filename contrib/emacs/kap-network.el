;;; -*- lexical-binding: t -*-

(require 'cl-lib)

(defvar kap--reading-first-line t
  "Set to t until the first line has bee read from the repl")

(defvar kap--connection nil
  "Process object for the current connection")

(defvar kap--current-incoming nil
  "Current input as a string.")

(defvar kap--results nil
  "List of results from the remote repl server")

(defun kap--process-incoming-data (s)
  (setq kap--results (nconc kap--results (list s))))

(defun kap--filter-network (proc output)
  (with-current-buffer (kap--get-interactive-session)
    (setq kap--current-incoming (concat kap--current-incoming output))
    (cl-loop with start = 0
             for pos = (cl-position ?\n kap--current-incoming :start start)
             while pos
             do (let ((s (cl-subseq kap--current-incoming start pos)))
                  (setq start (1+ pos))
                  (kap--process-incoming-data s))
             finally (when (cl-plusp start)
                       (setq kap--current-incoming (cl-subseq kap--current-incoming start))))))

(defun kap--connect-to-remote (host port)
  (open-network-stream "*kap-connection*" nil host port
                       :type 'plain
                       :return-list nil
                       :end-of-command "\n"))

(defun kap--connect (host port)
  (with-current-buffer (kap--get-interactive-session)
    (when (process-live-p kap--connection)
      (error "Connection is already established"))
    (condition-case err
        (let ((proc (kap--connect-to-remote host port)))
          (set-process-coding-system proc 'utf-8 'utf-8)
          (setq-local kap--connection proc)
          (setq-local kap--current-incoming "")
          (setq-local kap--results nil)
          (set-process-filter proc 'kap--filter-network))
      ('file-error (error "err:%S type:%S" err (type-of err))))))

(defun kap--parse-initial-line (line)
  (if (string-match "TcpServer started: host=\\([^ ]+\\) port=\\([0-9]+\\)" line)
      (let ((host (match-string 1 line))
            (port (match-string 2 line)))
        (kap--connect host (string-to-number port))
        t)
    nil))

(defun kap--check-initial-line (line)
  (if kap--reading-first-line
      (progn
        (setq kap--reading-first-line nil)
        (kap--parse-initial-line line))
    nil))

(defun kap--preoutput-filter (input)
  (let ((result "")
        (first t))
    (cl-labels ((append-output (s)
                  (if first
                      (setq first nil)
                    (setq result (concat result "\n")))
                  (setq result (concat result s)))

                (process-line (line)
                  (unless (kap--check-initial-line line)
                    (append-output line))))

      (mapc #'process-line (split-string input "\n"))
      result)))

(defun kap--send-network-command (cmd args)
  (with-current-buffer (kap--get-interactive-session)
    (let ((s (json-serialize `((cmd . ,cmd) ,@(if args `((args . ,args)) nil)))))
      (process-send-string kap--connection (concat s "\n")))))

(defun kap--read-network-reply-block ()
  (with-current-buffer (kap--get-interactive-session)
    (cl-loop while (and (null kap--results)
                        (process-live-p kap--connection))
             do (accept-process-output kap--connection 3))
    (unless kap--results
      (signal 'kap-network-proto-error 'disconnected))
    (let ((value (pop kap--results)))
      value)))

(defun kap--send-network-command-and-read (cmd args)
  (kap--send-network-command cmd args)
  (let ((s (kap--read-network-reply-block)))
    (json-parse-string s)))

(provide 'kap-network)
