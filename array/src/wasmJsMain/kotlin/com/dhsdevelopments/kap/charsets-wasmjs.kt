package com.dhsdevelopments.kap

@JsModule("xregexp")
external class XRegExp(regexp: String) {
    fun test(input: String): Boolean
}

val letterRegexp = XRegExp("^\\p{L}$")
val digitRegexp = XRegExp("^\\p{N}$")
val whitespaceRegexp = XRegExp("^[ \\t]$") // We really want "^\\p{Zs}$" here, but xregexp appears to have a bug so it doesn't match tabs

actual fun isLetter(codepoint: Int): Boolean {
    return letterRegexp.test(charToString(codepoint)) as Boolean
}

actual fun isDigit(codepoint: Int): Boolean {
    return digitRegexp.test(codepoint.toChar().toString()) as Boolean
}

actual fun isWhitespace(codepoint: Int): Boolean {
    return whitespaceRegexp.test(codepoint.toChar().toString()) as Boolean
}

actual fun charToString(codepoint: Int): String {
    return if (codepoint < 0x10000) {
        codepoint.toChar().toString()
    } else {
        val off = 0xD800 - (0x10000 shr 10)
        val high = off + (codepoint shr 10)
        val low = 0xDC00 + (codepoint and 0x3FF)
        "${high.toChar()}${low.toChar()}"
    }
}

actual fun nameToCodepoint(name: String): Int? {
    return null
}

actual fun codepointToName(codepoint: Int): String? {
    return null
}

actual val backendSupportsUnicodeNames = false

actual fun StringBuilder.addCodepoint(codepoint: Int): StringBuilder {
    this.append(charToString(codepoint))
    return this
}

fun makeCharFromSurrogatePair(high: Char, low: Char): Int {
    val highInt = high.code
    val lowInt = low.code
    require(highInt in 0xD800..0xDBFF) { "high character is outside valid range: 0x${highInt.toString(16)}" }
    require(lowInt in 0xDC00..0xDFFF) { "high character is outside valid range: 0x${lowInt.toString(16)}" }
    val off = 0x10000 - (0xD800 shl 10) - 0xDC00
    return (highInt shl 10) + lowInt + off
}

actual fun String.asCodepointList(): List<Int> {
    val result = ArrayList<Int>()
    var i = 0
    while (i < this.length) {
        val ch = this[i++]
        val v = when {
            ch.isHighSurrogate() -> {
                val low = this[i++]
                if (low.isLowSurrogate()) {
                    makeCharFromSurrogatePair(ch, low)
                } else {
                    throw IllegalStateException("Expected low surrogate, got: ${low.code}")
                }
            }
            ch.isLowSurrogate() -> throw IllegalStateException("Standalone low surrogate found: ${ch.code}")
            else -> ch.code
        }
        result.add(v)
    }
    return result
}

//@JsModule("grapheme-breaker-mjs")
//@JsNonModule
//external object GraphemeBreaker {
//    fun `break`(s: String): Array<String>
//}

@JsModule("grapheme-splitter")
external class GraphemeSplitter() {
    fun splitGraphemes(input: String): JsAny
}

val graphemeSplitter = GraphemeSplitter()

actual fun String.asGraphemeList(): List<String> {
    val result = graphemeSplitter.splitGraphemes(this)
    val a = result.unsafeCast<JsArray<JsString>>()
    val res = ArrayList<String>()
    repeat(a.length) { i ->
        res.add(a[i].toString())
    }
    return res
}
