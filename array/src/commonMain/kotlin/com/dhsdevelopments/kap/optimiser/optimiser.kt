package com.dhsdevelopments.kap.optimiser

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.OuterInnerJoinOp

interface Optimiser {
    fun optimiseParsedCode(parser: APLParser, instr: Instruction): Instruction

    companion object {
        fun makeConstantInstructionFromValue(value: APLValue, pos: Position): LiteralAPLValueInstruction {
            return when (value) {
                is APLLong -> LiteralInteger(value.value, pos)
                is APLDouble -> LiteralDouble(value.value, pos)
                is APLRational -> LiteralRational(value.value, pos)
                is APLBigInt -> LiteralBigInt(value.value, pos)
                is APLComplex -> LiteralComplex(value.value, pos)
                is APLChar -> LiteralCharacter(value.value, pos)
                else -> ConstantValueInstruction(value, pos)
            }
        }
    }
}

object ZeroOptimiser : Optimiser {
    override fun optimiseParsedCode(parser: APLParser, instr: Instruction): Instruction {
        return instr
    }
}

object StandardOptimiser : Optimiser {
    private fun findChildExpressions(instr: Instruction): Instruction {
        val childrenCopy = instr.children().map { childInstr ->
            findChildExpressions(childInstr)
        }
        require(childrenCopy.size == instr.children().size) { "Updated child list size is not the same as original list" }
        val newInstr = instr.copy(childrenCopy, instr.pos)
        val convertedInstr = maybeConvertLiteral(newInstr)
        return findOptimisedInstruction(convertedInstr) ?: convertedInstr
    }

    private fun findOptimisedInstruction(instr: Instruction): Instruction? {
        instructionOptimisers.forEach { opt ->
            val res = opt.attemptOptimise(instr)
            if (res != null) {
                return res
            }
        }
        return null
    }

    override fun optimiseParsedCode(parser: APLParser, instr: Instruction): Instruction {
        return findChildExpressions(instr)
    }

    private fun maybeConvertLiteral(instr: Instruction): Instruction {
        return when (instr) {
            is Literal1DArray -> maybeConvertLiteral1DArray(instr)
            else -> instr
        }
    }

    private fun maybeConvertLiteral1DArray(array: Literal1DArray): Instruction {
        val content = array.values
        return if (content.all { v -> v is LiteralAPLValueInstruction }) {
            val resultArray = APLArrayImpl.make(dimensionsOfSize(content.size)) { i ->
                val instr = content[i] as LiteralAPLValueInstruction
                instr.valueInt
            }
            ConstantValueInstruction(resultArray, array.pos)
        } else {
            array
        }
    }

    val instructionOptimisers = listOf(
        SingleElementConstantListOptimiser,
        DivideToFloorInstructionOptimiser,
        DivideToCeilInstructionOptimiser,
        IotaAddOptimiser,
        IndexOfMaxInstructionOptimiser,
        IndexOfMinInstructionOptimiser)
}

interface InstructionOptimiser {
    fun attemptOptimise(instr: Instruction): Instruction?
}

/**
 * Abstract class which provides the framework for optimising pairs of calls to
 * scalar functions in the form `A x B y`. This class implements support for
 * common variations such as calls as part of a tacit expression.
 */
abstract class Scalar2ArgInstructionChainOptimiser : InstructionOptimiser {
    override fun attemptOptimise(instr: Instruction): Instruction? {
        attemptRegularChainedFnCalls(instr)?.let { result -> return result }
        attemptCallChain(instr)?.let { result -> return result }
        attemptOuterProduct(instr)?.let { result -> return result }
        return null
    }

    private fun attemptRegularChainedFnCalls(instr: Instruction): Instruction? {
        if (instr !is FunctionCall1Arg || instr.rightArgs !is FunctionCall2Arg) {
            return null
        }
        val fn0 = instr.fn
        val secondInstr = instr.rightArgs
        val fn1 = secondInstr.fn
        val mergedFn = findMergedFunctions(fn0, fn1) ?: return null
        return FunctionCall2Arg(mergedFn, secondInstr.leftArgs, secondInstr.rightArgs, fn0.pos)
    }

    private fun attemptCallChain(instr: Instruction): Instruction? {
        if (instr !is FunctionCall2Arg || instr.fn !is FunctionCallChain.Chain2) {
            return null
        }
        val fn0 = instr.fn.fn0
        val fn1 = instr.fn.fn1
        val mergedFn = findMergedFunctions(fn0, fn1) ?: return null
        return FunctionCall2Arg(mergedFn, instr.leftArgs, instr.rightArgs, fn0.pos)
    }

    private fun attemptOuterProduct(instr: Instruction): Instruction? {
        if (instr !is FunctionCall1Arg || instr.rightArgs !is FunctionCall2Arg) {
            return null
        }
        val rightFunction = instr.rightArgs.fn
        if (rightFunction !is OuterInnerJoinOp.OuterJoinFunctionDescriptor.OuterJoinFunctionImpl) {
            return null
        }
        val fn0 = instr.fn
        val fn1 = rightFunction.fn
        val mergedFn = findMergedFunctions(fn0, fn1) ?: return null
        val rightArgs = instr.rightArgs
        val outerJoinFn = OuterInnerJoinOp.OuterJoinFunctionDescriptor.OuterJoinFunctionImpl(rightFunction.instantiation, mergedFn)
        return FunctionCall2Arg(outerJoinFn, rightArgs.leftArgs, rightArgs.rightArgs, rightFunction.pos)
    }

    abstract fun findMergedFunctions(fn0: APLFunction, fn1: APLFunction): APLFunction?
}

/**
 * Abstract class which implements support for optimising non-scalar calls to two monadic
 * functions of the form: `A B x`.
 */
abstract class NonScalar1ArgInstructionChainOptimiser : InstructionOptimiser {
    override fun attemptOptimise(instr: Instruction): Instruction? {
        attemptRegularChainedFnCalls(instr)?.let { result -> return result }
        attemptTwoFunctionChain(instr)?.let { result -> return result }
        return null
    }

    private fun attemptRegularChainedFnCalls(instr: Instruction): Instruction? {
        if (instr !is FunctionCall1Arg || instr.rightArgs !is FunctionCall1Arg) {
            return null
        }
        val secondInstr = instr.rightArgs
        val mergedFn = findMergedFunctions(instr.fn, secondInstr.fn) ?: return null
        return FunctionCall1Arg(mergedFn, secondInstr.rightArgs, instr.fn.pos)
    }

    private fun attemptTwoFunctionChain(instr: Instruction): Instruction? {
        if (instr !is FunctionCall1Arg) return null
        val mergedFn = attemptChain(instr.fn, instr.rightArgs) ?: return null
        return FunctionCall1Arg(mergedFn, instr.rightArgs, instr.fn.pos)
    }

    private fun attemptChain(fn: APLFunction, rightArgs: Instruction): APLFunction? {
        return if (fn is FunctionCallChain.Chain2) {
            val recurseResult = attemptChain(fn.fn1, rightArgs)
            val fn1 = recurseResult ?: fn.fn1
            val mergedFn = findMergedFunctions(fn.fn0, fn1)
            when {
                mergedFn != null -> mergedFn
                recurseResult != null -> FunctionCallChain.Chain2(fn.instantiation, fn.fn0, recurseResult)
                else -> null
            }
        } else {
            null
        }
    }

    abstract fun findMergedFunctions(fn0: APLFunction, fn1: APLFunction): APLFunction?
}
