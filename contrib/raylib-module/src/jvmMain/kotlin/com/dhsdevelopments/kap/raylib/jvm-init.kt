package com.dhsdevelopments.kap.raylib

import com.dhsdevelopments.kap.KapModuleBuilder
import com.dhsdevelopments.kap.ParameterInfo

class JvmRaylibModuleBuilder : KapModuleBuilder {
    override val name get() = "raylib"
    override val description get() = "Kap interface to Raylib"

    override fun canBeInstantiated() = true
    override fun configurationParameters() = emptyList<ParameterInfo>()
    override fun makeModuleWithParams(params: Map<String, Any>) = RaylibModule()
}
