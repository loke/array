package com.dhsdevelopments.kap

import kotlin.test.Test

class UniqueMaskTest : APLTest() {
    @Test
    fun test1DArray() {
        parseAPLExpression("≠ 1 0 3 1 1 3 2 10 9 8").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 0, 0, 0, 1, 1, 1, 1), result)
        }
    }

    @Test
    fun test2DArray() {
        parseAPLExpression("≠ 3 2 ⍴ 1 2 2 1 1 2").let { result ->
            assert1DArray(arrayOf(1, 1, 0), result)
        }
    }

    @Test
    fun test3DArray() {
        parseAPLExpression("≠ 4 2 2 ⍴ 1 2 3 4 1 2 3 4 1 3 3 5 3 5 1 1").let { result ->
            assert1DArray(arrayOf(1, 0, 1, 1), result)
        }
    }

    @Test
    fun testWithStrings() {
        parseAPLExpression("≠ \"foo\" \"test\" \"abc\" \"abc\" \"test\" \"foo\" \"foo123\"").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 0, 0, 0, 1), result)
        }
    }

    @Test
    fun testWithScalar() {
        parseAPLExpression("≠ 100").let { result ->
            assert1DArray(arrayOf(1), result)
        }
    }

    @Test
    fun testWithScalarEnclosed() {
        parseAPLExpression("≠ ⊂\"abc\"").let { result ->
            assert1DArray(arrayOf(1), result)
        }
    }
}
