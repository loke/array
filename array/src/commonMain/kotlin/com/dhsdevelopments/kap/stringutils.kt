package com.dhsdevelopments.kap

private val SINGLE_NEWLINE_REGEX = "([^\\n#])\\n([^\\n])".toRegex()
private val HASH_AT_END_OF_LINE_REGEX = "#\n".toRegex()

@Suppress("ReplaceGetOrSet")
fun String.reformatLines(): String {
    return this
        .trimIndent()
        .replace(SINGLE_NEWLINE_REGEX) { result -> (result.groups.get(1)!!.value + ' ' + result.groups.get(2)!!.value) }
        .replace(HASH_AT_END_OF_LINE_REGEX, "\n")
}
