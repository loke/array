package com.dhsdevelopments.kap.gui.arrayedit

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.csv.writeAPLArrayAsCsv
import com.dhsdevelopments.kap.gui.Client
import com.dhsdevelopments.kap.gui.ValueWithName
import com.dhsdevelopments.kap.gui.objview.ActionDescriptor
import com.dhsdevelopments.kap.gui.objview.ObjectEditorHandler
import com.dhsdevelopments.kap.msofficereader.saveExcelFile
import java.io.*

fun exportToExcel(client: Client, values: List<ValueWithName>) {
    val file = client.selectFile(forSave = true, nameHeader = "Select file", extensions = listOf("Excel" to listOf("xlsx")))
    if (file != null) {
        client.withErrorDialog("Export to Excel", "Error when exporting") {
            saveExcelFile(values.map { v -> Pair(v.name?.symbolName, v.value) }, file.path)
        }
    }
}

val exportToExcelProvider = object : ObjectEditorHandler {
    override fun name(value: APLValue) = "Export to Excel"

    override fun canEdit(value: APLValue): ActionDescriptor? {
        return if (value.dimensions.size in 1..2) {
            object : ActionDescriptor {
                override val menuTitle get() = "Export to Excel"

                override fun perform(client: Client, value: APLValue, name: Symbol?) {
                    exportToExcel(client, listOf(ValueWithName(value, name)))
                }
            }
        } else {
            null
        }
    }
}

fun exportToCsv(client: Client, value: APLValue) {
    require(value.dimensions.size == 2)
    val file = client.selectFile(forSave = true, nameHeader = "Select file")
    if (file != null) {
        client.withErrorDialog("Export to CSV") {
            WriterCharacterConsumer(FileWriter(file, Charsets.UTF_8)).use { dest ->
                writeAPLArrayAsCsv(dest, value)
            }
        }
    }
}

val exportToCsvProvider = object : ObjectEditorHandler {
    override fun name(value: APLValue) = "Export to CSV"

    override fun canEdit(value: APLValue): ActionDescriptor? {
        return if (value.dimensions.size == 2) {
            object : ActionDescriptor {
                override val menuTitle get() = "Export to CSV"

                override fun perform(client: Client, value: APLValue, name: Symbol?) {
                    exportToCsv(client, value)
                }
            }
        } else {
            null
        }
    }
}

fun exportToCode(client: Client, values: List<ValueWithName>) {
    val file = client.selectFile(forSave = true, nameHeader = "Select file")
    if (file != null) {
        client.withErrorDialog("Export to Code", "Error when exporting") {
            saveToCode(values, file.path)
        }
    }
}

private fun saveToCode(values: List<ValueWithName>, filename: String) {
    require(values.isNotEmpty())
    require(values.all { v -> v.name != null })
    PrintWriter(BufferedWriter(OutputStreamWriter(FileOutputStream(filename), Charsets.UTF_8))).use { outStream ->
        values.forEach { v ->
            if (v.description != null) {
                outStream.println("⍝ ${v.description}")
            }
            val asString = v.value.formatted(FormatStyle.READABLE)
            outStream.println("${v.name!!.nameWithNamespace} ← ${asString}")
        }
    }
}
