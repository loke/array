package com.dhsdevelopments.kap

class MethodNotFoundException(name: Symbol, pos: Position? = null) : APLEvalException("Method not found: ${name.nameWithNamespace}", pos)

fun interface KapMethod {
    fun callMethod(context: RuntimeContext, objRef: APLValue, a: APLValue, pos: Position): APLValue
}

class MapClass : SystemClass("map") {
    override fun resolveMethod(engine: Engine, ref: APLValue, methodName: Symbol, pos: Position): KapMethod {
        val map = ref.ensureMap(pos)
        val methodsValue = map.lookupValue(APLSymbol(engine.standardSymbols.methods).makeTypeQualifiedKey())
            ?: throwAPLException(MethodNotFoundException(methodName, pos))
        val methodsMap = methodsValue.ensureMap(pos)
        val v = methodsMap.lookupValue(APLSymbol(methodName).makeTypeQualifiedKey()) ?: throwAPLException(MethodNotFoundException(methodName, pos))
        val unwrapped = v.unwrapDeferredValue()
        if (unwrapped !is LambdaValue) {
            throwAPLException(MethodNotFoundException(methodName, pos))
        }
        return APLFunctionMethodCall(unwrapped.makeClosure())
    }
}

class MethodCallFunction(val objectRefInstruction: Instruction, val methodName: Symbol) : APLFunctionDescriptor {
    inner class MethodCallFunctionImpl(instantiation: FunctionInstantiation) : NoAxisAPLFunction(instantiation) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val objRef = objectRefInstruction.evalWithContext(context)
            val method = objRef.kapClass.resolveMethod(context.engine, objRef, methodName, pos)
            return method.callMethod(context, objRef, a, pos)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = MethodCallFunctionImpl(instantiation)
}

class APLFunctionMethodCall(val fn: APLFunction) : KapMethod {
    override fun callMethod(context: RuntimeContext, objRef: APLValue, a: APLValue, pos: Position): APLValue {
        return fn.eval2Arg(context, objRef, a, null)
    }
}
