package com.dhsdevelopments.kap.net

import com.dhsdevelopments.kap.LinuxByteConsumer
import com.dhsdevelopments.kap.LinuxByteProvider
import com.dhsdevelopments.kap.MPFileException
import kotlinx.cinterop.*
import platform.posix.*

@OptIn(ExperimentalForeignApi::class)
class LinuxNetworkApi : NetworkApi {
    override fun connect(host: String, port: Int): TcpConnection {
        memScoped {
            val addr = lookupAddr(host, port, AI_ADDRCONFIG or AI_V4MAPPED)
            val addrinfo = addr.pointed!!
            try {
                val s = socket(addrinfo.ai_family, addrinfo.ai_socktype, addrinfo.ai_protocol)
                if (s < 0) {
                    throw MPNetworkException("Failed to create socket: ${strerror(errno)?.toKString()}")
                }

                connect(s, addrinfo.ai_addr, addrinfo.ai_addrlen).let { res ->
                    if (res == -1) {
                        close(s)
                        throw MPNetworkException("failed to connect to host: ${strerror(errno)?.toKString()}")
                    }
                }

                return LinuxTcpConnection(s, LinuxSockaddr(host, port))
            } finally {
                freeaddrinfo(addr.value)
            }
        }
    }

    override fun hostname(): String {
        memScoped {
            val size = HOST_NAME_MAX + 1
            // According to the documentation, there are cases where the resulting string is
            // not null-terminated. In order to deal with this, the buffer is allocated to be
            // or byte larger than the required specified size, and set the last byte to 0.
            // This will ensure that the string is always null-terminated.
            val buf = allocArray<ByteVar>(size + 1)
            buf[size] = 0
            val res = gethostname(buf, size.toULong())
            if (res == -1) {
                throw MPNetworkException("gethostname failure: ${strerror(errno)?.toKString()}")
            }
            return buf.toKString()
        }
    }

    override fun startListener(port: Int, bindAddress: String?): TcpServerSocket {
        memScoped {
            val addr = lookupAddr(bindAddress, port, AI_PASSIVE or AI_ADDRCONFIG or AI_V4MAPPED)
            val addrinfo = addr.pointed!!
            try {
                val s = socket(addrinfo.ai_family, addrinfo.ai_socktype, addrinfo.ai_protocol)
                if (s < 0) {
                    throw MPNetworkException("Failed to create socket: ${strerror(errno)?.toKString()}")
                }

                val arg = alloc<IntVar>()
                arg.value = 1
                setsockopt(s, SOL_SOCKET, SO_REUSEADDR, arg.ptr, sizeOf<IntVar>().toUInt()).let { res ->
                    if (res == -1) {
                        close(s)
                        throw MPNetworkException("setsockopt failure: ${strerror(errno)?.toKString()}")
                    }
                }

                bind(s, addrinfo.ai_addr, addrinfo.ai_addrlen).let { res ->
                    if (res == -1) {
                        close(s)
                        throw MPNetworkException("Bind failure: ${strerror(errno)?.toKString()}")
                    }
                }

                listen(s, 50).let { res ->
                    if (res == -1) {
                        close(s)
                        throw MPNetworkException("Listen failure: ${strerror(errno)?.toKString()}")
                    }
                }

                return LinuxServerSocket(s)
            } finally {
                freeaddrinfo(addr.value)
            }
        }
    }

    @Suppress("NOTHING_TO_INLINE")
    private inline fun MemScope.lookupAddr(host: String?, port: Int, flags: Int): CPointerVar<addrinfo> {
        val addr = allocPointerTo<addrinfo>()
        val addrinfoInput = alloc<addrinfo>()
        addrinfoInput.ai_addr = null // This should be AF_UNSPEC, which is 0 on Linux, but Kotlin native doesn't have a good way to convert such value
        addrinfoInput.ai_socktype = SOCK_STREAM
        addrinfoInput.ai_protocol = 0
        addrinfoInput.ai_flags = flags
        addrinfoInput.ai_addrlen = 0U
        addrinfoInput.ai_addr = null
        addrinfoInput.ai_canonname = null
        addrinfoInput.ai_next = null

        getaddrinfo(host, port.toString(), addrinfoInput.ptr, addr.ptr).let { res ->
            if (res != 0) {
                throw MPNetworkException("Failed to lookup address: ${gai_strerror(res)?.toKString()}")
            }
        }

        return addr
    }
}

@OptIn(ExperimentalForeignApi::class)
class LinuxServerSocket(val socket: Int) : TcpServerSocket {
    override fun accept(): TcpConnection {
        memScoped {
            val addr = alloc<sockaddr_in6>()
            val socklen = alloc<socklen_tVar>()
            socklen.value = sizeOf<sockaddr_in6>().toUInt()
            val conn = accept(socket, addr.ptr.reinterpret(), socklen.ptr)
            if (conn < 0) {
                throw MPNetworkException("Error when calling accept: ${strerror(errno)?.toKString()}")
            }
            val sockaddr = try {
                LinuxSockaddr.make(addr.ptr, socklen.value)
            } catch (e: MPFileException) {
                close(conn)
                throw e
            }
            return LinuxTcpConnection(conn, sockaddr)
        }
    }

    override fun close() {
        close(socket)
    }
}

@OptIn(ExperimentalForeignApi::class)
class LinuxSockaddr(override val host: String, override val port: Int) : TcpSockaddr {
    override fun toString() = "LinuxSockaddr[host=${host}, port=${port}]"

    companion object {
        fun make(sockaddrRes: COpaquePointer, socklen: socklen_t): LinuxSockaddr {
            val h = resolveHostAndPort(sockaddrRes, socklen)
            return LinuxSockaddr(h.host, h.port)
        }

        private fun resolveHostAndPort(sockaddrRes: COpaquePointer, socklen: socklen_t): HostAndPort {
            val hostLength = 1024
            val servLength = 1024
            memScoped {
                val hostBuffer = allocArray<ByteVar>(hostLength)
                val servBuffer = allocArray<ByteVar>(servLength)
                val res = getnameinfo(
                    sockaddrRes.reinterpret(),
                    socklen,
                    hostBuffer,
                    hostLength.toUInt(),
                    servBuffer,
                    servLength.toUInt(),
                    NI_NUMERICHOST or NI_NUMERICSERV)
                if (res == 0) {
                    val h = hostBuffer.toKString()
                    val p = servBuffer.toKString().let { serviceString ->
                        try {
                            serviceString.toInt()
                        } catch (_: NumberFormatException) {
                            0
                        }
                    }
                    return HostAndPort(h, p)
                } else {
                    throw MPNetworkException("Unable to resolve address: ${gai_strerror(res)?.toKString()}")
                }
            }
        }
    }

    private data class HostAndPort(val host: String, val port: Int)
}

class LinuxTcpConnection(val conn: Int, override val sockaddr: LinuxSockaddr) : TcpConnection {
    override val byteProvider = LinuxByteProvider(conn, "addr=${sockaddr}")
    override val byteConsumer = LinuxByteConsumer(conn, "addr=${sockaddr}")

    override fun close() {
        close(conn)
    }
}

actual fun networkApi(): NetworkApi? {
    return LinuxNetworkApi()
}
