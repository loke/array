package com.dhsdevelopments.kap.json

class JsJsonDecoder : JsonDecoder {
    override fun decodeJsonFromString(source: String): JsonElement {
        val root = try {
            JSON.parse<Any?>(source)
        } catch (e: Throwable) {
            throw JsonParseException("Error when parsing: ${e.message}")
        }
        return parseElement(root)
    }

    private fun parseElement(element: Any?): JsonElement {
        return when (element) {
            null -> JsonNull
            is Array<*> -> parseArray(element)
            is String -> JsonString(element)
            is Double -> JsonNumber(element)
            is Boolean -> JsonBoolean.fromBoolean(element)
            else -> parseObject(element)
        }
    }

    private fun parseArray(array: Array<*>): JsonArray {
        val result = ArrayList<JsonElement>()
        array.forEach { element ->
            result.add(parseElement(element))
        }
        return JsonArray(result)
    }

    private fun parseObject(obj: dynamic): JsonObject {
        val result = ArrayList<Pair<String, JsonElement>>()
        val keysFn = js("Object.keys")
        val keyArray = keysFn(obj)
        keyArray.forEach { k ->
            if (k !is String) {
                throw JsonParseException("Key is not a string: ${k}")
            }
            result.add(Pair(k.unsafeCast<String>(), parseElement(obj[k])))
        }
        return JsonObject((result))
    }
}

actual fun makeJsonDecoder(): JsonDecoder {
    return JsJsonDecoder()
}
