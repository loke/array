package com.dhsdevelopments.kap.gui

import com.dhsdevelopments.kap.JvmReplBuilder
import com.dhsdevelopments.kap.KeyboardInput
import com.dhsdevelopments.kap.editor.JlineKeyboardInput
import com.dhsdevelopments.kap.gui.Client.Companion.replGlobal
import com.dhsdevelopments.kap.makeKeyboardInput
import com.dhsdevelopments.kap.repl.AbstractGenericRepl
import javafx.application.Application
import javafx.stage.Stage

interface ApplicationServices {
    fun showDocument(url: String)
}

class ClientApplication : Application() {
    private var client: Client? = null

    override fun start(stage: Stage) {
        val repl: AbstractGenericRepl = replGlobal ?: throw IllegalStateException("replGlobal is not set")
        val services = object : ApplicationServices {
            override fun showDocument(url: String) {
                hostServices.showDocument(url)
            }
        }
        client = Client(services, stage, repl)
    }
}

class FxClientGenericReplBuilder : JvmReplBuilder() {
    override fun hasGui() = true
    override fun guiIsDefault() = true

    override fun makeKbInput(repl: AbstractGenericRepl): KeyboardInput {
        return if (disableLineeditor) {
            makeKeyboardInput(repl.engine) ?: error("Can't create keyboard input")
        } else {
            val input = JlineKeyboardInput(repl)
            input.registerSignalHandler { repl.engine.interruptEvaluation() }
            input
        }
    }
}
