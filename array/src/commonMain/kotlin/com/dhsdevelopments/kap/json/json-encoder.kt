package com.dhsdevelopments.kap.json

import com.dhsdevelopments.kap.CharacterConsumer

interface JsonEncoder {
    fun encodeJsonToStream(element: JsonElement, consumer: CharacterConsumer)
}

fun makeJsonEncoder() = JsonEncoderImpl.make()

class JsonEncoderImpl private constructor() : JsonEncoder {
    override fun encodeJsonToStream(element: JsonElement, consumer: CharacterConsumer) {
        when (element) {
            is JsonString -> consumer.writeString(makeJsonString(element.value))
            is JsonNumber -> consumer.writeString(element.value.toString())
            is JsonBoolean -> consumer.writeString(if (element.value) "true" else "false")
            is JsonNull -> consumer.writeString("null")
            is JsonArray -> {
                consumer.writeString("[")
                var first = true
                element.values.forEach { e ->
                    if (first) {
                        first = false
                    } else {
                        consumer.writeString(",")
                    }
                    encodeJsonToStream(e, consumer)
                }
                consumer.writeString("]")
            }
            is JsonObject -> {
                consumer.writeString("{")
                var first = true
                element.values.forEach { (k, v) ->
                    if (first) {
                        first = false
                    } else {
                        consumer.writeString(",")
                    }
                    consumer.writeString("\"")
                    consumer.writeString(k)
                    consumer.writeString("\":")
                    encodeJsonToStream(v, consumer)
                }
                consumer.writeString("}")
            }
            else -> error("Unexpected type: ${element}")
        }
    }

    companion object {
        fun make() = JsonEncoderImpl()
    }
}
