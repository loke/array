package com.dhsdevelopments.kap.edit

import com.dhsdevelopments.kap.APLTest
import kotlin.test.Test
import kotlin.test.assertTrue

class ParenCandidatesTest : APLTest() {
    @Test
    fun parenCandidatesSimple() {
        val engine = makeEngine()
        val candidates = parenCandidates(engine, "1 + 2 + 3", 9)
        assertTrue(candidates.contains(4))
        assertTrue(candidates.contains(0))
    }

    @Test
    fun parenCandidatesMultiline() {
        val engine = makeEngine()
        val src =
            """
            |foo + bar + `
            |   test + (abc + def) + `
            |   qwert + abcdef
            """.trimMargin()
        val candidates = parenCandidates(engine, src, src.length)
        assertTrue(candidates.contains(src.length - 6))
        assertTrue(candidates.contains(src.length - 6 - 8))
        assertTrue(candidates.contains(src.length - 6 - 8 - 26))
    }

    @Test
    fun middleOfLine() {
        val engine = makeEngine()
        val src = "123 + 45678 + 987654"
        val candidates = parenCandidates(engine, src, 11)
        assertTrue(candidates.contains(6))
    }

    @Test
    fun customSyntaxInParsedExpression() {
        val engine = makeEngine(withStandardLib = true)
        val src = "while (0) { 1+2 }"
        val candidates = parenCandidates(engine, src, src.length - 2)
        assertTrue(candidates.contains(src.length - 2 - 1))
        assertTrue(candidates.contains(src.length - 2 - 3))
    }

    @Test
    fun parseError() {
        val engine = makeEngine()
        val src = "1+"
        val candidates = parenCandidates(engine, src, src.length)
        assertTrue(candidates.isEmpty())
    }

    @Test
    fun parseErrorWithComplexExpression() {
        val engine = makeEngine()
        val src = "1+2+(3 {1+} 0)+(4+5)"
        val candidates = parenCandidates(engine, src, src.length)
        assertTrue(candidates.isEmpty())
    }

    @Test
    fun wrapIndexLookup() {
        val engine = makeEngine()
        val src = "foo[1] + bar[2]"
        val candidates = parenCandidates(engine, src, src.length)
        assertTrue(candidates.contains(src.length - 6))
        assertTrue(candidates.contains(0))
    }
}
