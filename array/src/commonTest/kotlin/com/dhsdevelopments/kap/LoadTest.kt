package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.builtins.TagCatch
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class LoadTest : APLTest() {
    @Test
    fun loadSimpleSource() {
        val engine = makeEngine()
        val res0 = engine.parseAndEval(StringSourceLocation("io:load \"test-data/test-source.kap\""))
        assertSimpleNumber(10, res0)
        val res1 = engine.parseAndEval(StringSourceLocation("foo:bar 1"))
        assertSimpleNumber(101, res1)
    }

    @Test
    fun ensureLoadPreservesOldNamespace() {
        val engine = makeEngine()
        val res0 =
            engine.parseAndEval(
                StringSourceLocation("namespace(\"a\") x←1 ◊ io:load \"test-data/test-source.kap\""))
        assertSimpleNumber(10, res0)
        val res1 = engine.parseAndEval(StringSourceLocation("foo:bar 1"))
        assertSimpleNumber(101, res1)
        val res2 = engine.parseAndEval(StringSourceLocation("x + 10"))
        assertSimpleNumber(11, res2)
    }

    @Test
    fun ensureLoadPreservesOldNamespaceOnError() {
        val engine = makeEngine()
        try {
            engine.parseAndEval(
                StringSourceLocation("namespace(\"a\") x←1 ◊ io:load \"test-data/parse-error.kap\""))
        } catch (e: ParseException) {
            // expected
        }
        val res2 = engine.parseAndEval(StringSourceLocation("x + 10"))
        assertSimpleNumber(11, res2)
    }

    @Test
    fun testTabIndent() {
        val engine = makeEngine()
        engine.addLibrarySearchPath("standard-lib")
        engine.parseAndEval(StringSourceLocation("use(\"standard-lib.kap\")"))
        val result = engine.parseAndEval(FileSourceLocation("test-data/space-chars.kap"))
        assertSimpleNumber(102, result)
    }

    @Test
    fun testTabIndentWithUse() {
        val engine = makeEngine()
        engine.addLibrarySearchPath("standard-lib")
        engine.parseAndEval(StringSourceLocation("use(\"standard-lib.kap\")"))
        val result = engine.parseAndEval(StringSourceLocation("use(\"test-data/space-chars.kap\")"))
        assertSimpleNumber(102, result)
    }

    @Test
    fun loadWithMissingFileShouldThrowException() {
        val engine = makeEngine()
        try {
            engine.parseAndEval(StringSourceLocation("io:load \"foo-abc\""))
            fail()
        } catch (e: TagCatch) {
            val tagKey = e.tag.key.ensureSymbol().value
            assertEquals(engine.keywordNamespace.name, tagKey.namespace)
            assertEquals("fileNotFound", tagKey.symbolName)
        }
    }
}
