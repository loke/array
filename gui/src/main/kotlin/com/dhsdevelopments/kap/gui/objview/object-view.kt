package com.dhsdevelopments.kap.gui.objview

import com.dhsdevelopments.kap.APLNilValue
import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.FormatStyle
import com.dhsdevelopments.kap.Symbol
import com.dhsdevelopments.kap.gui.Client
import com.dhsdevelopments.kap.gui.arrayedit.ArrayEditor
import com.dhsdevelopments.kap.gui.arrayedit.exportToCsvProvider
import com.dhsdevelopments.kap.gui.arrayedit.exportToExcelProvider
import com.dhsdevelopments.kap.gui.mapview.MapPanel
import javafx.scene.Node
import javafx.scene.control.Label

interface ObjectEditorHandler {
    fun name(value: APLValue): String
    fun canEdit(value: APLValue): ActionDescriptor?
}

interface ActionDescriptor {
    val menuTitle: String
    fun perform(client: Client, value: APLValue, name: Symbol?)
}

interface ObjectRendererHandler {
    fun canRender(value: APLValue): APLValueRenderer?
}

interface APLValueRenderer {
    fun makeNode(value: APLValue): Node = Label(nodeAsString(value))
    fun nodeAsString(value: APLValue): String
    fun preferString(value: APLValue): Boolean
}

class ObjectViewerRegistry() {
    private val editorProviders = ArrayList<ObjectEditorHandler>()
    private val rendererProviders = ArrayList<ObjectRendererHandler>()

    init {
        ArrayEditor.PROVIDERS.forEach(::addEditorProvider)
        MapPanel.PROVIDERS.forEach(::addEditorProvider)
        addEditorProvider(exportToExcelProvider)
        addEditorProvider(exportToCsvProvider)

        addRendererProvider(DefaultRendererProvider)
        addRendererProvider(TableRendererProvider)
        addRendererProvider(RationalNumberRendererHandler)
        addRendererProvider(StringRendererProvider)
        addRendererProvider(NullRendererProvider)
    }

    fun addEditorProvider(provider: ObjectEditorHandler) {
        editorProviders.add(0, provider)
    }

    fun findEditorProviders(value: APLValue): List<ActionDescriptor> {
        return editorProviders.mapNotNull { p -> p.canEdit(value) }
    }

    fun addRendererProvider(provider: ObjectRendererHandler) {
        rendererProviders.add(0, provider)
    }

    fun findRendererProvider(value: APLValue): APLValueRenderer {
        return rendererProviders.firstNotNullOf { p -> p.canRender(value) }
    }
}

private object DefaultRendererProvider : ObjectRendererHandler {
    override fun canRender(value: APLValue): APLValueRenderer {
        return DefaultRenderer
    }
}

private object DefaultRenderer : APLValueRenderer {
    override fun makeNode(value: APLValue) = Label(nodeAsString(value))
    override fun nodeAsString(value: APLValue) = value.formatted(FormatStyle.READABLE)
    override fun preferString(value: APLValue) = true
}

private object NullRendererProvider : ObjectRendererHandler {
    override fun canRender(value: APLValue): APLValueRenderer? {
        return if (value is APLNilValue) {
            NullRenderer
        } else {
            null
        }
    }
}

private object NullRenderer : APLValueRenderer {
    override fun nodeAsString(value: APLValue) = ""
    override fun preferString(value: APLValue) = true
}
