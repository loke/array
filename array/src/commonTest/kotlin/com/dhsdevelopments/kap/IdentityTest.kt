package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertSame

class IdentityTest : APLTest() {
    @Test
    fun monadicRightTack() {
        parseAPLExpression("⊢ 1 2 3 4 5").let { result ->
            assert1DArray(arrayOf(1, 2, 3, 4, 5), result)
        }
    }

    @Test
    fun dyadicRightTack() {
        parseAPLExpression("1 2 ⊢ 3 4 5 6").let { result ->
            assert1DArray(arrayOf(3, 4, 5, 6), result)
        }
    }

    @Test
    fun dyadicRightTackShouldNotCollapse0() {
        parseAPLExpressionWithOutput("(io:print¨ 1 2) ⊢ (io:print¨ 3 4 5 6)").let { (result, out) ->
            assert1DArray(arrayOf(3, 4, 5, 6), result)
            assertEquals("3456", out)
        }
    }

    @Test
    fun dyadicRightTackShouldNotCollapse1() {
        parseAPLExpressionWithOutput("1↑ (io:print¨ 1 2) ⊢ (io:print¨ 3 4 5 6)").let { (result, out) ->
            assert1DArray(arrayOf(3), result)
            assertEquals("3", out)
        }
    }

    @Test
    fun monadicLeftTack() {
        parseAPLExpression("⊣ 1 2 3 4 5").let { result ->
            assertDimension(emptyListDimensions(), result)
            assertSame(APLNilValue, result.metadata.defaultValue)
        }
    }

    @Test
    fun monadicLeftTackShouldCollapse() {
        parseAPLExpressionWithOutput("⊣ io:print¨ 1 2 3 4 5").let { (result, out) ->
            assertDimension(emptyListDimensions(), result)
            assertSame(APLNilValue, result.metadata.defaultValue)
            assertEquals("12345", out)
        }
    }

    @Test
    fun dyadicLeftTack() {
        parseAPLExpression("1 2 ⊣ 3 4 5 6").let { result ->
            assert1DArray(arrayOf(1, 2), result)
        }
    }

    @Test
    fun dyadicLeftTackShouldNotCollapse0() {
        parseAPLExpressionWithOutput("(io:print¨ 1 2) ⊣ (io:print¨ 3 4 5 6)").let { (result, out) ->
            assert1DArray(arrayOf(1, 2), result)
            assertEquals("12", out)
        }
    }

    @Test
    fun dyadicLeftTackShouldNotCollapse1() {
        parseAPLExpressionWithOutput("1↑ (io:print¨ 1 2) ⊣ (io:print¨ 3 4 5 6)").let { (result, out) ->
            assert1DArray(arrayOf(1), result)
            assertEquals("1", out)
        }
    }

    @Test
    fun testInverseIdentity() {
        parseAPLExpression("⊢˝ 1234").let { result ->
            assertSimpleNumber(1234, result)
        }
    }

    @Test
    fun testRightTackInverse() {
        parseAPLExpression("9 ⊢˝ 1234").let { result ->
            assertSimpleNumber(1234, result)
        }
    }

    @Test
    fun testRightTackSwappedAndInversedShouldFail() {
        assertFailsWith<InverseNotAvailable> {
            parseAPLExpression("9 ⊢⍨˝ 1234")
        }
    }

    @Test
    fun testLeftTackSwappedAndInversed() {
        parseAPLExpression("9 ⊣⍨˝ 1234").let { result ->
            assertSimpleNumber(1234, result)
        }
    }

    @Test
    fun testUnderIdentity() {
        parseAPLExpression("(1+)⍢⊢ 10").let { result ->
            assertSimpleNumber(11, result)
        }
    }

    @Test
    fun testUnderRightTack() {
        parseAPLExpression("4 (1+)⍢⊢ 10").let { result ->
            assertSimpleNumber(11, result)
        }
    }

    @Test
    fun testUnderRightTackAndSwappedShouldFail() {
        assertFailsWith<InverseNotAvailable> {
            parseAPLExpression("4 (1+)⍢(⊢⍨) 10")
        }
    }

    @Test
    fun testUnderLeftTackAndSwapped() {
        parseAPLExpression("4 (1+)⍢(⊣⍨) 10").let { result ->
            assertSimpleNumber(11, result)
        }
    }
}
