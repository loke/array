package com.dhsdevelopments.kap.jvmmod

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.ThrowableTag
import com.dhsdevelopments.kap.builtins.makeAPLNumberAsBoolean
import java.lang.reflect.Constructor
import java.lang.reflect.Field
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

open class JvmModuleException(message: String, pos: Position? = null, cause: Throwable? = null) : APLEvalException(message, pos = pos, cause = cause)

object JvmInstanceClass : ModuleClass() {
    override val name get() = "jvmInstance"
}

class JvmModule : KapModule {
    override val name get() = "jvm"

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("jvm")
        engine.classManager.registerSupplementarySystemClass(ns.internAndExport("jvmInstance"), JvmInstanceClass)
        engine.registerFunction(ns.internAndExport("findClass"), FindClassFunction())
        engine.registerFunction(ns.internAndExport("findMethod"), FindMethodFunction())
        engine.registerFunction(ns.internAndExport("findConstructor"), FindConstructorFunction())
        engine.registerFunction(ns.internAndExport("findPrimitiveTypeClass"), FindPrimitiveTypeClassFunction())
        engine.registerFunction(ns.internAndExport("findField"), FindFieldFunction())
        engine.registerFunction(ns.internAndExport("callMethod"), CallMethodFunction())
        engine.registerFunction(ns.internAndExport("createInstance"), CreateInstanceFunction())
        engine.registerFunction(ns.internAndExport("toJvmString"), ToJvmStringFunction())
        engine.registerFunction(ns.internAndExport("toJvmShort"), ToJvmShortFunction())
        engine.registerFunction(ns.internAndExport("toJvmInt"), ToJvmIntFunction())
        engine.registerFunction(ns.internAndExport("toJvmLong"), ToJvmLongFunction())
        engine.registerFunction(ns.internAndExport("toJvmByte"), ToJvmByteFunction())
        engine.registerFunction(ns.internAndExport("toJvmChar"), ToJvmCharFunction())
        engine.registerFunction(ns.internAndExport("toJvmFloat"), ToJvmFloatFunction())
        engine.registerFunction(ns.internAndExport("toJvmDouble"), ToJvmDoubleFunction())
        engine.registerFunction(ns.internAndExport("toJvmBoolean"), ToJvmBooleanFunction())
        engine.registerFunction(ns.internAndExport("fromJvm"), FromJvmFunction())
        engine.registerFunction(ns.internAndExport("instanceOf"), InstanceOfFunction())
        engine.registerFunction(ns.internAndExport("getField"), GetFieldFunction())
        engine.registerFunction(ns.internAndExport("isJvmNull"), IsJvmNullFunction())
    }
}

class JvmFunctionCallException(
    engine: Engine, e: InvocationTargetException, pos: Position? = null
) : JvmModuleException("JVM Exception: ${e.targetException.message}", pos = pos, cause = e) {
    override val tag: ThrowableTag
    val originException: Throwable = e.targetException

    init {
        tag = ThrowableTag(APLSymbol(engine.coreNamespace.internSymbol("jvmMethodCallException")), JvmInstanceValue(originException))
    }
}

class JvmInstanceValue(val instance: Any?) : APLSingleValue() {
    override val kapClass get() = JvmInstanceClass

    override fun formatted(style: FormatStyle): String {
        return instance.toString()
    }

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Boolean {
        if (reference !is JvmInstanceValue) {
            return false
        }
        return instance == reference.instance
    }

    override fun typeQualifiedHashCode(): Int {
        return instance?.hashCode() ?: 0
    }
}

class FindClassFunction : APLFunctionDescriptor {
    class FindClassFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(1, 1, pos) {
        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val className = args[0].toStringValue(pos)
            val cl = try {
                Class.forName(className)
            } catch (_: ClassNotFoundException) {
                throwAPLException(JvmModuleException("Class not found: ${className}", pos))
            }
            return JvmInstanceValue(cl)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FindClassFunctionImpl(instantiation)
}

private fun ensureJvmGenericValue(a: APLValue, pos: Position): JvmInstanceValue {
    val a0 = a.unwrapDeferredValue()
    if (a0 !is JvmInstanceValue) {
        throwAPLException(IncompatibleTypeException("Expected JVM instance, got: ${a0.kapClass.name}", pos))
    }
    return a0
}

private inline fun <reified T : Any?> ensureJvmInstance(a: APLValue, pos: Position): T {
    val wrapped: JvmInstanceValue = ensureJvmGenericValue(a, pos)
    val instance = wrapped.instance
    if (instance !is T) {
        throwAPLException(
            APLIllegalArgumentException(
                "Expected Java type: ${T::class.qualifiedName}, got: ${if (instance == null) "null" else instance::class.qualifiedName}",
                pos))
    }
    return instance
}

private fun toJava(a: APLValue, pos: Position? = null): Any? {
    val a0 = a.unwrapDeferredValue()
    return when {
        a0 is APLNilValue -> null
        a0 is JvmInstanceValue -> a0.instance
        a0 is APLLong -> a0.value
        a0.isStringValue() -> a0.toStringValue()
        else -> throwAPLException(APLIllegalArgumentException("Cannot convert to Java: ${a}", pos))
    }
}

class FindMethodFunction : APLFunctionDescriptor {
    class FindMethodFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(2, null, pos) {
        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val cl = ensureJvmInstance<Class<*>>(args[0], pos)
            val methodName = args[1].toStringValue(pos) { "method name" }
            val method = try {
                cl.getMethod(methodName, *args.drop(2).map { v -> ensureJvmInstance<Class<*>>(v, pos) }.toTypedArray())
            } catch (e: NoSuchMethodException) {
                throwAPLException(JvmModuleException("Method not found: ${methodName}", pos))
            }
            return JvmInstanceValue(method)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FindMethodFunctionImpl(instantiation)
}

class FindConstructorFunction : APLFunctionDescriptor {
    class FindConstructorFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(1, null, pos) {
        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val cl = ensureJvmInstance<Class<*>>(args[0], pos)
            val argTypes = args.drop(1).map { v -> ensureJvmInstance<Class<*>>(v, pos) }.toTypedArray()
            val constructor = try {
                cl.getConstructor(*argTypes)
            } catch (_: NoSuchMethodException) {
                throwAPLException(JvmModuleException("Constructor not found with args: ${argTypes}", pos))
            }
            return JvmInstanceValue(constructor)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FindConstructorFunctionImpl(instantiation)
}

class FindPrimitiveTypeClassFunction : APLFunctionDescriptor {
    @Suppress("RemoveRedundantQualifierName")
    class FindPrimitiveTypeClassFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(1, 1, pos) {
        private val typeMap = HashMap<Symbol, java.lang.Class<*>>()

        init {
            val jvmNs = instantiation.engine.makeNamespace("jvm")
            typeMap[jvmNs.internSymbol("char")] = java.lang.Character.TYPE
            typeMap[jvmNs.internSymbol("byte")] = java.lang.Byte.TYPE
            typeMap[jvmNs.internSymbol("short")] = java.lang.Short.TYPE
            typeMap[jvmNs.internSymbol("int")] = java.lang.Integer.TYPE
            typeMap[jvmNs.internSymbol("long")] = java.lang.Long.TYPE
            typeMap[jvmNs.internSymbol("float")] = java.lang.Float.TYPE
            typeMap[jvmNs.internSymbol("double")] = java.lang.Double.TYPE

            typeMap[jvmNs.internSymbol("charArray")] = java.lang.Character.TYPE.arrayType()
            typeMap[jvmNs.internSymbol("byteArray")] = java.lang.Byte.TYPE.arrayType()
            typeMap[jvmNs.internSymbol("shortArray")] = java.lang.Short.TYPE.arrayType()
            typeMap[jvmNs.internSymbol("intArray")] = java.lang.Integer.TYPE.arrayType()
            typeMap[jvmNs.internSymbol("longArray")] = java.lang.Long.TYPE.arrayType()
            typeMap[jvmNs.internSymbol("floatArray")] = java.lang.Float.TYPE.arrayType()
            typeMap[jvmNs.internSymbol("doubleArray")] = java.lang.Double.TYPE.arrayType()

            typeMap[jvmNs.internSymbol("void")] = java.lang.Void.TYPE
        }

        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val sym = args[0].ensureSymbol(pos).value
            val cl = typeMap[sym] ?: throwAPLException(APLIllegalArgumentException("Unexpected type name: ${sym.nameWithNamespace}", pos))
            return JvmInstanceValue(cl)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FindPrimitiveTypeClassFunctionImpl(instantiation)
}


class CallMethodFunction : APLFunctionDescriptor {
    class CallMethodFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(2, null, pos) {
        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val method = ensureJvmInstance<Method>(args[0], pos)
            val instance = toJava(args[1], pos)
            val methodArgs = args.drop(2).map { v -> toJava(v, pos) }.toTypedArray()
            val result = try {
                method.invoke(instance, *methodArgs)
            } catch (e: InvocationTargetException) {
                throwAPLException(JvmFunctionCallException(context.engine, e, pos))
            } catch (e: IllegalArgumentException) {
                throwAPLException(
                    JvmModuleException(
                        "Error when calling ${method} with argument types: ${methodArgs.map { v -> if (v == null) "null" else v::class.qualifiedName }}: ${e.message}",
                        pos,
                        e))
            } catch (e: NullPointerException) {
                throwAPLException(JvmModuleException("Called method ${method} with a null instance: ${e.message}", pos, e))
            }
            return JvmInstanceValue(result)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = CallMethodFunctionImpl(instantiation)
}

class CreateInstanceFunction : APLFunctionDescriptor {
    class CreateInstanceFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(1, null, pos) {
        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val constructor = ensureJvmInstance<Constructor<*>>(args[0], pos)
            val argsArray = args.drop(1).map { v -> toJava(v, pos) }.toTypedArray()
            val result = constructor.newInstance(*argsArray)
            return JvmInstanceValue(result)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = CreateInstanceFunctionImpl(instantiation)
}

class ToJvmStringFunction : APLFunctionDescriptor {
    class ToJvmStringFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val s = a.toStringValue(pos)
            return JvmInstanceValue(s)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ToJvmStringFunctionImpl(instantiation)
}

class ToJvmShortFunction : APLFunctionDescriptor {
    class ToJvmShortFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val n = a.ensureNumber(pos).asLong(pos)
            if (n < Short.MIN_VALUE || n > Short.MAX_VALUE) {
                throwAPLException(KapOverflowException("Value does not fit in short: ${n}", pos))
            }
            return JvmInstanceValue(n.toShort())
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ToJvmShortFunctionImpl(instantiation)
}

class ToJvmIntFunction : APLFunctionDescriptor {
    class ToJvmIntFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val n = a.ensureNumber(pos).asLong(pos)
            if (n < Int.MIN_VALUE || n > Int.MAX_VALUE) {
                throwAPLException(KapOverflowException("Value does not fit in int: ${n}", pos))
            }
            return JvmInstanceValue(n.toInt())
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ToJvmIntFunctionImpl(instantiation)
}

class ToJvmLongFunction : APLFunctionDescriptor {
    class ToJvmLongFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val n = a.ensureNumber(pos).asLong(pos)
            return JvmInstanceValue(n)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ToJvmLongFunctionImpl(instantiation)
}

class ToJvmByteFunction : APLFunctionDescriptor {
    class ToJvmByteFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val n = a.ensureNumber(pos).asLong(pos)
            if (n < Byte.MIN_VALUE || n > Byte.MAX_VALUE) {
                throwAPLException(KapOverflowException("Value does not fit in byte: ${n}", pos))
            }
            return JvmInstanceValue(n.toByte())
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ToJvmByteFunctionImpl(instantiation)
}

class ToJvmCharFunction : APLFunctionDescriptor {
    class ToJvmCharFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val n = a.ensureNumber(pos).asLong(pos)
            if (n < Char.MIN_VALUE.code || n > Char.MAX_VALUE.code) {
                throwAPLException(KapOverflowException("Value does not fit in char: ${n}", pos))
            }
            return JvmInstanceValue(n.toInt().toChar())
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ToJvmCharFunctionImpl(instantiation)
}

class ToJvmFloatFunction : APLFunctionDescriptor {
    class ToJvmFloatFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val n = a.ensureNumber(pos).asDouble(pos)
            return JvmInstanceValue(n.toFloat())
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ToJvmFloatFunctionImpl(instantiation)
}

class ToJvmDoubleFunction : APLFunctionDescriptor {
    class ToJvmDoubleFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val n = a.ensureNumber(pos).asDouble(pos)
            return JvmInstanceValue(n)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ToJvmDoubleFunctionImpl(instantiation)
}

class ToJvmBooleanFunction : APLFunctionDescriptor {
    class ToJvmBooleanFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val n = a.asBoolean()
            return JvmInstanceValue(n)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ToJvmBooleanFunctionImpl(instantiation)
}

private fun javaObjToKap(engine: Engine, value: Any?, pos: Position): APLValue {
    return when (value) {
        null -> APLNilValue
        is Short -> value.toLong().makeAPLNumber()
        is Int -> value.makeAPLNumber()
        is Long -> value.makeAPLNumber()
        is Byte -> value.toInt().makeAPLNumber()
        is Float -> value.toDouble().makeAPLNumber()
        is Double -> value.makeAPLNumber()
        is String -> APLString.make(value)
        is ByteArray -> APLArrayLong(dimensionsOfSize(value.size), LongArray(value.size) { i -> value[i].toLong() })
        is ShortArray -> APLArrayLong(dimensionsOfSize(value.size), LongArray(value.size) { i -> value[i].toLong() })
        is IntArray -> APLArrayLong(dimensionsOfSize(value.size), LongArray(value.size) { i -> value[i].toLong() })
        is LongArray -> APLArrayLong(dimensionsOfSize(value.size), value.copyOf())
        is FloatArray -> APLArrayDouble(dimensionsOfSize(value.size), DoubleArray(value.size) { i -> value[i].toDouble() })
        is DoubleArray -> APLArrayDouble(dimensionsOfSize(value.size), value.copyOf())
        is Array<*> -> APLArrayImpl(dimensionsOfSize(value.size), value.map { v -> javaObjToKap(engine, v, pos) }.toTypedArray())
        is List<*> -> APLArrayImpl(dimensionsOfSize(value.size), value.map { v -> javaObjToKap(engine, v, pos) }.toTypedArray())
        else -> throwAPLException(APLIllegalArgumentException("Unexpected JVM type: ${value::class.qualifiedName}", pos))
    }
}

class FromJvmFunction : APLFunctionDescriptor {
    class FromJvmFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val a0 = ensureJvmInstance<Any?>(a, pos)
            return javaObjToKap(context.engine, a0, pos)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FromJvmFunctionImpl(instantiation)
}

class InstanceOfFunction : APLFunctionDescriptor {
    class InstanceOfFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(2, 2, pos) {
        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val obj = ensureJvmInstance<Any?>(args[0], pos)
            val type = ensureJvmInstance<Class<*>>(args[1], pos)
            if (obj == null) {
                return APLLONG_0
            }
            return type.isInstance(obj).makeAPLNumberAsBoolean()
        }
    }

    override fun make(instantiation: FunctionInstantiation) = InstanceOfFunctionImpl(instantiation)
}

class FindFieldFunction : APLFunctionDescriptor {
    class FindFieldFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(2, 2, pos) {
        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val cl = ensureJvmInstance<Class<*>>(args[0], pos)
            val name = args[1].toStringValue(pos)
            return JvmInstanceValue(cl.getField(name))
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FindFieldFunctionImpl(instantiation)
}

class GetFieldFunction : APLFunctionDescriptor {
    class GetFieldFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(2, 2, pos) {
        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val field = ensureJvmInstance<Field>(args[0], pos)
            val obj = toJava(args[1], pos)
            return JvmInstanceValue(field.get(obj))
        }
    }

    override fun make(instantiation: FunctionInstantiation) = GetFieldFunctionImpl(instantiation)
}

class IsJvmNullFunction : APLFunctionDescriptor {
    class IsJvmNullFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(1, 1, pos) {
        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val obj = toJava(args[0], pos)
            return (obj == null).makeAPLNumberAsBoolean()
        }
    }

    override fun make(instantiation: FunctionInstantiation) = IsJvmNullFunctionImpl(instantiation)
}
