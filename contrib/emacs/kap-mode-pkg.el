(define-package "kap" "0.1" "Kap integration for Emacs"
  '((emacs "29"))
  :url "https://kapdemo.dhsdevelopments.com/"
  :keywords '("languages" "kap"))
