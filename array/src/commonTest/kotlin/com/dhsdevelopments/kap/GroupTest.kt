package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertFailsWith

class GroupTest : APLTest() {
    @Test
    fun groupSimpleArray() {
        parseAPLExpression("1 0 2 1 ⫇ 10 20 30 40").let { result ->
            assert1DArray(arrayOf(Inner1Dn(20), Inner1Dn(10, 40), Inner1Dn(30)), result)
        }
    }

    @Test
    fun groupWithEmptyArrays() {
        parseAPLExpression("1 0 3 0 ⫇ 10 20 30 40").let { result ->
            assert1DArray(arrayOf(Inner1Dn(20, 40), Inner1Dn(10), InnerAPLNull(), Inner1Dn(30)), result)
        }
    }

    @Test
    fun groupWithNegativeIndex() {
        parseAPLExpression("1 0 ¯1 3 0 ⫇ 10 20 50 30 40").let { result ->
            assert1DArray(arrayOf(Inner1Dn(20, 40), Inner1Dn(10), InnerAPLNull(), Inner1Dn(30)), result)
        }
    }

    @Test
    fun multiDimensionalInput() {
        parseAPLExpression("0 1 0 ⫇ 3 3 ⍴ 1 2 3 4 5 6 7 8 9").let { result ->
            assert1DArray(
                arrayOf(
                    InnerArrayN(dimensionsOfSize(2, 3), 1, 2, 3, 7, 8, 9),
                    InnerArrayN(dimensionsOfSize(1, 3), 4, 5, 6)),
                result)
        }
    }

    @Test
    fun multiDimensionalInputWithEmptyArrays() {
        parseAPLExpression("0 3 3 ⫇ 3 3 ⍴ 1 2 3 4 5 6 7 8 9").let { result ->
            assert1DArray(
                arrayOf(
                    InnerArray(dimensionsOfSize(1, 3), arrayOf(1, 2, 3)),
                    InnerArray(dimensionsOfSize(0, 3), emptyArray()),
                    InnerArray(dimensionsOfSize(0, 3), emptyArray()),
                    InnerArray(dimensionsOfSize(2, 3), arrayOf(4, 5, 6, 7, 8, 9))),
                result)
        }
    }

    @Test
    fun multiDimensionalLeftArg0() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("(2 2 ⍴ 0 1 2 3) ⫇ 1 2 3 4")
        }
    }

    @Test
    fun multiDimensionalLeftArg1() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("(2 2 ⍴ 0 1 2 3) ⫇ 2 4 ⍴ 1 2 3 4")
        }
    }

    @Test
    fun leftArgScalarExtension() {
        parseAPLExpression("0 ⫇ (,10)").let { result ->
            assert1DArray(arrayOf(Inner1Dn(10)), result)
        }
    }

    @Test
    fun rightArgScalarExtension() {
        parseAPLExpression("(,0) ⫇ 10").let { result ->
            assert1DArray(arrayOf(Inner1Dn(10)), result)
        }
    }

    @Test
    fun arraySizeMismatch0() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("0 1 0 ⫇ 100 200 300 400")
        }
    }

    @Test
    fun arraySizeMismatch1() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("0 1 0 0 ⫇ 100 200 300")
        }
    }

    @Test
    fun rightArgHigherDimension0() {
        parseAPLExpression("1 1 0 ⫇ 3 3 2 ⍴ ⍳18").let { result ->
            assertDimension(dimensionsOfSize(2), result)
            result.valueAt(0).let { v ->
                assertDimension(dimensionsOfSize(1, 3, 2), v)
                assertArrayContent(arrayOf(12, 13, 14, 15, 16, 17), v)
            }
            result.valueAt(1).let { v ->
                assertDimension(dimensionsOfSize(2, 3, 2), v)
                assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11), v)
            }
        }
    }

    @Test
    fun rightArgHigherDimension1() {
        parseAPLExpression("2 3 3 ⫇ 3 3 2 ⍴ ⍳18").let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertDimension(dimensionsOfSize(0, 3, 2), result.valueAt(0))
            assertDimension(dimensionsOfSize(0, 3, 2), result.valueAt(1))
            result.valueAt(2).let { v ->
                assertDimension(dimensionsOfSize(1, 3, 2), v)
                assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5), v)
            }
            result.valueAt(3).let { v ->
                assertDimension(dimensionsOfSize(2, 3, 2), v)
                assertArrayContent(arrayOf(6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17), v)
            }
        }
    }
}
