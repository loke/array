package com.dhsdevelopments.kap.gui.arrayedit

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.gui.Client
import javafx.beans.value.ChangeListener
import javafx.util.StringConverter
import org.controlsfx.control.spreadsheet.*

class KapValueSpreadsheetCellType(client: Client) : SpreadsheetCellType<APLValueSpreadsheetCell>(KapValueStringConverter(client)) {
    override fun toString(obj: APLValueSpreadsheetCell, format: String?): String {
        return when (format) {
            null -> converter.toString(obj)
            CellRendererFormats.BLANK_FORMAT -> ""
            CellRendererFormats.STRING_FORMAT -> obj.renderer.nodeAsString(obj.value)
            else -> error("Invalid format: ${format}")
        }
    }

    override fun toString(obj: APLValueSpreadsheetCell): String {
        return converter.toString(obj)
    }

    override fun createEditor(view: SpreadsheetView): SpreadsheetCellEditor {
        return KapValueEditor(view)
    }

    override fun match(value: Any, vararg options: Any): Boolean {
        if (value !is String) {
            KapLogger.i { "Attempt to match non-string value" }
            return false
        }
        val result = (converter as KapValueStringConverter).fromStringOrNull(value)
        return result != null
    }

    override fun convertValue(value: Any?): APLValueSpreadsheetCell? {
        return when (value) {
            null -> null
            is APLValueSpreadsheetCell -> value
            else -> converter.fromString(value.toString())
        }
    }

    fun createCell(client: Client, value: MutableAPLValue, row: Int, col: Int, index: Int): SpreadsheetCell {
        fun updateCell(cell: SpreadsheetCellBase) {
            val e = (cell.item as APLValueSpreadsheetCell)
            val renderer = e.renderer
            if (renderer.preferString(e.value)) {
                cell.format = CellRendererFormats.STRING_FORMAT
                cell.graphic = null
            } else {
                cell.format = CellRendererFormats.BLANK_FORMAT
                cell.graphic = renderer.makeNode(e.value)
            }
        }

        return SpreadsheetCellBase(row, col, 1, 1, this).apply {
            item = APLValueSpreadsheetCell(client, value.elements[index])
            updateCell(this)
            val l = ChangeListener<Any> { observable, oldValue, newValue ->
                value.elements[index] =
                    if (newValue == null) APLNullValue else (newValue as APLValueSpreadsheetCell).value
                updateCell(this)
            }
            itemProperty().addListener(l)
        }
    }
}

object CellRendererFormats {
    const val BLANK_FORMAT = "blank"
    const val STRING_FORMAT = "string"
}

class KapValueStringConverter(val client: Client) : StringConverter<APLValueSpreadsheetCell>() {
    override fun toString(obj: APLValueSpreadsheetCell): String {
        return obj.value.formatted(FormatStyle.READABLE)
    }

    fun fromStringOrNull(string: String): APLValue? {
        val tokeniserEngine = Engine()
        try {
            val tokeniser = TokenGenerator(tokeniserEngine, StringSourceLocation(string))
            try {
                val token = tokeniser.nextToken()
                if (token == EndOfFile) {
                    return APLString.EMPTY_STRING
                }
                tokeniser.nextToken().let { next ->
                    if (next != EndOfFile) {
                        return APLString.make(string)
                    }
                }
                if (token is ConstantToken) {
                    return token.parsedValue()
                } else {
                    return APLString.make(string)
                }
            } catch (_: ParseException) {
                return APLString.make(string)
            }
        } finally {
            tokeniserEngine.close()
        }
    }

    override fun fromString(string: String): APLValueSpreadsheetCell {
        val result = fromStringOrNull(string) ?: throw IllegalStateException("Unable convert value from string")
        return APLValueSpreadsheetCell(client, result)
    }
}

class APLValueSpreadsheetCell(client: Client, val value: APLValue) {
    val renderer = client.objectViewerRegistry.findRendererProvider(value)

    override fun toString(): String {
        return renderer.nodeAsString(value)
    }
}
