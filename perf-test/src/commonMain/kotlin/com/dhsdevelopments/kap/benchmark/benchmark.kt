package com.dhsdevelopments.kap.benchmark

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.csv.CsvWriter
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.time.Duration
import kotlin.time.measureTime

class BenchmarkTestCase(val name: String, val src: String)

private fun benchmarkPrimes(): BenchmarkTestCase {
    val srcString = """
            |+/ (⍳N) /⍨ {~0∊⍵|⍨1↓1+⍳√⍵}¨ ⍳N←200000
        """.trimMargin()
    // N←1000
    // Default: 0.548
    // Specialised find result value: 0.072
    // N←4000
    // With previous opt: 4.191
    return BenchmarkTestCase("primes", srcString)
}

private fun benchmarkVarLookupScope(): BenchmarkTestCase {
    // Pre-rewrite: 1.3536
    // Orig: 1.2875
    // removed redundant: 1.0207
    // New stack: 0.9316
    // Storage list in array: 0.9357000000000001
    // Standalone stack allocation: 0.9074
    return BenchmarkTestCase("var lookup scope", "{ a←⍵ ◊ {a+⍺+⍵}/⍳1000000 } 4")
}

private fun contribBench(): BenchmarkTestCase {
    // Basic bignum: 0.3519
    return BenchmarkTestCase("contrib bench", "+/{+/⍵(⍵+1)}¨⍳1000000")
}

private fun simpleSum(): BenchmarkTestCase {
    // Basic bignum: 0.36469999999999997
    return BenchmarkTestCase("simple sum", "+/⍳100000000")
}

private fun benchmarkMultipleCall(): BenchmarkTestCase {
    val srcString = """
            |f ⇐ {⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵+⍵}
            |({f 5}⍣200000) 0
        """.trimMargin()
    // Pre-rewrite: 3.4658
    // Orig: 5.375100000000001
    // removed redundant lookup: 3.7815
    // New stack: 3.3473
    // Storage list in array: 3.2721
    return BenchmarkTestCase("multiple call", srcString)
}

private fun benchmarkFormatter(): BenchmarkTestCase {
    val srcString = """
        |use("output3.kap")
        |o3:format 200 20 ⍴ 10 100 "foo" (2 2 ⍴ ⍳10)
    """.trimMargin()
    return BenchmarkTestCase("formatter", srcString)
}

private fun benchmarkDecode(): BenchmarkTestCase {
    val srcString =
        """
        |(⊂60 ⍴ 2) ⊤¨ (10000 ⍴ 987654321098765432)
        """.trimMargin()
    return BenchmarkTestCase("decode", srcString)
}

// iterateOverReshaped: avg=117.675, median=119.5, min=117, max=121, stddev=0.905193349511584
// with comp:
// iterateOverReshaped: avg=187.575, median=184.0, min=183, max=214, stddev=5.513109376749203
// boolean check optimisation:
// iterateOverReshaped: avg=182.875, median=184.0, min=181, max=185, stddev=0.8714212528966688
private fun benchmarkIterateOverReshape(): BenchmarkTestCase {
    val srcString =
        """
        |+/ comp 100000000 ⍴ 1 2
        """.trimMargin()
    return BenchmarkTestCase("iterateOverReshaped", srcString)
}

class TestCaseResults(val name: String, val results: List<Duration>) {
    val resultsInMillis = results.map(Duration::inWholeMilliseconds)

    fun avg() = resultsInMillis.sum() / results.size.toDouble()
    fun max() = resultsInMillis.max()
    fun min() = resultsInMillis.min()

    fun median(): Double {
        return (resultsInMillis[(results.size - 1) / 2] + resultsInMillis[resultsInMillis.size / 2]) / 2.0
    }

    fun stddev(): Double {
        val avg = avg()
        return sqrt(resultsInMillis.sumOf { v -> (v - avg).pow(2) } / resultsInMillis.size)
    }

    fun summary(): String {
        return "avg=${avg()}, median=${median()}, min=${min()}, max=${max()}, stddev=${stddev()}"
    }
}

class BenchmarkResult(val name: String, testcases: List<BenchmarkTestCase>)

fun benchmarkSrc(name: String, srcString: String, libPath: String): TestCaseResults {
    val engine = Engine()
    try {
        engine.addLibrarySearchPath(libPath)
        engine.parseAndEval(StringSourceLocation("use(\"standard-lib.kap\")"))
        val warmupIterations = 40
        val iterations = 40
        val results = ArrayList<Duration>()
        repeat(warmupIterations + iterations) { i ->
            val elapsed = measureTime {
                val result = engine.parseAndEval(StringSourceLocation(srcString))
                result.collapse()
            }
            if (i >= warmupIterations) {
                results.add(elapsed)
            }
//        println("Result${if (i < warmupIterations) " (warmup)" else ""}: ${elapsed}")
        }
        return TestCaseResults(name, results)
    } finally {
        engine.close()
    }
}

fun runAllTests(name: String, libPath: String, reportPath: String, reportName: String): BenchmarkResult {
    val type = fileType(reportPath)
    if (type == null) {
        createDirectory(reportPath)
    } else if (type != FileNameType.DIRECTORY) {
        throw IllegalStateException("Report directory is a file: ${reportPath}")
    }

    val tests = listOf(
        //benchmarkPrimes(), benchmarkVarLookupScope(), contribBench(), simpleSum(), benchmarkMultipleCall(), benchmarkFormatter(), benchmarkDecode())
        benchmarkIterateOverReshape())
    println("Running tests: ${name}")
    val results = ArrayList<TestCaseResults>()
    tests.forEach { testcase ->
        val result = benchmarkSrc(testcase.name, testcase.src, libPath)
        println("${testcase.name}: ${result.summary()}")
        results.add(result)
    }

    openOutputCharFile("${reportPath}/benchmark-${reportName}-${name}.csv").use { output ->
        val writer = CsvWriter(output)
        results.forEach { testcase ->
            val row = arrayListOf(testcase.name)
            row.addAll(testcase.results.map { tm -> tm.inWholeMilliseconds.toString() })
            writer.writeRow(row)
        }
    }

    return BenchmarkResult(name, tests)
}
