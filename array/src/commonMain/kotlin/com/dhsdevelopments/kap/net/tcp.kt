package com.dhsdevelopments.kap.net

import com.dhsdevelopments.kap.ByteConsumer
import com.dhsdevelopments.kap.ByteProvider
import com.dhsdevelopments.kap.MPFileException
import com.dhsdevelopments.kap.NativeCloseable

class MPNetworkException(message: String, cause: Throwable? = null) : MPFileException(message, cause)

interface NetworkApi {
    fun connect(host: String, port: Int): TcpConnection
    fun startListener(port: Int, bindAddress: String? = null): TcpServerSocket

    fun hostname(): String
}

interface TcpConnection : NativeCloseable {
    val sockaddr: TcpSockaddr
    val byteProvider: ByteProvider
    val byteConsumer: ByteConsumer
}

interface TcpServerSocket : NativeCloseable {
    fun accept(): TcpConnection
}

interface TcpSockaddr {
    val host: String
    val port: Int
}

expect fun networkApi(): NetworkApi?
