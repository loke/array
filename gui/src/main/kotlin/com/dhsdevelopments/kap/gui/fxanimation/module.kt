package com.dhsdevelopments.kap.gui.fxanimation

import com.dhsdevelopments.kap.*
import javafx.application.Platform

class FxAnimationModule2 : KapModule {
    override val name get() = "fx-animation"

    private var currContent: AnimationPanel? = null

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("animation")
        engine.registerFunction(ns.internAndExport("run"), FxAnimationRunFunction(this))
    }

    override fun close() {
        Platform.runLater {
            currContent?.close()
        }
    }

    fun withAnimationContent(fn: (AnimationPanel) -> Unit) {
        Platform.runLater {
            val content = currContent ?: AnimationPanel().also { currContent = it }
            content.show()
            fn(content)
        }
    }
}

private class FxAnimationRunFunction(val module: FxAnimationModule2) : APLFunctionDescriptor {
    inner class FxAnimationRunFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        private val translateSym = pos.engine.keywordNamespace.internSymbol("translate")
        private val fadeSym = pos.engine.keywordNamespace.internSymbol("fade")
        private val alignSym = pos.engine.keywordNamespace.internSymbol("align")
        private val leftSym = pos.engine.keywordNamespace.internSymbol("left")
        private val centreSym = pos.engine.keywordNamespace.internSymbol("centre")
        private val rightSym = pos.engine.keywordNamespace.internSymbol("right")
        private val fontSym = pos.engine.keywordNamespace.internSymbol("font")
        private val sizeSym = pos.engine.keywordNamespace.internSymbol("size")
        private val delaySym = pos.engine.keywordNamespace.internSymbol("delay")
        private val colourSym = pos.engine.keywordNamespace.internSymbol("colour")

        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            if (a.dimensions.size != 1) {
                throwAPLException(InvalidDimensionsException("Animation definition must be a 1-dimensional list. Dimensions of argument: ${a.dimensions}", pos))
            }
            val elements = ArrayList<TextElement>()
            a.iterateMembersWithPosition { objDefinition, i ->
                // Each definition is a list of the following form:
                //   "Text" (x y) ...params
                val d = objDefinition.dimensions
                if (d.size != 1) {
                    throwAPLException(
                        InvalidDimensionsException(
                            "Each entry in animation definition must be a 1-dimensional list. Element ${i} dimensions: ${d}",
                            pos))
                }
                if (d[0] < 2) {
                    throwAPLException(
                        InvalidDimensionsException(
                            "Elements in animation definition should have a size of at least 2. Element ${i} is of size: ${d[0]}",
                            pos))
                }
                val text = objDefinition.valueAt(0).toStringValue(pos) { "Element ${i} in animation definition" }
                val (x, y) = parseCoord(objDefinition.valueAt(1))

                val element = TextElement(text, x, y)

                var i = 2
                while (i < d[0]) {
                    when (val kw = objDefinition.valueAt(i++).ensureSymbol(pos).value) {
                        translateSym -> {
                            element.translateFrom = parseCoord(objDefinition.valueAt(i++))
                        }
                        fadeSym -> {
                            element.fadeTime = objDefinition.valueAt(i++).ensureNumber(pos).asDouble(pos).also { t ->
                                if (t < 0) {
                                    throwAPLException(APLEvalException("Fade time cannot be less than 0. Got: ${t}", pos))
                                }
                            }
                        }
                        alignSym -> {
                            element.alignment = when (val name = objDefinition.valueAt(i++).ensureSymbol(pos).value) {
                                leftSym -> TextElement.Alignment.Left
                                centreSym -> TextElement.Alignment.Centre
                                rightSym -> TextElement.Alignment.Right
                                else -> throwAPLException(APLIllegalArgumentException("Unexpected alignment type: ${name.nameWithNamespace}"))
                            }
                        }
                        fontSym -> {
                            element.fontName = objDefinition.valueAt(i++).toStringValue(pos)
                        }
                        sizeSym -> {
                            val size = objDefinition.valueAt(i++).ensureNumber(pos).asDouble(pos)
                            if (size < 0) {
                                throwAPLException(APLIllegalArgumentException("Size may not be negative. Got: ${size}", pos))
                            }
                            element.fontSize = size
                        }
                        delaySym -> {
                            val time = objDefinition.valueAt(i++).ensureNumber(pos).asDouble(pos)
                            if (time < 0) {
                                throwAPLException(APLIllegalArgumentException("Delay is negative. Got: ${time}", pos))
                            }
                            element.delay = time
                        }
                        colourSym -> {
                            element.colour = parseColour(objDefinition.valueAt(i++))
                        }
                        else -> throwAPLException(APLEvalException("Unknown animation keyword: ${kw.nameWithNamespace}", pos))
                    }
                }

                elements.add(element)
            }

            module.withAnimationContent { panel ->
                panel.drawContent(elements)
            }

            return APLNullValue
        }

        private fun parseCoord(a: APLValue): Pair<Double, Double> {
            val d = a.dimensions
            if (d.size != 1 || d[0] != 2) {
                throwAPLException(InvalidDimensionsException("Coordinate list should be a single-dimensional array with 2 elements. Got dimensions: ${d}"))
            }
            val x = a.valueAt(0).ensureNumber(pos).asDouble(pos)
            val y = a.valueAt(1).ensureNumber(pos).asDouble(pos)
            return Pair(x, y)
        }

        private fun parseColour(a: APLValue): TextElement.ColourDefinition {
            if (!a.dimensions.compareEquals(dimensionsOfSize(3))) {
                throwAPLException(APLIllegalArgumentException("Colour definition should be an array of 3 elements. Got dimensions: ${a.dimensions}", pos))
            }
            val red = a.valueAtCoerceToDouble(0, pos)
            val green = a.valueAtCoerceToDouble(1, pos)
            val blue = a.valueAtCoerceToDouble(2, pos)
            return TextElement.ColourDefinition(red, green, blue)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FxAnimationRunFunctionImpl(instantiation)
}
