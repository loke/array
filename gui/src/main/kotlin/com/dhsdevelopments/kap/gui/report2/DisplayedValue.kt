package com.dhsdevelopments.kap.gui.report2

import com.dhsdevelopments.kap.*

abstract class DisplayedValue(
    val reportClient: ReportClient,
    engine: Engine,
    val storage: VariableHolder,
    val name: Symbol,
    var row: Int,
    var col: Int,
    var value: APLValue? = null,
    var nRows: Int = 0,
    var nCols: Int = 0
) {
    abstract val displayedRowCount: Int
    abstract val displayedColCount: Int

    var initialised: Boolean = false

    init {
        storage.registerListener(engine) { newValue, _ ->
            processUpdate(newValue.collapse())
        }
    }

    abstract fun processUpdate(v0: APLValue)

    override fun toString() = name.nameWithNamespace

    class DimensionsAndLabels(newContent: APLValue, rowLabels: Boolean = false, colLabels: Boolean = false) {
        val nRowsUpdated: Int
        val nColsUpdated: Int
        val rowLabelsList: List<String>?
        val colLabelsList: List<String>?
        val dimensions: Dimensions

        init {
            dimensions = newContent.dimensions

            require(dimensions.size <= 2) { "Invalid rank for value: ${dimensions.size}" }

            when (dimensions.size) {
                0 -> {
                    nRowsUpdated = 1
                    nColsUpdated = 1
                    rowLabelsList = null
                    colLabelsList = null
                }
                1 -> {
                    nRowsUpdated = 1
                    nColsUpdated = dimensions[0]
                    rowLabelsList = if (rowLabels) listOf("0") else null
                    colLabelsList = if (colLabels) newContent.labelsAsStrings(0) else null
                }
                2 -> {
                    nRowsUpdated = dimensions[0]
                    nColsUpdated = dimensions[1]
                    rowLabelsList = if (rowLabels) newContent.labelsAsStrings(0) else null
                    colLabelsList = if (colLabels) newContent.labelsAsStrings(1) else null
                }
                else -> throw IllegalArgumentException("Only rank 0-2 supported. Attempt to add value of rank: ${dimensions.size}")
            }
        }
    }
}
