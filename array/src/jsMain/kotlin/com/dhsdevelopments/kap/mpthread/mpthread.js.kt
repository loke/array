package com.dhsdevelopments.kap.mpthread

import kotlin.reflect.KClass

actual fun startThread(fn: (MPThread) -> Any?): MPThread {
    throw ThreadsNotSupportedException()
}

actual val backendSupportsThreading: Boolean get() = false

class JsAtomicRefArray<T>(size: Int) : MPAtomicRefArray<T> {
    private val content: MutableList<T?>

    init {
        content = ArrayList()
        repeat(size) {
            content.add(null)
        }
    }

    override fun get(index: Int) = content[index]

    override fun compareAndExchange(index: Int, expected: T?, newValue: T?): T? {
        val v = content[index]
        if (v == expected) {
            content[index] = newValue
        }
        return v
    }
}

actual fun <T> makeAtomicRefArray(size: Int): MPAtomicRefArray<T> {
    return JsAtomicRefArray(size)
}

actual fun <T : Any> makeMPThreadLocalBackend(type: KClass<T>): MPThreadLocal<T> {
    return object : MPThreadLocal<T> {
        override var value: T? = null
    }
}

actual class MPLock actual constructor(recursive: Boolean) {
    actual fun makeCondVar(): MPCondVar = error("JS backend does not support condvars")
}

actual inline fun <T> MPLock.withLocked(fn: () -> T): T {
    return fn()
}
