package com.dhsdevelopments.kap

import kotlin.test.Test

class FilesystemTest : APLTest() {
    @Test
    fun writeFileWithStreamApi() {
        val src =
            """
            |f ← :output io2:open "/foo"
            |f io2:write "abc\n"
            |f io2:write "line2\n"
            |close f
            |f ← :input io2:open "/foo"
            |io2:readLine f
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertString("abc", result)
        }
    }
}
