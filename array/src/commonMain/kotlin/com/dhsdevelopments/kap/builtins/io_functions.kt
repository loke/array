package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.csv.CsvParseException
import com.dhsdevelopments.kap.csv.readCsv
import com.dhsdevelopments.kap.csv.writeAPLArrayAsCsv

inline fun <reified T> ensureType(a: APLValue, typename: String, pos: Position): T {
    val a0 = a.unwrapDeferredValue()
    if (a0 !is T) {
        throwAPLException(APLIllegalArgumentException("Argument is not of type: ${typename}, got: ${a0::class.simpleName}", pos))
    }
    return a0
}

/**
 * Evaluate [fn], catching any exception of type [MPFileNotFoundException] and throw a tag with name `fileNotFound` instead.
 */
inline fun <T> withIOExceptionConversions(context: RuntimeContext, name: String, pos: Position, fn: () -> T): T {
    try {
        return fn()
    } catch (_: MPFileNotFoundException) {
        throwTagCatch(context.engine, "fileNotFound", APLString.make(name), "File not found: ${name}", pos)
    }
}

abstract class KotlinObjectWrappedValue<T>(val value: T) : APLSingleValue() {
    override val kapClass: KapClass get() = SystemClass.INTERNAL

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) =
        reference is KotlinObjectWrappedValue<*> && reference.value == value

    override fun typeQualifiedHashCode() = value.hashCode()
}

class ReadFunction : APLFunctionDescriptor {
    class ReadFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val file = a.toStringValue(pos)
            val result = ArrayList<APLValue>()
            withIOExceptionConversions(context, file, pos) {
                openInputCharFile(context.engine.resolvePathName(file)).use { provider ->
                    provider.lines().forEach { s ->
                        result.add(APLString.make(s))
                    }
                }
                return APLArrayList(dimensionsOfSize(result.size), result)
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ReadFunctionImpl(instantiation)
}

class WriteFunction : APLFunctionDescriptor {
    class WriteFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val b0 = b.collapse()
            val file = a.toStringValue(pos)
            openOutputCharFile(file).use { output ->
                output.writeString(b0.formatted(FormatStyle.PLAIN))
            }
            return b0
        }
    }

    override fun make(instantiation: FunctionInstantiation) = WriteFunctionImpl(instantiation)
}

class APLCharInputStream(prov: CharacterProvider) : KotlinObjectWrappedValue<CharacterProvider>(prov) {
    override val kapClass get() = SystemClass.INTERNAL
    override fun formatted(style: FormatStyle) = name

    fun close() {
        value.close()
    }

    companion object {
        val name = "char input stream"
        fun ensureStream(a: APLValue, pos: Position): APLCharInputStream = ensureType(a, name, pos)
    }
}

class APLCharOutputStream(prov: CharacterConsumer) : KotlinObjectWrappedValue<CharacterConsumer>(prov) {
    override val kapClass get() = SystemClass.INTERNAL
    override fun formatted(style: FormatStyle) = name

    fun close() {
        value.close()
    }

    companion object {
        val name = "char output stream"
        fun ensureStream(a: APLValue, pos: Position): APLCharOutputStream = ensureType(a, name, pos)
    }
}

class APLBinaryInputStream(prov: ByteProvider) : KotlinObjectWrappedValue<ByteProvider>(prov) {
    override val kapClass get() = SystemClass.INTERNAL
    override fun formatted(style: FormatStyle) = name

    fun close() {
        value.close()
    }

    companion object {
        val name = "binary input stream"
        fun ensureStream(a: APLValue, pos: Position): APLBinaryInputStream = ensureType(a, name, pos)
    }
}

class APLBinaryOutputStream(prov: ByteConsumer) : KotlinObjectWrappedValue<ByteConsumer>(prov) {
    override val kapClass get() = SystemClass.INTERNAL
    override fun formatted(style: FormatStyle) = name

    fun close() {
        value.close()
    }

    companion object {
        val name = "binary output stream"
        fun ensureStream(a: APLValue, pos: Position): APLBinaryOutputStream = ensureType(a, name, pos)
    }
}

class OpenFunction : APLFunctionDescriptor {
    class OpenFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val file = a.toStringValue(pos)
            return openStream(context.engine, file, OpenOptions(StreamMode.INPUT, StreamType.UTF8))
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val file = b.toStringValue(pos)
            val options = parseOptions(context, a, pos)
            return openStream(context.engine, file, options)
        }

        private fun openStream(engine: Engine, file: String, options: OpenOptions): APLValue {
            try {
                val file = engine.resolvePathName(file)
                return when (options.mode) {
                    StreamMode.INPUT -> when (options.contentType) {
                        StreamType.UTF8 -> APLCharInputStream(openInputCharFile(file))
                        StreamType.BINARY -> APLBinaryInputStream(openInputFile(file))
                    }
                    StreamMode.OUTPUT -> when (options.contentType) {
                        StreamType.UTF8 -> APLCharOutputStream(openOutputCharFile(file))
                        StreamType.BINARY -> APLBinaryOutputStream(openOutputFile(file))
                    }
                }
            } catch (e: MPFileException) {
                throwAPLException(APLEvalException("Error opening file: ${e.message}", pos, e))
            }
        }

        private fun parseOptions(context: RuntimeContext, a: APLValue, pos: Position): OpenOptions {
            fun throwDuplicateMode(): Nothing =
                throwAPLException(APLIllegalArgumentException("Duplicate mode argument", pos))

            val a0 = a.arrayify()
            if (a0.dimensions.size != 1) {
                throwAPLException(InvalidDimensionsException("Options should be a scalar or a 1-dimensional array", pos))
            }
            var mode: StreamMode? = null
            var type: StreamType? = null
            val inputKeyword = context.engine.keywordNamespace.internAndExport("input")
            val outputKeyword = context.engine.keywordNamespace.internAndExport("output")
            val utf8Keyword = context.engine.keywordNamespace.internAndExport("text")
            val binaryKeyword = context.engine.keywordNamespace.internAndExport("binary")
            a0.iterateMembers { v ->
                if (v is APLSymbol) {
                    when (v.value) {
                        inputKeyword -> if (mode != null) throwDuplicateMode() else mode = StreamMode.INPUT
                        outputKeyword -> if (mode != null) throwDuplicateMode() else mode = StreamMode.OUTPUT
                        utf8Keyword -> if (type != null) throwDuplicateMode() else type = StreamType.UTF8
                        binaryKeyword -> if (type != null) throwDuplicateMode() else type = StreamType.BINARY
                    }
                }
            }
            return OpenOptions(mode ?: StreamMode.INPUT, type ?: StreamType.UTF8)
        }
    }

    private class OpenOptions(val mode: StreamMode, val contentType: StreamType)

    private enum class StreamMode { INPUT, OUTPUT }
    private enum class StreamType { UTF8, BINARY }

    override fun make(instantiation: FunctionInstantiation) = OpenFunctionImpl(instantiation)
}

class ReadFromStreamFunction : APLFunctionDescriptor {
    class ReadFromStreamFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            return when (val a0 = a.unwrapDeferredValue()) {
                is APLCharInputStream -> readText(a0.value)
                is APLBinaryInputStream -> readBinary(a0.value)
                else -> throwAPLException(APLIllegalArgumentException("Expected input stream, got: ${a0::class.simpleName}"))
            }
        }

        private fun readText(f: CharacterProvider): APLValue {
            val result = ArrayList<APLValue>()
            f.lines().forEach { s ->
                result.add(APLString.make(s))
            }
            return APLArrayList(dimensionsOfSize(result.size), result)
        }

        private fun readBinary(f: ByteProvider): APLValue {
            val result = ByteArrayByteConsumer()
            val buf = ByteArray(1024 * 16)
            while (true) {
                val n = f.readBlock(buf, 0, buf.size)
                if (n == 0) break
                result.writeBlock(buf, 0, n)
            }
            val content = result.content()
            return APLArrayByte(dimensionsOfSize(content.size), content)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ReadFromStreamFunctionImpl(instantiation)
}

class ReadLineFromStreamFunction : APLFunctionDescriptor {
    class ReadLineFromStreamFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val f = APLCharInputStream.ensureStream(a, pos)
            val line = f.value.nextLine()
            return if (line == null) {
                APLNilValue
            } else {
                APLString.make(line)
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ReadLineFromStreamFunctionImpl(instantiation)
}

class WriteToStreamFunction : APLFunctionDescriptor {
    class WriteToStreamFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val b0 = b.arrayify()
            ensureValidDimensions(b0.dimensions)
            when (val a0 = a.unwrapDeferredValue()) {
                is APLCharOutputStream -> writeText(a0.value, b0)
                is APLBinaryOutputStream -> writeBinary(a0.value, b0)
                else -> throwAPLException(APLIllegalArgumentException("Expected output stream, got: ${a0::class.simpleName}"))
            }
            return b0
        }

        override fun eval2ArgDiscardResult(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?) {
            ensureAxisNull(axis)
            ensureValidDimensions(b.dimensions)
            when (val a0 = a.unwrapDeferredValue()) {
                is APLCharOutputStream -> writeText(a0.value, b)
                is APLBinaryOutputStream -> writeBinary(a0.value, b)
                else -> throwAPLException(APLIllegalArgumentException("Expected output stream, got: ${a0::class.simpleName}"))
            }
        }

        private fun ensureValidDimensions(d: Dimensions) {
            if (d.size != 1) {
                throwAPLException(InvalidDimensionsException("Argument must be a scalar or a 1-dimensional array, got: ${d}"))
            }
        }

        private fun writeText(f: CharacterConsumer, b: APLValue) {
            f.writeString(b.toStringValue(pos))
        }

        private fun writeBinary(f: ByteConsumer, b: APLValue) {
            b.asByteArray(pos).forEach(f::writeByte)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = WriteToStreamFunctionImpl(instantiation)
}


class PrintAPLFunction : APLFunctionDescriptor {
    class PrintAPLFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val a0 = a.collapse()
            printValue(context, a0, FormatStyle.PLAIN)
            return a0
        }

        override fun eval1ArgDiscardResult(context: RuntimeContext, a: APLValue, axis: APLValue?) {
            printValue(context, a, FormatStyle.PLAIN)
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val b0 = b.collapse()
            val engine = context.engine
            val plainSym = engine.internSymbol("plain", engine.keywordNamespace)
            val prettySym = engine.internSymbol("pretty", engine.keywordNamespace)
            val readSym = engine.internSymbol("read", engine.keywordNamespace)

            val style = when (val styleName = a.ensureSymbol().value) {
                plainSym -> FormatStyle.PLAIN
                prettySym -> FormatStyle.PRETTY
                readSym -> FormatStyle.READABLE
                else -> throwAPLException(APLIllegalArgumentException("Invalid print style: ${styleName.symbolName}", pos))
            }
            printValue(context, b0, style)
            return b0
        }
    }

    override fun make(instantiation: FunctionInstantiation) = PrintAPLFunctionImpl(instantiation)
}

class PrintLnAPLFunction : APLFunctionDescriptor {
    class PrintLnAPLFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val a0 = a.collapse()
            printValue(context, a0, FormatStyle.PLAIN)
            context.engine.standardOutput.writeString("\n")
            return a0
        }

        override fun eval1ArgDiscardResult(context: RuntimeContext, a: APLValue, axis: APLValue?) {
            printValue(context, a, FormatStyle.PLAIN)
            context.engine.standardOutput.writeString("\n")
        }
    }

    override fun make(instantiation: FunctionInstantiation) = PrintLnAPLFunctionImpl(instantiation)
}

private fun printValue(context: RuntimeContext, a: APLValue, style: FormatStyle) {
    context.engine.standardOutput.writeString(a.formatted(style))
}

class WriteCsvFunction : APLFunctionDescriptor {
    class WriteCsvFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val fileName = a.toStringValue(pos)
            openOutputCharFile(fileName).use { dest ->
                writeAPLArrayAsCsv(dest, b, pos)
            }
            return b
        }
    }

    override fun make(instantiation: FunctionInstantiation) = WriteCsvFunctionImpl(instantiation)
}

class ReadCsvFunction : APLFunctionDescriptor {
    class ReadCsvFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            openInputCharFile(a.toStringValue(pos)).use { source ->
                try {
                    return readCsv(source)
                } catch (e: CsvParseException) {
                    throwAPLException(APLEvalException("Error while parsing CSV: ${e.message}", pos))
                }
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ReadCsvFunctionImpl(instantiation)
}

class ReadFileFunction : APLFunctionDescriptor {
    class ReadFileFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            openInputCharFile(a.toStringValue(pos)).use { source ->
                val buf = StringBuilder()
                while (true) {
                    val ch = source.nextCodepoint() ?: break
                    buf.addCodepoint(ch)
                }
                return APLString.make(buf.toString())
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ReadFileFunctionImpl(instantiation)
}

class LoadFunction : APLFunctionDescriptor {
    class LoadFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val requestedFile = a.toStringValue(pos)
            val engine = context.engine
            val file = engine.resolvePathName(requestedFile)
            return engine.withSavedNamespace {
                withThreadLocalsUnassigned {
                    withIOExceptionConversions(context, file, pos) {
                        engine.parseAndEval(FileSourceLocation(file))
                    }
                }
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = LoadFunctionImpl(instantiation)
}

class ReaddirFunction : APLFunctionDescriptor {
    class ReaddirFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            return loadContent(context, a, emptyList())
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            return loadContent(context, b, parseOutputTypes(context, a))
        }

        private fun loadContent(context: RuntimeContext, file: APLValue, selectors: List<OutputType>): APLValue {
            val content = transformMPFileException(pos) {
                readDirectoryContent(context.engine.resolvePathName(file.toStringValue(pos)))
            }
            val numCols = 1 + selectors.size
            val d = dimensionsOfSize(content.size, numCols)
            val valueList = Array(d.contentSize()) { i ->
                val row = i / numCols
                val col = i % numCols
                val pathEntry = content[row]
                if (col == 0) {
                    APLString.make(pathEntry.name)
                } else {
                    when (selectors[col - 1]) {
                        OutputType.SIZE -> pathEntry.size.makeAPLNumber()
                        OutputType.TYPE -> pathEntryTypeToAPL(context, pathEntry.type)
                    }
                }
            }
            return APLArrayImpl(d, valueList)
        }

        private fun pathEntryTypeToAPL(context: RuntimeContext, type: FileNameType): APLValue {
            val sym = when (type) {
                FileNameType.FILE -> context.engine.internSymbol("file", context.engine.keywordNamespace)
                FileNameType.DIRECTORY -> context.engine.internSymbol("directory", context.engine.keywordNamespace)
                FileNameType.UNDEFINED -> context.engine.internSymbol("undefined", context.engine.keywordNamespace)
            }
            return APLSymbol(sym)
        }

        private fun parseOutputTypes(context: RuntimeContext, value: APLValue): List<OutputType> {
            val keywordToType =
                OutputType.entries.associateBy { outputType ->
                    context.engine.internSymbol(
                        outputType.selector,
                        context.engine.keywordNamespace)
                }

            val result = ArrayList<OutputType>()
            val asArray = value.arrayify()
            if (asArray.dimensions.size != 1) {
                throwAPLException(InvalidDimensionsException("Selector must be a scalar or a rank-1 array", pos))
            }
            asArray.iterateMembers { v ->
                val collapsed = v.collapse()
                if (collapsed !is APLSymbol) {
                    throwAPLException(APLIllegalArgumentException("Selector must be a symbol", pos))
                }
                val found =
                    keywordToType[collapsed.value]
                        ?: throwAPLException(
                            APLIllegalArgumentException(
                                "Illegal selector: ${collapsed.value.nameWithNamespace}",
                                pos))
                result.add(found)
            }
            return result
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ReaddirFunctionImpl(instantiation)

    private enum class OutputType(val selector: String) {
        SIZE("size"),
        TYPE("type")
    }
}

class ToHtmlFunction : APLFunctionDescriptor {
    class ToHtmlFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val buf = StringBuilder()
            a.asHtml(buf)
            return APLString.make(buf.toString())
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ToHtmlFunctionImpl(instantiation)
}

class ReadLineFunction : APLFunctionDescriptor {
    class ReadLineFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val prompt = a.toStringValue(pos)
            val keyboardInput = makeKeyboardInput(context.engine) ?: throwAPLException(APLEvalException("Backend does not support input", pos))
            val result = keyboardInput.readString(prompt)
            return if (result == null) {
                APLNullValue
            } else {
                APLString(result.asCodepointList().toIntArray())
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ReadLineFunctionImpl(instantiation)
}

fun registerIoClosableHandlers(engine: Engine) {
    engine.registerClosableHandler(ClosableHandler<APLCharInputStream> { value -> value.close() })
    engine.registerClosableHandler(ClosableHandler<APLCharOutputStream> { value -> value.close() })
    engine.registerClosableHandler(ClosableHandler<APLBinaryInputStream> { value -> value.close() })
    engine.registerClosableHandler(ClosableHandler<APLBinaryOutputStream> { value -> value.close() })
}
