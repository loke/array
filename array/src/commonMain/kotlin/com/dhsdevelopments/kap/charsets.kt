package com.dhsdevelopments.kap

expect fun isLetter(codepoint: Int): Boolean
expect fun isDigit(codepoint: Int): Boolean
expect fun isWhitespace(codepoint: Int): Boolean
expect fun charToString(codepoint: Int): String
expect fun nameToCodepoint(name: String): Int?
expect fun codepointToName(codepoint: Int): String?

/**
 * If false, the functions [nameToCodepoint] and [codepointToName] will always return null.
 */
expect val backendSupportsUnicodeNames: Boolean

fun isAlphanumeric(codepoint: Int) = isLetter(codepoint) || isDigit(codepoint)

expect fun StringBuilder.addCodepoint(codepoint: Int): StringBuilder
expect fun String.asCodepointList(): List<Int>
expect fun String.asGraphemeList(): List<String>

fun makeCharFromSurrogatePair(high: Char, low: Char): Int {
    val highInt = high.code
    val lowInt = low.code
    require(highInt in 0xD800..0xDBFF) { "high character is outside valid range: 0x${highInt.toString(16)}" }
    require(lowInt in 0xDC00..0xDFFF) { "low character is outside valid range: 0x${lowInt.toString(16)}" }
    val off = 0x10000 - (0xD800 shl 10) - 0xDC00
    return (highInt shl 10) + lowInt + off
}
