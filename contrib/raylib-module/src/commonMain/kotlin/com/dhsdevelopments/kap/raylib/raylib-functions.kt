package com.dhsdevelopments.kap.raylib

class RaylibApiRoot {
    var defines: List<RaylibDefine>? = null
    var structs: List<RaylibStruct>? = null
    var aliases: List<RaylibAlias>? = null
    var enums: List<RaylibEnum>? = null
    var callbacks: List<RaylibCallback>? = null
    var functions: List<RaylibFunction>? = null
}

class RaylibDefine {
    lateinit var name: String
    lateinit var type: String
    lateinit var value: String
    lateinit var description: String
}

class RaylibStruct {
    lateinit var name: String
    lateinit var description: String
    lateinit var fields: List<RaylibStructField>
}

class RaylibStructField {
    lateinit var type: String
    lateinit var name: String
    lateinit var description: String
}

class RaylibAlias {
    lateinit var type: String
    lateinit var name: String
    lateinit var description: String
}

class RaylibEnum {
    lateinit var name: String
    lateinit var description: String
    lateinit var values: List<RaylibEnumValue>
}

class RaylibEnumValue {
    lateinit var name: String
    var value: Int = -1
    lateinit var description: String
}

class RaylibCallback {
    lateinit var name: String
    lateinit var description: String
    lateinit var returnType: String
    lateinit var params: List<RaylibCallbackParam>
}

class RaylibCallbackParam {
    lateinit var type: String
    lateinit var name: String
}

class RaylibFunction {
    lateinit var name: String
    lateinit var description: String
    lateinit var returnType: String
    var params: List<RaylibFunctionParam>? = null
}

class RaylibFunctionParam {
    lateinit var type: String
    lateinit var name: String

    override fun toString() = "RaylibFunctionParam(type='$type', name='$name')"
}
