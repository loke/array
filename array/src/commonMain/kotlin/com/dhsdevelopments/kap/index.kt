package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.builtins.mapLookupFromAPLValue
import com.dhsdevelopments.kap.builtins.unwrapEnclosedSingleValue

class IndexedArrayValue private constructor(val content: APLValue, indexValue: Array<Either<Int, IntArrayValue>>) : APLArray() {
    override val dimensions: Dimensions
    override val specialisedType get() = content.specialisedType

    private val multipliers: Dimensions.DimensionMultipliers
    private val destToSourceAxis: List<AxisValueAndOffset>
    private val constantOffset: Int

    init {
        val contentMult = content.dimensions.multipliers()

        var offset = 0
        val a = ArrayList<Int>()
        val destAxis = ArrayList<AxisValueAndOffset>()
        var outputAxis = 0
        indexValue.forEachIndexed { i, selection ->
            when (selection) {
                is Either.Left -> {
                    offset += contentMult[i] * selection.value
                }
                is Either.Right -> {
                    selection.value.dimensions.forEach { v ->
                        a.add(v)
                    }
                    destAxis.add(
                        AxisValueAndOffset(
                            outputAxis,
                            selection.value,
                            contentMult[i],
                            i))
                    outputAxis += selection.value.dimensions.size
                }
            }
        }
        dimensions = Dimensions(a.toIntArray())
        multipliers = dimensions.multipliers()
        destToSourceAxis = destAxis
        constantOffset = offset
    }

    override fun valueAt(p: Int) = content.valueAt(computeNewIndex(p))
    override fun valueAtCoerceToInt(p: Int, pos: Position?) = content.valueAtCoerceToInt(computeNewIndex(p), pos)
    override fun valueAtLong(p: Int) = content.valueAtLong(computeNewIndex(p))
    override fun valueAtDouble(p: Int) = content.valueAtDouble(computeNewIndex(p))

    override val metadata by lazy { maybeDefaultMetadata(content.metadata) { m -> IndexedArrayValueMetadata(m, dimensions, destToSourceAxis) } }

    private fun computeNewIndex(p: Int): Int {
        // If the array is 1-dimensional, we can use an optimised code path that doesn't create temporary index arrays.
        // This gives more than 2x performance increase on code that uses 1-dimensional array indexes a lot.
        if (content.dimensions.size == 1 && dimensions.size == 1) {
            val srcAxisPos = destToSourceAxis[0].source.intValueAt(p)
            return srcAxisPos + constantOffset
        } else {
            val positionArray = multipliers.positionFromIndex(p)
            var result = constantOffset
            destToSourceAxis.forEach { dts ->
                val srcCoords = IntArray(dts.source.rank) { i -> positionArray[dts.sourceIndex + i] }
                val srcAxisPos = dts.source.intValueAt(dts.source.dimensions.indexFromPosition(srcCoords))
                result += srcAxisPos * dts.multiplier
            }
            return result
        }
    }

    override fun unwrapDeferredValue(): APLValue {
        return unwrapEnclosedSingleValue(this)
    }

    companion object {
        fun make(content: APLValue, indexValue: Array<Either<Int, IntArrayValue>>): APLValue {
            val d = content.dimensions
            require(indexValue.size == d.size)
            if (indexValue.all { v -> v is Either.Left }) {
                val coords = IntArray(indexValue.size) { i -> (indexValue[i] as Either.Left).value }
                return EnclosedAPLValue.make(content.valueAt(d.indexFromPosition(coords)))
            } else {
                return IndexedArrayValue(content, indexValue)
            }
        }
    }

    class AxisValueAndOffset(
        val sourceIndex: Int,
        val source: IntArrayValue,
        val multiplier: Int,
        val sourceAxis: Int
    )

    class IndexedArrayValueMetadata(
        val sourceMetadata: APLValueMetadata,
        val valueDimensions: Dimensions,
        val destToSourceAxis: List<AxisValueAndOffset>
    ) : APLValueMetadata {
        override val labels by lazy { computeLabels() }

        private fun computeLabels(): DimensionLabels? {
            if (valueDimensions.isEmpty()) {
                return null
            }
            val contentLabels = sourceMetadata.labels ?: return null

            val resultLabels = ArrayList<List<AxisLabel?>?>()
            var resultHasLabels = false
            destToSourceAxis.forEach { dts ->
                val res = when (val numAxisInSource = dts.source.dimensions.size) {
                    0 -> throw IllegalStateException("Source selection should not have 0 dimensions")
                    1 -> {
                        val srcLabelList = contentLabels.labels[dts.sourceAxis]
                        if (srcLabelList == null) {
                            null
                        } else {
                            dts.source.values.map { i -> srcLabelList[i].also { label -> if (label != null) resultHasLabels = true } }
                        }
                    }
                    else -> (0 until numAxisInSource).map { null } // There is no logical way to translate multiple axis labels to a single name
                }
                resultLabels.add(res)
            }
            return if (resultHasLabels) DimensionLabels(resultLabels) else null
        }
    }
}

class ArrayIndex(val content: Instruction, val indexInstr: Instruction, pos: Position) : Instruction(pos) {
    override fun evalWithContext(context: RuntimeContext): APLValue {
        val indexValue = indexInstr.evalWithContext(context)
        val contentValue = content.evalWithContext(context).unwrapDeferredValue()
        val aDimensions = contentValue.dimensions

        return if (contentValue is APLMap) {
            mapLookupFromAPLValue(contentValue, indexValue, pos)
        } else {
            lookupFromArray(indexValue, contentValue, aDimensions)
        }
    }

    override fun children() = listOf(content, indexInstr)
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ArrayIndex(updatedChildList[0], updatedChildList[1], newPos)

    private fun lookupFromArray(
        indexValue: APLValue,
        contentValue: APLValue,
        aDimensions: Dimensions
    ): APLValue {
        fun exceptionDetailsPrefix(s: String) =
            """
            The value being indexed is an array, and the index
            must be a list of indexes specifying which elements to get
            from each axis.
            
            ${s}            
            """.reformatLines()

        val indexAsList = indexValue.listify()
        if (indexAsList.listSize() != contentValue.dimensions.size) {
            throwAPLException(
                InvalidDimensionsException(
                    "Rank of argument does not match index. Argument=${aDimensions.size}, index=${indexAsList.listSize()}",
                    pos)
                    .details(
                        exceptionDetailsPrefix(
                            """
                            In this case the array being indexed has rank ${contentValue.dimensions.size},
                            and therefore the index specification must be a list of this many elements.
                            The index contains ${indexAsList.listSize()} elements, which is not equal to the array rank.
                            """.reformatLines())))
        }

        val axis = Array(indexAsList.listSize()) { i ->
            val axisSize = aDimensions[i]
            indexAsList.listElement(i).unwrapDeferredValue().let { result ->
                when {
                    result.dimensions.size > 0 -> {
                        Either.Right(IntArrayValue.fromAPLValue(result, pos) { v -> Dimensions.checkAndAdjustSelectedIndex(v, axisSize, pos) })
                    }
                    result is APLNilValue -> {
                        Either.Right(IntArrayValue(dimensionsOfSize(axisSize), IntArray(axisSize) { it }))
                    }
                    else -> {
                        val n = result.ensureNumberOrNull() ?: throwAPLException(
                            IncompatibleTypeException("Array index must be an integer or an array of integers. Got: ${result.typeDescription}")
                                .details(
                                    exceptionDetailsPrefix(
                                        """                                    
                                        When checking the indexes for axis ${i}, it was not possible to convert this value to an integer or
                                        an array of integers, since it is of the wrong type.
                                        """.reformatLines())))
                        Either.Left(Dimensions.checkAndAdjustSelectedIndex(n.asInt(pos), axisSize, pos))
                    }
                }
            }
        }

        return IndexedArrayValue.make(contentValue, axis)
    }
}
