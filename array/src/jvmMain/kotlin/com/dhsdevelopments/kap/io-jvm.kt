package com.dhsdevelopments.kap

import java.io.*
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract
import kotlin.math.min

class JvmStringCharacterProvider(private val s: String) : CharacterProvider {
    private var pos = 0

    override fun nextCodepoint(): Int? {
        return if (pos >= s.length) {
            null
        } else {
            val result = s.codePointAt(pos)
            pos = s.offsetByCodePoints(pos, 1)
            result
        }
    }

    override fun close() {}
}

actual fun makeStringCharacterProvider(s: String): CharacterProvider = JvmStringCharacterProvider(s)

class KeyboardInputJvm : KeyboardInput {
    private val reader = BufferedReader(InputStreamReader(System.`in`))

    override fun readString(prompt: String): String? {
        print(prompt)
        return reader.readLine()
    }
}

actual fun makeKeyboardInput(engine: Engine): KeyboardInput? {
    val nativeData = engine.nativeData as JvmNativeData
    return nativeData.keyboardInput
}

class InvalidCharacter : Exception()

class ReaderCharacterProvider(private val reader: Reader, val sourceName: String? = null) : CharacterProvider {
    private var endOfFile = false

    override fun sourceName() = sourceName

    override fun nextCodepoint(): Int? {
        transformIOException {
            if (endOfFile) {
                return null
            }

            val v = reader.read()
            return when {
                v == -1 -> {
                    endOfFile = true
                    null
                }
                Character.isHighSurrogate(v.toChar()) -> {
                    val v2 = reader.read()
                    if (!Character.isLowSurrogate(v2.toChar())) {
                        throw InvalidCharacter()
                    }
                    Character.toCodePoint(v.toChar(), v2.toChar())
                }
                else -> v
            }
        }
    }

    override fun close() {
        reader.close()
    }
}

class CharacterProviderReaderWrapper(val provider: CharacterProvider) : Reader() {
    private val buf = CharArray(10)
    private var bufPos = 0
    private var end = false

    override fun read(cbuf: CharArray, off: Int, len: Int): Int {
        if (end) {
            return -1
        }
        var i = 0
        if (bufPos > 0) {
            val charsToCopy = min(len, bufPos)
            buf.copyInto(cbuf, off, 0, charsToCopy)
            i += charsToCopy
            bufPos = if (charsToCopy < bufPos) {
                val newLength = bufPos - charsToCopy
                buf.copyInto(buf, 0, charsToCopy, charsToCopy + newLength)
                newLength
            } else {
                0
            }
        }
        while (i < len) {
            val value = provider.nextCodepoint() ?: break
            val valueString = charToString(value)
            val charsToCopy = min(valueString.length, len - i)
            var valueIndex = 0
            while (valueIndex < charsToCopy) {
                cbuf[off + i++] = valueString[valueIndex++]
            }
            if (charsToCopy < valueString.length) {
                var bufIndex = 0
                while (valueIndex < valueString.length) {
                    buf[bufIndex++] = valueString[valueIndex++]
                }
                bufPos = bufIndex
            }
        }
        if (i < len) {
            end = true
        }
        return if (i == 0) -1 else i
    }

    override fun close() {
        provider.close()
    }
}

actual fun openInputCharFile(name: String): CharacterProvider {
    transformIOException {
        return ReaderCharacterProvider(BufferedReader(FileReader(name, Charsets.UTF_8)))
    }
}

actual fun openOutputFile(name: String): ByteConsumer {
    transformIOException {
        return OutputStreamByteConsumer(BufferedOutputStream(FileOutputStream(name)))
    }
}

class InputStreamByteProvider(private val input: InputStream) : ByteProvider {
    override fun readByte(): Byte? {
        transformIOException {
            val result = input.read()
            return if (result == -1) null else result.toByte()
        }
    }

    override fun readBlock(buffer: ByteArray, start: Int?, length: Int?): Int {
        val start0 = when {
            start == null -> 0
            start >= 0 && start < buffer.size -> start
            else -> throw IllegalArgumentException("start should be a positive number less than the size of the buffer")
        }
        val length0 = when {
            length == null -> buffer.size - start0
            length > 0 -> length
            else -> throw IllegalArgumentException("length must be greater than 0, got: ${length}")
        }
        transformIOException {
            val result = input.read(buffer, start0, length0)
            require(result != 0) { "Got 0 from read" } // unexpected, since read should only return 0 when requesting a zero-length result
            return if (result == -1) 0 else result
        }
    }

    override fun close() {
        input.close()
    }
}

class ByteProviderInputStream(private val input: ByteProvider) : InputStream() {
    override fun read(): Int {
        transformIOException {
            val result = input.readByte()
            return if (result == null) {
                -1
            } else {
                result.toInt() and 0xFF
            }
        }
    }

    override fun read(b: ByteArray, off: Int, len: Int): Int {
        transformIOException {
            return input.readBlock(b, off, len)
        }
    }
}

class OutputStreamByteConsumer(private val output: OutputStream) : ByteConsumer {
    override fun writeByte(value: Byte) {
        output.write(value.toInt())
    }

    override fun writeBlock(buffer: ByteArray, offset: Int, length: Int?) {
        for (i in offset until offset + (length ?: (buffer.size - offset))) {
            output.write(buffer[i].toInt())
        }
    }

    override fun close() {
        output.close()
    }

    override fun flush() {
        output.flush()
    }
}

actual fun openInputFile(name: String): ByteProvider {
    transformIOException {
        return InputStreamByteProvider(FileInputStream(name))
    }
}

class WriterCharacterConsumer(private val output: Writer) : CharacterConsumer {
    override fun writeString(s: String) {
        output.write(s)
    }

    override fun writeChar(ch: Int) {
        if (Character.isSupplementaryCodePoint(ch)) {
            output.write(Character.highSurrogate(ch).code)
            output.write(Character.lowSurrogate(ch).code)
        } else {
            output.write(ch)
        }
    }

    override fun close() {
        output.close()
    }
}

actual fun openOutputCharFile(name: String): CharacterConsumer {
    transformIOException {
        return WriterCharacterConsumer(FileWriter(name, Charsets.UTF_8))
    }
}

@OptIn(ExperimentalContracts::class)
inline fun <T> transformIOException(fn: () -> T): T {
    contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
    try {
        return fn()
    } catch (e: FileNotFoundException) {
        throw MPFileNotFoundException(e.toString(), e)
    } catch (e: IOException) {
        throw MPFileException(e.toString(), e)
    }
}

actual fun fileType(path: String): FileNameType? {
    val p = Path.of(path)
    return when {
        !Files.exists(p) -> null
        Files.isRegularFile(p) -> FileNameType.FILE
        Files.isDirectory(p) -> FileNameType.DIRECTORY
        else -> FileNameType.UNDEFINED
    }
}

actual fun currentDirectory(): String {
    return System.getProperty("user.dir")
}

actual fun createDirectory(path: String) {
    transformIOException {
        Files.createDirectory(Path.of(path))
    }
}

actual fun readDirectoryContent(dirName: String): List<PathEntry> {
    val path = Paths.get(dirName)
    unless(Files.isDirectory(path)) {
        throw MPFileException("Argument is not a directory: ${dirName}")
    }
    val result = ArrayList<PathEntry>()
    Files.newDirectoryStream(path).forEach { p ->
        val fileNameType = when {
            Files.isDirectory(p) -> FileNameType.DIRECTORY
            Files.isRegularFile(p) -> FileNameType.FILE
            else -> FileNameType.UNDEFINED
        }
        result.add(
            PathEntry(
                p.fileName.toString(),
                if (fileNameType == FileNameType.FILE) Files.size(p) else 0,
                fileNameType))
    }
    return result
}

actual fun resolveDirectoryPathInt(fileName: String, workingDirectory: String): String {
    val file = Path.of(fileName)
    val parent = Path.of(workingDirectory)
    return parent.resolve(file).toString()
}
