;;; kap-mode --- Kap integration for Emacs -*- lexical-binding: t -*-

(require 'cl-lib)
(require 'comint)
(require 'quail)
(require 'kap-symbols)
(require 'kap-documentation)
(require 'kap-network)

;;;###autoload
(defgroup kap nil
  "Major mode for interacting with Kap"
  :prefix 'kap
  :group 'languages)

(defcustom kap-library-paths nil
  "A list of library search paths the Kap intereter should use"
  :type '(repeat string)
  :group 'kap)

(defcustom kap-executable-file nil
  "The name of the Kap executable (either the JVM- or native text-based versions)"
  :type 'string
  :group 'kap)

(defcustom kap-executable-extra-params nil
  "Extra commandline arguments to be passed to the Kap executable"
  :type '(repeat string)
  :group 'kap)

(defface kap-default
  ()
  "Face used for Kap buffers"
  :group 'kap)

(defface kap-kbd-help-screen
  '((t
     :inherit kap-default))
  "Face used to display the keyboard help popup"
  :group 'kap)

(defvar kap-current-session nil
  "The buffer that holds the currently active Kap session.
The value is nil if there is no active session.")

(defun kap--make-base-mode-map ()
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-k") 'kap-show-keyboard)
    (define-key map [menu-bar kap toggle-keyboard] '("Toggle keyboard" . kap-show-keyboard))
    map))

(defun kap--make-kap-mode-map ()
  (let ((map (kap--make-base-mode-map)))
    map))

(defun kap--make-interactive-mode-map ()
  (let ((map (kap--make-base-mode-map)))
    map))

(defvar kap-mode-map (kap--make-kap-mode-map)
  "The keymap used for ‘kap-mode’.")

(defvar kap-interactive-mode-map (kap--make-interactive-mode-map)
  "The keymap for ‘kap-interactive-mode'.")

(defun kap--init-mode-common ()
  "Generic initialisation code for all gnu-apl modes."
  (setq-local comment-start "⍝")
  (setq-local comment-padding " ")
  (setq-local comment-end "")
  (setq-local buffer-face-mode-face 'kap-default)
  (setq-local completion-at-point-functions '(kap-expand-symbol))
  (buffer-face-mode))

(defvar kap-mode-syntax-table
  (let ((table (make-syntax-table)))
    (modify-syntax-entry (aref "⍝" 0) "<" table)
    (modify-syntax-entry ?\n ">" table)
    (modify-syntax-entry ?\" "\"" table)
    (cl-loop for symbol in kap--symbols
             for char = (second symbol)
             do (modify-syntax-entry (aref char 0) "." table))
    table))

(define-derived-mode kap-mode prog-mode "Kap"
  "Major mode for editing Kap files."
  :syntax-table kap-mode-syntax-table
  :group 'kap
  (use-local-map kap-mode-map)
  (kap--init-mode-common))

(define-derived-mode kap-interactive-mode comint-mode "Kap repl"
  "Major mode for interacting with the Kap repl."
  :syntax-table kap-mode-syntax-table
  :group 'kap
  (use-local-map kap-interactive-mode-map)
  (kap--init-mode-common)
  (setq-local comint-prompt-regexp "^⊢ ")
  (setq-local comint-use-prompt-regexp nil)
  (setq-local kap--reading-first-line t)
  (setq-local kap--results nil)
  (add-hook 'comint-preoutput-filter-functions 'kap--preoutput-filter))

(defun kap--get-interactive-session-with-nocheck ()
  (when kap-current-session
    (let ((proc-status (comint-check-proc kap-current-session)))
      (when (eq (car proc-status) 'run)
        kap-current-session))))

(defun kap--get-interactive-session ()
  (let ((session (kap--get-interactive-session-with-nocheck)))
    (unless session
      (user-error "No active Kap session"))
    session))

(defun kap (kap-executable)
  (interactive (list (when current-prefix-arg
                       (read-file-name "Location of Kap executable: " nil nil t))))
  (let ((buffer (get-buffer-create "*Kap*"))
        (binary-path (or kap-executable kap-executable-file)))
    (unless binary-path
      (user-error "No path to Kap executable specified"))
    (pop-to-buffer-same-window buffer)
    (unless (comint-check-proc buffer)
      (apply #'make-comint-in-buffer
             "kap" buffer binary-path nil
             (append (mapcar (lambda (name)
                               (format "--lib-path=%s" name))
                             kap-library-paths)
                     (list "--tty"
                           "--no-lineeditor"
                           "--server=1234")
                     kap-executable-extra-params))
      (setq kap-current-session buffer)
      (kap-interactive-mode)
      (set-process-coding-system (get-buffer-process (current-buffer)) 'utf-8 'utf-8))))

(defun kap--find-largest-backward-match (regex)
  (save-excursion
    (cl-loop with old-pos = nil
          for pos = (save-excursion (search-backward-regexp regex nil t))
          while pos
          do (progn
               (backward-char 1)
               (setq old-pos pos))
          finally (return old-pos))))

(defun kap-expand-symbol ()
  "Implementation of expansion.
This function is designed to be used in ‘completion-at-point-functions’."
  (let ((row (buffer-substring (save-excursion (beginning-of-line) (point)) (point))))
    (when-let ((pos (kap--find-largest-backward-match "[a-zA-Z_][a-zA-Z0-9_]*\\=")))
      (let* ((s (buffer-substring pos (point)))
             (results (kap--send-network-command-and-read "syms" `((prefix . ,s)))))
        (when (plusp (length results))
          (list pos (point) (map 'list
                                 (lambda (v)
                                   (gethash "name" v))
                                 results)))))))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.kap\\'" . kap-mode))

;;;###autoload
(add-to-list 'interpreter-mode-alist '("kap" . kap-mode))

(provide 'kap-mode)
