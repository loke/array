package com.dhsdevelopments.kap

import com.dhsdevelopments.mpbignum.BigInt
import com.dhsdevelopments.mpbignum.Rational
import com.dhsdevelopments.mpbignum.make
import com.dhsdevelopments.mpbignum.of
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertIs

class ReshapeTest : APLTest() {
    @Test
    fun simpleReshape() {
        val result = parseAPLExpression("3 4 ⍴ ⍳12")
        assertDimension(dimensionsOfSize(3, 4), result)
        assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11), result)
    }

    @Test
    fun reshapeDecreaseSize() {
        val result = parseAPLExpression("3 ⍴ 1 2 3 4 5 6 7 8")
        assertDimension(dimensionsOfSize(3), result)
        assertArrayContent(arrayOf(1, 2, 3), result)
    }

    @Test
    fun reshapeIncreaseSize() {
        val result = parseAPLExpression("14 ⍴ 1 2 3 4")
        assertDimension(dimensionsOfSize(14), result)
        assertArrayContent(arrayOf(1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2), result)
    }

    @Test
    fun reshapeScalarToSingleDimension() {
        val result = parseAPLExpression("4 ⍴ 1")
        assertDimension(dimensionsOfSize(4), result)
        assertArrayContent(arrayOf(1, 1, 1, 1), result)
    }

    @Test
    fun reshapeScalarToMultiDimension() {
        val result = parseAPLExpression("2 4 ⍴ 1")
        assertDimension(dimensionsOfSize(2, 4), result)
        assertArrayContent(arrayOf(1, 1, 1, 1, 1, 1, 1, 1), result)
    }

    @Test
    fun reshapeScalarDoubleToArray() {
        parseAPLExpression("4 ⍴ 1.5").let { result ->
            assert1DArray(arrayOf(InnerDouble(1.5), InnerDouble(1.5), InnerDouble(1.5), InnerDouble(1.5)), result)
        }
    }

    /**
     * This test ensures that [APLValue.valueAt] is called on the reshaped value rather than [APLValue.valueAtDouble].
     */
    @Test
    fun reshapeScalarDoubleAndConcatenate() {
        parseAPLExpression("2 , 2 ⍴ 1.5").let { result ->
            assert1DArray(arrayOf(2, InnerDouble(1.5), InnerDouble(1.5)), result)
        }
    }

    @Test
    fun reshapeWithInvalidLeftArgument() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("(2 2 ⍴ 3 4 5 6) ⍴ 1 2 3 4")
        }
    }

    @Test
    fun reshapeCalculatedDimension0() {
        parseAPLExpression("¯1 2 ⍴ ⍳4").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 1, 2, 3), result)
        }
    }

    @Test
    fun reshapeCalculatedDimension1() {
        parseAPLExpression("2 ¯1 ⍴ ⍳4").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 1, 2, 3), result)
        }
    }

    @Test
    fun reshapeCalculatedDimension1MatchTag() {
        parseAPLExpression("2 :match ⍴ ⍳4").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 1, 2, 3), result)
        }
    }

    @Test
    fun reshapeCalculatedFailsWithMismatchedSource() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("2 ¯1 ⍴ ⍳5")
        }
    }

    @Test
    fun reshapeCalculatedFailsWithMultipleUndefinedDimensions() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("¯1 ¯1 ⍴ ⍳4")
        }
    }

    @Test
    fun reshapeCalculatedWithFillTag0() {
        parseAPLExpression("2 :fill ⍴ ⍳3").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 1, 2, 0), result)
        }
    }

    @Test
    fun reshapeCalculatedWithFillTag1() {
        parseAPLExpression(":fill 2 ⍴ ⍳3").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 1, 2, 0), result)
        }
    }

    @Test
    fun reshapeCalculatedWithTruncateTag0() {
        parseAPLExpression("2 :truncate ⍴ ⍳3").let { result ->
            assertDimension(dimensionsOfSize(2, 1), result)
            assertArrayContent(arrayOf(0, 1), result)
        }
    }

    @Test
    fun reshapeCalculatedWithTruncateTag1() {
        parseAPLExpression(":truncate 2 ⍴ ⍳3").let { result ->
            assertDimension(dimensionsOfSize(1, 2), result)
            assertArrayContent(arrayOf(0, 1), result)
        }
    }

    @Test
    fun reshapeCalculatedWithRecycleTag0() {
        parseAPLExpression("2 :recycle ⍴ 1+⍳3").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, 2, 3, 1), result)
        }
    }

    @Test
    fun reshapeCalculatedWithRecycleTag1() {
        parseAPLExpression(":recycle 2 ⍴ 1+⍳3").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, 2, 3, 1), result)
        }
    }

    @Test
    fun reshapeWithCalculatedAndMatchingKeywordMatch() {
        parseAPLExpression("3 :match ⍴ 1+⍳9").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9), result)
        }
    }

    @Test
    fun reshapeWithCalculatedAndMatchingKeywordFill() {
        parseAPLExpression("3 :fill ⍴ 1+⍳9").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9), result)
        }
    }

    @Test
    fun reshapeWithCalculatedAndMatchingKeywordTruncate() {
        parseAPLExpression("3 :truncate ⍴ 1+⍳9").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9), result)
        }
    }

    @Test
    fun reshapeWithCalculatedAndMatchingKeywordRecycle() {
        parseAPLExpression("3 :recycle ⍴ 1+⍳9").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9), result)
        }
    }

    @Test
    fun reshapeWithCalculated3DimensionalMatch() {
        parseAPLExpression("2 :match 4 ⍴ 1+⍳16").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 4), result)
            assertArrayContent(arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16), result)
        }
    }

    @Test
    fun reshapeWithCalculated3DimensionalFill() {
        parseAPLExpression("2 :fill 4 ⍴ 1+⍳14").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 4), result)
            assertArrayContent(arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0, 0), result)
        }
    }

    @Test
    fun reshapeWithCalculated3DimensionalTruncate() {
        parseAPLExpression("2 :truncate 4 ⍴ 1+⍳19").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 4), result)
            assertArrayContent(arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16), result)
        }
    }

    @Test
    fun reshapeWithCalculated3DimensionalRecycle() {
        parseAPLExpression("2 :recycle 4 ⍴ 1+⍳14").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 4), result)
            assertArrayContent(arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 1, 2), result)
        }
    }

    @Test
    fun reshapeSpecialisedLong() {
        parseAPLExpression("2 3 ⍴ 10 11 12 13").let { result ->
            assertDimension(dimensionsOfSize(2, 3), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
            assertEquals(10L, result.valueAtLong(0))
            assertEquals(11L, result.valueAtLong(1))
            assertEquals(12L, result.valueAtLong(2))
            assertEquals(13L, result.valueAtLong(3))
            assertEquals(10L, result.valueAtLong(4))
            assertEquals(11L, result.valueAtLong(5))
        }
    }

    @Test
    fun reshapeSpecialisedDouble() {
        parseAPLExpression("2 3 ⍴ 1.1 1.2 1.3 1.4").let { result ->
            assertDimension(dimensionsOfSize(2, 3), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
            assertEquals(1.1, result.valueAtDouble(0))
            assertEquals(1.2, result.valueAtDouble(1))
            assertEquals(1.3, result.valueAtDouble(2))
            assertEquals(1.4, result.valueAtDouble(3))
            assertEquals(1.1, result.valueAtDouble(4))
            assertEquals(1.2, result.valueAtDouble(5))
        }
    }

    @Test
    fun reshapeSpecialisedLongSingleValue() {
        parseAPLExpression("2 3 ⍴ 1").let { result ->
            assertDimension(dimensionsOfSize(2, 3), result)
            repeat(6) { i ->
                assertEquals(1, result.valueAtLong(i))
            }
        }
    }

    @Test
    fun reshapeSpecialisedDoubleSingleValue() {
        parseAPLExpression("2 3 ⍴ 1.2").let { result ->
            assertDimension(dimensionsOfSize(2, 3), result)
            repeat(6) { i ->
                assertEquals(1.2, result.valueAtDouble(i))
            }
        }
    }

    @Test
    fun reshapeFromSingleElementArray() {
        parseAPLExpression("{ (⍴⍵) ⍴ 1 } 1 2 3 4").let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(1, 1, 1, 1), result)
        }
    }

    @Test
    fun reshapeScalar0() {
        parseAPLExpression("⍬ ⍴ 1").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun reshapeScalar1() {
        parseAPLExpression("⍬ ⍴ 1 2 3 4 5 6").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun reshapeToNull() {
        parseAPLExpression("⍬ ⍴ ⊂1 2 3").let { result ->
            assertEquals(0, result.dimensions.size)
            val v = result.valueAt(0)
            assert1DArray(arrayOf(1, 2, 3), v)
        }
    }

    @Test
    fun reshapeReshapedArrayDefaultSimple() {
        parseAPLExpression("6 ⍴ 3 ⍴ 10 11 12 13 14 15 16 17 18").let { result ->
            assert1DArray(arrayOf(10, 11, 12, 10, 11, 12), result)
        }
    }

    @Test
    fun reshapeReshapedArrayGenericSimple() {
        parseAPLExpression("6 ⍴ 3 ⍴ int:ensureGeneric 10 11 12 13 14 15 16 17 18").let { result ->
            assert1DArray(arrayOf(10, 11, 12, 10, 11, 12), result)
        }
    }

    @Test
    fun reshapeReshapedArrayLongSimple() {
        parseAPLExpression("6 ⍴ 3 ⍴ int:ensureLong 10 11 12 13 14 15 16 17 18").let { result ->
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
            assert1DArray(arrayOf(10, 11, 12, 10, 11, 12), result)
        }
    }

    @Test
    fun reshapeReshapedArrayDoubleSimple() {
        parseAPLExpression("6 ⍴ 3 ⍴ int:ensureDouble 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0").let { result ->
            assert1DArray(
                arrayOf(
                    InnerDouble(10.0),
                    InnerDouble(11.0),
                    InnerDouble(12.0),
                    InnerDouble(10.0),
                    InnerDouble(11.0),
                    InnerDouble(12.0)),
                result)
        }
    }

    @Test
    fun reshapeReshapedArrayMulti() {
        parseAPLExpression("7 ⍴ 6 ⍴ 5 ⍴ int:ensureGeneric ⍳100").let { result ->
            assert1DArray(arrayOf(0, 1, 2, 3, 4, 0, 0), result)
        }
    }

    @Test
    fun reshapeReshapedArrayLongMulti() {
        parseAPLExpression("7 ⍴ 6 ⍴ 5 ⍴ int:ensureLong ⍳100").let { result ->
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
            assert1DArray(arrayOf(0, 1, 2, 3, 4, 0, 0), result)
        }
    }

    @Test
    fun reshapeReshapedArrayDoubleMulti() {
        parseAPLExpression("7 ⍴ 6 ⍴ 5 ⍴ int:ensureDouble ⍳100").let { result ->
            assert1DArray(
                arrayOf(
                    InnerDouble(0.0),
                    InnerDouble(1.0),
                    InnerDouble(2.0),
                    InnerDouble(3.0),
                    InnerDouble(4.0),
                    InnerDouble(0.0),
                    InnerDouble(0.0)),
                result)
        }
    }

    @Test
    fun reshapeReshapedArrayDefaultSameSize() {
        parseAPLExpression("3 ⍴ 3 ⍴ 10 11 12 13 14 15 16 17 18").let { result ->
            assert1DArray(arrayOf(10, 11, 12), result)
        }
    }

    @Test
    fun reshapeReshapedArrayGenericSameSize() {
        parseAPLExpression("3 ⍴ 3 ⍴ int:ensureGeneric 10 11 12 13 14 15 16 17 18").let { result ->
            assert1DArray(arrayOf(10, 11, 12), result)
        }
    }

    @Test
    fun reshapeReshapedArrayLongSameSize() {
        parseAPLExpression("3 ⍴ 3 ⍴ int:ensureLong 10 11 12 13 14 15 16 17 18").let { result ->
            assert1DArray(arrayOf(10, 11, 12), result)
        }
    }

    @Test
    fun reshapeReshapedArrayDoubleSameSize() {
        parseAPLExpression("3 ⍴ 3 ⍴ int:ensureDouble 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0").let { result ->
            assert1DArray(arrayOf(InnerDouble(10.0), InnerDouble(11.0), InnerDouble(12.0)), result)
        }
    }

    @Test
    fun reshapeReshapedArrayDefaultSmallerSize() {
        parseAPLExpression("2 ⍴ 6 ⍴ 10 11 12 13 14 15 16 17 18").let { result ->
            assert1DArray(arrayOf(10, 11), result)
        }
    }

    @Test
    fun reshapeReshapedArrayGenericSmallerSize() {
        parseAPLExpression("2 ⍴ 6 ⍴ int:ensureGeneric 10 11 12 13 14 15 16 17 18").let { result ->
            assert1DArray(arrayOf(10, 11), result)
        }
    }

    @Test
    fun reshapeReshapedArrayLongSmallerSize() {
        parseAPLExpression("2 ⍴ 6 ⍴ int:ensureLong 10 11 12 13 14 15 16 17 18").let { result ->
            assert1DArray(arrayOf(10, 11), result)
        }
    }

    @Test
    fun reshapeReshapedArrayDoubleSmallerSize() {
        parseAPLExpression("2 ⍴ 6 ⍴ int:ensureDouble 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0").let { result ->
            assert1DArray(arrayOf(InnerDouble(10.0), InnerDouble(11.0)), result)
        }
    }

    @Test
    fun reshapeReshapedArrayDefaultSmallerSizeTwoLevels() {
        parseAPLExpression("2 ⍴ 3 ⍴ 6 ⍴ 10 11 12 13 14 15 16 17 18").let { result ->
            assert1DArray(arrayOf(10, 11), result)
        }
    }

    @Test
    fun reshapeReshapedArrayGenericSmallerSizeTwoLevels() {
        parseAPLExpression("2 ⍴ 3 ⍴ 6 ⍴ int:ensureGeneric 10 11 12 13 14 15 16 17 18").let { result ->
            assert1DArray(arrayOf(10, 11), result)
        }
    }

    @Test
    fun reshapeReshapedArrayLongSmallerSizeTwoLevels() {
        parseAPLExpression("2 ⍴ 3 ⍴ 6 ⍴ int:ensureLong 10 11 12 13 14 15 16 17 18").let { result ->
            assert1DArray(arrayOf(10, 11), result)
        }
    }

    @Test
    fun reshapeReshapedArrayDoubleSmallerSizeTwoLevels() {
        parseAPLExpression("2 ⍴ 3 ⍴ 6 ⍴ int:ensureDouble 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0").let { result ->
            assert1DArray(arrayOf(InnerDouble(10.0), InnerDouble(11.0)), result)
        }
    }

    @Test
    fun reshapeReshapedMultiDimensionalArrayDefault() {
        parseAPLExpression("5 ⍴ 2 2 ⍴ ⍳100").let { result ->
            assert1DArray(arrayOf(0, 1, 2, 3, 0), result)
        }
    }

    @Test
    fun reshapeReshapedMultiDimensionalArrayGeneric() {
        parseAPLExpression("5 ⍴ 2 2 ⍴ int:ensureGeneric ⍳100").let { result ->
            assert1DArray(arrayOf(0, 1, 2, 3, 0), result)
        }
    }

    @Test
    fun reshapeReshapedMultiDimensionalArrayLong() {
        parseAPLExpression("5 ⍴ 2 2 ⍴ int:ensureLong ⍳100").let { result ->
            assert1DArray(arrayOf(0, 1, 2, 3, 0), result)
        }
    }

    @Test
    fun reshapeReshapedMultiDimensionalArrayDouble() {
        parseAPLExpression("5 ⍴ 2 2 ⍴ int:ensureDouble ⍳100").let { result ->
            assert1DArray(arrayOf(InnerDouble(0.0), InnerDouble(1.0), InnerDouble(2.0), InnerDouble(3.0), InnerDouble(0.0)), result)
        }
    }

    @Test
    fun reshapeReshapedArrayDefaultSameSizeWithIota() {
        parseAPLExpression("3 ⍴ 3 ⍴ ⍳100").let { result ->
            assert1DArray(arrayOf(0, 1, 2), result)
        }
    }

    @Test
    fun reshapeReshapedArrayDefaultSimpleWithIotaDescending() {
        parseAPLExpression("6 ⍴ 2 ⍴ ⍳100").let { result ->
            assert1DArray(arrayOf(0, 1, 0, 1, 0, 1), result)
        }
    }

    @Test
    fun reshapeReshapedArrayDefaultSimpleWithIotaAscending() {
        parseAPLExpression("2 ⍴ 6 ⍴ ⍳100").let { result ->
            assert1DArray(arrayOf(0, 1), result)
        }
    }

    @Test
    fun reshapeEmptyArrayWithCalculatedDimension0() {
        parseAPLExpression("¯1 3 ⍴ ⍬").let { result ->
            assertDimension(dimensionsOfSize(0, 3), result)
        }
    }

    @Test
    fun reshapeEmptyArrayWithCalculatedDimension1() {
        parseAPLExpression("3 ¯1 ⍴ ⍬").let { result ->
            assertDimension(dimensionsOfSize(3, 0), result)
        }
    }

    @Test
    fun reshapeEmptyArrayWithCalculatedDimension2() {
        parseAPLExpression("3 3 ¯1 2 2⍴ ⍬").let { result ->
            assertDimension(dimensionsOfSize(3, 3, 0, 2, 2), result)
        }
    }

    @Test
    fun reshapeEmptyArrayWithCalculatedDimension2MatchKeywordTag() {
        parseAPLExpression("3 3 :match 2 2⍴ ⍬").let { result ->
            assertDimension(dimensionsOfSize(3, 3, 0, 2, 2), result)
        }
    }

    @Test
    fun reshapeEmptyArrayWithMultipleCalculatedDimensions() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("3 3 ¯1 ¯1 2⍴ ⍬")
        }
    }

    @Test
    fun reshapeWithSingleCalculatedDimension0() {
        parseAPLExpression("¯1 ⍴ 1 2 3 4").let { result ->
            assert1DArray(arrayOf(1, 2, 3, 4), result)
        }
    }

    @Test
    fun reshapeWithSingleCalculatedDimensionWithMatchKeyword() {
        parseAPLExpression(":match ⍴ 1 2 3 4").let { result ->
            assert1DArray(arrayOf(1, 2, 3, 4), result)
        }
    }

    @Test
    fun reshapeWithSingleCalculatedDimension1() {
        parseAPLExpression("(,¯1) ⍴ 1 2 3 4").let { result ->
            assert1DArray(arrayOf(1, 2, 3, 4), result)
        }
    }

    @Test
    fun reshapeCalculatedWithFillPreserveTypeLong() {
        parseAPLExpression(":fill 2 ⍴ 1 2 3", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, 2, 3, 0), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun reshapeCalculatedWithFillPreserveTypeDoubleWithDefaultFill() {
        parseAPLExpression(":fill 2 ⍴ 1.1 2.1 3.1", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(
                arrayOf(
                    InnerDouble(1.1),
                    InnerDouble(2.1),
                    InnerDouble(3.1),
                    0),
                result)
            assertEquals(ArrayMemberType.ATOM, result.specialisedType)
        }
    }

    @Test
    fun reshapeCalculatedWithFillPreserveTypeDoubleWithDoubleFill() {
        parseAPLExpression(":fill 2 ⍴ 2.2 int:proto 1.1 2.1 3.1", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(
                arrayOf(
                    InnerDouble(1.1),
                    InnerDouble(2.1),
                    InnerDouble(3.1),
                    InnerDouble(2.2)),
                result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun reshapeWithFillUpdateTypeFromBooleanToLong() {
        parseAPLExpression(":fill 2 ⍴ 10 int:proto 1 0 1", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, 0, 1, 10), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun reshapeWithSingleNegativeValue() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("¯2 ⍴ 1 2 3 4")
        }
    }

    @Test
    fun reshapeNullArray() {
        parseAPLExpression("3 4 ⍴ ⍬").let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            assertArrayContent(Array(3 * 4) { 0 }, result)
        }
    }

    @Test
    fun reshapeEmptySizeArray() {
        parseAPLExpression("3 4 ⍴ comp 3 0 ⍴ 3").let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            assertArrayContent(Array(3 * 4) { 0 }, result)
        }
    }

    @Test
    fun resizedToEmptySetsTypeToAll() {
        parseAPLExpression("0⍴1 2 3").let { result ->
            assertDimension(emptyListDimensions(), result)
            assertEquals(ArrayMemberType.ALL, result.specialisedType)
        }
    }

    @Test
    fun resizedGenericToEmptySetsTypeToAll() {
        parseAPLExpression("0⍴1.2 1").let { result ->
            assertDimension(emptyListDimensions(), result)
            assertEquals(ArrayMemberType.ALL, result.specialisedType)
        }
    }

    @Test
    fun resizedDoubleToEmptySetsTypeToAll() {
        parseAPLExpression("0⍴1.2 1.1").let { result ->
            assertDimension(emptyListDimensions(), result)
            assertEquals(ArrayMemberType.ALL, result.specialisedType)
        }
    }

    @Test
    fun resizedToEmptyMultidimensionalSetsTypeToAll() {
        parseAPLExpression("2 2 0⍴1 2 3").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 0), result)
            assertEquals(ArrayMemberType.ALL, result.specialisedType)
        }
    }

    @Test
    fun resizedIotaArrayToEmptySetsTypeToAll() {
        parseAPLExpression("0 ⍴ ⍳100").let { result ->
            assertDimension(emptyListDimensions(), result)
            assertEquals(ArrayMemberType.ALL, result.specialisedType)
        }
    }

    @Test
    fun resizedIotaArrayToEmptyMultiDimensionsSetsTypeToAll() {
        parseAPLExpression("0 2 3 ⍴ ⍳100").let { result ->
            assertDimension(dimensionsOfSize(0, 2, 3), result)
            assertEquals(ArrayMemberType.ALL, result.specialisedType)
        }
    }

    @Test
    fun resizeNegativeDimension0() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("¯2 ⍴ ⍳10")
        }
    }

    @Test
    fun resizeNegativeDimension2() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("1000 ¯2 ⍴ ⍳10")
        }
    }

    @Test
    fun resizeOversizeArray() {
        assertFailsWith<ArraySizeException> {
            parseAPLExpression("1000000 1000000 ⍴ 1 2 3 4 5")
        }
    }

    @Test
    fun reshapeToEmpty() {
        parseAPLExpression("6 0 ⍴ 1").let { result ->
            assertIs<APLEmptyArray>(result)
        }
    }

    @Test
    fun addUnderReshape() {
        parseAPLExpression("(1+)⍢(2 2⍴) 10 20 30 40").let { result ->
            assert1DArray(arrayOf(11, 21, 31, 41), result)
        }
    }

    @Test
    fun restructureUnderReshape0() {
        parseAPLExpression("{100 103 ⍪ ¯1↑⍵}⍢(2 2⍴) 10 20 30 40").let { result ->
            assert1DArray(arrayOf(100, 103, 30, 40), result)
        }
    }

    @Test
    fun restructureUnderReshape1() {
        parseAPLExpression("{100 103 105 ⍪ ¯1↑⍵}⍢(2 3⍴) 3 2 ⍴ 10 20 30 40 50 60").let { result ->
            assertDimension(dimensionsOfSize(3, 2), result)
            assertArrayContent(arrayOf(100, 103, 105, 40, 50, 60), result)
        }
    }

    @Test
    fun restructureUnderReshapeDimensionsMustMatchResultLarge() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("{2 3 ⍴ 1 2 3 4 5 6}⍢(2 2⍴) 10 20 30 40")
        }
    }

    @Test
    fun restructureUnderReshapeDimensionsMustMatchResultSmall() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("{2 1 ⍴ 1 2 3 4 5 6}⍢(2 3⍴) 10 20 30 40 50 60")
        }
    }

    @Test
    fun replaceUnderChain() {
        parseAPLExpression("(3+)⍢(1↑2 2⍴) 10 20 30 40").let { result ->
            assert1DArray(arrayOf(13, 23, 30, 40), result)
        }
    }

    @Test
    fun addUnderReshapeWithDifferentSize0() {
        parseAPLExpression("(1+)⍢(2 2⍴) 10 20 30 40 50 60 70 80 90 100").let { result ->
            assert1DArray(arrayOf(11, 21, 31, 41, 50, 60, 70, 80, 90, 100), result)
        }
    }

    @Test
    fun addUnderReshapeWithDifferentSize1() {
        parseAPLExpression("(1+)⍢(2 2⍴) 3 4 ⍴ 10 20 30 40 50 60 70 80 90 100 110 120").let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            assertArrayContent(arrayOf(11, 21, 31, 41, 50, 60, 70, 80, 90, 100, 110, 120), result)
        }
    }

    @Test
    fun addUnderReshapeWithLargerDimension() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("(1+)⍢(8 2⍴) 3 4 ⍴ 10 20 30 40 50 60 70 80 90 100 110 120")
        }
    }

    @Test
    fun reshapeWithSingleCalculatedDimensionWithInvalidKeyword() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression(":abc ⍴ 1 2 3 4")
        }
    }

    @Test
    fun reshapeWithMultiCalculatedDimensionWithInvalidKeyword() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression(":abc 2 ⍴ 1 2 3 4")
        }
    }

    @Test
    fun reshapeWithBigint() {
        parseAPLExpression("3 3 ⍴ 123456789012345678901234567890").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            repeat(9) { i ->
                assertBigIntOrLong("123456789012345678901234567890", result.valueAt(0))
            }
        }
    }

    @Test
    fun reshapeWithRational() {
        parseAPLExpression("3 3 ⍴ 1r123456789012345678901234567890").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            repeat(9) { i ->
                assertRational(Rational.make(BigInt.of(1), BigInt.of("123456789012345678901234567890")), result.valueAt(0))
            }
        }
    }
}
