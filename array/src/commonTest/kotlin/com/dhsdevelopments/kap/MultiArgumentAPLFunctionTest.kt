package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertFailsWith

class MultiArgumentAPLFunctionTest : APLTest() {
    @Test
    fun singleArgument() {
        parseAndEvalWithMultiArgEngine(1, 1, "multiArgFn 10").let { result ->
            assert1DArray(arrayOf(10), result)
        }
    }

    @Test
    fun singleArgumentExtraArgs() {
        assertFailsWith<APLArgumentCountMismatch> {
            parseAndEvalWithMultiArgEngine(1, 1, "multiArgFn (10 ; 20)")
        }
    }

    @Test
    fun singleArgumentWithExplicitList() {
        parseAndEvalWithMultiArgEngine(1, 1, "multiArgFn ≬1234").let { result ->
            assert1DArray(arrayOf(1234), result)
        }
    }

    @Test
    fun zeroArgFunction() {
        assertAPLNull(parseAndEvalWithMultiArgEngine(0, 0, "multiArgFn ≬⍬"))
    }

    @Test
    fun zeroArgFnWithOneArg() {
        assertFailsWith<APLArgumentCountMismatch> {
            parseAndEvalWithMultiArgEngine(0, 0, "multiArgFn 1")
        }
    }

    @Test
    fun multiArg1To4Args() {
        val engine = makeMultiArgEngine(2, 4)
        assertFailsWith<APLArgumentCountMismatch> {
            engine.parseAndEval(StringSourceLocation("multiArgFn 100"))
        }
        assertFailsWith<APLArgumentCountMismatch> {
            engine.parseAndEval(StringSourceLocation("multiArgFn ≬100"))
        }
        engine.parseAndEval(StringSourceLocation("multiArgFn (100 ; 101)")).let { result ->
            assert1DArray(arrayOf(100, 101), result)
        }
        engine.parseAndEval(StringSourceLocation("multiArgFn (100 ; 101 ; 102)")).let { result ->
            assert1DArray(arrayOf(100, 101, 102), result)
        }
        engine.parseAndEval(StringSourceLocation("multiArgFn (100 ; 101 ; 102 ; 103)")).let { result ->
            assert1DArray(arrayOf(100, 101, 102, 103), result)
        }
        assertFailsWith<APLArgumentCountMismatch> {
            engine.parseAndEval(StringSourceLocation("multiArgFn (100 ; 101 ; 102 ; 103 ; 104)"))
        }
    }

    @Test
    fun multiArgUnlimitedMaxArgs() {
        parseAndEvalWithMultiArgEngine(3, null, "multiArgFn (100 ; 101 ; 102 ; 103 ; 104 ; 105)").let { result ->
            assert1DArray(arrayOf(100, 101, 102, 103, 104, 105), result)
        }
    }

    @Test
    fun multiArgUnlimitedMaxArgsTooFewArgs0() {
        assertFailsWith<APLArgumentCountMismatch> {
            parseAndEvalWithMultiArgEngine(3, null, "multiArgFn (100 ; 101)")
        }
    }

    @Test
    fun multiArgUnlimitedMaxArgsTooFewArgs1() {
        assertFailsWith<APLArgumentCountMismatch> {
            parseAndEvalWithMultiArgEngine(3, null, "multiArgFn 100")
        }
    }

    @Test
    fun multiArgUnlimitedMaxArgsTooFewArgs2() {
        assertFailsWith<APLArgumentCountMismatch> {
            parseAndEvalWithMultiArgEngine(3, null, "multiArgFn ≬⍬")
        }
    }

    private fun makeMultiArgEngine(minArgs: Int, maxArgs: Int?, withStandardLib: Boolean = false): Engine {
        val engine = makeEngine(withStandardLib = withStandardLib)
        engine.registerFunction(engine.currentNamespace.internAndExport("multiArgFn"), MultiArgTestFn(minArgs, maxArgs))
        return engine
    }

    private fun parseAndEvalWithMultiArgEngine(
        minArgs: Int,
        maxArgs: Int?,
        src: String,
        withStandardLib: Boolean = false,
        collapseResult: Boolean = true
    ): APLValue {
        val engine = makeMultiArgEngine(minArgs, maxArgs, withStandardLib = withStandardLib)
        return engine.parseAndEval(StringSourceLocation(src), collapseResult = collapseResult)
    }

    private class MultiArgTestFn(val minArgs: Int, val maxArgs: Int?) : APLFunctionDescriptor {
        inner class MultiArgTestFnImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(minArgs, maxArgs, pos) {
            override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
                return APLArrayImpl(dimensionsOfSize(args.size), Array(args.size) { i -> args[i] })
            }
        }

        override fun make(instantiation: FunctionInstantiation) = MultiArgTestFnImpl(instantiation)
    }
}
