package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.jsObject2
import kotlinx.browser.document
import kotlinx.html.dom.create
import kotlinx.html.js.div
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLScriptElement
import org.w3c.dom.get

@JsModule("chart.js/auto")
@JsNonModule
external class Chart(element: HTMLElement, data: dynamic)

fun displayChart(msg: dynamic) {
    val chartData = msg.chartdata
    when (chartData.type) {
        CHART_TYPE_LINE -> displayLineChart(chartData, "line")
        CHART_TYPE_BAR -> displayLineChart(chartData, "bar")
        CHART_TYPE_PIE -> displayLineChart(chartData, "pie")
        CHART_TYPE_DOUGHNUT -> displayLineChart(chartData, "doughnut")
        CHART_TYPE_SCATTER -> displayScatterChart(chartData)
        CHART_TYPE_SURFACE -> displaySurfaceChart(chartData)
        else -> println("No renderer implementation for type: ${chartData.type}")
    }
}

fun displayLineChart(content: dynamic, subtype: String) {
    val element = createDiv("output-chart")
    val canvas = createCanvas("output-chart-inner")
    element.appendChild(canvas)
    findCurrentOutput().appendNode(element)

    val descriptor = jsObject2(
        "type" to subtype,
        "data" to makeLinearChartData(content))
    Chart(canvas, descriptor)
}

private fun makeLinearChartData(content: dynamic): dynamic {
    val datasetsResult = js("[]")
    content.data.forEach { d ->
        val list = js("[]")
        repeat(d.data.length as Int) { i ->
            list.push(d.data[i])
        }
        datasetsResult.push(
            jsObject2(
                "label" to d.name,
                "data" to list))
    }
    val data = jsObject2(
        "labels" to content.horizontalAxisLabels,
        "datasets" to datasetsResult)
    return data
}

private fun displayScatterChart(content: dynamic) {
    val element = createDiv("output-chart")
    val canvas = createCanvas("output-chart-inner")
    element.appendChild(canvas)
    findCurrentOutput().appendNode(element)
    val descriptor = jsObject2(
        "type" to "scatter",
        "data" to makeScatterChartData(content))
    Chart(canvas, descriptor)
}

private fun makeScatterChartData(content: dynamic): dynamic {
    val list = js("[]")
    content.data.forEach { d ->
        val l = js("[]")
        d.points.forEach { point ->
            l.push(
                jsObject2(
                    "x" to point.x,
                    "y" to point.y))
        }
        list.push(
            jsObject2(
                "label" to d.name,
                "data" to l))
    }
    val res = jsObject2(
        "datasets" to list,
        "marker" to 1111)
    return res
}

fun initChart() {
    val doc: dynamic = document
    doc.chart3DDisplay = null
}

private enum class LoadState { UNINITIALISED, LOADING, LOADED }

private var chart3DInitialised = LoadState.UNINITIALISED

private fun displaySurfaceChart(content: dynamic) {
    val doc: dynamic = document

    fun sendRequest() {
        val element = createDiv("output-chart")
        findCurrentOutput().appendNode(element)
        doc.chart3DDisplay(element, content)
    }

    when (chart3DInitialised) {
        LoadState.UNINITIALISED -> {
            val elements = document.getElementsByTagName("head")
            if (elements.length > 0) {
                val head = elements[0]
                if (head != null) {
                    val statusWrapper = document.getElementById("status-wrapper")!!
                    val loadingNode = document.create.div { +"Loading chart library. Please wait." }
                    statusWrapper.appendChild(loadingNode)

                    val script = (document.createElement("script") as HTMLScriptElement)
                    script.setAttribute("src", "chart3d.js")
                    script.onload = {
                        loadingNode.remove()
                        chart3DInitialised = LoadState.LOADED
                        sendRequest()
                    }
                    chart3DInitialised = LoadState.LOADING
                    head.appendChild(script)
                }
            }
        }
        LoadState.LOADING -> {
            println("Attempt to load module while it's already being loaded")
        }
        LoadState.LOADED -> {
            sendRequest()
        }
    }
}
