package com.dhsdevelopments.kap.gui.objview

import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.FormatStyle

object TableRendererProvider : ObjectRendererHandler {
    override fun canRender(value: APLValue): APLValueRenderer? {
        return if (value.dimensions.size >= 2) {
            TableValueRenderer
        } else {
            null
        }
    }
}

object TableValueRenderer : APLValueRenderer {
    override fun nodeAsString(value: APLValue): String {
        return value.formatted(FormatStyle.READABLE)
    }

    override fun preferString(value: APLValue) = false
}
