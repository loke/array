package com.dhsdevelopments.kap

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

actual fun platformInit(engine: Engine) {
    engine.systemParameters[engine.standardSymbols.platform] = ConstantSymbolSystemParameterProvider(engine.internSymbol("js", engine.coreNamespace))
}

private var currentLocalStorage: StorageStack? = null

@OptIn(ExperimentalContracts::class)
actual fun <T> withThreadLocalStorageStackAssigned(stack: StorageStack?, fn: () -> T): T {
    contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
    val prevStack = currentLocalStorage
    currentLocalStorage = stack
    try {
        return fn()
    } finally {
        currentLocalStorage = prevStack
    }
}

actual fun currentStorageStack(): StorageStack {
    return currentLocalStorage!!
}

actual fun currentStorageStackOrNull(): StorageStack? {
    return currentLocalStorage
}
