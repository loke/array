package com.dhsdevelopments.mpbignum

interface Rational {
    val numerator: BigInt
    val denominator: BigInt

    operator fun plus(other: Rational): Rational
    operator fun minus(other: Rational): Rational
    operator fun times(other: Rational): Rational
    operator fun div(other: Rational): Rational
    operator fun unaryMinus(): Rational
    operator fun rem(other: Rational): Rational
    operator fun compareTo(other: Rational): Int

    fun pow(other: Long): Rational
    fun signum(): Int
    fun ceil(): BigInt
    fun floor(): BigInt

    val absoluteValue: Rational

    fun toDouble(): Double
    fun toLongTruncated(): Long
    fun isInteger(): Boolean = denominator == BigIntConstants.ONE

    fun rangeFitsInLong(): Boolean {
        if (!isInteger()) {
            return false
        }
        val d = numerator
        return d >= Long.MIN_VALUE && d <= Long.MAX_VALUE
    }

    companion object {
        val ZERO = Rational.make(BigIntConstants.ZERO, BigIntConstants.ONE)
        val ONE = Rational.make(BigIntConstants.ONE, BigIntConstants.ONE)
        val ONE_HALF = Rational.make(BigIntConstants.ONE, BigIntConstants.TWO)
    }
}

expect fun Rational.Companion.make(a: BigInt, b: BigInt): Rational
expect fun Rational.Companion.make(a: Long, b: Long): Rational
expect fun Rational.Companion.make(a: String, b: String): Rational

fun Short.toRational() = Rational.make(this.toLong(), 1)
fun Int.toRational() = Rational.make(this.toLong(), 1)
fun Long.toRational() = Rational.make(this, 1)
fun BigInt.toRational() = Rational.make(this, BigIntConstants.ONE)

operator fun Rational.plus(other: Long) = this + other.toRational()
operator fun Long.plus(other: Rational) = this.toRational() + other
operator fun Rational.minus(other: Long) = this - other.toRational()
operator fun Long.minus(other: Rational) = this.toRational() - other
operator fun Rational.times(other: Long) = this * other.toRational()
operator fun Long.times(other: Rational) = this.toRational() * other
operator fun Rational.div(other: Long) = this / other.toRational()
operator fun Long.div(other: Rational) = this.toRational() / other

operator fun Rational.compareTo(other: Long) = this.compareTo(other.toRational())
operator fun Rational.compareTo(other: Double) = this.toDouble().compareTo(other)
operator fun Long.compareTo(other: Rational) = this.toRational().compareTo(other)
operator fun Double.compareTo(other: Rational) = this.compareTo(other.toDouble())

fun Double.rationalise(): Rational {
    if (!isFinite()) {
        throw IllegalArgumentException("value is not finite: ${this}")
    }

    val bits = this.toBits()
    val s = if (((bits shr 63) == 0L)) 1 else -1
    val e = ((bits shr 52) and 0x7ffL).toInt()
    val eAdjusted = e - 1075
    val m = if ((e == 0)) (bits and 0xfffffffffffffL) shl 1 else (bits and 0xfffffffffffffL) or 0x10000000000000L
    return if (eAdjusted < 0) {
        Rational.make(BigInt.of(m * s), BigIntConstants.TWO.pow(-eAdjusted))
    } else {
        Rational.make(BigIntConstants.TWO.pow(eAdjusted) * BigInt.of(m * s), BigIntConstants.ONE)
    }
}

expect fun BigInt.isqrt(): BigInt

private val BIGINT_10 = BigInt.of(10)
private val RAT_10 = Rational.make(10, 1)

private fun powOf10(p: Long): BigInt {
    return BIGINT_10.pow(p)
}

private fun log10(value: BigInt): Int {
    return value.toString(10).length
}

fun Rational.formatToDecimal(maxWidth: Int = 8, prec: Int = 8): Pair<String, Boolean> {
    if (this == Rational.ZERO) {
        return Pair("0", true)
    }

    val intPart = this.ceil()
    if (intPart >= powOf10(maxWidth.toLong())) {
        // The number is larger than the accepted width
        val numIntDigits = log10(intPart)
        val s = (Rational.ONE_HALF + this / Rational.make(BIGINT_10.pow(numIntDigits - maxWidth), BigIntConstants.ONE)).floor().toString(10)
        return Pair("${s[0]}.${s.substring(1)}e${numIntDigits}", false)
    } else {
        val multiplier = powOf10(prec.toLong())
        val maxSize = this * Rational.make(multiplier, BigIntConstants.ONE)
        if (maxSize.denominator == BigIntConstants.ONE) {
            val s = maxSize.numerator.toString(10)
            val n = s.length - prec
            val suffixIndex = s.indexOfLast { ch -> ch != '0' }
            require(suffixIndex != -1)
            val res = if (n == 0) {
                "0.${s.substring(0, suffixIndex + 1)}"
            } else if (suffixIndex < n) {
                s.substring(0, n)
            } else {
                "${s.substring(0, n)}.${s.substring(n, suffixIndex + 1)}"
            }
            return Pair(res, true)
        } else {
            TODO("not implemented")
        }
    }
}
