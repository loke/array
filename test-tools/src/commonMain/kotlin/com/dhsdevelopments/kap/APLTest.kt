package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.complex.Complex
import com.dhsdevelopments.kap.optimiser.Optimiser
import com.dhsdevelopments.kap.optimiser.StandardOptimiser
import com.dhsdevelopments.kap.optimiser.ZeroOptimiser
import com.dhsdevelopments.mpbignum.BigInt
import com.dhsdevelopments.mpbignum.Rational
import com.dhsdevelopments.mpbignum.make
import com.dhsdevelopments.mpbignum.of
import kotlin.math.pow
import kotlin.test.*

class NearDouble(val expected: Double, val precision: Int = 4) {
    fun assertNear(v: Double, message: String? = null) {
        val dist = 10.0.pow(-precision)
        val messageWithPrefix = if (message == null) "" else ": ${message}"
        assertTrue(expected > v - dist && expected < v + dist, "Expected=${expected}, result=${v}${messageWithPrefix}")
    }
}

class NearComplex(val expected: Complex, val realPrecision: Int = 4, val imPrecision: Int = 4) : APLTest.InnerTest {
    fun assertNear(v: Complex, message: String? = null) {
        val realDist = 10.0.pow(-realPrecision)
        val imDist = 10.0.pow(-imPrecision)
        val messageWithPrefix = if (message == null) "" else ": ${message}"
        assertTrue(
            expected.re > v.re - realDist
                    && expected.re < v.re + realDist
                    && expected.im > v.im - imDist
                    && expected.im < v.im + imDist,
            "expected=${expected}, result=${v}${messageWithPrefix}")
    }

    override fun assertContent(result: APLValue, message: String?) {
        assertTrue(result is APLNumber, message)
        assertNear(result.asComplex(), message)
    }
}

abstract class APLTest {
    private val registeredEngines = ArrayList<Engine>()
    private var selectedOptimiser: Optimiser? = null

    @BeforeTest
    fun initAplTest() {
        selectedOptimiser = null
    }

    @AfterTest
    fun teardownAplTest() {
        val copy = ArrayList(registeredEngines)
        registeredEngines.clear()
        copy.forEach(Engine::close)
    }

    fun makeEngine(numComputeEngines: Int? = null, withStandardLib: Boolean = false): Engine {
        val engine = Engine(numComputeEngines = numComputeEngines, defaultOptimiser = selectedOptimiser)
        engine.addLibrarySearchPath("standard-lib")
        if (withStandardLib) {
            engine.parseAndEval(StringSourceLocation("use(\"standard-lib.kap\")"))
        }
        registerEngine(engine)
        return engine
    }

    fun registerEngine(engine: Engine) {
        registeredEngines.add(engine)
    }

    fun parseAndTestWithGeneric(expr: String, withStandardLib: Boolean = false, collapse: Boolean = true, numTasks: Int? = null, callback: (APLValue) -> Unit) {
        parseAPLExpression(expr.replace("{GENERIC}", ""), withStandardLib = withStandardLib, collapse = collapse, numTasks = numTasks).let { result ->
            callback(result)
        }
        parseAPLExpression(
            expr.replace("{GENERIC}", "int:ensureGeneric"),
            withStandardLib = withStandardLib,
            collapse = collapse,
            numTasks = numTasks).let { result ->
            callback(result)
        }
    }

    fun parseAPLExpression(
        expr: String,
        withStandardLib: Boolean = false,
        collapse: Boolean = true,
        numTasks: Int? = null,
        extraInit: ((Engine) -> Unit)? = null): APLValue {
        return parseAPLExpression2(expr, withStandardLib, collapse, numTasks, extraInit).first
    }

    fun parseAPLExpression2(
        expr: String,
        withStandardLib: Boolean = false,
        collapse: Boolean = true,
        numTasks: Int? = null,
        extraInit: ((Engine) -> Unit)? = null
    ): Pair<APLValue, Engine> {
        val engine = makeEngine(numTasks, withStandardLib = withStandardLib)
        if (extraInit != null) {
            extraInit(engine)
        }
        val result = engine.parseAndEval(StringSourceLocation(expr), collapseResult = collapse)
        return Pair(result, engine)
    }

    fun parseAPLExpressionWithOutput(
        expr: String,
        withStandardLib: Boolean = false,
        collapse: Boolean = true,
        numTasks: Int? = null
    ): Pair<APLValue, String> {
        val engine = makeEngine(numTasks, withStandardLib = withStandardLib)
        val output = StringBuilderOutput()
        engine.standardOutput = output
        val result = engine.parseAndEval(StringSourceLocation(expr))
        return engine.withThreadLocalAssigned {
            Pair(if (collapse) result.collapse() else result, output.buf.toString())
        }
    }

    fun assertArrayContent(expectedValue: Array<out Any>, value: APLValue, message: String? = null) {
        val prefix = if (message == null) "" else "${message}: "
        assertEquals(expectedValue.size, value.size, "Array dimensions mismatch")
        for (i in expectedValue.indices) {
            assertAPLValue(expectedValue[i], value.valueAt(i), "at index: ${i}: ${prefix}")
        }
    }

    fun assertArrayContentDouble(expectedValue: DoubleArray, value: APLValue, message: String? = null) {
        val prefix = if (message == null) "" else "${message}: "
        assertEquals(expectedValue.size, value.size, "Array dimensions mismatch")
        for (i in expectedValue.indices) {
            assertAPLValue(InnerDouble(expectedValue[i]), value.valueAt(i), prefix)
        }
    }

    fun assertDimension(expectDimensions: Dimensions, result: APLValue, message: String? = null) {
        val dimensions = result.dimensions
        val prefix = if (message == null) "" else "${message}: "
        assertTrue(result.dimensions.compareEquals(expectDimensions), "${prefix}expected dimension: $expectDimensions, actual $dimensions")
    }

    fun assertPairs(v: APLValue, vararg values: Array<Int>) {
        for (i in values.indices) {
            val cell = v.valueAt(i)
            val expectedValue = values[i]
            for (eIndex in expectedValue.indices) {
                assertSimpleNumber(expectedValue[eIndex].toLong(), cell.valueAt(eIndex))
            }
        }
    }

    fun assertSimpleNumber(expected: Long, value: APLValue, expr: String? = null) {
        val v = value.unwrapDeferredValue()
        val prefix = "Expected value: ${expected}, actual: ${value}"
        val exprMessage = if (expr == null) prefix else "${prefix}, expr: ${expr}"
        assertTrue(v is APLLong, exprMessage)
        assertEquals(expected, value.ensureNumber().asLong(), exprMessage)
    }

    fun assertDoubleWithRange(expected: Pair<Double, Double>, value: APLValue) {
        assertTrue(value.isScalar())
        val v = value.unwrapDeferredValue()
        assertTrue(v is APLDouble)
        val num = v.value
        assertTrue(expected.first <= num, "Comparison is not true: ${expected.first} <= ${num}")
        assertTrue(expected.second >= num, "Comparison is not true: ${expected.second} >= ${num}")
    }

    fun assertSimpleDouble(expected: Double, value: APLValue, message: String? = null) {
        assertAPLValue(InnerDouble(expected), value, message)
    }

    fun assertNearDouble(nearDouble: NearDouble, result: APLValue, message: String? = null) {
        nearDouble.assertNear(result.ensureNumber().asDouble(), message)
    }

    fun assertComplexWithRange(real: Pair<Double, Double>, imaginary: Pair<Double, Double>, result: APLValue) {
        assertTrue(result.isScalar())
        val complex = result.ensureNumber().asComplex()
        val message = "expected: ${real} ${imaginary}, actual: ${complex}"
        assertTrue(real.first <= complex.re && real.second >= complex.re, message)
        assertTrue(imaginary.first <= complex.im && imaginary.second >= complex.im, message)
    }

    fun assertNearComplex(expected: Pair<NearDouble, NearDouble>, result: APLValue, message: String? = null) {
        assertIs<APLComplex>(result, message)
        val v = result.value
        expected.first.assertNear(v.re, message)
        expected.second.assertNear(v.im, message)
    }

    fun assertSimpleComplex(expected: Complex, result: APLValue, message: String? = null) {
        assertTrue(result.isScalar(), message)
        val v = result.unwrapDeferredValue()
        assertTrue(v is APLNumber, message)
        assertEquals(expected, v.ensureNumber().asComplex(), message)
    }

    fun assertBigIntOrLong(expected: String, result: APLValue, message: String? = null) {
        assertAPLValue(InnerBigIntOrLong(expected), result, message)
    }

    fun assertBigIntOrLong(expected: Long, result: APLValue, message: String? = null) {
        assertAPLValue(InnerBigIntOrLong(expected), result, message)
    }

    fun assertBigIntOrLong(expected: BigInt, result: APLValue, message: String? = null) {
        assertAPLValue(InnerBigIntOrLong(expected), result, message)
    }

    fun assertRational(expected: Rational, result: APLValue, message: String? = null) {
        assertTrue(result is APLRational, "Got ${result}, expected ${expected}")
        assertEquals(expected, result.value, message)
    }

    fun assertString(expected: String, value: APLValue, message: String? = null) {
        val suffix = if (message != null) ": ${message}" else ""
        assertEquals(1, value.dimensions.size, "Expected rank-1, got: ${value.dimensions.size}${suffix}")
        val valueString = value.toStringValue()
        assertEquals(expected, valueString, "Expected '${expected}', got: '${valueString}'${suffix}")
    }

    fun assertChar(code: Int, value: APLValue, message: String? = null) {
        assertTrue(value is APLChar, message)
        assertEquals(code, value.value, message)
    }

    fun assertAPLNull(value: APLValue) {
        assertDimension(dimensionsOfSize(0), value)
        assertEquals(0, value.dimensions[0])
    }

    fun assertAPLNil(value: APLValue, message: String? = null) {
        assertSame(value, APLNilValue, message)
    }

    fun assertAPLValue(expected: Any, result: APLValue, message: String? = null) {
        when (expected) {
            // Note: The ordering here is important, since the JS target treats all number types as the same type.
            //       Because of this, the check for Long has to be done first, and we throw an explicit error if
            //       the value is a Double since that will fail on JS but work everywhere else.
            is Long -> assertSimpleNumber(expected, result, message)
            is Int -> assertSimpleNumber(expected.toLong(), result, message)
            is Double -> throw IllegalArgumentException("Plain doubles are not supported")
            is Complex -> assertSimpleComplex(expected, result, message)
            is String -> assertString(expected, result, message)
            is NearDouble -> assertNearDouble(expected, result, message)
            is InnerTest -> expected.assertContent(result, message)
            is Rational -> assertRational(expected, result, message)
            else -> throw IllegalArgumentException("No support for comparing values of type: ${result::class.simpleName}")
        }
    }

    fun assertSymbolName(engine: Engine, name: String, value: APLValue) {
        assertSame(engine.internSymbol(name), value.ensureSymbol().value)
    }

    fun assertSymbolNameCoreNamespace(engine: Engine, name: String, value: APLValue) {
        assertSame(engine.internSymbol(name, engine.coreNamespace), value.ensureSymbol().value)
    }

    fun assertEnclosed(expected: Any, result: APLValue, message: String? = null) {
        assertDimension(emptyDimensions(), result, message)
        assertFalse(result.isAtomic, message)
        assertAPLValue(expected, result.valueAt(0), message)
    }

    fun assert1DArray(expected: Array<out Any>, result: APLValue?, message: String? = null) {
        assertNotNull(result, "Expected an array, got null")
        assertDimension(dimensionsOfSize(expected.size), result, message)
        assertArrayContent(expected, result, message)
    }

    @BeforeTest
    fun initTest() {
        nativeTestInit()
    }

    interface InnerTest {
        fun assertContent(result: APLValue, message: String? = null)
    }

    open inner class InnerArray(val expectedDimensions: Dimensions, val expected: Array<out Any>) : InnerTest {
        override fun assertContent(result: APLValue, message: String?) {
            assertDimension(expectedDimensions, result, message)
            assertArrayContent(expected, result, message)
        }
    }

    inner class InnerArrayN(expectedDimensions: Dimensions, vararg expected: Any) : InnerArray(expectedDimensions, expected)

    open inner class Inner1D(val expected: Array<out Any>) : InnerTest {
        override fun assertContent(result: APLValue, message: String?) {
            assert1DArray(expected, result, message)
        }
    }

    inner class Inner1Dn(vararg expected: Any) : Inner1D(expected)

    inner class InnerAPLNull : InnerTest {
        override fun assertContent(result: APLValue, message: String?) {
            assertDimension(dimensionsOfSize(0), result, message)
        }
    }

    inner class InnerAPLNil : InnerTest {
        override fun assertContent(result: APLValue, message: String?) {
            assertAPLNil(result, message)
        }
    }

    inner class InnerDouble(val expectedDouble: Double) : InnerTest {
        override fun assertContent(result: APLValue, message: String?) {
            assertTrue(result is APLDouble, "Result should be double, was: ${result}")
            assertEquals(expectedDouble, result.ensureNumber().asDouble(), message)
        }
    }

    inner class InnerDoubleOrLong(val expectedDouble: Double) : InnerTest {
        override fun assertContent(result: APLValue, message: String?) {
            assertEquals(expectedDouble, result.ensureNumber().asDouble(), message)
        }
    }

    inner class InnerBigIntOrLong(val expected: BigInt) : InnerTest {
        constructor(expectedString: String) : this(BigInt.of(expectedString))
        constructor(expectedLong: Long) : this(BigInt.of(expectedLong))

        override fun assertContent(result: APLValue, message: String?) {
            val v: String = when (result) {
                is APLLong -> result.value.toString()
                is APLBigInt -> result.value.toString()
                else -> {
                    val msgSuffix = if (message == null) "" else ": ${message}"
                    fail("Unexpected type: ${result}${msgSuffix}")
                }
            }
            assertEquals(expected.toString(), v, "${expected} != ${v}, ${message}")
        }
    }

    inner class InnerRational(val expected: Rational) : InnerTest {
        constructor(num: Long, den: Long) : this(Rational.make(num, den))

        override fun assertContent(result: APLValue, message: String?) {
            assertRational(expected, result, message)
        }
    }

    inner class InnerChar(val expected: Int) : InnerTest {
        constructor(expected: Char) : this(expected.code)

        override fun assertContent(result: APLValue, message: String?) {
            assertIs<APLChar>(result, message)
            assertEquals(expected, result.value, message)
        }
    }

    fun evalWithDebugFunctions(src: String): APLValue {
        return evalWithDebugFunctionsOutput(src).first
    }

    fun evalWithDebugFunctionsOutput(src: String): Pair<APLValue, String> {
        val engine = makeEngine()
        val namespace = engine.coreNamespace
        engine.registerOperator(namespace.internAndExport("abc"), AbcOperator())
        engine.registerFunction(namespace.internAndExport("def"), TestFunction())
        engine.registerFunction(namespace.internAndExport("arg1Test"), Arg1TestFunction())
        engine.registerFunction(namespace.internAndExport("arg2Test"), Arg2TestFunction())
        val output = StringBuilderOutput()
        engine.standardOutput = output
        val result = engine.parseAndEval(StringSourceLocation(src))
        return Pair(result, output.buf.toString())
    }

    fun assertLabels(expected: List<List<String?>?>, result: APLValue) {
        assertEquals(expected.size, result.dimensions.size)
        val labels = result.metadata.labels
        assertNotNull(labels)
        assertEquals(expected.size, labels.labels.size)
        expected.forEachIndexed { i, l ->
            val axisLabels = labels.labels[i]
            if (l == null) {
                assertNull(axisLabels, "Labels at axis ${i} should be null")
            } else {
                assertNotNull(axisLabels, "Labels at axis ${i} should not be null")
                assertEquals(l.size, axisLabels.size)
                l.forEachIndexed { posIndex, s ->
                    val axisLabel = axisLabels[posIndex]
                    if (s == null) {
                        assertNull(axisLabel, "Axis label at axis ${i} position ${posIndex} should be null")
                    } else {
                        assertNotNull(axisLabel, "Axis label at axis ${i} position ${posIndex} should not be null")
                        assertEquals(s, axisLabel.title, "Axis label at axis ${i} position ${posIndex} should be ${s}, was: ${axisLabel.title}")
                    }
                }
            }
        }
    }

    fun assertLabelList(expected: List<String?>, actual: List<AxisLabel?>?) {
        assertNotNull(actual)
        assertEquals(expected.size, actual.size)
        expected.forEachIndexed { i, v ->
            val axisLabel = actual[i]
            if (v == null) {
                assertNull(axisLabel)
            } else {
                assertNotNull(axisLabel)
                assertEquals(v, axisLabel.title)
            }
        }
    }

    fun assertRenderOutput(expected: String, src: String) {
        val out = renderOutput(src)
        assertEquals("${expected}\n", out)
    }

    fun renderOutput(src: String): String {
        val (_, out) = parseAPLExpressionWithOutput("io:print o3:format ${src}", withStandardLib = true)
        return out
    }

    class AbcOperator : APLOperatorOneArg {
        override fun combineFunction(fn: APLFunction, pos: FunctionInstantiation): APLFunctionDescriptor {
            return AbcFunctionDescriptor(fn)
        }
    }

    fun withOptimiser(opt: Optimiser, fn: () -> Unit) {
        val prevOptimiser = selectedOptimiser
        try {
            selectedOptimiser = opt
            fn()
        } finally {
            selectedOptimiser = prevOptimiser
        }
    }

    fun withAllOptimisers(fn: () -> Unit) {
        withOptimiser(ZeroOptimiser, fn)
        withOptimiser(StandardOptimiser, fn)
    }

    class AbcFunctionDescriptor(val fn: APLFunction) : APLFunctionDescriptor {
        class AbcFunctionDescriptorImpl(fn: APLFunction, pos: FunctionInstantiation) : APLFunction(pos, listOf(fn)) {
            private val fn get() = fns[0]

            init {
                SaveStackSupport(this)
            }

            override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
                val result = fn.eval1Arg(context, a, null)
                val axisLong = if (axis == null) 0 else axis.ensureNumber(pos).asLong(pos)
                return (result.ensureNumber(pos).asLong(pos) * 1000 + axisLong * 1000000).makeAPLNumber()
            }

            override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
                val result = fn.eval2Arg(context, a, b, null)
                val axisLong = if (axis == null) 0 else axis.ensureNumber(pos).asLong(pos)
                return (result.ensureNumber(pos).asLong(pos) * 1000 + axisLong * 1000000).makeAPLNumber()
            }

            override fun copy(fns: List<APLFunction>): APLFunction {
                return AbcFunctionDescriptorImpl(fns[0], instantiation)
            }
        }

        override fun make(instantiation: FunctionInstantiation) = AbcFunctionDescriptorImpl(fn, instantiation)
    }

    class TestFunction : APLFunctionDescriptor {
        class TestFunctionImpl(pos: FunctionInstantiation) : APLFunction(pos) {
            override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
                val aLong = a.ensureNumber(pos).asLong(pos)
                val axisLong = if (axis == null) 0 else axis.ensureNumber(pos).asLong(pos)
                return (aLong * 10 + axisLong * 100).makeAPLNumber()
            }

            override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
                val aLong = a.ensureNumber(pos).asLong(pos)
                val bLong = b.ensureNumber(pos).asLong(pos)
                val axisLong = if (axis == null) 0 else axis.ensureNumber(pos).asLong(pos)
                return (aLong + bLong * 10 + axisLong * 100).makeAPLNumber()
            }
        }

        override fun make(instantiation: FunctionInstantiation) = TestFunctionImpl(instantiation)
    }

    class Arg1TestFunction : APLFunctionDescriptor {
        class Arg1TestFunctionImpl(pos: FunctionInstantiation) : APLFunction(pos) {
            override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
                return a
            }
        }

        override fun make(instantiation: FunctionInstantiation) = Arg1TestFunctionImpl(instantiation)
    }

    class Arg2TestFunction : APLFunctionDescriptor {
        class Arg2TestFunctionImpl(pos: FunctionInstantiation) : APLFunction(pos) {
            override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
                return a
            }
        }

        override fun make(instantiation: FunctionInstantiation) = Arg2TestFunctionImpl(instantiation)
    }
}

expect fun nativeTestInit()
expect fun tryGc()
