package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.mpbignum.*
import kotlin.random.Random

//time:measureTime { (n?n){+/(∨/⍵∘.=⍺)/⍵}n?n←40000 }
//Total time: 7.682
//
//799980000

class RandomAPLFunction : APLFunctionDescriptor {
    class RandomAPLFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            return recurseMonadic(context.engine.random, a)
        }

        private fun throwInvalidType(v: String): Nothing =
            throwAPLException(APLIllegalArgumentException("Argument must be an integer greater than or equal to 0. Got: ${v}", pos))

        private fun recurseMonadic(random: Random, a: APLValue): APLValue {
            val a0 = a.unwrapDeferredValue()
            return if (a0 is APLSingleValue) {
                when (a0) {
                    is APLLong -> randomFromInt(random, a0.value)
                    is APLBigInt -> randomFromBigint(random, a0.value)
                    is APLRational -> if (a0.value.isInteger()) {
                        randomFromBigint(random, a0.value.numerator)
                    } else {
                        throwInvalidType(a0.formatted(FormatStyle.PLAIN))
                    }
                    else -> throwInvalidType(a0.formatted(FormatStyle.PLAIN))
                }
            } else {
                val d = a0.dimensions
                if (d.size == 0) {
                    EnclosedAPLValue.make(recurseMonadic(random, a0.valueAt(0)))
                } else if (d.contentSize() == 0) {
                    APLArrayImpl(d, Array(0) { APLNullValue })
                } else {
                    computeRandomArray(random, a0, d)
                }
            }
        }

        private fun randomFromBigint(random: Random, value: BigInt): APLValue {
            return when {
                value == BigIntConstants.ZERO -> random.nextDouble().makeAPLNumber()
                value < 0 -> throwInvalidType(value.toString())
                value.rangeInLong() -> random.nextLong(value.toLong()).makeAPLNumber()
                else -> throwAPLException(APLEvalException("Random from bigint is not supported", pos))
            }
        }

        private fun computeRandomArray(random: Random, a0: APLValue, d: Dimensions): APLArray {
            return when (val firstValue = a0.valueAt(0).unwrapDeferredValue()) {
                is APLLong -> {
                    val v = firstValue.value
                    if (v > 0) {
                        computeRestRandomArrayLong(random, v, a0, d)
                    } else if (v == 0L) {
                        computeRestRandomArrayDouble(random, a0, d)
                    } else {
                        throwInvalidType(firstValue.formatted(FormatStyle.PLAIN))
                    }
                }
                else -> computeRestRandomArrayGeneric(random, firstValue, a0, d)
            }
        }

        /**
         * Handle the special case of an array filled with longs
         */
        private fun computeRestRandomArrayLong(random: Random, firstValue: Long, a0: APLValue, d: Dimensions): APLArray {
            val resLongArray = LongArray(d.contentSize())
            resLongArray[0] = random.nextLong(firstValue)
            var i = 1
            while (i < resLongArray.size) {
                val v = a0.valueAt(i).unwrapDeferredValue()
                if (v is APLLong) {
                    val vLong = v.value
                    if (vLong == 0L) break
                    if (vLong < 0) throwInvalidType(v.formatted(FormatStyle.PLAIN))
                    resLongArray[i] = random.nextLong(vLong)
                } else {
                    break
                }
                i++
            }
            return if (i == resLongArray.size) {
                APLArrayLong(d, resLongArray)
            } else {
                val res = Array(resLongArray.size) { newIndex ->
                    if (newIndex < i) {
                        resLongArray[newIndex].makeAPLNumber()
                    } else {
                        recurseMonadic(random, a0.valueAt(newIndex))
                    }
                }
                APLArrayImpl(d, res)
            }
        }

        private fun isIntegerZero(value: APLValue): Boolean {
            return (value is APLLong && value.value == 0L)
                    || (value is APLBigInt && value.value == BigIntConstants.ZERO)
                    || (value is APLRational && value.value == Rational.ZERO)
        }

        /**
         * Handle the special case of an array filled with zeroes
         */
        private fun computeRestRandomArrayDouble(random: Random, a0: APLValue, d: Dimensions): APLArray {
            val resDoubleArray = DoubleArray(d.contentSize())
            resDoubleArray[0] = random.nextDouble()
            var i = 1
            while (i < resDoubleArray.size) {
                val v = a0.valueAt(i).unwrapDeferredValue()
                if (!isIntegerZero(v)) break
                resDoubleArray[i] = random.nextDouble()
                i++
            }
            return if (i == resDoubleArray.size) {
                APLArrayDouble(d, resDoubleArray)
            } else {
                val res = Array(resDoubleArray.size) { newIndex ->
                    if (newIndex < i) {
                        resDoubleArray[newIndex].makeAPLNumber()
                    } else {
                        recurseMonadic(random, a0.valueAt(newIndex))
                    }
                }
                APLArrayImpl(d, res)
            }
        }

        private fun computeRestRandomArrayGeneric(random: Random, firstValue: APLValue, a0: APLValue, d: Dimensions): APLArray {
            return APLArrayImpl(d, Array(d.contentSize()) { i ->
                val v = if (i == 0) firstValue else a0.valueAt(i)
                recurseMonadic(random, v)
            })
        }

        private fun randomFromInt(random: Random, v: Long): APLNumber {
            return when {
                v > 0 -> random.nextLong(v).makeAPLNumber()
                v == 0L -> random.nextDouble().makeAPLNumber()
                else -> throwAPLException(APLIllegalArgumentException("Invalid random range: ${v}", pos))
            }
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val aInt = a.ensureNumber(pos).asInt(pos)
            val bLong = b.ensureNumber(pos).asLong(pos)
            if (aInt < 0) {
                throwAPLException(IncompatibleTypeException("A should not be negative, was: ${aInt}", pos))
            }
            if (bLong < 0) {
                throwAPLException(IncompatibleTypeException("B should not be negative, was: ${bLong}", pos))
            }
            if (aInt > bLong) {
                throwAPLException(
                    IncompatibleTypeException(
                        "A should not be greater than B. A: ${aInt}, B: ${bLong}",
                        pos))
            }
            if (aInt == 0) {
                return APLArrayLong(dimensionsOfSize(0), longArrayOf())
            }

            val result = randSubsetC2(context.engine.random, aInt, bLong)
            return APLArrayLong(dimensionsOfSize(result.size), result)
        }

        private fun randSubsetC2(random: Random, a: Int, b: Long): LongArray {
            val rp = LongArray(a) { i -> i.toLong() }
            val map = HashMap<Long, Long>(0)
            repeat(a) { i ->
                val j = random.nextLong(b - i) + i
                if (j < a) {
                    val jInt = j.toInt()
                    val c = rp[jInt]
                    rp[jInt] = rp[i]
                    rp[i] = c
                } else {
                    rp[i] = (map[j] ?: j).also { map[j] = rp[i] }
                }
            }
            return rp
        }

        override val name1Arg get() = "deal"
        override val name2Arg get() = "random"
    }

    override fun make(instantiation: FunctionInstantiation) = RandomAPLFunctionImpl(instantiation)
}
