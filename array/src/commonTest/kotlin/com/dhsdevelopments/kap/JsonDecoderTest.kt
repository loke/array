package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.json.*
import kotlin.test.*

class JsonDecoderTest {
    @Test
    fun testJsonElementComparators() {
        assertEquals(JsonNumber(1234), JsonNumber(1234))
        assertNotEquals(JsonNumber(1234), JsonNumber(987))

        assertEquals(JsonString("abc"), JsonString("abc"))
        assertNotEquals(JsonString("abc"), JsonString("def"))

        assertEquals(JsonBoolean(true), JsonBoolean(true))
        assertEquals(JsonBoolean(false), JsonBoolean(false))
        assertNotEquals(JsonBoolean(true), JsonBoolean(false))
        assertNotEquals(JsonBoolean(false), JsonBoolean(true))

        assertEquals(JsonNull, JsonNull)

        assertEquals(
            JsonArray(listOf(JsonNumber(1234), JsonString("aa"), JsonNumber(1))),
            JsonArray(listOf(JsonNumber(1234), JsonString("aa"), JsonNumber(1))))
        assertNotEquals(
            JsonArray(listOf(JsonNumber(1234), JsonString("aa"), JsonNumber(1))),
            JsonArray(listOf(JsonNumber(1235), JsonString("aa"), JsonNumber(1))))

        assertEquals(
            JsonObject(
                listOf(
                    Pair("a", JsonString("abc")),
                    Pair("b", JsonString("test")))),
            JsonObject(
                listOf(
                    Pair("a", JsonString("abc")),
                    Pair("b", JsonString("test")))))
        assertNotEquals(
            JsonObject(
                listOf(
                    Pair("a", JsonString("abc")),
                    Pair("b", JsonString("test")))),
            JsonObject(
                listOf(
                    Pair("a", JsonString("abc")),
                    Pair("b", JsonString("testz")))))
    }

    @Test
    fun testSimpleDouble() {
        // Doubles should always decode as doubles
        val res = makeJsonDecoder().decodeJsonFromString("1234.0")
        assertEquals(JsonNumber(1234.0), res)
    }

    @Test
    fun testSimpleInt() {
        val res = makeJsonDecoder().decodeJsonFromString("1234")
        assertIs<JsonNumber>(res)
        // Need to convert here since it's valid for a decoder to return either
        // an integre or a double in this case
        assertEquals(1234, res.value.toInt())
    }

    @Test
    fun testSimpleString() {
        val res = makeJsonDecoder().decodeJsonFromString("\"foo\"")
        assertEquals(JsonString("foo"), res)
    }

    @Test
    fun testSimpleBoolean() {
        assertEquals(JsonBoolean(true), makeJsonDecoder().decodeJsonFromString("true"))
        assertEquals(JsonBoolean(false), makeJsonDecoder().decodeJsonFromString("false"))
    }

    @Test
    fun testSimpleNull() {
        assertEquals(JsonNull, makeJsonDecoder().decodeJsonFromString("null"))
    }

    @Test
    fun testSyntaxError() {
        assertFailsWith<JsonParseException> {
            makeJsonDecoder().decodeJsonFromString("foo")
        }
    }

    @Test
    fun testJsonArray() {
        val res = makeJsonDecoder().decodeJsonFromString("[1,2,\"a\",\"b\",null,true,false,12,1234.5]")
        assertIs<JsonArray>(res)
        val values = res.values
        assertEquals(9, values.size)
        assertJsonNumber(1.0, values[0])
        assertJsonNumber(2.0, values[1])
        assertEquals(JsonString("a"), values[2])
        assertEquals(JsonString("b"), values[3])
        assertEquals(JsonNull, values[4])
        assertEquals(JsonBoolean(true), values[5])
        assertEquals(JsonBoolean(false), values[6])
        assertJsonNumber(12.0, values[7])
        assertJsonNumber(1234.5, values[8])
    }

    @Test
    fun testEmptyList() {
        val res = makeJsonDecoder().decodeJsonFromString("[]")
        assertEquals(JsonArray(emptyList()), res)
    }

    @Test
    fun testEmptyObject() {
        val res = makeJsonDecoder().decodeJsonFromString("{}")
        assertEquals(JsonObject(emptyList()), res)
    }

    @Test
    fun testJsonObject() {
        val src =
            """
            |{
            |  "a": "test",
            |  "b": "other string",
            |  "c": 1,
            |  "d": [ 1,2,3 ],
            |  "e": [ "foo", "bar", false, null, [1,2,"q",1.1] ],
            |  "f": true,
            |  "g": { "aa": "abcd", "bb": 123, "cc": [ "foo", "test0"] },
            |  "h": 3.3
            |}
            """.trimMargin()
        val res = makeJsonDecoder().decodeJsonFromString(src)
        assertIs<JsonObject>(res)
        val objectValues = res.values
        assertEquals(8, objectValues.size)
        assertKeyValue("a", JsonString("test"), objectValues[0])
        assertKeyValue("b", JsonString("other string"), objectValues[1])
        assertKeyValueNumber("c", 1.0, objectValues[2])
        objectValues[3].let { v ->
            val arrayValue = v.second
            assertIs<JsonArray>(arrayValue)
            val values = arrayValue.values
            assertEquals(3, values.size)
            assertJsonNumber(1.0, values[0])
            assertJsonNumber(2.0, values[1])
            assertJsonNumber(3.0, values[2])
        }
        objectValues[4].let { v ->
            assertEquals("e", v.first)
            val arrayValue = v.second
            assertIs<JsonArray>(arrayValue)
            val values = arrayValue.values
            assertEquals(5, values.size)
            assertEquals(JsonString("foo"), values[0])
            assertEquals(JsonString("bar"), values[1])
            assertEquals(JsonBoolean(false), values[2])
            assertEquals(JsonNull, values[3])
            values[4].let { array ->
                assertIs<JsonArray>(array)
                val elements = array.values
                assertEquals(4, elements.size)
                assertJsonNumber(1.0, elements[0])
                assertJsonNumber(2.0, elements[1])
                assertEquals(JsonString("q"), elements[2])
                assertJsonNumber(1.1, elements[3])
            }
        }
        assertKeyValue("f", JsonBoolean(true), objectValues[5])
        objectValues[6].let { v ->
            assertEquals("g", v.first)
            val objectValue = v.second
            assertIs<JsonObject>(objectValue)
            val values = objectValue.values
            assertKeyValue("aa", JsonString("abcd"), values[0])
            assertKeyValueNumber("bb", 123.0, values[1])
            values[2].let { innerElement ->
                assertEquals("cc", innerElement.first)
                val innerData = innerElement.second
                assertIs<JsonArray>(innerData)
                val innerArray = innerData.values
                assertEquals(2, innerArray.size)
                assertEquals(JsonString("foo"), innerArray[0])
                assertEquals(JsonString("test0"), innerArray[1])
            }
        }
        assertKeyValueNumber("h", 3.3, objectValues[7])
    }

    @Test
    fun testUnterminatedList() {
        assertFailsWith<JsonParseException> {
            makeJsonDecoder().decodeJsonFromString("[1,2")
        }
    }

    private fun assertJsonNumber(expected: Double, value: Any) {
        assertIs<JsonNumber>(value)
        assertEquals(expected, value.value.toDouble())
    }

    private fun assertKeyValue(expectedKey: String, expectedValue: Any, value: Any) {
        assertIs<Pair<String, Any>>(value)
        assertEquals(expectedKey, value.first)
        assertEquals(expectedValue, value.second)
    }

    private fun assertKeyValueNumber(expectedKey: String, expectedValue: Double, value: Any) {
        assertIs<Pair<String, Any>>(value)
        assertEquals(expectedKey, value.first)
        assertJsonNumber(expectedValue, value.second)
    }
}
