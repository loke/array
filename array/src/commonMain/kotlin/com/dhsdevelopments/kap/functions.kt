package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.builtins.defaultReduceDoubleToDouble
import com.dhsdevelopments.kap.builtins.defaultReduceGeneric
import com.dhsdevelopments.kap.builtins.defaultReduceLongToLong
import kotlin.jvm.JvmInline

/**
 * Set of flags that describes what various properties of the function. The instance is a bitmap where each
 * flag specifies that it is guaranteed that the function will behave in a certain way.
 */
@JvmInline
value class OptimisationFlags(val flags: Int) {
    /**
     * Indicates that monadic invocations when passed a long will return a long.
     */
    val is1ALong get() = (flags and OPTIMISATION_FLAG_1ARG_LONG) != 0

    /**
     * Indicates that monadic invocations when passed a double will return a double.
     */
    val is1ADouble get() = (flags and OPTIMISATION_FLAG_1ARG_DOUBLE) != 0

    /**
     * Indicates that dyadic invocations with two longs will return a long.
     */
    val is2ALongLong get() = (flags and OPTIMISATION_FLAG_2ARG_LONG_LONG) != 0

    /**
     * Indicates that dyadic invocations with two doubles will return a double.
     */
    val is2ADoubleDouble get() = (flags and OPTIMISATION_FLAG_2ARG_DOUBLE_DOUBLE) != 0

    /**
     * Indicates that dyadic invocations of the function follows the standard floating point conversion rules.
     * This means that computations on a mix of doubles and longs will return doubles.
     */
    val isFloatConversionRules get() = (flags and OPTIMISATION_FLAG_FLOAT_CONVERSION_RULES) != 0

    val is2ADFloatConversionRules
        get() = (flags and (OPTIMISATION_FLAG_2ARG_DOUBLE_DOUBLE or OPTIMISATION_FLAG_FLOAT_CONVERSION_RULES)) ==
                (OPTIMISATION_FLAG_2ARG_DOUBLE_DOUBLE or OPTIMISATION_FLAG_FLOAT_CONVERSION_RULES)

    private fun flagsString(): String {
        val flagMap = listOf(
            OPTIMISATION_FLAG_1ARG_LONG to "1ALong",
            OPTIMISATION_FLAG_1ARG_DOUBLE to "1ADouble",
            OPTIMISATION_FLAG_2ARG_LONG_LONG to "2ALongLong",
            OPTIMISATION_FLAG_2ARG_DOUBLE_DOUBLE to "2ADoubleDouble",
            OPTIMISATION_FLAG_FLOAT_CONVERSION_RULES to "floatConversionRules")
        val flagsString = flagMap.filter { (value, _) -> (flags and value) != 0 }.joinToString(", ") { it.second }
        return "OptimisationFlags(flags=0x${flags.toString(16)}, values: ${flagsString})"
    }

    /**
     * Return the intersection of this flag set and [other].
     */
    fun andWith(other: OptimisationFlags) = OptimisationFlags(flags and other.flags)

    /**
     * Return the union of this flag and [other].
     */
    fun orWith(other: OptimisationFlags) = OptimisationFlags(flags or other.flags)

    /**
     * Return the optimisation flags that apply to monadic invocations
     */
    val masked1Arg get() = OptimisationFlags(flags and OPTIMISATION_FLAGS_1ARG_MASK)

    /**
     * Return the optimisation flags that apply to dyadic invocations
     */
    val masked2Arg get() = OptimisationFlags(flags and OPTIMISATION_FLAGS_2ARG_MASK)

    /**
     * Returns a string representation of the optimisation flags.
     */
    override fun toString() = flagsString()

    companion object {
        const val OPTIMISATION_FLAG_1ARG_LONG = 0x1
        const val OPTIMISATION_FLAG_1ARG_DOUBLE = 0x2
        const val OPTIMISATION_FLAG_2ARG_LONG_LONG = 0x4
        const val OPTIMISATION_FLAG_2ARG_DOUBLE_DOUBLE = 0x8
        const val OPTIMISATION_FLAG_FLOAT_CONVERSION_RULES = 0x10

        const val OPTIMISATION_FLAGS_1ARG_MASK = OPTIMISATION_FLAG_1ARG_LONG or OPTIMISATION_FLAG_1ARG_DOUBLE
        const val OPTIMISATION_FLAGS_2ARG_MASK =
            OPTIMISATION_FLAG_2ARG_LONG_LONG or OPTIMISATION_FLAG_2ARG_DOUBLE_DOUBLE or OPTIMISATION_FLAG_FLOAT_CONVERSION_RULES
    }
}

/**
 * Class representing a function in Kap. Any subclass of this class that contains
 * a reference to another function should store this reference in the [fns] property.
 * This ensures that any closures created from this function will properly delegate
 * to dependent functions.
 *
 * @param instantiation The instantiation information, including the position and environment, where the function was defined
 * @param fns A list of functions that is used to implement this function.
 */
abstract class APLFunction(instantiation: FunctionInstantiation, val fns: List<APLFunction> = emptyList()) {
    // We want to extract Position instance here, since this member is used a lot so this improves
    // performance by eliminating one level of indirection
    val pos = instantiation.pos
    val instantiationEnv = instantiation.env
    val instantiationEngine = instantiation.engine
    val instantiation get() = FunctionInstantiation(instantiationEngine, pos, instantiationEnv)

    ///////////////////////////////////
    // 1-arg evaluation functions
    ///////////////////////////////////

    open fun evalArgsAndCall1Arg(context: RuntimeContext, rightArgs: Instruction): APLValue {
        val rightValue = rightArgs.evalWithContext(context)
        return eval1Arg(context, rightValue, null)
    }

    open fun evalArgsAndCall1ArgDiscardResult(context: RuntimeContext, rightArgs: Instruction) {
        val rightValue = rightArgs.evalWithContext(context)
        eval1ArgDiscardResult(context, rightValue, null)
    }

    open fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue =
        throwAPLException(Unimplemented1ArgException(pos))

    open fun eval1ArgDiscardResult(context: RuntimeContext, a: APLValue, axis: APLValue?) {
        eval1Arg(context, a, axis).collapse(withDiscard = true)
    }

    ///////////////////////////////////
    // 2-arg evaluation functions
    ///////////////////////////////////

    open fun evalArgsAndCall2Arg(context: RuntimeContext, leftArgs: Instruction, rightArgs: Instruction): APLValue {
        val rightValue = rightArgs.evalWithContext(context)
        val leftValue = leftArgs.evalWithContext(context)
        return eval2Arg(context, leftValue, rightValue, null)
    }

    open fun evalArgsAndCall2ArgDiscardResult(context: RuntimeContext, leftArgs: Instruction, rightArgs: Instruction) {
        val rightValue = rightArgs.evalWithContext(context)
        val leftValue = leftArgs.evalWithContext(context)
        eval2ArgDiscardResult(context, leftValue, rightValue, null)
    }

    open fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue =
        throwAPLException(Unimplemented2ArgException(pos))

    open fun eval2ArgDiscardResult(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?) {
        eval2Arg(context, a, b, axis).collapse(withDiscard = true)
    }

    /**
     * Return the identity value for this function
     *
     * @throws IncompatibleTypeException is the function does not have an identity value
     */
    open fun identityValue(): APLValue =
        throwAPLException(APLEvalException("Function does not have an identity value", pos))

    open fun deriveBitwise(): APLFunctionDescriptor? = null

    open val optimisationFlags get() = OptimisationFlags(0)

    open fun eval1ArgLong(context: RuntimeContext, a: Long, axis: APLValue?): Long =
        throw IllegalStateException("Illegal call to specialised function: ${this::class.simpleName}")

    open fun eval1ArgDouble(context: RuntimeContext, a: Double, axis: APLValue?): Double =
        throw IllegalStateException("Illegal call to specialised function: ${this::class.simpleName}")

    open fun eval2ArgLongToLongWithAxis(context: RuntimeContext, a: Long, b: Long, axis: APLValue?): Long =
        throw IllegalStateException("Illegal call to specialised function: ${this::class.simpleName}")

    open fun eval2ArgDoubleToDoubleWithAxis(context: RuntimeContext, a: Double, b: Double, axis: APLValue?): Double =
        throw IllegalStateException("Illegal call to specialised function: ${this::class.simpleName}")

    open fun evalInverse1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue =
        throwAPLException(InverseNotAvailable(pos))

    /**
     * Compute `x` given the equation `a FN x = b`.
     *
     * @throws InverseNotAvailable if the inverse cannot be computed
     */
    open fun evalInverse2ArgB(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue =
        throwAPLException(InverseNotAvailable(pos))

    /**
     * Compute `x` given the equation `x FN b = a`.
     *
     * @throws InverseNotAvailable if the inverse cannot be computed
     */
    open fun evalInverse2ArgA(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue =
        throwAPLException(InverseNotAvailable(pos))

    open fun computeClosure(parser: APLParser): Pair<APLFunction, List<Instruction>> {
        return if (fns.isEmpty()) {
            Pair(this, emptyList())
        } else {
            val closureList = fns.map { fn -> fn.computeClosure(parser) }
            val instrs = closureList.flatMap(Pair<APLFunction, List<Instruction>>::second)
            if (instrs.isEmpty()) {
                Pair(this, emptyList())
            } else {
                val newFn = copy(closureList.map(Pair<APLFunction, List<Instruction>>::first))
                Pair(newFn, instrs)
            }
        }
    }

    open fun copy(fns: List<APLFunction>): APLFunction {
        throw NotImplementedError("copy function must be implemented. class = ${this::class.simpleName}")
    }

    open fun evalWithStructuralUnder1Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue =
        throwAPLException(StructuralUnderNotSupported(pos))

    open fun evalWithStructuralUnder2Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue =
        throwAPLException(StructuralUnderNotSupported(pos))

    open fun capturedEnvironments(): List<Environment> = emptyList()

    fun allCapturedEnvironments(): List<Environment> {
        val result = ArrayList<Environment>()
        iterateFunctionTree { fn ->
            result.addAll(fn.capturedEnvironments())
        }
        return result
    }

    fun markEscapeEnvironment() {
        allCapturedEnvironments().forEach(Environment::markCanEscape)
    }

    open val name1Arg: String get() = this::class.simpleName ?: "unnamed"
    open val name2Arg: String get() = this::class.simpleName ?: "unnamed"

    open val inverseName1Arg: String get() = "inverse [${name1Arg}]"
    open val inverseName2Arg: String get() = "inverse [${name2Arg}]"

    fun inversibleStructuralUnder1Arg(underFn: APLFunction, baseFn: APLFunction, context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
        val v = underFn.eval1Arg(context, a, axis)
        val baseRes = baseFn.eval1Arg(context, v, null)
        return underFn.evalInverse1Arg(context, baseRes, axis)
    }

    fun inversibleStructuralUnder2Arg(
        underFn: APLFunction,
        baseFn: APLFunction,
        context: RuntimeContext,
        a: APLValue,
        b: APLValue,
        axis: APLValue?
    ): APLValue {
        val v = underFn.eval2Arg(context, a, b, axis)
        val baseRes = baseFn.eval1Arg(context, v, null)
        return underFn.evalInverse2ArgB(context, a, baseRes, axis)
    }

    open fun reduceLongToLong(
        context: RuntimeContext,
        arg: APLValue,
        offset: Int,
        sizeAlongAxis: Int,
        stepLength: Int,
        pos: Position,
        savedStack: StorageStack.StorageStackFrame?,
        functionAxis: APLValue?
    ): APLValue {
        return defaultReduceLongToLong(this, context, arg, offset, sizeAlongAxis, stepLength, pos, savedStack, functionAxis)
    }

    open fun reduceDoubleToDouble(
        context: RuntimeContext,
        arg: APLValue,
        offset: Int,
        sizeAlongAxis: Int,
        stepLength: Int,
        pos: Position,
        savedStack: StorageStack.StorageStackFrame?,
        functionAxis: APLValue?
    ): APLValue {
        return defaultReduceDoubleToDouble(this, context, arg, offset, sizeAlongAxis, stepLength, pos, savedStack, functionAxis)
    }

    open fun reduceGeneric(
        context: RuntimeContext,
        arg: APLValue,
        sizeAlongAxis: Int,
        stepLength: Int,
        offset: Int,
        savedStack: StorageStack.StorageStackFrame?,
        functionAxis: APLValue?
    ): APLValue {
        return defaultReduceGeneric(this, context, arg, offset, sizeAlongAxis, stepLength, pos, savedStack, functionAxis)
    }

    open fun callAsync1Arg(
        context: RuntimeContext,
        a: APLValue,
        axis: APLValue?,
        callback: (context: RuntimeContext, APLValue) -> Either<APLValue, Exception>
    ): Unit = throwAPLException(AsyncNotSupported(name1Arg, pos))

    open fun callAsync2Arg(
        context: RuntimeContext,
        a: APLValue,
        b: APLValue,
        axis: APLValue?,
        callback: (context: RuntimeContext, APLValue) -> Either<APLValue, Exception>
    ): Unit = throwAPLException(AsyncNotSupported(name2Arg, pos))
}

fun APLFunction.iterateFunctionTree(fn: (APLFunction) -> Unit) {
    fn(this)
    fns.forEach { childFn ->
        childFn.iterateFunctionTree(fn)
    }
}

fun APLFunction.ensureAxisNull(axis: APLValue?) {
    if (axis != null) {
        throwAPLException(AxisNotSupported(pos))
    }
}

abstract class NoAxisAPLFunction(pos: FunctionInstantiation, fns: List<APLFunction> = emptyList()) : APLFunction(pos, fns) {

    override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
        ensureAxisNull(axis)
        return eval1Arg(context, a)
    }

    open fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue =
        throwAPLException(Unimplemented1ArgException(pos))

    override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
        ensureAxisNull(axis)
        return eval2Arg(context, a, b)
    }

    open fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue =
        throwAPLException(Unimplemented2ArgException(pos))

    override fun evalInverse1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
        ensureAxisNull(axis)
        return evalInverse1Arg(context, a)
    }

    open fun evalInverse1Arg(context: RuntimeContext, a: APLValue): APLValue =
        throwAPLException(InverseNotAvailable(pos))

    override fun evalInverse2ArgB(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
        ensureAxisNull(axis)
        return evalInverse2ArgB(context, a, b)
    }

    open fun evalInverse2ArgA(context: RuntimeContext, a: APLValue, b: APLValue): APLValue =
        throwAPLException(InverseNotAvailable(pos))

    override fun evalInverse2ArgA(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
        ensureAxisNull(axis)
        return evalInverse2ArgA(context, a, b)
    }

    open fun evalInverse2ArgB(context: RuntimeContext, a: APLValue, b: APLValue): APLValue =
        throwAPLException(InverseNotAvailable(pos))

    open fun evalWithStructuralUnder1Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue): APLValue =
        throwAPLException(StructuralUnderNotSupported(pos))

    override fun evalWithStructuralUnder1Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
        ensureAxisNull(axis)
        return evalWithStructuralUnder1Arg(baseFn, context, a)
    }

    open fun evalWithStructuralUnder2Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, b: APLValue): APLValue =
        throwAPLException(StructuralUnderNotSupported(pos))

    override fun evalWithStructuralUnder2Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
        ensureAxisNull(axis)
        return evalWithStructuralUnder2Arg(baseFn, context, a, b)
    }
}

class APLArgumentCountMismatch(
    nargs: Int,
    minArgs: Int,
    maxArgs: Int?,
    pos: Position? = null
) : APLIllegalArgumentException(makeMessage(nargs, minArgs, maxArgs), pos) {
    companion object {
        fun makeMessage(nargs: Int, minArgs: Int, maxArgs: Int?): String {
            return buildString {
                when (maxArgs) {
                    null -> {
                        append("Expected at least ")
                        append(minArgs)
                        append(" arguments")
                    }
                    minArgs -> {
                        append("Expected ")
                        append(minArgs)
                        append(" arguments")
                    }
                    else -> {
                        append("Expected between ")
                        append(minArgs)
                        append("-")
                        append(maxArgs)
                        append(" arguments")
                    }
                }
                append(", got: ")
                append(nargs)
            }
        }
    }
}

abstract class MultiArgumentAPLFunction(
    val minArgs: Int,
    val maxArgs: Int?,
    pos: FunctionInstantiation,
    fns: List<APLFunction> = emptyList()
) : NoAxisAPLFunction(pos, fns) {
    override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
        val args = a.listify()
        val nargs = args.listSize()
        if (nargs < minArgs || (maxArgs != null && nargs > maxArgs)) {
            throwAPLException(APLArgumentCountMismatch(nargs, minArgs, maxArgs, pos))
        }
        return evalMultiArgument(context, args.elements)
    }

    abstract fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue
}

class SaveStackSupport(fn: APLFunction) {
    private var saveStack: Boolean = false

    fun savedStack() = if (saveStack) currentStorageStack().currentFrame() else null

    init {
        computeCapturedEnvs(fn.fns)
        fn.instantiationEnv.markCanEscape()
    }

    private fun computeCapturedEnvs(fns: List<APLFunction>) {
        val capturedEnvs = fns.flatMap(APLFunction::allCapturedEnvironments)
        if (capturedEnvs.isNotEmpty()) {
            saveStack = capturedEnvs.isNotEmpty()
            capturedEnvs.forEach(Environment::markCanEscape)
        }
    }
}

abstract class DelegatedAPLFunctionImpl(pos: FunctionInstantiation, fns: List<APLFunction> = emptyList()) : APLFunction(pos, fns) {
    override fun evalArgsAndCall1Arg(context: RuntimeContext, rightArgs: Instruction) =
        innerImpl().evalArgsAndCall1Arg(context, rightArgs)

    override fun evalArgsAndCall2Arg(context: RuntimeContext, leftArgs: Instruction, rightArgs: Instruction) =
        innerImpl().evalArgsAndCall2Arg(context, leftArgs, rightArgs)

    override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?) =
        innerImpl().eval1Arg(context, a, axis)

    override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?) =
        innerImpl().eval2Arg(context, a, b, axis)

    override fun identityValue() = innerImpl().identityValue()
    override fun deriveBitwise() = innerImpl().deriveBitwise()
    override val optimisationFlags: OptimisationFlags get() = innerImpl().optimisationFlags

    override fun eval1ArgLong(context: RuntimeContext, a: Long, axis: APLValue?) =
        innerImpl().eval1ArgLong(context, a, axis)

    override fun eval1ArgDouble(context: RuntimeContext, a: Double, axis: APLValue?) =
        innerImpl().eval1ArgDouble(context, a, axis)

    override fun eval2ArgLongToLongWithAxis(context: RuntimeContext, a: Long, b: Long, axis: APLValue?) =
        innerImpl().eval2ArgLongToLongWithAxis(context, a, b, axis)

    override fun eval2ArgDoubleToDoubleWithAxis(context: RuntimeContext, a: Double, b: Double, axis: APLValue?) =
        innerImpl().eval2ArgDoubleToDoubleWithAxis(context, a, b, axis)

    override fun evalInverse1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue =
        innerImpl().evalInverse1Arg(context, a, axis)

    override fun evalWithStructuralUnder1Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, axis: APLValue?) =
        innerImpl().evalWithStructuralUnder1Arg(baseFn, context, a, axis)

    override fun evalInverse2ArgB(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue =
        innerImpl().evalInverse2ArgB(context, a, b, axis)

    override fun evalInverse2ArgA(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue =
        innerImpl().evalInverse2ArgA(context, a, b, axis)

    override fun evalWithStructuralUnder2Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?) =
        innerImpl().evalWithStructuralUnder2Arg(baseFn, context, a, b, axis)

    override fun computeClosure(parser: APLParser) =
        innerImpl().computeClosure(parser)

    override fun capturedEnvironments() = innerImpl().capturedEnvironments()

    override fun reduceGeneric(
        context: RuntimeContext,
        arg: APLValue,
        sizeAlongAxis: Int,
        stepLength: Int,
        offset: Int,
        savedStack: StorageStack.StorageStackFrame?,
        functionAxis: APLValue?
    ): APLValue {
        return innerImpl().reduceGeneric(context, arg, sizeAlongAxis, stepLength, offset, savedStack, functionAxis)
    }

    @Suppress("LeakingThis")
    override val name1Arg = innerImpl().name1Arg

    @Suppress("LeakingThis")
    override val name2Arg = innerImpl().name2Arg

    abstract fun innerImpl(): APLFunction
}

/**
 * A function that is declared directly in a { ... } expression.
 */
class DeclaredFunction(
    val name: String,
    val instruction: Instruction,
    val leftArgName: EnvironmentBinding,
    val rightArgName: EnvironmentBinding,
    val env: Environment
) : APLFunctionDescriptor {
    inner class DeclaredFunctionImpl(pos: FunctionInstantiation) : APLFunction(pos) {
        private val leftArgRef = StackStorageRef(leftArgName)
        private val rightArgRef = StackStorageRef(rightArgName)

        private val contextNameMonadic = "declaredFunction1arg(${name})"

        /**
         * This function is needed for a test case that verifies that an optimisation has been performed correctly
         */
        internal val parentInstr get() = instruction

        override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            return withLinkedContext(env, contextNameMonadic, pos) {
                context.setVar(rightArgRef, a)
                instruction.evalWithContext(context)
            }
        }

        private val contextNameDyadic = "declaredFunction2arg(${name})"

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            return withLinkedContext(env, contextNameDyadic, pos) {
                context.setVar(leftArgRef, a)
                context.setVar(rightArgRef, b)
                instruction.evalWithContext(context)
            }
        }

        override fun capturedEnvironments() = listOf(env)

        override val name1Arg: String get() = name
        override val name2Arg: String get() = name
    }

    override fun make(instantiation: FunctionInstantiation) = DeclaredFunctionImpl(instantiation)
}

/**
 * A special declared function which ignores its arguments. Its primary use is inside defsyntax rules
 * where the functions are only used to provide code structure and not directly called by the user.
 */
class DeclaredNonBoundFunction(val instruction: Instruction) : APLFunctionDescriptor {
    inner class DeclaredNonBoundFunctionImpl(pos: FunctionInstantiation) : APLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            return instruction.evalWithContext(context)
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            return instruction.evalWithContext(context)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = DeclaredNonBoundFunctionImpl(instantiation)
}

class LeftAssignedFunction(
    underlying: APLFunction, val leftArgs: Instruction, pos: FunctionInstantiation, val leftArgBindEnv: Environment? = null
) : APLFunction(pos, listOf(underlying)) {

    private val underlying get() = fns[0]

    override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
        val leftArg = leftArgs.evalWithContext(context)
        return underlying.eval2Arg(context, leftArg, a, axis)
    }

    override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
        throwAPLException(LeftAssigned2ArgException(pos))
    }

    override fun evalInverse1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
        val leftArg = leftArgs.evalWithContext(context)
        return underlying.evalInverse2ArgB(context, leftArg, a, axis)
    }

    override fun evalInverse2ArgB(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
        throwAPLException(LeftAssigned2ArgException(pos))
    }

    override fun evalInverse2ArgA(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
        throwAPLException(LeftAssigned2ArgException(pos))
    }

    override fun evalWithStructuralUnder1Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
        val leftArg = leftArgs.evalWithContext(context)
        return underlying.evalWithStructuralUnder2Arg(baseFn, context, leftArg, a, axis)
    }

    override fun evalWithStructuralUnder2Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
        throwAPLException(LeftAssigned2ArgException(pos))
    }

    override fun capturedEnvironments(): List<Environment> {
        return if (leftArgBindEnv == null) emptyList() else listOf(leftArgBindEnv)
    }

    override fun computeClosure(parser: APLParser): Pair<APLFunction, List<Instruction>> {
        val sym = parser.tokeniser.engine.createAnonymousSymbol("leftAssignedFunction")
        val binding = parser.currentEnvironment().bindLocal(sym)
        val (innerFn, relatedInstrs) = underlying.computeClosure(parser)
        val ref = StackStorageRef(binding)
        val list = mutableListOf<Instruction>(AssignmentInstruction.make(ref, leftArgs, pos))
        list.addAll(relatedInstrs)
        val env = parser.currentEnvironment()
        return Pair(
            LeftAssignedFunction(innerFn, VariableRef(sym, ref, pos), FunctionInstantiation(parser.tokeniser.engine, pos, env), env),
            list)
    }

    override val name1Arg get() = underlying.name2Arg
}

class AxisValAssignedFunctionDirect(baseFn: APLFunction, val axis: Instruction, val axisBindEnv: Environment? = null) :
    NoAxisAPLFunction(baseFn.instantiation, listOf(baseFn)) {

    private val baseFn get() = fns[0]

    override fun evalArgsAndCall1Arg(context: RuntimeContext, rightArgs: Instruction): APLValue {
        val rightValue = rightArgs.evalWithContext(context)
        val axisValue = axis.evalWithContext(context)
        return baseFn.eval1Arg(context, rightValue, axisValue)
    }

    override fun evalArgsAndCall2Arg(context: RuntimeContext, leftArgs: Instruction, rightArgs: Instruction): APLValue {
        val rightValue = rightArgs.evalWithContext(context)
        val axisValue = axis.evalWithContext(context)
        val leftValue = leftArgs.evalWithContext(context)
        return baseFn.eval2Arg(context, leftValue, rightValue, axisValue)
    }

    override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
        return baseFn.eval1Arg(context, a, axis.evalWithContext(context))
    }

    override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
        return baseFn.eval2Arg(context, a, b, axis.evalWithContext(context))
    }

    override fun evalInverse1Arg(context: RuntimeContext, a: APLValue): APLValue {
        return baseFn.evalInverse1Arg(context, a, axis.evalWithContext(context))
    }

    override fun evalInverse2ArgA(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
        return baseFn.evalInverse2ArgA(context, a, b, axis.evalWithContext(context))
    }

    override fun evalInverse2ArgB(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
        return baseFn.evalInverse2ArgB(context, a, b, axis.evalWithContext(context))
    }

    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun evalWithStructuralUnder1Arg(processingFN: APLFunction, context: RuntimeContext, a: APLValue): APLValue {
        return baseFn.evalWithStructuralUnder1Arg(processingFN, context, a, axis.evalWithContext(context))
    }

    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun evalWithStructuralUnder2Arg(processingFn: APLFunction, context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
        return baseFn.evalWithStructuralUnder2Arg(processingFn, context, a, b, axis.evalWithContext(context))
    }

    override fun capturedEnvironments(): List<Environment> {
        return if (axisBindEnv == null) emptyList() else listOf(axisBindEnv)
    }

    override fun computeClosure(parser: APLParser): Pair<APLFunction, List<Instruction>> {
        val sym = parser.tokeniser.engine.createAnonymousSymbol("axisFn")
        val binding = parser.currentEnvironment().bindLocal(sym)
        val (innerFn, relatedInstrs) = baseFn.computeClosure(parser)
        val ref = StackStorageRef(binding)
        val list = ArrayList<Instruction>()
        list.addAll(relatedInstrs)
        list.add(AssignmentInstruction.make(ref, axis, pos))
        val env = parser.currentEnvironment()
        return Pair(
            AxisValAssignedFunctionDirect(innerFn, VariableRef(sym, ref, pos), env),
            list)
    }

    override val name1Arg get() = baseFn.name1Arg
    override val name2Arg get() = baseFn.name2Arg
}
