package com.dhsdevelopments.kap.gui.arrayedit

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.gui.Client
import com.dhsdevelopments.kap.gui.displayErrorWithStage
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuItem
import javafx.scene.control.SeparatorMenuItem
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import org.controlsfx.control.spreadsheet.GridBase
import org.controlsfx.control.spreadsheet.SpreadsheetCell
import org.controlsfx.control.spreadsheet.SpreadsheetView
import java.util.*

class ArrayEditSpreadsheetView : SpreadsheetView() {
    lateinit var client: Client
    lateinit var cellType: KapValueSpreadsheetCellType
    var content: MutableAPLValue
    var displayedPosition: IntArray
    var insertExpressionCallback: (() -> Unit)? = null
    var pasteTableCallback: (() -> Unit)? = null
    var pasteIntoExistingTableCallback: (() -> Unit)? = null
    var copySelectedCells: (() -> Unit)? = null

    init {
        content = MutableAPLValue(APLArrayImpl(dimensionsOfSize(2, 2), arrayOf(APLLONG_1, APLLONG_1, APLString.make("Foo"), APLString.make("test"))))
        displayedPosition = intArrayOf(0, 0)
//        updateGridBase()
    }

    fun updateClient(client: Client) {
        this.client = client
        this.cellType = KapValueSpreadsheetCellType(client)
    }

    fun replaceContent(newContent: MutableAPLValue, position: IntArray) {
        content = newContent
        displayedPosition = position
        updateGridBase()
    }

    private fun updateGridBase() {
        val d = content.dimensions
        require(d.size == displayedPosition.size)
        require(displayedPosition[displayedPosition.size - 1] == 0)
        if (d.size > 1) {
            require(displayedPosition[displayedPosition.size - 2] == 0)
        }
        val baseIndex = if (d.contentSize() == 0) 0 else d.indexFromPosition(displayedPosition)
        val (numRows, numCols) = if (d.size == 1) Pair(d[0], 1) else Pair(d[d.size - 2], d[d.size - 1])
        val gridBase = GridBase(numRows, numCols)
        val rows = FXCollections.observableArrayList<ObservableList<SpreadsheetCell>>()
        repeat(numRows) { rowIndex ->
            val row = FXCollections.observableArrayList<SpreadsheetCell>()
            repeat(numCols) { colIndex ->
                row.add(
                    cellType.createCell(
                        client,
                        content,
                        rowIndex,
                        colIndex,
                        baseIndex + (rowIndex * numCols) + colIndex))
            }
            rows.add(row)
        }
        gridBase.setRows(rows)

        grid = gridBase
        updateHeadersFromContent()
        // Need runLater here, otherwise the width calculation will fail and all columns becomes minimum size
        Platform.runLater {
            columns.forEach { col ->
                col.fitColumn()
            }
        }
    }

    private fun updateHeadersFromContent() {
        val d = content.dimensions
        val colHeaders = if (d.size == 1) {
            listOf("0")
        } else {
            val columnLabels = content.labels?.labels?.get(d.size - 1)
            (0 until grid.columnCount).map { i ->
                columnLabels?.get(i)?.title ?: i.toString()
            }
        }
        grid.columnHeaders.setAll(colHeaders)

        val rowLabels = content.labels?.labels?.get(if (d.size == 1) 0 else d.size - 2)
        val rowHeaders = (0 until grid.rowCount).map { i ->
            rowLabels?.get(i)?.title ?: i.toString()
        }
        grid.rowHeaders.setAll(rowHeaders)
    }

    private fun insertRowAbove() {
        val row = selectionModel.selectedCells.minOfOrNull { it.row }
        insertRow(row ?: 0)
    }

    private fun insertRowBelow() {
        val row = selectionModel.selectedCells.maxOfOrNull { it.row }
        insertRow(if (row == null) 0 else row + 1)
    }

    private fun insertRow(rowIndex: Int) {
        val d = content.dimensions
        if (d.size == 1) {
            content.insert(0, rowIndex, 1)
        } else {
            content.insert(d.size - 2, rowIndex, 1)
        }
        updateGridBase()
    }

    private fun insertColLeft() {
        val col = selectionModel.selectedCells.minOfOrNull { it.column }
        insertCol(col ?: 0)
    }

    private fun insertColRight() {
        val col = selectionModel.selectedCells.maxOfOrNull { it.column }
        insertCol(if (col == null) 0 else col + 1)
    }

    private fun insertCol(colIndex: Int) {
        val d = content.dimensions
        if (d.size > 1) {
            content.insert(d.size - 1, colIndex, 1)
            updateGridBase()
        }
    }

    private fun groupByAscending(list: List<Int>): List<Pair<Int, Int>> {
        if (list.isEmpty()) {
            return emptyList()
        } else {
            var start = list.first()
            var curr = start
            val res = ArrayList<Pair<Int, Int>>()
            list.rest().forEach { v ->
                if (v <= curr) {
                    throw IllegalArgumentException("List is not strictly increasing")
                }
                if (v != curr + 1) {
                    res.add(Pair(start, curr - start + 1))
                    start = v
                }
                curr = v
            }
            res.add(Pair(start, curr - start + 1))
            return res
        }
    }

    private fun deleteRows() {
        val rowIndexes = TreeSet<Int>()
        selectionModel.selectedCells.forEach { cell ->
            rowIndexes.add(cell.row)
        }
        groupByAscending(rowIndexes.toList()).asReversed().forEach { (index, n) ->
            content.remove(content.dimensions.lastAxis() - 1, index, n)
        }
        updateGridBase()
    }

    private fun deleteCols() {
        val colIndexes = TreeSet<Int>()
        selectionModel.selectedCells.forEach { cell ->
            colIndexes.add(cell.column)
        }
        groupByAscending(colIndexes.toList()).asReversed().forEach { (index, n) ->
            content.remove(content.dimensions.lastAxis(), index, n)
        }
        updateGridBase()
    }

    private fun rowToHeaders() {
        val d = content.dimensions
        if (d.size == 0) {
            displayErrorWithStage(null, "Cannot assign labels to scalars")
            return
        }
        val baseIndex = if (d.contentSize() == 0) 0 else d.indexFromPosition(displayedPosition)
        var selectedRow = -1
        val sel = selectionModel.selectedCells
        if (sel.isEmpty()) {
            displayErrorWithStage(null, "No cells selected")
        } else {
            sel.forEach { cell ->
                if (selectedRow == -1) {
                    selectedRow = cell.row
                } else if (selectedRow != cell.row) {
                    displayErrorWithStage(null, "Multiple rows selected", "When assigning a row to headers, only cells on a single row must be selected")
                    return
                }
            }
            val width = d.lastDimension()
            val startIndex = selectedRow * width
            val newRowLabels = (startIndex until startIndex + width).map { i ->
                content.valueAt(baseIndex + i).formatted(FormatStyle.PLAIN)
            }
            content.remove(d.lastAxis() - 1, selectedRow, 1)
            updateGridBase()
            content.updateLabels(d.lastAxis(), newRowLabels)
            updateHeadersFromContent()
        }
    }

    private fun colToHeaders() {
        val d = content.dimensions
        if (d.size == 0) {
            displayErrorWithStage(null, "Cannot assign labels to scalars")
            return
        }
        val baseIndex = if (d.contentSize() == 0) 0 else d.indexFromPosition(displayedPosition)
        var selectedCol = -1
        val sel = selectionModel.selectedCells
        if (sel.isEmpty()) {
            displayErrorWithStage(null, "No cells selected")
        } else {
            sel.forEach { cell ->
                if (selectedCol == -1) {
                    selectedCol = cell.column
                } else if (selectedCol != cell.column) {
                    displayErrorWithStage(
                        null,
                        "Multiple columns selected",
                        "When assigning a column to headers, only cells on a single column must be selected")
                    return
                }
            }
            val width = content.dimensions.lastDimension()
            val newRowLabels = (0 until d[d.lastAxis() - 1]).map { i ->
                content.valueAt(baseIndex + (i * width) + selectedCol).formatted(FormatStyle.PLAIN)
            }
            content.remove(content.dimensions.lastAxis(), selectedCol, 1)
            updateGridBase()
            content.updateLabels(d.lastAxis() - 1, newRowLabels)
            updateHeadersFromContent()
        }
    }

    override fun getSpreadsheetViewContextMenu(): ContextMenu {
        val menu = super.getSpreadsheetViewContextMenu()
        menu.items.add(2, MenuItem("Paste and replace entire table").apply {
            onAction = EventHandler {
                val fn = pasteTableCallback
                if (fn != null) {
                    fn()
                }
            }
        })
        menu.items.add(MenuItem("Insert expression").apply {
            onAction = EventHandler { insertExpressionCallback!!() }
            accelerator = KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN)
        })
        menu.items.add(SeparatorMenuItem())
        menu.items.add(MenuItem("Insert row above").apply {
            onAction = EventHandler { insertRowAbove() }
        })
        menu.items.add(MenuItem("Insert row below").apply {
            onAction = EventHandler { insertRowBelow() }
        })
        menu.items.add(MenuItem("Insert column left").apply {
            onAction = EventHandler { insertColLeft() }
        })
        menu.items.add(MenuItem("Insert column right").apply {
            onAction = EventHandler { insertColRight() }
        })
        menu.items.add(SeparatorMenuItem())
        menu.items.add(MenuItem("Delete rows").apply {
            onAction = EventHandler { deleteRows() }
        })
        menu.items.add(MenuItem("Delete columns").apply {
            onAction = EventHandler { deleteCols() }
        })
        menu.items.add(SeparatorMenuItem())
        menu.items.add(MenuItem("Assign row as column headers").apply {
            onAction = EventHandler { rowToHeaders() }
        })
        menu.items.add(MenuItem("Assign column as row headers").apply {
            onAction = EventHandler { colToHeaders() }
        })
        return menu
    }

    override fun pasteClipboard() {
        val fn = pasteIntoExistingTableCallback
        if (fn != null) {
            fn()
        }
    }


    override fun copyClipboard() {
        val fn = copySelectedCells
        if (fn != null) {
            fn()
        }
    }
}
