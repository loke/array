package com.dhsdevelopments.kap.optimiser

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.*
import com.dhsdevelopments.mpbignum.*
import kotlin.math.ceil
import kotlin.math.floor

object DivideToFloorInstructionOptimiser : Scalar2ArgInstructionChainOptimiser() {
    override fun findMergedFunctions(fn0: APLFunction, fn1: APLFunction): APLFunction? {
        if (fn0 !is MinAPLFunction.MinAPLFunctionImpl || fn1 !is DivAPLFunction.DivAPLFunctionImpl) {
            return null
        }
        return MergedFloorDivFunction(fn0, fn1, fn0.instantiation)
    }
}

object DivideToCeilInstructionOptimiser : Scalar2ArgInstructionChainOptimiser() {
    override fun findMergedFunctions(fn0: APLFunction, fn1: APLFunction): APLFunction? {
        if (fn0 !is MaxAPLFunction.MaxAPLFunctionImpl || fn1 !is DivAPLFunction.DivAPLFunctionImpl) {
            return null
        }
        return MergedCeilDivFunction(fn0, fn1, fn0.instantiation)
    }
}

abstract class AbstractMergedFloorCeilDivFunction(
    fn0: APLFunction,
    fn1: APLFunction,
    pos: FunctionInstantiation
) : MathNumericCombineAPLFunction(pos, listOf(fn0, fn1), resultType2Arg = ArrayMemberType.LONG) {

    override fun combine2Arg(a: APLSingleValue, b: APLSingleValue): APLValue {
        return opLong(a, b, { x -> x.makeAPLNumber() }, { x -> x.makeAPLNumber() })
    }

    override fun combine2ArgGenericToLong(a: APLSingleValue, b: APLSingleValue): Long {
        return opLong(a, b, { x -> x }, { x -> throw LongExpressionOverflow(x) })
    }

    private inline fun <T> convertOrOverflow(a: BigInt, convFn: (Long) -> T, overflowFn: (BigInt) -> T): T {
        return if (a.rangeInLong()) {
            convFn(a.toLong())
        } else {
            overflowFn(a)
        }
    }

    private inline fun <T> opLong(a: APLSingleValue, b: APLSingleValue, convFn: (Long) -> T, overflowFn: (BigInt) -> T): T {
        numericRelationOperation2(
            pos,
            a,
            b,
            { x, y -> return if (x == Long.MIN_VALUE && y == -1L) overflowFn(BIGINT_MAX_LONG_ADD_1) else convFn(divFloorOrCeilLong(x, y)) },
            { x, y ->
                return if (y == 0.0) convFn(0) else {
                    (x / y).let { result ->
                        if (result <= MIN_INT_DOUBLE || result >= MAX_INT_DOUBLE) {
                            overflowFn(fromDoubleFloorOrCeil(result))
                        } else {
                            convFn(floorOrCeilDouble(result).toLong())
                        }
                    }
                }
            },
            { _, _ -> throwAPLException(IncompatibleTypeException("Floor is not valid for complex values", pos)) },
            fnBigint = { x, y -> return convertOrOverflow(divFloorOrCeilBigInt(x, y), convFn, overflowFn) },
            fnRational = { x, y -> return convertOrOverflow(floorOrCeilRational(x / y), convFn, overflowFn) })
    }

    abstract fun fromDoubleFloorOrCeil(x: Double): BigInt
    abstract fun floorOrCeilDouble(x: Double): Double
    abstract fun floorOrCeilRational(x: Rational): BigInt
    abstract fun divFloorOrCeilLong(x: Long, y: Long): Long
    abstract fun divFloorOrCeilBigInt(x: BigInt, y: BigInt): BigInt

    override fun combine2ArgLongToLong(a: Long, b: Long) = divFloorOrCeilLong(a, b)

    override val optimisationFlags get() = OptimisationFlags(OptimisationFlags.OPTIMISATION_FLAG_2ARG_LONG_LONG)

    companion object {
        val BIGINT_MAX_LONG_ADD_1 = BigInt.of("9223372036854775808")
    }
}

class MergedFloorDivFunction(
    fn0: APLFunction, fn1: APLFunction, instantiation: FunctionInstantiation
) : AbstractMergedFloorCeilDivFunction(
    fn0, fn1, instantiation
) {
    override fun fromDoubleFloorOrCeil(x: Double) = BigInt.fromDoubleFloor(x)
    override fun floorOrCeilDouble(x: Double) = floor(x)
    override fun floorOrCeilRational(x: Rational): BigInt = x.floor()
    override fun divFloorOrCeilLong(x: Long, y: Long) = divFloor(x, y)
    override fun divFloorOrCeilBigInt(x: BigInt, y: BigInt) = divFloor(x, y)
}

class MergedCeilDivFunction(
    fn0: APLFunction, fn1: APLFunction, instantiation: FunctionInstantiation
) : AbstractMergedFloorCeilDivFunction(
    fn0, fn1, instantiation
) {
    override fun fromDoubleFloorOrCeil(x: Double) = BigInt.fromDoubleCeil(x)
    override fun floorOrCeilDouble(x: Double) = ceil(x)
    override fun floorOrCeilRational(x: Rational): BigInt = x.ceil()
    override fun divFloorOrCeilLong(x: Long, y: Long) = divCeil(x, y)
    override fun divFloorOrCeilBigInt(x: BigInt, y: BigInt) = divCeil(x, y)
}

private fun divFloor(a: Long, b: Long): Long {
    return a / b - if ((a % b) != 0L && (a xor b) < 0) 1 else 0
}

private fun divFloor(a: BigInt, b: BigInt): BigInt {
    return a / b - if ((a % b).signum() != 0 && (a xor b).signum() < 0) BigIntConstants.ONE else BigIntConstants.ZERO
}

private fun divCeil(a: Long, b: Long): Long {
    return a / b + if ((a % b) != 0L && (a xor b) > 0) 1 else 0
}

private fun divCeil(a: BigInt, b: BigInt): BigInt {
    return a / b + if ((a % b).signum() != 0 && (a xor b).signum() > 0) BigIntConstants.ONE else BigIntConstants.ZERO
}
