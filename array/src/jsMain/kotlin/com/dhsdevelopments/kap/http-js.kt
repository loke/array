package com.dhsdevelopments.kap

actual fun httpRequest(engine: Engine, httpRequestData: HttpRequestData, pos: Position?): HttpResult {
    val manager = engine.jsNativeData().httpManager ?: throwAPLException(APLEvalException("Backend does not support HTTP calls", pos))
    return manager.httpRequest(engine, httpRequestData, pos)
}

actual fun httpRequestCallback(
    engine: Engine,
    httpRequestData: HttpRequestData,
    pos: Position?,
    callback: (HttpResult) -> Either<APLValue, Exception>
) {
    val manager = engine.jsNativeData().httpManager ?: throwAPLException(APLEvalException("Backend does not support HTTP calls", pos))
    return manager.httpRequestCallback(engine, httpRequestData, pos, callback)
}

interface HttpManager {
    fun httpRequest(engine: Engine, httpRequestData: HttpRequestData, pos: Position?): HttpResult
    fun httpRequestCallback(engine: Engine, httpRequestData: HttpRequestData, pos: Position?, callback: (HttpResult) -> Either<APLValue, Exception>)
}
