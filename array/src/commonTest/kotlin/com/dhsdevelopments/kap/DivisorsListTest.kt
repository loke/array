package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertFailsWith

class DivisorsListTest : APLTest() {
    @Test
    fun simpleDivisors() {
        parseAPLExpression("math:divisors 8").let { result ->
            assert1DArray(arrayOf(2, 4), result)
        }
    }

    @Test
    fun smallDivisors() {
        parseAPLExpression("math:divisors 0 1").let { result ->
            assert1DArray(arrayOf(Inner1D(arrayOf()), Inner1D(arrayOf())), result)
        }
    }

    @Test
    fun smallDivisorsBigint() {
        parseAPLExpression("math:divisors (int:asBigint 0) (int:asBigint 1)").let { result ->
            assert1DArray(arrayOf(Inner1D(arrayOf()), Inner1D(arrayOf())), result)
        }
    }

    @Test
    fun divisorsOfPrime() {
        parseAPLExpression("math:divisors 179").let { result ->
            assert1DArray(arrayOf(), result)
        }
    }

    @Test
    fun divisorsOfArray() {
        parseAPLExpression("math:divisors 4 5 6 2").let { result ->
            assert1DArray(
                arrayOf(
                    Inner1D(arrayOf(2)),
                    Inner1D(arrayOf()),
                    Inner1D(arrayOf(2, 3)),
                    Inner1D(arrayOf())),
                result)
        }
    }

    @Test
    fun largeNumberOfDivisors() {
        parseAPLExpression("math:divisors ⌊!5").let { result ->
            assert1DArray(
                arrayOf(2, 3, 4, 5, 6, 8, 10, 12, 15, 20, 24, 30, 40, 60), result)
        }
    }

    @Test
    fun largeNumberOfDivisorsBigint() {
        parseAPLExpression("math:divisors (int:asBigint ⌊!5)").let { result ->
            assert1DArray(arrayOf(2, 3, 4, 5, 6, 8, 10, 12, 15, 20, 24, 30, 40, 60), result)
        }
    }

    @Test
    fun divisorsOfDoubleShouldFail() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpression("math:divisors 1.1")
        }
    }

    @Test
    fun divisorsOfComplexShouldFail() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpression("math:divisors 1j4")
        }
    }

    @Test
    fun divisorsOfRationalShouldFail() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpression("math:divisors 1r2")
        }
    }

    @Test
    fun divisorsOfIntegerRational() {
        parseAPLExpression("math:divisors int:asBigint 12").let { result ->
            assert1DArray(arrayOf(2, 3, 4, 6), result)
        }
    }
}
