package com.dhsdevelopments.kap.net

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.APLBinaryInputStream
import com.dhsdevelopments.kap.builtins.APLBinaryOutputStream
import com.dhsdevelopments.kap.builtins.KotlinObjectWrappedValue

class NetModule : KapModule {
    override val name get() = "net"

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("net")
        engine.registerFunction(ns.internAndExport("connect"), TcpConnectFunction())
        engine.registerFunction(ns.internAndExport("input"), TcpStreamBinaryInputFunction())
        engine.registerFunction(ns.internAndExport("output"), TcpStreamBinaryOutputFunction())
        engine.registerClosableHandler { value: TcpConnectionValue -> value.value.close() }
    }
}

class TcpConnectionValue(value: TcpConnection) : KotlinObjectWrappedValue<TcpConnection>(value) {
    override fun formatted(style: FormatStyle) = "[connection:host=${value.sockaddr.host},port=${value.sockaddr.port}]"

    companion object {
        fun ensureType(a: APLValue, pos: Position? = null): TcpConnection {
            val a0 = a.unwrapDeferredValue()
            if (a0 !is TcpConnectionValue) {
                throw IncompatibleTypeException("Expected TCP connection, got: ${a0.kapClass.name}", pos)
            }
            return a0.value
        }
    }
}

class TcpConnectFunction : APLFunctionDescriptor {
    class TcpConnectFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val args = a.listify()
            if (args.listSize() != 2) {
                throwAPLException(IllegalArgumentNumException(2, args.listSize(), pos))
            }
            val host = args.listElement(0).toStringValue(pos)
            val port = args.listElement(1).ensureNumber(pos).asInt(pos)
            if (port < 0 || port > 0xffff) {
                throwAPLException(APLIllegalArgumentException("Invalid port number: ${port}", pos))
            }
            val nwApi = networkApi() ?: throwAPLException(APLEvalException("Network api not available", pos))
            val conn = try {
                nwApi.connect(host, port)
            } catch (e: MPFileException) {
                throwAPLException(APLEvalException(e.message ?: "Connect error", pos))
            }
            return TcpConnectionValue(conn)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = TcpConnectFunctionImpl(instantiation)
}

class TcpStreamBinaryInputFunction : APLFunctionDescriptor {
    class TcpStreamBinaryInputFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val conn = TcpConnectionValue.ensureType(a)
            return APLBinaryInputStream(conn.byteProvider)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = TcpStreamBinaryInputFunctionImpl(instantiation)
}

class TcpStreamBinaryOutputFunction : APLFunctionDescriptor {
    class TcpStreamBinaryInputFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val conn = TcpConnectionValue.ensureType(a)
            return APLBinaryOutputStream(conn.byteConsumer)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = TcpStreamBinaryInputFunctionImpl(instantiation)
}
