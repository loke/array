package com.dhsdevelopments.kap.ffi

import com.dhsdevelopments.kap.use
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@Ignore
class LinuxNativeTest {
    @Test
    fun putsSimple() {
        ffiManager().use { mgr ->
            val lib = mgr.loadLibrary("libc.so.6")
            val fn = lib.findFunction("puts", mgr.makeInputArgInt(), listOf(mgr.makeInputArgString()))
            assertNotNull(fn)
            val res = fn.call(listOf("test message here"))
            assertEquals(18, res.result)
        }
    }

    @Test
    fun structTest() {
        ffiManager().use { mgr ->
            val lib = mgr.loadLibrary("libc.so.6")
            val structTm = mgr.defineStruct(
                mgr.makeInputArgInt(), // tm_sec
                mgr.makeInputArgInt(), // tm_min
                mgr.makeInputArgInt(), // tm_hour
                mgr.makeInputArgInt(), // tm_mday
                mgr.makeInputArgInt(), // tm_mon
                mgr.makeInputArgInt(), // tm_year
                mgr.makeInputArgInt(), // tm_wday
                mgr.makeInputArgInt(), // tm_yday
                mgr.makeInputArgInt(), // tm_isdst
                mgr.makeInputArgInt(), // tm_gmtoff
                mgr.makeInputArgString()) // tm_zone

            val structTimeval = mgr.defineStruct(
                mgr.makeInputArgLong(), // tv_sec
                mgr.makeInputArgLong()) // tv_usec

            val gettimeofdayFn = lib.findFunction("gettimeofday", mgr.makeInputArgInt(), listOf(mgr.makeInputArgPtr(), mgr.makeInputArgPtr()))
            assertNotNull(gettimeofdayFn)
            val buf = mgr.allocBytes(structTimeval.size)
            val res = gettimeofdayFn.call(listOf(buf.pointerToOffset(0), mgr.makeNullPtr()))
            assertEquals(0, res.result)
            val sec = structTimeval.derivePtr(buf.pointerToOffset(0), 0).readLongAtOffset(0)
            val usec = structTimeval.derivePtr(buf.pointerToOffset(0), 1).readLongAtOffset(0)
            println("sec = ${sec}, usec = ${usec}")
        }
    }
}
