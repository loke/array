;;; -*- lexical-binding: t -*-

(require 'cl-lib)
(require 'quail)

(defvar kap--symbols '(("`" "⋄") ("1" "¨") ("!" "⌶") ("2" "¯") ("@" "⍫") ("3" "≤") ("#" "⍒")
                       ("4" "≥") ("$" "⍋") ("5" "⟦") ("%" "⌽") ("6" "⟧") ("^" "⍉") ("&" "⊖")
                       ("8" "≠") ("*" "⍟") ("9" "∨") ("(" "⍱") ("0" "∧") (")" "⍲") ("-" "×")
                       ("_" "⍠") ("=" "÷") ("+" "⌹") ("q" "⦻") ("Q" "⫇") ("w" "⍵") ("W" "⍹")
                       ("e" "∊") ("E" "⍷") ("r" "⍴") ("R" "√") ("t" "⍓") ("T" "⍨") ("y" "↑")
                       ("Y" "≬") ("u" "↓") ("U" "⇐") ("i" "⍳") ("I" "⍸") ("o" "○") ("O" "⍥")
                       ("p" "⋆") ("P" "⍣") ("[" "←") ("{" "⍞") ("]" "→") ("}" "⍬") ("\\" "⊢")
                       ("|" "⊣") ("a" "⍺") ("A" "⍶") ("s" "⌈") ("S" "∵") ("d" "⌊") ("D" "˝")
                       ("f" "_") ("F" "⍛") ("g" "∇") ("G" "⍢") ("h" "∆") ("H" "⍙") ("j" "∘")
                       ("J" "⍤") ("k" "⌸") ("K" "⌻") ("l" "⎕") ("L" "⌷") (";" "⍎") (":" "≡")
                       ("'" "⍕") ("\"" "≢") ("z" "⊂") ("Z" "⊆") ("x" "⊃") ("X" "⊇") ("c" "∩")
                       ("C" "∙") ("v" "∪") ("V" "λ") ("b" "⊥") ("B" "«") ("n" "⊤") ("N" "»")
                       ("m" "|") ("M" "∥") ("," "⍝") ("<" "⍪") ("." "⍀") (">" "⍮") ("/" "⌿")
                       ("?" "⫽")))

(quail-define-package "Kap" "UTF-8" "Kap" t
                      "Input mode for Kap"
                      '(("\t" . quail-completion))
                      t                 ; forget-last-selection
                      nil               ; deterministic
                      nil               ; kbd-translate
                      t                 ; show-layout
                      nil               ; create-decode-map
                      nil               ; maximum-shortest
                      nil               ; overlay-plist
                      nil               ; update-translation-function
                      nil               ; conversion-keys
                      t                 ; simple
                      )

(defvar kap--transcription-alist)
(defun kap--update-key-prefix (symbol new)
  (quail-select-package "Kap")
  (quail-install-map
   (let* ((prefix (string new))
          (kap--transcription-alist
           (cl-loop for (key-command character) in kap--symbols
                    collect (cons (concat prefix key-command) character))))
     (quail-map-from-table
      '((default kap--transcription-alist)))))
  (set-default symbol new))

(defun kap--initialise-key-prefix (symbol new)
  (custom-initialize-default symbol new)
  (kap--update-key-prefix symbol (eval new)))

(defcustom kap-key-prefix ?`
  "The character used as an input prefix for Kap characters"
  :type 'character
  :group 'kap
  :initialize #'kap--initialise-key-prefix
  :set #'kap--update-key-prefix)

(provide 'kap-symbols)
