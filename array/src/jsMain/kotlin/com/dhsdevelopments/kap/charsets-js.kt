package com.dhsdevelopments.kap

import kotlin.js.RegExp

val XRegExp = js("require('xregexp')")
val letterRegexp = XRegExp("^\\p{L}$")
val digitRegexp = XRegExp("^\\p{N}$")
val whitespaceRegexp = RegExp("^[ \\t]$") // We really want "^\\p{Zs}$" here, but xregexp appears to have a bug so it doesn't match tabs

actual fun isLetter(codepoint: Int): Boolean {
    return letterRegexp.test(charToString(codepoint)) as Boolean
}

actual fun isDigit(codepoint: Int): Boolean {
    return digitRegexp.test(codepoint.toChar().toString()) as Boolean
}

actual fun isWhitespace(codepoint: Int): Boolean {
    return whitespaceRegexp.test(codepoint.toChar().toString()) as Boolean
}

actual fun charToString(codepoint: Int): String {
    return if (codepoint < 0x10000) {
        codepoint.toChar().toString()
    } else {
        val off = 0xD800 - (0x10000 shr 10)
        val high = off + (codepoint shr 10)
        val low = 0xDC00 + (codepoint and 0x3FF)
        "${high.toChar()}${low.toChar()}"
    }
}

actual fun nameToCodepoint(name: String): Int? {
    return null
}

actual fun codepointToName(codepoint: Int): String? {
    return null
}

actual val backendSupportsUnicodeNames = false

actual fun StringBuilder.addCodepoint(codepoint: Int): StringBuilder {
    this.append(charToString(codepoint))
    return this
}

actual fun String.asCodepointList(): List<Int> {
    val result = ArrayList<Int>()
    var i = 0
    while (i < this.length) {
        val ch = this[i++]
        val v = when {
            ch.isHighSurrogate() -> {
                val low = this[i++]
                if (low.isLowSurrogate()) {
                    makeCharFromSurrogatePair(ch, low)
                } else {
                    throw IllegalStateException("Expected low surrogate, got: ${low.code}")
                }
            }
            ch.isLowSurrogate() -> throw IllegalStateException("Standalone low surrogate found: ${ch.code}")
            else -> ch.code
        }
        result.add(v)
    }
    return result
}

val GraphemeSplitter = js("require('grapheme-splitter')")
val graphemeSplitter = GraphemeSplitter()

actual fun String.asGraphemeList(): List<String> {
    val result = graphemeSplitter.splitGraphemes(this) as Array<String>
    return result.asList()
}
