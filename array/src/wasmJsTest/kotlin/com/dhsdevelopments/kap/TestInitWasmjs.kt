package com.dhsdevelopments.kap

@JsModule("fs")
external object jsFilesystem {
    fun readdirSync(dir: String): JsAny
    fun statSync(name: String): FsStats
    fun readFileSync(name: String): JsNativeBuffer
}

@JsName("Buffer")
external object JsNativeBuffer {
    operator fun get(offset: Int): JsNumber
    val length: Int
}

external object FsStats {
    fun isDirectory(): Boolean
    fun isFile(): Boolean
}

private fun jsAnyToStringList(value: JsAny): List<String> {
    val array = value.unsafeCast<JsArray<JsString>>()
    val res = ArrayList<String>()
    repeat(array.length) { i ->
        res.add(array[i].toString())
    }
    return res
}

actual fun nativeTestInit() {
    fun readFileRecurse(fsDir: String, dir: RegisteredEntry.Directory) {
        val files = jsAnyToStringList(jsFilesystem.readdirSync(fsDir))
        files.forEach { name ->
            val newName = "${fsDir}/${name}"
            val result = jsFilesystem.statSync(newName)
            when {
                result.isDirectory() -> readFileRecurse(newName, dir.createDirectory(name, false))
                result.isFile() -> {
                    val content = jsFilesystem.readFileSync(newName)
                    val buf = ByteArray(content.length) { i ->
                        val value = content[i].toInt()
                        require(value >= 0 && value < 256)
                        value.toByte()
                    }
                    dir.registerFile(name, buf)
                }
            }
        }
    }

    fun initDirectory(fsDir: String, base: String) {
        readFileRecurse("${fsDir}/${base}", registeredFilesRoot.createDirectory(base, errorIfExists = false))
    }

    initDirectory("../../../../array", "standard-lib")
    initDirectory("../../../../array", "test-data")
}

actual fun tryGc() {
}
