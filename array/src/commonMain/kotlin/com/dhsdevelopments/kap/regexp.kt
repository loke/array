package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.builtins.KotlinObjectWrappedValue

class InvalidRegexp(message: String, pos: Position? = null) : APLEvalException(message, pos)

private fun regexpFromValue(a: APLValue, pos: Position): Regex {
    return if (a is RegexpMatcherValue) {
        a.value
    } else {
        val regexpString = a.toStringValue(pos)
        return createRegexOrThrowKapError(regexpString, emptySet(), pos)
    }
}

private fun createRegexOrThrowKapError(regexpString: String, flags: Set<RegexOption>, pos: Position): Regex {
    return try {
        toRegexpWithException(regexpString, flags)
    } catch (_: RegexpParseException) {
        throwAPLException(InvalidRegexp("Invalid format: ${regexpString}", pos))
    }
}

class RegexpMatchesFunction : APLFunctionDescriptor {
    class RegexpMatchesFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val matchString = b.toStringValue(pos)
            val regexp = regexpFromValue(a, pos)
            return if (regexp.find(matchString) != null) APLLONG_1 else APLLONG_0
        }

        override val name2Arg get() = "match regexp"
    }

    override fun make(instantiation: FunctionInstantiation) = RegexpMatchesFunctionImpl(instantiation)
}

class RegexpFindFunction : APLFunctionDescriptor {
    class RegexpFindFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val matchString = b.toStringValue(pos)
            val regexp = regexpFromValue(a, pos)
            val result = regexp.find(matchString) ?: return APLNullValue
            return makeAPLValueFromGroups(result, context)
        }

        override val name2Arg get() = "find regexp"
    }

    override fun make(instantiation: FunctionInstantiation) = RegexpFindFunctionImpl(instantiation)
}

class RegexpFindAllFunction : APLFunctionDescriptor {
    class RegexFindAllFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val matchString = b.toStringValue(pos)
            val regexp = regexpFromValue(a, pos)
            val result = regexp.findAll(matchString)
            val l = result.map { matchResult ->
                makeAPLValueFromGroups(matchResult, context)
            }.toList()
            return APLArrayList(dimensionsOfSize(l.size), l)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = RegexFindAllFunctionImpl(instantiation)
}


private fun makeAPLValueFromGroups(result: MatchResult, context: RuntimeContext): APLArrayImpl {
    val groups = result.groups
    var undefinedSym: Symbol? = null
    return APLArrayImpl(dimensionsOfSize(groups.size), Array(groups.size) { i ->
        val v = groups.get(i)
        require(!(i == 0 && v == null))
        if (v == null) {
            if (undefinedSym == null) {
                undefinedSym = context.engine.keywordNamespace.internSymbol("undefined")
            }
            APLSymbol(undefinedSym)
        } else {
            APLString.make(v.value)
        }
    })
}

class RegexpSplitFunction : APLFunctionDescriptor {
    class RegexpSplitFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val matchString = b.toStringValue(pos)
            val regexp = regexpFromValue(a, pos)
            val result = regexp.split(matchString)
            return APLArrayList(dimensionsOfSize(result.size), result.map(APLString::make))
        }

        override val name2Arg get() = "split regexp"
    }

    override fun make(instantiation: FunctionInstantiation) = RegexpSplitFunctionImpl(instantiation)
}

class RegexpMatcherValue(matcher: Regex) : KotlinObjectWrappedValue<Regex>(matcher) {
    override val kapClass get() = SystemClass.INTERNAL
    override fun formatted(style: FormatStyle) = "regex-matcher"
    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) =
        reference is RegexpMatcherValue && value == reference.value
}

class CreateRegexpFunction : APLFunctionDescriptor {
    class CreateRegexpFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            return RegexpMatcherValue(createRegexOrThrowKapError(a.toStringValue(pos), emptySet(), pos))
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            fun mkFlag(v: APLValue) = valueToFlag(v)
            val flags = when (a.dimensions.size) {
                0 -> setOf(mkFlag(a))
                1 -> a.membersSequence().map(::mkFlag).toSet()
                else -> throwAPLException(APLEvalException("Regexp flags must be a single symbol or a one-dimensional array", pos))
            }
            return RegexpMatcherValue(createRegexOrThrowKapError(b.toStringValue(pos), flags, pos))
        }

        private fun valueToFlag(v: APLValue): RegexOption {
            val s = v.unwrapDeferredValue()
            if (s !is APLSymbol) {
                throwAPLException(APLEvalException("Regexp flag must be a symbol", pos))
            }
            val sym = s.value
            fun throwUnknownRegexpFlag(): Nothing {
                throwAPLException(APLEvalException("Unknown regexp flag: ${sym.symbolName}", pos))
            }
            if (sym.namespace == NamespaceList.KEYWORD_NAMESPACE_NAME) {
                return when (sym.symbolName) {
                    "ignoreCase" -> RegexOption.IGNORE_CASE
                    "multiLine" -> RegexOption.MULTILINE
                    else -> throwUnknownRegexpFlag()
                }
            } else {
                throwUnknownRegexpFlag()
            }
        }

        override val name1Arg get() = "create regexp"
    }

    override fun make(instantiation: FunctionInstantiation) = CreateRegexpFunctionImpl(instantiation)
}

class RegexpReplaceFunction : APLFunctionDescriptor {
    class RegexpReplaceFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val regexp = regexpFromValue(a, pos)
            val args = b.listify()
            if (args.listSize() != 2) {
                throwAPLException(IllegalArgumentNumException(2, args.listSize(), pos))
            }
            val matchString = args.listElement(0).toStringValue(pos)
            val replacementArg = args.listElement(1).collapse()
            val s = when {
                replacementArg.isStringValue() -> {
                    regexp.replace(matchString, replacementArg.toStringValue(pos))
                }
                replacementArg is LambdaValue -> {
                    val replacementClosure = replacementArg.makeClosure()
                    regexp.replace(matchString) { r ->
                        replacementClosure.eval1Arg(context, makeAPLValueFromGroups(r, context), null).toStringValue(pos)
                    }
                }
                else -> {
                    throwAPLException(APLIllegalArgumentException("Replacement must be either a string or a lambda", pos))
                }
            }
            return APLString.make(s)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = RegexpReplaceFunctionImpl(instantiation)
}

class RegexpModule : KapModule {
    override val name get() = "regex"

    override fun init(engine: Engine) {
        val namespace = engine.makeNamespace("regex")
        fun registerFn(name: String, fn: APLFunctionDescriptor) {
            engine.registerFunction(namespace.internAndExport(name), fn)
        }
        registerFn("match", RegexpMatchesFunction())
        registerFn("find", RegexpFindFunction())
        registerFn("findall", RegexpFindAllFunction())
        registerFn("compile", CreateRegexpFunction())
        registerFn("split", RegexpSplitFunction())
        registerFn("replace", RegexpReplaceFunction())
    }
}
