package com.dhsdevelopments.kap.optimiser

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.GradeDownFunction
import com.dhsdevelopments.kap.builtins.GradeUpFunction
import com.dhsdevelopments.kap.builtins.TakeAPLFunction

object IndexOfMaxInstructionOptimiser : NonScalar1ArgInstructionChainOptimiser() {
    override fun findMergedFunctions(fn0: APLFunction, fn1: APLFunction): APLFunction? {
        if (fn0 !is TakeAPLFunction.TakeAPLFunctionImpl || fn1 !is GradeDownFunction.GradeDownFunctionImpl) {
            return null
        }
        return MergedIndexOfMaxFunction(fn0, fn1, fn0.instantiation)
    }
}

object IndexOfMinInstructionOptimiser : NonScalar1ArgInstructionChainOptimiser() {
    override fun findMergedFunctions(fn0: APLFunction, fn1: APLFunction): APLFunction? {
        if (fn0 !is TakeAPLFunction.TakeAPLFunctionImpl || fn1 !is GradeUpFunction.GradeUpFunctionImpl) {
            return null
        }
        return MergedIndexOfMinFunction(fn0, fn1, fn0.instantiation)
    }
}

abstract class AbstractMergedIndexOfMaxMinFunction(
    val fn0: APLFunction,
    val fn1: APLFunction,
    instantiation: FunctionInstantiation
) : APLFunction(instantiation, listOf(fn0, fn1)) {
    override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
        if (axis != null) return callDefault(context, a, axis)
        val d = a.dimensions
        if (d.size != 1) return callDefault(context, a, axis)
        if (d[0] == 0) return a.metadata.defaultValue

        var currMax = a.valueAt(0)
        var currIndex = 0
        for (i in 1 until d[0]) {
            val v = a.valueAt(i)
            val res = applyReverse(currMax.compareTotalOrdering(v, pos))
            if (res < 0) {
                currMax = v
                currIndex = i
            }
        }
        return currIndex.makeAPLNumber()
    }

    protected fun callDefault(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
        val res = fn1.eval1Arg(context, a, axis)
        return fn0.eval1Arg(context, res, null)
    }

    abstract fun applyReverse(value: Int): Int
}

class MergedIndexOfMaxFunction(
    fn0: APLFunction,
    fn1: APLFunction,
    instantiation: FunctionInstantiation
) : AbstractMergedIndexOfMaxMinFunction(fn0, fn1, instantiation) {
    override fun applyReverse(value: Int) = value
}

class MergedIndexOfMinFunction(
    fn0: APLFunction,
    fn1: APLFunction,
    instantiation: FunctionInstantiation
) : AbstractMergedIndexOfMaxMinFunction(fn0, fn1, instantiation) {
    override fun applyReverse(value: Int) = -value
}
