@file:Suppress("EXPECT_ACTUAL_CLASSIFIERS_ARE_IN_BETA_WARNING")

package com.dhsdevelopments.kap.mpthread

import com.dhsdevelopments.kap.Either
import kotlin.reflect.KClass

class ThreadsNotSupportedException() : Exception("Backend does not support threads")

interface MPThread {
    fun join()
    fun isStopped(): Boolean
    fun completionData(): Either<Any?, Throwable>
}

expect val backendSupportsThreading: Boolean
expect fun startThread(fn: (MPThread) -> Any?): MPThread

interface MPAtomicRefArray<T> {
    operator fun get(index: Int): T?

    fun compareAndExchange(index: Int, expected: T?, newValue: T?): T?
}

inline fun <T> MPAtomicRefArray<T>.checkOrUpdate(index: Int, fn: () -> T): T {
    val old = get(index)
    if (old != null) {
        return old
    }
    val update = fn()
    return compareAndExchange(index, null, update) ?: update
}

expect fun <T> makeAtomicRefArray(size: Int): MPAtomicRefArray<T>

interface MPThreadLocal<T> {
    var value: T?
}

expect fun <T : Any> makeMPThreadLocalBackend(type: KClass<T>): MPThreadLocal<T>

inline fun <reified T : Any> makeMPThreadLocal(): MPThreadLocal<T> {
    return makeMPThreadLocalBackend(T::class)
}

expect class MPLock(recursive: Boolean = true) {
    fun makeCondVar(): MPCondVar
}

interface MPCondVar {
    fun waitUpdate()
    fun signal()
    fun signalAll()
}

expect inline fun <T> MPLock.withLocked(fn: () -> T): T
