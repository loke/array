package com.dhsdevelopments.kap

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

@OptIn(ExperimentalContracts::class)
inline fun unless(cond: Boolean, fn: () -> Unit) {
    contract { callsInPlace(fn, InvocationKind.AT_MOST_ONCE) }
    if (!cond) {
        fn()
    }
}

fun Long.plusMod(divisor: Long): Long {
    val v = this % divisor
    return if (v < 0) divisor + v else v
}

object ArrayUtils {
    fun equals(a: IntArray, b: IntArray): Boolean {
        return a.contentEquals(b)
    }

    fun compare(a: IntArray, b: IntArray): Int {
        var i = 0
        while (i < a.size && i < b.size) {
            val aVal = a[i]
            val bVal = b[i]
            when {
                aVal < bVal -> return -1
                aVal > bVal -> return 1
            }
            i++
        }
        return when {
            i < a.size -> 1
            i < b.size -> -1
            else -> 0
        }
    }

    fun toString(values: Array<*>): String {
        return "[${values.joinToString(", ")}]"
    }

    fun toString(values: IntArray): String {
        return "[${values.joinToString(", ")}]"
    }
}

fun <T> List<T>.rest(): List<T> {
    if (this.isEmpty()) {
        throw IllegalStateException("Cannot take the rest of an empty list")
    }
    return this.subList(1, this.size)
}

inline fun <T, R : Comparable<R>> List<T>.maxValueBy(fn: (T) -> R): R {
    require(this.isNotEmpty()) { "call to maxValueBy on empty list" }
    var currMax: R? = null
    this.forEach { e ->
        val res = fn(e)
        if (currMax == null || res > currMax!!) {
            currMax = res
        }
    }
    return currMax!!
}

inline fun <T, R> List<T>.reduceWithInitial(initial: R, fn: (R, T) -> R): R {
    var curr = initial
    for (element in this) {
        curr = fn(curr, element)
    }
    return curr
}

inline fun IntArray.reduceWithInitial(initial: Int, fn: (Int, Int) -> Int): Int {
    var curr = initial
    for (element in this) {
        curr = fn(curr, element)
    }
    return curr
}

sealed class Either<out A, out B> {
    class Left<A>(val value: A) : Either<A, Nothing>()
    class Right<B>(val value: B) : Either<Nothing, B>()
}
