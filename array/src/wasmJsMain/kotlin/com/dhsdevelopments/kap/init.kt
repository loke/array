package com.dhsdevelopments.kap

actual fun platformInit(engine: Engine) {
    engine.systemParameters[engine.standardSymbols.platform] = ConstantSymbolSystemParameterProvider(engine.internSymbol("wasm", engine.coreNamespace))
}
