package com.dhsdevelopments.kap.gui.styledarea

import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.gui.settings.ConfigProvider
import javafx.scene.paint.Color
import org.fxmisc.richtext.TextExt
import java.lang.ref.SoftReference

class TextStyle(
    val config: ConfigProvider,
    val type: Type = Type.DEFAULT,
    val promptTag: Boolean = false,
    val contentTag: Any? = null,
    val contentRefs: ClearableStyleContent? = null,
    var innerValuePtr: Int? = null,
    val cssClassName: String? = null
) {
    fun styleContent(content: TextExt) {
        val css = when (type) {
            Type.DEFAULT -> null
            Type.INPUT -> null
            Type.LOG_INPUT -> "editcontent-loginput"
            Type.ERROR -> "editcontent-error"
            Type.ERROR_DETAILS -> "editcontent-error-details"
            Type.PROMPT -> "editcontent-prompt"
            Type.OUTPUT -> "editcontent-output"
            Type.RESULT -> "editcontent-result"
            Type.DETAILS_BUTTON -> "editcontent-detailsbutton"
            Type.SINGLE_CHAR_HIGHLIGHT -> "editcontent-warninghighlight"
            Type.SYNTAX_HIGHLIGHT_SYMBOL -> "editcontent-symbol"
            Type.SYNTAX_HIGHLIGHT_STRING -> "editcontent-string"
            Type.SYNTAX_HIGHLIGHT_NUMBER -> "editcontent-number"
            Type.SYNTAX_HIGHLIGHT_CHAR -> "editcontent-char"
            Type.SYNTAX_HIGHLIGHT_COMMENT -> "editcontent-comment"
            Type.TIMING -> "editcontent-evaluationtime"
        }
        if (css != null) {
            content.styleClass.add(css)
        }
        if (cssClassName != null) {
            content.styleClass.add(cssClassName)
        }
        if (type == Type.SINGLE_CHAR_HIGHLIGHT) {
            content.underlineWidth = 1.0
            content.underlineColor = Color.RED
        }
        content.font = config.selectedFont()
//        content.font = renderContext.font()
    }


    fun copyWithInnerValue(updatedInnerValue: Int, cssClassName: String): TextStyle {
        return TextStyle(config, type, promptTag, contentTag, contentRefs, updatedInnerValue, cssClassName)
    }

    override fun toString(): String {
        return "TextStyle(type=$type, promptTag=$promptTag, contentTag=$contentTag, contentRefs=$contentRefs, innerValuePtr=$innerValuePtr, cssClassName=$cssClassName)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TextStyle

        if (type != other.type) return false
        if (promptTag != other.promptTag) return false
        if (contentTag != other.contentTag) return false
        if (contentRefs != other.contentRefs) return false
        if (innerValuePtr != other.innerValuePtr) return false
        if (cssClassName != other.cssClassName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = type.hashCode()
        result = 31 * result + promptTag.hashCode()
        result = 31 * result + (contentTag?.hashCode() ?: 0)
        result = 31 * result + (contentRefs?.hashCode() ?: 0)
        result = 31 * result + (innerValuePtr ?: 0)
        result = 31 * result + (cssClassName?.hashCode() ?: 0)
        return result
    }

    enum class Type {
        DEFAULT,
        PROMPT,
        INPUT,
        LOG_INPUT,
        OUTPUT,
        RESULT,
        ERROR,
        ERROR_DETAILS,
        DETAILS_BUTTON,
        SINGLE_CHAR_HIGHLIGHT,
        SYNTAX_HIGHLIGHT_SYMBOL,
        SYNTAX_HIGHLIGHT_STRING,
        SYNTAX_HIGHLIGHT_NUMBER,
        SYNTAX_HIGHLIGHT_CHAR,
        SYNTAX_HIGHLIGHT_COMMENT,
        TIMING
    }

    /**
     * This class is used to track any information associated with some content in the repl.
     * The function [clear] is used to ensure that all references to this content is dropped
     * when the repl deletes old data.
     */
    class ClearableStyleContent(rootValue: APLValue?) {
        private var weakRef: SoftReference<InnerClearableStyleContent>? = SoftReference(InnerClearableStyleContent(rootValue))

        val values get() = weakRef?.get()?.values ?: emptyList()
        val rootValue get() = weakRef?.get()?.rootValue

        fun addValue(value: APLValue): Int {
            val n = values.size
            weakRef?.get()?.addValue(value)
            return n
        }

        fun clear() {
            weakRef = null
        }
    }

    private class InnerClearableStyleContent(var rootValue: APLValue?) {
        val values = ArrayList<APLValue>()

        fun addValue(value: APLValue): Int {
            val n = values.size
            values.add(value)
            return n
        }
    }
}
