package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.JsTransferQueue
import org.w3c.xhr.XMLHttpRequest

fun processHttpRequest(msg: dynamic) {
    val queue = jsTransferQueue
    require(queue != null) { "jsTransferQueue is null" }

    queue.waitAndUpdateStateValue(0, JsTransferQueue.STATE_NONE, JsTransferQueue.STATE_HTTP_RESULT) {
        val url = msg.url as String
        val method = msg.method as String

        val http = XMLHttpRequest()
        http.open(method, url)
        http.onload = {
            if (http.readyState == 4.toShort()) {
                queue.sendArray(http.responseText.encodeToByteArray()) {
                    queue.updateState(JsTransferQueue.STATE_NONE)
                }
            } else {
                error("Failed to contact server")
            }
        }
        http.send()
    }
}
