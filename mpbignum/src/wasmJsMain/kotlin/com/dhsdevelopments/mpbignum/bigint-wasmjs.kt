package com.dhsdevelopments.mpbignum

private fun jsMakeBigintFromInt(a: Int): JsAny = js("BigInt(a)")
private fun jsMakeBigintFromDouble(a: Double): JsAny = js("BigInt(a-(a%1))")
private fun jsCompare(a: JsAny, b: JsAny): Boolean = js("a==b")
private fun jsBigintToInt32String(a: JsAny): String = js("BigInt.asIntN(32,a).toString()")
private fun jsBigintToInt64String(a: JsAny): String = js("BigInt.asIntN(64,a).toString()")
private fun jsBigintToDouble(a: JsAny): Double = js("Number(a)")
private fun jsBigintToString(a: JsAny, radix: Int): String = js("a.toString(radix)")

class BigIntWrapper(val value: JsAny) {
    override fun equals(other: Any?): Boolean {
        if (other !is BigIntWrapper) {
            return false
        }
        return jsCompare(value, other.value)
    }

    override fun hashCode(): Int {
        return jsBigintToInt32String(value).toInt()
    }
}

actual value class BigInt(val impl: Any) {
    override fun toString(): String = inner.toString()

    internal val inner: JsAny
        get() {
            return (impl as BigIntWrapper).value
        }

    actual companion object {
        fun makeFromJs(v: JsAny): BigInt {
            return BigInt(BigIntWrapper(v))
        }
    }
}

private fun jsAbsoluteValue(a: JsAny): JsAny = js("(function(){if(a<0){return -a;} else {return a;}})()")
actual val BigInt.absoluteValue get() = BigInt.makeFromJs(jsAbsoluteValue(inner))

private fun jsPlus(a: JsAny, b: JsAny): JsAny = js("a+b")
actual operator fun BigInt.plus(other: BigInt) = BigInt.makeFromJs(jsPlus(inner, other.inner))

private fun jsMinus(a: JsAny, b: JsAny): JsAny = js("a-b")
actual operator fun BigInt.minus(other: BigInt) = BigInt.makeFromJs(jsMinus(inner, other.inner))

private fun jsTimes(a: JsAny, b: JsAny): JsAny = js("a*b")
actual operator fun BigInt.times(other: BigInt) = BigInt.makeFromJs(jsTimes(inner, other.inner))

private fun jsDiv(a: JsAny, b: JsAny): JsAny = js("a/b")
actual operator fun BigInt.div(other: BigInt) = BigInt.makeFromJs(jsDiv(inner, other.inner))

private fun jsUnaryMinus(a: JsAny): JsAny = js("-a")
actual operator fun BigInt.unaryMinus() = BigInt.makeFromJs(jsUnaryMinus(inner))

//When working with string versions:
//return BigInt.makeFromJs(js("(function(a0,b0){return eval(\"a0**b0\");})(BigInt(a),BigInt(b))"))
private fun jsPow(a: JsAny, b: Long): JsAny = js("a**b")
actual fun BigInt.pow(other: Long) = BigInt.makeFromJs(jsPow(inner, other))

private fun jsRem(a: JsAny, b: JsAny): JsAny = js("a%b")
actual operator fun BigInt.rem(other: BigInt) = BigInt.makeFromJs(jsRem(inner, other.inner))

private fun jsCompareTo(a: JsAny, b: JsAny): Int = js("(function(a0,b0){if(a0<b0){return -1;} else if(a0>b0){return 1;} else {return 0;}})(a,b)")
actual operator fun BigInt.compareTo(other: BigInt) = jsCompareTo(inner, other.inner)

actual fun BigInt.Companion.of(value: Short) = makeFromJs(jsMakeBigintFromInt(value.toInt()))
actual fun BigInt.Companion.of(value: Int) = makeFromJs(jsMakeBigintFromInt(value))

actual fun BigInt.Companion.of(value: Long): BigInt {
    val stringified: String = value.toString()
    return BigInt.of(stringified)
}

actual fun BigInt.Companion.of(s: String, radix: Int): BigInt {
    return standardParseWithBase(s, radix)
}

actual fun BigInt.Companion.of(value: Double): BigInt {
    if (!value.isFinite()) {
        throw IllegalArgumentException("number cannot be converted to bigint: ${value}")
    }
    return makeFromJs(jsMakeBigintFromDouble(value))
}

private fun jsAnd(a: JsAny, b: JsAny): JsAny = js("a&b")
actual infix fun BigInt.and(other: BigInt) = BigInt.makeFromJs(jsAnd(inner, other.inner))

private fun jsOr(a: JsAny, b: JsAny): JsAny = js("a|b")
actual infix fun BigInt.or(other: BigInt) = BigInt.makeFromJs(jsOr(inner, other.inner))

private fun jsXor(a: JsAny, b: JsAny): JsAny = js("a^b")
actual infix fun BigInt.xor(other: BigInt) = BigInt.makeFromJs(jsXor(inner, other.inner))

private fun jsInv(a: JsAny): JsAny = js("~a")
actual fun BigInt.inv() = BigInt.makeFromJs(jsInv(inner))

private fun jsShl(a: JsAny, b: Long): JsAny = js("(function(b0){return a<<b0})(BigInt(b))")
actual infix fun BigInt.shl(other: Long) = BigInt.makeFromJs(jsShl(inner, other))

private fun jsShr(a: JsAny, b: Long): JsAny = js("(function(b0){return a>>b0})(BigInt(b))")
actual infix fun BigInt.shr(other: Long) = BigInt.makeFromJs(jsShr(inner, other))

actual fun BigInt.toInt() = jsBigintToInt32String(inner).toInt()
actual fun BigInt.toLong() = jsBigintToInt64String(inner).toLong()
actual fun BigInt.toDouble() = jsBigintToDouble(inner)

private fun jsSignum(a: JsAny): Int = js("(function(a0){if(a0<0){return -1;} else if(a0>0){return 1;} else {return 0;}})(a)")
actual fun BigInt.signum() = jsSignum(inner)

actual fun BigInt.gcd(other: BigInt): BigInt {
    return standardGcd(this, other)
}

actual fun BigInt.toString(radix: Int) = jsBigintToString(inner, radix)

actual fun BigInt.rangeInLong(): Boolean {
    return this >= BigIntConstants.LONG_MIN_VALUE && this <= BigIntConstants.LONG_MAX_VALUE
}

actual fun BigInt.rangeInInt(): Boolean {
    return this >= BigIntConstants.INT_MIN_VALUE && this <= BigIntConstants.INT_MAX_VALUE
}

private fun invertBigintIfNegative(a: JsAny): JsAny = js("a < 0 ? ~a : a")

actual fun BigInt.bitLength(): Long {
    val a0 = invertBigintIfNegative(inner)
    return if (jsSignum(a0) == 0) {
        0
    } else {
        positiveBigintToBinaryString((a0)).length.toLong()
    }
}

private fun positiveBigintToBinaryString(a: JsAny): String = js("a.toString(2)")
private fun negativeBigintToBinaryString(a: JsAny): String = js("(BigInt(-1) - a).toString(2)")

actual fun BigInt.popcnt(): Long {
    val s = if (this < 0) {
        negativeBigintToBinaryString(this.inner)
    } else {
        positiveBigintToBinaryString(this.inner)
    }
    return s.count { ch -> ch == '1' }.toLong()
}

actual fun BigInt.isqrt(): BigInt {
    if (this < 0) {
        throw ArithmeticException("Argument is negative")
    }

    if (this < 2) {
        return this
    }

    val bitLength = positiveBigintToBinaryString(this.inner).length
    val shift = bitLength / 2
    var xk = (this shr shift) shl (shift / 2)

    while (true) {
        val xk1 = ((this / xk) + xk) shr 1
        if (xk1 >= xk) {
            return xk
        }
        xk = xk1
    }
}
