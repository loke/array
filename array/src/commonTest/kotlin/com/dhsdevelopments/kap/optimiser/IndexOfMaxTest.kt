package com.dhsdevelopments.kap.optimiser

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.IdentityAPLFunction
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertFailsWith
import kotlin.test.assertIs

class IndexOfMaxTest : APLTest() {
    @Test
    fun indexOfMin() {
        assertSimpleNumber(5, parseAPLExpression("↑⍋ 2 1 8 1 0 ¯2 0"))
    }

    @Test
    fun indexOfMax() {
        assertSimpleNumber(2, parseAPLExpression("↑⍒ 2 1 8 1 0"))
    }

    @Test
    fun indexOfMinWithTacit() {
        assertSimpleNumber(5, parseAPLExpression("(↑⍋) 2 1 8 1 0 ¯2 0"))
    }

    @Test
    fun indexOfMaxWithTacit() {
        assertSimpleNumber(2, parseAPLExpression("(↑⍒) 2 1 8 1 0"))
    }

    @Test
    fun indexOfMinWithTacitAndExtraLeftFunction() {
        assertSimpleNumber(5, parseAPLExpression("(⊢↑⍋) 2 1 8 1 0 ¯2 0"))
    }

    @Test
    fun indexOfMaxWithTacitAndExtraLeftFunction() {
        assertSimpleNumber(2, parseAPLExpression("(⊢↑⍒) 2 1 8 1 0"))
    }

    @Test
    fun indexOfMinEmptyArg() {
        assertSimpleNumber(0, parseAPLExpression("↑⍋⍬"))
    }

    @Test
    fun indexOfMaxEmptyArg() {
        assertSimpleNumber(0, parseAPLExpression("↑⍒⍬"))
    }

    @Test
    fun indexOfMinMultiDimensionalArray() {
        assertSimpleNumber(0, parseAPLExpression("↑⍋ 3 3 ⍴ 1 2 3 4 3 3 2 1 1"))
    }

    @Test
    fun indexOfMaxMultiDimensionalArray() {
        assertSimpleNumber(1, parseAPLExpression("↑⍒ 3 3 ⍴ 1 2 3 4 3 3 2 1 1"))
    }

    @Test
    fun indexOfMinScalar() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("↑⍒ 0")
        }
    }

    @Test
    fun indexOfMaxScalar() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("↑⍋ 0")
        }
    }

    @Test
    fun indexOfMinIsOptimised() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("↑⍋ 2 1 8 1 0 ¯2 0"))
        assertIs<FunctionCall1Arg>(instr)
        assertIs<MergedIndexOfMinFunction>(instr.fn)
    }

    @Test
    fun indexOfMinWithTacitIsOptimised() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(↑⍋) 2 1 8 1 0 ¯2 0"))
        assertIs<FunctionCall1Arg>(instr)
        assertIs<MergedIndexOfMinFunction>(instr.fn)
    }

    @Test
    fun indexOfMinWithTacitLeftExtraFunctionIsOptimised() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(⊢↑⍋) 2 1 8 1 0 ¯2 0"))
        assertIs<FunctionCall1Arg>(instr)
        val chain0 = instr.fn
        assertIs<FunctionCallChain.Chain2>(chain0)
        assertIs<IdentityAPLFunction.IdentityAPLFunctionImpl>(chain0.fn0)
        assertIs<MergedIndexOfMinFunction>(chain0.fn1)
    }

    @Test
    fun indexOfMaxWithTacitLeftExtraFunctionIsOptimised() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(⊢↑⍒) 2 1 8 1 0 ¯2 0"))
        assertIs<FunctionCall1Arg>(instr)
        val chain0 = instr.fn
        assertIs<FunctionCallChain.Chain2>(chain0)
        assertIs<IdentityAPLFunction.IdentityAPLFunctionImpl>(chain0.fn0)
        assertIs<MergedIndexOfMaxFunction>(chain0.fn1)
    }

    // Optimiser can't handle this case yet
    @Ignore
    @Test
    fun indexOfMinWithTacitRightExtraFunctionIsOptimised() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(⊢↑⍋⊢) 2 1 8 1 0 ¯2 0"))
        assertIs<FunctionCall1Arg>(instr)
        val chain0 = instr.fn
        assertIs<FunctionCallChain.Chain2>(chain0)
        assertIs<IdentityAPLFunction.IdentityAPLFunctionImpl>(chain0.fn0)
        assertIs<MergedIndexOfMinFunction>(chain0.fn1)
    }

    @Test
    fun indexOfMaxIsOptimised() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("↑⍒ 2 1 8 1 0"))
        assertIs<FunctionCall1Arg>(instr)
        assertIs<MergedIndexOfMaxFunction>(instr.fn)
    }

    @Test
    fun indexOfMaxWithTacitIsOptimised() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(↑⍒) 2 1 8 1 0"))
        assertIs<FunctionCall1Arg>(instr)
        assertIs<MergedIndexOfMaxFunction>(instr.fn)
    }

    @Test
    fun indexOfMaxWithLocalExplicitFunction() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("x ⇐ {↑⍒ ⍵} ⋄ x 2 1 8 1 0"))
        val firstInstr = instr.children().first()
        assertIs<APLParser.UpdateLocalFunctionInstruction>(firstInstr)
        assertIs<DeclaredFunction.DeclaredFunctionImpl>(firstInstr.fn)
        val inner = firstInstr.fn as DeclaredFunction.DeclaredFunctionImpl
        val innerInstruction = inner.parentInstr
        assertIs<FunctionCall1Arg>(innerInstruction)
        assertIs<MergedIndexOfMaxFunction>(innerInstruction.fn)
    }
}
