package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.ClientSideJsTransferQueue
import com.dhsdevelopments.kap.JsTransferQueue
import com.dhsdevelopments.kap.jsObject2
import com.dhsdevelopments.kap.keyboard.ExtendedCharsKeyboardInput
import com.dhsdevelopments.kap.keyboard.SYMBOL_BAR_LAYOUT
import com.dhsdevelopments.kap.keyboard.SYMBOL_DOC_LIST
import com.dhsdevelopments.kap.keyboard.SymbolDoc
import com.dhsdevelopments.kap.log.SimpleLogger
import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.dom.addClass
import kotlinx.html.*
import kotlinx.html.dom.create
import kotlinx.html.js.onClickFunction
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.w3c.dom.*
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

object Webclient2Logger : SimpleLogger("kap")

external fun decodeURIComponent(text: String): String
external fun encodeURIComponent(text: String): String

private fun initWorker(): Worker {
    val worker = Worker("compute-queue-worker.js")
    worker.onmessage = { event ->
        val msg: dynamic = event.data
        if (msg.messageType) {
            when (msg.messageType) {
                ENGINE_STARTED_TYPE -> engineAvailableCallback(worker, msg)
                OUTPUT_DESCRIPTOR_TYPE -> processOutput(msg)
                IMAGE_CONTENT_TYPE -> updateImage(msg)
                WINDOW_CREATED_TYPE -> openWindow(msg)
                PLAY_AUDIO_TYPE -> playAudioBuffer(msg.buffer)
                TOKENISE_RESULT_TYPE -> {
                    val reqId = msg.requestId
                    when {
                        TokenisationRequestInfo.isRequestIdEditorRequest(reqId) -> processTokeniseResult(msg)
                        isCommandResultTokenRequest(reqId) -> processCommandResultTokenisationResult(msg)
                    }
                }
                HTTP_REQUEST_TYPE -> processHttpRequest(msg)
                READ_LINE_TYPE -> processReadLine(msg)
                COMPLETION_RESULT_TYPE -> processCompletionResult(msg)
                ADDITIONAL_OUTPUT_CHART -> displayChart(msg)
                ADDITIONAL_OUTPUT_PLOT -> displayPlot(msg)
                ENGINE_RESPONSE -> processWorkerResponse(msg)
            }
        } else {
            when (val response = Json.decodeFromString<ResponseMessage>(event.data as String)) {
                is EvalResponse -> addResponseToResultHistory(response)
                is ExceptionDescriptor -> addExceptionResultToResultHistory(response)
                is EvalExceptionDescriptor -> processEvalExceptionResult(response)
                is OutputDescriptor -> error("not used")
                is EngineStartedDescriptor -> error("not used")
                is AdditionalOutput -> processAdditionalOutput(response)
                is ImportResult -> processImportException(response)
            }
        }
    }
    return worker
}

private fun processAdditionalOutput(response: AdditionalOutput) {
    when (response.content) {
        is WindowCreated -> error("not used")
    }
}

class CurrentOutput(val node: HTMLDivElement) {
    var inProgressNode: HTMLDivElement? = null

    fun appendText(text: String) {
        val parts = text.split("\n")
        val currNode = inProgressNode
        val remaining = if (currNode != null && parts.isNotEmpty()) {
            currNode.textContent = (currNode.textContent ?: "") + parts[0]
            parts.subList(1, parts.size)
        } else {
            parts
        }

        if (remaining.isNotEmpty()) {
            remaining.forEach { s ->
                val n = createDiv("output-result-element").also { n -> node.appendChild(n) }
                n.textContent = (n.textContent ?: "") + s
                inProgressNode = n
            }
        }
    }

    fun appendNode(newNode: HTMLElement) {
        inProgressNode = null
//        val n = createDiv("output-result-element").also { n -> node.appendChild(n) }
//        n.appendChild(newNode)
        node.appendChild(newNode)
    }
}

private fun clearCurrentOutput() {
    currentOutput = null
}

private var currentOutput: CurrentOutput? = null

fun findCurrentOutput(): CurrentOutput {
    return currentOutput ?: createDiv("current-output").let { n ->
        n.classList.add("output-result-outer")
        findResultHistoryNode().appendChild(n)
        CurrentOutput(n).also { c -> currentOutput = c }
    }
}

private fun processOutput(msg: dynamic) {
    val text = msg.text as String
    findCurrentOutput().appendText(text)
}

fun findResultHistoryNode(): HTMLDivElement {
    return findElement("result-history")
}

private fun addResponseToResultHistory(response: EvalResponse) {
    val outer = document.create.div(classes = "return-result source-node") {
        when (response) {
            is StringResponse -> {
                +response.result
            }
            is DataResponse -> {
                formatResponse(response)
            }
        }
    }
    appendNodeToResultHistory(outer)
    scrollToEnd()
}

private fun scrollToEnd() {
    window.scrollTo(0.0, document.body!!.scrollHeight.toDouble())
}

private fun addExceptionResultToResultHistory(response: ExceptionDescriptor) {
    val node = document.create.div(classes = "exception-result") {
        +response.message
    }
    appendNodeToResultHistory(node)
    scrollToEnd()
}

private fun addErrorMessageResultToResultHistory(message: String) {
    val node = document.create.div(classes = "exception-result") {
        +message
    }
    appendNodeToResultHistory(node)
    scrollToEnd()
}

fun processImportException(response: ImportResult) {
    (document.getElementById("import-result") as HTMLDivElement).textContent = response.message
}

private fun showDetailsPopup(text: String) {
    println("showing details text")
    val panel = document.getElementById("details-popup-panel")
    if (panel == null) {
        println("panel was null")
    } else {
        val contentElements = panel.getElementsByClassName("details-popup-content")
        if (contentElements.length != 1) {
            println("Content elements length should be 1, got: ${contentElements.length}")
        } else {
            val e = contentElements[0]
            if (e == null) {
                println("first content element was null")
            } else {
                e.textContent = text
                panel.addClass("details-popup-display")
            }
        }
    }
}

private fun processEvalExceptionResult(response: EvalExceptionDescriptor) {
    val detailsDiv = if (response.details == null) {
        null
    } else {
        document.create.div("exception-result-details exception-result-details-hidden") {
            +response.details
        }
    }
    val node = document.create.div("exception-result") {
        +response.message
        if (detailsDiv != null) {
            +" "
            span("details-button") {
                onClickFunction = { detailsDiv.classList.toggle("exception-result-details-hidden") }
                +"Details"
            }
        }
    }
    if (detailsDiv != null) {
        node.appendChild(detailsDiv)
    }
    appendNodeToResultHistory(node)

    // If the stack frame contained an error in the input field (it should), then highlight that in the
    // input history.
    val inputPos = response.inputPos
    if (inputPos != null) {
        val element = document.getElementById(makeElementIdFromRequestId(response.requestId)) as HTMLSpanElement?
        if (element != null) {
            highlightErrorForPos(element, inputPos)
        }
    }

    scrollToEnd()
}

private fun highlightErrorForPos(element: HTMLSpanElement, inputPos: PosDescriptor) {
    val elementText = element.textContent
    if (elementText != null) {
        var curr = 0
        repeat(inputPos.line) { i ->
            curr = elementText.indexOf('\n', curr)
            if (curr == -1) {
                return
            }
            curr++
        }
        val startIndex = curr + inputPos.col

        repeat(inputPos.endLine - inputPos.line) { i ->
            curr = elementText.indexOf('\n', curr)
            if (curr == -1) {
                return
            }
            curr++
        }
        val endIndex = curr + inputPos.endCol

        val prefix = elementText.substring(0, startIndex)
        val errorPart = elementText.substring(startIndex, endIndex)
        val suffix = elementText.substring(endIndex)
        element.textContent = ""
        element.appendChild(createSpan().apply { textContent = prefix })
        element.appendChild(createSpan("input-history-error").apply { textContent = errorPart })
        element.appendChild(createSpan().apply { textContent = suffix })
    }
}

private const val COMMAND_RESULT_TOKENISATION_REQUEST_PREFIX = "command-res"

private fun makeElementIdFromRequestId(requestId: String): String {
    return "command-id-${requestId}"
}

@OptIn(ExperimentalEncodingApi::class)
private fun addCommandResultToResultHistory(command: CommandDescriptor, worker: Worker) {
    val elementId = makeElementIdFromRequestId(command.requestId)
    val node = document.create.div(classes = "command-result-outer") {
        div(classes = "command-result-inner") {
            span(classes = "command-result-text") {
                id = elementId
                onClickFunction = { updateInputText(command.text) }
                +command.text
            }
            span(classes = "command-result-link") {
                val encoded = Base64.Default.encode(command.text.trim().encodeToByteArray())
                +" "
                a("#c=${encoded}") {
                    +"Link"
                }
            }
        }
    }

    appendNodeToResultHistory(node)

    worker.postMessage(Json.encodeToString<Request>(TokeniseRequest("${COMMAND_RESULT_TOKENISATION_REQUEST_PREFIX}-${elementId}", command.text)))
}

private fun isCommandResultTokenRequest(reqId: String): Boolean {
    return reqId.startsWith(COMMAND_RESULT_TOKENISATION_REQUEST_PREFIX)
}

private fun processCommandResultTokenisationResult(msg: dynamic) {
    val tokenList = msg.tokens as Array<dynamic>
    val requestId: String = msg.requestId
    require(requestId.startsWith("$COMMAND_RESULT_TOKENISATION_REQUEST_PREFIX-"))
    val nodeId = requestId.substring(COMMAND_RESULT_TOKENISATION_REQUEST_PREFIX.length + 1)
    val node = findElement<HTMLSpanElement>(nodeId)
    val text = node.textContent
    if (text != null) {
        node.textContent = ""

        val lines = text.split("\n")
        var currIndex = 0
        val indexes = IntArray(lines.size) { i ->
            currIndex.also {
                currIndex += lines[i].length + 1
            }
        }

        var currPos = 0
        tokenList.forEach { token ->
            val pos = token.p
            val line = pos.line as Int
            val col = pos.col as Int
            val endLine = pos.endLine as Int
            val endCol = pos.endCol as Int
            val startIndex = indexes[line] + col
            val endIndex = indexes[endLine] + endCol
            if (currPos < startIndex) {
                val s = text.substring(currPos, startIndex)
                node.appendChild(createSpan().apply { textContent = s })
            }
            if (startIndex < endIndex) {
                val s = text.substring(startIndex, endIndex)
                val tokenType = token.type as Int
                val styleClass = if (tokenType >= 0 && tokenType < tokenTypeToStyle.size) {
                    tokenTypeToStyle[tokenType]
                } else {
                    null
                }
                node.appendChild(createSpan(styleClass).apply {
                    textContent = s
                })
            }
            currPos = endIndex
        }
        if (currPos < text.length) {
            val s = text.substring(currPos)
            node.appendChild(createSpan().apply { textContent = s })
        }
    }
}

fun updateInputText(command: String) {
    val editor = editorGlobal ?: throw IllegalStateException("Editor is not configured")
    editor.updateContent(command)
}

private fun appendNodeToResultHistory(outer: HTMLElement) {
    findResultHistoryNode().appendChild(outer)
    clearCurrentOutput()
}

private fun sendCommandFromField(command: String, worker: Worker) {
    require(commandInputEnabled) { "Command input is not enabled" }
    val trimmed = command.trim()
    if (trimmed.startsWith("]")) {
        val cmd = trimmed.substring(1).trim()
        if (cmd != "") {
            updateInputText("")
            val req = makeEngineRequest(EngineRequestType.COMMAND, jsObject2("cmd" to cmd))
            sendEngineRequest(worker, req) { res ->
                if (!res.success) {
                    addErrorMessageResultToResultHistory(res.message)
                }
            }
        }
    } else if (trimmed != "") {
        val desc = CommandDescriptor(RequestIdGenerator.nextValue(), command)
        addCommandResultToResultHistory(desc, worker)
        sendCommand(worker, desc)
    }
}

private fun interruptEvaluation() {
    jsTransferQueue?.updateState(JsTransferQueue.STATE_BREAK)
}

inline fun <reified T : HTMLElement> findElement(id: String): T {
    return getElementByIdOrFail(id) as T
}

fun createDiv(className: String? = null) = createElementWithClassName("div", className) as HTMLDivElement
fun createSpan(className: String? = null) = createElementWithClassName("span", className) as HTMLSpanElement
fun createCanvas(className: String? = null) = createElementWithClassName("canvas", className) as HTMLCanvasElement

fun createElementWithClassName(type: String, className: String?): HTMLElement {
    val element = document.createElement(type)
    if (className != null) {
        element.className = className
    }
    return element as HTMLElement
}

fun getElementByIdOrFail(id: String): Element {
    return document.getElementById(id) ?: throw IllegalStateException("element not found: ${id}")
}

fun main() {
    val worker = initWorker()
    initFileUpload(worker)
}

/*
    <input class="kap-input" type="text" id="input" size="60">
    <button id="send-button">Send</button>
 */

private var engineInit = false
var jsTransferQueue: ClientSideJsTransferQueue? = null
var editorGlobal: EditorViewHolder? = null
var commandInputEnabled = false

@OptIn(ExperimentalEncodingApi::class)
fun engineAvailableCallback(worker: Worker, msg: dynamic) {
    if (engineInit) {
        throw IllegalStateException("Client already initialised")
    }

    engineInit = true
    val msgB = msg.sharedBuffers
    jsTransferQueue = if (msgB == null) {
        null
    } else {
        ClientSideJsTransferQueue.make(msgB)
    }

    val loadingElement = findElement<HTMLDivElement>("loading-message")
    loadingElement.remove()

    val topElement = findElement<HTMLDivElement>("top")
    val outer = document.create.div {
        div {
            style = "margin-top: 1em;"
            createKeyboardHelp()
        }
        div {
            style = "margin-top: 1em;"
            id = "result-history"
        }
        div {
            id = "status-wrapper"
        }
        div {
            id = "code-editor-wrapper"
        }
        div {
            id = "response-input-wrapper"
            hidden = true
            span {
                span {
                    id = "response-input-prompt"
                }
                input(type = InputType.text) {
                    id = "response-input-field"
                    size = "100"
                }
            }
        }
        div {
            button {
                id = "send-button"
                +"Send"
            }
            +" "
            button {
                id = "stop-button"
                +"Stop"
            }
        }
    }

    topElement.appendChild(outer)
    val codeEditorWrapperElement = findElement<HTMLDivElement>("code-editor-wrapper")
    val returnCallback = { content: String -> sendCommandFromField(content, worker) }
    val prefixConfiguration = PrefixConfiguration(ExtendedCharsKeyboardInput(), "`")
    val editor = addEditor(worker, codeEditorWrapperElement, prefixConfiguration, returnCallback)
    editorGlobal = editor
    addSymbolBar(editor, prefixConfiguration)

    val prefixTextfield = findElement<HTMLInputElement>("prefix-key-textfield")
    prefixTextfield.value = prefixConfiguration.prefixKey
    prefixTextfield.onchange = { event ->
        val s = prefixTextfield.value.trim()
        if (s.length == 1) {
            prefixConfiguration.updatePrefix(s)
        }
    }

//    val inputField = findElement<HTMLTextAreaElement>("input")
//    configureAPLInputForField(inputField) { sendCommandFromField(worker) }

    initResponseInputField()

    val sendButton = findElement<HTMLButtonElement>("send-button")
    sendButton.onclick = { sendCommandFromField(editor.editableText.text(), worker) }

    val stopButton = findElement<HTMLButtonElement>("stop-button")
    stopButton.onclick = { interruptEvaluation() }

    val location = document.location
    if (location != null) {
        if (location.hash.startsWith("#")) {
            val hashContent = location.hash.substring(1)
            val initialCommand = if (hashContent.contains('=')) {
                val parts = hashContent.split('&')
                val args = HashMap<String, String>()
                parts.forEach { s ->
                    val index = s.indexOf('=')
                    if (index > 0) {
                        args[s.substring(0, index)] = s.substring(index + 1)
                    }
                }
                val s = args["c"]
                if (s != null) {
                    Base64.Default.decode(s).decodeToString()
                } else {
                    null
                }
            } else {
                val s = decodeURIComponent(location.hash.substring(1)).trim()
                s.ifBlank {
                    null
                }
            }
            if (initialCommand != null) {
                editor.updateContent(initialCommand)
//                addCommandResultToResultHistory(initialCommand)
//                sendCommand(worker, initialCommand)
            }
        }
    }

    initChart()

    editor.focus()

    engineInit = true
    commandInputEnabled = true
}

fun addSymbolBar(editor: EditorViewHolder, prefixConfiguration: PrefixConfiguration) {
    val languageBarRoot = document.create.div {
        div("lang-bar-lb") {
            id = "lang-bar"
            table {
                tbody {
                    tr("lang-bar-symbol") {
                        SYMBOL_BAR_LAYOUT.forEach { symbol ->
                            td {
                                if (symbol == ' ') {
                                    b {
                                        span("lang-bar-spacing") {
                                            +" "
                                        }
                                    }
                                } else {
                                    val symbolAsString = symbol.toString()
                                    val desc = SYMBOL_DOC_LIST[symbolAsString]
                                    if (desc == null) {
                                        println("No description found for symbol: '${symbolAsString}'")
                                    } else {
                                        b("lang-bar-selectable") {
                                            title = makeTitle(symbolAsString, desc, prefixConfiguration)
                                            onClickFunction = {
                                                editor.insertCharAndCommit(symbolAsString)
                                                editor.focus()
                                            }
                                            +symbolAsString
                                        }
                                    }
                                }
                            }
                        }
                    }
                    tr("lang-bar-key-row") {
                        id = "lang-bar-key-name"
                        SYMBOL_BAR_LAYOUT.forEach { symbol ->
                            td {
                                if (symbol != ' ') {
                                    val symbolAsString = symbol.toString()
                                    val key = prefixConfiguration.keyboardInput.keymap2[symbolAsString]
                                    if (key != null) {
                                        +key.character
                                    }
                                }
                            }
                        }
                    }
                }
            }
            span("lang-bar-x") {
                title = "Close"
                onClickFunction = { hideLanguageBar() }
                +"❎"
            }
        }
    }
    document.body!!.appendChild(languageBarRoot)
}

private fun hideLanguageBar() {
    val langBar = findElement<HTMLElement>("lang-bar")
    langBar.addClass("lang-bar-closed")
}

fun makeTitle(s: String, desc: SymbolDoc, prefixConfiguration: PrefixConfiguration): String {
    val buf = StringBuilder()
    buf.append(s)
    desc.monadicName?.let { n ->
        buf.append("\nMonadic: ${n}")
    }
    desc.dyadicName?.let { n ->
        buf.append("\nDyadic: ${n}")
    }
    desc.monadicOperator?.let { n ->
        buf.append("\nMonadic operator: ${n}")
    }
    desc.dyadicOperator?.let { n ->
        buf.append("\nDyadic operator: ${n}")
    }
    desc.specialDescription?.let { n ->
        buf.append("\n${n}")
    }
    prefixConfiguration.keyboardInput.keymap2[s]?.let { n ->
        buf.append("\nInput: ${prefixConfiguration.prefixKey} ${n.character}")
    }
    return buf.toString()
}
