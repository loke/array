package com.dhsdevelopments.kap.clientweb2

const val CHART_TYPE_LINE = "line"
const val CHART_TYPE_BAR = "bar"
const val CHART_TYPE_PIE = "pie"
const val CHART_TYPE_DOUGHNUT = "doughnut"
const val CHART_TYPE_SCATTER = "scatter"
const val CHART_TYPE_SURFACE = "surface"

const val ADDITIONAL_OUTPUT_CHART = "additional-output-chart"
const val ADDITIONAL_OUTPUT_PLOT = "additional-output-plot"
