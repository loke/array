package com.dhsdevelopments.kap.optimiser

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.AddAPLFunction
import com.dhsdevelopments.kap.builtins.IotaAPLFunction
import com.dhsdevelopments.kap.builtins.IotaArrayImpls
import com.dhsdevelopments.kap.builtins.SubAPLFunction
import com.dhsdevelopments.mpbignum.addExact

object IotaAddOptimiser : InstructionOptimiser {
    override fun attemptOptimise(instr: Instruction): Instruction? {
        val (width, offset, pos) = findWidthAndOffset(instr) ?: return null

        // Resulting range must stay within the range of an int
        if (width < 0 || width > Int.MAX_VALUE || offset < Int.MIN_VALUE) {
            return null
        }

        // Use exact arithmetic to check if the resulting range will overflow
        if (width > 0) {
            try {
                addExact(width - 1, offset)
            } catch (_: ArithmeticException) {
                return null
            }
        }

        val widthAsInt = width.toInt()
        val instr = if (offset == 0L) {
            IotaArrayImpls.IotaArrayLong(widthAsInt)
        } else {
            IotaArrayImpls.ResizedIotaArrayLong(dimensionsOfSize(widthAsInt), widthAsInt, offset)
        }
        return ConstantValueInstruction(instr, pos)
    }

    private fun findWidthAndOffset(instr: Instruction): WidthAndOffset? {
        tryLeftArgument(instr)?.let { v -> return v }
        tryRightArgument(instr)?.let { v -> return v }
        return null
    }

    private fun tryLeftArgument(instr: Instruction): WidthAndOffset? {
        if (instr !is FunctionCall2Arg) return null
        if (instr.fn !is AddAPLFunction.AddAPLFunctionImpl) return null
        if (instr.leftArgs !is LiteralInteger) return null
        if (instr.rightArgs !is FunctionCall1Arg) return null
        if (instr.rightArgs.fn !is IotaAPLFunction.IotaAPLFunctionImpl) return null
        if (instr.rightArgs.rightArgs !is LiteralInteger) return null

        val width = instr.rightArgs.rightArgs.value
        val offset = instr.leftArgs.value
        return WidthAndOffset(width, offset, instr.leftArgs.pos.expandToEnd(instr.rightArgs.rightArgs.pos))
    }

    private fun tryRightArgument(instr: Instruction): WidthAndOffset? {
        if (instr !is FunctionCall2Arg) return null
        val offset = when (instr.fn) {
            is AddAPLFunction.AddAPLFunctionImpl -> {
                if (instr.rightArgs !is LiteralInteger) return null
                instr.rightArgs.value
            }
            is SubAPLFunction.SubAPLFunctionImpl -> {
                if (instr.rightArgs !is LiteralInteger) return null
                -instr.rightArgs.value
            }
            else -> {
                return null
            }
        }
        if (instr.leftArgs !is FunctionCall1Arg) return null
        if (instr.leftArgs.fn !is IotaAPLFunction.IotaAPLFunctionImpl) return null

        val iotaArg = instr.leftArgs.rightArgs
        if (iotaArg !is LiteralInteger) return null
        val width = iotaArg.value

        return WidthAndOffset(width, offset, instr.leftArgs.pos.expandToEnd(instr.rightArgs.pos))
    }

    data class WidthAndOffset(val width: Long, val offset: Long, val pos: Position)
}
