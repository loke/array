@file:OptIn(NativeRuntimeApi::class)

package com.dhsdevelopments.kap

import kotlin.native.runtime.GC
import kotlin.native.runtime.NativeRuntimeApi

actual fun nativeTestInit() {
}

actual fun tryGc() {
    GC.collect()
}
