package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*

class InternFunction : APLFunctionDescriptor {
    class InternFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val nameString = b.toStringValue(pos)
            val kwString = a.toStringValue(pos)
            val ns = context.engine.makeNamespace(kwString)
            val sym = context.engine.internSymbol(nameString, ns, exported = false)
            return APLSymbol(sym)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = InternFunctionImpl(instantiation)
}

class SymbolNameFunction : APLFunctionDescriptor {
    class SymbolNameFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val sym = a.ensureSymbol(pos).value
            return APLArrayImpl(dimensionsOfSize(2), arrayOf(APLString.make(sym.symbolName), APLString.make(sym.namespace.name)))
        }
    }

    override fun make(instantiation: FunctionInstantiation) = SymbolNameFunctionImpl(instantiation)
}
