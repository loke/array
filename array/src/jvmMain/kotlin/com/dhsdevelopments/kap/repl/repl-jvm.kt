package com.dhsdevelopments.kap.repl

import com.dhsdevelopments.kap.APLGenericException
import com.dhsdevelopments.kap.FormattedEvalResult

class WrappedKapException(cause: Throwable) : APLGenericException("JVM exception: ${cause::class.simpleName}: ${cause.message}", cause = cause)

class JvmReplExceptionTransformer : ReplExceptionTransformer {
    override fun callAndHandleExceptions(fn: () -> FormattedEvalResult): FormattedEvalResult {
        return callAndHandle(fn)
    }

    companion object {
        inline fun <T> callAndHandle(fn: () -> T): T {
            return try {
                fn()
            } catch (e: OutOfMemoryError) {
                throw WrappedKapException(e)
            } catch (e: StackOverflowError) {
                throw WrappedKapException(e)
            }
        }
    }
}
