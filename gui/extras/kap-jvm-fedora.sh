#!/bin/sh

JAVA_HOME=/usr/lib/jvm/java-21-openjdk
export JAVA_HOME

exec /opt/kap/bin/kap-jvm "$@"
