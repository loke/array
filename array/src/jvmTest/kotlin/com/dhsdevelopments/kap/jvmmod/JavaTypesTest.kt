package com.dhsdevelopments.kap.jvmmod

import com.dhsdevelopments.kap.*
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

@Suppress("unused")
class TypesExample() {
    fun createString() = "foostring"
    fun createLong() = 1L
}

@Suppress("unused")
class SimpleExample() {
    override fun toString() = "foo"
    fun abc(): String? = null
}

@Suppress("unused")
open class SuperExample() {
    @JvmField
    val a: Int = 10
}

@Suppress("unused")
class ChildExample : SuperExample() {
    @JvmField
    val b: Int = 100

    @JvmField
    val s: String = "test"
}

class JavaTypesTest : APLTest() {
    @Test
    fun convertToPrimitiveType() {
        val src =
            """
            |(jvm:toJvmFloat 50.0) (jvm:toJvmDouble 60.0) (jvm:toJvmShort 10) (jvm:toJvmInt 20) (jvm:toJvmLong 30) (jvm:toJvmByte 40) (jvm:toJvmChar @a-@\u0)
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(7), result)
            verifyJvmValue(50.0.toFloat(), result.valueAt(0))
            verifyJvmValue(60.0, result.valueAt(1))
            verifyJvmValue(10.toShort(), result.valueAt(2))
            verifyJvmValue(20, result.valueAt(3))
            verifyJvmValue(30.toLong(), result.valueAt(4))
            verifyJvmValue(40.toByte(), result.valueAt(5))
            verifyJvmValue('a', result.valueAt(6))
        }
    }

    @Test
    fun convertToBooleanTest() {
        val src =
            """
            |(jvm:toJvmBoolean 0) (jvm:toJvmBoolean 1) (jvm:toJvmBoolean 10) (jvm:toJvmBoolean 100 200 150)
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(4), result)
            verifyJvmValue(false, result.valueAt(0))
            verifyJvmValue(true, result.valueAt(1))
            verifyJvmValue(true, result.valueAt(2))
            verifyJvmValue(true, result.valueAt(3))
        }
    }

    @Test
    fun convertToKapTestString() {
        val src =
            """
            |typesExampleClass ← jvm:findClass "com.dhsdevelopments.kap.jvmmod.TypesExample"
            |constructor ← jvm:findConstructor typesExampleClass
            |a ← jvm:createInstance constructor
            |createStringMethod ← jvm:findMethod (typesExampleClass ; "createString")
            |jvm:fromJvm jvm:callMethod (createStringMethod ; a)
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertString("foostring", result)
        }
    }

    @Test
    fun convertToKapTestLong() {
        val src =
            """
            |typesExampleClass ← jvm:findClass "com.dhsdevelopments.kap.jvmmod.TypesExample"
            |constructor ← jvm:findConstructor typesExampleClass
            |a ← jvm:createInstance constructor
            |createLongMethod ← jvm:findMethod (typesExampleClass ; "createLong")
            |jvm:fromJvm jvm:callMethod (createLongMethod ; a)
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun missingMethodName() {
        val src =
            """
            |typesExampleClass ← jvm:findClass "com.dhsdevelopments.kap.jvmmod.TypesExample"
            |constructor ← jvm:findConstructor typesExampleClass
            |a ← jvm:createInstance constructor
            |createLongMethod ← jvm:findMethod (typesExampleClass ; "createLong")
            |jvm:fromJvm jvm:callMethod createLongMethod
            """.trimMargin()
        assertFailsWith<APLArgumentCountMismatch> {
            parseAPLExpression(src)
        }
    }

    @Test
    fun toStringCalledWhenPrinting() {
        val src =
            """
            |cl ← jvm:findClass "com.dhsdevelopments.kap.jvmmod.SimpleExample"
            |constructor ← jvm:findConstructor cl
            |jvm:createInstance constructor
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertEquals("foo", result.formatted(FormatStyle.PLAIN))
        }
    }

    @Test
    fun findPrimitiveTypeClass() {
        val src =
            """
            |(jvm:findPrimitiveTypeClass 'jvm:char) `
            |   (jvm:findPrimitiveTypeClass 'jvm:byte) `
            |   (jvm:findPrimitiveTypeClass 'jvm:short) `
            |   (jvm:findPrimitiveTypeClass 'jvm:int) `
            |   (jvm:findPrimitiveTypeClass 'jvm:long) `
            |   (jvm:findPrimitiveTypeClass 'jvm:float) `
            |   (jvm:findPrimitiveTypeClass 'jvm:double) `
            |   (jvm:findPrimitiveTypeClass 'jvm:void)
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(8), result)
            listOf(
                java.lang.Character.TYPE, java.lang.Byte.TYPE, java.lang.Short.TYPE, java.lang.Integer.TYPE,
                java.lang.Long.TYPE, java.lang.Float.TYPE, java.lang.Double.TYPE, java.lang.Void.TYPE
            ).forEachIndexed { i, type ->
                verifyJvmValue(type, result.valueAt(i))
            }
        }
    }

    @Test
    fun testInstanceOf() {
        assertSimpleNumber(1, parseAPLExpression("jvm:instanceOf (jvm:toJvmString \"abc\" ; jvm:findClass \"java.lang.String\")"))
        assertSimpleNumber(
            0,
            parseAPLExpression("cl ← jvm:findClass \"com.dhsdevelopments.kap.jvmmod.SimpleExample\"\njvm:instanceOf (jvm:createInstance (jvm:findConstructor cl) ; jvm:findClass \"java.lang.String\")"))
        assertSimpleNumber(
            1,
            parseAPLExpression("cl ← jvm:findClass \"com.dhsdevelopments.kap.jvmmod.ChildExample\"\njvm:instanceOf (jvm:createInstance (jvm:findConstructor cl) ; jvm:findClass \"com.dhsdevelopments.kap.jvmmod.SuperExample\")"))
    }

    private fun verifyJvmValue(expected: Any, v: APLValue) {
        assertTrue(v is JvmInstanceValue)
        val instance = v.instance
        assertEquals(expected, instance)
    }

    @Test
    fun javaFieldTest() {
        val src =
            """
            |childExampleClass ← jvm:findClass "com.dhsdevelopments.kap.jvmmod.ChildExample"
            |childExampleConstructor ← jvm:findConstructor childExampleClass
            |instance ← jvm:createInstance childExampleConstructor
            |read ⇐ { jvm:fromJvm jvm:getField (jvm:findField (childExampleClass; ⍵) ; instance) }
            |(read "a") (read "b") (read "s")
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assert1DArray(arrayOf<Any>(10, 100, "test"), result)
        }
    }
}
