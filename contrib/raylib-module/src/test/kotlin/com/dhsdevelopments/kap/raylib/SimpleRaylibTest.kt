package com.dhsdevelopments.kap.raylib

import kotlin.test.Ignore
import kotlin.test.Test

@Ignore
class SimpleRaylibTest : AbstractRaylibTest() {
    @Test
    fun createWindowTest() {
        val src =
            """
            |cols ⇐ { ⍉(⊂⍺ ⍳⍨ labels ⍵) ⌷ ⍉⍵ }
            |col ⇐ ⊂⍛cols
            |
            |^ ← toList ⍬
            |
            |raylib:InitWindow (500 ; 400 ; "Raylib test window")
            |
            |camera ← raylib:initCamera2D :zoom 1.0
            |
            |raylib:SetTargetFPS 60
            |while (~raylib:WindowShouldClose^) {
            |  if (raylib:IsMouseButtonDown 1) {
            |    delta ← raylib:GetMouseDelta^
            |    delta ← delta × ¯1 ÷ ⊃"zoom" col camera
            |    camera ← camera raylib:initCamera2D :target (delta + ⊃"target" col camera)
            |  }
            |  
            |  raylib:BeginDrawing^
            |  raylib:ClearBackground 0 0 0 255
            |  raylib:BeginMode2D camera
            |  raylib:rlPushMatrix^
            |  raylib:rlTranslatef (0 ; 25×50 ; 0)
            |  raylib:rlRotatef (90 ; 1 ; 0 ; 0)
            |  raylib:DrawGrid (100 ; 50)
            |  raylib:rlPopMatrix^
            |  raylib:DrawCircle (100 ; 100 ; 50 ; 0 255 0 255)
            |  raylib:EndMode2D^
            |  raylib:DrawText ("This is a Kap version of the raylib example code" ; 10 ; 10 ; 20 ; 255 255 255 255)
            |  raylib:EndDrawing^
            |}
            |raylib:CloseWindow^
            """.trimMargin()
        parseAPLExpression(src, withStandardLib = true)
    }
}
