package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.mpbignum.LongExpressionOverflow

class ForEachResult1Arg(
    val context: RuntimeContext,
    val fn: APLFunction,
    val a: APLValue,
    val axis: APLValue?,
    val pos: Position,
    val savedStack: StorageStack.StorageStackFrame?
) : APLArray() {
    override val dimensions = a.dimensions
    override val metadata by lazy { maybeDefaultMetadata(a.metadata) { m -> ForEachResult1ArgMetadata(m) } }
    override val specialisedType = run {
        val aType = a.specialisedType
        val optflags = fn.optimisationFlags
        when {
            aType.isLong && optflags.is1ALong -> ArrayMemberType.LONG
            aType.isDouble && optflags.is1ADouble -> ArrayMemberType.DOUBLE
            else -> ArrayMemberType.GENERIC
        }
    }

    override val rank get() = a.rank
    override val size get() = a.size

    override fun valueAt(p: Int) = withPossibleSavedStack(savedStack) { fn.eval1Arg(context, a.valueAt(p), axis) }

    override fun valueAtLong(p: Int): Long {
        withPossibleSavedStack(savedStack) {
            val a0 = try {
                a.valueAtLong(p)
            } catch (e: LongExpressionOverflow) {
                val res = fn.eval1Arg(context, e.result.makeAPLNumber(), axis).ensureNumber(pos).asBigInt(pos)
                throw LongExpressionOverflow(res)
            }
            return fn.eval1ArgLong(context, a0, axis)
        }
    }

    override fun valueAtDouble(p: Int) = fn.eval1ArgDouble(context, a.valueAtDouble(p), axis)

    override fun collapseInt(withDiscard: Boolean): APLValue {
        return if (withDiscard) {
            iterateMembers { v ->
                v.collapse(withDiscard = true)
            }
            UnusedResultAPLValue
        } else {
            super.collapseInt(withDiscard = false)
        }
    }

    class ForEachResult1ArgMetadata(private val sourceMetadata: APLValueMetadata) : APLValueMetadata {
        override val labels by lazy { sourceMetadata.labels }
    }
}

class ForEachResult2Arg(
    val context: RuntimeContext,
    val fn: APLFunction,
    val a: APLValue,
    val b: APLValue,
    val axis: APLValue?,
    val pos: Position,
    val savedStack: StorageStack.StorageStackFrame?
) : APLArray() {
    override val specialisedType: ArrayMemberType = run {
        val aType = a.specialisedType
        val bType = b.specialisedType
        val optflags = fn.optimisationFlags
        when {
            aType.isLong && bType.isLong && optflags.is2ALongLong -> ArrayMemberType.LONG
            aType.isDouble && bType.isDouble && optflags.is2ADoubleDouble -> ArrayMemberType.DOUBLE
            else -> ArrayMemberType.GENERIC
        }
    }

    init {
        unless(a.dimensions.compareEquals(b.dimensions)) {
            throwAPLException(InvalidDimensionsException("Arguments to foreach does not have the same dimensions. a=${a.dimensions}, b=${b.dimensions}", pos))
        }
    }

    override val dimensions get() = a.dimensions
    override val rank get() = a.rank
    override val size get() = a.size

    override fun valueAt(p: Int) = withPossibleSavedStack(savedStack) { fn.eval2Arg(context, a.valueAt(p), b.valueAt(p), axis) }

    override fun valueAtLong(p: Int) = withPossibleSavedStack(savedStack) {
        withPossibleSavedStack(savedStack) {
            eval2ArgLongFnOnIndexes(
                a, p, b, p,
                { a0, b0 -> fn.eval2ArgLongToLongWithAxis(context, a0, b0, null) },
                { a0, b0 -> fn.eval2Arg(context, a0, b0, null) })
        }
    }

    override fun valueAtDouble(p: Int) = withPossibleSavedStack(savedStack) {
        fn.eval2ArgDoubleToDoubleWithAxis(context, a.valueAtDouble(p), b.valueAtDouble(p), axis)
    }

    override fun collapseInt(withDiscard: Boolean): APLValue {
        return if (withDiscard) {
            iterateMembers { v ->
                v.collapse(withDiscard = true)
            }
            UnusedResultAPLValue
        } else {
            super.collapseInt(withDiscard = false)
        }
    }
}

class ForEachFunctionDescriptor(val fnInner: APLFunction) : APLFunctionDescriptor {
    class ForEachFunctionImpl(pos: FunctionInstantiation, fn: APLFunction) : APLFunction(pos, listOf(fn)), ParallelSupported {

        private val saveStackSupport = SaveStackSupport(this)

        override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            return if (a.isScalar()) {
                return EnclosedAPLValue.make(fn.eval1Arg(context, a.disclose(), null))
            } else {
                ForEachResult1Arg(context, fn, a, axis, pos, saveStackSupport.savedStack())
            }
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            return compute2Arg(context, fn, a, b, axis, pos, saveStackSupport.savedStack())
        }

        override fun computeParallelTasks1Arg(
            context: RuntimeContext, numTasks: Int, a: APLValue, axis: APLValue?
        ): ParallelTaskList {
            val res = eval1Arg(context, a, axis)
            return ParallelCompressTaskList.make(res, numTasks, pos)
        }

        override fun computeParallelTasks2Arg(
            context: RuntimeContext, numTasks: Int, a: APLValue, b: APLValue, axis: APLValue?
        ): ParallelTaskList {
            val res = eval2Arg(context, a, b, axis)
            return ParallelCompressTaskList.make(res, numTasks, pos)
        }

        override val underlyingFunction get() = this

        override fun copy(fns: List<APLFunction>) = ForEachFunctionImpl(instantiation, fns[0])

        val fn = fns[0]
    }

    override fun make(instantiation: FunctionInstantiation): APLFunction {
        return ForEachFunctionImpl(instantiation, fnInner)
    }

    companion object {
        fun compute2Arg(
            context: RuntimeContext,
            fn: APLFunction,
            a: APLValue,
            b: APLValue,
            axis: APLValue?,
            pos: Position,
            savedStack: StorageStack.StorageStackFrame?
        ): APLValue {
            if (a.isScalar() && b.isScalar()) {
                return EnclosedAPLValue.make(fn.eval2Arg(context, a.disclose(), b.disclose(), axis).unwrapDeferredValue())
            }
            val a1 = if (a.isScalar()) {
                ConstantArray(b.dimensions, a.disclose())
            } else {
                a
            }
            val b1 = if (b.isScalar()) {
                ConstantArray(a.dimensions, b.disclose())
            } else {
                b
            }
            return ForEachResult2Arg(context, fn, a1, b1, axis, pos, savedStack)
        }
    }
}

class ForEachOp : APLOperatorOneArg {
    override fun combineFunction(fn: APLFunction, pos: FunctionInstantiation) = ForEachFunctionDescriptor(fn)
}
