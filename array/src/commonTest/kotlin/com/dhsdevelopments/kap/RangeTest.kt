package com.dhsdevelopments.kap

import com.dhsdevelopments.mpbignum.*
import kotlin.test.Test
import kotlin.test.assertFailsWith

class RangeTest : APLTest() {
    @Test
    fun scalarToScalarRange() {
        parseAPLExpression("2 … 9").let { result ->
            assert1DArray(arrayOf(2, 3, 4, 5, 6, 7, 8, 9), result)
        }
    }

    @Test
    fun oneDimensionalArrayRange() {
        parseAPLExpression("(,2) … (,9)").let { result ->
            assert1DArray(arrayOf(2, 3, 4, 5, 6, 7, 8, 9), result)
        }
    }

    @Test
    fun scalarDecreasingRange() {
        parseAPLExpression("10 … 6").let { result ->
            assert1DArray(arrayOf(10, 9, 8, 7, 6), result)
        }
    }

    @Test
    fun singleRangeWithPrefixAndSuffix() {
        parseAPLExpression("10 4 3 … 12 200").let { result ->
            assert1DArray(arrayOf(10, 4, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 200), result)
        }
    }

    @Test
    fun singleDecreasingRangeWithPrefixAndSuffix() {
        parseAPLExpression("10 4 10 … 8 200").let { result ->
            assert1DArray(arrayOf(10, 4, 10, 9, 8, 200), result)
        }
    }

    @Test
    fun singleElementRange() {
        parseAPLExpression("4 … 4").let { result ->
            assert1DArray(arrayOf(4), result)
        }
    }

    @Test
    fun rangeWithHigherDimensionsLeftArgumentShouldFail() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("(3 5 ⍴ 1+⍳15) … 1000")
        }
    }

    @Test
    fun rangeWithHigherDimensionsRightArgumentShouldFail() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("0 … (3 5 ⍴ 1+⍳15)")
        }
    }

    @Test
    fun rangeWithComplexLeftArgShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("1j2 … 10")
        }
    }

    @Test
    fun rangeWithComplexRightArgShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("10 … 40j50")
        }
    }

    @Test
    fun mixIntWithCharShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("10 … @e")
        }
    }

    @Test
    fun mixCharWithIntShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("@e … 10")
        }
    }

    @Test
    fun rangeWithChars() {
        assertString("abcd", parseAPLExpression("@a … @d"))
    }

    @Test
    fun rangeWithCharsReverse() {
        assertString("dcba", parseAPLExpression("@d … @a"))
    }

    @Test
    fun rangeWithSingleChar() {
        assertString("z", parseAPLExpression("@z … @z"))
    }

    @Test
    fun rangeBigint() {
        parseAPLExpression("100000000000000000000 … 100000000000000000005").let { result ->
            assert1DArray(
                arrayOf(
                    InnerBigIntOrLong("100000000000000000000"),
                    InnerBigIntOrLong("100000000000000000001"),
                    InnerBigIntOrLong("100000000000000000002"),
                    InnerBigIntOrLong("100000000000000000003"),
                    InnerBigIntOrLong("100000000000000000004"),
                    InnerBigIntOrLong("100000000000000000005")),
                result)
        }
    }

    @Test
    fun rangeBigintReverse() {
        parseAPLExpression("100000000000000000005 … 100000000000000000000").let { result ->
            assert1DArray(
                arrayOf(
                    InnerBigIntOrLong("100000000000000000005"),
                    InnerBigIntOrLong("100000000000000000004"),
                    InnerBigIntOrLong("100000000000000000003"),
                    InnerBigIntOrLong("100000000000000000002"),
                    InnerBigIntOrLong("100000000000000000001"),
                    InnerBigIntOrLong("100000000000000000000")),
                result)
        }
    }

    @Test
    fun rangeCrossesLongLimitIncreasing() {
        parseAPLExpression("(¯2+2*63) … (5+2*63)").let { result ->
            val start = BigInt.of(2).pow(63) - 2
            val end = BigInt.of(2).pow(63) + 5
            val n = (end - start + 1).toInt()
            assertDimension(dimensionsOfSize(n), result)
            repeat(n) { i ->
                assertBigIntOrLong(start + i, result.valueAt(i))
            }
        }
    }

    @Test
    fun rangeCrossesLongLimitDecreasing() {
        parseAPLExpression("(2000+2*63) … (¯20000+2*63)").let { result ->
            val start = BigInt.of(2).pow(63) + 2000
            val end = BigInt.of(2).pow(63) - 20000
            val n = (start - end + 1).toInt()
            assertDimension(dimensionsOfSize(n), result)
            repeat(n) { i ->
                assertBigIntOrLong(start - i, result.valueAt(i))
            }
        }
    }
}
