package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.chart.computeDatasetsFromAPLValue
import com.dhsdevelopments.kap.chart.computeScatterDatasetFromAPLValue
import com.dhsdevelopments.kap.chart.computeSurfaceDatasetFromAPLValue

class JsChartModule(val sendMessageFn: (dynamic) -> Unit) : KapModule {
    override val name: String get() = "jschart"

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("chart")
        engine.registerFunction(ns.internAndExport("line"), LineChartFunction())
        engine.registerFunction(ns.internAndExport("bar"), BarChartFunction())
        engine.registerFunction(ns.internAndExport("doughnut"), DoughnutChartFunction())
        engine.registerFunction(ns.internAndExport("pie"), PieChartFunction())
        engine.registerFunction(ns.internAndExport("scatter"), ScatterChartFunction())
        engine.registerFunction(ns.internAndExport("surface"), SurfaceChartFunction())
        engine.registerFunction(ns.internAndExport("plot"), JsPlotFunction())
    }
}

abstract class GenericLineChartFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
    override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
        val (horizontalAxisLabels, datasets) = computeDatasetsFromAPLValue(a, pos)
        val output = jsObject2(
            "type" to typeName(),
            "horizontalAxisLabels" to horizontalAxisLabels,
            "data" to datasets.map { d ->
                jsObject2(
                    "name" to d.name,
                    "data" to d.data)
            }.toTypedArray())
        val module = context.engine.findModule<JsChartModule>() ?: throw IllegalStateException("Chart module not found")
        module.sendMessageFn(jsObject2("messageType" to ADDITIONAL_OUTPUT_CHART, "chartdata" to output))
        return APLNullValue
    }

    abstract fun typeName(): String
}

class LineChartFunction : APLFunctionDescriptor {
    class LineChartFunctionImpl(pos: FunctionInstantiation) : GenericLineChartFunctionImpl(pos) {
        override fun typeName() = CHART_TYPE_LINE
    }

    override fun make(instantiation: FunctionInstantiation) = LineChartFunctionImpl(instantiation)
}

class BarChartFunction : APLFunctionDescriptor {
    class BarChartFunctionImpl(pos: FunctionInstantiation) : GenericLineChartFunctionImpl(pos) {
        override fun typeName() = CHART_TYPE_BAR
    }

    override fun make(instantiation: FunctionInstantiation) = BarChartFunctionImpl(instantiation)
}

class PieChartFunction : APLFunctionDescriptor {
    class PieChartFunctionImpl(pos: FunctionInstantiation) : GenericLineChartFunctionImpl(pos) {
        override fun typeName() = CHART_TYPE_PIE
    }

    override fun make(instantiation: FunctionInstantiation) = PieChartFunctionImpl(instantiation)
}

class DoughnutChartFunction : APLFunctionDescriptor {
    class DoughnutChartFunctionImpl(pos: FunctionInstantiation) : GenericLineChartFunctionImpl(pos) {
        override fun typeName() = CHART_TYPE_DOUGHNUT
    }

    override fun make(instantiation: FunctionInstantiation) = DoughnutChartFunctionImpl(instantiation)
}

class ScatterChartFunction : APLFunctionDescriptor {
    class ScatterChartFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val series = computeScatterDatasetFromAPLValue(a, pos)
            val output = jsObject2(
                "type" to CHART_TYPE_SCATTER,
                "data" to series.map { d ->
                    jsObject2(
                        "name" to d.name,
                        "points" to d.points.map { point ->
                            jsObject2(
                                "x" to point.x,
                                "y" to point.y)
                        }.toTypedArray())
                }.toTypedArray())
            val module = context.engine.findModule<JsChartModule>() ?: throw IllegalStateException("Chart module not found")
            module.sendMessageFn(jsObject2("messageType" to ADDITIONAL_OUTPUT_CHART, "chartdata" to output))
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ScatterChartFunctionImpl(instantiation)
}

class SurfaceChartFunction : APLFunctionDescriptor {
    class SurfaceChartFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val series = computeSurfaceDatasetFromAPLValue(a, pos)
            val output = jsObject2(
                "type" to CHART_TYPE_SURFACE,
                "width" to series.width,
                "height" to series.height,
                "data" to series.data)
            val module = context.engine.findModule<JsChartModule>() ?: throw IllegalStateException("Chart module not found")
            module.sendMessageFn(jsObject2("messageType" to ADDITIONAL_OUTPUT_CHART, "chartdata" to output))
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = SurfaceChartFunctionImpl(instantiation)
}
