package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.jvmmod.JvmModule
import com.dhsdevelopments.kap.net.NetModule
import java.util.*
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

actual fun platformInit(engine: Engine) {
    engine.systemParameters[engine.standardSymbols.platform] = ConstantSymbolSystemParameterProvider(engine.internSymbol("jvm", engine.coreNamespace))

    initJvmInfoCommand(engine)

    engine.addModule(MetaModule())
    engine.addModule(NetModule())
    engine.addModule(JvmModule())
    engine.addModule(JvmAudioModule())

    ServiceLoader.load(KapModule::class.java).let { loader ->
        loader.forEach { module ->
            engine.addModule(module)
        }
    }
}

private val localStackScope: ScopedValue<StorageStack> = ScopedValue.newInstance()

@Suppress("LEAKED_IN_PLACE_LAMBDA", "WRONG_INVOCATION_KIND")
@OptIn(ExperimentalContracts::class)
actual fun <T> withThreadLocalStorageStackAssigned(stack: StorageStack?, fn: () -> T): T {
    contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
    var result: T? = null
    ScopedValue.callWhere(localStackScope, stack) {
        result = fn()
    }
    return result!!
}

actual fun currentStorageStack(): StorageStack {
    return localStackScope.get()
}

actual fun currentStorageStackOrNull(): StorageStack? {
    return localStackScope.orElse(null)
}
