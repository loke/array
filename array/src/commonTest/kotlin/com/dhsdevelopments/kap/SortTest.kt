package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.complex.Complex
import kotlin.test.*

class SortTest : APLTest() {
    @Test
    fun numberSort() {
        sortTest("10 30 20 5 11 21 3 1 12 2", arrayOf(7, 9, 6, 3, 0, 4, 8, 2, 5, 1))
    }

    @Test
    fun numberSortWithDuplicates() {
        sortTest(
            "3 2 4 2 3 6 7 ¯3 ¯1 0 ¯1 3 3 1 1 1",
            arrayOf(7, 8, 10, 9, 13, 14, 15, 1, 3, 0, 4, 11, 12, 2, 5, 6),
            arrayOf(6, 5, 2, 0, 4, 11, 12, 1, 3, 13, 14, 15, 9, 8, 10, 7))
    }

    @Test
    fun sortStrings() {
        sortTest(
            "\"foo\" \"bar\" \"test\" \"abc\" \"xyz\" \"some\" \"strings\" \"withlongtext\" \"b\"",
            arrayOf(3, 8, 1, 0, 5, 6, 2, 7, 4))
    }

    @Test
    fun sortStringsWithDuplicates() {
        sortTest(
            "\"foo\" \"bar\" \"abc\" \"bar\" \"test\" \"foo\" \"foo\"",
            arrayOf(2, 1, 3, 0, 5, 6, 4),
            arrayOf(4, 0, 5, 6, 1, 3, 2))
    }

    @Test
    fun sortMultiDimensional() {
        sortTest(
            "3 4 ⍴ 8 5 1 7 0 11 6 2 4 3 10 9",
            arrayOf(1, 2, 0))
    }

    @Test
    fun sortMixedTypes() {
        sortTest("1.2 2 0.1 ¯9 ¯9.9 4 7.1 8.3", arrayOf(4, 3, 2, 0, 1, 5, 6, 7))
    }

    @Test
    fun sortDouble() {
        sortTest(
            "1.2 0.1 ¯9.9 0.0 ¯0.0 0.0 ¯0.0 0.000001",
            arrayOf(2, 4, 6, 3, 5, 7, 1, 0),
            arrayOf(0, 1, 7, 3, 5, 4, 6, 2))
    }

    @Test
    fun sortMixedDoubleAndInt() {
        sortTest(
            "1.2 1 1.0 0.0 ¯0.0 0 ¯0.00001",
            arrayOf(6, 4, 3, 5, 2, 1, 0),
            arrayOf(0, 1, 2, 5, 3, 4, 6))
    }

    @Test
    fun sortIntegerWithOverflow() {
        sortTest("0x6000000000000000 + 0x1000 0x7000000000000000 0 ¯3 9 1000", arrayOf(3, 2, 4, 5, 0, 1))
    }

    @Test
    fun sortSingleElement() {
        sortTest(",1", arrayOf(0))
    }

    @Test
    fun sortingScalarsShouldFail() {
        assertFailsWith<APLEvalException> {
            parseAPLExpression("⍋1")
        }
        assertFailsWith<APLEvalException> {
            parseAPLExpression("⍒1")
        }
    }

    @Test
    fun sortingArraysOfDifferentDimensions() {
        sortTest(
            "(⊂4 3 ⍴ 1 2 3 4 5 6) (⊂3 4 ⍴ 1 2 3 4 5 6) (⊂3 2 ⍴ 1 2 3 4) (⊂2 5 ⍴ 1 2) (⊂2 5 3 ⍴ 1 2) (⊂4 3 ⍴ 1 2 2 4 5 6)",
            arrayOf(3, 2, 1, 5, 0, 4))
    }

    @Test
    fun compareNumbersAndChars() {
        assert1DArray(arrayOf(1, 2, 0, 3), parseAPLExpression("⍋ @a 1 2 @b"))
        assert1DArray(arrayOf(3, 0, 2, 1), parseAPLExpression("⍒ @a 1 2 @b"))
    }

    @Test
    fun compareLists() {
        assert1DArray(arrayOf(0, 1), parseAPLExpression("⍋ (1;2) (2;1)"))
        assert1DArray(arrayOf(1, 0), parseAPLExpression("⍒ (1;2) (2;1)"))
    }

    @Test
    fun compareComplexShouldFail() {
        assert1DArray(arrayOf(0, 1), parseAPLExpression("⍋ 1J2 2J3"))
        assert1DArray(arrayOf(1, 0), parseAPLExpression("⍒ 1J2 2J3"))
    }

    @Test
    fun mixStringsAndSymbolsShouldFail() {
        parseAPLExpression("⍋ \"foo\" \"bar\" 'somename").let { result ->
            assert1DArray(arrayOf(2, 1, 0), result)
        }
        parseAPLExpression("⍒ \"foo\" \"bar\" 'somename").let { result ->
            assert1DArray(arrayOf(0, 1, 2), result)
        }
    }

    @Test
    fun symbolsAndNumber() {
        assert1DArray(arrayOf(0, 1, 2, 3), parseAPLExpression("⍋ 1 2 3 'somename"))
        assert1DArray(arrayOf(3, 2, 1, 0), parseAPLExpression("⍒ 1 2 3 'somename"))
    }

    @Test
    fun numbersAndComplex() {
        assert1DArray(arrayOf(0, 1, 2, 3), parseAPLExpression("⍋ 1 2 3 1J2"))
        assert1DArray(arrayOf(3, 2, 1, 0), parseAPLExpression("⍒ 1 2 3 1J2"))
    }

    @Test
    fun sortSymbols() {
        sortTest("'foo 'bar 'a 'test 'abclongerstring", arrayOf(2, 4, 1, 0, 3))
    }

    @Test
    fun tokenSymbolComparison() {
        val engine = makeEngine()
        val sym = APLSymbol(engine.internSymbol("foo"))
        val str = APLString.make("bar")
        assertEquals(-1, sym.compareTotalOrdering(str))
        assertEquals(1, str.compareTotalOrdering(sym))
    }

    @Test
    fun numberSymbolComparison() {
        val engine = makeEngine()
        val num = 1.makeAPLNumber()
        val sym = APLSymbol(engine.internSymbol("foo"))
        assertEquals(-1, num.compareTotalOrdering(sym))
        assertEquals(1, sym.compareTotalOrdering(num))
    }

    @Test
    fun numberComplexComparison() {
        val num = 1.makeAPLNumber()
        val complex = Complex(2.0, 3.0).makeAPLNumber()
        assertEquals(-1, num.compareTotalOrdering(complex))
        assertEquals(1, complex.compareTotalOrdering(num))
    }

    @Test
    fun listComparison() {
        val list1 = APLList(listOf(1.makeAPLNumber(), 2.makeAPLNumber()))
        val list2 = APLList(listOf(2.makeAPLNumber(), 4.makeAPLNumber()))
        assertEquals(-1, list1.compareTotalOrdering(list2))
        assertEquals(1, list2.compareTotalOrdering(list1))
    }

    @Test
    fun numberCharComparison() {
        val char1 = APLChar('a'.code)
        val num1 = 1.makeAPLNumber()
        assertEquals(1, char1.compareTotalOrdering(num1))
        assertEquals(-1, num1.compareTotalOrdering(char1))
    }

    @Test
    fun symbolCharComparison() {
        val engine = makeEngine()
        val sym = APLSymbol(engine.internSymbol("foo"))
        val ch = APLChar('a'.code)
        assertEquals(1, sym.compareTotalOrdering(ch))
        assertEquals(-1, ch.compareTotalOrdering(sym))
    }

    private fun sortTest(content: String, expected: Array<Int>, reverseExpected: Array<Int>? = null) {
        fun sortTestInner(s: String, exp: Array<Int>) {
            parseAPLExpression(s).let { result ->
                assertDimension(dimensionsOfSize(exp.size), result)
                assertArrayContent(exp, result)
            }
        }

        sortTestInner("⍋ ${content}", expected)
        sortTestInner("⍒ ${content}", reverseExpected ?: expected.reversedArray())
    }

    @Test
    fun sortSimpleArray() {
        parseAPLExpression("∧ 4 2 ¯3 4 1 3 0").let { result ->
            assert1DArray(arrayOf(-3, 0, 1, 2, 3, 4, 4), result)
        }
        parseAPLExpression("∨ 4 2 ¯3 4 1 3 0").let { result ->
            assert1DArray(arrayOf(-3, 0, 1, 2, 3, 4, 4).reversedArray(), result)
        }
    }

    @Test
    fun sortEmptyArray() {
        assertAPLNull(parseAPLExpression("∧⍬"))
        assertAPLNull(parseAPLExpression("∨⍬"))
    }

    @Test
    fun sort2DArray() {
        parseAPLExpression("∧ 3 4 ⍴ 1 11 9 5 10 2 8 0 4 3 6 7").let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            assertArrayContent(arrayOf(1, 11, 9, 5, 4, 3, 6, 7, 10, 2, 8, 0), result)
        }
        parseAPLExpression("∨ 3 4 ⍴ 1 11 9 5 10 2 8 0 4 3 6 7").let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            assertArrayContent(arrayOf(10, 2, 8, 0, 4, 3, 6, 7, 1, 11, 9, 5), result)
        }
    }

    @Test
    fun sort3DArray() {
        parseAPLExpression("∧ 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(3, 6, 2, 11, 0, 1, 7, 5, 4, 9, 10, 8), result)
        }
        parseAPLExpression("∨ 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(7, 5, 4, 9, 10, 8, 3, 6, 2, 11, 0, 1), result)
        }
    }

    @Test
    fun sort3DArrayWithRank0() {
        parseAPLExpression("∧[0] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(3, 6, 2, 11, 0, 1, 7, 5, 4, 9, 10, 8), result)
        }
        parseAPLExpression("∨[0] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(7, 5, 4, 9, 10, 8, 3, 6, 2, 11, 0, 1), result)
        }
    }

    @Test
    fun sort3DArrayWithRank2() {
        parseAPLExpression("∧[2] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11), result)
        }
        parseAPLExpression("∨[2] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11).reversedArray(), result)
        }
    }

    @Test
    fun sortWithInvalidRankSpecifier() {
        assertFailsWith<IllegalAxisException> {
            parseAPLExpression("∧[3] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1")
        }
        assertFailsWith<IllegalAxisException> {
            parseAPLExpression("∧[¯3] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1")
        }
        assertFailsWith<IllegalAxisException> {
            parseAPLExpression("∨[3] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1")
        }
        assertFailsWith<IllegalAxisException> {
            parseAPLExpression("∨[¯3] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1")
        }
    }

    @Test
    fun sortMajorAxis() {
        parseAPLExpression("∧7 2 ⍴ 1 2 2 1 1 1 1 2 1 4 1 1 1 4").let { result ->
            assertDimension(dimensionsOfSize(7, 2), result)
            assertArrayContent(arrayOf(1, 1, 1, 1, 1, 2, 1, 2, 1, 4, 1, 4, 2, 1), result)
        }
    }

    @Test
    fun sortSingleDimensionArrayPreservesLabels() {
        parseAPLExpression("∧ \"a\" \"b\" \"c\" \"d\" labels 0 2 1 4").let { result ->
            assert1DArray(arrayOf(0, 1, 2, 4), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertLabelList(listOf("a", "c", "b", "d"), labels.labels[0])
        }
        parseAPLExpression("∨ \"a\" \"b\" \"c\" \"d\" labels 0 2 1 4").let { result ->
            assert1DArray(arrayOf(4, 2, 1, 0), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertLabelList(listOf("d", "b", "c", "a"), labels.labels[0])
        }
    }

    @Test
    fun sort3DArrayPreservesLabels() {
        parseAPLExpression("∧ \"f\" \"g\" labels[0] \"d\" \"e\" labels[1] \"a\" \"b\" \"c\" labels[2] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(3, 6, 2, 11, 0, 1, 7, 5, 4, 9, 10, 8), result)
            assertLabels(
                listOf(
                    listOf("g", "f"),
                    listOf("d", "e"),
                    listOf("a", "b", "c")),
                result)
        }
        parseAPLExpression("∨ \"f\" \"g\" labels[0] \"d\" \"e\" labels[1] \"a\" \"b\" \"c\" labels[2] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(7, 5, 4, 9, 10, 8, 3, 6, 2, 11, 0, 1), result)
            assertLabels(
                listOf(
                    listOf("f", "g"),
                    listOf("d", "e"),
                    listOf("a", "b", "c")),
                result)
        }
    }

    @Test
    fun sort3DArrayPreservesLabelsAndUnlabelledAxes() {
        parseAPLExpression("∧ \"f\" \"g\" labels[0] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(3, 6, 2, 11, 0, 1, 7, 5, 4, 9, 10, 8), result)
            assertLabels(
                listOf(
                    listOf("g", "f"),
                    null,
                    null),
                result)
        }
        parseAPLExpression("∨ \"f\" \"g\" labels[0] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(7, 5, 4, 9, 10, 8, 3, 6, 2, 11, 0, 1), result)
            assertLabels(
                listOf(
                    listOf("f", "g"),
                    null,
                    null),
                result)
        }
    }

    @Test
    fun sort3DArrayWithAxisClearsHigherAxisLabels0() {
        parseAPLExpression("∧[1] \"f\" \"g\" labels[0] \"d\" \"e\" labels[1] \"a\" \"b\" \"c\" labels[2] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(3, 6, 2, 7, 5, 4, 9, 10, 8, 11, 0, 1), result)
            assertLabels(
                listOf(
                    null,
                    null,
                    listOf("a", "b", "c")),
                result)
        }
        parseAPLExpression("∨[1] \"f\" \"g\" labels[0] \"d\" \"e\" labels[1] \"a\" \"b\" \"c\" labels[2] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(11, 0, 1, 9, 10, 8, 7, 5, 4, 3, 6, 2), result)
            assertLabels(
                listOf(
                    null,
                    null,
                    listOf("a", "b", "c")),
                result)
        }
    }

    @Test
    fun sort3DArrayWithAxisClearsHigherAxisLabels1() {
        parseAPLExpression("∧[2] \"f\" \"g\" labels[0] \"d\" \"e\" labels[1] \"a\" \"b\" \"c\" labels[2] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11), result)
            assertNull(result.metadata.labels)
        }
        parseAPLExpression("∨[2] \"f\" \"g\" labels[0] \"d\" \"e\" labels[1] \"a\" \"b\" \"c\" labels[2] 2 2 3 ⍴ 7 5 4 9 10 8 3 6 2 11 0 1").let { result ->
            assertDimension(dimensionsOfSize(2, 2, 3), result)
            assertArrayContent(arrayOf(11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0), result)
            assertNull(result.metadata.labels)
        }
    }
}
