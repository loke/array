package com.dhsdevelopments.kap.msofficereader

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.TagCatch
import com.dhsdevelopments.kap.builtins.ThrowableTag
import com.dhsdevelopments.kap.builtins.makeAPLNumberAsBoolean
import com.dhsdevelopments.kap.dates.APLTimestamp
import org.apache.poi.ss.usermodel.*
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.InputStream
import java.util.*

class ExcelFileWrapper(private val workbook: Workbook) : NativeCloseable {
    val sheetCount: Int get() = workbook.numberOfSheets

    fun sheetName(index: Int): String {
        return workbook.getSheetAt(index).sheetName
    }

    override fun close() {
        workbook.close()
    }

    fun parseSheet(sheetIndex: Int): APLValue {
        val evaluator = workbook.creationHelper.createFormulaEvaluator()
        val sheet = workbook.getSheetAt(sheetIndex)
        return readSheet(sheet, evaluator)
    }
}

fun loadExcelFileWrapper(name: String): ExcelFileWrapper {
    return ExcelFileWrapper(WorkbookFactory.create(File(name), null, true))
}

fun loadExcelFileWrapper(inStream: InputStream): ExcelFileWrapper {
    return ExcelFileWrapper(WorkbookFactory.create(inStream, null))
}

fun readExcelFile(name: String): APLValue {
    WorkbookFactory.create(File(name), null, true).use { workbook ->
        val evaluator = workbook.creationHelper.createFormulaEvaluator()
        val sheet = workbook.getSheetAt(0)
        return readSheet(sheet, evaluator)
    }
}

fun readSheet(sheet: Sheet, evaluator: FormulaEvaluator): APLValue {
    if (sheet.physicalNumberOfRows == 0) {
        return APLNullValue
    }

    val lastRowIndex = sheet.lastRowNum
    val rows = ArrayList<List<APLValue>>()
    for (i in 0..lastRowIndex) {
        val row = sheet.getRow(i)
        val parsedRow = if (row == null) {
            listOf(APLNilValue)
        } else {
            readRow(row, evaluator)
        }
        rows.add(parsedRow)
    }

    val width = rows.maxValueBy { it.size }
    return APLArrayImpl.make(dimensionsOfSize(rows.size, width)) { i ->
        val rowIndex = i / width
        val colIndex = i % width
        val row = rows[rowIndex]
        if (colIndex < row.size) {
            row[colIndex]
        } else {
            APLLONG_0
        }
    }
}

fun readRow(row: Row, evaluator: FormulaEvaluator): List<APLValue> {
    val cellList = ArrayList<APLValue>()
    val lastCellIndex = row.lastCellNum
    var numPendingNulls = 0
    for (i in 0 until lastCellIndex) {
        val cell = row.getCell(i)
        if (cell == null) {
            numPendingNulls++
        } else {
            repeat(numPendingNulls) {
                cellList.add(APLLONG_0)
            }
            numPendingNulls = 0
            cellList.add(cellToAPLValue(cell, evaluator))
        }
    }
    return cellList
}

fun cellToAPLValue(cell: Cell, evaluator: FormulaEvaluator): APLValue {
    return when (cell.cellType) {
        CellType.FORMULA -> parseEvaluatedCell(cell, evaluator)
        CellType.BOOLEAN -> cell.booleanCellValue.makeAPLNumberAsBoolean()
        CellType.BLANK -> APLNilValue
        CellType.NUMERIC -> APLDouble(cell.numericCellValue)
        CellType.STRING -> APLString.make(cell.stringCellValue)
        CellType.ERROR -> APLString.make("ERROR:${FormulaError.forInt(cell.errorCellValue).string}")
        else -> throw IllegalStateException("Unknown cell type: ${cell.cellType}")
    }
}

fun parseEvaluatedCell(cell: Cell, evaluator: FormulaEvaluator): APLValue {
    val v = evaluator.evaluate(cell)
    return when (cell.cellType) {
        CellType.FORMULA -> APLString.make("FORMULA:${v.stringValue}")
        CellType.BOOLEAN -> (if (v.booleanValue) 1 else 0).makeAPLNumber()
        CellType.BLANK -> APLNilValue
        CellType.NUMERIC -> v.numberValue.makeAPLNumber()
        CellType.STRING -> APLString.make(v.stringValue)
        CellType.ERROR -> APLString.make("ERROR:${FormulaError.forInt(cell.errorCellValue).string}")
        else -> throw IllegalStateException("Unknown cell type: ${v.cellType}")
    }
}

fun saveExcelFile(values: List<Pair<String?, APLValue>>, fileName: String) {
    require(values.isNotEmpty())

    val workbook: Workbook = XSSFWorkbook()
    values.forEachIndexed { i, (name, value) ->
        val value = value.arrayify()
        val dimensions = value.dimensions
        val width = when (dimensions.size) {
            1 -> 1
            2 -> dimensions[1]
            else -> throw IllegalArgumentException("Array must have a rank between 0 and 2. Element ${i} dimensions: ${dimensions}")
        }
        val sheet = if (name == null) workbook.createSheet() else workbook.createSheet(name)
        var rowIndex = 0
        var colIndex = 0
        var row: Row? = null
        value.iterateMembers { v ->
            if (colIndex == 0) {
                row = sheet.createRow(rowIndex)
            }
            val cell = row!!.createCell(colIndex)
            fillInCell(cell, v)
            if (++colIndex >= width) {
                rowIndex++
                colIndex = 0
            }
        }
    }
    FileOutputStream(fileName).use { s ->
        workbook.write(s)
    }
}

fun fillInCell(cell: Cell, v: APLValue) {
    when {
        v is APLNumber -> if (v.isComplex()) cell.setCellValue(v.formatted(FormatStyle.PLAIN)) else cell.setCellValue(v.asDouble())
        v is APLChar -> cell.setCellValue(v.asString())
        v is APLNilValue -> cell.setBlank()
        v is APLTimestamp -> cell.setCellValue(Date(v.time.toEpochMilliseconds()))
        v.isStringValue() -> cell.setCellValue(v.toStringValue())
        else -> cell.setCellValue("error")
    }
}

class LoadExcelFileFunction : APLFunctionDescriptor {
    class LoadExcelFileFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val filename = a.toStringValue(pos)
            try {
                return readExcelFile(filename)
            } catch (_: FileNotFoundException) {
                throwAPLException(
                    TagCatch(
                        ThrowableTag(
                            APLSymbol(context.engine.internSymbol("fileNotFound", context.engine.keywordNamespace)),
                            APLString.make(filename)),
                        "File not found: ${filename}",
                        pos))
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = LoadExcelFileFunctionImpl(instantiation)
}

class SaveExcelFileFunction : APLFunctionDescriptor {
    class SaveExcelFileFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val fileName = a.toStringValue(pos)
            saveExcelFile(listOf(Pair(null, b)), fileName)
            return b
        }
    }

    override fun make(instantiation: FunctionInstantiation) = SaveExcelFileFunctionImpl(instantiation)
}


class MsOfficeModule : KapModule {
    override val name get() = "msoffice"

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("msoffice")
        engine.registerFunction(ns.internAndExport("read"), LoadExcelFileFunction())
        engine.registerFunction(ns.internAndExport("write"), SaveExcelFileFunction())
    }
}
