package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.edit.EditorEngineConnector
import com.dhsdevelopments.kap.jsObject2
import org.w3c.dom.Worker

class JsEngineConnector(val worker: Worker) : EditorEngineConnector {
    override fun findParenCandidates(src: String, index: Int, callback: (List<Int>) -> Unit) {
        println("finding candidates at index=${index}")
        val data = makeParenCandidatesRequestData(src, index)
        val req = makeEngineRequest(EngineRequestType.WRAP_WITH_PAREN, data)
        sendEngineRequest(worker, req) { res ->
            val a = res.candidatesList
            val list = ArrayList<Int>()
            repeat(a.length) { i ->
                list.add(a[i])
            }
            println("got paren list: ${list}")
            callback(list)
        }
    }
}

private var currentId = 0
private fun makeRequestId() = "engine-${currentId++}"

fun makeEngineRequest(name: String, data: Any?): dynamic {
    return jsObject2(
        "messageType" to WorkerRequestType.ENGINE_REQUEST,
        "name" to name,
        "requestId" to makeRequestId(),
        "data" to data)
}

private fun makeParenCandidatesRequestData(src: String, index: Int): dynamic {
    return jsObject2(
        "src" to src,
        "index" to index)
}
