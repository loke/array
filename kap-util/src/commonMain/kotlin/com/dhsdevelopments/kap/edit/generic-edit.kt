package com.dhsdevelopments.kap.edit

import com.dhsdevelopments.kap.edit.EditableText.StateId

interface EditableText {
    fun text(start: Int = 0, end: Int? = null): String
    fun cursorPosition(): Int
    fun insert(text: String, index: Int)
    fun remove(numChars: Int, index: Int)
    fun commit()

    fun markState(): StateId
    fun stateChangedSinceLastMark(oldState: StateId): Boolean

    interface StateId
}

abstract class SimpleTextTrackingEditableText : EditableText {
    private var index = 0L

    override fun markState(): StateId {
        return SimpleTextStateId(cursorPosition(), text(), index)
    }

    override fun stateChangedSinceLastMark(oldState: StateId): Boolean {
        oldState as SimpleTextStateId
        return index != oldState.index || cursorPosition() != oldState.prevCursorPosition || text() != oldState.prevContent
    }

    fun markEdited() {
        index++
    }

    private class SimpleTextStateId(val prevCursorPosition: Int, val prevContent: String, val index: Long) : StateId
}

interface EditorAction {
    fun run(text: EditableText)
}

class WrapWithParen(val conn: EditorEngineConnector) : EditorAction {
    private var currentCandidates: List<Int>? = null
    private var candidateIndex = 0
    private var currentCursorIndex = -1
    private var prevParenIndex = -1
    private var state: StateId? = null

    override fun run(text: EditableText) {
        val s = state
        if (s == null || text.stateChangedSinceLastMark(s)) {
            currentCandidates = null
            candidateIndex = 0
            currentCursorIndex = -1
            prevParenIndex = -1
            state = null
        }
        if (currentCandidates == null) {
            val i = text.cursorPosition()
            currentCursorIndex = i
            conn.findParenCandidates(text.text(), i) { candidates ->
                require(i == text.cursorPosition())
                if (candidates.isNotEmpty()) {
                    currentCandidates = candidates
                    candidateIndex = 0
                    processCandidateList(text)
                }
            }
        } else {
            processCandidateList(text)
        }
    }

    private fun processCandidateList(text: EditableText) {
        val candidates = currentCandidates
        if (candidates != null && currentCursorIndex == text.cursorPosition()) {
            if (candidateIndex == 0) {
                text.insert(")", currentCursorIndex)
            } else {
                text.remove(1, prevParenIndex)
            }
            if (candidateIndex < candidates.size) {
                val newParenIndex = candidates[candidateIndex]
                candidateIndex++
                text.insert("(", newParenIndex)
                prevParenIndex = newParenIndex
            } else {
                // Subtract 2 to accommodate 1) We need to delete one char to the left of the cursor, and 2) the fact that we've removed the left paren already
                text.remove(1, currentCursorIndex - 2)
                candidateIndex = 0
            }
            text.commit()
            currentCursorIndex = text.cursorPosition()
            state = text.markState()
        }
    }
}
