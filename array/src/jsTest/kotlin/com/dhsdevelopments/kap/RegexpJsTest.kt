package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertTrue

class RegexpJsTest {
    @Test
    fun testWhitespace() {
        assertTrue(whitespaceRegexp.test(" "))
        assertTrue(whitespaceRegexp.test("\t"))
    }
}
