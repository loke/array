@file:Suppress("EXPECT_ACTUAL_CLASSIFIERS_ARE_IN_BETA_WARNING")

package com.dhsdevelopments.kap.dates

import com.dhsdevelopments.kap.*
import kotlinx.datetime.Instant

class APLTimestamp(val time: Instant) : APLSingleValue() {
    override val kapClass get() = SystemClass.TIMESTAMP

    override fun formatted(style: FormatStyle): String {
        return time.toString()
    }

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Boolean {
        return reference is APLTimestamp && time == reference.time
    }

    override fun numericCompare(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Int {
        return if (reference is APLTimestamp) {
            time.compareTo(reference.time)
        } else {
            super.numericCompare(reference, pos, typeDiscrimination)
        }
    }

    override fun compareSameType(reference: APLValue, pos: Position?): Int {
        return numericCompare(reference, pos, typeDiscrimination = true)
    }

    override fun typeQualifiedHashCode(): Int {
        return time.hashCode()
    }
}
