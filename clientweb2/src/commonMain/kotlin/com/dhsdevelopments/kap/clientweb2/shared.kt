package com.dhsdevelopments.kap.clientweb2

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

///////////////////////////////////////////////
// Worker communication structures
///////////////////////////////////////////////

@Serializable
enum class ResultType {
    FORMATTED_PRETTY,
    FORMATTED_READABLE,
    JS
}

@Serializable
sealed class Request

@Serializable
@SerialName("evalrequest")
data class EvalRequest(var requestId: String, val src: String, val resultType: ResultType) : Request()

@Serializable
@SerialName("importrequest")
data class ImportRequest(val varname: String, val data: JsKapValue) : Request()

@Serializable
@SerialName("importcsv")
data class ImportCsvRequest(val varname: String, val data: String) : Request()

@Serializable
@SerialName("tokeniserequest")
data class TokeniseRequest(val requestId: String, val src: String) : Request()

@Serializable
@SerialName("completionrequest")
data class CompletionRequest(val requestId: String, val src: String, val index: Int) : Request()

const val TOKENISE_RESULT_TYPE = "tokens"
const val HTTP_REQUEST_TYPE = "httpRequest"
const val READ_LINE_TYPE = "readLine"
const val COMPLETION_RESULT_TYPE = "completions"
const val ENGINE_RESPONSE = "engineResponse"

object WorkerRequestType {
    const val ENGINE_REQUEST = "engine-request"
}

object EngineRequestType {
    const val COMPLETIONS = "completions"
    const val WRAP_WITH_PAREN = "wrap-paren"
    const val COMMAND = "command"
}

object TokenTypes {
    const val SYMBOL = 0
    const val NUMBER = 1
    const val STRING = 2
    const val COMMENT = 3
    const val CHAR = 4
    const val NUM_TYPES = 5
}

@Serializable
data class PosDescriptor(val line: Int, val col: Int, val endLine: Int, val endCol: Int, val callerName: String?)

@Serializable
sealed class ResponseMessage

@Serializable
@SerialName("exception")
data class ExceptionDescriptor(val message: String) : ResponseMessage()

@Serializable
@SerialName("importresult")
data class ImportResult(val message: String) : ResponseMessage()

@Serializable
@SerialName("evalexception")
data class EvalExceptionDescriptor(
    val requestId: String,
    val message: String,
    val pos: PosDescriptor?,
    val inputPos: PosDescriptor?,
    val details: String?
) : ResponseMessage()

@Serializable
sealed class EvalResponse : ResponseMessage()

@Serializable
@SerialName("stringresponse")
data class StringResponse(val result: String, val time: Long) : EvalResponse()

const val DATA_RESPONSE_TYPE = "dataresponse"

@Serializable
@SerialName("dataresponse")
data class DataResponse(val result: JsKapValue, val time: Long) : EvalResponse()

const val OUTPUT_DESCRIPTOR_TYPE = "output"

@Serializable
@SerialName("output")
data class OutputDescriptor(val text: String) : ResponseMessage()

const val ENGINE_STARTED_TYPE = "avail"

@Serializable
@SerialName("avail")
data class EngineStartedDescriptor(val text: String) : ResponseMessage()


@Serializable
@SerialName("additionaloutput")
data class AdditionalOutput(val content: AdditionalOutputData) : ResponseMessage()

@Serializable
sealed class AdditionalOutputData

///////////////////////////////////////////////
// Request types
///////////////////////////////////////////////

//abstract external class WorkerResponse {
//    var messageType: String
//}
//
//external class EngineResponse : WorkerResponse {
//    var requestId: String
//    var name: String
//    var data: Any?
//}

//abstract external class WorkerRequest {
//    var messageType: String
//}
//
//external class EngineRequest : WorkerRequest {
//    var name: String
//    var requestId: String
//    var data: Any?
//}

//external class ParenCandidatesRequestData {
//    var src: String
//    var index: Int
//}
//
//external class ParenCandidatesResultData {
//    var candidatesList: IntArray
//}

///////////////////////////////////////////////
// JSON representation of Kap datatypes
///////////////////////////////////////////////

@Serializable
sealed class JsKapValue

@Serializable
@SerialName("string")
data class JsKapString(val value: String) : JsKapValue()

@Serializable
sealed class JsKapNumber : JsKapValue()

@Serializable
@SerialName("integer")
data class JsKapInteger(val value: String) : JsKapNumber()

@Serializable
@SerialName("double")
data class JsKapDouble(val value: Double) : JsKapNumber()

@Serializable
@SerialName("char")
data class JsKapChar(val value: String) : JsKapValue()

@Serializable
@SerialName("array")
data class JsKapArray(val dimensions: List<Int>, val values: List<JsKapValue>) : JsKapValue()

@Serializable
@SerialName("list")
data class JsKapList(val values: List<JsKapValue>) : JsKapValue()

@Serializable
@SerialName("undefined")
data class JsKapUndefined(val value: String) : JsKapValue()
