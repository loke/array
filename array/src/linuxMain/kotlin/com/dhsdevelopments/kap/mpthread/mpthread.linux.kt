@file:OptIn(ExperimentalForeignApi::class, ExperimentalNativeApi::class)

package com.dhsdevelopments.kap.mpthread

import com.dhsdevelopments.kap.Either
import kotlinx.cinterop.*
import platform.posix.*
import kotlin.concurrent.AtomicReference
import kotlin.concurrent.Volatile
import kotlin.experimental.ExperimentalNativeApi
import kotlin.native.ref.createCleaner
import kotlin.reflect.KClass

private class ThreadArg(val thread: LinuxThread)

@OptIn(ExperimentalForeignApi::class)
private class LinuxThread(val fn: (MPThread) -> Any?) : MPThread {
    var tidInt: pthread_t? = null
    val tid: pthread_t get() = tidInt ?: throw IllegalStateException("tid is not assigned")
    private var stopped = false

    @Volatile
    private var completionData: Either<Any?, Throwable>? = null

    override fun isStopped(): Boolean {
        return stopped
    }

    override fun completionData(): Either<Any?, Throwable> {
        val d = completionData
        requireNotNull(d) { "Completion data is not available" }
        return d
    }

    override fun join() {
        pthread_join(tid, null)
    }

    fun assignTid(tidNew: pthread_t) {
        require(this.tidInt == null) { "Thread is already assigned" }
        tidInt = tidNew
    }

    fun executeThread(): COpaquePointer? {
        try {
            val res = fn(this)
            completionData = Either.Left(res)
        } catch (e: Exception) {
            completionData = Either.Right(e)
        } finally {
            stopped = true
        }
        return null
    }
}

@OptIn(ExperimentalForeignApi::class)
actual fun startThread(fn: (MPThread) -> Any?): MPThread {
    memScoped {
        val thread = LinuxThread(fn)
        val arg = ThreadArg(thread)
        val argRef = StableRef.create(arg)
        val tid = alloc<pthread_tVar>()
        pthread_create(tid.ptr, null, staticCFunction(::threadMain), argRef.asCPointer())
        thread.assignTid(tid.value)
        return thread
    }
}

@OptIn(ExperimentalForeignApi::class)
private fun threadMain(data: COpaquePointer?): COpaquePointer? {
    if (data == null) {
        throw IllegalStateException("Thread data is null")
    }
    val argRef = data.asStableRef<ThreadArg>()
    val arg = argRef.get()
    argRef.dispose()

    return arg.thread.executeThread()
}

actual val backendSupportsThreading: Boolean get() = true

class LinuxMPAtomicRefArray<T>(size: Int) : MPAtomicRefArray<T> {
    private val content = ArrayList<AtomicReference<T?>>(size)

    init {
        repeat(size) { content.add(AtomicReference(null)) }
    }

    override operator fun get(index: Int): T? = content[index].value

    override fun compareAndExchange(index: Int, expected: T?, newValue: T?): T? {
        val reference = content[index]
        return reference.compareAndExchange(expected, newValue)
    }
}

actual fun <T> makeAtomicRefArray(size: Int): MPAtomicRefArray<T> {
    return LinuxMPAtomicRefArray(size)
}

actual fun <T : Any> makeMPThreadLocalBackend(type: KClass<T>): MPThreadLocal<T> {
    // Use the single-threaded implementation as the native version doesn't support multi-threading yet
    return object : MPThreadLocal<T> {
        override var value: T? = null
    }
}

actual class MPLock actual constructor(recursive: Boolean) {
    val impl: CArrayPointer<pthread_mutex_t>

    init {
        memScoped {
            val attrs = allocArray<pthread_mutexattr_t>(1)
            pthread_mutexattr_init(attrs)
            if (recursive) {
                pthread_mutexattr_settype(attrs, PTHREAD_MUTEX_RECURSIVE.toInt())
            }
            val mutex = nativeHeap.allocArray<pthread_mutex_t>(1)
            pthread_mutex_init(mutex, attrs)
            pthread_mutexattr_destroy(attrs)
            impl = mutex
        }
    }

    actual fun makeCondVar(): MPCondVar = LinuxCondVar(this)

    @Suppress("unused")
    private val cleaner = createCleaner(impl) { obj ->
        pthread_mutex_destroy(obj)
        nativeHeap.free(obj)
    }
}

class LinuxCondVar(val lock: MPLock) : MPCondVar {
    val impl: CArrayPointer<pthread_cond_t>

    init {
        val condVar = nativeHeap.allocArray<pthread_cond_t>(1)
        pthread_cond_init(condVar, null)
        impl = condVar
    }

    @Suppress("unused")
    private val cleaner = createCleaner(impl) { obj ->
        pthread_cond_destroy(obj)
        nativeHeap.free(obj)
    }

    override fun waitUpdate() {
        pthread_cond_wait(impl, lock.impl)
    }

    override fun signal() {
        pthread_cond_signal(impl)
    }

    override fun signalAll() {
        pthread_cond_broadcast(impl)
    }
}

actual inline fun <T> MPLock.withLocked(fn: () -> T): T {
    pthread_mutex_lock(impl)
    try {
        return fn()
    } finally {
        pthread_mutex_unlock(impl)
    }
}
