package com.dhsdevelopments.kap.clientweb2.chart3d

import com.dhsdevelopments.kap.jsObject2
import kotlinx.browser.document
import org.w3c.dom.HTMLElement

fun main() {
    val doc: dynamic = document
    doc.chart3DDisplay = { element: HTMLElement, content: dynamic -> addChartToNode(element, content) }
}

@JsModule("plotly.js-dist")
@JsNonModule
external object Plotly {
    fun newPlot(element: HTMLElement, data: dynamic, layout: dynamic)
}

@Suppress("UnusedVariable")
private fun jsObject3(vararg fields: Pair<String, dynamic>): dynamic {
    val res = js("{}")
    fields.forEach { (key, value) ->
        js("res[key] = value")
    }
    return res
}

private fun addChartToNode(element: HTMLElement, content: dynamic) {
    val width = content.width as Int
    val height = content.height as Int
    val contentData = content.data as DoubleArray
    val rows = js("[]")
    repeat(height) { y ->
        val row = js("[]")
        repeat(width) { x ->
            row.push(contentData[y * width + x])
        }
        rows.push(row)
    }

    val data = arrayOf(
// This should use jsObject2 in kap-util, but doing so triggers a compiler bug
        jsObject3(
            "z" to rows,
            "type" to "surface"))

    val layout = jsObject2(
//        "title" to "Foo",
        "autosize" to true,
        "width" to 800,
        "height" to 500)

    Plotly.newPlot(element, data, layout)
}
