package com.dhsdevelopments.kap.clientweb2.http

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.clientweb2.HTTP_REQUEST_TYPE

class HttpManagerImpl : HttpManager {
    override fun httpRequest(engine: Engine, httpRequestData: HttpRequestData, pos: Position?): HttpResult {
        val jsTransferQueue = engine.jsNativeData().jsTransferQueue
            ?: throwAPLException(APLEvalException("Backend does not support synchronous http calls", pos))
        val message = makeHttpRequestMessage(httpRequestData)
        engine.jsNativeData().sendMessageFn(message)

        val newState = jsTransferQueue.waitForStateOrBreak(JsTransferQueue.STATE_HTTP_RESULT)
        if (newState == JsTransferQueue.STATE_BREAK) {
            engine.checkInterrupted(pos)
        }
        require(newState == JsTransferQueue.STATE_HTTP_RESULT) { "Illegal state value. Got: ${newState}" }
        val dec = jsTransferQueue.readArray()
        return HttpResultJs(200, dec.decodeToString(), emptyMap())
    }

    override fun httpRequestCallback(engine: Engine, httpRequestData: HttpRequestData, pos: Position?, callback: (HttpResult) -> Either<APLValue, Exception>) {
        TODO("not implemented")
    }

    private fun makeHttpRequestMessage(httpRequestData: HttpRequestData): dynamic {
        return jsObject2(
            "messageType" to HTTP_REQUEST_TYPE,
            "url" to httpRequestData.url,
            "method" to httpRequestData.method.name)
    }
}

class HttpResultJs(
    override val code: Long,
    override val content: String,
    override val headers: Map<String, List<String>>
) : HttpResult
