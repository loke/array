package com.dhsdevelopments.kap.csv

import com.dhsdevelopments.kap.*

fun writeAPLArrayAsCsv(dest: CharacterConsumer, value: APLValue, pos: Position? = null) {
    val dimensions = value.dimensions
    if (dimensions.size != 2) {
        throwAPLException(InvalidDimensionsException("Value must be a 2-dimensional array", pos))
    }

    val csvWriter = CsvWriter(dest)

    val width = dimensions[1]
    val currRow = ArrayList<String>()
    value.iterateMembers { v ->
        val v0 = v.unwrapDeferredValue()
        val s = v0.toStringValueOrNull()
        val s0 = if (s != null) {
            s
        } else {
            when {
                v0 is APLNumber -> {
                    when (val n = v0.ensureNumber(pos)) {
                        is APLLong -> n.value.toString()
                        is APLDouble -> n.value.toString()
                        is APLComplex -> if (n.isComplex()) "${n.value.re}J${n.value.im}" else n.asDouble(pos).toString()
                        is APLBigInt -> n.value.toString()
                        is APLRational -> n.value.toDouble().toString()
                        else -> "error"
                    }
                }
                else -> "unknown-type"
            }
        }
        currRow.add(s0)
        if (currRow.size >= width) {
            csvWriter.writeRow(currRow)
            currRow.clear()
        }
    }
    require(currRow.isEmpty())
}

class CsvWriter(val consumer: CharacterConsumer) {
    private var numColumns: Int? = null

    fun writeRow(values: List<String>) {
        val n = numColumns
        if (n == null) {
            numColumns = values.size
        } else if (n != values.size) {
            throw IllegalArgumentException("Attempt to add a row of ${values.size} cells. Table is expected to have ${n} columns.")
        }
        values.joinToString(",") { v -> "\"${escapeString(v)}\"" }.let(consumer::writeString)
        consumer.writeString("\n")
    }

    private fun escapeString(s: String): String {
        val buf = StringBuilder()
        s.forEach { ch ->
            when (ch) {
                '\"' -> buf.append("\"\"")
                else -> buf.append(ch)
            }
        }
        return buf.toString()
    }
}
