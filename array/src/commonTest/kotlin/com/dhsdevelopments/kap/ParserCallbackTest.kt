package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.ParseTreeResultCallbacks.Node
import com.dhsdevelopments.kap.ParserCallbacks.ParserLevel
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs
import kotlin.test.assertTrue

class ParserCallbackTest : APLTest() {
    @Test
    fun simpleCallbackTest() {
        val engine = makeEngine()
        val src = "1 + 2"
        val callbacks = ParseTreeResultCallbacks()
        engine.parse(StringSourceLocation(src), callbacks = listOf(callbacks))
        val subexpressions = callbacks.root
        assertEquals(1, subexpressions.size)
        val expr = subexpressions[0]
        assertIs<Node.Expression>(expr)
        assertEquals(0, expr.pos.col)
        assertEquals(5, expr.pos.computedEndCol)
        assertSubexpressionExists("1", src, subexpressions)
        assertSubexpressionExists("2", src, subexpressions)
        assertSubexpressionExists("1 + 2", src, subexpressions)
    }

    @Test
    fun singleNumberTest() {
        val engine = makeEngine()
        val src = "123"
        val callbacks = ParseTreeResultCallbacks()
        engine.parse(StringSourceLocation(src), callbacks = listOf(callbacks))
        val subexpressions = callbacks.root
        assertEquals(1, subexpressions.size)
        val expr = subexpressions[0]
        assertIs<Node.Expression>(expr)
        assertEquals(0, expr.pos.col)
        assertEquals(3, expr.pos.computedEndCol)
        assertSubexpressionExists("123", src, subexpressions)
    }

    @Test
    fun nestedNoParensCallbackTest() {
        val engine = makeEngine()
        val src = "1 + 2 + 3"
        val callbacks = ParseTreeResultCallbacks()
        engine.parse(StringSourceLocation(src), callbacks = listOf(callbacks))
        val subexpressions = callbacks.root
        assertEquals(1, subexpressions.size)
        val e0 = subexpressions[0]
        assertEquals(ParserLevel.LIST, e0.level)
        assertSubexpressionExists("2 + 3", src, subexpressions)
        assertSubexpressionExists("1 + 2 + 3", src, subexpressions)
    }

    @Test
    fun strandedList() {
        val engine = makeEngine()
        val src = "123 + 4567 11 22 33 + 987654"
        val callbacks = ParseTreeResultCallbacks()
        engine.parse(StringSourceLocation(src), callbacks = listOf(callbacks))
        val subexpressions = callbacks.root
        assertEquals(1, subexpressions.size)
        val e0 = subexpressions[0]
        assertEquals(ParserLevel.LIST, e0.level)
        assertSubexpressionExists("4567", src, subexpressions)
        assertSubexpressionExists("11", src, subexpressions)
        assertSubexpressionExists("22", src, subexpressions)
        assertSubexpressionExists("33", src, subexpressions)
        assertSubexpressionExists("987654", src, subexpressions)
//        assertSubexpressionExists("4567 11 22 33", src, subexpressions)
        assertSubexpressionExists("4567 11 22 33 + 987654", src, subexpressions)
//        printParseTree(src, subexpressions)
        assertSubexpressionExists("123 + 4567 11 22 33 + 987654", src, subexpressions)
    }

    @Test
    fun nestedWithParensCallbackTest() {
        val engine = makeEngine()
        val src = "(1 + 2) + (3 + 4)"
        val callbacks = ParseTreeResultCallbacks()
        engine.parse(StringSourceLocation(src), callbacks = listOf(callbacks))
        val subexpressions = callbacks.root
        assertEquals(1, subexpressions.size)
        val e0 = subexpressions[0]
        assertEquals(ParserLevel.LIST, e0.level)
        assertSubexpressionExists("1 + 2", src, subexpressions)
        assertSubexpressionExists("(1 + 2)", src, subexpressions)
        assertSubexpressionExists("3 + 4", src, subexpressions)
        assertSubexpressionExists("(3 + 4)", src, subexpressions)
        assertSubexpressionExists("(1 + 2) + (3 + 4)", src, subexpressions)
    }

    @Test
    fun listAndBooleanCallbacks() {
        val engine = makeEngine()
        val src = "(1;2) (3 or 4)"
        val callbacks = ParseTreeResultCallbacks()
        engine.parse(StringSourceLocation(src), callbacks = listOf(callbacks))
        val subexpressions = callbacks.root
        assertEquals(1, subexpressions.size)
        val e0 = subexpressions[0]
        assertEquals(ParserLevel.LIST, e0.level)
        assertSubexpressionExists("1;2", src, subexpressions)
        assertSubexpressionExists("(1;2)", src, subexpressions)
        assertSubexpressionExists("3 or 4", src, subexpressions)
        assertSubexpressionExists("(3 or 4)", src, subexpressions)
        assertSubexpressionExists("(1;2) (3 or 4)", src, subexpressions)
    }

    @Test
    fun monadicFunctionCall() {
        val engine = makeEngine()
        val src = "+ 123"
        val callbacks = ParseTreeResultCallbacks()
        engine.parse(StringSourceLocation(src), callbacks = listOf(callbacks))
        val subexpressions = callbacks.root
        assertEquals(1, subexpressions.size)
        val e0 = subexpressions[0]
        assertEquals(ParserLevel.LIST, e0.level)
        assertSubexpressionExists("123", src, subexpressions)
        assertSubexpressionExists("+ 123", src, subexpressions)
    }

    @Test
    fun simpleArrayIndex() {
        val src = "foo[1]"
        val subexpressions = parseToParseTree(src)
        assertSubexpressionExists("foo", src, subexpressions)
        assertSubexpressionExists("1", src, subexpressions)
        assertSubexpressionExists("foo[1]", src, subexpressions)
    }

    @Test
    fun arrayIndexWithExpression() {
        val src = "foo[1] + bar[2]"
        val subexpressions = parseToParseTree(src)
        assertSubexpressionExists("foo", src, subexpressions)
        assertSubexpressionExists("1", src, subexpressions)
        assertSubexpressionExists("bar", src, subexpressions)
        assertSubexpressionExists("2", src, subexpressions)
        assertSubexpressionExists("foo[1]", src, subexpressions)
        assertSubexpressionExists("bar[2]", src, subexpressions)
        assertSubexpressionExists("foo[1] + bar[2]", src, subexpressions)
    }

    @Test
    fun sequentialArrayIndex() {
        val src = "foo[abc][def]"
        val subexpressions = parseToParseTree(src)
        assertSubexpressionExists("foo", src, subexpressions)
        assertSubexpressionExists("abc", src, subexpressions)
        assertSubexpressionExists("def", src, subexpressions)
        assertSubexpressionExists("foo[abc]", src, subexpressions)
        assertSubexpressionExists("foo[abc][def]", src, subexpressions)
    }

    private fun parseToParseTree(src: String): List<Node> {
        val engine = makeEngine()
        val callbacks = ParseTreeResultCallbacks()
        engine.parse(StringSourceLocation(src), callbacks = listOf(callbacks))
        val subexpressions = callbacks.root
        assertEquals(1, subexpressions.size)
        assertEquals(ParserLevel.LIST, subexpressions[0].level)
        return subexpressions
    }

    private fun assertSubexpressionExists(expected: String, src: String, nodes: List<Node>) {
        fun recurse(l: List<Node>): Boolean {
            l.forEach { node ->
                val s = src.substring(node.pos.col, node.pos.computedEndCol)
                if (s == expected) {
                    return true
                }
                if (recurse(node.subnodes)) {
                    return true
                }
            }
            return false
        }

        val res = recurse(nodes)
        assertTrue(res, "Expected: '${expected}' to be found")
    }

    private fun printParseTree(src: String, root: List<Node>) {
        fun printNode(node: Node, level: Int) {
            val indent = buildString { repeat(level) { append("  ") } }
            println("${indent}Node: ${node::class.simpleName}: '${src.substring(node.pos.col, node.pos.computedEndCol)}'")
            println("${indent}      level=${node.level}")
            println("${indent}      pos=${node.pos}")
            if (node is Node.Expression) {
                println("${indent}      expr=${node.instr}")
            }
            node.subnodes.forEach { subnode -> printNode(subnode, level + 1) }
        }

        root.forEach { node -> printNode(node, 0) }
    }
}
