package com.dhsdevelopments.kap.gui2.report

import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.Engine
import com.dhsdevelopments.kap.FormatStyle
import com.dhsdevelopments.kap.VariableHolder
import javax.swing.SwingUtilities
import javax.swing.table.DefaultTableModel

class ReportTableModel : DefaultTableModel() {
    private val numRows = 100
    private val numCols = 40
    private val cells: List<List<ReportCell>> = makeDefaultContent()

    private val displayedValues = ArrayList<DisplayedValue>()

    override fun getRowCount() = numRows
    override fun getColumnCount() = numCols

    override fun getValueAt(row: Int, column: Int): ReportCell {
        return cells[row][column]
    }

    override fun getColumnClass(columnIndex: Int): Class<*> {
        return ReportCell::class.java
    }

    private fun makeDefaultContent(): List<List<ReportCell>> {
        val ret = ArrayList<List<ReportCell>>()
        repeat(numRows) {
            val row = ArrayList<ReportCell>()
            repeat(numCols) {
                row.add(ReportCell())
            }
            ret.add(row)
        }
        return ret
    }


    fun addDisplayedValue(engine: Engine, storage: VariableHolder, value: APLValue, row: Int, col: Int) {
        val displayedValue = DisplayedValue(engine, storage, row, col)
        displayedValues.add(displayedValue)
        updateContentForValue(displayedValue, value)
    }

    fun updateContentForValue(displayedValue: DisplayedValue, newContent: APLValue) {
        val row = displayedValue.row
        val col = displayedValue.col
        val d = newContent.dimensions

        val nRows: Int
        val nCols: Int
        when (d.size) {
            0 -> {
                nRows = 1
                nCols = 1
            }
            1 -> {
                nRows = 1
                nCols = d[0]
            }
            2 -> {
                nRows = d[0]
                nCols = d[1]
            }
            else -> throw IllegalArgumentException("Only rank 0-2 supported. Attempt to add value of rank: ${d.size}")
        }

        if (!displayedValue.initialised) {
            displayedValue.nRows = nRows
            displayedValue.nCols = nCols
            displayedValue.initialised = true
        } else {
            // TODO: Possibly update the grid size to accomodate a change in content size
        }

        if (d.size == 0) {
            cells[row][col].updateContent(newContent.disclose().formatted(FormatStyle.PLAIN))
            fireTableCellUpdated(row, col)
        } else if (d.size <= 2) {
            repeat(nRows) { rowIndex ->
                val currRow = cells[row + rowIndex]
                repeat(nCols) { colIndex ->
                    val v0 = newContent.valueAt((rowIndex * nCols) + colIndex)
                    currRow[col + colIndex].updateContent(v0.formatted(FormatStyle.PLAIN))
                }
            }
            fireTableRowsUpdated(row, row + nRows - 1)
        }
    }

    inner class DisplayedValue(
        engine: Engine,
        val storage: VariableHolder,
        var row: Int,
        var col: Int,
        var nRows: Int = 0,
        var nCols: Int = 0,
        var initialised: Boolean = false
    ) {
        init {
            storage.registerListener(engine) { newValue, oldValue ->
                val v0 = newValue.collapse()
                SwingUtilities.invokeLater {
                    updateContentForValue(this@DisplayedValue, v0)
                }
            }
        }
    }
}
