package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.builtins.StructuralUnderOp
import com.dhsdevelopments.kap.complex.Complex
import com.dhsdevelopments.mpbignum.BigInt
import com.dhsdevelopments.mpbignum.Rational
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

interface LvalueReader {
    fun assignArg(context: RuntimeContext, value: APLValue)
}

abstract class Instruction(val pos: Position) {
    abstract fun evalWithContext(context: RuntimeContext): APLValue

    open fun evalWithContextAndDiscardResult(context: RuntimeContext) {
        evalWithContext(context).collapse(withDiscard = true)
    }

    abstract fun children(): List<Instruction>
    abstract fun copy(updatedChildList: List<Instruction>, newPos: Position): Instruction

    protected inline fun ensureEmptyChildList(updatedChildList: List<Instruction>, fn: () -> Instruction): Instruction {
        require(updatedChildList.isEmpty()) { "updated child list must be empty" }
        return fn()
    }

    open fun deriveLvalueReader(): LvalueReader = throw IncompatibleTypeParseException("Unable to assign to expression", this.pos)
}

class DummyInstr(pos: Position) : Instruction(pos) {
    override fun evalWithContext(context: RuntimeContext): APLValue {
        throw IllegalStateException("Attempt to call dummy instruction")
    }

    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { DummyInstr(newPos) }
}

class InstructionList private constructor(val instructions: List<Instruction>, pos: Position) : Instruction(pos) {
    constructor(instructions: List<Instruction>) : this(instructions, computePos(instructions))

    override fun evalWithContext(context: RuntimeContext): APLValue {
        val len = instructions.size
        for (i in 0 until len - 1) {
            val instr = instructions[i]
            instr.evalWithContextAndDiscardResult(context)
        }
        return instructions[len - 1].evalWithContext(context)
    }

    override fun children() = instructions
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = InstructionList(updatedChildList, pos)

    companion object {
        private fun computePos(l: List<Instruction>): Position {
            return when (l.size) {
                0 -> throw IllegalStateException("Empty instruction list")
                1 -> l[0].pos
                else -> l.last().pos.let { last -> l[0].pos.copy(endLine = last.computedEndLine, endCol = last.computedEndCol) }
            }
        }
    }
}

class LiteralAPLList private constructor(val instructions: List<Instruction>, pos: Position) : Instruction(pos) {
    constructor(instructions: List<Instruction>) : this(instructions, instructions[0].pos)

    override fun evalWithContext(context: RuntimeContext): APLValue {
        val resultList = ArrayList<APLValue>()
        instructions.forEach { instr ->
            resultList.add(instr.evalWithContext(context))
        }
        return APLList(resultList)
    }

    override fun children() = instructions
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = LiteralAPLList(updatedChildList, newPos)

    override fun deriveLvalueReader(): LvalueReader {
        val innerReaders = instructions.map(Instruction::deriveLvalueReader)
        return object : LvalueReader {
            override fun assignArg(context: RuntimeContext, value: APLValue) {
                if (value !is APLList) {
                    throwAPLException(
                        DestructuringAssignmentShapeMismatch(
                            "In destructuring assignment, expected a list, got: ${value.kapClass.name}", pos))
                }
                if (value.listSize() != innerReaders.size) {
                    throwAPLException(
                        DestructuringAssignmentShapeMismatch(
                            "In destructuring assignment, expected a list of size ${innerReaders.size}, got: ${value.listSize()}", pos))
                }
                innerReaders.forEachIndexed { i, lvalueReader ->
                    lvalueReader.assignArg(context, value.listElement(i))
                }
            }
        }
    }

    override fun toString() = "LiteralAPLList[instructions=${instructions}]"
}

abstract class FunctionCallInstruction(pos: Position) : Instruction(pos) {
    @OptIn(ExperimentalContracts::class)
    protected inline fun <T> withStackRecovery(fn: () -> T): T {
        contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
        try {
            return fn()
        } catch (e: APLEvalException) {
            // Recover the calling position and update the corresponding stack frame.
            // This approach may seem to be overly complex compared to simply writing passing the caller position when
            // calling the function. That approach with be simpler, but it would have a performance impact on every
            // function call. Since exception handlers are free unless they are called, we move the performance
            // impact to the case when an exception is thrown, which should not be very often.
            val stack = currentStorageStackOrNull()
            val exceptionStack = e.stack
            if (stack != null && exceptionStack != null) {
                val activeFrame = stack.stack.last()
                val matchedFrame = exceptionStack.frames.find { v -> v.frame === activeFrame }
                if (matchedFrame != null) {
                    matchedFrame.calledFrom = pos
                }
            }
            throw e
        }
    }
}

class FunctionCall1Arg(
    val fn: APLFunction,
    val rightArgs: Instruction,
    pos: Position
) : FunctionCallInstruction(pos) {
    override fun evalWithContext(context: RuntimeContext): APLValue {
        return withStackRecovery {
            fn.evalArgsAndCall1Arg(context, rightArgs)
        }
    }

    override fun evalWithContextAndDiscardResult(context: RuntimeContext) {
        withStackRecovery {
            fn.evalArgsAndCall1ArgDiscardResult(context, rightArgs)
        }
    }

    override fun children() = listOf(rightArgs)
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = FunctionCall1Arg(fn, updatedChildList[0], newPos)

    override fun toString() = "FunctionCall1Arg(fn=${fn}, rightArgs=${rightArgs})"
}

class FunctionCall2Arg(
    val fn: APLFunction,
    val leftArgs: Instruction,
    val rightArgs: Instruction,
    pos: Position
) : FunctionCallInstruction(pos) {
    override fun evalWithContext(context: RuntimeContext): APLValue {
        return withStackRecovery {
            fn.evalArgsAndCall2Arg(context, leftArgs, rightArgs)
        }
    }

    override fun evalWithContextAndDiscardResult(context: RuntimeContext) {
        withStackRecovery {
            fn.evalArgsAndCall2ArgDiscardResult(context, leftArgs, rightArgs)
        }
    }

    override fun children() = listOf(leftArgs, rightArgs)
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = FunctionCall2Arg(fn, updatedChildList[0], updatedChildList[1], newPos)

    override fun toString() = "FunctionCall2Arg(fn=${fn}, leftArgs=${leftArgs}, rightArgs=${rightArgs})"
}

class DynamicFunctionDescriptor(val instr: Instruction) : APLFunctionDescriptor {
    class DynamicFunctionImpl(val instr: Instruction, pos: FunctionInstantiation, val bindEnv: Environment? = null) : APLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            return resolveFn(context).eval1Arg(context, a, axis)
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            return resolveFn(context).eval2Arg(context, a, b, axis)
        }

        override fun evalInverse1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            return resolveFn(context).evalInverse1Arg(context, a, axis)
        }

        override fun evalInverse2ArgB(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            return resolveFn(context).evalInverse2ArgB(context, a, b, axis)
        }

        override fun evalInverse2ArgA(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            return resolveFn(context).evalInverse2ArgA(context, a, b, axis)
        }

        override fun evalWithStructuralUnder1Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            return resolveFn(context).evalWithStructuralUnder1Arg(baseFn, context, a, axis)
        }

        override fun evalWithStructuralUnder2Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            return resolveFn(context).evalWithStructuralUnder2Arg(baseFn, context, a, b, axis)
        }

        private fun resolveFn(context: RuntimeContext): APLFunction {
            val result = instr.evalWithContext(context)
            val v = result.unwrapDeferredValue()
            if (v !is LambdaValue) {
                throwAPLException(IncompatibleTypeException("Cannot evaluate values of type: ${v.kapClass.name}", pos))
            }
            return v.makeClosure()
        }

        override fun capturedEnvironments(): List<Environment> {
            return if (bindEnv == null) emptyList() else listOf(bindEnv)
        }

        override fun computeClosure(parser: APLParser): Pair<APLFunction, List<Instruction>> {
            val sym = parser.tokeniser.engine.createAnonymousSymbol("leftAssignedFunction")
            val binding = parser.currentEnvironment().bindLocal(sym)
            val ref = StackStorageRef(binding)
            val list = listOf<Instruction>(AssignmentInstruction.make(ref, instr, pos))
            val env = parser.currentEnvironment()
            return Pair(DynamicFunctionImpl(VariableRef(sym, ref, pos), FunctionInstantiation(parser.tokeniser.engine, pos, env), env), list)
        }
    }

    override fun make(instantiation: FunctionInstantiation): APLFunction {
        return DynamicFunctionImpl(instr, instantiation, if (instantiation.env.subtreeHasLocalBindings()) instantiation.env else null)
    }
}

class VariableRef(val name: Symbol, val storageRef: StackStorageRef, pos: Position) : Instruction(pos) {
    override fun evalWithContext(context: RuntimeContext): APLValue {
        return currentStorageStack().findStorage(storageRef).value() ?: throwAPLException(VariableNotAssigned(storageRef.name, pos))
    }

    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { VariableRef(name, storageRef, newPos) }

    override fun deriveLvalueReader(): LvalueReader {
        if (storageRef.binding.storage.isConst) {
            throw AssignmentToConstantException(storageRef.binding.name, pos)
        }
        return object : LvalueReader {
            override fun assignArg(context: RuntimeContext, value: APLValue) {
                context.setVar(storageRef, value)
            }
        }
    }

    override fun toString() = "Var(${name})"
}

class Literal1DArray private constructor(val values: List<Instruction>, pos: Position) : Instruction(pos) {
    override fun evalWithContext(context: RuntimeContext): APLValue {
        val size = values.size
        val result = Array<APLValue?>(size) { null }
        for (i in (size - 1) downTo 0) {
            result[i] = values[i].evalWithContext(context)
        }
        return APLArrayImpl.make(dimensionsOfSize(size)) { result[it]!! }
    }

    override fun children() = values
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = Literal1DArray(updatedChildList, newPos)

    override fun deriveLvalueReader(): LvalueReader {
        val innerReaders = values.map(Instruction::deriveLvalueReader)
        return object : LvalueReader {
            override fun assignArg(context: RuntimeContext, value: APLValue) {
                if (value.dimensions.size != 1 || value.dimensions[0] != innerReaders.size) {
                    throwAPLException(
                        DestructuringAssignmentShapeMismatch(
                            "In destructuring assignment, expected a rank-1 array of ${innerReaders.size}, got dimensions: ${value.dimensions}}",
                            pos))
                }
                innerReaders.forEachIndexed { i, lvalueReader ->
                    lvalueReader.assignArg(context, value.valueAt(i))
                }
            }
        }
    }

    override fun toString() = "Literal1DArray(${values})"

    companion object {
        fun make(values: List<Instruction>): Instruction {
            require(values.isNotEmpty()) { "attempt to create empty Literal1DArray" }
            return when (val firstElement = values[0]) {
                is LiteralInteger -> {
                    collectLongValues(firstElement.value, values, firstElement.pos)
                }
                is LiteralDouble -> {
                    collectDoubleValues(firstElement.value, values, firstElement.pos)
                }
                else -> Literal1DArray(values, firstElement.pos)
            }
        }

        private fun collectLongValues(firstValue: Long, values: List<Instruction>, pos: Position): Instruction {
            fun makePos(): Position {
                return if (values.isEmpty()) {
                    pos
                } else {
                    pos.expandToEnd(values.last().pos)
                }
            }

            val result = ArrayList<Long>()
            result.add(firstValue)
            var isBoolean = firstValue == 0L || firstValue == 1L
            for (i in 1 until values.size) {
                val v = values[i]
                if (v is LiteralInteger) {
                    if (isBoolean && v.value != 0L && v.value != 1L) {
                        isBoolean = false
                    }
                    result.add(v.value)
                } else {
                    return Literal1DArray(values, makePos())
                }
            }
            return if (isBoolean) {
                val booleanResultArray = result.map { v ->
                    when (v) {
                        0L -> false
                        1L -> true
                        else -> error("Non-boolean value in array")
                    }
                }.toBooleanArray()
                LiteralBooleanArray(booleanResultArray, makePos())
            } else {
                LiteralLongArray(result.toLongArray(), makePos())
            }
        }

        private fun collectDoubleValues(firstValue: Double, values: List<Instruction>, pos: Position): Instruction {
            fun makePos(): Position {
                return if (values.isEmpty()) {
                    pos
                } else {
                    pos.expandToEnd(values.last().pos)
                }
            }

            val result = ArrayList<Double>()
            result.add(firstValue)
            for (i in 1 until values.size) {
                val v = values[i]
                if (v is LiteralDouble) {
                    result.add(v.value)
                } else {
                    return Literal1DArray(values, makePos())
                }
            }
            return LiteralDoubleArray(result.toDoubleArray(), makePos())
        }
    }
}

abstract class LiteralAPLValueInstruction(pos: Position) : Instruction(pos) {
    abstract val valueInt: APLValue
}

class LiteralInteger private constructor(override val valueInt: APLLong, pos: Position) : LiteralAPLValueInstruction(pos) {
    constructor(value: Long, pos: Position) : this(value.makeAPLNumber(), pos)

    override fun evalWithContext(context: RuntimeContext) = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralInteger(valueInt, newPos) }
    override fun toString() = "LiteralInteger[value=$valueInt]"
    val value get() = valueInt.value
}

class LiteralDouble private constructor(override val valueInt: APLDouble, pos: Position) : LiteralAPLValueInstruction(pos) {
    constructor(value: Double, pos: Position) : this(value.makeAPLNumber(), pos)

    override fun evalWithContext(context: RuntimeContext) = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralDouble(valueInt, newPos) }
    override fun toString() = "LiteralDouble[value=$valueInt]"
    val value get() = valueInt.value
}

class LiteralComplex private constructor(override val valueInt: APLComplex, pos: Position) : LiteralAPLValueInstruction(pos) {
    constructor(value: Complex, pos: Position) : this(APLComplex(value), pos)

    override fun evalWithContext(context: RuntimeContext) = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralComplex(valueInt, newPos) }
    override fun toString() = "LiteralComplex[value=$valueInt]"
    val value get() = valueInt.value
}

class LiteralBigInt private constructor(override val valueInt: APLBigInt, pos: Position) : LiteralAPLValueInstruction(pos) {
    constructor(value: BigInt, pos: Position) : this(APLBigInt(value), pos)

    override fun evalWithContext(context: RuntimeContext) = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralBigInt(valueInt, newPos) }
    override fun toString() = "LiteralComplex[value=$valueInt]"
    val value get() = valueInt.asBigInt(pos)
}

class LiteralRational private constructor(override val valueInt: APLRational, pos: Position) : LiteralAPLValueInstruction(pos) {
    constructor(value: Rational, pos: Position) : this(APLRational(value), pos)

    override fun evalWithContext(context: RuntimeContext) = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralRational(valueInt, newPos) }

    override fun toString() = "LiteralRational[value=$valueInt]"
    val value get() = valueInt.asRational(pos)
}

class LiteralCharacter private constructor(override val valueInt: APLChar, pos: Position) : LiteralAPLValueInstruction(pos) {
    constructor(value: Int, pos: Position) : this(APLChar(value), pos)

    override fun evalWithContext(context: RuntimeContext) = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralCharacter(valueInt, newPos) }
    override fun toString() = "LiteralCharacter[value=$valueInt]"
    val value get() = valueInt.value
}

class LiteralSymbol private constructor(override val valueInt: APLSymbol, pos: Position) : LiteralAPLValueInstruction(pos) {
    constructor(name: Symbol, pos: Position) : this(APLSymbol(name), pos)

    override fun evalWithContext(context: RuntimeContext): APLValue = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralSymbol(valueInt, newPos) }
    val value get() = valueInt.value
}

class LiteralAPLNullValue(pos: Position) : LiteralAPLValueInstruction(pos) {
    override val valueInt get() = APLNullValue
    override fun evalWithContext(context: RuntimeContext) = APLNullValue
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralAPLNullValue(newPos) }
}

class EmptyValueMarker(pos: Position) : Instruction(pos) {
    override fun evalWithContext(context: RuntimeContext) = APLNilValue
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { EmptyValueMarker(newPos) }
}

class LiteralStringValue private constructor(override val valueInt: APLString, pos: Position) : LiteralAPLValueInstruction(pos) {
    constructor(value: String, pos: Position) : this(APLString.make(value), pos)

    override fun evalWithContext(context: RuntimeContext) = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralStringValue(valueInt, newPos) }
}

class LiteralBooleanArray private constructor(override val valueInt: APLArrayBoolean, pos: Position) : LiteralAPLValueInstruction(pos) {
    constructor(value: BooleanArray, pos: Position) : this(APLArrayBoolean(dimensionsOfSize(value.size), value), pos)

    override fun evalWithContext(context: RuntimeContext): APLValue = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralBooleanArray(valueInt, newPos) }
    val value get() = valueInt.values
}

class LiteralLongArray private constructor(override val valueInt: APLArrayLong, pos: Position) : LiteralAPLValueInstruction(pos) {
    constructor(value: LongArray, pos: Position) : this(APLArrayLong(dimensionsOfSize(value.size), value), pos)

    override fun evalWithContext(context: RuntimeContext): APLValue = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralLongArray(valueInt, newPos) }
    val value get() = valueInt.values
}

class LiteralDoubleArray private constructor(override val valueInt: APLArrayDouble, pos: Position) : LiteralAPLValueInstruction(pos) {
    constructor(value: DoubleArray, pos: Position) : this(APLArrayDouble(dimensionsOfSize(value.size), value), pos)

    override fun evalWithContext(context: RuntimeContext): APLValue = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = ensureEmptyChildList(updatedChildList) { LiteralDoubleArray(valueInt, newPos) }
    val value get() = valueInt.values
}

class ConstantValueInstruction<T : APLValue>(override val valueInt: T, pos: Position) : LiteralAPLValueInstruction(pos) {
    override fun evalWithContext(context: RuntimeContext) = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) =
        ensureEmptyChildList(updatedChildList) { ConstantValueInstruction(valueInt, newPos) }
}

class AssignmentInstruction private constructor(val variableRef: StackStorageRef, val instr: Instruction, pos: Position) : Instruction(pos) {
    override fun evalWithContext(context: RuntimeContext): APLValue {
        val res = instr.evalWithContext(context).collapse()
        context.setVar(variableRef, res)
        return res
    }

    override fun children() = listOf(instr)
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = AssignmentInstruction(variableRef, updatedChildList[0], newPos)

    companion object {
        fun make(variableRef: StackStorageRef, instr: Instruction, pos: Position): AssignmentInstruction {
            if (variableRef.binding.storage.isConst) {
                throw AssignmentToConstantException(variableRef.binding.name, pos)
            }
            return AssignmentInstruction(variableRef, instr, pos)
        }
    }
}

class DestructureAssignInstruction(val lvalueReader: LvalueReader, val arg: Instruction, pos: Position) : Instruction(pos) {
    override fun evalWithContext(context: RuntimeContext): APLValue {
        val res = arg.evalWithContext(context)
        val v = res.collapse()
        lvalueReader.assignArg(context, v)
        return v
    }

    override fun children() = listOf(arg)
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = DestructureAssignInstruction(lvalueReader, updatedChildList[0], newPos)
}

class UserFunction(
    val name: Symbol,
    val leftFnArgs: List<EnvironmentBinding>,
    val rightFnArgs: List<EnvironmentBinding>,
    var instr: Instruction,
    val env: Environment
) : APLFunctionDescriptor {
    inner class UserFunctionImpl(pos: FunctionInstantiation) : APLFunction(pos) {
        private val leftStorageRefs = leftFnArgs.map(::StackStorageRef)
        private val rightStorageRefs = rightFnArgs.map(::StackStorageRef)

        override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            return withLinkedContext(env, name.nameWithNamespace, pos) {
                context.assignArgs(rightStorageRefs, a, pos)
                instr.evalWithContext(context)
            }
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            return withLinkedContext(env, name.nameWithNamespace, pos) {
                context.assignArgs(leftStorageRefs, a, pos)
                context.assignArgs(rightStorageRefs, b, pos)
                instr.evalWithContext(context)
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = UserFunctionImpl(instantiation)
}

class EvalLambdaFnx(val fn: APLFunction, pos: Position, val relatedInstructions: List<Instruction> = emptyList()) : Instruction(pos) {
    override fun evalWithContext(context: RuntimeContext): APLValue {
        relatedInstructions.asReversed().forEach { instr ->
            instr.evalWithContext(context)
        }
        return LambdaValue(fn, currentStorageStack().currentFrame())
    }

    override fun children() = relatedInstructions
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = EvalLambdaFnx(fn, newPos, updatedChildList)
}

class MemberDereferenceInstruction(val leftInstr: Instruction, val rightInstr: Instruction, pos: Position) : Instruction(pos) {
    override fun evalWithContext(context: RuntimeContext): APLValue {
        val rightValue = rightInstr.evalWithContext(context)
        val leftValue = leftInstr.evalWithContext(context).unwrapDeferredValue()
        return when {
            leftValue is APLMap -> leftValue.lookupValue(rightValue.makeTypeQualifiedKey()) ?: throwAPLException(KeyNotFoundException(pos))
            leftValue is APLList -> leftValue.listElement(rightValue.ensureNumber(pos).asInt(pos), pos)
            leftValue.dimensions.size > 0 -> {
                val rightValueDimensions = rightValue.dimensions
                val index = when (rightValueDimensions.size) {
                    0 -> leftValue.dimensions.indexFromPositionNegativeSupport(rightValue.ensureNumber(pos).asInt(pos), pos)
                    1 -> leftValue.dimensions.indexFromPositionNegativeSupport(rightValue.toIntArray(pos), pos)
                    else -> throwAPLException(InvalidDimensionsException("Index must be a scalar or 1-dimensional array", pos))
                }
                leftValue.valueAt(index)
            }
            else -> throwAPLException(IncompatibleTypeException("Invalid type for member dereference. Got: ${leftValue.kapClass.name}"))
        }
    }

    override fun children() = listOf(leftInstr, rightInstr)
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = MemberDereferenceInstruction(updatedChildList[0], updatedChildList[1], newPos)
}

sealed class FunctionCallChain(pos: FunctionInstantiation, fns: List<APLFunction>) : APLFunction(pos, fns) {
    class Chain2(pos: FunctionInstantiation, fn0: APLFunction, fn1: APLFunction) :
        FunctionCallChain(pos, listOf(fn0, fn1)) {
        val fn0 get() = fns[0]
        val fn1 get() = fns[1]

        override val optimisationFlags = computeOptimisationFlags()

        private fun computeOptimisationFlags(): OptimisationFlags {
            return OptimisationFlags(0)
        }

        override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            ensureAxisNull(axis)
            val res = fn1.eval1Arg(context, a, null)
            return fn0.eval1Arg(context, res, null)
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            ensureAxisNull(axis)
            val res = fn1.eval2Arg(context, a, b, null)
            return fn0.eval1Arg(context, res, null)
        }

        override fun evalInverse1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            ensureAxisNull(axis)
            val res = fn0.evalInverse1Arg(context, a, null)
            return fn1.evalInverse1Arg(context, res, null)
        }

        private val structuralUnderOp = StructuralUnderOp()

        override fun evalWithStructuralUnder1Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            ensureAxisNull(axis)
            val innerFn: APLFunction = structuralUnderOp.combineFunction(baseFn, fn0, fn0.instantiation).make(instantiation)
            val outerFn = structuralUnderOp.combineFunction(innerFn, fn1, fn1.instantiation).make(instantiation)
            return outerFn.eval1Arg(context, a, null)
        }

        override fun evalInverse2ArgB(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            ensureAxisNull(axis)
            val res = fn0.evalInverse1Arg(context, b, null)
            return fn1.evalInverse2ArgB(context, a, res, null)
        }

        override fun copy(fns: List<APLFunction>) = Chain2(instantiation, fns[0], fns[1])
    }

    class Chain3(pos: FunctionInstantiation, fn0: APLFunction, fn1: APLFunction, fn2: APLFunction) : FunctionCallChain(pos, listOf(fn0, fn1, fn2)) {
        val fn0 get() = fns[0]
        val fn1 get() = fns[1]
        val fn2 get() = fns[2]

        override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            ensureAxisNull(axis)
            val right = fn2.eval1Arg(context, a, null)
            val left = fn0.eval1Arg(context, a, null)
            return fn1.eval2Arg(context, left, right, null)
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            ensureAxisNull(axis)
            val right = fn2.eval2Arg(context, a, b, null)
            val left = fn0.eval2Arg(context, a, b, null)
            return fn1.eval2Arg(context, left, right, null)
        }

        override fun copy(fns: List<APLFunction>) = Chain3(instantiation, fns[0], fns[1], fns[2])
    }
}
