package com.dhsdevelopments.kap.log

import kotlinx.datetime.Clock
import kotlinx.datetime.format
import kotlinx.datetime.format.DateTimeComponents

enum class Level(val level: Int) {
    OFF(0), ERROR(1), WARNING(2), INFO(3), VERBOSE(4), DEBUG(5)
}

//abstract class Log4kLogger(tag: String) {
//    val logger = Logger.of(tag)
//
//    var level: Level
//        get() {
//            return when (logger.level) {
//                io.github.smyrgeorge.log4k.Level.TRACE -> Level.VERBOSE
//                io.github.smyrgeorge.log4k.Level.DEBUG -> Level.DEBUG
//                io.github.smyrgeorge.log4k.Level.INFO -> Level.INFO
//                io.github.smyrgeorge.log4k.Level.WARN -> Level.WARNING
//                io.github.smyrgeorge.log4k.Level.ERROR -> Level.ERROR
//                io.github.smyrgeorge.log4k.Level.OFF -> Level.OFF
//            }
//        }
//        set(newLevel) {
//            logger.level = when (newLevel) {
//                Level.DEBUG -> io.github.smyrgeorge.log4k.Level.DEBUG
//                Level.VERBOSE -> io.github.smyrgeorge.log4k.Level.TRACE
//                Level.INFO -> io.github.smyrgeorge.log4k.Level.INFO
//                Level.WARNING -> io.github.smyrgeorge.log4k.Level.WARN
//                Level.ERROR -> io.github.smyrgeorge.log4k.Level.ERROR
//                Level.OFF -> io.github.smyrgeorge.log4k.Level.OFF
//            }
//        }
//
//    inline fun d(message: () -> String) = logger.debug(message)
//    inline fun d(e: Throwable, message: () -> String) = logger.debug(e, message)
//    inline fun i(message: () -> String) = logger.info(message)
//    inline fun i(e: Throwable, message: () -> String) = logger.info(e, message)
//    inline fun v(message: () -> String) = logger.trace(message)
//    inline fun v(e: Throwable, message: () -> String) = logger.trace(e, message)
//    inline fun w(message: () -> String) = logger.warn(message)
//    inline fun w(e: Throwable, message: () -> String) = logger.warn(e, message)
//    inline fun e(message: () -> String) = logger.error(message)
//    inline fun e(e: Throwable, message: () -> String) = logger.error(e, message)
//}

abstract class SimpleLogger(val tag: String) {
    var level: Level = Level.INFO

    inline fun logWithLevel(msgLevel: Level, message: () -> String) {
        if (msgLevel.level < level.level) {
            formatMessage(msgLevel, message())
        }
    }

    inline fun logWithLevelAndThrowable(msgLevel: Level, e: Throwable, message: () -> String) {
        if (msgLevel.level <= level.level) {
            formatMessage(msgLevel, message(), e)
        }
    }

    inline fun d(message: () -> String) = logWithLevel(Level.DEBUG, message)
    inline fun d(e: Throwable, message: () -> String) = logWithLevelAndThrowable(Level.DEBUG, e, message)
    inline fun i(message: () -> String) = logWithLevel(Level.INFO, message)
    inline fun i(e: Throwable, message: () -> String) = logWithLevelAndThrowable(Level.INFO, e, message)
    inline fun v(message: () -> String) = logWithLevel(Level.VERBOSE, message)
    inline fun v(e: Throwable, message: () -> String) = logWithLevelAndThrowable(Level.VERBOSE, e, message)
    inline fun w(message: () -> String) = logWithLevel(Level.WARNING, message)
    inline fun w(e: Throwable, message: () -> String) = logWithLevelAndThrowable(Level.WARNING, e, message)
    inline fun e(message: () -> String) = logWithLevel(Level.ERROR, message)
    inline fun e(e: Throwable, message: () -> String) = logWithLevelAndThrowable(Level.ERROR, e, message)

    fun formatMessage(msgLevel: Level, message: String, e: Throwable? = null) {
        val now = Clock.System.now()
        val formatted = now.format(DateTimeComponents.Formats.ISO_DATE_TIME_OFFSET)
        println("${formatted}: ${tag}: ${msgLevel}: ${message}")
        if (e != null) {
            e.printStackTrace()
        }
    }
}
