package com.dhsdevelopments.kap.json

abstract class JsonElement

data class JsonString(val value: String) : JsonElement()

data class JsonObject(val values: List<Pair<String, JsonElement>>) : JsonElement() {
    fun lookup(key: String) = values.find { (k, _) -> k == key }?.second
}

data class JsonNumber(val value: Number) : JsonElement()
data class JsonArray(val values: List<JsonElement>) : JsonElement()
data class JsonBoolean(val value: Boolean) : JsonElement() {
    companion object {
        fun fromBoolean(value: Boolean) = if (value) JSON_TRUE else JSON_FALSE

        val JSON_TRUE = JsonBoolean(true)
        val JSON_FALSE = JsonBoolean(false)
    }
}

object JsonNull : JsonElement()

interface JsonDecoder {
    fun decodeJsonFromString(source: String): JsonElement
}

expect fun makeJsonDecoder(): JsonDecoder

fun makeJsonString(s: String): String {
    val buf = StringBuilder()
    buf.append("\"")
    s.forEach { ch ->
        val escaped = when (ch) {
            '\n' -> "\\n"
            '\r' -> "\\r"
            '\t' -> "\\t"
            '\b' -> "\\b"
            '\"' -> "\\\""
            '\\' -> "\\\\"
            else -> ch.toString()
        }
        buf.append(escaped)
    }
    buf.append("\"")
    return buf.toString()
}
