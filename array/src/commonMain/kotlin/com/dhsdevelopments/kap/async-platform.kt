package com.dhsdevelopments.kap

class CallAsyncFunction(val fn0: APLFunction, val fn1: APLFunction) : APLFunctionDescriptor {
    class CallAsyncFunctionImpl(fn0: APLFunction, fn1: APLFunction, pos: FunctionInstantiation) : APLFunction(pos, listOf(fn0, fn1)) {
        val fn0 get() = fns[0]
        val fn1 get() = fns[1]

        override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            val a0 = a.collapse()
            val axis0 = axis?.collapse()
            fn1.callAsync1Arg(context, a0, axis0) { context0, res ->
                try {
                    val v0 = fn0.eval1Arg(context0, res, null).collapse()
                    Either.Left(v0)
                } catch (e: APLGenericException) {
                    Either.Right(e)
                }
            }
            return APLNullValue
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            val b0 = b.collapse()
            val axis0 = axis?.collapse()
            val a0 = a.collapse()
            fn1.callAsync2Arg(context, a0, b0, axis0) { context0, res ->
                try {
                    val v0 = fn0.eval1Arg(context0, res, null).collapse()
                    Either.Left(v0)
                } catch (e: APLGenericException) {
                    Either.Right(e)
                }
            }
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = CallAsyncFunctionImpl(fn0, fn1, instantiation)
}


class CallAsyncOp : APLOperatorTwoArg {
    override fun combineFunction(fn0: APLFunction, fn1: APLFunction, opPos: FunctionInstantiation): APLFunctionDescriptor {
        return CallAsyncFunction(fn0, fn1)
    }
}
