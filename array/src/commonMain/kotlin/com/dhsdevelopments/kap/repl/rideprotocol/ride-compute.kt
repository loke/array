package com.dhsdevelopments.kap.repl.rideprotocol

import com.dhsdevelopments.kap.Engine
import com.dhsdevelopments.kap.mpthread.MPLock
import com.dhsdevelopments.kap.mpthread.MPThread
import com.dhsdevelopments.kap.mpthread.startThread
import com.dhsdevelopments.kap.mpthread.withLocked

class RideComputeQueue(val engine: Engine) {
    private var computeThread: MPThread? = null
    private val queue = ArrayDeque<EvalRequest>()
    private val lock = MPLock()
    private val condvar = lock.makeCondVar()
    private var stopRequested = false

    fun start() {
        computeThread = startThread {
            mainLoop()
        }
    }

    fun addRequest(req: EvalRequest) {
        lock.withLocked {
            queue.add(req)
            condvar.signal()
        }
    }

    private fun mainLoop() {
        while (true) {
            val req = lock.withLocked {
                while (queue.isEmpty()) {
                    condvar.waitUpdate()
                }
                // Need to return null here since we can't break out of a withLocked form
                if (stopRequested) null else queue.removeFirst()
            }
            println("Got request: ${req}")
            if (req == null) {
                break
            } else {
                req.handle(engine)
            }
        }
    }

    fun stop() {
        lock.withLocked {
            stopRequested = true
            condvar.signal()
        }
    }
}

fun interface EvalRequest {
    fun handle(engine: Engine)
}
