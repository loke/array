package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.jsObject2
import kotlinx.browser.document
import kotlinx.html.div
import kotlinx.html.dom.create
import kotlinx.html.js.div

fun displayPlot(data: dynamic) {
    val additionalError = data.additionalError
    if (additionalError != null) {
        val node = document.create.div {
            div { +"Note: At least one evaluation resulted in an error:" }
            div { +(additionalError.message as String) }
        }
        findResultHistoryNode().appendChild(node)
    }

    val plotData = data.plotData
    createChartOnPage(plotData)
}

private fun createChartOnPage(plotData: dynamic) {
    val element = createDiv("output-chart")
    val canvas = createCanvas("output-chart-inner")
    element.appendChild(canvas)
    findCurrentOutput().appendNode(element)
    val descriptor = jsObject2(
        "type" to "line",
        "data" to plotData,
        "options" to jsObject2(
            "elements" to jsObject2(
                "point" to jsObject2(
                    "pointStyle" to false))))
    Chart(canvas, descriptor)
}
