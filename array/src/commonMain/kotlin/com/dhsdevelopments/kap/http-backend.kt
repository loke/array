package com.dhsdevelopments.kap

interface HttpResult {
    val code: Long
    val content: String
    val headers: Map<String, List<String>>
}

enum class HttpMethod {
    GET,
    POST,
    PUT,
    DELETE,
    PATCH,
    HEAD,
    OPTIONS
}

data class HttpRequestData(
    val url: String,
    val method: HttpMethod,
    val data: ByteArray?,
    val headers: Map<String, String>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as HttpRequestData

        if (url != other.url) return false
        if (method != other.method) return false
        if (!data.contentEquals(other.data)) return false
        if (headers != other.headers) return false

        return true
    }

    override fun hashCode(): Int {
        var result = url.hashCode()
        result = 31 * result + method.hashCode()
        result = 31 * result + (data?.contentHashCode() ?: 0)
        result = 31 * result + headers.hashCode()
        return result
    }
}

expect fun httpRequest(engine: Engine, httpRequestData: HttpRequestData, pos: Position? = null): HttpResult

expect fun httpRequestCallback(engine: Engine, httpRequestData: HttpRequestData, pos: Position?, callback: (HttpResult) -> Either<APLValue, Exception>)
