package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.builtins.TagCatch
import kotlin.test.*

class ExceptionsTest : APLTest() {
    @Test
    fun simpleException() {
        parseAPLExpression("{'foo throw 1}catch 1 2 ⍴ 'foo λ{2+⍺}").let { result ->
            assertSimpleNumber(3, result)
        }
    }

    @Test
    fun simpleExceptionWithSingleDimensionCatch() {
        parseAPLExpression("{'foo throw 1}catch 'foo λ{2+⍺}").let { result ->
            assertSimpleNumber(3, result)
        }
    }

    @Test
    fun exceptionHandlerTagCheck() {
        parseAPLExpression2("{'foo throw 1}catch 1 2 ⍴ 'foo λ{⍵}").let { (result, engine) ->
            assertSymbolName(engine, "foo", result)
        }
    }

    @Test
    fun multipleTagHandlers() {
        parseAPLExpression("{'foo throw 1}catch 4 2 ⍴ 'xyz λ{2+⍺} 'test123 λ{3+⍺} 'bar λ{4+⍺} 'foo λ{5+⍺}").let { result ->
            assertSimpleNumber(6, result)
        }
    }

    @Test
    fun multipleTagHandlersSingleDimensionCatch() {
        parseAPLExpression("{'foo throw 1}catch 'xyz λ{2+⍺} 'test123 λ{3+⍺} 'bar λ{4+⍺} 'foo λ{5+⍺}").let { result ->
            assertSimpleNumber(6, result)
        }
    }

    @Test
    fun unmatchedTag() {
        assertFailsWith<TagCatch> {
            parseAPLExpression("{'foo throw 1}catch 1 2 ⍴ 'bar λ{2+⍺}")
        }
    }

    @Test
    fun throwWithoutTagHandler() {
        assertFailsWith<TagCatch> {
            parseAPLExpression("1 + 'foo throw 2")
        }
    }

    @Test
    fun throwDefaultWithoutTagHandler() {
        val engine = makeEngine()
        try {
            engine.parseAndEval(StringSourceLocation("throw \"foo\""))
            fail("An exception should have been thrown here")
        } catch (e: TagCatch) {
            val sym = e.tag.key.ensureSymbol().value
            assertSame(engine.internSymbol("error", engine.coreNamespace), sym)
            assertEquals("foo", e.tag.data.toStringValue())
        }
    }

    @Test
    fun stackTrace() {
        try {
            parseAPLExpression(
                """
                |∇ foo (x) {
                |  x[1;2]
                |}
                |
                |∇ bar (y) {
                |  foo y
                |}
                |
                |bar 1 2 3 4
                """.trimMargin())
            assertTrue(false, "Exception was expected")
        } catch (ex: APLEvalException) {
            val callStack = ex.stack
            assertNotNull(callStack)
            assertEquals(3, callStack.frames.size)
        }
    }
}
