package com.dhsdevelopments.kap

class ModuleInitSkipped(message: String? = null, cause: Throwable? = null) : Exception(message, cause)
class ModuleInitFailedException(message: String, cause: Throwable? = null) : Exception(message, cause)

interface KapModule {
    /**
     * The name of the module.
     */
    val name: String

    /**
     * A description of the module or `null` if no description is available. This description does not have to be constant.
     */
    val description: String? get() = null

    /**
     * Initialise the module.
     *
     * @throws ModuleInitSkipped if the module did not initialise, and it is safe to disable it and continue.
     * @throws ModuleInitFailedException if the module failed to initialise, and it is safe to disable it and continue. A warning will be logged when the engine is started.
     */
    @Throws(ModuleInitSkipped::class, ModuleInitFailedException::class)
    fun init(engine: Engine)

    /**
     * Called when the engine is shutting down.
     */
    fun close() {}
}

class ParameterInfo(val name: String, val description: String, val type: ParameterType, val required: Boolean) {
    enum class ParameterType { INT, STRING }
}

class ModuleBuilderInitFailedException(message: String, cause: Throwable? = null) : Exception(message, cause)

interface KapModuleBuilder {
    val name: String
    val description: String

    fun canBeInstantiated(): Boolean
    fun configurationParameters(): List<ParameterInfo>
    fun makeModuleWithParams(params: Map<String, Any>): KapModule
}

/**
 * Handler for the `modlist` command which displays a list of active modules.
 */
class ModListCommandHandler : NoArgCommandHandler() {
    override fun handleNoArgCommand(context: CommandContext, cmd: String) {
        val s = buildString {
            append("Active modules:\n")
            context.engine.modules.sortedBy(KapModule::name).forEach { mod ->
                append(mod.name)
                val desc = mod.description
                if (desc != null) {
                    append(": ")
                    append(desc)
                }
                append("\n")
            }
        }
        context.print(s)
    }

    override fun description() = "Display list of activated installed modules"
}
