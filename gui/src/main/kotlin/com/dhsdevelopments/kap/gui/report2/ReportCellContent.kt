package com.dhsdevelopments.kap.gui.report2

abstract class ReportCellContent {
    abstract fun cellContentString(): String
}

class LabelCellContent(val text: String) : ReportCellContent() {
    override fun cellContentString() = text
}

abstract class LinkedVariableCellContent(val owner: DisplayedValue) : ReportCellContent()

class FieldCellContent(owner: DisplayedValue, val text: String) : LinkedVariableCellContent(owner) {
    override fun cellContentString() = text
}

class HeaderCellContent(owner: DisplayedValue, val text: String) : LinkedVariableCellContent(owner) {
    override fun cellContentString() = text
}
