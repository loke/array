package com.dhsdevelopments.kap

import kotlin.test.Ignore
import kotlin.test.Test

class KeyTest : APLTest() {
    @Test
    fun simpleKeyTest() {
        parseAPLExpression("1 2 2 5 ⌸ \"a1\" \"b2\" \"c3\" \"d4\"", withStandardLib = true).let { result ->
            assertDimension(dimensionsOfSize(3, 2), result)
            assertSimpleNumber(1, result.valueAt(0))
            assert1DArray(arrayOf("a1"), result.valueAt(1))
            assertSimpleNumber(2, result.valueAt(2))
            assert1DArray(arrayOf("b2", "c3"), result.valueAt(3))
            assertSimpleNumber(5, result.valueAt(4))
            assert1DArray(arrayOf("d4"), result.valueAt(5))
        }
    }

    // Need to add a special check for this in the implementation
    @Ignore
    @Test
    fun emptyKeysAndValues() {
        parseAPLExpression("⍬⌸⍬", withStandardLib = true).let { result ->
            assertDimension(dimensionsOfSize(0, 2), result)
        }
    }

    @Test
    fun stringKeys() {
        parseAPLExpression("\"foo\" \"bar\" \"foo\" \"test\" \"qwer\" ⌸ \"a1\" \"b2\" \"c3\" \"d4\" \"e5\"", withStandardLib = true).let { result ->
            assertDimension(dimensionsOfSize(4, 2), result)
            assertString("foo", result.valueAt(0))
            assert1DArray(arrayOf("a1", "c3"), result.valueAt(1))
            assertString("bar", result.valueAt(2))
            assert1DArray(arrayOf("b2"), result.valueAt(3))
            assertString("test", result.valueAt(4))
            assert1DArray(arrayOf("d4"), result.valueAt(5))
            assertString("qwer", result.valueAt(6))
            assert1DArray(arrayOf("e5"), result.valueAt(7))
        }
    }
}
