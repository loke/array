package com.dhsdevelopments.kap

import kotlin.test.Test

class StandardLibMathTest : APLTest() {
    @Test
    fun testSimpleMatrixInverse() {
        parseAPLExpression("⌹ 5 5 ⍴ 1 0 0 0 0 0", true).let { result ->
            assertDimension(dimensionsOfSize(5, 5), result)
            assertArrayContentDouble(
                doubleArrayOf(
                    1.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 1.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 1.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 1.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 1.0), result)
        }
    }

    @Test
    fun testMatrixDivision() {
        parseAPLExpression("(4 4⍴12 1 4 10 ¯6 ¯5 4 7 ¯4 9 3 4 ¯2 ¯6 7 7)⌹93 81 93.5 120.5", true).let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertDoubleWithRange(Pair(0.00038988887, 0.00038988889), result.valueAt(0))
            assertDoubleWithRange(Pair(-0.0050295666, -0.0050295664), result.valueAt(1))
            assertDoubleWithRange(Pair(0.047306516, 0.047306518), result.valueAt(2))
            assertDoubleWithRange(Pair(0.07055688, 0.07055690), result.valueAt(3))
        }
    }

    @Test
    fun matrixInverseContrib() {
        parseAPLExpression("⌹3 3⍴1 2 3 4 15 16 7 18 9", withStandardLib = true).let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(
                arrayOf(
                    NearDouble(1.53), NearDouble(-0.36), NearDouble(0.13),
                    NearDouble(-0.76), NearDouble(0.12), NearDouble(0.04),
                    NearDouble(0.33), NearDouble(0.04), NearDouble(-0.07)),
                result)
        }
    }

    @Test
    fun matrixDivideIntToDouble0() {
        parseAPLExpression("9 8 7 ⌹ 2.0", withStandardLib = true).let { result ->
            assertSimpleDouble(12.0, result)
        }
    }

    @Test
    fun matrixDivideIntToDouble1() {
        parseAPLExpression("9.0 8.0 7.0 ⌹ 2", withStandardLib = true).let { result ->
            assertSimpleDouble(12.0, result)
        }
    }
}
