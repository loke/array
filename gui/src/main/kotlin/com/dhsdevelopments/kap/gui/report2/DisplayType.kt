package com.dhsdevelopments.kap.gui.report2

import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.Engine
import com.dhsdevelopments.kap.Symbol
import com.dhsdevelopments.kap.VariableHolder
import com.dhsdevelopments.kap.gui.arrayedit.ArrayEditor
import javafx.scene.Parent

interface DisplayType {
    val description: String
    fun makeConfigurator(client: ReportClient, selection: ArrayEditor.SelectedArea): DisplayConfigurator
}

interface DisplayConfigurator {
    fun settingsPanel(): Parent
    fun saveToResult(engine: Engine, storage: VariableHolder, value: APLValue, sym: Symbol)
}
