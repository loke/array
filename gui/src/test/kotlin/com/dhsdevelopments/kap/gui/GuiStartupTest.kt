package com.dhsdevelopments.kap.gui

import com.dhsdevelopments.kap.gui.styledarea.ROStyledArea
import com.dhsdevelopments.kap.repl.DEFAULT_PROMPT_CHAR
import javafx.scene.Node
import javafx.stage.Stage
import org.testfx.api.FxAssert
import org.testfx.matcher.base.GeneralMatchers
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals

@Ignore
class GuiStartupTest : KotlinUiApplicationTest() {
    protected var client: Client? = null

    override fun start(stage: Stage) {
        val builder = FxClientGenericReplBuilder()
        val repl = builder.build(emptyArray())
        requireNotNull(repl) { "Failed to create repl" }
        val services = object : ApplicationServices {
            override fun showDocument(url: String) {}
        }
        Client(services, stage, repl)
    }

    @Test
    fun applicationStart() {
        clickOn("#fileMenu")
        clickOn("#closeMenuAction")
    }

    @Test
    fun simpleKapExpression() {
        val expr = "5+7"
        write("${expr}\n")
        verifyNodePredicate<ROStyledArea>("#replPanel") { styledArea ->
            val pos = styledArea.findInputStartEnd()
            val text = styledArea.document.subSequence(0, pos.promptStartPos).text
            text.endsWith("12\n")
        }
        verifyNodePredicate<ROStyledArea>("#replPanel") { styledArea ->
            val pos = styledArea.findInputStartEnd()
            assertEquals("${DEFAULT_PROMPT_CHAR} ", styledArea.document.subSequence(pos.promptStartPos, pos.inputStart).text)
            assertEquals(expr, styledArea.document.subSequence(pos.inputStart, pos.inputEnd).text)
            true
        }
    }

    protected inline fun <reified T : Node> verifyNodePredicate(id: String, crossinline fn: (T) -> Boolean) {
        val matcher = GeneralMatchers.baseMatcher("replPanel should contain 6") { node: Node ->
            val node0 = node as T
            fn(node0)
        }
        FxAssert.verifyThat(id, matcher)
    }
}
