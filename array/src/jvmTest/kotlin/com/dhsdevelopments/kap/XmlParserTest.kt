package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.jvmmod.JvmInstanceValue
import org.w3c.dom.Document
import org.w3c.dom.Node
import kotlin.test.*

class XmlParserTest : APLTest() {
    @Test
    fun simpleParseString() {
        val src =
            """
            |use("xml.kap")
            |xml ← "<foo><bar>xyz</bar><bar>testing</bar></foo>"
            |xml:readString xml
            """.trimMargin()
        parseAPLExpression(src, withStandardLib = true).let { result ->
            assertIs<JvmInstanceValue>(result)
            assertIs<Document>(result.instance)
            assertEquals("foo", result.instance.childNodes.item(0).nodeName)
        }
    }

    @Test
    fun simpleParseFile() {
        val src =
            """
            |use("xml.kap")
            |xml:readFile "test-data/foo.xml"
            """.trimMargin()
        parseAPLExpression(src, withStandardLib = true).let { result ->
            assertIs<JvmInstanceValue>(result)
            assertIs<Document>(result.instance)
            assertEquals("foo", result.instance.childNodes.item(0).nodeName)
        }
    }

    @Test
    fun nodeTypes() {
        val src =
            """
            |use("xml.kap")
            |doc ← xml:readFile "test-data/foo.xml"
            |children ← xml:childNodes doc
            |root ← children.(0)
            |rootChildren ← xml:childNodes root
            |(xml:nodeType doc) , (xml:nodeType root) , {xml:nodeType rootChildren.(⍵)}¨ ⍳4
            """.trimMargin()
        parseAPLExpression2(src, withStandardLib = true).let { (result, engine) ->
            assertDimension(dimensionsOfSize(6), result)
            val ns = engine.findNamespace("xml")
            assertNotNull(ns)

            fun assertSym(name: String, value: APLValue) {
                val sym = ns.findSymbol(name, true)
                assertNotNull(sym)
                assertIs<APLSymbol>(value)
                assertSame(sym, value.value)
            }

            assertSym("document", result.valueAt(0))
            assertSym("element", result.valueAt(1))
            assertSym("text", result.valueAt(2))
            assertSym("comment", result.valueAt(3))
            assertSym("text", result.valueAt(4))
            assertSym("element", result.valueAt(5))
        }
    }

    @Test
    fun testAttributes() {
        val src =
            """
            |use("xml.kap")
            |doc ← xml:readFile "test-data/foo.xml"
            |children ← xml:childNodes doc
            |root ← children.(0)
            |rootChildren ← xml:childNodes root
            |xml:attributes rootChildren.(3)
            """.trimMargin()
        parseAPLExpression(src, withStandardLib = true).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf("a", "123", "b", "some text"), result)
        }
    }

    @Test
    fun testNodeText() {
        val src =
            """
            |use("xml.kap")
            |doc ← xml:readFile "test-data/foo.xml"
            |children ← xml:childNodes doc
            |root ← ↑children
            |rootChildren ← xml:childNodes root
            |xml:nodeText ↑xml:childNodes rootChildren.(3)
            """.trimMargin()
        parseAPLExpression(src, withStandardLib = true).let { result ->
            assertString(
                "\n" +
                        "        Some text here\n" +
                        "        ",
                result)
        }
    }

    @Test
    fun testXpathSimpleStringResult() {
        val src =
            """
            |use("xml.kap")
            |doc ← xml:readFile "test-data/foo.xml"
            |xml:xpathEvalToString ("/foo/bar"; doc)
            """.trimMargin()
        parseAPLExpression(src, withStandardLib = true).let { result ->
            assertString(
                "\n" +
                        "        Some text here\n" +
                        "        \n            \n                \n            \n        \n    ",
                result)
        }
    }

    @Test
    fun testXpathNoResultStringResult() {
        val src =
            """
            |use("xml.kap")
            |doc ← xml:readFile "test-data/foo.xml"
            |xml:xpathEvalToString ("/foo/barfoo"; doc)
            """.trimMargin()
        parseAPLExpression(src, withStandardLib = true).let { result ->
            assertString("", result)
        }
    }

    @Test
    fun testXpathSimpleNodeResult() {
        val src =
            """
            |use("xml.kap")
            |doc ← xml:readFile "test-data/foo.xml"
            |xml:xpathEvalToNode ("/foo/ghi"; doc)
            """.trimMargin()
        parseAPLExpression(src, withStandardLib = true).let { result ->
            assertIs<JvmInstanceValue>(result)
            assertIs<Node>(result.instance)
            assertEquals("ghi", result.instance.nodeName)
            assertEquals("test0", result.instance.attributes.getNamedItem("a").nodeValue)
        }
    }

    @Test
    fun testXpathSimpleNodeListResult() {
        val src =
            """
            |use("xml.kap")
            |doc ← xml:readFile "test-data/foo.xml"
            |xml:xpathEvalToNodeList ("/foo/ghi"; doc)
            """.trimMargin()
        parseAPLExpression(src, withStandardLib = true).let { result ->
            assertDimension(dimensionsOfSize(2), result)
            result.valueAt(0).let { v ->
                assertIs<JvmInstanceValue>(v)
                assertIs<Node>(v.instance)
                assertEquals("ghi", v.instance.nodeName)
                assertEquals("test0", v.instance.attributes.getNamedItem("a").nodeValue)
            }
            result.valueAt(1).let { v ->
                assertIs<JvmInstanceValue>(v)
                assertIs<Node>(v.instance)
                assertEquals("ghi", v.instance.nodeName)
                assertEquals("test1", v.instance.attributes.getNamedItem("a").nodeValue)
            }
        }
    }
}
