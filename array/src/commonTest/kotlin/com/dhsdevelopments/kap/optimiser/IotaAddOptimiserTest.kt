package com.dhsdevelopments.kap.optimiser

import com.dhsdevelopments.kap.APLTest
import com.dhsdevelopments.kap.ConstantValueInstruction
import com.dhsdevelopments.kap.StringSourceLocation
import com.dhsdevelopments.kap.builtins.IotaArrayImpls
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs

class IotaAddOptimiserTest : APLTest() {
    @Test
    fun simpleIotaAddCheckResult() {
        withAllOptimisers {
            parseAPLExpression("4+⍳6").let { result ->
                assert1DArray(arrayOf(4, 5, 6, 7, 8, 9), result)
            }
        }
    }

    @Test
    fun simpleIotaAddOptimiser() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("10+⍳3"))
        assertIs<ConstantValueInstruction<IotaArrayImpls.ResizedIotaArrayLong>>(instr)
        assertEquals(3, instr.valueInt.width)
        assertEquals(10, instr.valueInt.offset)
    }

    @Test
    fun simpleIotaAddReverseOrderCheckResult() {
        withAllOptimisers {
            parseAPLExpression("(⍳6)+4").let { result ->
                assert1DArray(arrayOf(4, 5, 6, 7, 8, 9), result)
            }
        }
    }

    @Test
    fun simpleIotaAddOptimiserReverseOrder() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(⍳3)+10"))
        assertIs<ConstantValueInstruction<IotaArrayImpls.ResizedIotaArrayLong>>(instr)
        assertEquals(3, instr.valueInt.width)
        assertEquals(10, instr.valueInt.offset)
    }

    @Test
    fun simpleIotaSubtractReverseOrderCheckResult() {
        withAllOptimisers {
            parseAPLExpression("(⍳6)-4").let { result ->
                assert1DArray(arrayOf(-4, -3, -2, -1, 0, 1), result)
            }
        }
    }

    @Test
    fun simpleIotaSubtractOptimiserReverseOrder() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(⍳6)-4"))
        assertIs<ConstantValueInstruction<IotaArrayImpls.ResizedIotaArrayLong>>(instr)
        assertEquals(6, instr.valueInt.width)
        assertEquals(-4, instr.valueInt.offset)
    }

    @Test
    fun subtractIotaFromConstant() {
        withAllOptimisers {
            parseAPLExpression("3-⍳3").let { result ->
                assert1DArray(arrayOf(3, 2, 1), result)
            }
        }
    }

    /**
     * Test range that overflows in the middle: from 2^63-2 to 2^63+1
     */
    @Test
    fun longOverflow0() {
        withAllOptimisers {
            parseAPLExpression("9223372036854775806 + ⍳4").let { result ->
                assert1DArray(
                    arrayOf(
                        InnerBigIntOrLong("9223372036854775806"),
                        InnerBigIntOrLong("9223372036854775807"),
                        InnerBigIntOrLong("9223372036854775808"),
                        InnerBigIntOrLong("9223372036854775809")),
                    result)
            }
        }
    }

    /**
     * Test range where all elements overflows: from 2^65+10 to 2^65+13
     */
    @Test
    fun longOverflow1() {
        withAllOptimisers {
            parseAPLExpression("36893488147419103242 + ⍳4").let { result ->
                assert1DArray(
                    arrayOf(
                        InnerBigIntOrLong("36893488147419103242"),
                        InnerBigIntOrLong("36893488147419103243"),
                        InnerBigIntOrLong("36893488147419103244"),
                        InnerBigIntOrLong("36893488147419103245")),
                    result)
            }
        }
    }

    /**
     * Test range of a single element just at the far end of the range: 2^63-1
     */
    @Test
    fun longNoOverflowSingleElement() {
        withAllOptimisers {
            parseAPLExpression("9223372036854775807 + ⍳1").let { result ->
                assert1DArray(arrayOf(9223372036854775807L), result)
            }
        }
    }

    @Test
    fun longNoOverflowSingleElementIsOptimised() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("9223372036854775807 + ⍳1"))
        assertIs<ConstantValueInstruction<IotaArrayImpls.ResizedIotaArrayLong>>(instr)
        assertEquals(1, instr.valueInt.width)
        assertEquals(9223372036854775807L, instr.valueInt.offset)
    }

    @Test
    fun offset0IsOptimised() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("0 + ⍳10"))
        assertIs<ConstantValueInstruction<IotaArrayImpls.IotaArrayLong>>(instr)
        assertEquals(10, instr.valueInt.length)

    }

    /**
     * Test range that underflows in the middle: from -2^63+1 to -2^63-2
     */
    @Test
    fun longUnderflow0() {
        parseAPLExpression("¯9223372036854775807 + ⍳4").let { result ->
            assert1DArray(
                arrayOf(
                    InnerBigIntOrLong("-9223372036854775807"),
                    InnerBigIntOrLong("-9223372036854775806"),
                    InnerBigIntOrLong("-9223372036854775805"),
                    InnerBigIntOrLong("-9223372036854775804")),
                result)
        }
    }

    /**
     * Test range that underflows: from -2^70 to -2^70+3
     */
    @Test
    fun longUnderflow1() {
        parseAPLExpression("¯1180591620717411303424 + ⍳4").let { result ->
            assert1DArray(
                arrayOf(
                    InnerBigIntOrLong("-1180591620717411303424"),
                    InnerBigIntOrLong("-1180591620717411303423"),
                    InnerBigIntOrLong("-1180591620717411303422"),
                    InnerBigIntOrLong("-1180591620717411303421")),
                result)
        }
    }

    @Test
    fun addToNullArray() {
        withAllOptimisers {
            assertAPLNull(parseAPLExpression("0 + ⍳0"))
            assertAPLNull(parseAPLExpression("0.0 + ⍳0"))
            assertAPLNull(parseAPLExpression("3 + ⍳0"))
            assertAPLNull(parseAPLExpression("3.0 + ⍳0"))
            assertAPLNull(parseAPLExpression("¯1234 + ⍳0"))
            assertAPLNull(parseAPLExpression("¯1234.0 + ⍳0"))
            assertAPLNull(parseAPLExpression("1000000000000000000000000000000000 + ⍳0"))
            assertAPLNull(parseAPLExpression("¯10000000000000000000000000000000000000 + ⍳0"))
        }
    }
}
