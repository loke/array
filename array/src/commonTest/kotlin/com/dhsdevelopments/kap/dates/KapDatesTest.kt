package com.dhsdevelopments.kap.dates

import com.dhsdevelopments.kap.APLTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs

class KapDatesTest : APLTest() {
    @Test
    fun convertToTimestamp() {
        parseAPLExpression("time:toTimestamp 1654321234599").let { result ->
            assertIs<APLTimestamp>(result)
            assertEquals(1654321234599L, result.time.toEpochMilliseconds())
        }
    }

    @Test
    fun convertFromTimestamp() {
        parseAPLExpression("time:fromTimestamp time:toTimestamp 1654321234599").let { result ->
            assertSimpleNumber(1654321234599L, result)
        }
    }

    @Test
    fun formatTimestamp() {
        parseAPLExpression("time:format time:toTimestamp 1654355533333").let { result ->
            assertString("2022-06-04T15:12:13.333Z", result)
        }
    }

    @Test
    fun parseTimestamp() {
        parseAPLExpression("time:parse \"2022-06-04T15:12:13.333Z\"").let { result ->
            assertIs<APLTimestamp>(result)
            assertEquals(1654355533333L, result.time.toEpochMilliseconds())
        }
    }

    private fun compareDates(sym: String, eq: Boolean, lowToHigh: Boolean, highToLow: Boolean) {
        assertSimpleNumber(
            if (eq) 1 else 0,
            parseAPLExpression("(time:parse \"2022-06-04T15:12:13.333Z\") ${sym} time:parse \"2022-06-04T15:12:13.333Z\""))
        assertSimpleNumber(
            if (lowToHigh) 1 else 0,
            parseAPLExpression("(time:parse \"2022-03-04T15:12:13.333Z\") ${sym} time:parse \"2022-06-04T15:12:13.333Z\""))
        assertSimpleNumber(
            if (highToLow) 1 else 0,
            parseAPLExpression("(time:parse \"2022-06-04T15:12:13.333Z\") ${sym} time:parse \"2022-03-04T15:12:13.333Z\""))
    }

    @Test
    fun compareIdentityDates() {
        compareDates("≡", true, false, false)
        compareDates("≢", false, true, true)
        assertSimpleNumber(0, parseAPLExpression("(time:parse \"2022-06-04T15:12:13.333Z\") ≡ 12345"))
        assertSimpleNumber(0, parseAPLExpression("12345 ≡ time:parse \"2022-06-04T15:12:13.333Z\""))
        assertSimpleNumber(1, parseAPLExpression("(time:parse \"2022-06-04T15:12:13.333Z\") ≢ 12345"))
        assertSimpleNumber(1, parseAPLExpression("12345 ≢ time:parse \"2022-06-04T15:12:13.333Z\""))
    }

    @Test
    fun compareEqualsDates() {
        compareDates("=", true, false, false)
        compareDates("≠", false, true, true)
        assertSimpleNumber(0, parseAPLExpression("(time:parse \"2022-06-04T15:12:13.333Z\") = 12345"))
        assertSimpleNumber(0, parseAPLExpression("12345 = time:parse \"2022-06-04T15:12:13.333Z\""))
        assertSimpleNumber(1, parseAPLExpression("(time:parse \"2022-06-04T15:12:13.333Z\") ≠ 12345"))
        assertSimpleNumber(1, parseAPLExpression("12345 ≠ time:parse \"2022-06-04T15:12:13.333Z\""))
    }

    @Test
    fun compareDatesLessThan() {
        compareDates("<", false, true, false)
    }

    @Test
    fun compareDatesLessThanOrEqual() {
        compareDates("≤", true, true, false)
    }

    @Test
    fun compareDatesGreaterThan() {
        compareDates(">", false, false, true)
    }

    @Test
    fun compareDatesGreaterThanOrEqual() {
        compareDates("≥", true, false, true)
    }
}
