package com.dhsdevelopments.kap.optimiser

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.ConcatenateAPLFunctionLastAxis.ConcatenateAPLFunctionLastAxisImpl
import com.dhsdevelopments.kap.builtins.EncloseAPLFunction
import com.dhsdevelopments.kap.builtins.ResizedArrayImpls

internal class OptimisedConstantListInstruction(override val valueInt: APLValue, pos: Position) : LiteralAPLValueInstruction(pos) {
    override fun evalWithContext(context: RuntimeContext) = valueInt
    override fun children(): List<Instruction> = emptyList()
    override fun copy(updatedChildList: List<Instruction>, newPos: Position) = this
}

//object EnclosedValueToLiteralAPLValueOptimiser : InstructionOptimiser {
//    override fun attemptOptimise(instr: Instruction): Instruction? {
//        if (instr !is FunctionCall1Arg) return null
//        if (instr.fn !is EncloseAPLFunction.EncloseAPLFunctionImpl) return null
//        val args = instr.rightArgs
//        if (args !is LiteralAPLValueInstruction) return null
//        val value = args.valueInt
//        return Optimiser.makeConstantInstructionFromValue(EnclosedAPLValue.make(value), args.pos)
//    }
//}

object SingleElementConstantListOptimiser : InstructionOptimiser {
    override fun attemptOptimise(instr: Instruction): Instruction? {
        if (instr !is FunctionCall1Arg) return null
        if (instr.fn !is ConcatenateAPLFunctionLastAxisImpl) return null
        return when (val args = instr.rightArgs) {
            is FunctionCall1Arg -> {
                if (args.fn !is EncloseAPLFunction.EncloseAPLFunctionImpl) return null
                if (args.rightArgs !is LiteralAPLValueInstruction) return null
                OptimisedConstantListInstruction(ResizedArrayImpls.resizedSingleValue(dimensionsOfSize(1), args.rightArgs.valueInt), args.rightArgs.pos)
            }
            is LiteralAPLValueInstruction -> {
                val value = args.valueInt
                if (value.dimensions.size != 0) return null
                val disclosedValue = value.disclose()
                val res = ResizedArrayImpls.resizedSingleValue(dimensionsOfSize(1), disclosedValue)
                OptimisedConstantListInstruction(res, args.pos)
            }
            else -> null
        }
    }
}
