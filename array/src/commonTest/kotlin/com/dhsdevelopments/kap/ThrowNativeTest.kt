package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs

class ThrowNativeTest : APLTest() {
    @Test
    fun testThrowInvalidDimensionsException() {
        try {
            parseAPLExpression("'kap:InvalidDimensionsException int:throwNative \"abcd\"")
        } catch (e: InvalidDimensionsException) {
            assertIs<InvalidDimensionsException>(e)
            assertEquals("abcd", e.message)
        }
    }
}
