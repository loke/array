package com.dhsdevelopments.kap.gui.msoffice

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.dates.APLTimestamp
import kotlinx.datetime.Instant
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.io.ByteArrayInputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.XMLConstants
import javax.xml.parsers.DocumentBuilderFactory

/*
  The Excel XML parser is adapted from the following project: https://github.com/Maxoudela/XMLSpreadsheetParser/
  That project was implemented by Samir Hadzic, and it is distributed under the MIT licence
  and contains the following disclaimer:

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
 */

class ReadExcelXMLFileParseError(message: String, cause: Throwable? = null) : Exception(message, cause)

@Throws(ReadExcelXMLFileParseError::class)
fun readExcelXMLFileFromByteBuffer(buffer: ByteBuffer): APLValue {
    val fac = DocumentBuilderFactory.newInstance()
    fac.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true)
    fac.isExpandEntityReferences = false
    fac.isNamespaceAware = true

    val array = buffer.array()
    val indexOfZero = array.indexOf(0)
    val subArray = if (indexOfZero == -1) array else array.copyOfRange(0, indexOfZero)

    val doc = ByteArrayInputStream(subArray).use { inStream ->
        fac.newDocumentBuilder().parse(inStream)
    }

    val excelClipboardParser = ClipBoardXML(doc)
    try {
        excelClipboardParser.parse()
    } catch (e: IOException) {
        throw ReadExcelXMLFileParseError("IO exception when parsing excel content", e)
    }
    val res = excelClipboardParser.result
    if (res.isEmpty()) {
        return APLArrayImpl(dimensionsOfSize(0, 0), emptyArray())
    } else {
        val list = ArrayList<APLValue>()
        val width = res[0].size
        res.forEach { row ->
            if (row.size != width) {
                throw ReadExcelXMLFileParseError("Unexpected width in parse result")
            }
            list.addAll(row)
        }
        return APLArrayList(dimensionsOfSize(res.size, width), list)
    }
}

private class ClipBoardXML(private val doc: Document) {
    private val baseRow = 0
    private val baseColumn = 0

    private var currentRow: Int = 0
    private var currentColumn: Int = 0
    private var oldRow = 0
    private var newRow = 0

    private var oldCol = 0
    private var newCol = 0

    private var selectionRowCount = 0
    private var selectionColumnCount = 0

    private val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")

    /**
     * We will flag to true each cell we have considered. All others will be
     * deleted.
     */
    private var cellsUsed: MutableMap<Int, BitSet>? = null

    @Throws(ParseException::class)
    fun parse() {
        retrieveTableInfo()

        val listRows = doc.getElementsByTagName(ROW_TAG)
        for (temp in 0 until listRows.length) {
            val row = listRows.item(temp)
            if (row.nodeType == Node.ELEMENT_NODE) {
                val rowEl = row as Element
                handleRow(rowEl)
            }
        }
        erase()
    }

    val result = ArrayList<MutableList<APLValue>>()

    fun eraseValue(rowIndex: Int, colIndex: Int) {
        handleValue(rowIndex, colIndex, APLLONG_0)
    }

    fun handleValue(rowIndex: Int, colIndex: Int, value: APLValue) {
        val row = if (rowIndex >= result.size) {
            repeat(result.size - rowIndex) {
                result.add(ArrayList<APLValue>())
            }
            ArrayList<APLValue>().also { v ->
                result.add(v)
            }
        } else {
            result[rowIndex]
        }
        if (colIndex >= row.size) {
            repeat(colIndex - row.size) {
                row.add(APLLONG_0)
            }
            row.add(value)
        } else {
            row[colIndex] = value
        }
    }

    private fun erase() {
        if (cellsUsed == null) {
            return
        }
        var row = 0
        var consideredRow: Int
        var consideredColumn: Int
        while (row < selectionRowCount) {
            var column = 0
            consideredRow = row + baseRow
            if (cellsUsed!!.containsKey(row)) {
                val bitSet = cellsUsed!![row]
                while (column < selectionColumnCount) {
                    consideredColumn = column + baseColumn
                    if (!bitSet!![column]) {
                        eraseValue(consideredRow, consideredColumn)
                    }
                    ++column
                }
            } else {
                while (column < selectionColumnCount) {
                    consideredColumn = column + baseColumn
                    eraseValue(consideredRow, consideredColumn)
                    ++column
                }
            }
            ++row
        }
    }

    private fun set(row: Int, column: Int) {
        if (cellsUsed == null) {
            return
        }
        if (!cellsUsed!!.containsKey(row)) {
            cellsUsed!![row] = BitSet()
        }
        cellsUsed!![row]!!.set(column)
    }

    @Throws(ParseException::class)
    private fun handleRow(rowEl: Element) {
        newRow = getIndex(rowEl, newRow)

        //We adjust the currentRow by the difference between the old and the new row.
        currentRow += newRow - oldRow - 1
        rowEl.hasAttributeNS(SPREADSHEET_NS_NAME, INDEX_TAG)
        val cells = rowEl.getElementsByTagName(CELL_TAG)
        handleColumns(cells)

        currentColumn = baseColumn
        currentRow++
        oldRow = newRow
        //Reset columns
        newCol = 0
        oldCol = 0
    }

    @Throws(ParseException::class)
    private fun handleColumns(cells: NodeList) {
        for (column in 0 until cells.length) {
            val cell = cells.item(column)
            if (cell.nodeType == Node.ELEMENT_NODE) {
                val cellEl = cell as Element
                newCol = getIndex(cellEl, newCol)
                currentColumn += newCol - oldCol - 1
                handleCell(currentRow, currentColumn, cellEl)
                currentColumn++
                handleSpan(cellEl)
            }

            oldCol = newCol
        }
    }

    private fun handleSpan(cellEl: Element) {
        var columnSpan = 0
        if (cellEl.hasAttributeNS(SPREADSHEET_NS_NAME, COLUMN_SPAN_TAG)) {
            columnSpan = cellEl.getAttributeNS(SPREADSHEET_NS_NAME, COLUMN_SPAN_TAG).toInt()
            for (i in 0 until columnSpan) {
                set(newRow - 1, newCol + i)
            }
        }

        if (cellEl.hasAttributeNS(SPREADSHEET_NS_NAME, ROW_SPAN_TAG)) {
            val rowSpan = cellEl.getAttributeNS(SPREADSHEET_NS_NAME, ROW_SPAN_TAG).toInt()
            for (i in 0 until rowSpan) {
                set(newRow + i, newCol - 1)
            }
        }
        currentColumn += columnSpan
        newCol += columnSpan
    }

    private fun retrieveTableInfo() {
        val tableList = doc.getElementsByTagName(TABLE_TAG)

        //Retrieve possible row and column count of Excel selection.
        if (tableList.length == 1 && tableList.item(0).nodeType == Node.ELEMENT_NODE) {
            val tableEl = tableList.item(0) as Element
            if (tableEl.hasAttributeNS(SPREADSHEET_NS_NAME, ROW_COUNT_TAG)) {
                selectionRowCount = tableEl.getAttributeNS(SPREADSHEET_NS_NAME, ROW_COUNT_TAG).toInt()
            }
            if (tableEl.hasAttributeNS(SPREADSHEET_NS_NAME, COLUMN_COUNT_TAG)) {
                selectionColumnCount = tableEl.getAttributeNS(SPREADSHEET_NS_NAME, COLUMN_COUNT_TAG).toInt()
            }
            //Initialize array if both values are respected.
            if (selectionRowCount != 0 && selectionColumnCount != 0) {
                cellsUsed = HashMap()
            }
        }
    }

    /**
     * If the Element has no index specified, it means it's just one after the
     * previous. If it has an index, it's the absolute index relative to the
     * first index of the first row.
     *
     * @param rowEl
     * @param index
     * @return
     */
    private fun getIndex(rowEl: Element, index: Int): Int {
        return if (rowEl.hasAttributeNS(SPREADSHEET_NS_NAME, INDEX_TAG)) rowEl.getAttributeNS(SPREADSHEET_NS_NAME, INDEX_TAG).toInt() else index + 1
    }

    @Throws(ParseException::class)
    private fun handleCell(currentRow: Int, currentColumn: Int, cellEl: Element) {
        val nodeList = cellEl.getElementsByTagName(DATA_TAG)
        if (nodeList.length == 1 && nodeList.item(0).nodeType == Node.ELEMENT_NODE) {
            //We must only set the cell if we have a valid data. We can have a cell with only style and no data.
            set(newRow - 1, newCol - 1)
            val data = nodeList.item(0) as Element
            val cellType = data.getAttributeNS(SPREADSHEET_NS_NAME, TYPE_TAG)
            val obj = when (cellType) {
                NUMBER_TYPE_TAG -> {
                    data.textContent.toDouble().makeAPLNumber()
                }
                STRING_TYPE_TAG -> {
                    APLString.make(data.textContent)
                }
                DATE_TYPE_TAG -> {
                    val date = dateFormat.parse(data.textContent)
                    APLTimestamp(Instant.fromEpochMilliseconds(date.toInstant().toEpochMilli()))
                }
                else -> {
                    APLString.make(data.textContent)
                }
            }
            handleValue(currentRow, currentColumn, obj)
        }
    }

    companion object {
        private const val SPREADSHEET_NS_NAME = "urn:schemas-microsoft-com:office:spreadsheet"
        private const val ROW_TAG = "Row"
        private const val CELL_TAG = "Cell"
        private const val DATA_TAG = "Data"
        private const val TYPE_TAG = "Type"
        private const val DATE_TYPE_TAG = "DateTime"
        private const val NUMBER_TYPE_TAG = "Number"
        private const val STRING_TYPE_TAG = "String"
        private const val INDEX_TAG = "Index"
        private const val ROW_COUNT_TAG = "ExpandedRowCount"
        private const val COLUMN_COUNT_TAG = "ExpandedColumnCount"
        private const val TABLE_TAG = "Table"
        private const val COLUMN_SPAN_TAG = "MergeAcross"
        private const val ROW_SPAN_TAG = "MergeDown"
    }
}
