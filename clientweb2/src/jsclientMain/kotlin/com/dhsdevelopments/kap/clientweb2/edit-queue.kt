package com.dhsdevelopments.kap.clientweb2

import kotlinx.browser.document
import org.w3c.dom.Worker

private val outstandingEngineRequests = ArrayList<OutstandingEngineRequestInfo>()

private class OutstandingEngineRequestInfo(
    val worker: Worker,
    val data: dynamic,
    val callback: (Any?) -> Unit
) {
    fun start() {
        println("Requesting data from engine, name=${data.messageType}, reqId=${data.requestId}")
        worker.postMessage(data)
    }
}

fun sendEngineRequest(worker: Worker, data: dynamic, callback: (dynamic) -> Unit) {
    val req = OutstandingEngineRequestInfo(worker, data) { result ->
        callback(result)
    }
    val isEmpty = outstandingEngineRequests.isEmpty()
    outstandingEngineRequests.add(req)
    if (isEmpty) {
        req.start()
    }
}

private fun findAndRemoveOutstandingRequest(requestId: String): OutstandingEngineRequestInfo {
    require(outstandingEngineRequests.isNotEmpty()) { "Got response with no outstanding requests" }
    for (i in (outstandingEngineRequests.size - 1)..0) {
        val req = outstandingEngineRequests[i]
        if (req.data.requestId == requestId) {
            outstandingEngineRequests.removeAt(i)
            return req
        }
    }
    error("Request is not found in outstanding requests: ${requestId}")
}

fun processWorkerResponse(response: dynamic) {
    val req = findAndRemoveOutstandingRequest(response.requestId)
    document.asDynamic().xx = response
    req.callback(response.data)
    if (outstandingEngineRequests.isNotEmpty()) {
        outstandingEngineRequests.last().start()
    }
}
