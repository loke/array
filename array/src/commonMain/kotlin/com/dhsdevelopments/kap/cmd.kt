package com.dhsdevelopments.kap

abstract class CommandException(message: String, cause: Throwable? = null) : Exception(message, cause)
class CommandNotAvailableException(val cmd: String) : CommandException("Command not available: '${cmd}'")
class CommandExecutionFailedException(message: String, cause: Throwable? = null) : CommandException(message, cause)

class CommandManager(val engine: Engine) {
    private val registeredCommands = HashMap<String, CommandHandler>()

    init {
        registerCommandHandler("help", HelpCommandHandler())
    }

    fun registerCommandHandler(command: String, handler: CommandHandler, allowReplace: Boolean = false) {
        if (!allowReplace && registeredCommands.containsKey(command)) {
            throw IllegalStateException("Command already registered: '${command}'")
        }
        registeredCommands[command] = handler
    }

    @Throws(CommandException::class)
    fun processCommandString(cmd: String) {
        val trimmed = cmd.trim()
        val i = trimmed.indexOf(' ')
        val (cmd, args) = if (i == -1) {
            Pair(trimmed, "")
        } else {
            Pair(trimmed.substring(0, i), trimmed.substring(i).trimStart())
        }
        val handler = registeredCommands[cmd] ?: throw CommandNotAvailableException(cmd)
        handler.handleCommand(CommandContextImpl(engine), cmd, args)
    }

    fun processPrefix(s: String): String? {
        return if (s.startsWith(COMMAND_PREFIX)) {
            s.substring(COMMAND_PREFIX.length)
        } else {
            null
        }
    }

    fun registerQuitHandler(fn: () -> Unit) {
        registerCommandHandler("quit", QuitCommandHandler(fn))
    }

    companion object {
        const val COMMAND_PREFIX = "]"
    }

    private inner class HelpCommandHandler() : NoArgCommandHandler() {
        override fun handleNoArgCommand(context: CommandContext, cmd: String) {
            val s = buildString {
                append("Available commands:\n")
                val cmdList = registeredCommands.entries.sortedBy { (cmd, _) -> cmd }
                val w = cmdList.maxValueBy { (cmd, _) -> cmd.length } + 1
                cmdList.forEach { (cmd, handler) ->
                    append(cmd)
                    append(":")
                    repeat(w - cmd.length) {
                        append(' ')
                    }
                    append(handler.description())
                    append('\n')
                }
            }
            context.print(s)
        }

        override fun description() = "Display help"
    }

    private inner class QuitCommandHandler(val fn: () -> Unit) : NoArgCommandHandler() {
        override fun handleNoArgCommand(context: CommandContext, cmd: String) {
            fn()
        }

        override fun description() = "Quit the Kap interpreter"
    }
}

interface CommandContext {
    val engine: Engine
    fun print(s: String)
}

class CommandContextImpl(override val engine: Engine) : CommandContext {
    override fun print(s: String) {
        engine.standardOutput.writeString(s)
    }
}

interface CommandHandler {
    fun handleCommand(context: CommandContext, cmd: String, args: String)
    fun description(): String
}

abstract class NoArgCommandHandler : CommandHandler {
    override fun handleCommand(context: CommandContext, cmd: String, args: String) {
        if (args.isNotBlank()) {
            throw CommandExecutionFailedException("Command does not accept parameters")
        }
        handleNoArgCommand(context, cmd)
    }

    abstract fun handleNoArgCommand(context: CommandContext, cmd: String)
}

abstract class NumArgsCheckedCommandHandler(val minArgs: Int, val maxArgs: Int? = null) : CommandHandler {
    override fun handleCommand(context: CommandContext, cmd: String, args: String) {
        val parts = args.split(" ").filter(String::isNotEmpty)
        if (parts.size < minArgs || (maxArgs != null && parts.size > maxArgs)) {
            throw CommandExecutionFailedException("Command requires ${minArgs}${if (maxArgs != null) "-${maxArgs}" else ""}, got: ${parts.size}")
        }
        handleNArgsCommand(context, cmd, parts.map(String::trim))
    }

    abstract fun handleNArgsCommand(context: CommandContext, cmd: String, args: List<String>)
}

class RegisterCmdFunction : APLFunctionDescriptor {
    class RegisterCmdFunctionImpl(pos: FunctionInstantiation) : MultiArgumentAPLFunction(3, 3, pos) {
        override fun evalMultiArgument(context: RuntimeContext, args: List<APLValue>): APLValue {
            val cmdname = args[0].toStringValue(pos)
            if (!cmdname.matches("^[a-zA-Z_][a-zA-Z0-9_]*$".toRegex())) {
                throwAPLException(APLIllegalArgumentException("Invalid command name: '${cmdname}'", pos))
            }
            val description = args[1].toStringValue(pos)
            val handler = LambdaValue.fromValue(args[2], pos).makeClosure()
            val cmdHandler = object : CommandHandler {
                override fun handleCommand(context: CommandContext, cmd: String, args: String) {
                    context.engine.withThreadLocalAssigned {
                        val runtimeContext = RuntimeContext(context.engine)
                        try {
                            handler.eval1Arg(runtimeContext, APLString.make(args), null)
                        } catch (e: APLEvalException) {
                            throw CommandExecutionFailedException("Exception from custom command: ${e.message}", e)
                        }
                    }
                }

                override fun description() = description
            }
            context.engine.commandManager.registerCommandHandler(cmdname, cmdHandler, allowReplace = true)
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = RegisterCmdFunctionImpl(instantiation)
}
