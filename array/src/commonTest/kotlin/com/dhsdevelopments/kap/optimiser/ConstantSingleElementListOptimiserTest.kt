package com.dhsdevelopments.kap.optimiser

import com.dhsdevelopments.kap.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs

class ConstantSingleElementListOptimiserTest : APLTest() {
    @Test
    fun simpleSingleElementLong() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(,1)"))
        assertIs<OptimisedConstantListInstruction>(instr)
        assertDimension(dimensionsOfSize(1), instr.valueInt)
        assertEquals(ArrayMemberType.BOOLEAN, instr.valueInt.specialisedType)
        val v = instr.valueInt.valueAt(0)
        assertIs<APLLong>(v)
        assertEquals(1, v.value)
    }

    @Test
    fun simpleSingleElementDouble() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(,1.5)"))
        assertIs<OptimisedConstantListInstruction>(instr)
        assertDimension(dimensionsOfSize(1), instr.valueInt)
        assertEquals(ArrayMemberType.DOUBLE, instr.valueInt.specialisedType)
        val v = instr.valueInt.valueAt(0)
        assertIs<APLDouble>(v)
        assertEquals(1.5, v.value)
    }

    @Test
    fun simpleSingleElementChar() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(,@a)"))
        assertIs<OptimisedConstantListInstruction>(instr)
        assertDimension(dimensionsOfSize(1), instr.valueInt)
        assertEquals(ArrayMemberType.CHAR, instr.valueInt.specialisedType)
        val v = instr.valueInt.valueAt(0)
        assertIs<APLChar>(v)
        assertEquals('a'.code, v.value)
    }

    @Test
    fun simpleSingleElementList() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(,⊂1 2)"))
        assertIs<OptimisedConstantListInstruction>(instr)
        assertDimension(dimensionsOfSize(1), instr.valueInt)
        val v = instr.valueInt.valueAt(0)
        assert1DArray(arrayOf(1, 2), v)
    }

    @Test
    fun simpleSingleElementMixedList() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation("(,⊂@a 1 2)"))
        assertIs<OptimisedConstantListInstruction>(instr)
        assertDimension(dimensionsOfSize(1), instr.valueInt)
        val v = instr.valueInt.valueAt(0)
        assert1DArray(arrayOf(InnerChar('a'), 1, 2), v)
    }

    @Test
    fun arrayOfSingleElementLists() {
        val engine = makeEngine()
        val instr = engine.parse(StringSourceLocation(",⊂(,⊂1 2) (,⊂3 4)"))
        assertIs<OptimisedConstantListInstruction>(instr)
    }
}
