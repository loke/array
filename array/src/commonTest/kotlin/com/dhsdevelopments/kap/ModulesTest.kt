package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.log.Level
import kotlin.test.*

class ModulesTest : APLTest() {
    @Test
    fun interruptedModuleInit() {
        KapLogger.level = Level.WARNING
        val engine = makeEngine()
        engine.addModule(InterruptTestModule())
        val result = engine.parseAndEval(StringSourceLocation("1+10"))
        assertSimpleNumber(11, result)
        assertTrue(engine.modules.none { m -> m is InterruptTestModule })
    }

    private var oldLevel: Level? = null

    @BeforeTest
    fun beforeTest() {
        oldLevel = KapLogger.level
    }

    @AfterTest
    fun afterTest() {
        KapLogger.level = oldLevel!!
    }

    @Test
    fun simpleModuleInit() {
        val engine = makeEngine()
        val mod = SimpleTestModule()
        assertEquals(0, mod.state)
        engine.addModule(mod)
        assertEquals(1, mod.state)
        val foundMod = engine.modules.first { m -> m is SimpleTestModule }
        assertSame(mod, foundMod)
        engine.close()
        assertEquals(2, mod.state)
    }

    @Test
    fun testFindModule() {
        val engine = makeEngine()
        val mod = SimpleTestModule()
        assertEquals(0, mod.state)
        engine.addModule(mod)
        assertEquals(1, mod.state)
        val foundMod = engine.findModuleOrError<SimpleTestModule>()
        assertSame(mod, foundMod)
        engine.close()
        assertEquals(2, mod.state)
    }

    class InterruptTestModule : KapModule {
        override val name get() = "interrupt-test-module"

        override fun init(engine: Engine) {
            throw ModuleInitSkipped("Testing init skip")
        }
    }

    class SimpleTestModule : KapModule {
        override val name: String get() = "simple-test-module"

        var state = 0

        override fun init(engine: Engine) {
            state = 1
        }

        override fun close() {
            state = 2
        }
    }
}
