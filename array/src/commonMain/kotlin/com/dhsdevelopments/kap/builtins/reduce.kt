package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.mpthread.checkOrUpdate
import com.dhsdevelopments.kap.mpthread.makeAtomicRefArray
import com.dhsdevelopments.mpbignum.LongExpressionOverflow
import kotlin.math.absoluteValue

class ReduceResult1Arg(
    val context: RuntimeContext,
    val fn: APLFunction,
    val arg: APLValue,
    opAxis: Int,
    val pos: Position,
    val savedStack: StorageStack.StorageStackFrame?
) : APLArray() {
    override val dimensions: Dimensions
    private val stepLength: Int
    private val sizeAlongAxis: Int
    private val fromSourceMul: Int
    private val toDestMul: Int
    override val specialisedType: ArrayMemberType

    init {
        val argDimensions = arg.dimensions
        val argMultipliers = argDimensions.multipliers()

        ensureValidAxis(opAxis, argDimensions, pos)

        stepLength = argMultipliers[opAxis]
        sizeAlongAxis = argDimensions[opAxis]
        dimensions = argDimensions.remove(opAxis)

        val multipliers = dimensions.multipliers()

        fromSourceMul = if (opAxis == 0) size else multipliers[opAxis - 1]
        toDestMul = fromSourceMul * argDimensions[opAxis]

        specialisedType = when {
            arg.specialisedType.isLong && fn.optimisationFlags.is2ALongLong -> ArrayMemberType.LONG
            arg.specialisedType.isDouble && fn.optimisationFlags.is2ADoubleDouble -> ArrayMemberType.DOUBLE
            else -> ArrayMemberType.GENERIC
        }
    }

    override fun valueAt(p: Int): APLValue {
        return reduceAtPosition(fn, context, arg, p, sizeAlongAxis, stepLength, fromSourceMul, toDestMul, savedStack, pos)
    }

    // TODO: This implementation creates unnecessary boxing
    override fun valueAtLong(p: Int): Long {
        return when (val v = valueAt(p)) {
            is APLLong -> v.value
            is APLBigInt -> throw LongExpressionOverflow(v.value)
            else -> error("Result from reduction should be an integer, got: ${v}")
        }
    }

    override fun valueAtDouble(p: Int): Double {
        val v = valueAt(p)
        require(v is APLDouble) { "Result from reduction should be a double, got: ${v}" }
        return v.value
    }

    override fun unwrapDeferredValue(): APLValue {
        return unwrapEnclosedSingleValue(this)
    }
}

private fun reduceAtPosition(
    fn: APLFunction,
    context: RuntimeContext,
    arg: APLValue,
    p: Int,
    sizeAlongAxis: Int,
    stepLength: Int,
    fromSourceMul: Int,
    toDestMul: Int,
    savedStack: StorageStack.StorageStackFrame?,
    pos: Position
): APLValue {
    return if (sizeAlongAxis == 0) {
        fn.identityValue()
    } else {
        val highPosition = p / fromSourceMul
        val lowPosition = p % fromSourceMul
        val posInSrc = highPosition * toDestMul + lowPosition

        val specialisedType = arg.specialisedType
        when {
            specialisedType.isLong && fn.optimisationFlags.is2ALongLong -> {
                fn.reduceLongToLong(context, arg, posInSrc, sizeAlongAxis, stepLength, pos, savedStack, null)
            }
            specialisedType.isDouble && fn.optimisationFlags.is2ADoubleDouble -> {
                fn.reduceDoubleToDouble(context, arg, posInSrc, sizeAlongAxis, stepLength, pos, savedStack, null)
            }
            else -> {
                fn.reduceGeneric(context, arg, sizeAlongAxis, stepLength, posInSrc, savedStack, null)
            }
        }
    }
}

fun defaultReduceDoubleToDouble(
    fn: APLFunction,
    context: RuntimeContext,
    arg: APLValue,
    offset: Int,
    sizeAlongAxis: Int,
    stepLength: Int,
    pos: Position,
    savedStack: StorageStack.StorageStackFrame?,
    functionAxis: APLValue?
): APLValue {
    val engine = context.engine
    var curr = arg.valueAtDouble(offset)
    withPossibleSavedStack(savedStack) {
        for (i in 1 until sizeAlongAxis) {
            engine.checkInterrupted(pos)
            curr = fn.eval2ArgDoubleToDoubleWithAxis(context, curr, arg.valueAtDouble(i * stepLength + offset), functionAxis)
        }
    }
    return curr.makeAPLNumber()
}

fun defaultReduceLongToLong(
    fn: APLFunction,
    context: RuntimeContext,
    arg: APLValue,
    offset: Int,
    sizeAlongAxis: Int,
    stepLength: Int,
    pos: Position,
    savedStack: StorageStack.StorageStackFrame?,
    functionAxis: APLValue?
): APLValue {
    val engine = context.engine
    return withPossibleSavedStack(savedStack) {
        var i = 1
        try {
            var curr = arg.valueAtLong(offset)
            while (i < sizeAlongAxis) {
                engine.checkInterrupted(pos)
                curr = fn.eval2ArgLongToLongWithAxis(context, curr, arg.valueAtLong(i++ * stepLength + offset), functionAxis)
            }
            APLLong(curr)
        } catch (e: LongExpressionOverflow) {
            // If we get here, the current evaluation must have overflowed, so continue in generic mode
            var curr0: APLValue = APLBigInt(e.result)
            while (i < sizeAlongAxis) {
                engine.checkInterrupted(pos)
                curr0 = fn.eval2Arg(context, curr0, arg.valueAt(i++ * stepLength + offset), functionAxis)
            }
            curr0
        }
    }
}

fun defaultReduceGeneric(
    fn: APLFunction,
    context: RuntimeContext,
    arg: APLValue,
    offset: Int,
    sizeAlongAxis: Int,
    stepLength: Int,
    pos: Position,
    savedStack: StorageStack.StorageStackFrame?,
    functionAxis: APLValue?
): APLValue {
    val engine = context.engine
    var curr = arg.valueAt(offset)
    withPossibleSavedStack(savedStack) {
        for (i in 1 until sizeAlongAxis) {
            engine.checkInterrupted(pos)
            curr = fn.eval2Arg(context, curr, arg.valueAt(i * stepLength + offset), functionAxis).collapse()
        }
    }
    return curr
}

fun unwrapEnclosedSingleValue(value: APLValue): APLValue {
    return if (value.dimensions.isEmpty()) {
        EnclosedAPLValue.make(value.valueAt(0).unwrapDeferredValue())
    } else {
        value
    }
}

class ReduceNWiseResultValue(
    val context: RuntimeContext,
    val fn: APLFunction,
    val reductionSize: Int,
    val b: APLValue,
    operatorAxis: Int,
    val savedStack: StorageStack.StorageStackFrame?
) : APLArray() {
    override val dimensions: Dimensions

    private val axisActionFactors: AxisActionFactors
    private val highMultiplier: Int
    private val axisMultiplier: Int
    private val dir: Int
    private val reductionSizeAbsolute: Int
    private val cachedSources = makeAtomicRefArray<APLValue>(b.size)

    init {
        val bDimensions = b.dimensions
        dimensions = Dimensions(IntArray(bDimensions.size) { i ->
            val s = bDimensions[i]
            if (i == operatorAxis) {
                s - reductionSize.absoluteValue + 1
            } else {
                s
            }
        })

        val bMultipliers = bDimensions.multipliers()
        axisMultiplier = bMultipliers[operatorAxis]
        highMultiplier = axisMultiplier * bDimensions[operatorAxis]
        dir = if (reductionSize < 0) -1 else 1
        reductionSizeAbsolute = reductionSize.absoluteValue

        axisActionFactors = AxisActionFactors(dimensions, operatorAxis)
    }

    private fun lookupSource(p: Int): APLValue {
        return cachedSources.checkOrUpdate(p) {
            b.valueAt(p)
        }
    }

    override fun valueAt(p: Int): APLValue {
        axisActionFactors.withFactors(p) { high, low, axisCoord ->
            var pos = if (reductionSize < 0) reductionSizeAbsolute - 1 else 0
            var curr = lookupSource((high * highMultiplier) + ((axisCoord + pos) * axisMultiplier) + low)
            withPossibleSavedStack(savedStack) {
                repeat(reductionSizeAbsolute - 1) {
                    pos += dir
                    val value = lookupSource((high * highMultiplier) + ((axisCoord + pos) * axisMultiplier) + low)
                    curr = fn.eval2Arg(context, curr, value, null)
                }
            }
            return curr
        }
    }
}

abstract class ReduceFunctionImpl(fn: APLFunction, pos: FunctionInstantiation) : APLFunction(pos, listOf(fn)) {
    private val saveStackSupport = SaveStackSupport(this)

    val fn get() = fns[0]

    override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
        val axisParam = if (axis == null) null else axis.ensureNumber(pos).asInt(pos)
        return if (a.rank == 0) {
            if (axisParam != null && axisParam != 0) {
                throwAPLException(IllegalAxisException(axisParam, a.dimensions, pos))
            }
            a
        } else {
            val axisInt = axisParam ?: defaultAxis(a)
            ensureValidAxis(axisInt, a.dimensions, pos)
            ReduceResult1Arg(context, fn, a, axisInt, pos, saveStackSupport.savedStack())
        }
    }

    override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
        val bDimensions = b.dimensions
        val axisParam = if (axis == null) null else axis.ensureNumber(pos).asInt(pos)
        val size = a.ensureNumber(pos).asInt(pos)
        if (bDimensions.size == 0) {
            if (axisParam != null && axisParam != 0) {
                throwAPLException(IllegalAxisException(axisParam, bDimensions, pos))
            }
            return when (size) {
                1 -> APLArrayImpl(size1Dimensions(), arrayOf(b))
                0 -> APLNullValue
                -1 -> APLArrayImpl(size1Dimensions(), arrayOf(APLLONG_0))
                else -> throwAPLException(InvalidDimensionsException("Invalid left argument for scalar right arg", pos))
            }
        }
        val axisInt = axisParam ?: defaultAxis(b)
        ensureValidAxis(axisInt, bDimensions, pos)
        return when {
            size.absoluteValue > bDimensions[axisInt] + 1 -> {
                throwAPLException(InvalidDimensionsException("Left argument too large. The absolute value of A (${size}) must be less than or equal to the size of the reduced axis (${bDimensions[axisInt]}) - 1.", pos))
            }
            size.absoluteValue == bDimensions[axisInt] + 1 -> {
                val d = Dimensions(IntArray(bDimensions.size) { i ->
                    if (i == axisInt) 0 else bDimensions[i]
                })
                APLArrayImpl(d, emptyArray())
            }
            else -> {
                ReduceNWiseResultValue(context, fn, size, b, axisInt, saveStackSupport.savedStack())
            }
        }
    }

    abstract fun defaultAxis(a: APLValue): Int

    override val name1Arg get() = "reduce by ${fn.name2Arg}"
    override val name2Arg get() = "windowed reduce by ${fn.name2Arg}"
}

class ReduceFunctionImplLastAxis(fn: APLFunction, pos: FunctionInstantiation) : ReduceFunctionImpl(fn, pos) {
    override fun defaultAxis(a: APLValue) = a.dimensions.size - 1
    override val name1Arg = "reduce last axis [${fn.name2Arg}]"
}

class ReduceFunctionImplFirstAxis(fn: APLFunction, pos: FunctionInstantiation) : ReduceFunctionImpl(fn, pos) {
    override fun defaultAxis(a: APLValue) = 0
    override val name1Arg = "reduce first axis [${fn.name2Arg}]"
}

class ReduceOpLastAxis : APLOperatorOneArg {
    override fun combineFunction(fn: APLFunction, pos: FunctionInstantiation): APLFunctionDescriptor {
        return ReduceOpFunctionDescriptor(fn)
    }

    class ReduceOpFunctionDescriptor(val fn: APLFunction) : APLFunctionDescriptor {
        override fun make(instantiation: FunctionInstantiation): APLFunction {
            return ReduceFunctionImplLastAxis(fn, instantiation)
        }
    }
}

class ReduceOpFirstAxis : APLOperatorOneArg {
    override fun combineFunction(fn: APLFunction, pos: FunctionInstantiation): APLFunctionDescriptor {
        return ReduceOpFunctionDescriptor(fn)
    }

    class ReduceOpFunctionDescriptor(val fn: APLFunction) : APLFunctionDescriptor {
        override fun make(instantiation: FunctionInstantiation): APLFunction {
            return ReduceFunctionImplFirstAxis(fn, instantiation)
        }
    }
}

// Scan is similar in concept to reduce, so we'll keep it in this file

class ScanResult1Arg(val context: RuntimeContext, val fn: APLFunction, val fnAxis: APLValue?, val a: APLValue, axis: Int) : APLArray() {
    override val dimensions = a.dimensions

    private val cachedResults = makeAtomicRefArray<APLValue>(size)
    private val axisActionFactors = AxisActionFactors(dimensions, axis)

    override fun valueAt(p: Int): APLValue {
        axisActionFactors.withFactors(p) { high, low, axisCoord ->
            var currIndex = axisCoord
            var leftValue: APLValue
            while (true) {
                val index = axisActionFactors.indexForAxis(high, low, currIndex)
                if (currIndex == 0) {
                    leftValue = cachedResults.checkOrUpdate(index) { a.valueAt(index) }
                    break
                } else {
                    val cachedVal = cachedResults[index]
                    if (cachedVal != null) {
                        leftValue = cachedVal
                        break
                    }
                }
                currIndex--
            }

            if (currIndex < axisCoord) {
                for (i in (currIndex + 1)..axisCoord) {
                    val index = axisActionFactors.indexForAxis(high, low, i)
                    leftValue = cachedResults.checkOrUpdate(index) { fn.eval2Arg(context, leftValue, a.valueAt(index), fnAxis).collapse() }
                }
            }

            return leftValue
        }
    }
}

abstract class ScanFunctionImpl(val fn: APLFunction, pos: FunctionInstantiation) : APLFunction(pos) {
    override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
        val axisParam = if (axis != null) axis.ensureNumber(pos).asInt(pos) else null
        return if (a.rank == 0) {
            if (axisParam != null && axisParam != 0) {
                throwAPLException(IllegalAxisException(axisParam, a.dimensions, pos))
            }
            a
        } else {
            val v = axisParam ?: defaultAxis(a)
            ensureValidAxis(v, a.dimensions, pos)
            ScanResult1Arg(context, fn, axis, a, v)
        }
    }

    abstract fun defaultAxis(a: APLValue): Int
}

class ScanLastAxisFunctionImpl(fn: APLFunction, pos: FunctionInstantiation) : ScanFunctionImpl(fn, pos) {
    override fun defaultAxis(a: APLValue) = a.dimensions.size - 1
}

class ScanFirstAxisFunctionImpl(fn: APLFunction, pos: FunctionInstantiation) : ScanFunctionImpl(fn, pos) {
    override fun defaultAxis(a: APLValue) = 0
}

class ScanLastAxisOp : APLOperatorOneArg {
    override fun combineFunction(fn: APLFunction, pos: FunctionInstantiation): APLFunctionDescriptor {
        return ScanOpFunctionDescriptor(fn)
    }

    class ScanOpFunctionDescriptor(val fn: APLFunction) : APLFunctionDescriptor {
        override fun make(instantiation: FunctionInstantiation): APLFunction {
            return ScanLastAxisFunctionImpl(fn, instantiation)
        }
    }
}

class ScanFirstAxisOp : APLOperatorOneArg {
    override fun combineFunction(fn: APLFunction, pos: FunctionInstantiation): APLFunctionDescriptor {
        return ScanOpFunctionDescriptor(fn)
    }

    class ScanOpFunctionDescriptor(val fn: APLFunction) : APLFunctionDescriptor {
        override fun make(instantiation: FunctionInstantiation): APLFunction {
            return ScanFirstAxisFunctionImpl(fn, instantiation)
        }
    }
}
