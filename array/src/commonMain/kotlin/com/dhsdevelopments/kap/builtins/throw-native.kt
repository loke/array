package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*

class ThrowNativeFunction() : APLFunctionDescriptor {
    inner class ThrowNativeFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        private val symInvalidDimensionsException: Symbol
        private val kapEvalException: Symbol

        init {
            val ns = pos.engine.coreNamespace
            symInvalidDimensionsException = ns.internSymbol("InvalidDimensionsException")
            kapEvalException = ns.internSymbol("KapEvalException")
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val sym = a.ensureSymbol(pos).value
            val message = b.toStringValue(pos)
            when (sym) {
                symInvalidDimensionsException -> throwAPLException(InvalidDimensionsException(message, pos))
                kapEvalException -> throwAPLException(APLEvalException(message, pos))
                else -> throwAPLException(APLEvalException("Invalid exception name: ${sym.nameWithNamespace}", pos))
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ThrowNativeFunctionImpl(instantiation)
}
