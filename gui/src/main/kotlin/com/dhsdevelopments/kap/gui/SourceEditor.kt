package com.dhsdevelopments.kap.gui

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.gui.settings.ConfigProvider
import com.dhsdevelopments.kap.gui.styledarea.KapEditorStyledArea
import com.dhsdevelopments.kap.gui.styledarea.TextStyle
import com.dhsdevelopments.kap.gui.syms.LanguageBar
import javafx.application.Platform
import javafx.event.Event
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.text.TextFlow
import javafx.stage.Stage
import javafx.stage.WindowEvent
import org.fxmisc.flowless.VirtualizedScrollPane
import org.fxmisc.richtext.LineNumberFactory
import org.fxmisc.richtext.StyledTextArea
import org.fxmisc.richtext.TextExt
import org.fxmisc.richtext.model.*
import org.fxmisc.wellbehaved.event.EventPattern
import org.fxmisc.wellbehaved.event.InputMap
import java.io.File
import java.io.FileWriter
import java.lang.Integer.min
import java.lang.ref.WeakReference
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.function.BiConsumer
import java.util.function.Function
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

class SourceEditor(val client: Client) {
    private val stage = Stage()
    private var styledArea: SourceEditorStyledArea
    private var loaded: File? = null
    private val messageArea = TextField()

    private val highlightTimer = Timer()
    private var highlightTimerTask: TimerTask? = null
    private var inLoad = false

    init {
        val vbox = VBox()

        val menuBar = MenuBar().apply {
            val fileMenu = Menu("File").apply {
                items.add(MenuItem("Save").apply {
                    onAction = EventHandler { processSave() }
                })
                items.add(MenuItem("Close").apply {
                    onAction = EventHandler { closeMaybeSave(true) }
                })
            }
            menus.add(fileMenu)
            val runMenu = Menu("Code").apply {
                items.add(MenuItem("Run").apply {
                    onAction = EventHandler { runClicked() }
                    accelerator = KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN)
                })
            }
            menus.add(runMenu)
        }
        vbox.children.add(menuBar)
        vbox.children.add(LanguageBar(client))

        styledArea = initStyledArea()
        val scrollArea = VirtualizedScrollPane(styledArea)
        vbox.children.add(scrollArea)
        VBox.setVgrow(scrollArea, Priority.ALWAYS)

        vbox.children.add(messageArea)

        stage.scene = Scene(vbox, 1200.0, 800.0)

        client.sourceEditors.add(this)
        stage.onCloseRequest = EventHandler { event -> handleCloseRequest(event) }

        styledArea.textProperty().addListener { observable, oldValue, newValue ->
            if (!inLoad) {
                highlightTimerTask?.cancel()
                val task = object : TimerTask() {
                    override fun run() {
                        Platform.runLater {
                            highlightTokens()
                            highlightTimerTask = null
                        }
                    }
                }
                highlightTimer.schedule(task, 250)
                highlightTimerTask = task
            }
        }
    }

    private var highlightedRow: Int? = null

    private fun initStyledArea(): SourceEditorStyledArea {
        val applyParagraphStyle = BiConsumer<TextFlow, SourceEditorParStyle> { flow, parStyle ->
            flow.background = when (parStyle.type) {
                SourceEditorParStyle.StyleType.NORMAL -> Background.EMPTY
                SourceEditorParStyle.StyleType.ERROR -> Background(
                    BackgroundFill(
                        Color(1.0, 0.6, 0.6, 1.0),
                        CornerRadii.EMPTY,
                        Insets.EMPTY))
            }
        }
        val nodeFactory = Function<StyledSegment<String, TextStyle>, Node> { seg ->
            val applyStyle = { a: TextExt, b: TextStyle ->
                b.styleContent(a)
            }
            StyledTextArea.createStyledTextNode(seg.segment, seg.style, applyStyle)
        }
        val styledTextOps = SegmentOps.styledTextOps<TextStyle>()
        GenericEditableStyledDocument(SourceEditorParStyle(), TextStyle(client.config), styledTextOps)
        val srcEdit = SourceEditorStyledArea(
            this,
            SourceEditorParStyle(),
            applyParagraphStyle,
            TextStyle(client.config),
            styledTextOps,
            nodeFactory,
            client.config)

        srcEdit.content.paragraphs.addChangeObserver {
            if (highlightedRow != null) {
                highlightedRow = null
                srcEdit.clearHighlights()
                messageArea.text = ""
            }
        }

        return srcEdit
    }

    /**
     * If the file contains unsaved changes, ask the user whether they want to save.
     * If the user accepts without saving, or if they want to save, close the window
     * and return true. Otherwise return false.
     */
    fun closeMaybeSave(hideAfterClose: Boolean): Boolean {
        var close = false
        if (!styledArea.modifided) {
            close = true
        } else {
            val dialog = Dialog<ButtonType>().apply {
                title = "File is modified"
                contentText = "This file has unsaved changes. Save changes?"
                dialogPane.buttonTypes.apply {
                    add(ButtonType("Save", ButtonBar.ButtonData.YES))
                    add(ButtonType("Don't save", ButtonBar.ButtonData.NO))
                    add(ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE))
                }
            }
            val result = dialog.showAndWait()
            if (result.isPresent) {
                val type = result.get()
                if (type.buttonData == ButtonBar.ButtonData.YES) {
                    if (processSave()) {
                        close = true
                    }
                } else if (type.buttonData == ButtonBar.ButtonData.NO) {
                    close = true
                }
            }
        }
        if (close) {
            client.sourceEditors.remove(this)
            highlightTimer.cancel()
            if (hideAfterClose) {
                stage.hide()
            }
        }
        return close
    }

    private fun handleCloseRequest(event: WindowEvent) {
        if (!closeMaybeSave(false)) {
            event.consume()
        }
    }

    fun runClicked() {
        val source = EditorSourceLocation(this, styledArea.document.text)
        client.evalSource(source, preserveNamespace = true)
    }

    private fun processSave(): Boolean {
        if (loaded == null) {
            val selectedFile = client.selectFile(true) ?: return false
            updateLoadedFile(selectedFile)
        }
        saveContentToFile(loaded)
        styledArea.modifided = false
        return true
    }

    private fun updateLoadedFile(file: File) {
        loaded = file
        stage.title = file.absolutePath
    }

    private fun saveContentToFile(name: File?) {
        FileWriter(name, StandardCharsets.UTF_8).use { out ->
            val content = styledArea.document.text
            val fixed = if (content.isNotEmpty() && !content.endsWith("\n")) {
                content + "\n"
            } else {
                content
            }
            out.write(fixed)
        }
    }

    @OptIn(ExperimentalContracts::class)
    private inline fun <T> withInLoadSet(fn: () -> T): T {
        contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
        val oldInLoad = inLoad
        inLoad = true
        try {
            return fn()
        } finally {
            inLoad = oldInLoad
        }
    }

    fun setFile(file: File) {
        val content = file.readLines()
        styledArea.deleteText(0, styledArea.length)
        //styledArea.insert(0, content, TextStyle())
        val builder = ReadOnlyStyledDocumentBuilder(styledArea.segOps, styledArea.initialParagraphStyle)
        content.forEach { text ->
            builder.addParagraph(text, styledArea.initialTextStyle)
        }
        withInLoadSet {
            styledArea.insert(0, builder.build())
            highlightTokens()
            styledArea.undoManager.forgetHistory()
            styledArea.undoManager.mark()
            styledArea.modifided = false
        }
        updateLoadedFile(file)
    }

    fun show() {
        stage.show()
    }

    fun highlightError(pos: Position, message: String?) {
        try {
            messageArea.text = message ?: ""
            styledArea.caretSelectionBind.moveTo(pos.line, pos.col)
            styledArea.highlightRow(pos.line)
            highlightedRow = pos.line
            styledArea.requestFocus()
        } catch (e: Exception) {
            KapLogger.e(e) { "Error when highlighting error location" }
        }
    }

    fun highlightTokens() {
        val src = EditorSourceLocation(this, styledArea.document.text)
        val tokeniser = TokenGenerator(client.engine, src)
        styledArea.clearStyles()
        try {
            while (true) {
                val (token, pos) = tokeniser.nextTokenOrSpace()
                if (token == EndOfFile) break
                if (pos.line < pos.computedEndLine || pos.col < pos.computedEndCol) {
                    val style = tokenToStyle(token)
                    if (style != null) {
                        styledArea.setStyleForRange(pos.line, pos.col, pos.computedEndLine, pos.computedEndCol, style)
                    }
                }
            }
        } catch (_: ParseException) {
            // If there is a parse exception, simply stop the highlight activity
        }
    }

    private fun tokenToStyle(token: Token): TextStyle? {
        return tokenClassToStyle[token::class]
    }

    private val tokenClassToStyle = mapOf(
        Symbol::class to TextStyle(client.config, TextStyle.Type.SYNTAX_HIGHLIGHT_SYMBOL),
        StringToken::class to TextStyle(client.config, TextStyle.Type.SYNTAX_HIGHLIGHT_STRING),
        ParsedLong::class to TextStyle(client.config, TextStyle.Type.SYNTAX_HIGHLIGHT_NUMBER),
        ParsedDouble::class to TextStyle(client.config, TextStyle.Type.SYNTAX_HIGHLIGHT_NUMBER),
        ParsedComplex::class to TextStyle(client.config, TextStyle.Type.SYNTAX_HIGHLIGHT_NUMBER),
        ParsedBigInt::class to TextStyle(client.config, TextStyle.Type.SYNTAX_HIGHLIGHT_NUMBER),
        ParsedRational::class to TextStyle(client.config, TextStyle.Type.SYNTAX_HIGHLIGHT_NUMBER),
        ParsedCharacter::class to TextStyle(client.config, TextStyle.Type.SYNTAX_HIGHLIGHT_CHAR),
        Comment::class to TextStyle(client.config, TextStyle.Type.SYNTAX_HIGHLIGHT_COMMENT))

    class EditorSourceLocation(editor: SourceEditor, val text: String) : SourceLocation {
        private val editorReference = WeakReference(editor)

        val editor get() = editorReference.get()

        override fun sourceText() = text
        override fun open() = makeStringCharacterProvider(text)
    }
}

class SourceEditorParStyle(val type: StyleType = StyleType.NORMAL) {
    enum class StyleType {
        NORMAL,
        ERROR
    }
}

class SourceEditorStyledArea(
    private val sourceEditor: SourceEditor,
    parStyle: SourceEditorParStyle,
    applyParagraphStyle: BiConsumer<TextFlow, SourceEditorParStyle>,
    textStyle: TextStyle,
    styledTextOps: TextOps<String, TextStyle>,
    nodeFactory: Function<StyledSegment<String, TextStyle>, Node>,
    configProvider: ConfigProvider
) : KapEditorStyledArea<SourceEditorParStyle, String, TextStyle>(
    parStyle,
    applyParagraphStyle,
    textStyle,
    styledTextOps,
    nodeFactory, configProvider
) {
    var modifided = false
    override val client get() = sourceEditor.client

    init {
        paragraphGraphicFactory = LineNumberFactory.get(this)
        textProperty().addListener { _, _, _ -> modifided = true }
        undoManager.forgetHistory()
        undoManager.mark()
    }

    override fun addInputMappings(entries: MutableList<InputMap<out Event>>) {
        entries.add(InputMap.consume(EventPattern.keyPressed(KeyCode.F, KeyCombination.CONTROL_DOWN), { caretSelectionBind.moveToNextChar() }))
        entries.add(InputMap.consume(EventPattern.keyPressed(KeyCode.B, KeyCombination.CONTROL_DOWN), { caretSelectionBind.moveToPrevChar() }))
        entries.add(InputMap.consume(EventPattern.keyPressed(KeyCode.P, KeyCombination.CONTROL_DOWN), { moveToPrevLine() }))
        entries.add(InputMap.consume(EventPattern.keyPressed(KeyCode.N, KeyCombination.CONTROL_DOWN), { moveToNextLine() }))
        entries.add(InputMap.consume(EventPattern.keyPressed(KeyCode.A, KeyCombination.CONTROL_DOWN), { caretSelectionBind.moveToParStart() }))
        entries.add(InputMap.consume(EventPattern.keyPressed(KeyCode.E, KeyCombination.CONTROL_DOWN), { caretSelectionBind.moveToParEnd() }))
        entries.add(InputMap.consume(EventPattern.keyPressed(KeyCode.D, KeyCombination.CONTROL_DOWN), { delSingleCharacter() }))
    }

    private fun moveToNextLine() {
        val n = currentParagraph
        if (n < paragraphs.size - 1) {
            val p = paragraphs[n + 1]
            caretSelectionBind.moveTo(n + 1, min(caretSelectionBind.columnPosition, p.length()))
        }
    }

    private fun moveToPrevLine() {
        val n = currentParagraph
        if (n > 0) {
            val p = paragraphs[n - 1]
            caretSelectionBind.moveTo(n - 1, min(caretSelectionBind.columnPosition, p.length()))
        }
    }

    private fun delSingleCharacter() {
        val n = currentParagraph
        val p = paragraphs[n]
        val pos = caretSelectionBind.anchorColPosition
        if (pos < p.length()) {
            deleteText(n, pos, n, pos + 1)
        }
    }

    fun highlightRow(row: Int) {
        clearHighlights()
        setParagraphStyle(row, SourceEditorParStyle(type = SourceEditorParStyle.StyleType.ERROR))
    }

    fun clearHighlights() {
        for (row in 0 until paragraphs.size) {
            val paragraph = paragraphs[row]
            if (paragraph.paragraphStyle.type == SourceEditorParStyle.StyleType.ERROR) {
                setParagraphStyle(row, SourceEditorParStyle(type = SourceEditorParStyle.StyleType.NORMAL))
            }
        }
    }
}
