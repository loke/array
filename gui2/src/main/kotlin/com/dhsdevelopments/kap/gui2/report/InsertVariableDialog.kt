package com.dhsdevelopments.kap.gui2.report

import com.dhsdevelopments.kap.StackStorageRef
import com.dhsdevelopments.kap.currentStorageStack
import com.dhsdevelopments.kap.gui2.Request
import java.awt.Dimension
import javax.swing.*

class InsertVariableDialog(
    val reportFrame: ReportFrame,
    val rowIndex: Int,
    val colIndex: Int
) : JDialog(reportFrame, "Insert value", true) {
    private val nameField: JTextField

    init {
        defaultCloseOperation = DISPOSE_ON_CLOSE

        val panel = JPanel().apply {
            layout = BoxLayout(this, BoxLayout.PAGE_AXIS)
            add(JLabel("Variable name:").apply { alignmentX = LEFT_ALIGNMENT })
            add(Box.createRigidArea(Dimension(0, 5)))
            nameField = JTextField(50).apply {
                maximumSize = preferredSize
                alignmentX = LEFT_ALIGNMENT
            }
            add(nameField)
            add(Box.createRigidArea(Dimension(0, 5)))
            add(Box.createVerticalGlue())

            val buttonPanel = JPanel().apply {
                layout = BoxLayout(this, BoxLayout.LINE_AXIS)
                alignmentX = LEFT_ALIGNMENT
                setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10))
                add(Box.createHorizontalGlue())
                add(JButton("Cancel").apply { addActionListener { dispose() } })
                add(Box.createRigidArea(Dimension(10, 0)))
                add(JButton("OK").apply { addActionListener { okClicked() } })
            }
            add(buttonPanel)
        }

        contentPane = panel

        pack()
    }

    private fun okClicked() {
        reportFrame.client.computeQueue.requestJob(Request { engine ->
            val sym = engine.currentNamespace.findSymbol(nameField.text.trim(), includePrivate = true)
            if (sym == null) {
                SwingUtilities.invokeLater {
                    JOptionPane.showMessageDialog(this, "Variable not found", "Error", JOptionPane.ERROR_MESSAGE)
                }
            } else {
                engine.rootEnvironment.findBinding(sym)?.let { binding ->
                    engine.withThreadLocalAssigned {
                        val storage = currentStorageStack().findStorage(StackStorageRef(binding))
                        val value = storage.value()
                        SwingUtilities.invokeLater {
                            if (value == null) {
                                JOptionPane.showMessageDialog(this, "Variable is not assigned", "Error", JOptionPane.ERROR_MESSAGE)
                            } else {
                                reportFrame.addDisplayedValue(engine, storage, value, rowIndex, colIndex)
                            }
                        }
                    }
                }
            }
        })
        dispose()
    }
}
