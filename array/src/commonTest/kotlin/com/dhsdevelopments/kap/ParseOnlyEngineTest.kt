package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.builtins.AddAPLFunction
import kotlin.test.*

class ParseOnlyEngineTest : APLTest() {
    @Test
    fun registerFunctionInCopy() {
        val engine = makeEngine()
        engine.parseAndEval(StringSourceLocation("∇ foo { ⍵+1 } ⋄ foo 100")).collapse().let { result ->
            assertSimpleNumber(101, result)
        }
        val engine2 = engine.copyToSyntaxChecker()
        registerEngine(engine2)
        engine2.parse(StringSourceLocation("∇ bar { ⍵+2 } ⋄ bar 1 ⋄ bar 100")).let { instr ->
            assertIs<InstructionList>(instr)
        }
        assertFailsWith<VariableNotAssigned> {
            engine.parseAndEval(StringSourceLocation("bar 100"))
        }
    }

    @Test
    fun registerLocalFunctionInCopy() {
        val engine = makeEngine()
        engine.parseAndEval(StringSourceLocation("foo ⇐ { ⍵+1 } ⋄ 'bar foo 100")).collapse().let { result ->
            assertSimpleNumber(101, result)
        }
        val engine2 = engine.copyToSyntaxChecker()
        registerEngine(engine2)
        engine2.parse(StringSourceLocation("bar ⇐ { ⍵+2 } ⋄ 2 + bar 1 ⋄ bar 100")).let { instr ->
            assertIs<InstructionList>(instr)
            assertEquals(3, instr.instructions.size)
            instr.instructions[0].let { instr0 ->
                assertIs<APLParser.UpdateLocalFunctionInstruction>(instr0)
            }
            instr.instructions[1].let { instr0 ->
                assertIs<FunctionCall2Arg>(instr0)
                val s = engine2.findSymbolInImports("+")
                assertNotNull(s)
                assertIs<AddAPLFunction.AddAPLFunctionImpl>(instr0.fn)
                val leftArgs = instr0.leftArgs
                assertIs<LiteralInteger>(leftArgs)
                assertEquals(2L, leftArgs.value)
                val rightArgs = instr0.rightArgs
                assertIs<FunctionCall1Arg>(rightArgs)
                val fn = rightArgs.fn
                assertIs<APLParser.LocalFunctionCall>(fn)
            }
            instr.instructions[2].let { instr0 ->
                assertIs<FunctionCall1Arg>(instr0)
            }
        }
        assertFailsWith<VariableNotAssigned> {
            engine.parseAndEval(StringSourceLocation("bar 100"))
        }
    }

    @Test
    fun explicitCloseOfCopyDoesNotAffectSource() {
        val engine = makeEngine()
        engine.parseAndEval(StringSourceLocation("∇ foo { ⍵+1 } ⋄ foo 100")).collapse().let { result ->
            assertSimpleNumber(101, result)
        }
        val engine2 = engine.copyToSyntaxChecker()
        try {
            engine2.parse(StringSourceLocation("∇ bar { ⍵+2 } ⋄ bar 1 ⋄ bar 100")).let { instr ->
                assertIs<InstructionList>(instr)
            }
        } finally {
            engine2.close()
        }
        assertFailsWith<VariableNotAssigned> {
            engine.parseAndEval(StringSourceLocation("bar 100"))
        }
    }

    @Test
    fun explicitCloseOfCopyDoesNotAffectSourceLocal() {
        val engine = makeEngine()
        engine.parseAndEval(StringSourceLocation("foo ⇐ { ⍵+1 } ⋄ 'bar foo 100")).collapse().let { result ->
            assertSimpleNumber(101, result)
        }
        val engine2 = engine.copyToSyntaxChecker()
        try {
            engine2.parse(StringSourceLocation("bar ⇐ { ⍵+2 } ⋄ bar 1 ⋄ bar 100")).let { instr ->
                assertIs<InstructionList>(instr)
            }
        } finally {
            engine2.close()
        }
        assertFailsWith<VariableNotAssigned> {
            engine.parseAndEval(StringSourceLocation("bar 100"))
        }
    }

    @Test
    fun oldSymbolsInDefaultNamespaceAreFoundInCopy() {
        val engine = makeEngine()
        engine.parseAndEval(StringSourceLocation("∇ foo { ⍵+1 } ⋄ foo 100")).collapse().let { result ->
            assertSimpleNumber(101, result)
        }
        val ns = engine.findNamespace(NamespaceList.DEFAULT_NAMESPACE_NAME.name)
        assertNotNull(ns)
        val sym = ns.findSymbol("foo", true)
        assertNotNull(sym)
        val engine2 = engine.copyToSyntaxChecker()
        registerEngine(engine2)
        val nsCopy = engine2.findNamespace(NamespaceList.DEFAULT_NAMESPACE_NAME.name)
        assertNotNull(nsCopy)
        assertNotSame(ns, nsCopy)
        val symCopy = nsCopy.findSymbol("foo", true)
        assertSame(sym, symCopy)
    }

    @Test
    fun newSymbolsInSourceAreNotFoundInCopy() {
        val engine = makeEngine()
        engine.parseAndEval(StringSourceLocation("∇ foo { ⍵+1 } ⋄ foo 100")).collapse().let { result ->
            assertSimpleNumber(101, result)
        }
        val engine2 = engine.copyToSyntaxChecker()
        registerEngine(engine2)
        engine2.parse(StringSourceLocation("bar ⇐ {⍵-1} ⋄ bar 1 ⋄ bar 100")).let { instr ->
            assertIs<InstructionList>(instr)
        }
        val nsCopy = engine2.findNamespace(NamespaceList.DEFAULT_NAMESPACE_NAME.name)
        assertNotNull(nsCopy)
        val symCopy = nsCopy.findSymbol("bar", true)
        assertNotNull(symCopy)

        val nsSource = engine.findNamespace(NamespaceList.DEFAULT_NAMESPACE_NAME.name)
        assertNotNull(nsSource)
        assertNotSame(nsSource, nsCopy)
        val symSource = nsSource.findSymbol("bar", true)
        assertNull(symSource)
    }

    @Test
    fun functionBindingIsPreservedInCopyGlobal() {
        val engine = makeEngine()
        engine.parseAndEval(StringSourceLocation("∇ foo { ⍺+⍵ } ⋄ 2 foo 100")).collapse().let { result ->
            assertSimpleNumber(102, result)
        }
        val engine2 = engine.copyToSyntaxChecker()
        registerEngine(engine2)
        val instr = engine2.parse(StringSourceLocation("foo⌻⍨ 1 2 3 ⋄ 1+2"))
        assertIs<InstructionList>(instr)
    }

    @Test
    fun functionBindingIsPreservedInCopyLocal() {
        val engine = makeEngine()
        engine.parseAndEval(StringSourceLocation("foo ⇐ { ⍺+⍵ } ⋄ 2 foo 100")).collapse().let { result ->
            assertSimpleNumber(102, result)
        }
        val engine2 = engine.copyToSyntaxChecker()
        registerEngine(engine2)
        val instr = engine2.parse(StringSourceLocation("foo⌻⍨ 1 2 3 ⋄ 1+2"))
        assertIs<InstructionList>(instr)
    }

    @Test
    fun evalCodeWithParseOnlyEngineShouldFail() {
        val engine = makeEngine()
        val engine2 = engine.copyToSyntaxChecker()
        registerEngine(engine2)
        assertFailsWith<IllegalStateException> {
            engine2.parseAndEval(StringSourceLocation("1"))
        }
    }

    @Test
    fun userDefinedFunctionsShouldBePreserved() {
        val engine = makeEngine()
        engine.parseAndEval(StringSourceLocation("∇ foo { 1+⍵ }"))
        engine.parseAndEval(StringSourceLocation("foo 10")).let { result ->
            assertSimpleNumber(11, result)
        }

        val engine2 = engine.copyToSyntaxChecker()
        registerEngine(engine2)

        engine2.parse(StringSourceLocation("∇ foo { 2+⍵ }"))

        engine.parseAndEval(StringSourceLocation("foo 10")).let { result ->
            assertSimpleNumber(11, result)
        }
    }
}
