package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.mpbignum.*
import kotlin.math.absoluteValue

private abstract class RangeValueGeneric(val a: APLValue, val b: APLValue, width: BigInt, pos: Position) : APLArray() {
    private val aSize = a.dimensions[0]
    private val bSize = b.dimensions[0]

    private val d: Dimensions
    override val dimensions get() = d
    private val width: Int

    init {
        val size = BigInt.of(aSize - 1) + BigInt.of(bSize - 1) + width
        if (size >= Int.MAX_VALUE) {
            throwAPLException(
                ArrayTooLargeException("Resulting range too large", pos)
                    .details("The resulting array would be ${size} elements, which is larger than the maximum allowed size for an array, which is ${Int.MAX_VALUE}"))
        }
        this.d = dimensionsOfSize(size.toInt())
        this.width = width.toInt()
    }

    override fun valueAt(p: Int): APLValue {
        return if (p < aSize - 1) {
            a.valueAt(p)
        } else if (p < aSize - 1 + width) {
            computeElementInRange(p - aSize + 1)
        } else {
            b.valueAt(p - (aSize - 1) - width + 1)
        }
    }

    abstract fun computeElementInRange(n: Int): APLValue

    private class RangeValueLong(a: APLValue, b: APLValue, pos: Position, val startVal: Long, endVal: Long)
        : RangeValueGeneric(a, b, (BigInt.of(endVal) - BigInt.of(startVal)).absoluteValue + 1, pos) {

        private val multiplier = if (startVal > endVal) -1 else 1

        override fun computeElementInRange(n: Int): APLValue {
            return (startVal + n * multiplier).makeAPLNumber()
        }
    }

    private class RangeValueChar(a: APLValue, b: APLValue, pos: Position, val startVal: Int, endVal: Int)
        : RangeValueGeneric(a, b, BigInt.of((endVal - startVal).absoluteValue + 1), pos) {

        private val multiplier = if (startVal > endVal) -1 else 1

        override fun computeElementInRange(n: Int): APLValue {
            return APLChar(startVal + n * multiplier)
        }
    }

    private class RangeValueBigInt(a: APLValue, b: APLValue, pos: Position, val startVal: BigInt, endVal: BigInt)
        : RangeValueGeneric(a, b, (endVal - startVal).absoluteValue + 1, pos) {

        private val isReverse = startVal > endVal

        override fun computeElementInRange(n: Int): APLValue {
            val res = if (isReverse) {
                startVal - n
            } else {
                startVal + n
            }
            return res.makeAPLNumberWithReduction()
        }
    }

    companion object {
        fun make(a: APLValue, b: APLValue, pos: Position): APLValue {
            val aSize = a.dimensions[0]

            val startElement = a.valueAt(aSize - 1).unwrapDeferredValue()
            val endElement = b.valueAt(0).unwrapDeferredValue()

            return when {
                startElement is APLInteger && endElement is APLInteger -> {
                    if (startElement is APLLong && endElement is APLLong) {
                        RangeValueLong(a, b, pos, startElement.value, endElement.value)
                    } else {
                        RangeValueBigInt(a, b, pos, startElement.asBigInt(pos), endElement.asBigInt(pos))
                    }
                }
                startElement is APLChar && endElement is APLChar -> {
                    val startPos = startElement.value
                    val endPos = endElement.value
                    RangeValueChar(a, b, pos, startPos, endPos)
                }
                else -> throwAPLException(
                    IncompatibleTypeException("Range types not compatible. A=${startElement.kapClass.name}, B=${endElement.kapClass.name}", pos)
                        .details("The last element of A and the first element of B has to both be integers or both be characters"))
            }
        }
    }
}

class RangeFunction : APLFunctionDescriptor {
    class RangeFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val a = a.arrayify()
            val b = b.arrayify()
            if (a.dimensions.size != 1 || a.dimensions[0] == 0 || b.dimensions.size != 1 || b.dimensions[0] == 0) {
                throwAPLException(InvalidDimensionsException("Both arguments to range must be scalars or non-empty 1-dimensional arrays", pos))
            }
            return RangeValueGeneric.make(a, b, pos)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = RangeFunctionImpl(instantiation)
}
