package com.dhsdevelopments.kap.ffi

import com.dhsdevelopments.kap.ArrayUtils
import java.lang.foreign.*
import java.lang.foreign.ValueLayout.*
import java.lang.invoke.MethodHandle

class JvmFfiManager : FfiManager {
    val linker: Linker = Linker.nativeLinker()

    override fun loadLibrary(file: String): FfiLibrary {
        val lookup = try {
            SymbolLookup.libraryLookup(file, Arena.global())
        } catch (e: IllegalArgumentException) {
            throw LoadLibraryFailedException("Lookup: ${e.message}", e)
        }
        return JvmFfiLibrary(this, file, lookup)
    }

    override fun defineStruct(vararg fields: InputArgType<*>): FfiStructDefinition {
        return JvmFfiStructDefinition(fields)
    }

    override fun allocBytes(size: Long): FfiBuffer {
        return JvmFfiBuffer(size)
    }

    override fun close() {
    }

    override fun makeInputArgChar() = InputArgTypeByte
    override fun makeInputArgInt() = InputArgTypeInt
    override fun makeInputArgLong() = InputArgTypeLong
    override fun makeInputArgFloat() = InputArgTypeFloat
    override fun makeInputArgDouble() = InputArgTypeDouble
    override fun makeInputArgString() = InputArgTypeString
    override fun makeInputArgPtr() = InputArgTypePtr
    override fun makeInputArgBoolean() = InputArgTypeBoolean
    override fun makeInputArgStruct(structDefinition: FfiStructDefinition) = InputArgTypeStruct(structDefinition as JvmFfiStructDefinition)
    override fun <T> makeInputArgPointerToArg(type: InputArgType<T>): InputArgType<T> = InputArgTypePtrArg(type as JvmInputArgType<T>)

    override fun makeInputArgTypeVoid() = InputArgTypeVoid

    override fun makeNullPtr() = JvmFfiPointer(MemorySegment.NULL)

    companion object {
        object InputArgTypeByte : JvmInputArgType<Byte>(JAVA_BYTE) {
            override fun makeNativeValue(arena: Arena, arg: Any) = arg as Byte
            override fun makeJvmValue(arg: Any?) = arg as Byte
            override fun readValueAtOffset(buf: MemorySegment, offset: Long) = buf.get(JAVA_BYTE, offset)
            override fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any) = buf.set(JAVA_BYTE, offset, value as Byte)
        }

        object InputArgTypeInt : JvmInputArgType<Int>(JAVA_INT) {
            override fun makeNativeValue(arena: Arena, arg: Any) = arg as Int
            override fun makeJvmValue(arg: Any?) = arg as Int
            override fun readValueAtOffset(buf: MemorySegment, offset: Long) = buf.get(JAVA_INT, offset)
            override fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any) = buf.set(JAVA_INT, offset, value as Int)
        }

        object InputArgTypeLong : JvmInputArgType<Long>(JAVA_LONG) {
            override fun makeNativeValue(arena: Arena, arg: Any) = arg as Long
            override fun makeJvmValue(arg: Any?) = arg as Long
            override fun readValueAtOffset(buf: MemorySegment, offset: Long) = buf.get(JAVA_LONG, offset)
            override fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any) = buf.set(JAVA_LONG, offset, value as Long)
        }

        object InputArgTypeFloat : JvmInputArgType<Float>(JAVA_FLOAT) {
            override fun makeNativeValue(arena: Arena, arg: Any) = arg as Float
            override fun makeJvmValue(arg: Any?) = arg as Float
            override fun readValueAtOffset(buf: MemorySegment, offset: Long) = buf.get(JAVA_FLOAT, offset)
            override fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any) = buf.set(JAVA_FLOAT, offset, value as Float)
        }

        object InputArgTypeDouble : JvmInputArgType<Double>(JAVA_DOUBLE) {
            override fun makeNativeValue(arena: Arena, arg: Any) = arg as Double
            override fun makeJvmValue(arg: Any?) = arg as Double
            override fun readValueAtOffset(buf: MemorySegment, offset: Long) = buf.get(JAVA_DOUBLE, offset)
            override fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any) = buf.set(JAVA_DOUBLE, offset, value as Double)
        }

        object InputArgTypeString : JvmInputArgType<String?>(ADDRESS) {
            override fun makeNativeValue(arena: Arena, arg: Any): MemorySegment = arena.allocateUtf8String(arg as String)
            override fun makeJvmValue(arg: Any?): String = (arg as MemorySegment).getUtf8String(0)
            override fun readValueAtOffset(buf: MemorySegment, offset: Long): String = buf.get(ADDRESS, offset).getUtf8String(0)
            override fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any) {
                buf.set(ADDRESS, offset, arena.allocateUtf8String(value as String))
            }
        }

        object InputArgTypePtr : JvmInputArgType<FfiPointer>(ADDRESS) {
            override fun makeNativeValue(arena: Arena, arg: Any) = (arg as JvmFfiPointer).ptr
            override fun makeJvmValue(arg: Any?) = JvmFfiPointer(arg as MemorySegment)
            override fun readValueAtOffset(buf: MemorySegment, offset: Long) = makeJvmValue(buf.get(ADDRESS, offset))
            override fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any) {
                buf.set(ADDRESS, offset, (value as JvmFfiPointer).ptr)
            }
        }

        object InputArgTypeBoolean : JvmInputArgType<Boolean>(JAVA_BOOLEAN) {
            override fun makeNativeValue(arena: Arena, arg: Any) = arg as Boolean
            override fun makeJvmValue(arg: Any?) = arg as Boolean
            override fun readValueAtOffset(buf: MemorySegment, offset: Long) = buf.get(JAVA_BOOLEAN, offset)
            override fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any) = buf.set(JAVA_BOOLEAN, offset, value as Boolean)
        }

        object InputArgTypeVoid : JvmInputArgType<FfiManager.VoidWrapper>(JAVA_INT) {
            override fun makeNativeValue(arena: Arena, arg: Any) = error("Attempt to convert a value to void")
            override fun makeJvmValue(arg: Any?): FfiManager.VoidWrapper {
                require(arg == null)
                return FfiManager.VoidWrapper
            }

            override fun readValueAtOffset(buf: MemorySegment, offset: Long) = error("Attempt to convert a value to void")
            override fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any) {
                error("Attempt to convert a value to void")
            }
        }

        class InputArgTypeStruct(val structDefinition: JvmFfiStructDefinition) : JvmInputArgType<StructResult>(structDefinition.makeLayout()) {
            override fun makeNativeValue(arena: Arena, arg: Any): Any {
                val argArray = arg as JvmFfiStructDefinition.JvmStructResult
                val buf = arena.allocate(typeLayout)
                if (argArray.size() != structDefinition.structFieldList.size) {
                    throw IllegalStateException("Argument array size must match struct size")
                }
                structDefinition.fields.forEachIndexed { i, f ->
                    val f0 = f as JvmInputArgType<*>
                    val value = argArray.getAtIndex(i)
                    f0.writeJvmValueAtOffset(arena, buf, structDefinition.structFieldList[i].offset, value)
                }
                return buf
            }

            override fun makeJvmValue(arg: Any?): StructResult {
                val buf = arg as MemorySegment
                return structDefinition.makeStructResult(buf, 0)
            }

            // raylib:UpdateCamera (raylib:initCamera3D ⍬ ; 0)
            override fun readValueAtOffset(buf: MemorySegment, offset: Long): StructResult {
                return structDefinition.makeStructResult(buf, offset)
            }

            override fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any) {
                structDefinition.writeStructResult(arena, buf, offset, value as StructResult)
            }
        }

        class InputArgTypePtrArg<T>(val innerType: JvmInputArgType<T>) : JvmInputArgType<T>(ADDRESS) {
            override fun makeNativeValue(arena: Arena, arg: Any): Any {
                val ptr = arena.allocate(innerType.typeLayout)
                innerType.writeJvmValueAtOffset(arena, ptr, 0, arg)
                return ptr
            }

            override fun makeJvmValue(arg: Any?): T {
                val ptr = (arg as MemorySegment).reinterpret(innerType.typeLayout.byteSize())
                return innerType.readValueAtOffset(ptr, 0)
            }

            override fun readValueAtOffset(buf: MemorySegment, offset: Long): T {
                val ptr = buf.get(ADDRESS, offset).reinterpret(innerType.typeLayout.byteSize())
                return innerType.readValueAtOffset(ptr, 0)
            }

            override fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any) {
                val valueBuf = arena.allocate(innerType.typeLayout)
                innerType.writeJvmValueAtOffset(arena, valueBuf, 0, value)
                buf.set(ADDRESS, offset, valueBuf)
            }
        }
    }
}

class JvmFfiStructDefinition(val fields: Array<out InputArgType<*>>) : FfiStructDefinition {
    override val size: Long
    val structFieldList: List<JvmStructField>

    init {
        var currOffset = 0L
        structFieldList = fields.map { field ->
            val layout = (field as JvmInputArgType).typeLayout
            val alignment = layout.byteAlignment()
            // Alignment is guaranteed to be a power of two, so subtracting 1 gives us the mask
            val mask = (alignment - 1).inv()
            var newOffset = currOffset and mask
            if (newOffset != currOffset) {
                // Offset didn't have the correct alignment, so round up
                newOffset = (currOffset + alignment) and mask
            }
            currOffset = newOffset + layout.byteSize()
            JvmStructField(layout, newOffset)
        }
        size = currOffset
    }

    override fun derivePtr(base: FfiPointer, index: Int): FfiPointer {
        val p = base as JvmFfiPointer
        return JvmFfiPointer(p.ptr.asSlice(structFieldList[index].offset))
    }

    override fun makeStructResult(values: Array<Any>): StructResult {
        return JvmStructResult(values)
    }

    fun makeLayout(): MemoryLayout {
        val elements = ArrayList<MemoryLayout>()
        var pos = 0L
        structFieldList.forEach { field ->
            if (field.offset > pos) {
                elements.add(MemoryLayout.paddingLayout(field.offset - pos))
            }
            elements.add(field.layout)
            pos = field.offset + field.layout.byteSize()
        }
        return MemoryLayout.structLayout(*elements.toTypedArray())
    }

    fun makeStructResult(buf: MemorySegment, offset: Long): StructResult {
//        println("making struct result from address=${buf}, at offset=${offset}")
        val elements = Array(fields.size) { i ->
            val field = fields[i] as JvmInputArgType<*>
//            println("    reading element ${i}: ${field.typeLayout}, from offset=${structFieldList[i].offset}")
            val res = field.readValueAtOffset(buf, offset + structFieldList[i].offset)
            require(res != null)
            res
        }
        return JvmStructResult(elements)
    }

    fun writeStructResult(arena: Arena, buf: MemorySegment, offset: Long, structResult: StructResult) {
        require(fields.size == structResult.size())
//        println("writing struct result to ptr=${buf}, offset=${offset}")
        fields.forEachIndexed { i, field ->
            val s = structFieldList[i]
//            println("    writing to field ${i} at offset=${s.offset}, type=${field}")
            (field as JvmInputArgType).writeJvmValueAtOffset(arena, buf, offset + s.offset, structResult.getAtIndex(i))
        }
    }

    class JvmStructField(val layout: MemoryLayout, val offset: Long)

    class JvmStructResult(val elements: Array<Any>) : StructResult {
        override fun getAtIndex(index: Int) = elements[index]
        override fun size() = elements.size
        override fun toString() = "JvmStructResult[elements=${ArrayUtils.toString(elements)}]"
    }
}

actual fun ffiManager(): FfiManager {
    return JvmFfiManager()
}

abstract class JvmInputArgType<T>(val typeLayout: MemoryLayout) : InputArgType<T> {
    abstract fun makeNativeValue(arena: Arena, arg: Any): Any
    abstract fun makeJvmValue(arg: Any?): T
    abstract fun readValueAtOffset(buf: MemorySegment, offset: Long): T
    abstract fun writeJvmValueAtOffset(arena: Arena, buf: MemorySegment, offset: Long, value: Any)
}

class JvmFfiLibrary(
    private val mgr: JvmFfiManager,
    override val name: String,
    private val lookup: SymbolLookup
) : FfiLibrary {
    override fun <R> findFunction(name: String, returnType: InputArgType<R>, argTypes: List<InputArgType<*>>): FfiFunctionDescriptor<R>? {
        val ptr = lookup.find(name)
        if (ptr.isEmpty) {
            return null
        }
        return JvmFfiFunctionDescriptor(mgr, ptr.get(), returnType as JvmInputArgType<R>, argTypes.map { v -> v as JvmInputArgType<*> })
    }
}

class JvmFfiFunctionDescriptor<R>(
    mgr: JvmFfiManager,
    ptr: MemorySegment,
    override val returnType: JvmInputArgType<R>,
    override val argTypes: List<JvmInputArgType<*>>
) : FfiFunctionDescriptor<R> {
    private val handle: MethodHandle

    init {
        val inputLayoutList = argTypes.map(JvmInputArgType<*>::typeLayout).toTypedArray()
        val fnDescriptor = if (returnType is JvmFfiManager.Companion.InputArgTypeVoid) {
            FunctionDescriptor.ofVoid(*inputLayoutList)
        } else {
            FunctionDescriptor.of(returnType.typeLayout, *inputLayoutList)
        }
        handle = mgr.linker.downcallHandle(ptr, fnDescriptor)
    }

    private fun processResultArgs(inputArgs: List<Any>, outputParams: Set<Int>?): List<Any?>? {
        return if (outputParams == null) {
            null
        } else {
            argTypes.mapIndexed { i, type ->
                if (outputParams.contains(i)) {
                    type.makeJvmValue(inputArgs[i])
                } else {
                    null
                }
            }
        }
    }

    override fun call(args: List<Any>, outputParams: Set<Int>?): FfiFunctionDescriptor.CallResult<R> {
        Arena.ofConfined().use { arena ->
            val inputArgs = convertInputArgs(arena, args)
            val rtCopy = returnType
            if (rtCopy is JvmFfiManager.Companion.InputArgTypeStruct) {
                val newArgs = ArrayList<Any>()
                newArgs.add(arena)
                newArgs.addAll(inputArgs)
                val res = handle.invokeWithArguments(newArgs)
                return FfiFunctionDescriptor.CallResult(returnType.makeJvmValue(res), processResultArgs(inputArgs, outputParams))
            } else {
                val res = handle.invokeWithArguments(inputArgs)
                return FfiFunctionDescriptor.CallResult(returnType.makeJvmValue(res), processResultArgs(inputArgs, outputParams))
            }
        }
    }

    private fun convertInputArgs(arena: Arena, input: List<Any>): List<Any> {
        require(argTypes.size == input.size) { "Expected input argument size: ${argTypes.size}, got: ${input.size}" }
        return input.mapIndexed { i, argValue ->
            argTypes[i].makeNativeValue(arena, argValue)
        }
    }
}

class JvmFfiPointer(val ptr: MemorySegment) : FfiPointer {

    override fun readLongAtOffset(index: Long) = ptr.getAtIndex(JAVA_LONG, index)
    override fun writeLongAtOffset(index: Long, value: Long) = ptr.setAtIndex(JAVA_LONG, index, value)
    override fun readByteAtOffset(index: Long) = ptr.getAtIndex(JAVA_BYTE, index)
    override fun writeByteAtOffset(index: Long, value: Byte) = ptr.setAtIndex(JAVA_BYTE, index, value)

    override fun copyBytesToArray(offset: Long, buf: ByteArray, bufPosition: Int, size: Int) {
        ptr.reinterpret(offset + size).asSlice(offset).asByteBuffer().get(buf, bufPosition, size)
    }

    override fun copyBytesFromArray(offset: Long, buf: ByteArray, bufPosition: Int, size: Int) {
        ptr.reinterpret(offset + size).asSlice(offset).asByteBuffer().put(buf, bufPosition, size)
    }

    override fun toString() = "JvmFfiPointer[ptr=${ptr}]"
}

class JvmFfiBuffer(size: Long) : FfiBuffer {
    private val arena = Arena.ofConfined()
    private val ptr = arena.allocate(size)

    override fun pointerToOffset(offset: Long): FfiPointer {
        return JvmFfiPointer(ptr.asSlice(offset))
    }

    override fun close() {
        arena.close()
    }

    override fun toString() = "JvmFfiBuffer[ptr=${ptr}]"
}
