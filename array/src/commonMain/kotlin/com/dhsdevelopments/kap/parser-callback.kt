package com.dhsdevelopments.kap

interface ParserCallbacks {
    fun begin(pos: Position, level: ParserLevel)
    fun endExpression(pos: Position, instr: Instruction)
    fun endFunction(pos: Position, fn: APLFunction)
    fun endEmpty(pos: Position)
    fun exception(pos: Position, exception: ParseException)

    enum class ParserLevel(val level: Int) {
        LIST(0), BOOLEAN(1), VALUE(2), LEFT_ARG(3)
    }
}

class ParserCallbackManager(val parser: APLParser, val callbacks: List<ParserCallbacks>) {
    private fun callCallbacks(fn: (ParserCallbacks) -> Unit) {
        callbacks.forEach { cb -> fn(cb) }
    }

    private fun <T> callbackHandler(level: ParserCallbacks.ParserLevel, startPos: Position, fn: () -> T, processResults: (res: T) -> Unit): T {
        callCallbacks { cb -> cb.begin(startPos, level) }
        val res = try {
            fn()
        } catch (e: ParseException) {
            callCallbacks { cb -> cb.exception(startPos.expandToStart(parser.tokeniser.currentPos), e) }
            throw e
        }
        processResults(res)
        return res
    }

    fun withExpressionParser(
        level: ParserCallbacks.ParserLevel,
        startPos: Position = parser.tokeniser.currentPos,
        fn: () -> ParseResultHolder
    ): ParseResultHolder {
        return callbackHandler(level, startPos, fn) { res ->
            val pos = res.pos //startPos.expandToStart(parser.tokeniser.currentPos)
            when (res) {
                is ParseResultHolder.EmptyParseResult -> callCallbacks { cb -> cb.endEmpty(pos) }
                is ParseResultHolder.InstrParseResult -> callCallbacks { cb -> cb.endExpression(pos, res.instr) }
                is ParseResultHolder.FnParseResult -> callCallbacks { cb -> cb.endFunction(pos, res.fn) }
            }
        }
//        callCallbacks { cb -> cb.begin(startPos, level) }
//        val res = try {
//            fn()
//        } catch (e: ParseException) {
//            callCallbacks { cb -> cb.exception(startPos.expandToStart(parser.tokeniser.currentPos), e) }
//            throw e
//        }
//        val pos = res.pos //startPos.expandToStart(parser.tokeniser.currentPos)
//        when (res) {
//            is ParseResultHolder.EmptyParseResult -> callCallbacks { cb -> cb.endEmpty(pos) }
//            is ParseResultHolder.InstrParseResult -> callCallbacks { cb -> cb.endExpression(pos, res.instr) }
//            is ParseResultHolder.FnParseResult -> callCallbacks { cb -> cb.endFunction(pos, res.fn) }
//        }
//        return res
    }

    fun withValueParser(level: ParserCallbacks.ParserLevel, startPos: Position = parser.tokeniser.currentPos, fn: () -> Instruction): Instruction {
        return callbackHandler(level, startPos, fn) { res ->
            callCallbacks { cb -> cb.endExpression(res.pos, res) }
        }
    }
}

class ParseTreeResultCallbacks : ParserCallbacks {
    val root = arrayListOf<Node>()
    private val stack = ArrayList<ExpressionCollector>()

    override fun begin(pos: Position, level: ParserCallbacks.ParserLevel) {
        stack.add(ExpressionCollector(level))
    }

    override fun endEmpty(pos: Position) {
        processEndBlock { collector ->
            Node.Empty(collector.level, collector.subnodes, pos)
        }
    }

    override fun endExpression(pos: Position, instr: Instruction) {
        processEndBlock { collector ->
            Node.Expression(collector.level, collector.subnodes, pos, instr)
        }
    }

    override fun endFunction(pos: Position, fn: APLFunction) {
        processEndBlock { collector ->
            Node.Function(collector.level, collector.subnodes, pos, fn)
        }
    }

    override fun exception(pos: Position, exception: ParseException) {
        processEndBlock { collector ->
            Node.Exception(collector.level, collector.subnodes, pos, exception)
        }
    }

    private fun processEndBlock(fn: (ExpressionCollector) -> Node) {
        val collector = stack.removeLast()
        val node = fn(collector)
        if (stack.isEmpty()) {
            root.add(node)
        } else {
            stack.last().subnodes.add(node)
        }
    }

    sealed class Node(val level: ParserCallbacks.ParserLevel, val subnodes: List<Node>, val pos: Position) {
        class Empty(level: ParserCallbacks.ParserLevel, subnodes: List<Node>, pos: Position) : Node(level, subnodes, pos)
        class Expression(level: ParserCallbacks.ParserLevel, subnodes: List<Node>, pos: Position, val instr: Instruction) : Node(level, subnodes, pos)
        class Function(level: ParserCallbacks.ParserLevel, subnodes: List<Node>, pos: Position, val fn: APLFunction) : Node(level, subnodes, pos)
        class Exception(level: ParserCallbacks.ParserLevel, subnodes: List<Node>, pos: Position, val exception: ParseException) : Node(level, subnodes, pos)
    }

    private class ExpressionCollector(val level: ParserCallbacks.ParserLevel) {
        val subnodes = ArrayList<Node>()
    }
}
