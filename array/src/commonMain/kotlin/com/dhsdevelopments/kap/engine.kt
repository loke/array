package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.builtins.*
import com.dhsdevelopments.kap.builtins.math.DivisorsAPLFunction
import com.dhsdevelopments.kap.builtins.math.FactorAPLFunction
import com.dhsdevelopments.kap.json.JsonAPLModule
import com.dhsdevelopments.kap.mpthread.MPLock
import com.dhsdevelopments.kap.mpthread.makeMPThreadLocal
import com.dhsdevelopments.kap.mpthread.withLocked
import com.dhsdevelopments.kap.optimiser.Optimiser
import com.dhsdevelopments.kap.optimiser.StandardOptimiser
import com.dhsdevelopments.kap.optimiser.ZeroOptimiser
import com.dhsdevelopments.kap.syntax.CustomSyntax
import kotlin.concurrent.Volatile
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract
import kotlin.random.Random
import kotlin.reflect.KClass
import kotlin.time.Duration
import kotlin.time.TimeSource

/**
 * Class that holds information about the context in which a function is materialised in the code.
 * In short, an [APLFunctionDescriptor] describes a function itself. When a function call is
 * expressed in code, the [APLFunctionDescriptor.make] function is called to create an instance
 * of [APLFunction] which represents a particular invocation of the function at a particular
 * place in the code. This place is represented by an instance of this class.
 *
 * @param pos The position in the code where the function was materialised
 * @param env The environment where the function is called
 */
class FunctionInstantiation(val engine: Engine, val pos: Position, val env: Environment) {
    inline fun updatePos(fn: (Position) -> Position): FunctionInstantiation {
        return FunctionInstantiation(engine, fn(pos), env)
    }

    fun maybeMarkEscape(vararg fns: APLFunction) {
        if (fns.any { it.allCapturedEnvironments().isNotEmpty() }) {
            env.markCanEscape()
        }
    }
}

interface APLFunctionDescriptor {
    fun make(instantiation: FunctionInstantiation): APLFunction
    fun copy(): APLFunctionDescriptor = this
}

class StorageStack private constructor() {
    val stack = ArrayList<StorageStackFrame>()

    constructor(rootFrame: StorageStackFrame) : this() {
        stack.add(rootFrame)
    }

    constructor(prevStack: List<StorageStackFrame>) : this() {
        stack.addAll(prevStack)
    }

    fun copy() = StorageStack(stack)

    @Suppress("WRONG_INVOCATION_KIND")
    @OptIn(ExperimentalContracts::class)
    inline fun withStackFrame(environment: Environment, name: String, pos: Position, crossinline fn: (StorageStackFrame) -> APLValue): APLValue {
        contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
        val frame = StorageStackFrame(this, environment, name, pos)
        stack.add(frame)
        var result: APLValue
        @Suppress("LiftReturnOrAssignment")
        try {
            result = fn(frame)
        } catch (retValue: ReturnValue) {
            if (retValue.returnEnvironment === environment) {
                result = retValue.value
            } else {
                throw retValue
            }
        } finally {
            val removed = stack.removeLast()
            require(removed === frame) { "Removed frame does not match inserted frame" }
            frame.fireReleaseCallbacks()
        }
        return result
    }

    fun findStorage(storageRef: StackStorageRef): VariableHolder {
        return findStorageFromFrameIndexAndOffset(storageRef.frameIndex, storageRef.storageOffset)
    }

    fun findStorageFromFrameIndexAndOffset(frameIndex: Int, storageOffset: Int): VariableHolder {
        val frame = stack[if (frameIndex == -2) 0 else stack.size - frameIndex - 1]
        return frame.storageList[storageOffset]
    }

    fun currentFrame() = stack.last()

    class StorageStackFrame private constructor(val environment: Environment, val name: String, val pos: Position?, var storageList: Array<VariableHolder>) {
        private var releaseCallbacks: MutableList<() -> Unit>? = null

        constructor(stack: StorageStack, environment: Environment, name: String, pos: Position?)
                : this(environment, name, pos, computeStorageList(stack, environment))

        constructor(environment: Environment)
                : this(environment, "root", null, computeRootFrame(environment))

        fun pushReleaseCallback(callback: () -> Unit) {
            val list = releaseCallbacks
            if (list == null) {
                val updated = ArrayList<() -> Unit>()
                updated.add(callback)
                releaseCallbacks = updated
            } else {
                list.add(callback)
            }
        }

        fun fireReleaseCallbacks() {
            val list = releaseCallbacks
            @Suppress("IfThenToSafeAccess")
            if (list != null) {
                list.asReversed().forEach { fn ->
                    fn()
                }
            }
        }
    }
}

@Suppress("NOTHING_TO_INLINE")
private inline fun computeStorageList(stack: StorageStack, environment: Environment): Array<VariableHolder> {
    val localStorageSize = environment.storageList.size
    val externalStorageList = environment.externalStorageList
    val externalStorageSize = externalStorageList.size
    return Array(localStorageSize + externalStorageSize) { i ->
        if (i < localStorageSize) {
            VariableHolder()
        } else {
            val ref = externalStorageList[i - localStorageSize]
            // We don't subtract 1 from stackIndex here because at this point the element has not been added to the stack yet
            val stackIndex = if (ref.frameIndex == -2) 0 else stack.stack.size - ref.frameIndex
            val frame = stack.stack[stackIndex]
            frame.storageList[ref.storageOffset]
        }
    }
}

private fun computeRootFrame(env: Environment): Array<VariableHolder> {
    require(env.externalStorageList.isEmpty()) { "Root frame should not have external refs" }
    return Array(env.storageList.size) { VariableHolder() }
}

@OptIn(ExperimentalContracts::class)
inline fun <T> withPossibleSavedStack(frame: StorageStack.StorageStackFrame?, fn: () -> T): T {
    contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
    return if (frame == null) {
        fn()
    } else {
        withSavedStackFrame(frame) {
            fn()
        }
    }
}

@OptIn(ExperimentalContracts::class)
inline fun <T> withSavedStackFrame(frame: StorageStack.StorageStackFrame, fn: () -> T): T {
    contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
    val stack = currentStorageStack().stack
    stack.add(frame)
    try {
        return fn()
    } finally {
        val removed = stack.removeLast()
        require(removed === frame) { "Removed frame does not match inserted frame" }
    }
}

/**
 * A handler that is registered by [Engine.registerClosableHandler] which is responsible for implementing
 * the backend for the `close` Kap function.
 */
fun interface ClosableHandler<T : APLValue> {
    /**
     * Close the underlying object.
     */
    fun close(value: T)
}

expect fun <T> withThreadLocalStorageStackAssigned(stack: StorageStack?, fn: () -> T): T
expect fun currentStorageStack(): StorageStack
expect fun currentStorageStackOrNull(): StorageStack?

@Suppress("LEAKED_IN_PLACE_LAMBDA", "WRONG_INVOCATION_KIND")
@OptIn(ExperimentalContracts::class)
inline fun <T> withThreadLocalsUnassigned(crossinline fn: () -> T): T {
    contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
    require(currentStorageStackOrNull() != null)
    return withThreadLocalStorageStackAssigned(null) {
        fn()
    }
}

interface SystemParameterProvider {
    fun lookupValue(): APLValue
    fun updateValue(newValue: APLValue, pos: Position): Unit = throwAPLException(UnmodifiableSystemParameterException(pos))
}

class ConstantStringSystemParameterProvider(val value: String) : SystemParameterProvider {
    override fun lookupValue() = APLString.make(value)
}

class ConstantSymbolSystemParameterProvider(val name: Symbol) : SystemParameterProvider {
    override fun lookupValue() = APLSymbol(name)
}

class Engine private constructor(
    numComputeEngines: Int? = null,
    val defaultOptimiser: Optimiser,
    val forSyntaxCheck: Boolean
) {
    constructor(numComputeEngines: Int? = null, defaultOptimiser: Optimiser? = null) : this(numComputeEngines, defaultOptimiser ?: StandardOptimiser, false)

    private var closed = false
    private val functions = HashMap<Symbol, APLFunctionDescriptor>()
    private val operators = HashMap<Symbol, APLOperator>()
    private val functionDefinitionListeners = ArrayList<FunctionDefinitionListener>()
    private val functionAliases = HashMap<Symbol, Symbol>()
    private val namespaces = NamespaceList()
    private val customSyntaxSubEntries = HashMap<Symbol, CustomSyntax>()
    private val customSyntaxEntries = HashMap<Symbol, CustomSyntax>()
    private val librarySearchPaths = ArrayList<String>()
    val modules = ArrayList<KapModule>()
    private val exportedSingleCharFunctions = initSingleCharFunctionList()

    var customRenderer: LambdaValue? = null
    val classManager = ClassManager(this)
    val commandManager = CommandManager(this)

    /**
     * Lock that protects the update listener list for any variable.
     * This is a global lock rather than a lock on the [VariableHolder] itself
     * in order to minimise the amount of work needed to initialise an instance
     * of [VariableHolder]. Since registering listeners on variables is such
     * a rare event, this is acceptable.
     */
    val updateListenerLock = MPLock()

    val rootEnvironment = Environment("root", null)
    val rootStackFrame = StorageStack.StorageStackFrame(rootEnvironment)
    var standardOutput: CharacterOutput = NullCharacterOutput()
    var standardInput: CharacterProvider = NullCharacterProvider()
    private val timerHandler: TimerHandler? = makeTimerHandler(this)

    val coreNamespace get() = namespaces.coreNamespace
    val keywordNamespace get() = namespaces.keywordNamespace
    val initialNamespace get() = namespaces.initialNamespace

    var currentNamespace = initialNamespace
    val closableHandlers = HashMap<KClass<out APLValue>, ClosableHandler<*>>()
    val backgroundDispatcher = makeBackgroundDispatcher(numComputeEngines ?: numCores())
    val inComputeThread = makeMPThreadLocal<Boolean>()
    val systemParameters = HashMap<Symbol, SystemParameterProvider>()
    val standardSymbols = StandardSymbols(this)
    val nativeData = makeNativeData()
    var random: Random = Random

    @Volatile
    private var breakPending = false

    init {
        // Other symbols also needs exporting
        for (name in listOf("⍵", "⍺", "⍹", "⍶")) {
            coreNamespace.internAndExport(name)
        }

        // core functions
        registerNativeFunction("+", AddAPLFunction())
        registerNativeFunction("-", SubAPLFunction())
        registerNativeFunction("×", MulAPLFunction())
        registerNativeFunction("÷", DivAPLFunction())
        registerNativeFunction("⋆", PowerAPLFunction())
        registerNativeFunction("⍟", LogAPLFunction())
        registerNativeFunction("⍳", IotaAPLFunction())
        registerNativeFunction("⍴", RhoAPLFunction())
        registerNativeFunction("⊢", IdentityAPLFunction())
        registerNativeFunction("⊣", HideAPLFunction())
        registerNativeFunction("=", EqualsAPLFunction())
        registerNativeFunction("≠", NotEqualsAPLFunction())
        registerNativeFunction("<", LessThanAPLFunction())
        registerNativeFunction(">", GreaterThanAPLFunction())
        registerNativeFunction("≤", LessThanEqualAPLFunction())
        registerNativeFunction("≥", GreaterThanEqualAPLFunction())
        registerNativeFunction("⌷", AccessFromIndexAPLFunction())
        registerNativeFunction("⊂", EncloseAPLFunction())
        registerNativeFunction("⊃", DiscloseAPLFunction())
        registerNativeFunction("∧", AndAPLFunction())
        registerNativeFunction("∨", OrAPLFunction())
        registerNativeFunction(",", ConcatenateAPLFunctionLastAxis())
        registerNativeFunction("⍪", ConcatenateAPLFunctionFirstAxis())
        registerNativeFunction("↑", TakeAPLFunction())
        registerNativeFunction("?", RandomAPLFunction())
        registerNativeFunction("⌽", RotateHorizFunction())
        registerNativeFunction("⊖", RotateVertFunction())
        registerNativeFunction("↓", DropAPLFunction())
        registerNativeFunction("⍉", TransposeFunction())
        registerNativeFunction("⌊", MinAPLFunction())
        registerNativeFunction("⌈", MaxAPLFunction())
        registerNativeFunction("|", ModAPLFunction())
        registerNativeFunction("∘", NullFunction())
        registerNativeFunction("≡", CompareFunction())
        registerNativeFunction("≢", CompareNotEqualFunction())
        registerNativeFunction("∊", MemberFunction())
        registerNativeFunction("⍋", GradeUpFunction())
        registerNativeFunction("⍒", GradeDownFunction())
        registerNativeFunction("⍷", FindFunction())
        registerNativeFunction("⫽", SelectElementsLastAxisFunction())
        registerNativeFunction("/", SelectElementsLastAxisFunction())
        registerNativeFunction("⌿", SelectElementsFirstAxisFunction())
        registerNativeFunction("\\", ExpandLastAxisFunction())
        registerNativeFunction("⍀", ExpandFirstAxisFunction())
        registerNativeFunction("∼", NotAPLFunction())
        registerNativeFunction("⍕", FormatAPLFunction())
        registerNativeFunction("⍸", WhereAPLFunction())
        registerNativeFunction("∪", UniqueFunction())
        registerNativeFunction("⍲", NandAPLFunction())
        registerNativeFunction("⍱", NorAPLFunction())
        registerNativeFunction("∩", IntersectionAPLFunction())
        registerNativeFunction("!", BinomialAPLFunction())
        registerNativeFunction("⍎", ParseNumberFunction())
        registerNativeFunction("%", CaseFunction())
        registerNativeFunction("⊆", PartitionedEncloseFunction())
        registerNativeFunction("⊇", PickAPLFunction())
        registerNativeFunction("cmp", CompareObjectsFunction())
        registerNativeFunction("→", ReturnFunction())
        registerNativeFunction("⍮", PairAPLFunction())
        registerNativeFunction("⫇", GroupFunction())
        registerNativeFunction("…", RangeFunction())

        // hash tables
        registerNativeFunction("map", MapAPLFunction())
        registerNativeFunction("mapGet", MapGetAPLFunction())
        registerNativeFunction("mapPut", MapPutAPLFunction())
        registerNativeFunction("mapRemove", MapRemoveAPLFunction())
        registerNativeFunction("mapToArray", MapKeyValuesFunction())
        registerNativeFunction("mapSize", MapSizeFunction())
        registerNativeFunction("mapKeys", MapKeysFunction())

        // io functions
        registerNativeFunction("read", ReadFunction(), "io")
        registerNativeFunction("write", WriteFunction(), "io")
        registerNativeFunction("print", PrintAPLFunction(), "io")
        registerNativeFunction("println", PrintLnAPLFunction(), "io")
        registerNativeFunction("readLine", ReadLineFunction(), "io")
        registerNativeFunction("writeCsv", WriteCsvFunction(), "io")
        registerNativeFunction("readCsv", ReadCsvFunction(), "io")
        registerNativeFunction("readFile", ReadFileFunction(), "io")
        registerNativeFunction("load", LoadFunction(), "io")
        registerNativeFunction("readdir", ReaddirFunction(), "io")
        registerNativeFunction("close", CloseAPLFunction())
        registerNativeFunction("toHtml", ToHtmlFunction(), "io")
        registerNativeFunction("fromHtmlTable", FromHtmlTableFunction(), "io")

        // http functions
        registerNativeFunction("get", HttpRequestFunction(HttpMethod.GET, false), "http")
        registerNativeFunction("post", HttpRequestFunction(HttpMethod.POST, true), "http")
        registerNativeFunction("put", HttpRequestFunction(HttpMethod.PUT, true), "http")
        registerNativeFunction("delete", HttpRequestFunction(HttpMethod.DELETE, false), "http")

        // experimental new io api
        registerNativeFunction("open", OpenFunction(), "io2")
        registerNativeFunction("read", ReadFromStreamFunction(), "io2")
        registerNativeFunction("readLine", ReadLineFromStreamFunction(), "io2")
        registerNativeFunction("write", WriteToStreamFunction(), "io2")

        // time functions
        registerNativeFunction("sleep", SleepFunction(), "time")
        registerNativeFunction("timeMillis", TimeMillisFunction(), "time")
        registerNativeFunction("makeTimer", MakeTimerFunction(), "time")
        registerNativeFunction("toTimestamp", ToTimestampFunction(), "time")
        registerNativeFunction("fromTimestamp", FromTimestampFunction(), "time")
        registerNativeFunction("format", FormatTimestampFunction(), "time")
        registerNativeFunction("parse", ParseTimestampFunction(), "time")
        registerNativeFunction("throw", ThrowFunction())
        registerNativeOperator("catch", CatchOperator())
        registerNativeFunction("labels", LabelsFunction())
        registerNativeFunction("unwindProtect", UnwindProtectAPLFunction(), "int")
        registerNativeOperator("defer", DeferAPLOperator())
        registerNativeFunction("ensureGeneric", EnsureTypeFunction(ArrayMemberType.GENERIC), "int")
        registerNativeFunction("ensureLong", EnsureTypeFunction(ArrayMemberType.LONG), "int")
        registerNativeFunction("ensureDouble", EnsureTypeFunction(ArrayMemberType.DOUBLE), "int")
        registerNativeFunction("ensureBoolean", EnsureTypeFunction(ArrayMemberType.BOOLEAN), "int")
        registerNativeFunction("asBigint", AsBigintFunction(), "int")
        registerNativeFunction("asRational", AsRationalFunction(), "int")
        registerNativeOperator("atLeave", AtLeaveScopeOperator())
        registerNativeFunction("toList", ToListFunction())
        registerNativeFunction("≬", ToListFunction())
        registerNativeFunction("fromList", FromListFunction())
        registerNativeFunction("proto", AssignPrototypeFunction(), "int")
        registerNativeFunction("toBoolean", ToBooleanFunction())
        registerNativeFunction("toRational", RationaliseAPLFunction())
        registerNativeFunction("hashCode", HashCodeAPLFunction(), "int")
        registerNativeFunction("formatRational", RenderRationalFunction(), "int")
        registerNativeFunction("isKapString", IsStringFunction(), "int")

        // maths
        registerNativeFunction("sin", SinAPLFunction(), "math")
        registerNativeFunction("cos", CosAPLFunction(), "math")
        registerNativeFunction("tan", TanAPLFunction(), "math")
        registerNativeFunction("asin", AsinAPLFunction(), "math")
        registerNativeFunction("acos", AcosAPLFunction(), "math")
        registerNativeFunction("atan", AtanAPLFunction(), "math")
        registerNativeFunction("sinh", SinhAPLFunction(), "math")
        registerNativeFunction("cosh", CoshAPLFunction(), "math")
        registerNativeFunction("tanh", TanhAPLFunction(), "math")
        registerNativeFunction("asinh", AsinhAPLFunction(), "math")
        registerNativeFunction("acosh", AcoshAPLFunction(), "math")
        registerNativeFunction("atanh", AtanhAPLFunction(), "math")
        registerNativeFunction("√", SqrtAPLFunction())
        registerNativeFunction("gcd", GcdAPLFunction(), "math")
        registerNativeFunction("lcm", LcmAPLFunction(), "math")
        registerNativeFunction("numerator", NumeratorAPLFunction(), "math")
        registerNativeFunction("denominator", DenominatorAPLFunction(), "math")
        registerNativeFunction("factor", FactorAPLFunction(), "math")
        registerNativeFunction("divisors", DivisorsAPLFunction(), "math")
        registerNativeFunction("re", RealpartAPLFunction(), "math")
        registerNativeFunction("im", ImagpartAPLFunction(), "math")
        registerNativeFunction("floorc", ComplexFloorFunction(), "math")
        registerNativeFunction("ceilc", ComplexCeilFunction(), "math")
        registerNativeFunction("round", RoundNumFunction(), "math")

        // metafunctions
        registerNativeFunction("typeof", TypeofFunction())
        registerNativeFunction("isLocallyBound", IsLocallyBoundFunction())
        registerNativeFunction("comp", CompFunction())
        registerNativeFunction("sysparam", SystemParameterFunction())
        registerNativeFunction("valueInfo", InternalValueInfoFunction(), "int")
        registerNativeFunction("list", ListModulesFunction(), "mod")
        registerNativeFunction("hasLabels", HasLabelsFunction(), "int")
        registerNativeFunction("throwNative", ThrowNativeFunction(), "int")
        registerNativeFunction("intern", InternFunction(), "int")
        registerNativeFunction("symbolName", SymbolNameFunction(), "int")

        // operators
        registerNativeOperator("¨", ForEachOp())
        registerNativeOperator("/", ReduceOpLastAxis())
        registerNativeOperator("⌿", ReduceOpFirstAxis())
        registerNativeOperator("⌻", OuterJoinOp())
        registerNativeOperator("∙", OuterInnerJoinOp())
        registerNativeOperator("⍨", CommuteOp())
        registerNativeOperator("⍣", PowerAPLOperator())
        registerNativeOperator("\\", ScanLastAxisOp())
        registerNativeOperator("⍀", ScanFirstAxisOp())
        registerNativeOperator("⍤", RankOperator())
        registerNativeOperator("∵", BitwiseOp())
        registerNativeOperator("∘", ComposeOp())
        registerNativeOperator("⍥", OverOp())
        registerNativeOperator("∥", ParallelOp())
        registerNativeOperator("⍛", ReverseComposeOp())
        registerNativeOperator("˝", InverseFnOp())
        registerNativeOperator("⍢", StructuralUnderOp())
        registerNativeOperator("callAsync", CallAsyncOp())

        // function aliases                             
        functionAliases[coreNamespace.internAndExport("*")] = coreNamespace.internAndExport("⋆")
        functionAliases[coreNamespace.internAndExport("~")] = coreNamespace.internAndExport("∼")

        classManager.init()

        registerIoClosableHandlers(this)

        systemParameters[standardSymbols.renderer] = CustomRendererParameter(this)

        if (!forSyntaxCheck) {
            standardEngineInit()
        }

        KapLogger.v { "Engine initialised" }
    }

    /**
     * Engine initialisation that is only performed for non-syntax checked engines.
     */
    private fun standardEngineInit() {
        platformInit(this)

        addStandardCmds()

        addModule(UnicodeModule())
        addModule(JsonAPLModule())
        addModule(RegexpModule())
    }

    private fun addStandardCmds() {
        registerNativeFunction("registerCmd", RegisterCmdFunction(), "int")
        commandManager.registerCommandHandler("modlist", ModListCommandHandler())
        commandManager.registerCommandHandler("cd", ChangeWorkingDirectoryCommandHandler())
    }

    private fun initSingleCharFunctionList(): MutableSet<String> {
        return hashSetOf(
            "!", "#", "%", "&", "*", "+", ",", "-", "/", "<", "=", ">", "?", "^", "|",
            "~", "¨", "×", "÷", "↑", "→", "↓", "∊", "∘", "∧", "∨", "∩", "∪", "∼", "≠", "≡",
            "≢", "≤", "≥", "⊂", "⊃", "⊖", "⊢", "⊣", "⊤", "⊥", "⋆", "⌈", "⌊", "⌶", "⌷", "⌹",
            "⌻", "⌽", "⌿", "⍀", "⍉", "⍋", "⍎", "⍒", "⍕", "⍞", "⍟", "⍠", "⍣", "⍤", "⍥",
            "⍨", "⍪", "⍫", "⍱", "⍲", "⍳", "⍴", "⍵", "⍶", "⍷", "⍸", "⍹", "⍺", "◊",
            "○", "$", "¥", "χ", "\\", "∵", "⍓", "⫽", "⑊", "⊆", "⊇", "⍥", "∥", "⍛", "˝",
            "⍢", "√", "∙", "⌸", "≬", "⍮", "⫇", "…")
    }

    fun charIsSingleCharExported(ch: String) = exportedSingleCharFunctions.contains(ch)

    fun isCharSymbolDelimiter(ch: Int): Boolean {
        if (charIsSingleCharExported(charToString(ch))) {
            return true
        }
        if (isAlphanumeric(ch) || ch == ':'.code) {
            return false
        }
        return true
    }

    private fun checkNotClosed() {
        if (closed) {
            throw IllegalStateException("Engine is closed")
        }
    }

    fun close() {
        if (!closed) {
            backgroundDispatcher.close()
            modules.forEach(KapModule::close)
            nativeData.close()
            closed = true
            KapLogger.v { "Engine closed" }
        }
    }

    fun interruptEvaluation() {
        breakPending = true
        nativeUpdateBreakPending(this, true)
    }

    fun checkInterrupted(pos: Position? = null) {
        val pending = breakPending
        if (pending || nativeBreakPending(this)) {
            if (!isInComputeThread) {
                clearInterrupted()
            }
            throw APLEvaluationInterrupted(pos)
        }
    }

    fun clearInterrupted() {
        breakPending = false
        nativeUpdateBreakPending(this, false)
    }

    val isInComputeThread get() = inComputeThread.value == true

    fun findSymbolWithNamespace(name: String): Symbol? {
        val res = TokenGenerator.parseStringToSymbol(name) ?: return null
        val nsName = res.first
        val ns = if (nsName == null) currentNamespace else makeNamespace(nsName)
        return ns.internSymbol(res.second)
    }

    fun addModule(module: KapModule) {
        if (modules.any { m -> m.name == module.name }) {
            KapLogger.e { "Attempted to add module that was already registered: ${module.name}" }
        } else {
            try {
                module.init(this)
                modules.add(module)
            } catch (e: ModuleInitSkipped) {
                KapLogger.i(e) { "Skipped initialisation of module: ${module::class.simpleName}" }
            } catch (e: ModuleInitFailedException) {
                KapLogger.e(e) { "Failed to initialise module: ${module::class.simpleName}" }
            } catch (e: Exception) {
                close()
                throw e
            }
        }
    }

    inline fun <reified T : KapModule> findModule(): T? {
        return modules.filterIsInstance<T>().firstOrNull()
    }

    inline fun <reified T : KapModule> findModuleOrError(pos: Position? = null): T {
        return findModule() ?: throwAPLException(ModuleNotFound(T::class.simpleName ?: "empty name", pos))
    }

    fun addLibrarySearchPath(path: String) {
        val fixedPath = PathUtils.cleanupPathName(path)
        if (!librarySearchPaths.contains(fixedPath)) {
            librarySearchPaths.add(fixedPath)
        }
    }

    fun resolveLibraryFile(requestedFile: String): String? {
        if (requestedFile.isEmpty()) {
            return null
        }
        if (PathUtils.isAbsolutePath(requestedFile)) {
            return null
        }
        librarySearchPaths.forEach { path ->
            val name = "${path}/${requestedFile}"
            if (fileExists(name)) {
                return name
            }
        }
        return null
    }

    fun addFunctionDefinitionListener(listener: FunctionDefinitionListener) {
        functionDefinitionListeners.add(listener)
    }

    @Suppress("unused")
    fun removeFunctionDefinitionListener(listener: FunctionDefinitionListener) {
        functionDefinitionListeners.remove(listener)
    }

    fun registerFunction(name: Symbol, fn: APLFunctionDescriptor) {
        functions[name] = fn
        functionDefinitionListeners.forEach { it.functionDefined(name, fn) }
    }

    private fun registerNativeFunction(name: String, fn: APLFunctionDescriptor, namespaceName: String? = null, exported: Boolean = true) {
        val namespace = if (namespaceName == null) coreNamespace else makeNamespace(namespaceName)
        val sym = if (exported) namespace.internAndExport(name) else namespace.internSymbol(name)
        registerFunction(sym, fn)
    }

    fun registerOperator(name: Symbol, fn: APLOperator) {
        operators[name] = fn
        functionDefinitionListeners.forEach { it.operatorDefined(name, fn) }
    }

    private fun registerNativeOperator(name: String, fn: APLOperator, namespaceName: String? = null) {
        val namespace = if (namespaceName == null) coreNamespace else makeNamespace(namespaceName)
        val sym = namespace.internAndExport(name)
        registerOperator(sym, fn)
    }

    /**
     * Return a list of all global functions.
     */
    fun getFunctions() = functions.toList()

    fun getFunction(name: Symbol) = functions[resolveAlias(name)]
    fun getOperator(name: Symbol) = operators[resolveAlias(name)]

    fun createAnonymousSymbol(name: String? = null) =
        Symbol(if (name == null) "<anonymous>" else "<anonymous: ${name}>", NamespaceList.ANONYMOUS_SYMBOL_NAMESPACE_NAME)

    fun parse(source: SourceLocation, optimiser: Optimiser? = null, callbacks: List<ParserCallbacks> = emptyList()): Instruction {
        checkNotClosed()
        TokenGenerator(this, source).use { tokeniser ->
            val parser = APLParser(tokeniser, optimiser, callbacks)
            return parser.parseValueToplevel(EndOfFile)
        }
    }

    fun parseAndEvalWithFormat(
        source: SourceLocation,
        extraBindings: Map<Symbol, APLValue>? = null,
    ): FormattedEvalResult {
        return parseAndEvalWithPostProcessing(source, extraBindings = extraBindings) { context, result, evaluationTime ->
            val collapsed = result.collapse()
            val renderedResult = renderResult(context, collapsed)
            FormattedEvalResult(collapsed, renderedResult, evaluationTime)
        }
    }

    fun parseAndEval(
        source: SourceLocation,
        extraBindings: Map<Symbol, APLValue>? = null,
        collapseResult: Boolean = true
    ): APLValue {
        return parseAndEvalWithPostProcessing(source, extraBindings = extraBindings) { _, result, _ ->
            if (collapseResult) {
                result.collapse()
            } else {
                result
            }
        }
    }

    fun <T> parseAndEvalWithPostProcessing(
        source: SourceLocation,
        extraBindings: Map<Symbol, APLValue>? = null,
        postProcess: (RuntimeContext, APLValue, Duration) -> T
    ): T {
        val ts = TimeSource.Monotonic
        val startTime = ts.markNow()

        fun callPostProcess(context: RuntimeContext, result: APLValue): T {
            val currTime = ts.markNow()
            return postProcess(context, result, currTime - startTime)
        }

        TokenGenerator(this, source).use { tokeniser ->
            val parser = APLParser(tokeniser)
            if (extraBindings == null) {
                val instr = parser.parseValueToplevel(EndOfFile)
                rootEnvironment.escapeAnalysis()

                return withThreadLocalAssigned {
                    recomputeRootFrame()
                    val context = RuntimeContext(this)
                    val result = instr.evalWithContext(context)
                    callPostProcess(context, result)
                }
            } else {
                // Extra bindings are requested, so we have to create an inner environment to bind the variables
                val instr: Instruction
                val bindingsWithValues: Map<EnvironmentBinding, APLValue>
                val updatedEnv: Environment
                parser.withEnvironment { env ->
                    bindingsWithValues = extraBindings.map { (sym, value) ->
                        Pair(env.bindLocal(sym), value)
                    }.toMap()
                    instr = parser.parseValueToplevel(EndOfFile)
                    updatedEnv = env
                }
                rootEnvironment.escapeAnalysis()

                return withThreadLocalAssigned {
                    recomputeRootFrame()
                    val context = RuntimeContext(this)
                    val result = withLinkedContext(updatedEnv, "extraBindings", instr.pos) {
                        val stack = currentStorageStack()
                        bindingsWithValues.forEach { (binding, value) ->
                            val storage = stack.findStorage(StackStorageRef(binding))
                            storage.updateValue(value)
                        }
                        instr.evalWithContext(context)
                    }
                    callPostProcess(context, result)
                }
            }
        }
    }

    fun internSymbol(name: String, namespace: Namespace? = null, exported: Boolean = false): Symbol {
        val ns = namespace ?: currentNamespace
        return if (exported) ns.internAndExport(name) else ns.internSymbol(name)
    }

    fun makeNamespace(name: String, overrideDefaultImport: Boolean = false): Namespace {
        return namespaces.makeNamespace(name, overrideDefaultImport)
    }

    fun findNamespace(name: String): Namespace? {
        return namespaces.findNamespaceByNameString(name)
    }

    fun findNamespaceFromName(name: NamespaceName): Namespace {
        return namespaces.findNamespaceFromName(name)
    }

    private fun resolveAlias(name: Symbol) = functionAliases[name] ?: name

    /**
     * Returns true if the tokeniser should treat this symbol as self-evaluating.
     *
     * Note that this function should only be called from the tokeniser.
     */
    internal fun isSelfEvaluatingSymbol(name: Symbol) = name.namespace == keywordNamespace.name

    fun registerCustomSyntax(customSyntax: CustomSyntax) {
        customSyntaxEntries[customSyntax.name] = customSyntax
    }

    fun syntaxRulesForSymbol(name: Symbol): CustomSyntax? {
        return customSyntaxEntries[name]
    }

    fun registerCustomSyntaxSub(customSyntax: CustomSyntax) {
        customSyntaxSubEntries[customSyntax.name] = customSyntax
    }

    fun customSyntaxSubRulesForSymbol(name: Symbol): CustomSyntax? {
        return customSyntaxSubEntries[name]
    }

    @OptIn(ExperimentalContracts::class)
    inline fun <T> withSavedNamespace(fn: () -> T): T {
        contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
        val oldNamespace = currentNamespace
        try {
            return fn()
        } finally {
            currentNamespace = oldNamespace
        }
    }

    @OptIn(ExperimentalContracts::class)
    inline fun <T> withCurrentNamespace(namespace: Namespace, fn: () -> T): T {
        contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
        withSavedNamespace {
            currentNamespace = namespace
            return fn()
        }
    }

    @Suppress("LEAKED_IN_PLACE_LAMBDA", "WRONG_INVOCATION_KIND")
    @OptIn(ExperimentalContracts::class)
    inline fun <T> withThreadLocalAssigned(crossinline fn: () -> T): T {
        contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
        require(currentStorageStackOrNull() == null)
        val newStack = StorageStack(rootStackFrame)
        return withThreadLocalStorageStackAssigned(newStack) {
            fn()
        }
    }

    /**
     * Resizes the root frame to accommodate new variable assignments
     */
    fun recomputeRootFrame() {
        if (currentStorageStack().stack.size != 1) {
            throw IllegalStateException("Attempt to recompute the root frame without an empty stack")
        }
        val frame = rootStackFrame
        if (rootEnvironment.externalStorageList.isNotEmpty()) {
            throw IllegalStateException("External storage list for the root environment is not empty")
        }
        val oldStorageList = frame.storageList
        frame.storageList = Array(rootEnvironment.storageList.size) { i ->
            if (i < oldStorageList.size) {
                frame.storageList[i]
            } else {
                VariableHolder()
            }
        }
    }

    fun registerExportedSingleCharFunction(name: String) {
        exportedSingleCharFunctions.add(name)
    }

    inline fun <reified T : APLValue> registerClosableHandler(handler: ClosableHandler<T>) {
        if (closableHandlers.containsKey(T::class)) {
            throw IllegalStateException("Closable handler for class ${T::class.simpleName} already added")
        }
        closableHandlers[T::class] = handler
    }

    inline fun <reified T : APLValue> callClosableHandler(value: T, pos: Position) {
        val handler =
            closableHandlers[value::class] ?: throwAPLException(APLEvalException("Value cannot be closed: ${value.formatted(FormatStyle.PLAIN)}", pos))
        @Suppress("UNCHECKED_CAST")
        (handler as ClosableHandler<T>).close(value)
    }

    fun resolvePathName(file: String) = resolveDirectoryPath(file, workingDirectory)

    fun makeTimer(delays: IntArray, callbacks: List<LambdaValue>, pos: Position): APLValue {
        val handler = timerHandler ?: throwAPLException(APLEvalException("Backend does not support timers", pos))
        return handler.registerTimer(delays, callbacks)
    }

    fun findSymbolInImportsOrIntern(name: String): Symbol {
        return findSymbolInImports(name) ?: currentNamespace.internSymbol(name)
    }

    fun findSymbolInImports(name: String): Symbol? {
        return namespaces.findSymbolInImports(currentNamespace.name, name)
    }

    fun iterateSymbolsInImports(fn: (Symbol) -> Boolean) {
        return namespaces.iterateSymbolsInImports(currentNamespace.name, fn)
    }

    fun namespaceFromNamespaceName(namespaceName: NamespaceName): Namespace {
        return namespaces.findNamespaceFromName(namespaceName)
    }

    var workingDirectory: String? = currentDirectory()
        set(s) {
            val updatedDir = if (s != null && fileType(s) != FileNameType.DIRECTORY) {
                "/"
            } else {
                s
            }

            val oldDir = field
            field = updatedDir
            if (oldDir != updatedDir) {
                fireWorkingDirectoryUpdated(updatedDir)
            }
        }

    private val workingDirectoryListeners = ArrayList<(newName: String?) -> Unit>()

    /**
     * Add a listener that will be called when the working directory is changed.
     */
    fun addWorkingDirectoryListener(listener: (String?) -> Unit) {
        if (!workingDirectoryListeners.contains(listener)) {
            workingDirectoryListeners.add(listener)
        }
    }

    private fun fireWorkingDirectoryUpdated(updatedDir: String?) {
        workingDirectoryListeners.forEach { listener ->
            listener(updatedDir)
        }
    }

    /**
     * Create a copy of this engine that is valid for syntax checking. The resulting engine instance
     * will have the same functions registered, and the same variables bound in the root environment.
     * However, any code that results from running the [Engine.parse] on such an engine cannot be run.
     * The purpose of the engine is purely to parse code in order to get useful parse errors in a
     * source editor.
     *
     * The class [RuntimeContext] will in fact throw an exception if an attempt is
     * made to create an instance of it for a syntax checker engine.
     */
    fun copyToSyntaxChecker(): Engine {
        val newEngine = Engine(numComputeEngines = null, defaultOptimiser = ZeroOptimiser, forSyntaxCheck = true)
        newEngine.namespaces.copyFrom(namespaces)
        newEngine.functions.clear()
        newEngine.functions.putAll(functions.map { (k, v) -> k to v.copy() })
        newEngine.operators.clear()
        newEngine.operators.putAll(operators.map { (k, v) -> k to v.copy() })
        newEngine.exportedSingleCharFunctions.clear()
        newEngine.exportedSingleCharFunctions.addAll(exportedSingleCharFunctions)
        newEngine.customSyntaxEntries.putAll(customSyntaxEntries)
        newEngine.customSyntaxSubEntries.putAll(customSyntaxSubEntries)
        newEngine.rootEnvironment.copyFrom(rootEnvironment)
        return newEngine
    }
}

class CloseAPLFunction : APLFunctionDescriptor {
    class CloseAPLFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val value = a.collapseFirstLevel()
            context.engine.callClosableHandler(value, pos)
            return value
        }

        override val name1Arg: String get() = "close"
    }

    override fun make(instantiation: FunctionInstantiation) = CloseAPLFunctionImpl(instantiation)
}

class ChangeWorkingDirectoryCommandHandler : NumArgsCheckedCommandHandler(1, 1) {
    override fun handleNArgsCommand(context: CommandContext, cmd: String, args: List<String>) {
        val name = args[0]
        if (fileType(name) != FileNameType.DIRECTORY) {
            context.print("Not a directory: ${name}\n")
        } else {
            context.engine.workingDirectory = name
            context.print("Changed working directory to: ${name}\n")
        }
    }

    override fun description() = "Change the working directory"
}

fun throwAPLException(ex: APLEvalException): Nothing {
    val stack = currentStorageStackOrNull()
    if (stack != null) {
        ex.stack = CapturedStorageStack(stack)
    }
    throw ex
}

expect fun platformInit(engine: Engine)

class VariableHolder {
    @Volatile
    private var value: APLValue? = null

    fun value() = value

    fun updateValue(newValue: APLValue?) {
        val oldValue = value
        value = newValue
        if (newValue != null && oldValue !== newValue) {
            fireListeners(newValue, oldValue)
        }
    }

    fun updateValueNoPropagate(newValue: APLValue?) {
        value = newValue
    }

    // The listener registration is thread-safe, but the rest of variable management is not.
    // The lack of thread-safety for variables was intentional, as the responsibility for preventing issues
    // is on the programmer. Listener registrations are outside the direct influence of the programmer, which
    // requires it to be thread-safe.
    private var listeners: MTSafeArrayList<VariableUpdateListener>? = null

    private fun fireListeners(newValue: APLValue, oldValue: APLValue?) {
        listeners?.forEach { listener -> listener.updated(newValue, oldValue) }
    }

    fun registerListener(engine: Engine, listener: VariableUpdateListener) {
        engine.updateListenerLock.withLocked {
            val listenersCopy = listeners
            val list = if (listenersCopy == null) {
                val newListenerList = MTSafeArrayList<VariableUpdateListener>()
                listeners = newListenerList
                newListenerList
            } else {
                listenersCopy
            }
            list.add(listener)
        }
    }

    fun unregisterListener(listener: VariableUpdateListener): Boolean {
        return listeners?.remove(listener) == true
    }
}

fun interface VariableUpdateListener {
    fun updated(newValue: APLValue, oldValue: APLValue?)
}

@OptIn(ExperimentalContracts::class)
inline fun withLinkedContext(env: Environment, name: String, pos: Position, crossinline fn: () -> APLValue): APLValue {
    contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
    return currentStorageStack().withStackFrame(env, name, pos) {
        fn()
    }
}

class RuntimeContext(val engine: Engine) {
    init {
        if (engine.forSyntaxCheck) {
            throw IllegalStateException("Attempt to create a runtime context for a syntax checker engine")
        }
    }

    fun isLocallyBound(sym: Symbol): Boolean {
        val b = currentStorageStack().currentFrame().environment.findBinding(sym)
        return if (b == null) {
            false
        } else {
            val storage = currentStorageStack().findStorage(StackStorageRef(b))
            storage.value() != null
        }
    }

    fun setVar(storageRef: StackStorageRef, value: APLValue) {
        val stack = currentStorageStack()
        val holder = stack.findStorage(storageRef)
        holder.updateValue(value)
    }

    fun assignArgs(args: List<StackStorageRef>, a: APLValue, pos: Position? = null) {
        fun checkLength(expectedLength: Int, actualLength: Int) {
            if (expectedLength != actualLength) {
                throwAPLException(IllegalArgumentNumException(expectedLength, actualLength, pos))
            }
        }

        val v = a.unwrapDeferredValue()
        if (v is APLList && args.size != 1) {
            checkLength(args.size, v.listSize())
            for (i in args.indices) {
                setVar(args[i], v.listElement(i))
            }
        } else {
            checkLength(args.size, 1)
            setVar(args[0], v)
        }
    }
}

@Suppress("unused")
interface FunctionDefinitionListener {
    fun functionDefined(name: Symbol, fn: APLFunctionDescriptor) = Unit
    fun functionRemoved(name: Symbol) = Unit
    fun operatorDefined(name: Symbol, fn: APLOperator) = Unit
    fun operatorRemoved(name: Symbol) = Unit
}

class StandardSymbols(val engine: Engine) {
    val platform by lazy { engine.internSymbol("platform", engine.coreNamespace) }
    val renderer by lazy { engine.internSymbol("renderer", engine.coreNamespace) }
    val methods by lazy { engine.internSymbol("methods", engine.coreNamespace) }
}
