package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SpecialisedComparisonTest : APLTest() {
    private fun checkComparisonResultTypes(expected: Array<Long>, src: String) {
        parseAPLExpression(src).let { res ->
            assert1DArray(expected, res)
            assertTrue(res.isDepth0)
            assertEquals(ArrayMemberType.BOOLEAN, res.specialisedType)
        }
        parseAPLExpression(src, collapse = false).let { res ->
            assert1DArray(expected, res)
            assertTrue(res.isDepth0)
            assertEquals(ArrayMemberType.BOOLEAN, res.specialisedType)
        }
    }

    @Test
    fun specialisedCompareLongToMixed() {
        checkComparisonResultTypes(arrayOf(1, 0), "1 2 = 1 1.2")
        checkComparisonResultTypes(arrayOf(0, 1), "1 2 ≠ 1 1.2")
        checkComparisonResultTypes(arrayOf(0, 0), "1 2 < 1 1.2")
        checkComparisonResultTypes(arrayOf(1, 0), "1 2 ≤ 1 1.2")
        checkComparisonResultTypes(arrayOf(0, 1), "1 2 > 1 1.2")
        checkComparisonResultTypes(arrayOf(1, 1), "1 2 ≥ 1 1.2")
    }

    @Test
    fun specialisedCompareMixedToLong() {
        checkComparisonResultTypes(arrayOf(1, 0), "1 1.2 = 1 2")
        checkComparisonResultTypes(arrayOf(0, 1), "1 1.2 ≠ 1 2")
        checkComparisonResultTypes(arrayOf(0, 1), "1 1.2 < 1 2")
        checkComparisonResultTypes(arrayOf(1, 1), "1 1.2 ≤ 1 2")
        checkComparisonResultTypes(arrayOf(0, 0), "1 1.2 > 1 2")
        checkComparisonResultTypes(arrayOf(1, 0), "1 1.2 ≥ 1 2")
    }

    @Test
    fun specialisedCompareScalarLongToMixed() {
        checkComparisonResultTypes(arrayOf(0, 0, 1, 0, 0), "3 = 1 2 3 1.1 1.2")
        checkComparisonResultTypes(arrayOf(1, 1, 0, 1, 1), "3 ≠ 1 2 3 1.1 1.2")
        checkComparisonResultTypes(arrayOf(0, 0, 0, 0, 0, 1, 1), "3 < 1 2 3 1.1 1.2 4 4.1")
        checkComparisonResultTypes(arrayOf(0, 0, 1, 0, 0, 1, 1), "3 ≤ 1 2 3 1.1 1.2 4 4.1")
        checkComparisonResultTypes(arrayOf(1, 1, 0, 1, 1, 0, 0), "3 > 1 2 3 1.1 1.2 4 4.1")
        checkComparisonResultTypes(arrayOf(1, 1, 1, 1, 1, 0, 0), "3 ≥ 1 2 3 1.1 1.2 4 4.1")
    }

    @Test
    fun specialisedCompareMixedToScalarLong() {
        checkComparisonResultTypes(arrayOf(0, 0, 1, 0, 0), "1 2 3 1.1 1.2 = 3")
        checkComparisonResultTypes(arrayOf(1, 1, 0, 1, 1), "1 2 3 1.1 1.2 ≠ 3")
        checkComparisonResultTypes(arrayOf(1, 1, 0, 1, 1, 0, 0), "1 2 3 1.1 1.2 4 4.1 < 3")
        checkComparisonResultTypes(arrayOf(1, 1, 1, 1, 1, 0, 0), "1 2 3 1.1 1.2 4 4.1 ≤ 3")
        checkComparisonResultTypes(arrayOf(0, 0, 0, 0, 0, 1, 1), "1 2 3 1.1 1.2 4 4.1 > 3")
        checkComparisonResultTypes(arrayOf(0, 0, 1, 0, 0, 1, 1), "1 2 3 1.1 1.2 4 4.1 ≥ 3")
    }

    @Test
    fun specialisedCompareScalarDoubleToMixed() {
        checkComparisonResultTypes(arrayOf(0, 0, 0, 1, 0), "1.1 = 1 2 3 1.1 1.2")
        checkComparisonResultTypes(arrayOf(1, 1, 1, 0, 1), "1.1 ≠ 1 2 3 1.1 1.2")
        checkComparisonResultTypes(arrayOf(0, 1, 1, 0, 1), "1.1 < 1 2 3 1.1 1.2")
        checkComparisonResultTypes(arrayOf(0, 1, 1, 1, 1), "1.1 ≤ 1 2 3 1.1 1.2")
        checkComparisonResultTypes(arrayOf(1, 0, 0, 0, 0), "1.1 > 1 2 3 1.1 1.2")
        checkComparisonResultTypes(arrayOf(1, 0, 0, 1, 0), "1.1 ≥ 1 2 3 1.1 1.2")
    }

    @Test
    fun specialisedCompareMixedToScalarDouble() {
        checkComparisonResultTypes(arrayOf(0, 0, 0, 1, 0), "1 2 3 1.1 1.2 = 1.1")
        checkComparisonResultTypes(arrayOf(1, 1, 1, 0, 1), "1 2 3 1.1 1.2 ≠ 1.1")
        checkComparisonResultTypes(arrayOf(1, 0, 0, 0, 0), "1 2 3 1.1 1.2 < 1.1")
        checkComparisonResultTypes(arrayOf(1, 0, 0, 1, 0), "1 2 3 1.1 1.2 ≤ 1.1")
        checkComparisonResultTypes(arrayOf(0, 1, 1, 0, 1), "1 2 3 1.1 1.2 > 1.1")
        checkComparisonResultTypes(arrayOf(0, 1, 1, 1, 1), "1 2 3 1.1 1.2 ≥ 1.1")
    }

    @Test
    fun specialisedCompareMixedToMixed() {
        checkComparisonResultTypes(arrayOf(1, 0, 1), "1 2 1.1 = 1 1.2 1.1")
        checkComparisonResultTypes(arrayOf(0, 1, 0), "1 2 1.1 ≠ 1 1.2 1.1")
        checkComparisonResultTypes(arrayOf(0, 0, 0), "1 2 1.1 < 1 1.2 1.1")
        checkComparisonResultTypes(arrayOf(1, 0, 1), "1 2 1.1 ≤ 1 1.2 1.1")
        checkComparisonResultTypes(arrayOf(0, 1, 0), "1 2 1.1 > 1 1.2 1.1")
        checkComparisonResultTypes(arrayOf(1, 1, 1), "1 2 1.1 ≥ 1 1.2 1.1")
    }

    @Test
    fun specialisedCompareBooleanToBoolean() {
        checkComparisonResultTypes(arrayOf(1, 0, 1, 0), "1 1 0 0 = 1 0 0 1")
        checkComparisonResultTypes(arrayOf(0, 1, 0, 1), "1 1 0 0 ≠ 1 0 0 1")
        checkComparisonResultTypes(arrayOf(0, 0, 0, 1), "1 1 0 0 < 1 0 0 1")
        checkComparisonResultTypes(arrayOf(1, 0, 1, 1), "1 1 0 0 ≤ 1 0 0 1")
        checkComparisonResultTypes(arrayOf(0, 1, 0, 0), "1 1 0 0 > 1 0 0 1")
        checkComparisonResultTypes(arrayOf(1, 1, 1, 0), "1 1 0 0 ≥ 1 0 0 1")
    }

    @Test
    fun specialisedCompareLongToLong() {
        checkComparisonResultTypes(arrayOf(0, 1, 0, 1, 0), "1 2 3 0 0 = 2 2 1 0 5")
        checkComparisonResultTypes(arrayOf(1, 0, 1, 0, 1), "1 2 3 0 0 ≠ 2 2 1 0 5")
        checkComparisonResultTypes(arrayOf(1, 0, 0, 0, 1), "1 2 3 0 0 < 2 2 1 0 5")
        checkComparisonResultTypes(arrayOf(1, 1, 0, 1, 1), "1 2 3 0 0 ≤ 2 2 1 0 5")
        checkComparisonResultTypes(arrayOf(0, 0, 1, 0, 0), "1 2 3 0 0 > 2 2 1 0 5")
        checkComparisonResultTypes(arrayOf(0, 1, 1, 1, 0), "1 2 3 0 0 ≥ 2 2 1 0 5")
    }

    @Test
    fun specialisedCompareLongScalarToLong() {
        checkComparisonResultTypes(arrayOf(0, 1), "3 = 1 3")
        checkComparisonResultTypes(arrayOf(1, 0), "3 ≠ 1 3")
        checkComparisonResultTypes(arrayOf(0, 0), "3 < 1 3")
        checkComparisonResultTypes(arrayOf(0, 1), "3 ≤ 1 3")
        checkComparisonResultTypes(arrayOf(1, 0), "3 > 1 3")
        checkComparisonResultTypes(arrayOf(1, 1), "3 ≥ 1 3")
    }

    @Test
    fun specialisedCompareLongToLongScalar() {
        checkComparisonResultTypes(arrayOf(0, 1), "1 3 = 3")
        checkComparisonResultTypes(arrayOf(1, 0), "1 3 ≠ 3")
        checkComparisonResultTypes(arrayOf(1, 0), "1 3 < 3")
        checkComparisonResultTypes(arrayOf(1, 1), "1 3 ≤ 3")
        checkComparisonResultTypes(arrayOf(0, 0), "1 3 > 3")
        checkComparisonResultTypes(arrayOf(0, 1), "1 3 ≥ 3")
    }

    @Test
    fun specialisedCompareDoubleToDouble() {
        checkComparisonResultTypes(arrayOf(0, 1), "1.2 1.1 = 1.1 1.1")
        checkComparisonResultTypes(arrayOf(1, 0), "1.2 1.1 ≠ 1.1 1.1")
        checkComparisonResultTypes(arrayOf(0, 0), "1.2 1.1 < 1.1 1.1")
        checkComparisonResultTypes(arrayOf(0, 1), "1.2 1.1 ≤ 1.1 1.1")
        checkComparisonResultTypes(arrayOf(1, 0), "1.2 1.1 > 1.1 1.1")
        checkComparisonResultTypes(arrayOf(1, 1), "1.2 1.1 ≥ 1.1 1.1")
    }

    @Test
    fun specialisedCompareDoubleScalarToDouble() {
        checkComparisonResultTypes(arrayOf(0, 1), "1.2 = 1.1 1.2")
        checkComparisonResultTypes(arrayOf(1, 0), "1.2 ≠ 1.1 1.2")
        checkComparisonResultTypes(arrayOf(0, 0), "1.2 < 1.1 1.2")
        checkComparisonResultTypes(arrayOf(0, 1), "1.2 ≤ 1.1 1.2")
        checkComparisonResultTypes(arrayOf(1, 0), "1.2 > 1.1 1.2")
        checkComparisonResultTypes(arrayOf(1, 1), "1.2 ≥ 1.1 1.2")
    }

    @Test
    fun specialisedCompareDoubleToScalarDouble() {
        checkComparisonResultTypes(arrayOf(0, 1), "1.1 1.2 = 1.2")
        checkComparisonResultTypes(arrayOf(1, 0), "1.1 1.2 ≠ 1.2")
        checkComparisonResultTypes(arrayOf(1, 0), "1.1 1.2 < 1.2")
        checkComparisonResultTypes(arrayOf(1, 1), "1.1 1.2 ≤ 1.2")
        checkComparisonResultTypes(arrayOf(0, 0), "1.1 1.2 > 1.2")
        checkComparisonResultTypes(arrayOf(0, 1), "1.1 1.2 ≥ 1.2")
    }

    @Test
    fun specialisedCompareLongToDouble() {
        checkComparisonResultTypes(arrayOf(0, 0), "1 2 = 1.1 1.2")
        checkComparisonResultTypes(arrayOf(1, 1), "1 2 ≠ 1.1 1.2")
        checkComparisonResultTypes(arrayOf(1, 0), "1 2 < 1.1 1.2")
        checkComparisonResultTypes(arrayOf(1, 0), "1 2 ≤ 1.1 1.2")
        checkComparisonResultTypes(arrayOf(0, 1), "1 2 > 1.1 1.2")
        checkComparisonResultTypes(arrayOf(0, 1), "1 2 ≥ 1.1 1.2")
    }

    @Test
    fun specialisedCompareDoubleToLong() {
        checkComparisonResultTypes(arrayOf(0, 0), "1.1 1.2 = 1 2")
        checkComparisonResultTypes(arrayOf(1, 1), "1.1 1.2 ≠ 1 2")
        checkComparisonResultTypes(arrayOf(0, 1), "1.1 1.2 < 1 2")
        checkComparisonResultTypes(arrayOf(0, 1), "1.1 1.2 ≤ 1 2")
        checkComparisonResultTypes(arrayOf(1, 0), "1.1 1.2 > 1 2")
        checkComparisonResultTypes(arrayOf(1, 0), "1.1 1.2 ≥ 1 2")
    }
}