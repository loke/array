package com.dhsdevelopments.kap.benchmark.jvm

import com.dhsdevelopments.kap.benchmark.runAllTests

/*
New cpu:
primes: avg=26.025, median=26.0, min=26, max=27, stddev=1.0000179940633445
var lookup scope: avg=50.425, median=51.0, min=47, max=53, stddev=1.0007458586140505
contrib bench: avg=67.325, median=67.5, min=67, max=68, stddev=1.0000241990969359
simple sum: avg=166.225, median=166.5, min=166, max=168, stddev=1.0000040602325206
multiple call: avg=59.5, median=58.5, min=58, max=63, stddev=1.0002541870292694
formatter: avg=18.075, median=18.0, min=17, max=20, stddev=1.0003356816799442
decode: avg=34.525, median=35.0, min=34, max=36, stddev=1.000167512465977

primes: avg=24.925, median=25.0, min=24, max=26, stddev=1.000096070974037
var lookup scope: avg=44.4, median=44.5, min=44, max=45, stddev=1.0000608698299254
contrib bench: avg=68.675, median=68.0, min=68, max=76, stddev=1.0002193634332839
simple sum: avg=169.225, median=169.0, min=169, max=170, stddev=1.000003044559867
multiple call: avg=57.425, median=57.0, min=57, max=58, stddev=1.000037052432569
formatter: avg=18.275, median=18.5, min=17, max=20, stddev=1.0005229179175181
decode: avg=34.675, median=34.5, min=34, max=36, stddev=1.00015359292534

Removed threadlocals:
primes: avg=24.725, median=25.0, min=24, max=26, stddev=1.0003674739206059
var lookup scope: avg=39.075, median=39.5, min=38, max=42, stddev=1.0003010227782392
contrib bench: avg=62.175, median=62.0, min=60, max=64, stddev=1.0000768744918354
simple sum: avg=167.325, median=167.0, min=167, max=169, stddev=1.0000048106590724
multiple call: avg=48.15, median=49.0, min=47, max=50, stddev=1.0001461016082476
formatter: avg=18.2, median=18.0, min=18, max=20, stddev=1.0003169404759733
decode: avg=35.125, median=35.0, min=35, max=36, stddev=1.0000443246974267
*/

class Main {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runAllTests("jvm", "../array/standard-lib", "../benchmark-reports", "unnamed")
        }
    }
}
