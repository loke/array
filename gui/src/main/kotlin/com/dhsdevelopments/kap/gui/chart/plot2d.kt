package com.dhsdevelopments.kap.gui.chart

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.chart.Plot2DParams
import com.dhsdevelopments.kap.chart.computeFunctionGraphDataset
import com.dhsdevelopments.kap.chart.makeFunctionList
import com.dhsdevelopments.kap.chart.plot2DParamsFromAPLValue
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.scene.chart.XYChart

class PlotFunction : APLFunctionDescriptor {
    class PlotFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val params = plot2DParamsFromAPLValue(a, pos)
            val (functionList, labelsList) = makeFunctionList(b, pos)
            return openGraphWithParams(params, functionList, labelsList, context)
        }

        private fun openGraphWithParams(params: Plot2DParams, functions: Array<APLFunction>, labels: Array<String?>?, context: RuntimeContext): APLValue {
            val datasets = computeFunctionGraphDataset(context, params, functions, labels, pos)
            val posList = datasets.xPositions
            val list = FXCollections.observableArrayList<XYChart.Series<Number, Number>>()
            datasets.datasets.forEach { ds ->
                val series = FXCollections.observableArrayList<XYChart.Data<Number, Number>>()
                ds.data.forEachIndexed { i, point ->
                    if (point.isFinite()) {
                        series.add(XYChart.Data(posList[i], point))
                    }
                }
                list.add(XYChart.Series(ds.label, series))
            }
            Platform.runLater {
                PlotPanel.open(params, list)
            }
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = PlotFunctionImpl(instantiation)
}
