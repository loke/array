package com.dhsdevelopments.kap.gui.styledarea

import com.dhsdevelopments.kap.gui.settings.AbstractConfigProvider
import com.dhsdevelopments.kap.gui.settings.ConfigProvider
import com.dhsdevelopments.kap.gui.settings.Settings
import javafx.scene.Node
import javafx.scene.text.TextFlow
import org.fxmisc.richtext.StyledTextArea
import org.fxmisc.richtext.TextExt
import org.fxmisc.richtext.model.SegmentOps
import org.fxmisc.richtext.model.StyledSegment
import org.fxmisc.richtext.model.TextOps
import java.util.function.BiConsumer
import java.util.function.Function

class InputFieldStyledArea(val configProvider: MutableConfigProvider = MutableConfigProvider()) : KapEditorStyledArea<ParStyle, String, TextStyle>(
    ParStyle(),
    applyParagraphStyle,
    TextStyle(configProvider),
    segOps,
    nodeFactory,
    configProvider
) {
    companion object {
        val applyParagraphStyle = BiConsumer<TextFlow, ParStyle> { flow, parStyle -> }
        val segOps: TextOps<String, TextStyle> = SegmentOps.styledTextOps()
        val nodeFactory = Function<StyledSegment<String, TextStyle>, Node> { seg ->
            val applyStyle = { a: TextExt, b: TextStyle ->
                b.styleContent(a)
            }
            StyledTextArea.createStyledTextNode(seg.segment, seg.style, applyStyle)
        }
    }

    class MutableConfigProvider : AbstractConfigProvider() {
        private val defaultSettings = Settings()
        private var backendProvider: ConfigProvider? = null
        private val pendingListeners = ArrayList<(Settings, Settings) -> Unit>()

        override fun settings() = backendProvider?.settings() ?: defaultSettings

        override fun addConfigUpdateListener(fn: (Settings, Settings) -> Unit) {
            val p = backendProvider
            if (p == null) {
                pendingListeners.add(fn)
            } else {
                p.addConfigUpdateListener(fn)
            }
        }

        fun updateBackendProvider(provider: ConfigProvider) {
            if (backendProvider == null) {
                val newSettings = provider.settings()
                pendingListeners.forEach { l ->
                    l(defaultSettings, newSettings)
                }
                pendingListeners.forEach { l ->
                    provider.addConfigUpdateListener(l)
                }
                pendingListeners.clear()
                backendProvider = provider
            }
        }
    }
}
