package com.dhsdevelopments.kap.options

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class ArgParserTest {
    @Test
    fun simpleTest() {
        val parser = ArgParser(Option("foo", requireArg = "a"), Option("bar"))
        val result = parser.parse(arrayOf("--foo=b", "--bar"))
        assertEquals(2, result.size)
        assertEquals(listOf("b"), result["foo"])
        assertTrue(result.containsKey("bar"))
        assertEquals(listOf(null), result["bar"])
    }

    @Test
    fun noArgOptionWithArgShouldFail() {
        assertFailsWith<InvalidOption> {
            ArgParser(Option("foo")).parse(arrayOf("--foo=b"))
        }
    }

    @Test
    fun argOptionWithoutArgShouldFail() {
        assertFailsWith<InvalidOption> {
            ArgParser(Option("foo", requireArg = "a")).parse(arrayOf("--foo"))
        }
    }

    @Test
    fun noArguments() {
        val result = ArgParser(Option("foo", requireArg = "a"), Option("bar", requireArg = "a")).parse(emptyArray())
        assertEquals(0, result.size)
    }

    @Test
    fun invalidOptionFormat0() {
        assertFailsWith<InvalidOption> {
            ArgParser(Option("foo", requireArg = "a"), Option("bar", requireArg = "a")).parse(arrayOf("-foo=b"))
        }
    }

    @Test
    fun invalidOptionFormat1() {
        assertFailsWith<InvalidOption> {
            ArgParser(Option("foo", requireArg = "x"), Option("bar", requireArg = "a")).parse(arrayOf("-bar"))
        }
    }

    @Test
    fun invalidOptionFormat2() {
        assertFailsWith<InvalidOption> {
            ArgParser(Option("foo", requireArg = "x"), Option("bar", requireArg = "a")).parse(arrayOf(" --foo=a"))
        }
    }

    @Test
    fun shortOptionOneArg() {
        val result = ArgParser(Option("foo", shortOption = "f")).parse(arrayOf("-f"))
        assertEquals(1, result.size)
        assertTrue(result.containsKey("foo"))
        assertEquals(listOf(null), result["foo"])
    }

    @Test
    fun shortOptionTwoArg() {
        val result = ArgParser(Option("foo", shortOption = "f"), Option("xyz", shortOption = "x")).parse(arrayOf("-fx"))
        assertEquals(2, result.size)
        assertTrue(result.containsKey("foo"))
        assertEquals(listOf(null), result["foo"])
        assertTrue(result.containsKey("xyz"))
        assertEquals(listOf(null), result["xyz"])
    }

    @Test
    fun shortOptionTwoArgMultiEntry() {
        val result = ArgParser(Option("foo", shortOption = "f"), Option("xyz", shortOption = "x")).parse(arrayOf("-f", "-x"))
        assertEquals(2, result.size)
        assertTrue(result.containsKey("foo"))
        assertEquals(listOf(null), result["foo"])
        assertTrue(result.containsKey("xyz"))
        assertEquals(listOf(null), result["xyz"])
    }

    @Test
    fun shortOptionTwoArgMixLong() {
        val result = ArgParser(Option("foo", shortOption = "f"), Option("xyz", shortOption = "x")).parse(arrayOf("-f", "--xyz"))
        assertEquals(2, result.size)
        assertTrue(result.containsKey("foo"))
        assertEquals(listOf(null), result["foo"])
        assertTrue(result.containsKey("xyz"))
        assertEquals(listOf(null), result["xyz"])
    }

    @Test
    fun shortOptionTwoArgMixLongWithArg() {
        val result = ArgParser(
            Option("foo", shortOption = "f", multiple = true),
            Option("xyz", requireArg = "a", shortOption = "x", multiple = true))
            .parse(arrayOf("-f", "--xyz=b"))
        assertEquals(2, result.size)
        assertTrue(result.containsKey("foo"))
        assertEquals(listOf(null), result["foo"])
        assertTrue(result.containsKey("xyz"))
        assertEquals(listOf("b"), result["xyz"])
    }

    @Test
    fun shortOptionArgWithParam() {
        val result = ArgParser(
            Option("foo", shortOption = "f"),
            Option("xyz", requireArg = "a", shortOption = "x"))
            .parse(arrayOf("-x", "b"))
        assertEquals(1, result.size)
        assertTrue(result.containsKey("xyz"))
        assertEquals(listOf("b"), result["xyz"])
    }

    @Test
    fun invalidShortArg() {
        assertFailsWith<InvalidOption> {
            ArgParser(Option("foo", shortOption = "f")).parse(arrayOf("-x"))
        }
    }

    @Test
    fun duplicateArgNames0() {
        assertFailsWith<InvalidOptionDefinition> {
            ArgParser(Option("foo", shortOption = "z"), Option("bar", shortOption = "z"))
        }
    }

    @Test
    fun duplicateArgNames1() {
        assertFailsWith<InvalidOptionDefinition> {
            ArgParser(Option("foo", shortOption = "a"), Option("foo", shortOption = "b"))
        }
    }

    @Test
    fun multipleCopiesOfSameArg0() {
        val result = ArgParser(Option("foo", shortOption = "f", multiple = true), Option("xyz", shortOption = "x")).parse(arrayOf("-f", "-f"))
        assertEquals(1, result.size)
        assertTrue(result.containsKey("foo"))
        assertEquals(listOf(null, null), result["foo"])
    }

    @Test
    fun multipleCopiesOfSameArg1() {
        val result = ArgParser(
            Option("foo", shortOption = "f", multiple = true),
            Option("xyz", shortOption = "x", multiple = true)
        ).parse(arrayOf("--foo", "--foo"))
        assertEquals(1, result.size)
        assertTrue(result.containsKey("foo"))
        assertEquals(listOf(null, null), result["foo"])
    }

    @Test
    fun multipleCopiesOfSameArg2() {
        val result = ArgParser(
            Option("foo", requireArg = "a", shortOption = "f", multiple = true),
            Option("xyz", shortOption = "x", multiple = true)
        ).parse(arrayOf("--foo=b", "--foo=c"))
        assertEquals(1, result.size)
        assertEquals(listOf("b", "c"), result["foo"])
    }

    @Test
    fun multipleCopiesOfSameArg3() {
        val result =
            ArgParser(
                Option("foo", requireArg = "a", shortOption = "f", multiple = true),
                Option("xyz", shortOption = "x", multiple = true)
            ).parse(arrayOf("-f", "b", "--foo=c"))
        assertEquals(1, result.size)
        assertEquals(listOf("b", "c"), result["foo"])
    }

    @Test
    fun missingShortOptionArgument0() {
        assertFailsWith<OptionSyntaxException> {
            ArgParser(Option("foo", requireArg = "a", shortOption = "f"))
                .parse(arrayOf("-f"))
        }
    }

    @Test
    fun missingShortOptionArgument1() {
        assertFailsWith<OptionSyntaxException> {
            ArgParser(
                Option("foo", requireArg = "a", shortOption = "f"),
                Option("bar", requireArg = "a", shortOption = "b")
            ).parse(arrayOf("-f", "b", "-b"))
        }
    }
}
