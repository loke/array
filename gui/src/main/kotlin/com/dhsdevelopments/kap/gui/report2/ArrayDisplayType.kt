package com.dhsdevelopments.kap.gui.report2

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.gui.arrayedit.ArrayEditor
import javafx.application.Platform
import javafx.scene.control.CheckBox
import javafx.scene.layout.HBox

object ArrayDisplayType : DisplayType {
    override val description get() = "Array"
    override fun makeConfigurator(client: ReportClient, selection: ArrayEditor.SelectedArea) = ArrayDisplayConfigurator(client, selection)
}

class ArrayDisplayConfigurator(val client: ReportClient, val selection: ArrayEditor.SelectedArea) : DisplayConfigurator {
    private val colLabelsCheckbox: CheckBox
    private val rowLabelsCheckbox: CheckBox
    private val panel: HBox

    init {
        colLabelsCheckbox = CheckBox("Column headers")
        rowLabelsCheckbox = CheckBox("Row headers")
        val hbox = HBox()
        hbox.spacing = 40.0
        hbox.children.addAll(colLabelsCheckbox, rowLabelsCheckbox)
        panel = hbox
    }

    override fun settingsPanel() = panel

    override fun saveToResult(engine: Engine, storage: VariableHolder, value: APLValue, sym: Symbol) {
        val displayedValue = ArrayDisplayedValue(
            client,
            engine,
            storage,
            sym,
            selection.row,
            selection.column,
            value,
            colLabelsCheckbox.isSelected,
            rowLabelsCheckbox.isSelected)
        Platform.runLater {
            client.variableList.items.add(displayedValue)
            displayedValue.updateContentForValue(value)
        }
    }
}

class ArrayDisplayedValue(
    reportClient: ReportClient,
    engine: Engine,
    storage: VariableHolder,
    name: Symbol,
    row: Int,
    col: Int,
    value: APLValue? = null,
    val colLabels: Boolean = false,
    val rowLabels: Boolean = false,
) : DisplayedValue(reportClient, engine, storage, name, row, col, value) {
    override fun processUpdate(v0: APLValue) {
        Platform.runLater {
            value = v0
            updateContentForValue(v0)
        }
    }

    override val displayedRowCount: Int = if (colLabels) nRows + 1 else nRows
    override val displayedColCount: Int = if (colLabels) nCols + 1 else nCols

    fun updateContentForValue(newContent: APLValue) {
        val d = DimensionsAndLabels(newContent, rowLabels, colLabels)

        if (!initialised) {
            nRows = d.nRowsUpdated
            nCols = d.nColsUpdated
            initialised = true
        } else {
            // TODO: Possibly update the grid size to accommodate a change in content size
        }

        val grid = reportClient.table.grid
        if (d.dimensions.size == 0) {
            val list = grid.rows[row]
            list[col] = HeaderCellType.createCell(HeaderCellContent(this, newContent.disclose().formatted(FormatStyle.PLAIN)), row, col)
            grid.rows[row] = list
        } else if (d.dimensions.size <= 2) {
            if (d.colLabelsList != null) {
                val s = if (d.rowLabelsList == null) col else col + 1
                val currRow = grid.rows[row]
                repeat(nCols) { colIndex ->
                    currRow[colIndex + s] = HeaderCellType.createCell(HeaderCellContent(this, d.colLabelsList[colIndex]), row, colIndex + s)
                }
            }

            val firstDataRow = if (d.colLabelsList == null) row else row + 1

            repeat(nRows) { rowIndex ->
                val currRow = grid.rows[firstDataRow + rowIndex]
                if (d.rowLabelsList != null) {
                    currRow[col] = HeaderCellType.createCell(HeaderCellContent(this, d.rowLabelsList[rowIndex]), firstDataRow + rowIndex, col)
                }
                val firstDataCol = if (d.rowLabelsList == null) col else col + 1
                repeat(nCols) { colIndex ->
                    val v0 = newContent.valueAt((rowIndex * nCols) + colIndex)
                    val cell = FieldCellType.createCell(
                        FieldCellContent(this, v0.formatted(FormatStyle.PLAIN)),
                        firstDataRow + rowIndex,
                        firstDataCol + colIndex)
                    currRow[firstDataCol + colIndex] = cell
                }
                grid.rows[firstDataRow + rowIndex] = currRow
            }
        } else {
            error("Rank should have been checked earlier")
        }
    }
}
