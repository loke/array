package com.dhsdevelopments.kap.builtins.math

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.MAX_INT_DOUBLE
import com.dhsdevelopments.kap.builtins.MathCombineAPLFunction
import com.dhsdevelopments.kap.builtins.singleArgNumericRelationOperation
import com.dhsdevelopments.mpbignum.*
import kotlin.math.sqrt

fun factoriseLong(n: Long): List<Long> {
    if (n < 0) {
        throw ArithmeticException("Argument is negative")
    }

    if (n == 0L) {
        return emptyList()
    }

    if (n <= 3) {
        return listOf(n)
    }

    var curr = n
    val factors = ArrayList<Long>()
    var i = 2L
    while (i <= curr / i) {
        while (curr % i == 0L) {
            factors.add(i)
            curr /= i
        }
        i++
    }
    if (curr > 1) {
        factors.add(curr)
    }
    return factors
}

fun factoriseBigint(n: BigInt): List<BigInt> {
    if (n < 0) {
        throw ArithmeticException("Argument is negative")
    }

    if (n.rangeInLong()) {
        return factoriseLong(n.toLong()).map(BigInt::of)
    }

    var curr = n
    val factors = ArrayList<BigInt>()
    var i = BigIntConstants.TWO
    while (i <= curr / i) {
        while ((curr % i).signum() == 0) {
            factors.add(i)
            curr /= i
        }
        i = i + 1
    }
    if (curr > 1) {
        factors.add(curr)
    }
    return factors
}

fun divisorsLong(n: Long, pos: Position): List<Long> {
    if (n < 0) {
        throwAPLException(APLArithmeticException("Argument is negative", pos))
    }

    if (n > MAX_INT_DOUBLE) {
        return divisorsBigint(n.toBigInt())
    }

    val res0 = ArrayList<Long>()
    val res1 = ArrayList<Long>()

    val nEven = n % 2 == 0L
    val stepLength = if (nEven) 1L else 2L
    val start = if (nEven) 2L else 3L
    for (i in start..(sqrt(n.toDouble()).toLong()) step stepLength) {
        if (n % i == 0L) {
            res0.add(i)
            res1.add(n / i)
        }
    }
    return when {
        res0.isEmpty() -> res0
        res0.last() == res1.last() -> res0 + res1.dropLast(1).reversed()
        else -> res0 + res1.reversed()
    }
}

fun divisorsBigint(n: BigInt): List<Long> {
    if (n.signum() < 0) {
        throw ArithmeticException("Argument is negative")
    }

    val s = n.isqrt()
    if (n >= Long.MAX_VALUE) {
        throw ArithmeticException("Argument too large")
    }

    val res0 = ArrayList<Long>()
    val res1 = ArrayList<Long>()

    val nEven = (n % 2).signum() == 0
    val stepLength = if (nEven) 1L else 2L
    val start = if (nEven) 2L else 3L
    for (i in start..s.toLong() step stepLength) {
        if ((n % i).signum() == 0) {
            res0.add(i)
            res1.add((n / i).toLong())
        }
    }
    return when {
        res0.isEmpty() -> res0
        res0.last() == res1.last() -> res0 + res1.dropLast(1).reversed()
        else -> res0 + res1.reversed()
    }
}

class FactorAPLFunction : APLFunctionDescriptor {
    class FactorAPLFunctionImpl(pos: FunctionInstantiation) : MathCombineAPLFunction(pos) {
        override fun combine1Arg(a: APLSingleValue): APLValue {
            return singleArgNumericRelationOperation(
                pos,
                a,
                fnLong = { x ->
                    if (x < 0) {
                        raiseArgumentMustBePositive()
                    } else {
                        val res = factoriseLong(x)
                        APLArrayList(dimensionsOfSize(res.size), res.map { v -> v.makeAPLNumber() }, ArrayMemberType.LONG)
                    }
                },
                fnDouble = { x -> raiseTypeException() },
                fnComplex = { x -> raiseTypeException() },
                fnBigInt = { x -> opBigInt(x) },
                fnRational = { x -> if (x.denominator == BigIntConstants.ONE) opBigInt(x.numerator) else raiseTypeException() })
        }

        private fun opBigInt(x: BigInt): APLValue {
            return if (x < 0) {
                raiseArgumentMustBePositive()
            } else {
                val res = factoriseBigint(x)
                APLArrayList(dimensionsOfSize(res.size), res.map { v -> v.makeAPLNumberWithReduction() }, ArrayMemberType.LONG)
            }
        }

        private fun raiseTypeException(): Nothing = throwAPLException(APLIllegalArgumentException("Only integers can be factorised", pos))
        private fun raiseArgumentMustBePositive(): Nothing = throwAPLException(APLArithmeticException("Argument must be positive", pos))
    }

    override fun make(instantiation: FunctionInstantiation) = FactorAPLFunctionImpl(instantiation)
}

class DivisorsAPLFunction : APLFunctionDescriptor {
    class DivisorsAPLFunctionImpl(pos: FunctionInstantiation) : MathCombineAPLFunction(pos) {
        override fun combine1Arg(a: APLSingleValue): APLValue {
            return singleArgNumericRelationOperation(
                pos,
                a,
                fnLong = { x ->
                    if (x < 0) {
                        raiseArgumentMustBePositive()
                    } else {
                        val res = divisorsLong(x, pos)
                        APLArrayList(dimensionsOfSize(res.size), res.map { v -> v.makeAPLNumber() }, ArrayMemberType.LONG)
                    }
                },
                fnDouble = { x -> raiseTypeException() },
                fnComplex = { x -> raiseTypeException() },
                fnBigInt = { x -> opBigInt(x) },
                fnRational = { x -> if (x.denominator == BigIntConstants.ONE) opBigInt(x.numerator) else raiseTypeException() })
        }

        private fun opBigInt(x: BigInt): APLValue {
            return if (x < 0) {
                raiseArgumentMustBePositive()
            } else {
                val res = divisorsBigint(x)
                APLArrayList(dimensionsOfSize(res.size), res.map { v -> v.makeAPLNumber() }, ArrayMemberType.LONG)
            }
        }

        private fun raiseTypeException(): Nothing = throwAPLException(APLIllegalArgumentException("Argument is not an integer", pos))
        private fun raiseArgumentMustBePositive(): Nothing = throwAPLException(APLArithmeticException("Argument must be positive", pos))
    }

    override fun make(instantiation: FunctionInstantiation) = DivisorsAPLFunctionImpl(instantiation)
}
