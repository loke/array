package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals

class SpecialisedArrayTest : APLTest() {
    @Test
    fun readSpecialisedArrayLong0() {
        parseAPLExpression("1 2 3 4", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(1, 2, 3, 4), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun readSpecialisedArrayLong1() {
        parseAPLExpression("1 2 3 4", collapse = true).let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(1, 2, 3, 4), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun readSpecialisedArrayDouble0() {
        parseAPLExpression("1.1 2.1 3.1 4.1", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContentDouble(doubleArrayOf(1.1, 2.1, 3.1, 4.1), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun readSpecialisedArrayDouble1() {
        parseAPLExpression("1.1 2.1 3.1 4.1", collapse = true).let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContentDouble(doubleArrayOf(1.1, 2.1, 3.1, 4.1), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun readSpecialisedArrayDoubleWithZeroDecimals0() {
        parseAPLExpression("1.0 2.0 3.0 4.0", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContentDouble(doubleArrayOf(1.0, 2.0, 3.0, 4.0), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun readSpecialisedArrayDoubleWithZeroDecimals1() {
        parseAPLExpression("1.0 2.0 3.0 4.0", collapse = true).let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContentDouble(doubleArrayOf(1.0, 2.0, 3.0, 4.0), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun readSpecialisedArrayMixedLongAndDouble0() {
        parseAPLExpression("1 2 3 4.5", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(1, 2, 3, InnerDouble(4.5)), result)
            assertEquals(ArrayMemberType.ATOM, result.specialisedType)
        }
    }

    @Test
    fun readSpecialisedArrayMixedLongAndDouble1() {
        parseAPLExpression("1 2 3 4.5", collapse = true).let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(1, 2, 3, InnerDouble(4.5)), result)
            assertEquals(ArrayMemberType.ATOM, result.specialisedType)
        }
    }

    @Test
    fun iotaIsSpecialised0() {
        parseAPLExpression("⍳3", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(3), result)
            assertArrayContent(arrayOf(0, 1, 2), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun iotaIsSpecialised1() {
        parseAPLExpression("⍳3", collapse = true).let { result ->
            assertDimension(dimensionsOfSize(3), result)
            assertArrayContent(arrayOf(0, 1, 2), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun iotaWithAdditionScalarLeft0() {
        parseAPLExpression("1000+⍳3", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(3), result)
            assertArrayContent(arrayOf(1000, 1001, 1002), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun iotaWithAdditionScalarLeft1() {
        parseAPLExpression("1000+⍳3", collapse = true).let { result ->
            assertDimension(dimensionsOfSize(3), result)
            assertArrayContent(arrayOf(1000, 1001, 1002), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun iotaWithAdditionScalarRight0() {
        parseAPLExpression("(⍳3)+1000", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(3), result)
            assertArrayContent(arrayOf(1000, 1001, 1002), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun iotaWithAdditionScalarRight1() {
        parseAPLExpression("(⍳3)+1000",collapse=false).let { result ->
            assertDimension(dimensionsOfSize(3), result)
            assertArrayContent(arrayOf(1000, 1001, 1002), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun iotaWithAdditionPlusArray0() {
        parseAPLExpression("(⍳3) + 10 11 12", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(3), result)
            assertArrayContent(arrayOf(10, 12, 14), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun iotaWithAdditionPlusArray1() {
        parseAPLExpression("(⍳3) + 10 11 12").let { result ->
            assertDimension(dimensionsOfSize(3), result)
            assertArrayContent(arrayOf(10, 12, 14), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun transposeHorizontalLong() {
        parseAPLExpression("⌽ 1 2 3 4 5 6", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(6), result)
            assertArrayContent(arrayOf(6, 5, 4, 3, 2, 1), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun transposeHorizontalDouble() {
        parseAPLExpression("⌽ 1.1 2.1 3.1 4.1 5.1 6.1", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(6), result)
            assertArrayContentDouble(doubleArrayOf(6.1, 5.1, 4.1, 3.1, 2.1, 1.1), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun transposeVerticalLong() {
        parseAPLExpression("⊖ 3 4 ⍴ ⍳12", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            assertArrayContent(arrayOf(8, 9, 10, 11, 4, 5, 6, 7, 0, 1, 2, 3), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun transposeVerticalDouble() {
        parseAPLExpression("⊖ 3 4 ⍴ int:ensureDouble 0.7+⍳12", collapse = false).let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            assertArrayContentDouble(doubleArrayOf(8.7, 9.7, 10.7, 11.7, 4.7, 5.7, 6.7, 7.7, 0.7, 1.7, 2.7, 3.7), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun transposeArrayLong() {
        parseAPLExpression("⍉ 2 3 ⍴ ⍳12",collapse=false).let { result ->
            assertDimension(dimensionsOfSize(3, 2), result)
            assertArrayContent(arrayOf(0, 3, 1, 4, 2, 5), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun transposeArrayDouble() {
        parseAPLExpression("⍉ 2 3 ⍴ int:ensureDouble 0.1+⍳12",collapse=false).let { result ->
            assertDimension(dimensionsOfSize(3, 2), result)
            assertArrayContentDouble(doubleArrayOf(0.1, 3.1, 1.1, 4.1, 2.1, 5.1), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun concatenatedArraysInt() {
        parseAPLExpression("1 2 3 , 10 11",collapse=false).let { result ->
            assert1DArray(arrayOf(1, 2, 3, 10, 11), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun concatenatedArraysDouble() {
        parseAPLExpression("1.5 2.0 3.5 , 10.5 11.5", collapse = false).let { result ->
            assert1DArray(
                arrayOf(
                    InnerDouble(1.5),
                    InnerDouble(2.0),
                    InnerDouble(3.5),
                    InnerDouble(10.5),
                    InnerDouble(11.5)
                ), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun concatenatedArraysMixed() {
        parseAPLExpression("1 2 3 , 10.5 11.5", collapse = false).let { result ->
            assert1DArray(arrayOf(1, 2, 3, InnerDouble(10.5), InnerDouble(11.5)), result)
            assertEquals(ArrayMemberType.ATOM, result.specialisedType)
        }
    }

    @Test
    fun concatenateSingleElementWithArrayInt() {
        parseAPLExpression("1 2 , 3", collapse = false).let { result ->
            assert1DArray(arrayOf(1, 2, 3), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
        parseAPLExpression("1 , 2 3", collapse = false).let { result ->
            assert1DArray(arrayOf(1, 2, 3), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun concatenateSingleElementWithArrayDouble() {
        parseAPLExpression("1.0 2.0 , 3.0", collapse = false).let { result ->
            assert1DArray(arrayOf(InnerDouble(1.0), InnerDouble(2.0), InnerDouble(3.0)), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
        parseAPLExpression("1.0 , 2.5 3.0", collapse=false).let { result ->
            assert1DArray(arrayOf(InnerDouble(1.0), InnerDouble(2.5), InnerDouble(3.0)), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun concatenateScalarInt() {
        parseAPLExpression("1 , 2", collapse=false).let { result ->
            assert1DArray(arrayOf(1, 2), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun concatenateScalarDouble() {
        parseAPLExpression("1.0 , 2.0", collapse=false).let { result ->
            assert1DArray(arrayOf(InnerDouble(1.0), InnerDouble(2.0)), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun sortedArrayInt() {
        parseAPLExpression("∧ 4 5 2", collapse=false).let { result ->
            assert1DArray(arrayOf(2, 4, 5), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
        parseAPLExpression("∨ 4 5 2", collapse=false).let { result ->
            assert1DArray(arrayOf(5, 4, 2), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun sortedArrayDouble() {
        parseAPLExpression("∧ 3.5 5.5 1.0", collapse=false).let { result ->
            assert1DArray(arrayOf(InnerDouble(1.0), InnerDouble(3.5), InnerDouble(5.5)), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
        parseAPLExpression("∨ 3.5 5.5 1.0", collapse=false).let { result ->
            assert1DArray(arrayOf(InnerDouble(5.5), InnerDouble(3.5), InnerDouble(1.0)), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun indexedArrayInt() {
        parseAPLExpression("(1 2 3)[1 2]", collapse=false).let { result ->
            assert1DArray(arrayOf(2, 3), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun indexedArrayDouble() {
        parseAPLExpression("(1.5 2.5 3.5)[1 2]", collapse=false).let { result ->
            assert1DArray(arrayOf(InnerDouble(2.5), InnerDouble(3.5)), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun indexedPickInt() {
        parseAPLExpression("1 2 ⊇ 10 20 30", collapse=false).let { result ->
            assert1DArray(arrayOf(20, 30), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun indexedPickDouble() {
        parseAPLExpression("1 2 ⊇ 10.0 20.0 30.0", collapse=false).let { result ->
            assert1DArray(arrayOf(InnerDouble(20.0), InnerDouble(30.0)), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun indexedArrayGeneric() {
        parseAPLExpression("(1.5 2.5 3)[1 2]", collapse=false).let { result ->
            assert1DArray(arrayOf(InnerDouble(2.5), 3), result)
            assertEquals(ArrayMemberType.ATOM, result.specialisedType)
        }
    }

    @Test
    fun resizedArrayInt() {
        parseAPLExpression("2 3 ⍴ 1 2 3 4 5 6", collapse=false).let { result ->
            assertDimension(dimensionsOfSize(2, 3), result)
            assertArrayContent(arrayOf(1, 2, 3, 4, 5, 6), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun resizedArrayDouble() {
        parseAPLExpression("2 3 ⍴ 1.0 2.0 3.0 4.0 5.0 6.0", collapse=false).let { result ->
            assertDimension(dimensionsOfSize(2, 3), result)
            assertArrayContent(
                arrayOf(
                    InnerDouble(1.0),
                    InnerDouble(2.0),
                    InnerDouble(3.0),
                    InnerDouble(4.0),
                    InnerDouble(5.0),
                    InnerDouble(6.0),
                ), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun resizedArrayChar() {
        parseAPLExpression("7 ⍴ @a", collapse=false).let { result ->
            assertString("aaaaaaa", result)
            assertEquals(ArrayMemberType.CHAR, result.specialisedType)
        }
    }

    @Test
    fun resizedArrayCharArray() {
        parseAPLExpression("7 ⍴ \"ab\"", collapse=false).let { result ->
            assertString("abababa", result)
            assertEquals(ArrayMemberType.CHAR, result.specialisedType)
        }
    }

    @Test
    fun ravelResultInt() {
        parseAPLExpression(",2 3 ⍴ 1 2 3 4 5 6", collapse=false).let { result ->
            assert1DArray(arrayOf(1, 2, 3, 4, 5, 6), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun ravelResultDouble() {
        parseAPLExpression(",2 3 ⍴ 1.0 2.0 3.0 4.0 5.0 6.0", collapse=false).let { result ->
            assert1DArray(
                arrayOf(
                    InnerDouble(1.0),
                    InnerDouble(2.0),
                    InnerDouble(3.0),
                    InnerDouble(4.0),
                    InnerDouble(5.0),
                    InnerDouble(6.0),
                ), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }

    @Test
    fun ravelSingleElementInt() {
        parseAPLExpression(",5", collapse=false).let { result ->
            assert1DArray(arrayOf(5), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun ravelSingleElementDouble() {
        parseAPLExpression(",5.5", collapse = false).let { result ->
            assert1DArray(arrayOf(InnerDouble(5.5)), result)
            assertEquals(ArrayMemberType.DOUBLE, result.specialisedType)
        }
    }
}
