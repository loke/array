package com.dhsdevelopments.kap.ffi

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.KotlinObjectWrappedValue
import com.dhsdevelopments.kap.builtins.ensureType

class FfiModule private constructor(val ffiManager: FfiManager) : KapModule {
    override val name get() = "ffi"

    private lateinit var keywordChar: Symbol
    private lateinit var keywordInt: Symbol
    private lateinit var keywordString: Symbol
    private lateinit var keywordPtr: Symbol

    fun initKeywords(engine: Engine) {
        keywordChar = engine.keywordNamespace.internSymbol("char")
        keywordInt = engine.keywordNamespace.internSymbol("int")
        keywordString = engine.keywordNamespace.internSymbol("string")
        keywordPtr = engine.keywordNamespace.internSymbol("ptr")
    }

    override fun init(engine: Engine) {
        initKeywords(engine)
        val ns = engine.makeNamespace("ffi")
        engine.registerFunction(ns.internAndExport("libraries"), ListLibrariesFunction(this))
        engine.registerFunction(ns.internAndExport("loadLibrary"), LoadLibraryFunction(this))
        engine.registerFunction(ns.internAndExport("findFunction"), FindNativeFn(this))
        engine.registerFunction(ns.internAndExport("callFunction"), CallNativeFn(this))
        engine.registerFunction(ns.internAndExport("allocBytes"), AllocBytes(this))
        engine.registerFunction(ns.internAndExport("free"), FreeBufferKapFunction(this))
        engine.registerFunction(ns.internAndExport("copyInto"), CopyIntoPtrFunction(this))
        engine.registerFunction(ns.internAndExport("readFrom"), CopyFromPtrFunction(this))
    }

    override fun close() {
        ffiManager.close()
    }

    class InputTypeWrapper(val argType: InputArgType<*>, val conv: (APLValue, Position) -> Any)
    class OutputTypeWrapper(val argType: InputArgType<*>, val conv: (FfiFunctionDescriptor.CallResult<*>, Position) -> APLValue)

    fun parseInputArgType(a: APLValue, pos: Position): InputTypeWrapper {
        val a0 = a.collapse()
        return when (val sym = a0.ensureSymbol(pos).value) {
            keywordChar -> InputTypeWrapper(ffiManager.makeInputArgChar()) { v, pos2 -> v.ensureNumber(pos2).asLong(pos2).also { checkRange(it, 255) } }
            keywordInt -> InputTypeWrapper(ffiManager.makeInputArgInt()) { v, pos2 -> v.ensureNumber(pos2).asInt(pos2) }
            keywordString -> InputTypeWrapper(ffiManager.makeInputArgString()) { v, pos2 -> v.toStringValue(pos2) }
            keywordPtr -> InputTypeWrapper(ffiManager.makeInputArgPtr()) { v, pos2 -> kapPtrOrBufferToFfiPtr(v, pos2) }
            else -> throwAPLException(APLIllegalArgumentException("Unexpected keyword: ${sym}"))
        }
    }

    fun parseOutputArgType(a: APLValue, pos: Position): OutputTypeWrapper {
        val a0 = a.collapse()
        return when (val sym = a0.ensureSymbol(pos).value) {
            keywordChar -> OutputTypeWrapper(ffiManager.makeInputArgChar()) { v, pos2 -> APLLong((v.result as Byte).toLong()) }
            keywordInt -> OutputTypeWrapper(ffiManager.makeInputArgInt()) { v, pos2 -> APLLong((v.result as Int).toLong()) }
            keywordString -> OutputTypeWrapper(ffiManager.makeInputArgString()) { v, pos2 -> APLString.make(v.result as String) }
            keywordPtr -> OutputTypeWrapper(ffiManager.makeInputArgPtr()) { v, pos2 -> FfiPointerKapValue(v.result as FfiPointer) }
            else -> throwAPLException(APLIllegalArgumentException("Unexpected keyword: ${sym}"))
        }
    }

    private fun checkRange(v: Long, high: Long, low: Long = 0) {
        if (v < low || v > high) {
            throwAPLException(APLIllegalArgumentException("Value not in range (${low}-${high}). Got: $v"))
        }
    }

    companion object {
        fun make(): FfiModule? {
            return try {
                FfiModule(ffiManager())
            } catch (e: FfiNotAvailableException) {
                null
            }
        }
    }
}

object FfiPointerKapClass : ModuleClass() {
    override val name get() = "ffiPointer"
}

private fun kapPtrOrBufferToFfiPtr(a: APLValue, pos: Position): FfiPointer {
    return when (val a0 = a.unwrapDeferredValue()) {
        is FfiBufferKapValue -> a0.value.pointerToOffset(0)
        is FfiPointerKapValue -> a0.value
        else -> throwAPLException(APLIllegalArgumentException("Value must be an FFI pointer or buffer, was: ${a::class.simpleName}", pos))
    }
}

class ListLibrariesFunction(val module: FfiModule) : APLFunctionDescriptor {
    inner class ListLibrariesFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ListLibrariesFunctionImpl(instantiation)
}

class FfiLibraryKapValue(value: FfiLibrary) : KotlinObjectWrappedValue<FfiLibrary>(value) {
    override fun formatted(style: FormatStyle) = "[native-library: ${value.name}]"

    companion object {
        fun ensureValue(a: APLValue, pos: Position): FfiLibraryKapValue = ensureType(a, "FFI library", pos)
    }
}

class FfiFunctionDescriptorKapValue<R>(
    value: FfiFunctionDescriptor<R>,
    val returnType: FfiModule.OutputTypeWrapper,
    val argTypes: List<FfiModule.InputTypeWrapper>
) : KotlinObjectWrappedValue<FfiFunctionDescriptor<R>>(value) {
    override fun formatted(style: FormatStyle) = "[ffi-function]"

    companion object {
        fun <R> ensureValue(a: APLValue, pos: Position): FfiFunctionDescriptorKapValue<R> = ensureType(a, "FFI function", pos)
    }
}

class LoadLibraryFunction(val module: FfiModule) : APLFunctionDescriptor {
    inner class LoadLibraryFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val file = a.toStringValue(pos)
            val lib = try {
                module.ffiManager.loadLibrary(file)
            } catch (e: LoadLibraryFailedException) {
                throwAPLException(APLEvalException("Error from FFI: ${e.message}", pos, e))
            }
            return FfiLibraryKapValue(lib)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = LoadLibraryFunctionImpl(instantiation)
}

class FindNativeFn(val module: FfiModule) : APLFunctionDescriptor {
    inner class FindNativeFnImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val library = FfiLibraryKapValue.ensureValue(a, pos).value
            val args = b.listify()
            if (args.listSize() < 2) {
                throwAPLException(APLIllegalArgumentException("Arguments must be of at least size 2", pos))
            }
            val name = args.listElement(0).toStringValue(pos)
            val returnType = module.parseOutputArgType(args.listElement(1), pos)
            val argTypes = (0 until args.listSize() - 2).map { i ->
                module.parseInputArgType(args.listElement(i + 2), pos)
            }
            val fn =
                library.findFunction(name, returnType.argType, argTypes.map { v -> v.argType }) ?: throwAPLException(
                    APLEvalException(
                        "Function not found: ${name}",
                        pos))
            return FfiFunctionDescriptorKapValue(fn, returnType, argTypes)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FindNativeFnImpl(instantiation)
}

class CallNativeFn(val module: FfiModule) : APLFunctionDescriptor {
    inner class CallNativeFnImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            val fn: FfiFunctionDescriptorKapValue<Any> = FfiFunctionDescriptorKapValue.ensureValue(a, pos)
            val args = b.listify()
            if (args.listSize() != fn.argTypes.size) {
                throwAPLException(APLIllegalArgumentException("FFI function expects argument list of size: ${fn.argTypes.size}, got size: ${args.listSize()}"))
            }
            val converted = fn.argTypes.mapIndexed { i, type ->
                type.conv(args.listElement(i), pos)
            }
            val result = fn.value.call(converted)
            return fn.returnType.conv(result, pos)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = CallNativeFnImpl(instantiation)
}

class FfiBufferKapValue(value: FfiBuffer) : KotlinObjectWrappedValue<FfiBuffer>(value) {
    override fun formatted(style: FormatStyle) = "[ffi-buffer]"

    companion object {
        fun ensureValue(a: APLValue, pos: Position): FfiBufferKapValue = ensureType(a, "FFI buffer", pos)
    }
}

class FfiPointerKapValue(value: FfiPointer) : KotlinObjectWrappedValue<FfiPointer>(value) {
    override fun formatted(style: FormatStyle) = "[ffi-buffer]"

    override val kapClass get() = FfiPointerKapClass

    companion object {
        fun ensureValue(a: APLValue, pos: Position): FfiPointerKapValue = ensureType(a, "FFI pointer", pos)
    }
}

class AllocBytes(val module: FfiModule) : APLFunctionDescriptor {
    inner class AllocBytesImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val size = a.ensureNumber(pos).asLong(pos)
            if (size < 1) {
                throwAPLException(APLIllegalArgumentException("Invalid size: ${size}", pos))
            }
            val buffer = module.ffiManager.allocBytes(size)
            return FfiBufferKapValue(buffer)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = AllocBytesImpl(instantiation)
}

class FreeBufferKapFunction(val module: FfiModule) : APLFunctionDescriptor {
    inner class FreeBufferKapFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val buffer = FfiBufferKapValue.ensureValue(a, pos).value
            buffer.close()
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FreeBufferKapFunctionImpl(instantiation)
}

class CopyIntoPtrFunction(val module: FfiModule) : APLFunctionDescriptor {
    inner class CopyIntoPtrFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val ptr = kapPtrOrBufferToFfiPtr(a, pos)
            val b0 = b.arrayify()
            unless(b0.rank == 1) {
                throwAPLException(InvalidDimensionsException("Left argument must be a scalar or 1-dimensional array", pos))
            }
            val buf = ByteArray(b0.size) { i ->
                val v = b0.valueAtCoerceToLong(i, pos)
                if (v !in 0..255) {
                    throwAPLException(APLIllegalArgumentException("Value at index ${i} does not fit in a byte: ${v}"))
                }
                v.toByte()
            }
            ptr.copyBytesFromArray(0, buf, 0, buf.size)
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = CopyIntoPtrFunctionImpl(instantiation)
}

class CopyFromPtrFunction(val module: FfiModule) : APLFunctionDescriptor {
    inner class CopyFromPtrFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val ptr = kapPtrOrBufferToFfiPtr(b, pos)
            val count = a.ensureNumber(pos).asInt(pos)
            val buf = ByteArray(count)
            ptr.copyBytesToArray(0, buf, 0, count)
            return APLArrayByte(dimensionsOfSize(buf.size), buf)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = CopyFromPtrFunctionImpl(instantiation)
}
