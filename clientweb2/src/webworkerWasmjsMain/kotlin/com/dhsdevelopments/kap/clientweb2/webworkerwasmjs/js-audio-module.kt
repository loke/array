package com.dhsdevelopments.kap.clientweb2.webworkerwasmjs

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.clientweb2.PLAY_AUDIO_TYPE
import kotlin.math.max
import kotlin.math.min

class JsAudioModule(val sendMessageFn: (JsAny) -> Unit) : KapModule {
    override val name get() = "jsaudio"

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("audio")
        engine.registerFunction(ns.internAndExport("play"), PlayAudioFunction(this))
    }
}

class PlayAudioFunction(val owner: JsAudioModule) : APLFunctionDescriptor {
    inner class PlayAudioFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val d = a.dimensions
            if (d.size != 1) {
                throwAPLException(InvalidDimensionsException("Argument must be a 1-dimensional array", pos))
            }
            val size = d[0]
            val jsBuffer = makeJsArrayOfSize<JsNumber>(size)
            repeat(size) { i ->
                jsBuffer[i] = max(min(a.valueAtDouble(i, pos), 1.0), -1.0).toJsNumber()
            }
            owner.sendMessageFn(makePlayAudioMessage(jsBuffer))
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = PlayAudioFunctionImpl(instantiation)
}

private fun makePlayAudioMessage(buffer: JsArray<JsNumber>): JsAny {
    return makeJsObject(
        "messageType" to PLAY_AUDIO_TYPE.toJsString(),
        "buffer" to buffer)
}
