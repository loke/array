package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertFailsWith

class StandardLibSimpleFunctionsTest : APLTest() {
    @Test
    fun formatterTest() {
        val src =
            """
            |use("standard-lib.kap")
            |use("output3.kap")
            |o3:format 2 2 ⍴ 1 100 10000 1.2
            """.trimMargin()
        parseAPLExpression(src)
    }

    @Test
    fun toHexTestDefaultSize() {
        parseAPLExpression("io:toHex¨ 74667 4096 0 16 3", withStandardLib = true).let { result ->
            assert1DArray(arrayOf("123AB", "1000", "0", "10", "3"), result)
        }
    }

    @Test
    fun toHexWithSize() {
        parseAPLExpression("12 io:toHex 74667", withStandardLib = true).let { result ->
            assertString("0000000123AB", result)
        }
    }

    @Test
    fun fromHex() {
        val src = "io:fromHex¨ \"0\" \"1\" \"1ff\" \"1ABCDEF\" \"123456abcdef0123456789\" \"10000000000000000000000000000000000000\""
        parseAPLExpression(src, withStandardLib = true).let { result ->
            assert1DArray(
                arrayOf(
                    0, 1, 511, 28036591,
                    InnerBigIntOrLong("22007826609976624594380681"),
                    InnerBigIntOrLong("356811923176489970264571492362373784095686656")),
                result)
        }
    }

    @Test
    fun fromHexInvalidCharacters() {
        assertFailsWith<APLEvalException> {
            parseAPLExpression("io:fromHex \"qwetest\"", withStandardLib = true)
        }
    }

    @Test
    fun base64EncodeTest() {
        parseAPLExpression("io:base64Encode io:encodeUtf8 \"teststring1234\"", withStandardLib = true).let { result ->
            assertString("dGVzdHN0cmluZzEyMzQ=", result)
        }
    }
}
