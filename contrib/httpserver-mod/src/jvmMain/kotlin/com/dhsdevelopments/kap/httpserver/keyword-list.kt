package com.dhsdevelopments.kap.httpserver

import com.dhsdevelopments.kap.*

class KeywordList(private val params: Map<String, APLValue>) {
    fun lookup(name: String) = params[name]

    fun lookupInteger(s: String, pos: Position?): Int? {
        val v = lookup(s) ?: return null
        return v.ensureNumber(pos).asInt(pos)
    }

    companion object {
        fun parse(arg: APLValue, pos: Position?): KeywordList {
            val d = arg.dimensions
            unless((d.size == 1 && d[0] % 2 == 0) || (d.size == 2 && d[0] == 2)) {
                throwAPLException(
                    InvalidDimensionsException(
                        "Keyword list needs to be a 1-dimensional array with an even number of element, or a 2-dimensional array with two columns",
                        pos))
            }
            val res = HashMap<String, APLValue>()
            for (i in 0 until d.contentSize() step 2) {
                val key = arg.valueAt(i).unwrapDeferredValue()
                if (key !is APLSymbol || key.value.namespace != NamespaceList.KEYWORD_NAMESPACE_NAME) {
                    throwAPLException(APLIllegalArgumentException("Non-keyword key found in argument at index: ${i}", pos))
                }
                val name = key.value.symbolName
                val value = arg.valueAt(i + 1).collapse()
                res[name] = value
            }
            return KeywordList(res)
        }
    }
}
