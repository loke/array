@file:Suppress("EXPECT_ACTUAL_CLASSIFIERS_ARE_IN_BETA_WARNING")

package com.dhsdevelopments.kap

class SleepNotSupportedException : Exception("Sleep not supported")

expect fun sleepMillis(engine: Engine, time: Long)
expect fun sleepMillisCallback(engine: Engine, time: Long, pos: Position, callback: () -> Either<APLValue, Exception>)

/**
 * Format a double in a standardised way. A value with zero decimal part should be rendered as 4.0.
 * This is needed because Javascript does not include the decimal by default.
 */
expect fun Double.formatDouble(): String

class RegexpParseException(message: String, cause: Throwable) : Exception(message, cause)

expect fun toRegexpWithException(string: String, options: Set<RegexOption>): Regex

expect fun numCores(): Int

interface BackgroundTask<T> {
    fun await(): T
}

interface MPThreadPoolExecutor {
    val numThreads: Int
    fun <T : Any> start(fn: () -> T): BackgroundTask<out T>
    fun close()
}

class SingleThreadedThreadPoolExecutor : MPThreadPoolExecutor {
    override val numThreads get() = 1

    override fun <T : Any> start(fn: () -> T): BackgroundTask<T> {
        return object : BackgroundTask<T> {
            override fun await(): T {
                return withThreadLocalsUnassigned {
                    fn()
                }
            }
        }
    }

    override fun close() {}
}

expect fun makeBackgroundDispatcher(numThreads: Int): MPThreadPoolExecutor

interface MPWeakReference<T> {
    val value: T?

    companion object
}

expect fun <T : Any> MPWeakReference.Companion.make(ref: T): MPWeakReference<T>

interface TimerHandler {
    fun registerTimer(delays: IntArray, callbacks: List<LambdaValue>): APLValue
}

expect fun makeTimerHandler(engine: Engine): TimerHandler?

expect fun getEnvValue(name: String): String?
expect val getEnvValueSupported: Boolean

interface NativeData {
    fun close()
}

expect fun makeNativeData(): NativeData

expect inline fun nativeUpdateBreakPending(engine: Engine, state: Boolean)
expect inline fun nativeBreakPending(engine: Engine): Boolean

expect fun findInstallPath(): String?

expect val brokenNegativeZero: Boolean
