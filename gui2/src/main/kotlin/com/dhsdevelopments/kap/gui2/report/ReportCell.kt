package com.dhsdevelopments.kap.gui2.report

import javax.swing.JLabel

class ReportCell {
    var value: String? = null

    fun formatLabel(label: JLabel) {
        label.text = value ?: ""
    }

    fun updateContent(newValue: String) {
        value = newValue
    }
}
