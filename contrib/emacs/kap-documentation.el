;;; -*- lexical-binding: t -*-

(require 'kap-refdocs)

(defcustom kap-keyboard-simplified-mouse-action-mode t
  "Defines the action to be performed on mouse over the symbol in
keyboard help. Possible variants:
nil - tooltip shows help on possible actions,
mouse 1 to open help window, mouse 3 to insert symbol
t - inspired by Dyalog APL IDE toolbar, tooltip shows symbol
help, mouse 1 to insert symbol, mouse 2 to open help window"
  :type 'boolean
  :group 'kap)

(defvar *kap-keymap-buffer-name* "*kap keymap*")

(defvar kap-keymap-template
  "╔════╦════╦════╦════╦════╦════╦════╦════╦════╦════╦════╦════╦════╦═════════╗
║ ~∇ ║ !∇ ║ @∇ ║ #∇ ║ $∇ ║ %∇ ║ ^∇ ║ &∇ ║ *∇ ║ (∇ ║ )∇ ║ _∇ ║ +∇ ║         ║
║ `∇ ║ 1∇ ║ 2∇ ║ 3∇ ║ 4∇ ║ 5∇ ║ 6∇ ║ 7∇ ║ 8∇ ║ 9∇ ║ 0∇ ║ -∇ ║ =∇ ║ BACKSP  ║
╠════╩══╦═╩══╦═╩══╦═╩══╦═╩══╦═╩══╦═╩══╦═╩══╦═╩══╦═╩══╦═╩══╦═╩══╦═╩══╦══════╣
║       ║ Q∇ ║ W∇ ║ E∇ ║ R∇ ║ T∇ ║ Y∇ ║ U∇ ║ I∇ ║ O∇ ║ P∇ ║ {∇ ║ }∇ ║  |∇  ║
║  TAB  ║ q∇ ║ w∇ ║ e∇ ║ r∇ ║ t∇ ║ y∇ ║ u∇ ║ i∇ ║ o∇ ║ p∇ ║ [∇ ║ ]∇ ║  \\∇  ║
╠═══════╩═╦══╩═╦══╩═╦══╩═╦══╩═╦══╩═╦══╩═╦══╩═╦══╩═╦══╩═╦══╩═╦══╩═╦══╩══════╣
║ (CAPS   ║ A∇ ║ S∇ ║ D∇ ║ F∇ ║ G∇ ║ H∇ ║ J∇ ║ K∇ ║ L∇ ║ :∇ ║ \"∇ ║         ║
║  LOCK)  ║ a∇ ║ s∇ ║ d∇ ║ f∇ ║ g∇ ║ h∇ ║ j∇ ║ k∇ ║ l∇ ║ ;∇ ║ '∇ ║ RETURN  ║
╠═════════╩═══╦╩═══╦╩═══╦╩═══╦╩═══╦╩═══╦╩═══╦╩═══╦╩═══╦╩═══╦╩═══╦╩═════════╣
║             ║ Z∇ ║ X∇ ║ C∇ ║ V∇ ║ B∇ ║ N∇ ║ M∇ ║ <∇ ║ >∇ ║ ?∇ ║          ║
║  SHIFT      ║ z∇ ║ x∇ ║ c∇ ║ v∇ ║ b∇ ║ n∇ ║ m∇ ║ ,∇ ║ .∇ ║ /∇ ║  SHIFT   ║
╚═════════════╩════╩════╩════╩════╩════╩════╩════╩════╩════╩════╩══════════╝"
  "Kap keyboard layout template. The nable character gets replaced with
the corresponding symbol from the actual keymap.")

(defun kap-keymap-mode-kill-buffer ()
  "Close the buffer displaying the keymap."
  (interactive)
  (let ((buffer (get-buffer *kap-keymap-buffer-name*)))
    (when buffer
      (delete-windows-on buffer)
      (kill-buffer buffer))))

(defun kap--get-doc-for-symbol (string)
  (cl-loop for e in kap--symbol-doc
           for name = (car e)
           when (or (and (stringp name)
                         (string= string name))
                    (and (listp name)
                         (cl-find string name :test #'string=)))
           return e
           finally (return nil)))

(defun kap--get-full-docstring-for-native-symbol (sym full-text-p)
  (let ((doc (kap--get-doc-for-symbol sym))
        (format-short 
         (if full-text-p "\n%s\n\n" "\n%s\n")))
    (when doc
      (with-temp-buffer
        (cl-loop for e in (cl-second doc)
                 for first = t then nil
                 unless first
                 do (insert "\n")
                 do (progn
                      (insert (format "%s: %s" (cl-first e) (cl-second e)))
                      (insert (format format-short (cl-third e)))
                      (let ((long (cl-fourth e)))
                        (when long
                          (insert (format "%s\n" long)))))
                 when full-text-p
                 do (insert "\n===================================\n"))
        (buffer-string)))))

(defun kap--get-full-docstring-for-symbol (sym full-text-p)
  "Get the documentation for the symbol or function STRING.
When FULL-TEXT is t format the output string suitable for separate
buffer. Otherwise try to make it short to fit into the tooltip."
  (kap--get-full-docstring-for-native-symbol sym full-text-p))

(defun kap-symbol-insert-from-keymap ()
  "Send a symbol from the keymap buffer to the current Kap interpreter."
  (interactive)
  (let ((string (get-text-property (point) 'kap-insert))
        (session (kap--get-interactive-session)))
    (with-current-buffer session
      (insert string))))

(defun kap-mouse-help-from-keymap (event)
  "In the keymap buffer, describe the symbol that was clicked."
  (interactive "e")
  (let ((window (posn-window (event-end event)))
        (pos (posn-point (event-end event))))
    (unless (windowp window)
      (error "Can't find window"))
    (let ((string (with-current-buffer (window-buffer window)
                    (get-text-property pos 'kap-insert))))
      (kap-show-help-for-symbol string))))

(defun kap-symbol-help-from-keymap ()
  "Describe a symbol in the keymap buffer."
  (interactive)
  (let ((string (get-text-property (point) 'kap-insert)))
    (kap-show-help-for-symbol string)))

(defun kap-mouse-insert-from-keymap (event)
  "In the keymap buffer, insert the symbol that was clicked."
  (interactive "e")
  (let ((window (posn-window (event-end event)))
        (pos (posn-point (event-end event))))
    (unless (windowp window)
      (error "Can't find window"))
    (let* ((string (with-current-buffer (window-buffer window)
                     (get-text-property pos 'kap-insert)))
           (session (kap--get-interactive-session))
           (interactive-session-windows
            (get-buffer-window-list session nil 'visible)))
      (with-current-buffer session
        (insert string))
      ;; after we have inserted the special character,
      ;; it is reasonable to switch focus back to the interactive
      ;; APL session to continue typing.
      ;; NOTE: if there are more than 1 visible windows
      ;; with the same interactive session, the first
      ;; one will be activated.
      (when interactive-session-windows
        (select-window (car interactive-session-windows))
        ;; advance point after the inserted string
        (goto-char (+ (point) (length string)))))))

(defun kap--make-help-property-keymap ()
  (let ((map (make-sparse-keymap)))  
    (cond (kap-keyboard-simplified-mouse-action-mode
           (define-key map [mouse-1] 'kap-mouse-insert-from-keymap)
           (define-key map [down-mouse-2] 'kap-mouse-help-from-keymap))
          (t
           (define-key map [down-mouse-1] 'kap-mouse-help-from-keymap)
           (define-key map [mouse-3] 'kap-mouse-insert-from-keymap)))
    (define-key map (kbd "?") 'kap-symbol-help-from-keymap)
    (define-key map (kbd "RET") 'kap-symbol-insert-from-keymap)
    map))

(defvar kap-keymap-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "q") 'kap-keymap-mode-kill-buffer)
    map)
  "Keymap for keymap mode buffers")

(define-derived-mode kap-keymap-mode fundamental-mode "Kap-keymap"
  "Major mode for displaying the keymap help."
  (use-local-map kap-keymap-mode-map)
  (read-only-mode 1)
  (setq truncate-lines t))

(defun kap--make-clickable (string keymap)
  (let ((help-echo-string (concat "mouse-1: Show documentation for " string "\n"
                                  "mouse-3: Insert " string " in Kap buffer"))
        (description
         (kap--get-full-docstring-for-symbol string
                                             (not kap-keyboard-simplified-mouse-action-mode))))
    (cond ((and kap-keyboard-simplified-mouse-action-mode
                description)
           (setf help-echo-string description))           
          (kap-keyboard-simplified-mouse-action-mode
           (setf help-echo-string "No documentation available")))
    (propertize string
                'mouse-face 'highlight
                'help-echo help-echo-string
                'kap-insert string
                'keymap keymap
                )))

(defun kap--make-readable-keymap ()
  ;; Ensure that the buffer is recreated
  (let ((old-buffer (get-buffer *kap-keymap-buffer-name*)))
    (when old-buffer
      (kill-buffer old-buffer)))
  ;; Recreate the buffer according to the active keymap.
  (let ((buffer (get-buffer-create *kap-keymap-buffer-name*))
        (keymap (kap--make-help-property-keymap)))
    (with-current-buffer buffer
      (delete-region (point-min) (point-max))
      (insert kap-keymap-template)
      (goto-char (point-min))
      (while (search-forward-regexp "\\(.\\)∇" nil t)
        (let* ((key (match-string 1))
               (found (cl-find key kap--symbols :key #'cl-first :test #'equal))
               (found-nonspecial (cl-find key kap--symbol-doc :key #'cl-first :test #'equal))
               (result-string (if found (save-match-data (kap--make-clickable (cl-second found) keymap)) " "))
               (nonspecial-string (if found-nonspecial (kap--make-clickable key keymap) key)))
          (replace-match (concat nonspecial-string result-string) t t)))
      (add-text-properties (point-min) (point-max) (list 'face 'kap-kbd-help-screen))
      (kap-keymap-mode))
    buffer))

(defun kap-show-keyboard (&optional arg)
  "When arg is nil, toggle the display of the keyboard help.
If positive, always show the buffer, if negative close the buffer
if it is open."
  (interactive "P")
  (let ((keyboard-help (get-buffer *kap-keymap-buffer-name*)))
    (if (and keyboard-help (get-buffer-window keyboard-help))
        ;; The buffer is displayed. Maybe close it.
        (when (or (null arg) (cl-minusp arg))
          (kap-keymap-mode-kill-buffer))
      ;; The buffer is not displayed, check if it's supposed to be displayed
      (when (or (null arg) (cl-plusp arg))
        (let* ((buffer (or (when nil ; Make sure the buffer is always created
                             (get-buffer *kap-keymap-buffer-name*))
                           (kap--make-readable-keymap)))
               (window (split-window nil)))
          (set-window-buffer window buffer)
          (fit-window-to-buffer window))))))

(defun kap-show-help-for-symbol (symbol)
  "Open the help window for SYMBOL."
  (interactive (list (let ((default-sym (kap--name-at-point)))
                       (read-string (if default-sym
                                        (format "Symbol (default '%s'): " default-sym)
                                      "Symbol: ")
                                    nil nil default-sym t))))
  (when (or (null symbol) (string= symbol ""))
    (error "Symbol is empty"))
  (let ((string (kap--get-full-docstring-for-symbol symbol t)))
    (unless string
      (user-error "No documentation available for %s" symbol))
    (let ((buffer (get-buffer-create *kap-documentation-buffer-name*)))
      (with-current-buffer buffer
        (read-only-mode 0)
        (delete-region (point-min) (point-max))
        (insert string)
        (goto-char (point-min))
        (add-text-properties (point-min) (point-max) '(face kap-help))
        (kap-documentation-mode)
        (read-only-mode 1))
      (pop-to-buffer buffer))))

(provide 'kap-documentation)
