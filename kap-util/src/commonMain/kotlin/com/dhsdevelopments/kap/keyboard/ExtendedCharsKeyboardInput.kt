package com.dhsdevelopments.kap.keyboard

class ExtendedCharsKeyboardInput {
    val keymap: Map<KeyDescriptor, String>

    init {
        keymap = hashMapOf(
            // First row
            makeKeyDescriptor("`") to "⋄",
            makeKeyDescriptor("1") to "¨", makeKeyDescriptor("!", Flag.SHIFT) to "⌶",
            makeKeyDescriptor("2") to "¯", makeKeyDescriptor("@", Flag.SHIFT) to "⍫",
            makeKeyDescriptor("3") to "≤", makeKeyDescriptor("#", Flag.SHIFT) to "⍒",
            makeKeyDescriptor("4") to "≥", makeKeyDescriptor("$", Flag.SHIFT) to "⍋",
            makeKeyDescriptor("5") to "⟦", makeKeyDescriptor("%", Flag.SHIFT) to "⌽",
            makeKeyDescriptor("6") to "⟧", makeKeyDescriptor("^", Flag.SHIFT) to "⍉",
            /* Unshifted 7 is unassigned */ makeKeyDescriptor("&", Flag.SHIFT) to "⊖",
            makeKeyDescriptor("8") to "≠", makeKeyDescriptor("*", Flag.SHIFT) to "⍟",
            makeKeyDescriptor("9") to "∨", makeKeyDescriptor("(", Flag.SHIFT) to "⍱",
            makeKeyDescriptor("0") to "∧", makeKeyDescriptor(")", Flag.SHIFT) to "⍲",
            makeKeyDescriptor("-") to "×", makeKeyDescriptor("_", Flag.SHIFT) to "⍠",
            makeKeyDescriptor("=") to "÷", makeKeyDescriptor("+", Flag.SHIFT) to "⌹",
            // Second row
            makeKeyDescriptor("q") to "⦻", makeKeyDescriptor("Q", Flag.SHIFT) to "⫇",
            makeKeyDescriptor("w") to "⍵", makeKeyDescriptor("W", Flag.SHIFT) to "⍹",
            makeKeyDescriptor("e") to "∊", makeKeyDescriptor("E", Flag.SHIFT) to "⍷",
            makeKeyDescriptor("r") to "⍴", makeKeyDescriptor("R", Flag.SHIFT) to "√",
            makeKeyDescriptor("t") to "⍓", makeKeyDescriptor("T", Flag.SHIFT) to "⍨",
            makeKeyDescriptor("y") to "↑", makeKeyDescriptor("Y", Flag.SHIFT) to "≬",
            makeKeyDescriptor("u") to "↓", makeKeyDescriptor("U", Flag.SHIFT) to "⇐",
            makeKeyDescriptor("i") to "⍳", makeKeyDescriptor("I", Flag.SHIFT) to "⍸",
            makeKeyDescriptor("o") to "○", makeKeyDescriptor("O", Flag.SHIFT) to "⍥",
            makeKeyDescriptor("p") to "⋆", makeKeyDescriptor("P", Flag.SHIFT) to "⍣",
            makeKeyDescriptor("[") to "←", makeKeyDescriptor("{", Flag.SHIFT) to "⍞",
            makeKeyDescriptor("]") to "→", makeKeyDescriptor("}", Flag.SHIFT) to "⍬",
            makeKeyDescriptor("\\") to "⊢", makeKeyDescriptor("|", Flag.SHIFT) to "⊣",
            // Third row
            makeKeyDescriptor("a") to "⍺", makeKeyDescriptor("A", Flag.SHIFT) to "⍶",
            makeKeyDescriptor("s") to "⌈", makeKeyDescriptor("S", Flag.SHIFT) to "∵",
            makeKeyDescriptor("d") to "⌊", makeKeyDescriptor("D", Flag.SHIFT) to "˝",
            makeKeyDescriptor("f") to "_", makeKeyDescriptor("F", Flag.SHIFT) to "⍛",
            makeKeyDescriptor("g") to "∇", makeKeyDescriptor("G", Flag.SHIFT) to "⍢",
            makeKeyDescriptor("h") to "∆", makeKeyDescriptor("H", Flag.SHIFT) to "⍙",
            makeKeyDescriptor("j") to "∘", makeKeyDescriptor("J", Flag.SHIFT) to "⍤",
            makeKeyDescriptor("k") to "⌸", makeKeyDescriptor("K", Flag.SHIFT) to "⌻",
            makeKeyDescriptor("l") to "⎕", makeKeyDescriptor("L", Flag.SHIFT) to "⌷",
            makeKeyDescriptor(";") to "⍎", makeKeyDescriptor(":", Flag.SHIFT) to "≡",
            makeKeyDescriptor("'") to "⍕", makeKeyDescriptor("\"", Flag.SHIFT) to "≢",
            // Fourth row
            makeKeyDescriptor("z") to "⊂", makeKeyDescriptor("Z", Flag.SHIFT) to "⊆",
            makeKeyDescriptor("x") to "⊃", makeKeyDescriptor("X", Flag.SHIFT) to "⊇",
            makeKeyDescriptor("c") to "∩", makeKeyDescriptor("C", Flag.SHIFT) to "∙",
            makeKeyDescriptor("v") to "∪", makeKeyDescriptor("V", Flag.SHIFT) to "λ",
            makeKeyDescriptor("b") to "⊥", makeKeyDescriptor("B", Flag.SHIFT) to "«",
            makeKeyDescriptor("n") to "⊤", makeKeyDescriptor("N", Flag.SHIFT) to "»",
            makeKeyDescriptor("m") to "…", makeKeyDescriptor("M", Flag.SHIFT) to "∥",
            makeKeyDescriptor(",") to "⍝", makeKeyDescriptor("<", Flag.SHIFT) to "⍪",
            makeKeyDescriptor(".") to "⍀", makeKeyDescriptor(">", Flag.SHIFT) to "⍮",
            makeKeyDescriptor("/") to "⌿", makeKeyDescriptor("?", Flag.SHIFT) to "⫽")
    }

    enum class Flag {
        SHIFT
    }

    private fun makeKeyDescriptor(character: String, vararg flags: Flag): KeyDescriptor {
        return KeyDescriptor(character, flags.contains(Flag.SHIFT))
    }

    val keymap2 = keymap.entries.associate { (key, value) -> value to key }

    data class KeyDescriptor(val character: String, val shift: Boolean)
}

val SYMBOL_DOC_LIST = hashMapOf(
    "←" to SymbolDoc(specialDescription = "assign"),
    "⇐" to SymbolDoc(specialDescription = "assign to function"),
    "⟦" to SymbolDoc(specialDescription = "function argument list open"),
    "⟧" to SymbolDoc(specialDescription = "function argument list close"),
    "+" to SymbolDoc("conjugate", "plus"),
    "-" to SymbolDoc("negate", "minus"),
    "×" to SymbolDoc("direction", "times"),
    "÷" to SymbolDoc("reciprocal", "divide"),
    "*" to SymbolDoc("exponential", "power"),
    "⍟" to SymbolDoc("natural logarithm", "logarithm"),
    "√" to SymbolDoc("square root", "n'th root"),
    "⌹" to SymbolDoc("matrix inverse", "matrix divide"),
    "○" to SymbolDoc("not used", "not used"),
    "!" to SymbolDoc("factorial", "binomial"),
    "?" to SymbolDoc("roll", "deal"),
    "|" to SymbolDoc("magnitude", "residue"),
    "⌈" to SymbolDoc("ceiling", "maximum"),
    "⌊" to SymbolDoc("floor", "minimum"),
    "⊥" to SymbolDoc(null, "decode"),
    "⊤" to SymbolDoc(null, "encode"),
    "⊣" to SymbolDoc("same", "left"),
    "⊢" to SymbolDoc("same", "right"),
    "⌸" to SymbolDoc(null, "key"),
    "=" to SymbolDoc(null, "equal"),
    "≠" to SymbolDoc("unique mask", "not equal"),
    "≤" to SymbolDoc(null, "less than or equal"),
    "<" to SymbolDoc("increase rank", "less than"),
    ">" to SymbolDoc("decrease rank", "greater than"),
    "≥" to SymbolDoc(null, "greater than or equal"),
    "≡" to SymbolDoc("depth", "match"),
    "≢" to SymbolDoc("tally", "not match"),
    "∨" to SymbolDoc("sort decreasing", "or"),
    "∧" to SymbolDoc("sort increasing", "and"),
    "⍲" to SymbolDoc(null, "nand"),
    "⍱" to SymbolDoc(null, "nor"),
    "↑" to SymbolDoc("take first", "take"),
    "↓" to SymbolDoc("drop first", "drop"),
    "⊂" to SymbolDoc("enclose", "partitioned enclose"),
    "⊃" to SymbolDoc("disclose", "pick"),
    "⊆" to SymbolDoc("nest", "partition"),
    "⊇" to SymbolDoc(null, "pick multi"),
    "⌷" to SymbolDoc("list to array", "index"),
    "⍋" to SymbolDoc("grade up", "grades up"),
    "⍒" to SymbolDoc("grade down", "grades down"),
    "≬" to SymbolDoc("array to list"),
    "⍳" to SymbolDoc("indices", "indices of"),
    "∊" to SymbolDoc(null, "member of"),
    "⍷" to SymbolDoc(null, "find"),
    "∪" to SymbolDoc("unique", "union"),
    "∩" to SymbolDoc(null, "intersection"),
    "~" to SymbolDoc("not", "without"),
    "⫽" to SymbolDoc(null, "replicate"),
    "/" to SymbolDoc(dyadicName = "replicate", monadicOperator = "reduce"),
    "\\" to SymbolDoc(dyadicName = "expand", monadicOperator = "scan"),
    "⌿" to SymbolDoc(dyadicName = "replicate first", monadicOperator = "reduce first"),
    "⍀" to SymbolDoc(dyadicName = "expand first", monadicOperator = "scan first"),
    "," to SymbolDoc("ravel", "catenate/laminate"),
    "⍪" to SymbolDoc("table", "catenate first/laminate"),
    "⍮" to SymbolDoc("singleton", "pair"),
    "⍴" to SymbolDoc("shape", "reshape"),
    "⌽" to SymbolDoc("reverse", "rotate"),
    "⊖" to SymbolDoc("reverse first", "rotate first"),
    "⍉" to SymbolDoc("transpose", "reorder axes"),
    "→" to SymbolDoc("return", "conditional return"),
    "¨" to SymbolDoc(monadicOperator = "each"),
    "⍨" to SymbolDoc(monadicOperator = "swap"),
    "⍣" to SymbolDoc(dyadicOperator = "repeat/until"),
    "∙" to SymbolDoc(dyadicOperator = "inner product"),
    "⌻" to SymbolDoc(monadicOperator = "outer product"),
    "˝" to SymbolDoc(monadicOperator = "functional inverse"),
    "∘" to SymbolDoc(dyadicOperator = "bind"),
    "⍛" to SymbolDoc(dyadicOperator = "bind left"),
    "⍤" to SymbolDoc(dyadicOperator = "rank"),
    "⍥" to SymbolDoc(dyadicOperator = "over"),
    "⍢" to SymbolDoc(dyadicOperator = "under"),
    "∵" to SymbolDoc(monadicOperator = "derive bitwise"),
    "∥" to SymbolDoc(monadicOperator = "derive parallel"),
    "λ" to SymbolDoc(specialDescription = "lambda"),
    "⍞" to SymbolDoc(specialDescription = "apply"),
    "⍎" to SymbolDoc("parse string"),
    "⍕" to SymbolDoc("format"),
    "«" to SymbolDoc(specialDescription = "left fork"),
    "»" to SymbolDoc(specialDescription = "right fork"),
    "⋄" to SymbolDoc(specialDescription = "statement separator"),
    "⍝" to SymbolDoc(specialDescription = "comment"),
    "⍵" to SymbolDoc(specialDescription = "right argument"),
    "⍺" to SymbolDoc(specialDescription = "left argument"),
    "∇" to SymbolDoc(specialDescription = "define function"),
    "⍓" to SymbolDoc(specialDescription = "recursion"),
    "¯" to SymbolDoc(specialDescription = "negative"),
    "⍬" to SymbolDoc(specialDescription = "empty numeric vector"),
    "∆" to SymbolDoc(specialDescription = "identifier character"),
    "⍙" to SymbolDoc(specialDescription = "identifier character"),
    "⫇" to SymbolDoc(null, "group"),
    "…" to SymbolDoc(null, "range"))

const val SYMBOL_BAR_LAYOUT =
    "←⇐⟦⟧ +-×÷*⍟√⌹○!? |⌈⌊⊥⊤⊣⊢⌸ =≠≤<>≥≡≢ ∨∧⍲⍱ ↑↓⊂⊃⊆⊇⌷⍋⍒≬⫇ ⍳∊⍷∪∩~⫽/\\⌿⍀… ,⍪⍮⍴⌽⊖⍉ ¨⍨⍣∙⌻˝∘⍛⍤⍥⍢∵∥λ⍞⍎⍕ «»⋄⍝→⍵⍺∇⍓ ¯⍬∆⍙"

class SymbolDoc(
    val monadicName: String? = null,
    val dyadicName: String? = null,
    val monadicOperator: String? = null,
    val dyadicOperator: String? = null,
    val specialDescription: String? = null)
