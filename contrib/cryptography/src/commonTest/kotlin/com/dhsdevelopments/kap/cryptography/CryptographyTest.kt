package com.dhsdevelopments.kap.cryptography

import com.dhsdevelopments.kap.APLTest
import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.Engine

abstract class CryptographyTest : APLTest() {
    fun evalCryptography(src: String): APLValue {
        fun extraInit(engine: Engine) {
            if (engine.findModule<CryptographyModule>() == null) {
                engine.addModule(CryptographyModule())
            }
        }
        return parseAPLExpression(src, extraInit = ::extraInit)
    }
}
