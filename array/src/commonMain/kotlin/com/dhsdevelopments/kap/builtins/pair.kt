package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*

class PairAPLFunction : APLFunctionDescriptor {
    class PairAPLFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            return ResizedArrayImpls.resizedSingleValue(dimensionsOfSize(1), a)
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            return APLArrayImpl(dimensionsOfSize(2), arrayOf(a, b))
        }
    }

    override fun make(instantiation: FunctionInstantiation) = PairAPLFunctionImpl(instantiation)
}
