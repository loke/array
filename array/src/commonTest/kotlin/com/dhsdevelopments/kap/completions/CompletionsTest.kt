package com.dhsdevelopments.kap.completions

import com.dhsdevelopments.kap.APLTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class CompletionsTest : APLTest() {
    @Test
    fun wordAtIndex() {
        val engine = makeEngine()
        assertEquals("foo", completionWordAtIndex(engine, "foo", 3))
        assertEquals("foo", completionWordAtIndex(engine, "footest", 3))
        assertEquals("", completionWordAtIndex(engine, "footest", 0))
        assertEquals("", completionWordAtIndex(engine, "", 0))
        assertEquals("foo", completionWordAtIndex(engine, "bar+foo", 7))
        assertEquals("", completionWordAtIndex(engine, "bar+foo", 4))
        assertEquals("", completionWordAtIndex(engine, "bar++foo", 4))
        assertEquals("foo:bar", completionWordAtIndex(engine, "foo:bar", 7))
        assertEquals("foo:bar", completionWordAtIndex(engine, "abc+foo:bar", 11))
        assertEquals(":test", completionWordAtIndex(engine, "x+:test", 7))
        assertEquals(":", completionWordAtIndex(engine, " :", 2))
        assertEquals("", completionWordAtIndex(engine, ": ", 2))
        assertEquals("", completionWordAtIndex(engine, "abc ", 4))
    }

    @Test
    fun wordAtIndexFailsWithNegativeIndex() {
        val engine = makeEngine()
        assertFailsWith<IndexOutOfBoundsException> {
            completionWordAtIndex(engine, "", -1)
        }
    }

    @Test
    fun wordAtIndexFailsWithExcessiveIndex() {
        val engine = makeEngine()
        assertFailsWith<IndexOutOfBoundsException> {
            completionWordAtIndex(engine, "foo", 4)
        }
    }
}
