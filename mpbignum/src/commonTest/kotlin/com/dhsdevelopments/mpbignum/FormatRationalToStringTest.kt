package com.dhsdevelopments.mpbignum

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class FormatRationalToStringTest {
    @Test
    fun formatLargeRational() {
        val r = Rational.make(BigInt.of("123456789012345678901234567890"), BigIntConstants.ONE)
        val (s, isExact) = r.formatToDecimal(8, 8)
        assertFalse(isExact)
        assertEquals("1.2345679e30", s)
    }

    @Test
    fun formatSmallExactRational0() {
        val r = Rational.make(1, 2)
        val (s, isExact) = r.formatToDecimal(8, 8)
        assertTrue(isExact)
        assertEquals("0.5", s)
    }

    @Test
    fun formatSmallExactRational1() {
        val r = Rational.make(3, 2)
        val (s, isExact) = r.formatToDecimal(8, 8)
        assertTrue(isExact)
        assertEquals("1.5", s)
    }

    @Test
    fun formatZero() {
        val r = Rational.make(0, 1)
        val (s, isExact) = r.formatToDecimal(8, 8)
        assertTrue(isExact)
        assertEquals("0", s)
    }

    @Test
    fun formatNonOverflowRationalInteger() {
        val r = Rational.make(BigInt.of("1234567"), BigIntConstants.ONE)
        val (s, isExact) = r.formatToDecimal(7, 8)
        assertTrue(isExact)
        assertEquals("1234567", s)
    }
}
