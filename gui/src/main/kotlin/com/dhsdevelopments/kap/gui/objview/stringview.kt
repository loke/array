package com.dhsdevelopments.kap.gui.objview

import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.KapLogger
import com.dhsdevelopments.kap.isStringValue
import com.dhsdevelopments.kap.toStringValueOrNull

object StringRendererProvider : ObjectRendererHandler {
    override fun canRender(value: APLValue): APLValueRenderer? {
        return if (value.isStringValue()) {
            StringValueRenderer
        } else {
            null
        }
    }
}

private object StringValueRenderer : APLValueRenderer {
    override fun nodeAsString(value: APLValue): String {
        val s = value.toStringValueOrNull()
        return if (s == null) {
            KapLogger.w { "Trying to render non-string object as string: ${value::class.qualifiedName}" }
            ""
        } else {
            s
        }
    }

    override fun preferString(value: APLValue) = true
}
