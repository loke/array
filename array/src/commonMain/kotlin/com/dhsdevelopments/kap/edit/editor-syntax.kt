package com.dhsdevelopments.kap.edit

import com.dhsdevelopments.kap.*

fun parenCandidates(engine: Engine, src: String, index: Int): List<Int> {
    val e = engine.copyToSyntaxChecker()
    try {
        val spaceChars = listOf(' ', '\t')
        var leftIndex = index
        while (leftIndex >= 1 && src[leftIndex - 1] in spaceChars) {
            leftIndex--
        }

        var rightIndex = index
        while (rightIndex < src.length && src[rightIndex] in spaceChars) {
            rightIndex++
        }

        val srcLocation = StringSourceLocation(src)
        val callbacks = ParenCandidateCallbacks(srcLocation, leftIndex, rightIndex)
        try {
            e.parse(srcLocation, callbacks = listOf(callbacks))
        } catch (_: ParseException) {
            // Parse errors are normal, since we may be calling this function on incomplete input
        }
        return callbacks.candidates()
    } finally {
        e.close()
    }
}

private class ParenCandidateCallbacks(val src: SourceLocation, val leftIndex: Int, val rightIndex: Int) : ParserCallbacks {
    private val candidates = ArrayList<Position>()

    fun candidates(): List<Int> {
        fun posToIndex(pos: Position) = lineColToIndex(src.sourceText(), pos.line, pos.col)

        val res = ArrayList<Int>()
        if (candidates.isNotEmpty()) {
            var curr = posToIndex(candidates.first())
            res.add(curr)
            for (i in 1 until candidates.size) {
                val v = posToIndex(candidates[i])
                if (v != curr) {
                    res.add(v)
                    curr = v
                }
            }
        }
        return res
    }

    override fun begin(pos: Position, level: ParserCallbacks.ParserLevel) {
    }

    override fun endExpression(pos: Position, instr: Instruction) {
        if (pos.source == src) {
            val i = lineColToIndex(src.sourceText(), pos.computedEndLine, pos.computedEndCol)
            if (i in leftIndex..rightIndex) {
                candidates.add(pos)
            }
        }
    }

    override fun endFunction(pos: Position, fn: APLFunction) {
    }

    override fun endEmpty(pos: Position) {
    }

    override fun exception(pos: Position, exception: ParseException) {
    }
}

private fun lineColToIndex(src: String, line: Int, col: Int): Int {
    var index = 0
    repeat(line) {
        while (src[index] != '\n') index++
        index++
    }
    return index + col
}
