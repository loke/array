package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.mpbignum.LongExpressionOverflow
import com.dhsdevelopments.mpbignum.compareTo
import com.dhsdevelopments.mpbignum.toBigInt

fun compareAPLArrays(a: APLValue, b: APLValue, pos: Position?, typeDiscrimination: Boolean): Int {
    val aDimensions = a.dimensions
    val bDimensions = b.dimensions

    // Lower rank arrays always compare less than higher rank
    aDimensions.size.compareTo(bDimensions.size).let { result ->
        if (result != 0) {
            return result
        }
    }

    // If both arrays are rank 1, compare lexicographically
    if (aDimensions.size == 1 && bDimensions.size == 1) {
        val aLength = aDimensions[0]
        val bLength = bDimensions[0]
        var i = 0
        while (i < aLength && i < bLength) {
            val aVal = a.valueAt(i)
            val bVal = b.valueAt(i)
            val result = aVal.compareTotalOrdering(bVal, pos, typeDiscrimination)
            if (result != 0) {
                return result
            }
            i++
        }
        return when {
            i < aLength -> 1
            i < bLength -> -1
            else -> 0
        }
    }

    // Both arrays are higher dimension. Do APL-style comparison by checking dimensions first.
    aDimensions.compareTo(bDimensions).let { result ->
        if (result != 0) {
            return result
        }
    }

    // Both arrays have the same dimensions, iterate over all members
    for (i in 0 until a.size) {
        val aVal = a.valueAt(i)
        val bVal = b.valueAt(i)
        aVal.compareTotalOrdering(bVal, pos, typeDiscrimination).let { result ->
            if (result != 0) {
                return result
            }
        }
    }
    return 0
}

abstract class GradeFunction(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
    override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
        val aDimensions = a.dimensions

        // Scalars can't be sorted
        if (aDimensions.size == 0) {
            throwAPLException(InvalidDimensionsException("Scalars cannot be sorted", pos))
        }

        // If the value has a single element along its first axis, return a simple zero
        if (aDimensions[0] in 0..1) {
            return ConstantArray(size1Dimensions(), APLLONG_0)
        }

        val multipliers = aDimensions.multipliers()
        val firstAxisMultiplier = multipliers[0]

        val source = a.collapse()
        val list = IntArray(aDimensions[0]) { it }
        val type = source.specialisedType
        val sorted = when {
            type.isLong -> opLong(list, firstAxisMultiplier, source)
            type.isDouble -> opDouble(list, firstAxisMultiplier, source)
            else -> opGeneric(list, firstAxisMultiplier, source)
        }
        return APLArrayLong(dimensionsOfSize(sorted.size), LongArray(sorted.size) { i -> sorted[i].toLong() })
    }

    @Suppress("NOTHING_TO_INLINE")
    private inline fun lookupAndCompareLong(source: APLValue, ap: Int, bp: Int): Int {
        val objA = try {
            source.valueAtLong(ap)
        } catch (e: LongExpressionOverflow) {
            val objB = source.valueAt(bp)
            return e.result.makeAPLNumber().numericCompare(objB, pos = pos, typeDiscrimination = true)
        }
        val objB = try {
            source.valueAtLong(bp)
        } catch (e: LongExpressionOverflow) {
            return objA.toBigInt().compareTo(e.result)
        }
        return objA.compareTo(objB)
    }

    private fun opLong(list: IntArray, firstAxisMultiplier: Int, source: APLValue): List<Int> {
        return list.sortedWith { aIndex, bIndex ->
            var ap = aIndex * firstAxisMultiplier
            var bp = bIndex * firstAxisMultiplier
            var res = 0
            for (i in 0 until firstAxisMultiplier) {
                val result = lookupAndCompareLong(source, ap, bp)
                if (result != 0) {
                    res = result
                    break
                }
                ap++
                bp++
            }
            applyReverse(res)
        }
    }

    private fun opDouble(list: IntArray, firstAxisMultiplier: Int, source: APLValue): List<Int> {
        return list.sortedWith { aIndex, bIndex ->
            var ap = aIndex * firstAxisMultiplier
            var bp = bIndex * firstAxisMultiplier
            var res = 0
            for (i in 0 until firstAxisMultiplier) {
                val objA = source.valueAtDouble(ap)
                val objB = source.valueAtDouble(bp)
                val result = objA.compareTo(objB)
                if (result != 0) {
                    res = result
                    break
                }
                ap++
                bp++
            }
            applyReverse(res)
        }
    }

    private fun opGeneric(list: IntArray, firstAxisMultiplier: Int, source: APLValue): List<Int> {
        return list.sortedWith { aIndex, bIndex ->
            var ap = aIndex * firstAxisMultiplier
            var bp = bIndex * firstAxisMultiplier
            var res = 0
            for (i in 0 until firstAxisMultiplier) {
                val objA = source.valueAt(ap)
                val objB = source.valueAt(bp)
                val result = objA.compareTotalOrdering(objB, pos)
                if (result != 0) {
                    res = result
                    break
                }
                ap++
                bp++
            }
            applyReverse(res)
        }
    }

    abstract fun applyReverse(result: Int): Int
}

class GradeUpFunction : APLFunctionDescriptor {
    class GradeUpFunctionImpl(pos: FunctionInstantiation) : GradeFunction(pos) {
        override fun applyReverse(result: Int) = result
        override val name1Arg get() = "grade up"
    }

    override fun make(instantiation: FunctionInstantiation) = GradeUpFunctionImpl(instantiation)
}

class GradeDownFunction : APLFunctionDescriptor {
    class GradeDownFunctionImpl(pos: FunctionInstantiation) : GradeFunction(pos) {
        override fun applyReverse(result: Int) = -result
        override val name1Arg get() = "grade down"
    }

    override fun make(instantiation: FunctionInstantiation) = GradeDownFunctionImpl(instantiation)
}

fun sortKapArray(a: APLValue, axis: APLValue?, reverse: Boolean, pos: Position): APLValue {
    val a0 = a.collapse()
    val d = a0.dimensions
    val axisInt = if (axis == null) 0 else axis.ensureNumber(pos).asInt(pos)
    ensureValidAxis(axisInt, d, pos)

    val multipliers = d.multipliers()
    val valueSize = multipliers[axisInt]
    val indexes = IntArray(a0.size / valueSize) { i -> i }

    val reverseMultiplier = if (reverse) -1 else 1
    val comparator = object : Comparator<Int> {
        override fun compare(a: Int, b: Int): Int {
            repeat(valueSize) { innerIndex ->
                val o0 = a0.valueAt((a * valueSize) + innerIndex)
                val o1 = a0.valueAt((b * valueSize) + innerIndex)
                val res = o0.compareTotalOrdering(o1, pos)
                if (res != 0) {
                    return res * reverseMultiplier
                }
            }
            return 0
        }
    }

    val result = indexes.sortedWith(comparator)
    return SortedAPLArray(d, a0, result, valueSize, axisInt)
}

private class SortedAPLArray(override val dimensions: Dimensions, val a: APLValue, val mapping: List<Int>, val valueSize: Int, axis: Int) : APLArray() {
    override val specialisedType get() = a.specialisedType
    override val metadata by lazy { maybeDefaultMetadata(a.metadata) { m -> SortedAPLArrayMetadata(m, mapping, axis) } }

    private inline fun valueAtInternal(p: Int, fn: (v: APLValue, position: Int) -> Nothing): Nothing {
        val high = p / valueSize
        val low = p % valueSize
        val remapPos = mapping[high]
        fn(a, (remapPos * valueSize) + low)
    }

    override fun valueAt(p: Int): APLValue {
        valueAtInternal(p) { v, position ->
            return v.valueAt(position)
        }
    }

    override fun valueAtLong(p: Int): Long {
        valueAtInternal(p) { v, position ->
            return v.valueAtLong(position)
        }
    }

    override fun valueAtDouble(p: Int): Double {
        valueAtInternal(p) { v, position ->
            return v.valueAtDouble(position)
        }
    }

    private class SortedAPLArrayMetadata(sourceMetadata: APLValueMetadata, mapping: List<Int>, axis: Int) : DerivedAPLValueMetadata(sourceMetadata) {
        override val labels by lazy {
            val sourceLabels = sourceMetadata.labels
            if (sourceLabels == null) {
                null
            } else {
                val l = sourceLabels.labels.mapIndexed { i, axisLabels ->
                    when {
                        axisLabels == null -> null
                        i < axis -> null
                        i == axis -> if (i != 0) {
                            null
                        } else {
                            (0 until axisLabels.size).map { ii -> axisLabels[mapping[ii]] }
                        }
                        else -> axisLabels
                    }
                }
                DimensionLabels.make(l)
            }
        }
    }
}
