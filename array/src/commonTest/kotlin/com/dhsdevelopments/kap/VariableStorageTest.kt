package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull
import kotlin.test.assertSame
import kotlin.test.assertTrue

class VariableStorageTest : APLTest() {
    @Test
    fun lookupUnassignedVariable() {
        val engine = makeEngine(withStandardLib = true)
        assertFailsWith<ParseException> {
            engine.parseAndEval(StringSourceLocation("(foo←-) ⊣ (bar←123)"))
        }
        // At this point, foo should be bound, but bar is not, since the first expression caused a parse error.
        // Even though foo is bound, there should have been no stack space allocated for it yet, since no
        // code was evaluated.
        val fooSym = engine.internSymbol("foo", engine.currentNamespace)
        val fooBinding = engine.rootEnvironment.bindings.find { b -> b.name == fooSym }
        assertNotNull(fooBinding)
        // The index of the binding exceeds the stack storage by 1
        assertTrue(fooBinding.storage.index == engine.rootStackFrame.storageList.size)

        // We now evaluate a different expression which creates another binding.
        // After evaluating this, storage will have been allocated for both the
        // binding of 'foo' as well as 'a'.
        val result = engine.parseAndEval(StringSourceLocation("a←2"))
        assertSimpleNumber(2, result)
        val newFooBinding = engine.rootEnvironment.bindings.find { b -> b.name == fooSym }
        // Ensure that the 'foo' binding didn't change
        assertSame(newFooBinding, fooBinding)
        val aSym = engine.internSymbol("a", engine.currentNamespace)
        val aBinding = engine.rootEnvironment.bindings.find { b -> b.name == aSym }
        assertNotNull(aBinding)
        assertEquals(fooBinding.storage.index + 1, aBinding.storage.index)
        // There is now stack storage allocated for both 'foo' and 'a'
        assertTrue(aBinding.storage.index == engine.rootStackFrame.storageList.size - 1)
    }
}
