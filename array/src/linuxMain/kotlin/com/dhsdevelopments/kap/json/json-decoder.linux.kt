package com.dhsdevelopments.kap.json

import jansson.*
import kotlinx.cinterop.*

@OptIn(ExperimentalForeignApi::class)
class LinuxJsonDecoder : JsonDecoder {
    override fun decodeJsonFromString(source: String): JsonElement {
        memScoped {
            val error = alloc<json_error_t>()
            val root = json_loads(source, JSON_DECODE_ANY.toULong(), error.ptr) ?: throw JsonParseException("Error parsing JSON: ${error.text.toKString()}")
            try {
                return parseElement(root)
            } finally {
                json_decref(root)
            }
        }
    }

    private fun parseElement(value: CPointer<json_t>): JsonElement {
        @Suppress("REDUNDANT_ELSE_IN_WHEN")
        return when (val type = jsonTypeof(value)) {
            json_type.JSON_ARRAY -> parseArray(value)
            json_type.JSON_OBJECT -> parseObject(value)
            json_type.JSON_REAL -> JsonNumber(json_real_value(value))
            json_type.JSON_INTEGER -> JsonNumber(json_integer_value(value))
            json_type.JSON_STRING -> {
                val s = json_string_value(value) ?: throw JsonParseException("Null string value")
                JsonString(s.toKString())
            }
            json_type.JSON_TRUE -> JsonBoolean.JSON_TRUE
            json_type.JSON_FALSE -> JsonBoolean.JSON_FALSE
            json_type.JSON_NULL -> JsonNull
            else -> throw JsonParseException("Unexpected json type: ${type}")
        }
    }

    private fun parseObject(p: CPointer<json_t>): JsonObject {
        val result = ArrayList<Pair<String, JsonElement>>()
        var iterator = json_object_iter(p)
        while (iterator != null) {
            val key = json_object_iter_key(iterator) ?: throw JsonParseException("Null key from object iterator")
            val value = json_object_iter_value(iterator) ?: throw JsonParseException("Null value from object iterator")
            result.add(Pair(key.toKString(), parseElement(value)))
            iterator = json_object_iter_next(p, iterator)
        }
        return JsonObject(result)
    }

    private fun parseArray(p: CPointer<json_t>): JsonArray {
        val result = ArrayList<JsonElement>()
        val size = json_array_size(p)
        if (size > Int.MAX_VALUE.toUInt()) {
            throw JsonParseException("Array too large: ${size}")
        }
        for (i in 0 until size.toInt()) {
            val v = json_array_get(p, i.toULong()) ?: throw JsonParseException("Null value from array at index: ${i}")
            result.add(parseElement(v))
        }
        return JsonArray(result)
    }
}

actual fun makeJsonDecoder(): JsonDecoder {
    return LinuxJsonDecoder()
}
