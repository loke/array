package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals

class RationalRenderTest : APLTest() {
    @Test
    fun renderDefaultRational() {
        val expected =
            """
                ┌→──────┐
                │1/2 1/3│
                └───────┘
            """.trimIndent()
        assertRenderOutput(expected, "1r2 1r3")
    }

    @Test
    fun renderRationalAsDecimal() {
        val engine = makeEngine(withStandardLib = true)
        engine.commandManager.processCommandString("ratmode decimal")
        val expected =
            """
                ┌→────────────────────────┐
                │0.5r ≈0.3333333333333333r│
                └─────────────────────────┘
            """.trimIndent()
        val output = StringBuilderOutput()
        engine.standardOutput = output
        engine.parseAndEval(StringSourceLocation("io:print o3:format 1r2 1r3"))
        assertEquals("${expected}\n", output.buf.toString())
    }
}
