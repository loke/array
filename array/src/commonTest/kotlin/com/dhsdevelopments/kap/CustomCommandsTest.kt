package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals

class CustomCommandsTest : APLTest() {
    @Test
    fun simpleCustomCommand() {
        val engine = makeEngine()
        val output = StringBuilderOutput()
        engine.standardOutput = output
        val result = engine.parseAndEval(StringSourceLocation("int:registerCmd (\"foo\";\"Test command\";λ{io:println \"abc\"})"))
        assertAPLNull(result)
        engine.commandManager.processCommandString("foo")
        assertEquals("abc\n", output.buf.toString())
    }

    @Test
    fun simpleCustomCommandWithArgs() {
        val engine = makeEngine()
        val output = StringBuilderOutput()
        engine.standardOutput = output
        val result = engine.parseAndEval(StringSourceLocation("int:registerCmd (\"foo\";\"Test command\";λ{io:println \"abc\",⍵})"))
        assertAPLNull(result)
        engine.commandManager.processCommandString("foo qwert")
        assertEquals("abcqwert\n", output.buf.toString())
    }

    @Test
    fun simpleCustomCommandWithArgsExtraWhitespace() {
        val engine = makeEngine()
        val output = StringBuilderOutput()
        engine.standardOutput = output
        val result = engine.parseAndEval(StringSourceLocation("int:registerCmd (\"foo\";\"Test command\";λ{io:println \"abc\",⍵})"))
        assertAPLNull(result)
        engine.commandManager.processCommandString("foo     qwert  ")
        assertEquals("abcqwert\n", output.buf.toString())
    }
}
