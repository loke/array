package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertFailsWith

class FactorTest : APLTest() {
    @Test
    fun simpleFactor() {
        parseAPLExpression("math:factor 4").let { result ->
            assert1DArray(arrayOf(2, 2), result)
        }
    }

    @Test
    fun factorPrime() {
        parseAPLExpression("math:factor 7").let { result ->
            assert1DArray(arrayOf(7), result)
        }
    }

    @Test
    fun factorMultiple() {
        parseAPLExpression("math:factor 0 1 2 3 4 5 6 7 8").let { result ->
            assert1DArray(
                arrayOf(
                    Inner1D(arrayOf()),
                    Inner1D(arrayOf(1)),
                    Inner1D(arrayOf(2)),
                    Inner1D(arrayOf(3)),
                    Inner1D(arrayOf(2, 2)),
                    Inner1D(arrayOf(5)),
                    Inner1D(arrayOf(2, 3)),
                    Inner1D(arrayOf(7)),
                    Inner1D(arrayOf(2, 2, 2))),
                result)
        }
    }

    @Test
    fun factorLargeInteger() {
        parseAPLExpression("math:factor 1234567700").let { result ->
            assert1DArray(arrayOf(2, 2, 5, 5, 29, 425713), result)
        }
    }

    @Test
    fun factorBigint() {
        parseAPLExpression("math:factor 312430759692903949351680000").let { result ->
            assert1DArray(arrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 5, 5, 5, 5, 223, 617, 823, 16883, 1576229), result)
        }
    }

    @Test
    fun factorDoubleShouldFail() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpression("math:factor 1.1")
        }
    }

    @Test
    fun factorComplexShouldFail() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpression("math:factor 1j3")
        }
    }

    @Test
    fun factorRationalShouldFail() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpression("math:factor 1r2")
        }
    }

    @Test
    fun factorIntegerRational() {
        parseAPLExpression("math:factor int:asRational 10").let { result ->
            assert1DArray(arrayOf(2, 5), result)
        }
    }
}
