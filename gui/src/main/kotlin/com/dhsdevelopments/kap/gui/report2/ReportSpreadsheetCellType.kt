package com.dhsdevelopments.kap.gui.report2

import org.controlsfx.control.spreadsheet.*

abstract class ReportSpreadsheetCellType<T : ReportCellContent> : SpreadsheetCellType<T>() {
    override fun createEditor(spreadsheetView: SpreadsheetView): SpreadsheetCellEditor? {
        return null
    }

    override fun toString(obj: T): String {
        return obj.cellContentString()
    }

    override fun match(value: Any, vararg options: Any): Boolean {
        return false
    }

    override fun convertValue(value: Any?): T? {
        TODO("string conversion not implemented")
    }

    companion object {
        fun <T : ReportCellContent> createCellWithType(content: T, row: Int, col: Int, type: ReportSpreadsheetCellType<in T>): SpreadsheetCell {
            return SpreadsheetCellBase(row, col, 1, 1, type).apply {
                item = content
                type.initCell(this)
            }
        }
    }

    open fun initCell(spreadsheetCellBase: SpreadsheetCell) {}
}

abstract class LinkedVariableCellType<T : LinkedVariableCellContent> : ReportSpreadsheetCellType<T>()

object HeaderCellType : LinkedVariableCellType<HeaderCellContent>() {
    fun createCell(content: HeaderCellContent, row: Int, col: Int): SpreadsheetCell {
        return createCellWithType(content, row, col, HeaderCellType)
    }

    override fun initCell(spreadsheetCellBase: SpreadsheetCell) {
        spreadsheetCellBase.styleClass.add("report-header")
    }
}

object FieldCellType : LinkedVariableCellType<FieldCellContent>() {
    fun createCell(content: FieldCellContent, row: Int, col: Int): SpreadsheetCell {
        return createCellWithType(content, row, col, FieldCellType)
    }

    override fun initCell(spreadsheetCellBase: SpreadsheetCell) {
        spreadsheetCellBase.styleClass.add("report-data")
    }
}

interface StyleableCellType {
    fun invertStyle(cell: SpreadsheetCell, style: LabelTextStyle)

    enum class LabelTextStyle(val styleClassName: String) {
        BOLD("report-bold"),
        ITALICS("report-italics")
    }
}

interface CellSpannable

object LabelCellType : ReportSpreadsheetCellType<LabelCellContent>(), StyleableCellType {
    fun createCell(content: LabelCellContent, row: Int, col: Int): SpreadsheetCell {
        return createCellWithType(content, row, col, LabelCellType)
    }

    override fun createEditor(spreadsheetView: SpreadsheetView): SpreadsheetCellEditor {
        return ReportCellEditor(spreadsheetView)
    }

    override fun match(value: Any, vararg options: Any): Boolean {
        return value is String
    }

    override fun convertValue(value: Any?): LabelCellContent {
        return when (value) {
            null -> LabelCellContent("")
            is LabelCellContent -> value
            is String -> LabelCellContent(value)
            else -> throw IllegalStateException("Unexpected type: ${value}")
        }
    }

    override fun initCell(spreadsheetCellBase: SpreadsheetCell) {
        spreadsheetCellBase.styleClass.add("report-text")
    }

    override fun invertStyle(cell: SpreadsheetCell, style: StyleableCellType.LabelTextStyle) {
        if (cell.styleClass.contains(style.styleClassName)) {
            cell.styleClass.remove(style.styleClassName)
        } else {
            cell.styleClass.add(style.styleClassName)
        }
    }
}
