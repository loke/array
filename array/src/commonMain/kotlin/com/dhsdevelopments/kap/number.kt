package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.APLDouble.Companion.compareDoubleToLong
import com.dhsdevelopments.kap.complex.Complex
import com.dhsdevelopments.kap.complex.toComplex
import com.dhsdevelopments.mpbignum.*

abstract class APLNumber : APLSingleValue() {
    override fun toString() = "APLNumber(${formatted(FormatStyle.PRETTY)})"
    override fun formattedAsCodeRequiresParens() = false
    override fun ensureNumberOrNull() = this

    abstract fun asDouble(pos: Position? = null): Double
    abstract fun asLong(pos: Position? = null): Long
    abstract fun asComplex(): Complex

    abstract fun isComplex(): Boolean

    open fun asBinary(pos: Position? = null): Int {
        return when (val l = asLong(pos)) {
            0L -> 0
            1L -> 1
            else -> throwAPLException(KapOverflowException("Value must be 0 or 1: ${l}"))
        }
    }

    open fun asInt(pos: Position? = null): Int {
        val l = asLong(pos)
        return if (l >= Int.MIN_VALUE && l <= Int.MAX_VALUE) {
            l.toInt()
        } else {
            throwAPLException(IntMagnitudeException(l, pos))
        }
    }

    override fun asBoolean(pos: Position?) = asLong(pos) != 0L

    open fun asBigInt(pos: Position? = null): BigInt =
        throwAPLException(APLIllegalArgumentException("Value cannot be converted to bigint: ${formatted(FormatStyle.PLAIN)}", pos))

    open fun asRational(pos: Position? = null): Rational =
        throwAPLException(APLIllegalArgumentException("Value cannot be converted to rational: ${formatted(FormatStyle.PLAIN)}", pos))

    override fun asHtml(buf: Appendable) {
        buf.append(formatted(FormatStyle.PLAIN))
    }

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Boolean {
        val ref0 = reference.unwrapDeferredValue()
        return when {
            ref0 !is APLSingleValue -> false
            !typeDiscrimination -> numericCompareEquals(ref0, pos)
            else -> compareTotalOrdering(reference, pos, typeDiscrimination) == 0
        }
    }

    abstract fun isZero(): Boolean
}

interface APLInteger {
    fun asBigInt(pos: Position?): BigInt
}

class APLLong(val value: Long) : APLNumber(), APLInteger {
    override val kapClass get() = SystemClass.INTEGER
    override val specialisedTypeAsMember get() = if (value == 0L || value == 1L) ArrayMemberType.BOOLEAN else ArrayMemberType.LONG
    override fun asDouble(pos: Position?) = value.toDouble()
    override fun asLong(pos: Position?) = value
    override fun asComplex() = Complex(value.toDouble())
    override fun isComplex() = false
    override fun asBoolean(pos: Position?) = value != 0L
    override fun asBigInt(pos: Position?) = BigInt.of(value)
    override fun asRational(pos: Position?) = Rational.make(value.toBigInt(), BigIntConstants.ONE)
    override fun isZero() = value == 0L

    override fun formatted(style: FormatStyle) = when (style) {
        FormatStyle.PLAIN -> value.toString()
        FormatStyle.PRETTY -> value.toString()
        FormatStyle.READABLE -> if (value < 0) "¯" + (-value).toString() else value.toString()
    }

    override fun numericCompare(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Int {
        return when (val v = reference.unwrapDeferredValue()) {
            is APLLong -> value.compareTo(v.value)
            is APLDouble -> -compareDoubleToLong(v.value, value, pos = pos, typeDiscrimination = typeDiscrimination)
            is APLComplex -> v.value.let { v0 ->
                if (v0.im == 0.0) {
                    -compareDoubleToLong(v0.re, value, pos, typeDiscrimination = typeDiscrimination)
                } else {
                    throwAPLException(APLArgumentComplexOrderingException(pos))
                }
            }
            is APLBigInt -> value.toBigInt().compareTo(v.value)
            is APLRational -> value.toRational().compareTo(v.value)
            else -> super.numericCompare(v, pos, typeDiscrimination)
        }
    }

    override val numericCompareValid get() = true

    override fun compareSameType(reference: APLValue, pos: Position?): Int {
        return value.compareTo((reference as APLLong).value)
    }

    override fun toString() = "APLLong(${formatted(FormatStyle.PRETTY)})"

    override fun typeQualifiedHashCode() = value.hashCode()

    override fun nonTypeQualifiedHashCode(): Int {
        val v = this@APLLong.value
        val vDouble = v.toDouble()
        if (v in MIN_EXACT_INT_DOUBLE..MAX_EXACT_INT_DOUBLE) {
            return vDouble.hashCode()
        }
        // Now we need to check if the long can be exactly converted to a double.
        // The easiest way to do this is to convert back to a long to see if we
        // get the same value.
        if (vDouble.toLong() == v) {
            return vDouble.hashCode()
        }
        // This value cannot be exactly converted to a double, just use the
        // standard long hashcode function.
        return v.hashCode()
    }

    companion object {
        const val MIN_EXACT_INT_DOUBLE = -0x20000000000000L
        const val MAX_EXACT_INT_DOUBLE = 0x20000000000000L
    }
}

class APLDouble(val value: Double) : APLNumber() {
    override val kapClass get() = SystemClass.FLOAT
    override val specialisedTypeAsMember get() = ArrayMemberType.DOUBLE
    override fun asDouble(pos: Position?) = value
    override fun asLong(pos: Position?) = value.toLong()
    override fun asComplex() = Complex(value)
    override fun isComplex() = false
    override fun isZero() = value == 0.0

    override fun formatted(style: FormatStyle) = when (style) {
        FormatStyle.PLAIN -> value.formatDouble()
        FormatStyle.PRETTY -> value.formatDouble()
        FormatStyle.READABLE -> formatDoubleToReadable(value)
    }

    override fun numericCompare(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Int {
        val v = reference.unwrapDeferredValue()
        return compareDoubleToSingleValue(value, v, pos, typeDiscrimination) {
            super.numericCompare(reference, pos, typeDiscrimination)
        }
    }

    override val numericCompareValid get() = !value.isNaN()

    override fun compareSameType(reference: APLValue, pos: Position?): Int {
        return compareDoublesNaNAware(value, (reference as APLDouble).value)
    }

    override fun toString() = "APLDouble(${formatted(FormatStyle.PRETTY)})"
    override fun asBoolean(pos: Position?) = value != 0.0

    override fun typeQualifiedHashCode(): Int = value.hashCode()

    companion object {
        inline fun compareDoubleToSingleValue(a: Double, b: APLValue, pos: Position?, typeDiscrimination: Boolean, otherFn: () -> Int): Int {
            return when (b) {
                is APLLong -> compareDoubleToLong(a, b.value, pos, typeDiscrimination)
                is APLDouble -> b.value.let { bDouble ->
                    when {
                        typeDiscrimination -> a.compareTo(bDouble)
                        a < bDouble -> -1
                        a > bDouble -> 1
                        else -> 0
                    }
                }
                is APLComplex -> b.value.let { v0 ->
                    if (v0.im == 0.0) {
                        val realPart = v0.re
                        when {
                            typeDiscrimination -> a.compareTo(realPart)
                            a < realPart -> -1
                            a > realPart -> 1
                            else -> 0
                        }
                    } else {
                        throwAPLException(APLArgumentComplexOrderingException(pos))
                    }
                }
                is APLBigInt -> compareDoubleToBigint(a, b.value, pos, typeDiscrimination)
                is APLRational -> compareDoubleToRational(a, b.value, pos, typeDiscrimination)
                else -> otherFn()
            }
        }

        @Suppress("NOTHING_TO_INLINE")
        inline fun compareDoubleToLong(a: Double, b: Long, pos: Position?, typeDiscrimination: Boolean): Int {
            val res = when {
                a.isFinite() -> {
                    when {
                        typeDiscrimination && a.equals(-0.0) && b == 0L -> -1
                        !a.isInteger() -> a.rationalise().compareTo(b.toRational())
                        a >= Long.MIN_VALUE && a <= Long.MAX_VALUE -> a.toLong().compareTo(b)
                        else -> BigInt.of(a).compareTo(b.toBigInt())
                    }
                }
                a.isInfinite() -> if (a > 0) 1 else -1
                else -> throwAPLException(APLArgumentIsNotANumberException(pos))
            }
            return if (typeDiscrimination && res == 0) -1 else res
        }

        @Suppress("NOTHING_TO_INLINE")
        inline fun compareDoubleToBigint(a: Double, b: BigInt, pos: Position?, typeDiscrimination: Boolean): Int {
            val res = when {
                a.isFinite() -> {
                    when {
                        typeDiscrimination && a.equals(-0.0) && b.signum() == 0 -> -1
                        a.isInteger() -> BigInt.of(a).compareTo(b)
                        else -> a.rationalise().compareTo(b.toRational())
                    }
                }
                a.isInfinite() -> if (a > 0) 1 else -1
                else -> throwAPLException(APLArgumentIsNotANumberException(pos))
            }
            return if (typeDiscrimination && res == 0) -1 else res
        }

        @Suppress("NOTHING_TO_INLINE")
        inline fun compareDoubleToRational(a: Double, b: Rational, pos: Position?, typeDiscrimination: Boolean): Int {
            val res = when {
                a.isFinite() -> when {
                    typeDiscrimination && a.equals(-0.0) && b.signum() == 0 -> -1
                    else -> a.rationalise().compareTo(b)
                }
                a.isInfinite() -> if (a > 0) 1 else -1
                else -> throwAPLException(APLArgumentIsNotANumberException(pos))
            }
            return if (typeDiscrimination && res == 0) -1 else res
        }

        fun compareDoublesNaNAware(a: Double, b: Double): Int {
            return when {
                a.isNaN() -> if (b.isNaN()) 0 else 1
                b.isNaN() -> -1
                else -> a.compareTo(b)
            }
        }

        fun formatDoubleToReadable(value: Double) = if (value < 0 || value.equals(-0.0)) "¯" + (-value).formatDouble() else value.formatDouble()
    }
}

fun checkBigIntInRangeLong(value: BigInt, pos: Position?) {
    if (!value.rangeInLong()) {
        throwAPLException(LongMagnitudeException(value, pos))
    }
}

interface APLWideNumber

private fun bigIntToStringReadable(value: BigInt) = if (value < 0) "¯${-value}" else value.toString()

class APLBigInt(val value: BigInt) : APLNumber(), APLWideNumber, APLInteger {
    override val kapClass get() = SystemClass.INTEGER
    override val specialisedTypeAsMember get() = ArrayMemberType.LONG

    override fun asDouble(pos: Position?) = value.toDouble()

    override fun asLong(pos: Position?): Long {
        checkBigIntInRangeLong(value, pos)
        return value.toLong()
    }

    override fun asComplex() = value.toDouble().toComplex()
    override fun isComplex() = false
    override fun isZero() = value == BigIntConstants.ZERO
    override fun asBigInt(pos: Position?) = value
    override fun asRational(pos: Position?) = Rational.make(value, BigIntConstants.ONE)
    override fun asBoolean(pos: Position?) = value.signum() != 0

    override fun formatted(style: FormatStyle) = when (style) {
        FormatStyle.PLAIN -> value.toString()
        FormatStyle.PRETTY -> value.toString()
        FormatStyle.READABLE -> bigIntToStringReadable(value)
    }

    override fun numericCompare(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Int {
        return when (val v = reference.unwrapDeferredValue()) {
            is APLLong -> value.compareTo(v.value.toBigInt())
            is APLDouble -> -APLDouble.compareDoubleToBigint(v.value, value, pos = pos, typeDiscrimination = typeDiscrimination)
            is APLComplex -> v.value.let { v0 ->
                if (v0.im == 0.0) {
                    -APLDouble.compareDoubleToBigint(v0.re, value, pos = pos, typeDiscrimination = typeDiscrimination)
                } else {
                    throwAPLException(APLArgumentComplexOrderingException(pos))
                }
            }
            is APLBigInt -> value.compareTo(v.value)
            is APLRational -> Rational.make(value, BigIntConstants.ONE).compareTo(v.value)
            else -> super.numericCompare(v, pos, typeDiscrimination)
        }
    }

    override val numericCompareValid get() = true

    override fun compareSameType(reference: APLValue, pos: Position?): Int {
        return value.compareTo((reference as APLBigInt).value)
    }

    override fun toString() = "APLBigInt(${formatted(FormatStyle.PRETTY)})"

    override fun typeQualifiedHashCode(): Int {
        return if (value.rangeInLong()) {
            value.toLong().hashCode()
        } else {
            value.hashCode()
        }
    }

    override fun nonTypeQualifiedHashCode(): Int {
        val v = this@APLBigInt.value
        val vDouble = v.toDouble()
        if (v >= APLLong.MIN_EXACT_INT_DOUBLE && v <= APLLong.MAX_EXACT_INT_DOUBLE) {
            return vDouble.hashCode()
        }
        if (BigInt.of(vDouble) == v) {
            return vDouble.hashCode()
        }
        return typeQualifiedHashCode()
    }
}

class APLRational(val value: Rational) : APLNumber(), APLWideNumber {
    override fun asDouble(pos: Position?) = value.toDouble()

    override fun asLong(pos: Position?): Long {
        val n = value.numerator / value.denominator
        if (n.rangeInLong()) {
            return n.toLong()
        }
        throwAPLException(APLEvalException("Value does not fit in a long: ${value}", pos))
    }

    override fun asComplex() = value.toDouble().toComplex()
    override fun isComplex() = false
    override fun isZero() = value == Rational.ZERO
    override fun asRational(pos: Position?) = value
    override fun asBoolean(pos: Position?) = value.signum() != 0

    override val kapClass get() = SystemClass.RATIONAL

    override fun formatted(style: FormatStyle) = when (style) {
        FormatStyle.PLAIN -> "${value.numerator}/${value.denominator}"
        FormatStyle.PRETTY -> "${value.numerator}/${value.denominator}"
        FormatStyle.READABLE -> "${bigIntToStringReadable(value.numerator)}r${bigIntToStringReadable(value.denominator)}"
    }

    override fun numericCompare(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) = when (val v = reference.unwrapDeferredValue()) {
        is APLLong -> value.compareTo(Rational.make(v.value.toBigInt(), BigIntConstants.ONE))
        is APLDouble -> -APLDouble.compareDoubleToRational(v.value, value, pos, typeDiscrimination)
        is APLComplex -> v.value.let { v0 ->
            if (v0.im == 0.0) {
                -APLDouble.compareDoubleToRational(v0.re, value, pos, typeDiscrimination)
            } else {
                throwAPLException(APLArgumentComplexOrderingException(pos))
            }
        }
        is APLBigInt -> value.compareTo(Rational.make(v.value, BigIntConstants.ONE))
        is APLRational -> value.compareTo(v.value)
        else -> super.numericCompare(v, pos, typeDiscrimination)
    }

    override val numericCompareValid get() = true

    override fun compareSameType(reference: APLValue, pos: Position?): Int {
        return value.compareTo((reference as APLRational).value)
    }

    override fun toString() = "APLRational(${formatted(FormatStyle.PRETTY)})"

    override fun typeQualifiedHashCode(): Int {
        return if (value.rangeFitsInLong()) {
            value.toLongTruncated().hashCode()
        } else {
            value.hashCode()
        }
    }

    override fun nonTypeQualifiedHashCode(): Int {
        val v = this@APLRational.value
        val vDouble = v.toDouble()
        if (vDouble.rationalise() == v) {
            return vDouble.hashCode()
        }
        return typeQualifiedHashCode()
    }
}

class NumberComplexException(value: Complex, pos: Position? = null) : IncompatibleTypeException("Number is complex: ${value}", pos)

class APLComplex(val value: Complex) : APLNumber() {
    override val kapClass get() = SystemClass.COMPLEX

    override fun asDouble(pos: Position?): Double {
        if (value.im != 0.0) {
            throwAPLException(NumberComplexException(value, pos))
        }
        return value.re
    }

    override fun asLong(pos: Position?): Long {
        if (value.im != 0.0) {
            throwAPLException(NumberComplexException(value, pos))
        }
        return value.re.toLong()
    }

    override fun asComplex() = value
    override fun isComplex() = value.im != 0.0
    override fun isZero() = value == Complex.ZERO

    override fun formatted(style: FormatStyle) =
        when (style) {
            FormatStyle.PLAIN -> formatToAPL()
            FormatStyle.PRETTY -> formatToAPL()
            FormatStyle.READABLE -> formatToReadable()
        }

    private fun formatToAPL() = "${value.re.formatDouble()}J${value.im.formatDouble()}"
    private fun formatToReadable() = "${APLDouble.formatDoubleToReadable(value.re)}J${APLDouble.formatDoubleToReadable(value.im)}"

    override fun asBoolean(pos: Position?) = value != Complex.ZERO

    override fun numericCompareEquals(reference: APLSingleValue, pos: Position?): Boolean =
        compareEqualsComplexToSingleValue(this, reference, pos)

    override fun numericCompare(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Int {
        if (value.im != 0.0) {
            throwAPLException(APLArgumentComplexOrderingException(pos))
        }
        val v = reference.unwrapDeferredValue()
        return APLDouble.compareDoubleToSingleValue(value.re, v, pos, typeDiscrimination) {
            super.numericCompare(reference, pos, typeDiscrimination)
        }
    }

    override val numericCompareValid get() = value.im == 0.0 && !value.re.isNaN()

    override fun compareSameType(reference: APLValue, pos: Position?): Int {
        val other = (reference as APLComplex).value
        val imResult = APLDouble.compareDoublesNaNAware(value.im, other.im)
        return if (imResult == 0) {
            APLDouble.compareDoublesNaNAware(value.re, other.re)
        } else {
            imResult
        }
    }

    override fun typeQualifiedHashCode(): Int {
        return if (value.im == 0.0) {
            value.re.hashCode()
        } else {
            value.hashCode()
        }
    }

    override fun nonTypeQualifiedHashCode() = typeQualifiedHashCode()

    companion object {
        @Suppress("NOTHING_TO_INLINE")
        inline fun compareEqualsComplexToSingleValue(a: APLComplex, b: APLSingleValue, pos: Position?): Boolean {
            return if (a.value.im != 0.0) {
                b is APLComplex && a.value == b.value
            } else {
                a.numericCompare(b, pos = pos, typeDiscrimination = false) == 0
            }
        }
    }
}

val APLLONG_0 = APLLong(0)
val APLLONG_1 = APLLong(1)
val APLDOUBLE_0 = APLDouble(0.0)
val APLDOUBLE_1 = APLDouble(1.0)

private const val NUMBER_CACHE_SIZE = 1024

private val longCache = Array(NUMBER_CACHE_SIZE) { i -> APLLong(i - (NUMBER_CACHE_SIZE / 2L)) }

fun Int.makeAPLNumber(): APLLong = this.toLong().makeAPLNumber()

fun Long.makeAPLNumber(): APLLong {
    return if (this >= -(NUMBER_CACHE_SIZE / 2) && this <= NUMBER_CACHE_SIZE / 2 - 1) {
        longCache[this.toInt() + NUMBER_CACHE_SIZE / 2]
    } else {
        APLLong(this)
    }
}

fun Double.makeAPLNumber(): APLDouble = APLDouble(this)
fun Complex.makeAPLNumber(): APLNumber = if (im == 0.0) APLDouble(re) else APLComplex(this)
fun BigInt.makeAPLNumber(): APLBigInt = APLBigInt(this)
fun BigInt.makeAPLNumberWithReduction(): APLNumber = if (this.rangeInLong()) this.toLong().makeAPLNumber() else APLBigInt(this)
fun Rational.makeAPLNumber(): APLNumber = if (denominator == BigIntConstants.ONE) numerator.makeAPLNumberWithReduction() else APLRational(this)

fun Double.isInteger(): Boolean = this % 1.0 == 0.0
