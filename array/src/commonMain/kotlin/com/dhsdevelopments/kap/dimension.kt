package com.dhsdevelopments.kap

import kotlin.jvm.JvmInline

@JvmInline
value class Dimensions(private val dimensions: IntArray) {
    val size: Int get() = dimensions.size
    val indices: IntRange get() = dimensions.indices

    init {
        var curr = 1L
        dimensions.forEach { v ->
            if (v < 0) {
                throwAPLException(InvalidDimensionsException("Dimensions contains negative values"))
            }
            curr *= v
            if (curr > Int.MAX_VALUE) {
                throwAPLException(ArraySizeException(dimensions))
            }
        }
    }

    operator fun get(i: Int) = dimensions[i]
    fun contentSize() = if (dimensions.isEmpty()) 1 else dimensions.reduce { a, b -> a * b }
    fun isEmpty() = dimensions.isEmpty()
    fun isNullDimensions() = dimensions.size == 1 && dimensions[0] == 0
    fun compareEquals(other: Dimensions) = ArrayUtils.equals(dimensions, other.dimensions)

    inline fun forEach(fn: (Int) -> Unit) {
        repeat(size) { i -> fn(this[i]) }
    }

    inline fun forEachIndexed(fn: (Int, Int) -> Unit) {
        repeat(size) { i -> fn(i, this[i]) }
    }

    fun asList(): List<Int> = dimensions.asList()

    fun compareTo(other: Dimensions) = ArrayUtils.compare(dimensions, other.dimensions)

    fun insert(pos: Int, newValue: Int): Dimensions {
        val v = IntArray(dimensions.size + 1) { index ->
            when {
                index < pos -> dimensions[index]
                index > pos -> dimensions[index - 1]
                else -> newValue
            }
        }
        return Dimensions(v)
    }

    fun remove(toRemove: Int): Dimensions {
        if (toRemove < 0 || toRemove >= dimensions.size) {
            throw IndexOutOfBoundsException("Index does not fit in array. index=${toRemove}, size=${dimensions.size}")
        }
        val v = IntArray(dimensions.size - 1) { index ->
            if (index < toRemove) dimensions[index] else dimensions[index + 1]
        }
        return Dimensions(v)
    }

    fun replace(axis: Int, newValue: Int): Dimensions {
        if (axis < 0 || axis >= dimensions.size) {
            throw IndexOutOfBoundsException("Selected axis is not valid. Selected axis: ${axis}, numer of axis: ${dimensions.size}")
        }
        return Dimensions(IntArray(dimensions.size) { i -> if (i == axis) newValue else dimensions[i] })
    }

    private fun throwIndexOutOfRange(p: IntArray, pos: Position?) {
        throwAPLException(APLIndexOutOfBoundsException("Index out of range. Requested position: ${ArrayUtils.toString(p)}, dimensions: ${dimensions}", pos))
    }

    private fun refCoord(p: IntArray, dimensionIndex: Int, pos: Position?): Int {
        val v = p[dimensionIndex]
        if (v < 0 || v >= dimensions[dimensionIndex]) {
            throwIndexOutOfRange(p, pos)
        }
        return v
    }

    fun indexFromPosition(p: IntArray, pos: Position? = null): Int {
        if (p.size != dimensions.size) {
            throwAPLException(InvalidDimensionsException("Dimensions does not match", pos))
        }
        return when (p.size) {
            0 -> 0
            1 -> refCoord(p, 0, pos)
            2 -> refCoord(p, 0, pos) * dimensions[1] + refCoord(p, 1, pos)
            else -> {
                var curr = refCoord(p, 0, pos)
                for (i in 1 until p.size) {
                    curr = curr * dimensions[i] + refCoord(p, i, pos)
                }
                curr
            }
        }
    }

    /**
     * Like [indexFromPosition] but negative arguments index from the end of the dimension.
     */
    fun indexFromPositionNegativeSupport(p: IntArray, pos: Position? = null): Int {
        if (p.size != dimensions.size) {
            throwAPLException(InvalidDimensionsException("Dimensions does not match", pos))
        }
        return when (p.size) {
            0 -> 0
            1 -> checkAndAdjustSelectedIndex(p[0], dimensions[0], pos)
            2 -> checkAndAdjustSelectedIndex(p[0], dimensions[0], pos) * dimensions[1] + checkAndAdjustSelectedIndex(p[1], dimensions[1], pos)
            else -> {
                var curr = checkAndAdjustSelectedIndex(p[0], dimensions[0], pos)
                for (i in 1 until p.size) {
                    curr = curr * dimensions[i] + checkAndAdjustSelectedIndex(p[i], dimensions[i], pos)
                }
                curr
            }
        }
    }

    fun indexFromPositionNegativeSupport(p: Int, pos: Position? = null): Int {
        if (dimensions.size != 1) {
            throwAPLException(InvalidDimensionsException("Dimensions does not match", pos))
        }
        val size = dimensions[0]
        return checkAndAdjustSelectedIndex(p, size, pos)
    }

    fun multipliers(): DimensionMultipliers {
        val res = when (dimensions.size) {
            0 -> EMPTY_INT_ARRAY
            1 -> intArrayOf(1)
            2 -> intArrayOf(dimensions[1], 1)
            else -> {
                var curr = 1
                val a = IntArray(dimensions.size)
                for (i in dimensions.size - 1 downTo 1) {
                    a[i] = curr
                    curr *= dimensions[i]
                }
                a[0] = curr
                a
            }
        }
        return DimensionMultipliers(res)
    }

    fun positionFromIndex(p: Int): IntArray {
        return multipliers().positionFromIndex(p)
    }

    fun lastDimension(pos: Position? = null): Int {
        return if (dimensions.isEmpty()) {
            throwAPLException(InvalidDimensionsException("Can't take dimension from scalar", pos))
        } else {
            dimensions[dimensions.size - 1]
        }
    }

    fun lastAxis(pos: Position? = null): Int {
        if (dimensions.isEmpty()) {
            throwAPLException(InvalidDimensionsException("No axis available", pos))
        } else {
            return dimensions.size - 1
        }
    }

    fun incrementMutablePosition(position: IntArray) {
        for (i in position.size - 1 downTo 0) {
            val p = position[i]
            val size = dimensions[i]
            when {
                p < size - 1 -> {
                    position[i]++
                    break
                }
                p == size - 1 -> position[i] = 0
                p >= size -> throw IllegalStateException("Invalid position")
            }
        }
    }

    override fun toString() = "[${dimensions.joinToString(", ")}]"

    @JvmInline
    value class DimensionMultipliers(private val values: IntArray) {
        fun positionFromIndex(p: Int): IntArray {
            return when (values.size) {
                0 -> EMPTY_INT_ARRAY
                1 -> intArrayOf(p)
                2 -> values[0].let { multiplier -> intArrayOf(p / multiplier, p % multiplier) }
                else -> {
                    val size = values.size
                    IntArray(size).also { result ->
                        var curr = p
                        repeat(size - 2) { i ->
                            val multiplier = values[i]
                            result[i] = curr / multiplier
                            curr %= multiplier
                        }
                        val multiplier = values[size - 2]
                        result[size - 2] = curr / multiplier
                        result[size - 1] = curr % multiplier
                    }
                }
            }
        }

        val size: Int get() = values.size
        val indices: IntRange get() = values.indices
        operator fun get(i: Int) = values[i]
    }

    companion object {
        val EMPTY_INT_ARRAY = intArrayOf()

        fun checkAndAdjustSelectedIndex(index: Int, axisSize: Int, pos: Position?): Int {
            fun throwError(): Nothing = throwAPLException(APLIndexOutOfBoundsException("Position ${index} is outside valid range. Axis size: ${axisSize}", pos))
            return if (index >= 0) {
                if (index >= axisSize) {
                    throwError()
                }
                index
            } else {
                if (index < -axisSize) {
                    throwError()
                }
                axisSize + index
            }
        }
    }
}

private val EMPTY_LIST_DIMENSIONS = Dimensions(intArrayOf(0))
private val EMPTY_DIMENSIONS = Dimensions(intArrayOf())
private val SIZE_1_DIMENSIONS = Dimensions(intArrayOf(1))

fun emptyListDimensions() = EMPTY_LIST_DIMENSIONS
fun emptyDimensions() = EMPTY_DIMENSIONS
fun size1Dimensions() = SIZE_1_DIMENSIONS

fun dimensionsOfSize(vararg values: Int) = Dimensions(values)
