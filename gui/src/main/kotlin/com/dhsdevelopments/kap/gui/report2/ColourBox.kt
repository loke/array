package com.dhsdevelopments.kap.gui.report2

import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle

class ColourBox(red: Double, green: Double, blue: Double) : Rectangle(24.0, 24.0, Color(red, green, blue, 1.0))
