package com.dhsdevelopments.kap.gui.settings

import com.dhsdevelopments.kap.unless
import javafx.scene.text.Font
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.IOException
import java.nio.file.Path
import java.nio.file.Paths
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.io.path.*

@Serializable
enum class ReturnBehaviour { CLEAR_INPUT, PRESERVE }

@Serializable
data class Settings(
    val recentPath: String? = null,
    val directory: String? = null,
    val fontFamily: String? = null,
    val fontSize: Int? = null,
    val newlineBehaviour: ReturnBehaviour = ReturnBehaviour.CLEAR_INPUT,
    val keyPrefix: String = "`",
    val displayWelcomeText: Boolean = true,
    val displayTiming: Boolean = false
) {
    fun fontFamilyWithDefault() = fontFamily ?: "Iosevka Fixed"
    fun fontSizeWithDefault() = fontSize ?: 16
}

private fun findSettingsDirectory(): Path? {
    val homeString = System.getProperty("user.home") ?: return null
    val home = Paths.get(homeString)
    unless(home.exists()) {
        return null
    }
    val configDir = home.resolve(".kap")
    return when {
        !configDir.exists() -> configDir.also { configDir.createDirectory() }
        configDir.isDirectory() -> configDir
        else -> null
    }
}

fun loadSettings(): Settings {
    val settingsDir = findSettingsDirectory() ?: throw IOException("Unable to find settings directory")
    val settingsFile = settingsDir.resolve("kap.conf")
    if (!settingsFile.exists()) {
        return Settings()
    }
    val content = settingsFile.readText()
    return Json.decodeFromString(content)
}

fun saveSettings(settings: Settings) {
    val settingsDir = findSettingsDirectory() ?: throw IOException("Unable to find settings directory")
    val settingsFile = settingsDir.resolve("kap.conf")
    val content = Json.encodeToString(settings)
    settingsFile.writeText(content)
}

interface ConfigProvider {
    fun settings(): Settings
    fun addConfigUpdateListener(fn: (Settings, Settings) -> Unit)
    fun selectedFont(): Font
}

abstract class AbstractConfigProvider : ConfigProvider {
    private var currentFont: Font? = null

    protected fun maybeUpdateFont(oldSettings: Settings, newSettings: Settings) {
        if ((oldSettings.fontFamilyWithDefault() != newSettings.fontFamilyWithDefault()) || (oldSettings.fontSizeWithDefault() != newSettings.fontSizeWithDefault())) {
            currentFont = null
        }
    }

    override fun selectedFont(): Font {
        return currentFont ?: Font.font(settings().fontFamilyWithDefault(), settings().fontSizeWithDefault().toDouble()).also { f -> currentFont = f }
    }
}

class ConfigProviderImpl(initial: Settings) : AbstractConfigProvider() {
    private val listeners = CopyOnWriteArrayList<(Settings, Settings) -> Unit>()
    var settings: Settings = initial
        private set

    override fun settings() = settings

    fun updateSettings(newSettings: Settings) {
        val oldSettings = settings
        settings = newSettings
        maybeUpdateFont(oldSettings, newSettings)
        listeners.forEach { fn -> fn(oldSettings, newSettings) }
    }

    override fun addConfigUpdateListener(fn: (Settings, Settings) -> Unit) {
        listeners.add(fn)
    }
}
