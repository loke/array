package com.dhsdevelopments.kap.net

import com.dhsdevelopments.kap.InputStreamByteProvider
import com.dhsdevelopments.kap.OutputStreamByteConsumer
import com.dhsdevelopments.kap.transformIOException
import java.net.InetAddress
import java.net.ServerSocket
import java.net.Socket

class JvmNetworkApi : NetworkApi {
    override fun connect(host: String, port: Int): TcpConnection {
        val socket = Socket(host, port)
        return JvmTcpConnection(socket)
    }

    override fun startListener(port: Int, bindAddress: String?): TcpServerSocket {
        val addr = if (bindAddress != null) {
            InetAddress.getByName(bindAddress)
        } else {
            null
        }
        transformIOException {
            val serverSocket = ServerSocket(port, 50, addr)
            return JvmServerSocket(serverSocket)
        }
    }

    override fun hostname(): String {
        System.getenv("HOSTNAME")?.let { h -> return h }
        System.getenv("COMPUTERNAME")?.let { h -> return h }
        return InetAddress.getLocalHost().hostName
    }
}

class JvmTcpConnection(private val socket: Socket) : TcpConnection {
    override val sockaddr = JvmSockaddr(socket.inetAddress, socket.port)
    override val byteProvider = InputStreamByteProvider(socket.getInputStream())
    override val byteConsumer = OutputStreamByteConsumer(socket.getOutputStream())

    override fun close() = socket.close()
}

class JvmServerSocket(val serverSocket: ServerSocket) : TcpServerSocket {
    override fun accept(): TcpConnection {
        val socket = serverSocket.accept()
        return JvmTcpConnection(socket)
    }

    override fun close() {
        serverSocket.close()
    }
}

class JvmSockaddr(private val addr: InetAddress, override val port: Int) : TcpSockaddr {
    override val host: String get() = addr.hostAddress
    override fun toString() = "JvmSockaddr[addr=${addr}, port=${port}]"
}

actual fun networkApi(): NetworkApi? = JvmNetworkApi()
