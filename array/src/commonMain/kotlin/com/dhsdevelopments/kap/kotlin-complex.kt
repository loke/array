package com.dhsdevelopments.kap.complex

import kotlin.math.*

data class Complex(val re: Double, val im: Double = 0.0) {

    constructor(re: Int, im: Int = 0) : this(re.toDouble(), im.toDouble())
    constructor(re: Int, im: Double) : this(re.toDouble(), im)
    constructor(re: Double, im: Int) : this(re, im.toDouble())

    fun reciprocal(): Complex {
        val scale = (re * re) + (im * im)
        return Complex(re / scale, -im / scale)
    }

    fun abs(): Double = hypot(re, im)

    operator fun unaryMinus(): Complex = Complex(-re, -im)
    operator fun plus(other: Double): Complex = Complex(re + other, im)
    operator fun minus(other: Double): Complex = Complex(re - other, im)
    operator fun times(other: Double): Complex = Complex(re * other, im * other)
    operator fun div(other: Double): Complex = Complex(re / other, im / other)

    operator fun plus(other: Complex): Complex =
        Complex(re + other.re, im + other.im)

    operator fun minus(other: Complex): Complex =
        Complex(re - other.re, im - other.im)

    operator fun times(other: Complex): Complex =
        Complex(
            (re * other.re) - (im * other.im),
            (re * other.im) + (im * other.re))

    operator fun div(other: Complex): Complex = this * other.reciprocal()

    fun pow(complex: Complex): Complex {
        return if (re == 0.0 && im == 0.0) {
            ZERO
        } else {
            val arg = atan2(this.im, this.re)
            val resultAbsolute = exp(ln(this.abs()) * complex.re - (arg * complex.im))
            val resultArg = ln(this.abs()) * complex.im + arg * complex.re
            fromPolarCoord(resultAbsolute, resultArg)
        }
    }

    fun signum(): Complex {
        return if (re == 0.0 && im == 0.0) {
            ZERO
        } else {
            this / this.abs()
        }
    }

    fun ln(): Complex = Complex(ln(hypot(re, im)), atan2(im, re))
    fun log(base: Complex): Complex = ln() / base.ln()
    fun log(base: Double): Complex = log(base.toComplex())

    fun exp(): Complex = this.pow(COMPLEX_E)

    fun nearestGaussian(): Complex {
        return Complex(round(re), round(im))
    }

    override fun equals(other: Any?) = other != null && other is Complex && re == other.re && im == other.im
    override fun hashCode() = re.hashCode() xor im.hashCode()

    @Suppress("unused")
    companion object {
        fun fromPolarCoord(absolute: Double, arg: Double): Complex {
            return Complex(cos(arg) * absolute, sin(arg) * absolute)
        }

        val ZERO = Complex(0.0, 0.0)
        val ONE = Complex(1.0, 0.0)
        val TWO = Complex(2.0, 0.0)
        val I = Complex(0.0, 1.0)
        val TWO_I = Complex(0.0, 2.0)
        val COMPLEX_E = E.toComplex()
        val COMPLEX_PI = PI.toComplex()
        val NEGATIVE_ZERO = Complex(-0.0, 0.0)
        val NEGATIVE_ONE = Complex(-1.0, 0.0)
        val ONE_HALF = Complex(0.5, 0.0)
        val ONE_HALF_PI = Complex(PI / 2.0, 0.0)
    }
}

operator fun Double.plus(complex: Complex) = this.toComplex() + complex
operator fun Double.times(complex: Complex) = this.toComplex() * complex
operator fun Double.minus(complex: Complex) = this.toComplex() - complex
operator fun Double.div(complex: Complex) = this.toComplex() / complex

fun Double.toComplex() = Complex(this, 0.0)
fun Double.pow(complex: Complex) = this.toComplex().pow(complex)
fun Double.log(base: Complex) = this.toComplex().log(base)

object ComplexFieldExtension {
    fun sin(v: Complex): Complex {
        return (E.pow(v * Complex.I) - E.pow(-v * Complex.I)) / Complex.TWO_I
    }

    fun cos(v: Complex): Complex {
        return (E.pow(v * Complex.I) + E.pow(-v * Complex.I)) / Complex.TWO
    }

    fun tan(v: Complex): Complex {
        val re2 = v.re * 2
        val im2 = v.im * 2
        val d = cos(re2) + cosh(im2)
        return Complex(sin(re2) / d, sinh(im2) / d)
    }

    fun asin(v: Complex): Complex {
        return (-Complex.I * ((1.0 - (v * v)).pow(Complex.ONE_HALF) + Complex.I * v).ln())
    }

    fun acos(v: Complex): Complex {
        return (Complex.ONE_HALF_PI + Complex.I * ((1.0 - v * v).pow(Complex.ONE_HALF) + Complex.I * v).ln())
    }

    fun atan(v: Complex): Complex {
        val arg = Complex.I * v
        return Complex.I * ((1.0 - arg).ln() - (1.0 + arg).ln()) / 2.0
    }

    fun sinh(v: Complex): Complex {
        return (v.exp() - (-v).exp()) / 2.0
    }

    fun cosh(v: Complex): Complex {
        return (v.exp() + (-v).exp()) / 2.0
    }

    fun tanh(v: Complex): Complex {
        return (v.exp() - (-v).exp()) / ((-v).exp() + v.exp())
    }

    fun asinh(v: Complex): Complex {
        return ((v * v + Complex.ONE).pow(Complex.ONE_HALF) + v).ln()
    }

    fun acosh(v: Complex): Complex {
        return (v + ((v - Complex.ONE) * (v + Complex.ONE)).pow(Complex.ONE_HALF)).ln()
    }

    fun atanh(v: Complex): Complex {
        return ((v + Complex.ONE).ln() - (Complex.ONE - v).ln()) / 2.0
    }
}

//val Complex.Companion.I get() = ComplexField.i
//val Complex.Companion.ZERO get() = ComplexField.zero
//private val COMPLEX_NEGATIVE_ZERO = Complex(-0.0, 0.0)
//val Complex.Companion.NEGATIVE_ZERO get() = COMPLEX_NEGATIVE_ZERO
//private val COMPLEX_HALF = Complex(0.5, 0.0)
//val Complex.Companion.ONE_HALF get() = COMPLEX_HALF
//
//fun Complex.signum(): Complex {
//    return if (re == 0.0 && im == 0.0) {
//        Complex.ZERO
//    } else {
//        this / this.r
//    }
//}
//
//fun Complex.nearestGaussian(): Complex {
//    return Complex(round(re), round(im))
//}
//
//fun Complex.ln(): Complex = ComplexField.ln(this)
//fun Complex.log(base: Complex): Complex = ln() / base.ln()
//fun Complex.log(base: Double): Complex = log(base.toComplex())
//
//object ComplexFieldExtension {
//    fun power(x: Complex, y: Complex): Complex {
//        return if (x == Complex.ZERO || x == Complex.NEGATIVE_ZERO) {
//            Complex.ZERO
//        } else {
//            val arg = atan2(x.im, x.re)
//            val resultAbsolute = exp(ln(x.r) * y.re - (arg * y.im))
//            val resultArg = ln(x.r) * y.im + arg * y.re
//            fromPolarCoord(resultAbsolute, resultArg)
//        }
//    }
//}
//
//private fun fromPolarCoord(absolute: Double, arg: Double): Complex {
//    return Complex(cos(arg) * absolute, sin(arg) * absolute)
//}
