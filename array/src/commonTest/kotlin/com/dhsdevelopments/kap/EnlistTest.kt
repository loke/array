package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class EnlistTest : APLTest() {
    @Test
    fun enlistSimple() {
        parseAPLExpression("∊ (1 2 (3 4)) 5 6").let { result ->
            assert1DArray(arrayOf(1, 2, 3, 4, 5, 6), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun enlistWithHigherDimension() {
        parseAPLExpression("∊ 2 3 ⍴ 1 (10 20) 3 ((41 42) (43 44)) 5 6").let { result ->
            assert1DArray(arrayOf(1, 10, 20, 3, 41, 42, 43, 44, 5, 6), result)
            assertEquals(ArrayMemberType.LONG, result.specialisedType)
        }
    }

    @Test
    fun enlistWithMixedDatatypes() {
        parseAPLExpression("∊ (1 2) (3 4 (5 @a @b @c))").let { result ->
            assert1DArray(arrayOf(1, 2, 3, 4, 5, InnerChar('a'), InnerChar('b'), InnerChar('c')), result)
            assertEquals(ArrayMemberType.ATOM, result.specialisedType)
        }
    }

    @Test
    fun enlistEmpty() {
        parseAPLExpression("∊⍬").let { result ->
            assertAPLNull(result)
            assertEquals(ArrayMemberType.ALL, result.specialisedType)
        }
    }

    @Test
    fun enlistWithLimit0() {
        parseAPLExpression("∊[0] (((1 2) (3 4)) ((5 6) (7 8))) (9 10) 11 12").let { result ->
            assert1DArray(
                arrayOf(
                    Inner1D(
                        arrayOf(
                            Inner1D(arrayOf(Inner1D(arrayOf(1, 2)), Inner1D(arrayOf(3, 4)))),
                            Inner1D(arrayOf(Inner1D(arrayOf(5, 6)), Inner1D(arrayOf(7, 8)))))),
                    Inner1D(arrayOf(9, 10)),
                    11, 12),
                result)
        }
    }

    @Test
    fun enlistWithLimit1() {
        parseAPLExpression("∊[1] (((1 2) (3 4)) ((5 6) (7 8))) (9 10) 11 12").let { result ->
            assert1DArray(
                arrayOf(
                    Inner1D(arrayOf(Inner1D(arrayOf(1, 2)), Inner1D(arrayOf(3, 4)))),
                    Inner1D(arrayOf(Inner1D(arrayOf(5, 6)), Inner1D(arrayOf(7, 8)))),
                    9, 10, 11, 12),
                result)
        }
    }

    @Test
    fun enlistWithLimit2() {
        parseAPLExpression("∊[2] (((1 2) (3 4)) ((5 6) (7 8))) (9 10) 11 12").let { result ->
            assert1DArray(
                arrayOf(
                    Inner1D(arrayOf(1, 2)),
                    Inner1D(arrayOf(3, 4)),
                    Inner1D(arrayOf(5, 6)),
                    Inner1D(arrayOf(7, 8)),
                    9, 10, 11, 12),
                result)
        }
    }

    @Test
    fun enlistWithLimit3() {
        parseAPLExpression("∊[3] (((1 2) (3 4)) ((5 6) (7 8))) (9 10) 11 12").let { result ->
            assert1DArray(
                arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
                result)
        }
    }

    @Test
    fun enlistWithOversizeLimit() {
        parseAPLExpression("∊[3000] (((1 2) (3 4)) ((5 6) (7 8))) (9 10) 11 12").let { result ->
            assert1DArray(
                arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
                result)
        }
    }

    @Test
    fun enlistWithInvalidLimit() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpression("∊[¯1] (((1 2) (3 4)) ((5 6) (7 8))) (9 10) 11 12")
        }
    }

    @Test
    fun enlistWithEnclosedArrays0() {
        parseAPLExpression("∊⊂1 2").let { result ->
            assert1DArray(arrayOf(1, 2), result)
        }
    }

    @Test
    fun enlistWithEnclosedArrays1() {
        parseAPLExpression("∊ (⊂1 2) (⊂10 20)").let { result ->
            assert1DArray(arrayOf(1, 2, 10, 20), result)
        }
    }

    @Test
    fun enlistWithEnclosedArraysAndLimit0() {
        parseAPLExpression("∊[2] ((⊂1 2) (⊂10 20)) 3").let { result ->
            assert1DArray(arrayOf(Inner1D(arrayOf(1, 2)), Inner1D(arrayOf(10, 20)), 3), result)
        }
    }

    @Test
    fun enlistWithEnclosedArraysAndLimit1() {
        parseAPLExpression("∊[1]⊂1 2").let { result ->
            assert1DArray(arrayOf(1, 2), result)
        }
    }

    @Test
    fun enlistGeneric2DArray() {
        parseAPLExpression("∊2 3 ⍴ 1 2 3 4 5 @a").let { result ->
            assert1DArray(arrayOf(1, 2, 3, 4, 5, InnerChar('a')), result)
        }
    }

    @Test
    fun enlistSpecialised2DArrayWithLongs() {
        parseAPLExpression("∊2 3 ⍴ 1 2 3 4 5 6").let { result ->
            assert1DArray(arrayOf(1, 2, 3, 4, 5, 6), result)
        }
    }

    @Test
    fun enlistSpecialised2DArrayWithDoubles() {
        parseAPLExpression("∊0.0+2 3 ⍴ 1 2 3 4 5 6").let { result ->
            assert1DArray(
                arrayOf(
                    InnerDouble(1.0),
                    InnerDouble(2.0),
                    InnerDouble(3.0),
                    InnerDouble(4.0),
                    InnerDouble(5.0),
                    InnerDouble(6.0)),
                result)
        }
    }

    @Test
    fun enlistNested2DArraysGeneric() {
        parseAPLExpression("∊2 3 ⍴ (3 4 ⍴ (10+⍳11),@a) 2 3 4 5 @a").let { result ->
            assert1DArray(arrayOf(10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, InnerChar('a'), 2, 3, 4, 5, InnerChar('a')), result)
        }
    }

    @Test
    fun enlistNested2DArraysWithLongs() {
        parseAPLExpression("∊2 3 ⍴ (3 4 ⍴ 10+⍳12) 2 3 4 5 6").let { result ->
            assert1DArray(arrayOf(10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 2, 3, 4, 5, 6), result)
        }
    }

    @Test
    fun enlistNested2DArraysWithDoubles() {
        parseAPLExpression("∊0.0+2 3 ⍴ (3 4 ⍴ 10+⍳12) 2 3 4 5 6").let { result ->
            assert1DArray(
                arrayOf(
                    InnerDouble(10.0),
                    InnerDouble(11.0),
                    InnerDouble(12.0),
                    InnerDouble(13.0),
                    InnerDouble(14.0),
                    InnerDouble(15.0),
                    InnerDouble(16.0),
                    InnerDouble(17.0),
                    InnerDouble(18.0),
                    InnerDouble(19.0),
                    InnerDouble(20.0),
                    InnerDouble(21.0),
                    InnerDouble(2.0),
                    InnerDouble(3.0),
                    InnerDouble(4.0),
                    InnerDouble(5.0),
                    InnerDouble(6.0)),
                result)
        }
    }
}
