package com.dhsdevelopments.kap

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * Provide metadata information for an instance of [APLValue].
 */
interface APLValueMetadata {
    val labels: DimensionLabels? get() = null
    val defaultValue: APLValue get() = DefaultMetadata.defaultValue
    val isDefault get() = false

    fun collapse(): APLValueMetadata {
        return CollapsedAPLValueMetadata(this)
    }

    object DefaultMetadata : APLValueMetadata {
        override val labels get() = null
        override val defaultValue get() = APLLONG_0
        override val isDefault get() = true

        override fun collapse() = this
    }

    private class CollapsedAPLValueMetadata(parent: APLValueMetadata) : APLValueMetadata {
        override val labels = parent.labels
        override val defaultValue = parent.defaultValue
        override fun collapse() = this
    }
}

abstract class DerivedAPLValueMetadata(parent: APLValueMetadata) : APLValueMetadata {
    override val labels by lazy { parent.labels }
    override val defaultValue by lazy { parent.defaultValue }
}

@OptIn(ExperimentalContracts::class)
inline fun maybeDefaultMetadata(sourceMetadata: APLValueMetadata, fn: (APLValueMetadata) -> APLValueMetadata): APLValueMetadata {
    contract { callsInPlace(fn, InvocationKind.AT_MOST_ONCE) }
    return if (sourceMetadata.isDefault) {
        APLValueMetadata.DefaultMetadata
    } else {
        fn(sourceMetadata)
    }
}

class MetadataOverrideArray(override val value: APLValue, override val metadata: APLValueMetadata) : AbstractDelegatedValue() {
    init {
        require(!value.isAtomic) { "Attempt to override metadata for an atomic value" }
    }

    override fun formatted(style: FormatStyle): String {
        return when (style) {
            FormatStyle.PLAIN -> value.formatted(style)
            FormatStyle.PRETTY -> value.formatted(style)
            FormatStyle.READABLE -> {
                val labelsDefinition = makeReadableLabelsDefinitionIfExists(metadata)
                val content = value.formatted(style)
                if (labelsDefinition == null) {
                    content
                } else {
                    "${labelsDefinition}${content}"
                }
            }
        }
    }

    override fun unwrapDeferredValue(): APLValue {
        val v = value.unwrapDeferredValue()
        require(v !is APLSingleValue) { "Metadata was attached to singlevalue" }
        return MetadataOverrideArray(v, metadata)
    }

    companion object {
        fun makeWithMergedLabels(value: APLValue, extraLabels: List<List<AxisLabel?>?>): MetadataOverrideArray {
            val m = APLValueMetadataOverrideLabels(value.metadata, DimensionLabels.makeDerived(value.dimensions, value.metadata.labels, extraLabels))
            return MetadataOverrideArray(value, m)
        }
    }

    class APLValueMetadataOverrideLabels(private val sourceMetadata: APLValueMetadata, override val labels: DimensionLabels) : APLValueMetadata {
        override val defaultValue get() = sourceMetadata.defaultValue
    }
}

class AssignPrototypeFunction : APLFunctionDescriptor {
    class AssignPrototypeFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            return wrapWithPrototype(b, a, pos)
        }

        class APLValueMetadataOverridePrototype(private val bm: APLValueMetadata, a: APLValue) : APLValueMetadata {
            override val labels by lazy { bm.labels }
            override val defaultValue = a
        }

        companion object {
            fun wrapWithPrototype(value: APLValue, defaultValue: APLValue, pos: Position? = null): APLValue {
                if (value.isAtomic) {
                    throwAPLException(APLIllegalArgumentException("Prototypes can only be set for arrays. Argument is atomic.", pos))
                }
                val bm = value.metadata
                val newMetadata = APLValueMetadataOverridePrototype(bm, defaultValue)
                var curr = value
                while (curr is MetadataOverrideArray) {
                    curr = curr.value
                }
                return MetadataOverrideArray(curr, newMetadata)
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = AssignPrototypeFunctionImpl(instantiation)
}
