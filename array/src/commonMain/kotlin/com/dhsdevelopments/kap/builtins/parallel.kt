package com.dhsdevelopments.kap.builtins

import kotlin.math.ceil
import kotlin.math.min

class ParallelWrappedException(val exceptions: List<Throwable>, pos: com.dhsdevelopments.kap.Position? = null) :
    com.dhsdevelopments.kap.APLEvalException("Wrapped exceptions. Primary: ${exceptions[0].message}", pos) {
    fun primaryException() = exceptions[0]
}

interface ParallelTaskResult

interface ParallelTask {
    fun computeResult(context: com.dhsdevelopments.kap.RuntimeContext): com.dhsdevelopments.kap.Either<ParallelTaskResult, Throwable>
}

abstract class ParallelTaskList {
    val tasks = ArrayList<ParallelTask>()
    abstract fun finaliseCompute(results: ArrayList<com.dhsdevelopments.kap.Either<ParallelTaskResult, Throwable>>): com.dhsdevelopments.kap.APLValue
}

interface ParallelSupported {
    fun computeParallelTasks1Arg(
        context: com.dhsdevelopments.kap.RuntimeContext,
        numTasks: Int,
        a: com.dhsdevelopments.kap.APLValue,
        axis: com.dhsdevelopments.kap.APLValue?): ParallelTaskList

    fun computeParallelTasks2Arg(
        context: com.dhsdevelopments.kap.RuntimeContext,
        numTasks: Int,
        a: com.dhsdevelopments.kap.APLValue,
        b: com.dhsdevelopments.kap.APLValue,
        axis: com.dhsdevelopments.kap.APLValue?): ParallelTaskList

    val underlyingFunction: com.dhsdevelopments.kap.APLFunction
}

/**
 * Implementation of parallel task that does not perform parallelisation.
 */
class SimpleParallelTaskList1Arg(
    val fn: com.dhsdevelopments.kap.APLFunction,
    val a: com.dhsdevelopments.kap.APLValue,
    val axis: com.dhsdevelopments.kap.APLValue?) : ParallelTaskList() {
    init {
        tasks.add(SimpleParallelTask())
    }

    override fun finaliseCompute(results: ArrayList<com.dhsdevelopments.kap.Either<ParallelTaskResult, Throwable>>): com.dhsdevelopments.kap.APLValue {
        require(results.size == 1)
        when (val res = results[0]) {
            is com.dhsdevelopments.kap.Either.Left -> return (res.value as SimpleParallelTaskResult).result
            is com.dhsdevelopments.kap.Either.Right -> throw res.value
        }
    }

    private inner class SimpleParallelTask : ParallelTask {
        override fun computeResult(context: com.dhsdevelopments.kap.RuntimeContext): com.dhsdevelopments.kap.Either<ParallelTaskResult, Throwable> {
            return try {
                val res = fn.eval1Arg(context, a, axis)
                _root_ide_package_.com.dhsdevelopments.kap.Either.Left(SimpleParallelTaskResult(res))
            } catch (e: Exception) {
                _root_ide_package_.com.dhsdevelopments.kap.Either.Right(e)
            }
        }
    }

    private class SimpleParallelTaskResult(val result: com.dhsdevelopments.kap.APLValue) : ParallelTaskResult
}

class ConstantParallelTaskList(val value: com.dhsdevelopments.kap.APLValue) : ParallelTaskList() {
    override fun finaliseCompute(results: ArrayList<com.dhsdevelopments.kap.Either<ParallelTaskResult, Throwable>>): com.dhsdevelopments.kap.APLValue {
        return value
    }
}

class ParallelCompressTaskList(val value: com.dhsdevelopments.kap.APLValue, numTasks: Int, val pos: com.dhsdevelopments.kap.Position?) : ParallelTaskList() {
    private val valueMetadata = value.metadata
    private val dimensions = value.dimensions
    private val size = dimensions.contentSize()

    init {
        val r = size % numTasks
        val unitSize = size / numTasks
        var start = 0
        repeat(min(numTasks, size)) { i ->
            val n = if (i < r) unitSize + 1 else unitSize
            tasks.add(ParallelCompressTask(start, start + n))
            start += n
        }
    }

    override fun finaliseCompute(results: ArrayList<com.dhsdevelopments.kap.Either<ParallelTaskResult, Throwable>>): com.dhsdevelopments.kap.APLValue {
        val list = ArrayList<com.dhsdevelopments.kap.APLValue>()
        val exceptions = ArrayList<Throwable>()
        var st = _root_ide_package_.com.dhsdevelopments.kap.ArrayMemberType.Companion.ALL
        results.forEach { v ->
            when (v) {
                is com.dhsdevelopments.kap.Either.Left -> {
                    val computeRes = v.value as ParallelCompressResult
                    st = st.intersection(computeRes.specialisedType)
                    list.addAll(computeRes.result)
                }
                is com.dhsdevelopments.kap.Either.Right -> exceptions.add(v.value)
            }
        }
        if (exceptions.isNotEmpty()) {
            _root_ide_package_.com.dhsdevelopments.kap.throwAPLException(ParallelWrappedException(exceptions, pos))
        }
        val result = _root_ide_package_.com.dhsdevelopments.kap.CollapsedArrayImpl(dimensions, list.toTypedArray(), st)
        return if (valueMetadata.isDefault) {
            result
        } else {
            _root_ide_package_.com.dhsdevelopments.kap.MetadataOverrideArray(value, valueMetadata)
        }
    }

    private inner class ParallelCompressTask(val start: Int, val end: Int) : ParallelTask {
        override fun computeResult(context: com.dhsdevelopments.kap.RuntimeContext): com.dhsdevelopments.kap.Either<ParallelTaskResult, Throwable> {
            return try {
                val res = ArrayList<com.dhsdevelopments.kap.APLValue>()
                var st = _root_ide_package_.com.dhsdevelopments.kap.ArrayMemberType.Companion.ALL
                for (i in start until end) {
                    val v = value.valueAt(i).collapseInt()
                    st = st.intersection(v.specialisedTypeAsMember)
                    res.add(v)
                }
                _root_ide_package_.com.dhsdevelopments.kap.Either.Left(ParallelCompressResult(res, st))
            } catch (e: Exception) {
                _root_ide_package_.com.dhsdevelopments.kap.Either.Right(e)
            }
        }
    }

    private inner class ParallelCompressResult(
        val result: List<com.dhsdevelopments.kap.APLValue>,
        val specialisedType: com.dhsdevelopments.kap.ArrayMemberType) : ParallelTaskResult

    companion object {
        fun make(value: com.dhsdevelopments.kap.APLValue, numTasks: Int, pos: com.dhsdevelopments.kap.Position): ParallelTaskList {
            return if (value.dimensions.size == 0) {
                return ConstantParallelTaskList(value)
            } else {
                ParallelCompressTaskList(value, numTasks, pos)
            }
        }
    }
}

class ParallelOp : com.dhsdevelopments.kap.APLOperatorOneArg {
    override fun combineFunction(
        fn: com.dhsdevelopments.kap.APLFunction,
        pos: com.dhsdevelopments.kap.FunctionInstantiation): com.dhsdevelopments.kap.APLFunctionDescriptor {
        if (fn !is ParallelSupported) {
            throw _root_ide_package_.com.dhsdevelopments.kap.ParallelNotSupported(pos.pos)
        }
        return ParallelHandler(fn)
    }
}

private class ParallelHandler(val derived: ParallelSupported, val numTasksWeightFactor: Double = 10.0) : com.dhsdevelopments.kap.APLFunctionDescriptor {
    inner class ParallelHandlerImpl(pos: com.dhsdevelopments.kap.FunctionInstantiation) : com.dhsdevelopments.kap.APLFunction(pos) {
        override fun eval1Arg(
            context: com.dhsdevelopments.kap.RuntimeContext,
            a: com.dhsdevelopments.kap.APLValue,
            axis: com.dhsdevelopments.kap.APLValue?): com.dhsdevelopments.kap.APLValue {
            return if (context.engine.isInComputeThread) {
                derived.underlyingFunction.eval1Arg(context, a, axis)
            } else {
                val parallelTaskList = derived.computeParallelTasks1Arg(context, computeNumEngines(context), a, axis)
                evalTaskList(context, parallelTaskList)
            }
        }

        override fun eval2Arg(
            context: com.dhsdevelopments.kap.RuntimeContext,
            a: com.dhsdevelopments.kap.APLValue,
            b: com.dhsdevelopments.kap.APLValue,
            axis: com.dhsdevelopments.kap.APLValue?): com.dhsdevelopments.kap.APLValue {
            return if (context.engine.isInComputeThread) {
                derived.underlyingFunction.eval2Arg(context, a, b, axis)
            } else {
                val parallelTaskList = derived.computeParallelTasks2Arg(context, computeNumEngines(context), a, b, axis)
                evalTaskList(context, parallelTaskList)
            }
        }

        private fun checkNotIsInComputeThread(engine: com.dhsdevelopments.kap.Engine) {
            if (engine.isInComputeThread) {
                _root_ide_package_.com.dhsdevelopments.kap.throwAPLException(
                    _root_ide_package_.com.dhsdevelopments.kap.APLEvalException(
                        "Attempt to start a parallel task from a compute thread",
                        pos))
            }
        }

        private fun computeNumEngines(context: com.dhsdevelopments.kap.RuntimeContext) =
            ceil(context.engine.backgroundDispatcher.numThreads * numTasksWeightFactor).toInt()

        private fun evalTaskList(context: com.dhsdevelopments.kap.RuntimeContext, parallelTaskList: ParallelTaskList): com.dhsdevelopments.kap.APLValue {
            val engine = context.engine
            val dispatcher = context.engine.backgroundDispatcher
            val tasks = parallelTaskList.tasks.map { task ->
                dispatcher.start {
                    engine.inComputeThread.value = true
                    engine.withThreadLocalAssigned {
                        task.computeResult(context)
                    }
                }
            }
            val results = ArrayList<com.dhsdevelopments.kap.Either<ParallelTaskResult, Throwable>>()
            tasks.forEach { task ->
                val res = task.await()
                results.add(res)
            }
            engine.checkInterrupted(pos)
            return parallelTaskList.finaliseCompute(results)
        }
    }

    override fun make(instantiation: com.dhsdevelopments.kap.FunctionInstantiation) = ParallelHandlerImpl(instantiation)
}
