package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.builtins.TagCatch
import com.dhsdevelopments.kap.builtins.ThrowableTag
import com.dhsdevelopments.kap.log.SimpleLogger
import com.dhsdevelopments.mpbignum.BigInt

object KapLogger : SimpleLogger("kap")

open class APLGenericException(message: String, val pos: Position? = null, cause: Throwable? = null) : Exception(message, cause) {
    var extendedDescription: String? = null

    fun formattedError(): String {
        return buildString {
            if (pos != null) {
                val name = pos.source.name
                val fileName = if (name == null) "" else " ${name}"
                append("Error at${fileName}: ${pos.line + 1}:${pos.col + 1}: ")
                if (pos.callerName != null) {
                    append("${pos.callerName}: ")
                }
            } else {
                append("Error: ")
            }
            if (message != null) {
                append(message)
            } else {
                append("no message")
            }
        }
    }

    override fun toString() = formattedError()
}

fun <T : APLGenericException> T.details(description: String): T {
    if (extendedDescription != null) {
        throw IllegalStateException("Extended description already set")
    }
    extendedDescription = description
    return this
}

fun throwTagCatch(
    engine: Engine,
    name: String,
    data: APLValue,
    message: String,
    pos: Position? = null,
    relatedException: Throwable? = null,
): Nothing {
    throwAPLException(
        TagCatch(
            ThrowableTag(
                APLSymbol(engine.internSymbol(name, engine.keywordNamespace)),
                data),
            message,
            pos,
            relatedException))
}

class APLEvaluationInterrupted(pos: Position? = null) : APLGenericException("Interrupted", pos)

open class APLEvalException(message: String, pos: Position? = null, cause: Throwable? = null) : APLGenericException(message, pos, cause) {
    var stack: CapturedStorageStack? = null
    open val tag: ThrowableTag? = null

}

class CapturedStorageStack(stack: StorageStack) {
    val frames = stack.stack.map { frame -> CapturedStorageStackFrame(frame) }

    class CapturedStorageStackFrame(val frame: StorageStack.StorageStackFrame) {
        val primaryPos: Position? get() = calledFrom ?: frame.pos
        var calledFrom: Position? = null
    }
}

open class IncompatibleTypeException(message: String, pos: Position? = null, cause: Throwable? = null) : APLEvalException(message, pos, cause)

class InvalidDimensionsException(message: String, pos: Position? = null) : APLEvalException(message, pos) {
    constructor(aDimensions: Dimensions, bDimensions: Dimensions, pos: Position)
            : this("Mismatched dimensions. a: ${aDimensions}, b: ${bDimensions}", pos)
}

class APLIndexOutOfBoundsException(message: String, pos: Position? = null) : APLEvalException("Index out of bounds: ${message}", pos)
class VariableNotAssigned(name: Symbol, pos: Position? = null) : APLEvalException("Variable not assigned: ${name.nameWithNamespace}", pos)
class IllegalAxisException(message: String, pos: Position? = null) : APLEvalException(message, pos) {
    constructor(axis: Int, dimensions: Dimensions, pos: Position? = null)
            : this("Axis $axis is not valid. Expected: ${dimensions.size}", pos)
}

class AxisNotSupported(pos: Position) : APLEvalException("Function does not support axis specifier", pos)

open class APLIllegalArgumentException(message: String, pos: Position? = null) : APLEvalException(message, pos)

open class NumericComparisonNotSupported(message: String, pos: Position? = null) : APLIllegalArgumentException(message, pos)
class APLArgumentIsNotANumberException(pos: Position? = null) : NumericComparisonNotSupported("Value is not a number", pos)
class APLArgumentComplexOrderingException(pos: Position? = null) : NumericComparisonNotSupported("Complex numbers does not have a total order", pos)

class Unimplemented1ArgException(pos: Position? = null) : APLEvalException("Function cannot be called with one argument", pos) {
    init {
        details(
            "An attempt was made to call this function with a single argument (i.e. a call of the form: FN x), " +
                    "but this function can only be called with two arguments.")
    }
}

class Unimplemented2ArgException(pos: Position? = null) : APLEvalException("Function cannot be called with two arguments", pos) {
    init {
        details(
            "An attempt was made to call this function with two arguments (i.e. a call of the form: x FN y), " +
                    "but this function can only be called with a single argument to the right of the function name.")
    }
}

class IllegalArgumentNumException(expectedCount: Int, receivedCount: Int, pos: Position? = null, minArgs: Int? = null) :
    APLEvalException(
        "Expected a list of ${if (minArgs == null) expectedCount.toString() else "${minArgs}-${expectedCount}"} values. Actual elements: ${receivedCount}",
        pos)

class APLArithmeticException(message: String, pos: Position? = null) : APLEvalException(message, pos)

open class KapOverflowException(message: String, pos: Position? = null, cause: Throwable? = null) : APLEvalException(message, pos, cause)
class IntMagnitudeException(value: Long, pos: Position? = null) : KapOverflowException("Value does not fit in an int: ${value}", pos)
class LongMagnitudeException(value: BigInt, pos: Position? = null) : KapOverflowException("Value does not fit in a long: ${value}", pos)
class ArrayTooLargeException(message: String, pos: Position? = null) : KapOverflowException(message, pos)

class InverseNotAvailable(pos: Position? = null) : APLEvalException("Function does not have an inverse", pos)

class LeftAssigned2ArgException(pos: Position? = null) :
    APLEvalException("Left assigned functions cannot be called with two arguments", pos)

class ArraySizeException(d: IntArray, pos: Position? = null) :
    APLEvalException("Array too large. Requested size: [${d.joinToString(", ")}]", pos)

class DestructuringAssignmentShapeMismatch(message: String, pos: Position? = null) : APLEvalException(message, pos) {
    constructor(pos: Position? = null) : this("Destructuring assignment target shape does not match argument", pos)
}

class StructuralUnderNotSupported(pos: Position? = null) : APLEvalException("under not supported for function", pos)
class CircularDynamicAssignment(pos: Position? = null) : APLEvalException("Circular dynamic assignment", pos)
class ListOutOfBounds(message: String, pos: Position? = null) : APLEvalException(message, pos)
class UnmodifiableSystemParameterException(pos: Position? = null) : APLEvalException("System parameter cannot be modified", pos)
class ModuleNotFound(name: String, pos: Position? = null) : APLEvalException("Module not available: ${name}", pos)
class AsyncNotSupported(fnName: String, pos: Position? = null) : APLEvalException("Async not supported for function: ${fnName}", pos)
class KapInternalError(message: String, pos: Position? = null) : APLEvalException(message, pos)
class KeyNotFoundException(pos: Position? = null) : APLEvalException("Key not found", pos)

open class ParseException(message: String, pos: Position? = null, cause: Throwable? = null) : APLGenericException(message, pos, cause)

class UnexpectedSymbol(ch: Int, pos: Position? = null) :
    ParseException("Unexpected symbol: '${charToString(ch)}' (${ch.toString(16)})", pos)

class UnexpectedToken(token: Token, pos: Position? = null) : ParseException("Unexpected token: ${token.formatted()}", pos)
class IncompatibleTypeParseException(message: String, pos: Position? = null) : ParseException(message, pos)
class IllegalNumberFormat(message: String, pos: Position? = null) : ParseException(message, pos)
class IllegalContextForFunction(pos: Position? = null) : ParseException("No arguments specified for function", pos)
class OperatorAxisNotSupported(pos: Position) : ParseException("Operator does not support axis argument", pos)
class SyntaxRuleMismatch(expectedSymbol: Symbol, foundSymbol: Symbol, pos: Position? = null) :
    ParseException("In custom syntax rule: Expected: ${expectedSymbol.symbolName}. Found: ${foundSymbol.symbolName}", pos)

class BitwiseNotSupported(pos: Position? = null) : ParseException("Function does not support bitwise operations", pos)
class ParallelNotSupported(pos: Position? = null) : ParseException("Function does not support parallel", pos)
class SyntaxSubRuleNotFound(name: Symbol, pos: Position? = null) : ParseException("Syntax sub rule does not exist. Name: ${name}", pos)
class IllegalDeclaration(message: String, pos: Position? = null) : ParseException("Illegal declaration: ${message}", pos)

class InvalidFunctionRedefinition(name: Symbol, pos: Position? = null) :
    ParseException("Function cannot be redefined: ${name.nameWithNamespace}", pos)

class InvalidOperatorArgument(name: Symbol, pos: Position? = null) : ParseException("Operator without left function: ${name.symbolName}", pos)

class AssignmentToConstantException(name: Symbol, pos: Position? = null) :
    ParseException("Assignment to constant variable: ${name.nameWithNamespace}", pos)

class ReturnValue(val value: APLValue, val returnEnvironment: Environment, pos: Position? = null) : APLEvalException("Target stack frame is not available", pos)

fun ensureValidAxis(axis: Int, dimensions: Dimensions, pos: Position? = null) {
    if (axis < 0 || axis >= dimensions.size) {
        throwAPLException(IllegalAxisException(axis, dimensions, pos))
    }
}

fun checkAxisPositionIsInRange(posAlongAxis: Int, dimensions: Dimensions, axis: Int, pos: Position?) {
    if (posAlongAxis < 0 || posAlongAxis >= dimensions[axis]) {
        throwAPLException(
            APLIndexOutOfBoundsException(
                "Position ${posAlongAxis} does not fit in dimensions: ${dimensions} axis ${axis}",
                pos))
    }
}
