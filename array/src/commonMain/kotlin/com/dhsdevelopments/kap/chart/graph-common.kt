package com.dhsdevelopments.kap.chart

import com.dhsdevelopments.kap.*


class Plot2DParams(val min: Double, val max: Double) {
    init {
        require(min < max)
    }
}

fun plot2DParamsFromAPLValue(a: APLValue, pos: Position): Plot2DParams {
    val d = a.dimensions
    if (d.size != 1 || d[0] != 2) {
        throwAPLException(APLIllegalArgumentException("Params argument must be a rank-1 array with 2 elements", pos))
    }
    val min = a.valueAtCoerceToDouble(0, pos)
    val max = a.valueAtCoerceToDouble(1, pos)
    if (min >= max) {
        throwAPLException(APLIllegalArgumentException("Lower bound must be strictly less than upper bound", pos))
    }
    return Plot2DParams(min, max)
}

fun makeFunctionList(a: APLValue, pos: Position): Pair<Array<APLFunction>, Array<String?>?> {
    val a0 = a.arrayify()
    if (a0.dimensions.size != 1) {
        throwAPLException(APLIllegalArgumentException("Function list must be a scalar or a 1-dimensional array", pos))
    }
    val labels = a0.metadata.labels?.labels?.get(0)
    val fnList = Array(a0.dimensions[0]) { i ->
        val v = a0.valueAt(i)
        if (v !is LambdaValue) {
            throwAPLException(APLIllegalArgumentException("Element ${i} is not a function. Found: ${v.kapClass.name}", pos))
        }
        v.makeClosure()
    }
    val labelsList = if (labels == null) {
        null
    } else {
        Array(a0.dimensions[0]) { i ->
            labels[i]?.title
        }
    }
    return Pair(fnList, labelsList)
}

class AdditionalError(val message: String, val pos: Position?)
class FunctionGraphDataset(val label: String, val data: DoubleArray)
class FunctionGraphDatasetList(
    val xPositions: DoubleArray,
    val datasets: List<FunctionGraphDataset>,
    val additionalError: AdditionalError?)

fun computeFunctionGraphDataset(
    context: RuntimeContext,
    params: Plot2DParams,
    functions: Array<APLFunction>,
    labels: Array<String?>?,
    pos: Position,
    numPoints: Int = 600,
): FunctionGraphDatasetList {
    val posList = DoubleArray(numPoints) { i ->
        params.min + ((params.max - params.min) / numPoints.toDouble()) * i
    }
    val datasets = ArrayList<FunctionGraphDataset>()
    var additionalError: AdditionalError? = null
    functions.forEachIndexed { functionIndex, fnRef ->
        val points = DoubleArray(numPoints) { i ->
            try {
                evalFnDouble(fnRef, posList[i], context, pos)
            } catch (e: APLEvalException) {
                if (additionalError == null) {
                    additionalError = AdditionalError(e.formattedError(), e.pos)
                }
                Double.NaN
            }
        }
        val label = if (labels != null) labels[functionIndex] else null
        datasets.add(FunctionGraphDataset(label ?: functionIndex.toString(), points))
    }
    return FunctionGraphDatasetList(posList, datasets, additionalError)
}

private fun evalFnDouble(fn: APLFunction, x: Double, context: RuntimeContext, pos: Position): Double {
    return if (fn.optimisationFlags.is1ADouble) {
        fn.eval1ArgDouble(context, x, null)
    } else {
        fn.eval1Arg(context, x.makeAPLNumber(), null).ensureNumber(pos).asDouble(pos)
    }
}
