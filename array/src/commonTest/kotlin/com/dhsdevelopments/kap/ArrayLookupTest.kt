package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertFailsWith

class ArrayLookupTest : APLTest() {
    @Test
    fun testSimpleArrayLookup() {
        val result = parseAPLExpression("2 ⌷ 1 2 3 4")
        assertSimpleNumber(3, result)
    }

    @Test
    fun testSimpleArrayLookupFromFunctionInvocation() {
        val result = parseAPLExpression("2 ⌷ 10 + 10 11 12 13")
        assertSimpleNumber(22, result)
    }

    @Test
    fun testIllegalIndex() {
        assertFailsWith<APLEvalException> {
            parseAPLExpression("3 ⌷ 1 2 3").collapse()
        }
    }

    @Test
    fun illegalDimension() {
        assertFailsWith<APLEvalException> {
            parseAPLExpression("2 3 ⌷ 1 2 3 4").collapse()
        }
    }

    @Test
    fun multiDimensionLookup() {
        parseAPLExpression("3 4 ⌷ 4 5 ⍴ 100+⍳100").let { result ->
            assertSimpleNumber(119, result)
        }
    }

    @Test
    fun multiValueLookup0() {
        parseAPLExpression("(⊂ 5 10) ⌷ 100+⍳100").let { result ->
            assertDimension(dimensionsOfSize(2), result)
            assertArrayContent(arrayOf(105, 110), result)
        }
    }

    @Test
    fun multiValueLookup1() {
        parseAPLExpression("(3 0) (3 2) ⌷ 4 5⍴100+⍳100").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(118, 117, 103, 102), result)
        }
    }

    @Test
    fun lookupMajorAxis() {
        parseAPLExpression("0 ⌷ 4 4⍴100+⍳100").let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(100, 101, 102, 103), result)
        }
    }

    @Test
    fun lookupMajorAxisExplicit() {
        parseAPLExpression("0 ⌷[0] 4 4⍴100+⍳100").let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(100, 101, 102, 103), result)
        }
    }

    @Test
    fun lookupOtherAxisExplicit() {
        parseAPLExpression("0 ⌷[1] 4 4⍴100+⍳100").let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(100, 104, 108, 112), result)
        }
    }

    @Test
    fun multiValueLookupMajorAxis() {
        parseAPLExpression("(⊂ 3 0) ⌷ 4 6 ⍴ 100+⍳100").let { result ->
            assertDimension(dimensionsOfSize(2, 6), result)
            assertArrayContent(arrayOf(118, 119, 120, 121, 122, 123, 100, 101, 102, 103, 104, 105), result)
        }
    }

    @Test
    fun lookupMultiAxes() {
        parseAPLExpression("0 1 ⌷[0 1] 4 4 4 ⍴ 100+⍳100").let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(104, 105, 106, 107), result)
        }
    }

    @Test
    fun lookupMultiAxesWithNegativeIndex0() {
        parseAPLExpression("0 ¯1 ⌷[0 1] 4 4 4 ⍴ 100+⍳100").let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(112, 113, 114, 115), result)
        }
    }

    @Test
    fun lookupMultiAxesWithNegativeIndex1() {
        parseAPLExpression("(¯2 ¯1) 2 ⌷[0 1] 4 4 4 ⍴ 100+⍳100").let { result ->
            assertDimension(dimensionsOfSize(2, 4), result)
            assertArrayContent(arrayOf(140, 141, 142, 143, 156, 157, 158, 159), result)
        }
    }

    @Test
    fun lookupMultiAxesIllegalAxis() {
        assertFailsWith<IllegalAxisException> {
            parseAPLExpression("(0 1) (3 1) ⌷[0 3] 4 4 4 ⍴ 100+⍳100")
        }
    }

    @Test
    fun lookupMultiAxesNotEnoughArguments() {
        assertFailsWith<IllegalAxisException> {
            parseAPLExpression("(0 1) (3 1) ⌷[0] 4 4 4 ⍴ 100+⍳100")
        }
    }

    @Test
    fun lookupMultiAxesTooManyAxes() {
        assertFailsWith<IllegalAxisException> {
            parseAPLExpression("(0 1) (2 3) ⌷[0 1 2 3] 10 4 4 4 ⍴ 100+⍳100")
        }
    }

    @Test
    fun multiValueLookupWithIndexOutOfRange() {
        assertFailsWith<APLIndexOutOfBoundsException> {
            parseAPLExpression("(⊂ 11 100) ⌷ 100+⍳100")
        }
    }


    @Test
    fun duplicatedAxes() {
        assertFailsWith<IllegalAxisException> {
            parseAPLExpression("1 2 ⌷[1 1] 4 4⍴100+⍳100")
        }
    }

    @Test
    fun axisInvalidDimension() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("0 (0 1) 0 0 ⌷[2 2 ⍴ 1 0 2 3] 4 4 4 4 4 4 ⍴ ⍳4")
        }
    }

    @Test
    fun lookupWithBigint() {
        assertFailsWith<LongMagnitudeException> {
            parseAPLExpression("(1 2 3)[10000000000000000000000000000000000000000000000000000000000000000]")
        }
    }

    @Test
    fun lookupWithBigintValidArg() {
        assertBigIntOrLong(2, parseAPLExpression("(1 2 3)[int:asBigint 1]"))
    }

    @Test
    fun lookupWithLargeRational() {
        assertFailsWith<LongMagnitudeException> {
            parseAPLExpression("(1 2 3)[10000000000000000000000000000000000000000000000000000000000000000÷5]")
        }
    }

    @Test
    fun bracketLookupWithNull() {
        parseAPLExpression("(3 3 3 ⍴ ⍳100)[2;null;2]").let { result ->
            assert1DArray(arrayOf(20, 23, 26), result)
        }
    }

    @Test
    fun fnLookupWithNull() {
        parseAPLExpression("2 null 2 ⌷ 3 3 3 ⍴ ⍳100").let { result ->
            assert1DArray(arrayOf(20, 23, 26), result)
        }
    }

    @Test
    fun fnLookupWithAxisAndNull() {
        parseAPLExpression("0 null 2 ⌷[1 0 2] 3 3 3 ⍴ ⍳100").let { result ->
            assert1DArray(arrayOf(2, 11, 20), result)
        }
    }

    @Test
    fun fnLookupWithMultipleNull() {
        parseAPLExpression("null 0 null ⌷ 3 3 3 ⍴ ⍳100").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(arrayOf(0, 1, 2, 9, 10, 11, 18, 19, 20), result)
        }
    }

    @Test
    fun fnLookupWithAxisAndMultipleNull() {
        parseAPLExpression("null (1 2) null ⌷[1 2 0] 6 5 4 ⍴ ⍳100").let { result ->
            assertDimension(dimensionsOfSize(6, 5, 2), result)
            assertArrayContent(
                arrayOf(
                    1, 2, 5, 6, 9, 10, 13, 14, 17, 18, 21, 22, 25, 26, 29, 30, 33, 34, 37, 38, 41, 42, 45,
                    46, 49, 50, 53, 54, 57, 58, 61, 62, 65, 66, 69, 70, 73, 74, 77, 78, 81, 82, 85, 86, 89,
                    90, 93, 94, 97, 98, 1, 2, 5, 6, 9, 10, 13, 14, 17, 18),
                result)
        }
    }

    @Test
    fun fnLookupWithNegativeIndex() {
        parseAPLExpression("¯1 ⌷ 1 2 3 4 5").let { result ->
            assertSimpleNumber(5, result)
        }
    }

    @Test
    fun fnLookupMultipleWithNegativeIndex() {
        parseAPLExpression("(⊂ ¯1 ¯4) ⌷ 1 2 3 4 5 6 7 8 9 10").let { result ->
            assert1DArray(arrayOf(10, 7), result)
        }
    }

    @Test
    fun fnLookupMultiDimensionalWithNegativeIndex() {
        parseAPLExpression("(0 1) (¯3 2) ⌷ 5 4 ⍴ 100+⍳20").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(101, 102, 105, 106), result)
        }
    }

    @Test
    fun fnLookupWithOverflowShouldFail() {
        assertFailsWith<APLIndexOutOfBoundsException> {
            parseAPLExpression("(0 1 4) (1 2) (0 1) ⌷[1 2 0] 3 3 3 ⍴ ⍳100")
        }
    }

    // This is the same as the previous test, except that the index is larger than the total
    // size of the target array. The difference compared to the previous test is that this
    // one did not raise an error if the result is not collapsed.
    @Test
    fun fnLookupWithAxisAndLargeOverflowShouldFail() {
        assertFailsWith<APLIndexOutOfBoundsException> {
            parseAPLExpression("⍴ (0 1 400) (1 2) (0 1) ⌷[1 2 0] 3 3 3 ⍴ ⍳100")
        }
    }

    @Test
    fun fnLookupWithAxisAndNegativeIndexShouldFail() {
        assertFailsWith<APLIndexOutOfBoundsException> {
            parseAPLExpression("⍴ (0 1 2) (1 ¯20) (0 1) ⌷[1 2 0] 3 3 3 ⍴ ⍳100")
        }
    }

    @Test
    fun fnLookupNoAxisAndLargeOverflowShouldFail() {
        assertFailsWith<APLIndexOutOfBoundsException> {
            parseAPLExpression("⍴ (0 1 400) (1 ¯2) (0 1) ⌷ 3 3 3 ⍴ ⍳100")
        }
    }

    @Test
    fun fnLookupNoAxisAndNegativeIndexShouldFail() {
        assertFailsWith<APLIndexOutOfBoundsException> {
            parseAPLExpression("⍴ (0 1 2) (1 ¯20) (0 1) ⌷ 3 3 3 ⍴ ⍳100")
        }
    }

    @Test
    fun indexLookupNestedValue0() {
        parseAPLExpression("0 ⌷ (1 2) (3 4 5)").let { result ->
            assertDimension(emptyDimensions(), result)
            assert1DArray(arrayOf(1, 2), result.valueAt(0))
        }
    }

    @Test
    fun indexLookupNestedValue1() {
        parseAPLExpression("0 1 ⌷ 2 3 ⍴ (1 2) (3 4 5) (6 7) (8 9) (10 11 12 13) (14 15 16 17)").let { result ->
            assertDimension(emptyDimensions(), result)
            assert1DArray(arrayOf(3, 4, 5), result.valueAt(0))
        }
    }
}
