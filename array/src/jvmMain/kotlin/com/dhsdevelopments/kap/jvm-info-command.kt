package com.dhsdevelopments.kap

fun initJvmInfoCommand(engine: Engine) {
    engine.commandManager.registerCommandHandler("jvminfo", JvmInfoCommandHandler())
}

private class JvmInfoCommandHandler : NoArgCommandHandler() {
    override fun handleNoArgCommand(context: CommandContext, cmd: String) {
        val runtime = Runtime.getRuntime()
        System.gc()
        context.print("JVM version: ${System.getProperty("java.vm.version")}\n")
        context.print("Free memory: ${runtime.freeMemory()}\n")
        context.print("Total memory: ${runtime.totalMemory()}\n")
        context.print("Maximum memory: ${runtime.maxMemory()}\n")
    }

    override fun description() = "Display information about the running JVM"
}
