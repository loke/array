package com.dhsdevelopments.kap.gui.report2

import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.Engine
import com.dhsdevelopments.kap.Symbol
import com.dhsdevelopments.kap.VariableHolder
import com.dhsdevelopments.kap.chart.DatasetDouble
import com.dhsdevelopments.kap.chart.computeDatasetsFromAPLValue
import com.dhsdevelopments.kap.gui.arrayedit.ArrayEditor
import javafx.application.Platform
import javafx.geometry.Orientation
import javafx.scene.control.CheckBox
import javafx.scene.layout.HBox
import org.controlsfx.control.SegmentedBar
import org.controlsfx.control.spreadsheet.SpreadsheetCell

object SegmentedBarDisplayType : DisplayType {
    override val description get() = "Segmented bar"

    override fun makeConfigurator(client: ReportClient, selection: ArrayEditor.SelectedArea): DisplayConfigurator {
        return SegmentedBarConfigurator(client, selection)
    }
}

class SegmentedBarConfigurator(val client: ReportClient, val selection: ArrayEditor.SelectedArea) : DisplayConfigurator {
    private val panel = HBox()
    private val rowLabelsCheckbox = CheckBox("Row headers").apply { isSelected = true }

    init {
        panel.children.add(rowLabelsCheckbox)
    }

    override fun settingsPanel() = panel

    override fun saveToResult(engine: Engine, storage: VariableHolder, value: APLValue, sym: Symbol) {
        val displayedValue = SegmentedBarDisplayedValue(
            client,
            engine,
            storage,
            sym,
            selection.row,
            selection.column,
            value,
            rowLabelsCheckbox.isSelected)
        displayedValue.processUpdate(value)
        Platform.runLater {
            client.variableList.items.add(displayedValue)
        }
    }
}

class SegmentedBarDisplayedValue(
    client: ReportClient,
    engine: Engine,
    storage: VariableHolder,
    sym: Symbol,
    row: Int,
    col: Int,
    value: APLValue,
    val rowLabels: Boolean
) : DisplayedValue(client, engine, storage, sym, row, col, value) {
    override val displayedRowCount get() = nRows
    override val displayedColCount get() = if (rowLabels) 2 else 1

    override fun processUpdate(v0: APLValue) {
        val (labels, datasets) = computeDatasetsFromAPLValue(v0)
        Platform.runLater {
            value = v0
            updateContentsForValue(labels, datasets)
        }
    }

    private fun updateContentsForValue(labels: Array<String>, datasets: Array<DatasetDouble>) {
        if (!initialised) {
            nRows = datasets.size
            nCols = if (rowLabels) 2 else 1
        } else {
            // TODO: Needs to update nRows unless crashes is the thing we're looking for
        }

        val grid = reportClient.table.grid
        repeat(nRows) { y ->
            val currRow = grid.rows[row + y]
            val updatedCol = if (rowLabels) {
                currRow[col] = HeaderCellType.createCell(HeaderCellContent(this, datasets[y].name), row + y, col)
                col + 1
            } else {
                col
            }
            val content = SegmentedBarCellContent(this)
            val cell = SegmentedBarCellType.createCell(content, row + y, updatedCol)
            content.updateContent(labels, datasets[y])
            currRow[updatedCol] = cell
            grid.rows[row + y] = currRow
        }
    }
}

class SegmentedBarCellContent(owner: SegmentedBarDisplayedValue) : LinkedVariableCellContent(owner) {
    val segmentedBar = SegmentedBar<SegmentedBar.Segment>()

    init {
        segmentedBar.orientation = Orientation.HORIZONTAL
    }

    fun updateContent(labels: Array<String>, dataset: DatasetDouble) {
        segmentedBar.segments.clear()
        repeat(dataset.data.size) { i ->
            val name = labels[i]
            val value = dataset.data[i]
            segmentedBar.segments.add(SegmentedBar.Segment(value, name))
        }
    }

    override fun cellContentString() = ""
}

object SegmentedBarCellType : ReportSpreadsheetCellType<SegmentedBarCellContent>(), CellSpannable {
    fun createCell(content: SegmentedBarCellContent, row: Int, col: Int): SpreadsheetCell {
        val cell = createCellWithType(content, row, col, SegmentedBarCellType)
        cell.graphic = content.segmentedBar
        return cell
    }

    override fun convertValue(value: Any?): SegmentedBarCellContent? {
        return when (value) {
            null -> null
            is SegmentedBarCellContent -> value
            else -> throw IllegalArgumentException("Cannot convert type: ${value}")
        }
    }
}
