package com.dhsdevelopments.kap

external class SharedArrayBuffer(size: Int)

external fun setTimeout(callback: () -> Unit, time: Double)

@Suppress("UNUSED_DESTRUCTURED_PARAMETER_ENTRY", "UnusedVariable")
fun jsObject2(vararg fields: Pair<String, dynamic>): dynamic {
    val res = js("{}")
    fields.forEach { (key, value) ->
        js("res[key] = value")
    }
    return res
}

fun crossOriginIsolatedAndDefined(): Boolean {
    return js("typeof(crossOriginIsolated) !== \"undefined\" && crossOriginIsolated") as Boolean
}
