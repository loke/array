package com.dhsdevelopments.kap.gui2.chart

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.chart.computeDatasetsFromAPLValue
import org.jfree.chart.ChartFactory
import org.jfree.chart.JFreeChart
import org.jfree.data.category.CategoryDataset
import org.jfree.data.category.DefaultCategoryDataset
import javax.swing.SwingUtilities

class Gui2ChartModule : KapModule {
    override val name get() = "gui2-chart"

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("chart")
        engine.registerFunction(ns.internAndExport("line"), Gui2LineChartFunction { ds -> ChartFactory.createLineChart("Chart", null, null, ds) })
        engine.registerFunction(ns.internAndExport("bar"), Gui2LineChartFunction { ds -> ChartFactory.createBarChart("Chart", null, null, ds) })
        engine.registerFunction(ns.internAndExport("plot"), Gui2PlotFunction())
    }
}

class ChartRowColKey(val index: Int, val name: String) : Comparable<ChartRowColKey> {
    override fun compareTo(other: ChartRowColKey) = index.compareTo(other.index)
    override fun toString() = name
}

class Gui2LineChartFunction(val builder: ChartBuilder) : APLFunctionDescriptor {
    inner class Gui2LineChartFunctionImpl(instantiation: FunctionInstantiation) : NoAxisAPLFunction(instantiation) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val (horizontalAxisLabels, datasets) = computeDatasetsFromAPLValue(a, pos)

            SwingUtilities.invokeLater {
                val jfreechartDs = DefaultCategoryDataset()

                val colKeys = ArrayList<ChartRowColKey>()
                horizontalAxisLabels.forEachIndexed { i, label -> colKeys.add(ChartRowColKey(i, label)) }

                datasets.forEachIndexed { rowIndex, ds ->
                    val rowKey = ChartRowColKey(rowIndex, ds.name)
                    ds.data.forEachIndexed { colIndex, v ->
                        jfreechartDs.addValue(v, rowKey, colKeys[colIndex])
                    }
                }

                val chart = builder.make(jfreechartDs)
                displayChartPanel(chart)
            }

            return a
        }
    }

    override fun make(instantiation: FunctionInstantiation) = Gui2LineChartFunctionImpl(instantiation)
}

fun interface ChartBuilder {
    fun make(ds: CategoryDataset): JFreeChart
}
