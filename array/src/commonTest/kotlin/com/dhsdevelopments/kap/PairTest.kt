package com.dhsdevelopments.kap

import kotlin.test.Test

class PairTest : APLTest() {
    ////////////////////////////////////////////////////////////////////
    // Pair test
    ////////////////////////////////////////////////////////////////////

    @Test
    fun pairTwo1DArrays() {
        parseAPLExpression("1 2 3 4 5 ⍮ 10 11").let { result ->
            assert1DArray(
                arrayOf(
                    Inner1D(arrayOf(1, 2, 3, 4, 5)),
                    Inner1D(arrayOf(10, 11))
                ), result)
        }
    }

    @Test
    fun pairTwoAtoms() {
        parseAPLExpression("10 ⍮ 100").let { result ->
            assert1DArray(arrayOf(10, 100), result)
        }
    }

    @Test
    fun pairArrayWithEmptyOnRight() {
        parseAPLExpression("1 2 3 4 5 ⍮ ⍬").let { result ->
            assert1DArray(arrayOf(Inner1D(arrayOf(1, 2, 3, 4, 5)), InnerAPLNull()), result)
        }
    }

    @Test
    fun pairArrayWithEmptyOnLeft() {
        parseAPLExpression("⍬ ⍮ 100 200 300").let { result ->
            assert1DArray(arrayOf(InnerAPLNull(), Inner1D(arrayOf(100, 200, 300))), result)
        }
    }

    @Test
    fun pairArrayWithEmpty() {
        parseAPLExpression("⍬⍮⍬").let { result ->
            assert1DArray(arrayOf(InnerAPLNull(), InnerAPLNull()), result)
        }
    }

    @Test
    fun pairEnclosed() {
        parseAPLExpression("(⊂1 2 3 4 5) ⍮ (⊂100×1+⍳7)").let { result ->
            assertDimension(dimensionsOfSize(2), result)
            result.valueAt(0).let { v0 ->
                assertDimension(emptyDimensions(), v0)
                assert1DArray(arrayOf(1, 2, 3, 4, 5), v0.valueAt(0))
            }
            result.valueAt(1).let { v0 ->
                assertDimension(emptyDimensions(), v0)
                assert1DArray(arrayOf(100, 200, 300, 400, 500, 600, 700), v0.valueAt(0))
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    // Singleton tests
    ////////////////////////////////////////////////////////////////////

    @Test
    fun singletonSimpleList() {
        parseAPLExpression("⍮ 1 2 3").let { result ->
            assert1DArray(arrayOf(Inner1D(arrayOf(1, 2, 3))), result)
        }
    }

    @Test
    fun singletonAtomicValue() {
        parseAPLExpression("⍮ 1").let { result ->
            assert1DArray(arrayOf(1), result)
        }
    }

    @Test
    fun singletonEnclosedValue() {
        parseAPLExpression("⍮⊂1 2 3").let { result ->
            assertDimension(dimensionsOfSize(1), result)
            val v0 = result.valueAt(0)
            assertDimension(emptyDimensions(), v0)
            assert1DArray(arrayOf(1, 2, 3), v0.valueAt(0))
        }
    }
}
