package com.dhsdevelopments.kap

import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertFailsWith

class RandomTest : APLTest() {
    @Test
    fun simpleLong() {
        assertSimpleNumber(100, parseAPLExpressionWithSpecialRandom("?1000"))
    }

    @Test
    fun simpleDouble() {
        assertSimpleDouble(5.5, parseAPLExpressionWithSpecialRandom("?0"))
    }

    @Test
    fun longArray() {
        parseAPLExpressionWithSpecialRandom("?1000 1000 2000 3000").let { result ->
            assert1DArray(arrayOf(100, 100, 200, 300), result)
        }
    }

    @Test
    fun doubleArray() {
        parseAPLExpressionWithSpecialRandom("?100⍴0").let { result ->
            assertDimension(dimensionsOfSize(100), result)
            repeat(100) { i ->
                assertSimpleDouble(5.5, result.valueAt(i))
            }
        }
    }

    @Test
    fun mixedLongAndDoubleInitialLong() {
        parseAPLExpressionWithSpecialRandom("?100 100 0").let { result ->
            assert1DArray(arrayOf(10, 10, InnerDouble(5.5)), result)
        }
    }

    @Test
    fun mixedLongAndDoubleInitialDouble() {
        parseAPLExpressionWithSpecialRandom("?0 0 100 0 300").let { result ->
            assert1DArray(arrayOf(InnerDouble(5.5), InnerDouble(5.5), 10, InnerDouble(5.5), 30), result)
        }
    }

    @Test
    fun floatValueInArg0() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?10 10 1.2")
        }
    }

    @Test
    fun floatValueInArg1() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?0 0 1.2")
        }
    }

    @Test
    fun floatValueInArg2() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?1.2 10 10")
        }
    }

    @Test
    fun charValueInArg0() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?@a 10 10")
        }
    }

    @Test
    fun charValueInArg1() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?10 10 @a")
        }
    }

    @Test
    fun charValueInArg2() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?0 0 @a")
        }
    }

    @Test
    fun charValueInArg3() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?10 10 0 @a")
        }
    }

    @Test
    fun highRankInputLong() {
        parseAPLExpressionWithSpecialRandom("?3 4⍴10×1+⍳12").let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            repeat(3 * 4) { i ->
                assertSimpleNumber(i.toLong() + 1, result.valueAt(i))
            }
        }
    }

    @Test
    fun highRankInputDouble() {
        parseAPLExpressionWithSpecialRandom("?3 4⍴0").let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            repeat(3 * 4) { i ->
                assertSimpleDouble(5.5, result.valueAt(i))
            }
        }
    }

    @Test
    fun highRankInputMixedInitialLong() {
        parseAPLExpressionWithSpecialRandom("?3 4⍴(10×1+⍳10),0 0").let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            repeat(10) { i ->
                assertSimpleNumber(i.toLong() + 1, result.valueAt(i))
            }
            assertSimpleDouble(5.5, result.valueAt(10))
            assertSimpleDouble(5.5, result.valueAt(11))
        }
    }

    @Test
    fun highRankInputMixedInitialDouble() {
        parseAPLExpressionWithSpecialRandom("?3 4⍴0 0,(10×1+⍳10)").let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            assertSimpleDouble(5.5, result.valueAt(0))
            assertSimpleDouble(5.5, result.valueAt(1))
            repeat(10) { i ->
                assertSimpleNumber(i.toLong() + 1, result.valueAt(i + 2))
            }
        }
    }

    @Test
    fun nestedArrays0() {
        parseAPLExpressionWithSpecialRandom("?(10 10) (20 20)").let { result ->
            assert1DArray(
                arrayOf(
                    Inner1D(arrayOf(1, 1)),
                    Inner1D(arrayOf(2, 2))),
                result)
        }
    }

    @Test
    fun nestedArrays1() {
        parseAPLExpressionWithSpecialRandom("?(0 0) (100 200)").let { result ->
            assert1DArray(
                arrayOf(
                    Inner1D(arrayOf(InnerDouble(5.5), InnerDouble(5.5))),
                    Inner1D(arrayOf(10, 20))),
                result)
        }
    }

    @Test
    fun nestedArrays2() {
        parseAPLExpressionWithSpecialRandom("? 2 2 ⍴ 0 0 (3 2 ⍴ 100 200 300 400 500 600) (1000 2000 (2 4 ⍴ 110 120 0 0 130 140 150 160))").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertSimpleDouble(5.5, result.valueAt(0))
            assertSimpleDouble(5.5, result.valueAt(1))
            result.valueAt(2).let { v ->
                assertDimension(dimensionsOfSize(3, 2), v)
                assertArrayContent(arrayOf(10, 20, 30, 40, 50, 60), v)
            }
            result.valueAt(3).let { v ->
                assertDimension(dimensionsOfSize(3), v)
                assertSimpleNumber(100, v.valueAt(0))
                assertSimpleNumber(200, v.valueAt(1))
                val v0 = v.valueAt(2)
                assertDimension(dimensionsOfSize(2, 4), v0)
                assertArrayContent(arrayOf(11, 12, InnerDouble(5.5), InnerDouble(5.5), 13, 14, 15, 16), v0)
            }
        }
    }

    @Test
    fun negativeScalar() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?¯6")
        }
    }

    @Test
    fun negativeScalarWithLong() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?10 10 ¯6")
        }
    }

    @Test
    fun negativeScalarWithDouble() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?0 0 ¯6")
        }
    }

    @Test
    fun negativeScalarWithMixed() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?0 10 10 ¯6")
        }
    }

    @Test
    fun simpleBigintFitsInLong() {
        assertBigIntOrLong(10, parseAPLExpressionWithSpecialRandom("? int:asBigint 100"))
    }

    @Test
    fun simpleBigintZero() {
        assertSimpleDouble(5.5, parseAPLExpressionWithSpecialRandom("? int:asBigint 0"))
    }

    @Test
    fun bigintFitsInLongInLongArray0() {
        parseAPLExpressionWithSpecialRandom("?100 200 (int:asBigint 300)").let { result ->
            assert1DArray(arrayOf(10, 20, InnerBigIntOrLong(30)), result)
        }
    }

    @Test
    fun bigintFitsInLongInLongArray1() {
        parseAPLExpressionWithSpecialRandom("?0 0 (int:asBigint 0)").let { result ->
            assert1DArray(arrayOf(InnerDouble(5.5), InnerDouble(5.5), InnerDouble(5.5)), result)
        }
    }

    @Test
    fun bigintFitsInLongInLongArray2() {
        parseAPLExpressionWithSpecialRandom("?0 0 100 (int:asBigint 0)").let { result ->
            assert1DArray(arrayOf(InnerDouble(5.5), InnerDouble(5.5), 10, InnerDouble(5.5)), result)
        }
    }

    @Test
    fun bigintFitsInLongInLongArray3() {
        parseAPLExpressionWithSpecialRandom("?0 0 100 (int:asBigint 200)").let { result ->
            assert1DArray(arrayOf(InnerDouble(5.5), InnerDouble(5.5), 10, InnerBigIntOrLong(20)), result)
        }
    }

    @Test
    fun largeBigint() {
        assertFailsWith<APLEvalException> {
            parseAPLExpressionWithSpecialRandom("?100000000000000000000000000000000000000")
        }
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpressionWithSpecialRandom("?¯100000000000000000000000000000000000000")
        }
    }

    private fun parseAPLExpressionWithSpecialRandom(src: String): APLValue {
        val engine = makeEngine()
        engine.random = TestRandom()
        return engine.parseAndEval(StringSourceLocation(src))
    }

    private class TestRandom : Random() {
        override fun nextLong(until: Long): Long {
            return until / 10
        }

        override fun nextDouble(): Double {
            return 5.5
        }

        override fun nextBits(bitCount: Int): Int {
            return 0
        }
    }
}
