package com.dhsdevelopments.kap.htmlconverter

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.mpbignum.BigInt
import com.dhsdevelopments.mpbignum.of
import com.fleeksoft.ksoup.nodes.Document
import com.fleeksoft.ksoup.nodes.Element

private fun parseAsHtmlTable(doc: Document, n: Int = 0): Pair<List<List<String>>, List<String>?>? {
    val body = doc.body()
    val tableNodes = body.select("table")
    if (n < 0 || n >= tableNodes.size) {
        return null
    }
    val tableNode = tableNodes[n]
    val tbodyElements = tableNode.getElementsByTag("tbody")
    if (tbodyElements.size != 1) return null
    val tbody = tbodyElements[0]
    val result = ArrayList<List<String>>()
    tbody.children().forEach { row ->
        if (row.tagName() == "tr") {
            val rowElements = ArrayList<String>()
            row.children().forEach { n ->
                if (n.tagName() == "td" || n.tagName() == "th")
                    rowElements.add(n.text())
            }
            result.add(rowElements)
        }
    }

    val h = parseHeaders(tableNode)
    val headers = if (h != null && h.size == result.first().size) h else null
    return Pair(result, headers)
}

private fun parseHeaders(tableNode: Element): List<String>? {
    val theadElements = tableNode.getElementsByTag("thead")
    if (theadElements.size != 1) return null
    val trElements = theadElements[0].children()
    if (trElements.isEmpty()) return null
    val thElements = trElements[0]
    val result = ArrayList<String>()
    thElements.children().forEach { n ->
        if (n.tagName() == "td" || n.tagName() == "th") {
            result.add(n.text())
        }
    }
    return result
}

fun htmlTableToArray(doc: Document, n: Int = 0): APLValue? {
    val (content, headers) = parseAsHtmlTable(doc, n) ?: return null
    if (content.isEmpty()) return null
    val numRows = content.size
    val numCols = content.maxValueBy(List<String>::size)
    if (numCols == 0) return null
    val list = ArrayList<APLValue>()
//    val format = NumberFormat.getInstance(Locale.UK)
    content.forEach { rowData ->
        rowData.forEach { value ->
//            val parsed = try {
//                when (val n = format.parse(value.trim())) {
//                    is Short -> n.toLong().makeAPLNumber()
//                    is Int -> n.makeAPLNumber()
//                    is Long -> n.makeAPLNumber()
//                    is Float -> n.toDouble().makeAPLNumber()
//                    is Double -> n.makeAPLNumber()
//                    else -> null
//                }
//            } catch (e: java.text.ParseException) {
//                null
//            }
            list.add(parseStringToAPLValue(value))
        }
        repeat(numCols - rowData.size) {
            list.add(APLLONG_0)
        }
    }
    val res = APLArrayList(dimensionsOfSize(numRows, numCols), list)
    return if (headers == null) {
        res
    } else {
        MetadataOverrideArray.makeWithMergedLabels(res, listOf(null, headers.map { s -> AxisLabel(s) }))
    }
}

private interface ValueParser {
    fun parse(s: String): APLValue?
}

private object NumberWithThousandsSeparator : ValueParser {
    private val PATTERN = "^(-?)((?:[0-9]+,)*[0-9]+)(?:\\.([0-9]+))?.*$".toRegex()

    override fun parse(s: String): APLValue? {
        val matchResult = PATTERN.matchEntire(s) ?: return null
        val neg = if (matchResult.groups.get(1)?.value == "-") "-" else ""
        val intPart = matchResult.groups.get(2)!!.value.filter { ch -> ch in '0'..'9' }
        val decimalPart = matchResult.groups.get(3)?.value
        return if (decimalPart == null) {
            BigInt.of("${neg}${intPart}").makeAPLNumberWithReduction()
        } else {
            "${neg}${intPart}.${decimalPart}".toDouble().makeAPLNumber()
        }
    }
}

private val VALUE_PARSERS = listOf(NumberWithThousandsSeparator)

private fun parseStringToAPLValue(text: String): APLValue {
    val trimmed = text.trim()
    for (parser in VALUE_PARSERS) {
        val v = parser.parse(trimmed)
        if (v != null) {
            return v
        }
    }
    return APLString.make(trimmed)
}
