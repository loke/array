package com.dhsdevelopments.kap

import kotlin.test.Test

class TrimTest : APLTest() {
    @Test
    fun trimLeft() {
        assertString("foo      ", parseAPLExpression("s:trimLeft \"   foo      \"", withStandardLib = true))
        assertString("foo  test    ", parseAPLExpression("s:trimLeft \"  foo  test    \"", withStandardLib = true))
        assertString("foo", parseAPLExpression("s:trimLeft \"foo\"", withStandardLib = true))
        assertString("", parseAPLExpression("s:trimLeft \"    \"", withStandardLib = true))
        assertString("", parseAPLExpression("s:trimLeft \"\"", withStandardLib = true))
    }

    @Test
    fun trimRight() {
        assertString("   foo", parseAPLExpression("s:trimRight \"   foo      \"", withStandardLib = true))
        assertString("  foo  test", parseAPLExpression("s:trimRight \"  foo  test    \"", withStandardLib = true))
        assertString("foo", parseAPLExpression("s:trimRight \"foo\"", withStandardLib = true))
        assertString("", parseAPLExpression("s:trimRight \"    \"", withStandardLib = true))
        assertString("", parseAPLExpression("s:trim \"    \"", withStandardLib = true))
    }

    @Test
    fun trimLeftRight() {
        assertString("foo", parseAPLExpression("s:trim \"   foo      \"", withStandardLib = true))
        assertString("foo  test", parseAPLExpression("s:trim \"  foo  test    \"", withStandardLib = true))
        assertString("foo", parseAPLExpression("s:trim \"foo\"", withStandardLib = true))
        assertString("", parseAPLExpression("s:trim \"    \"", withStandardLib = true))
        assertString("", parseAPLExpression("s:trim \"\"", withStandardLib = true))
    }
}
