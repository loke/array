package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.chart.FunctionGraphDatasetList
import com.dhsdevelopments.kap.chart.computeFunctionGraphDataset
import com.dhsdevelopments.kap.chart.makeFunctionList
import com.dhsdevelopments.kap.chart.plot2DParamsFromAPLValue
import kotlin.math.ceil
import kotlin.math.log10

class JsPlotFunction() : APLFunctionDescriptor {
    inner class JsPlotFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val params = plot2DParamsFromAPLValue(a, pos)
            val (functionList, labelList) = makeFunctionList(b, pos)
            val datasets = computeFunctionGraphDataset(context, params, functionList, labelList, pos, numPoints = 400)

            val data = makeGraphData(datasets)
            val output = jsObject2(
                "messageType" to ADDITIONAL_OUTPUT_PLOT,
                "plotData" to data)
            val module = context.engine.findModule<JsChartModule>() ?: throw IllegalStateException("Chart module not found")
            module.sendMessageFn(output)
            return APLNullValue
        }

        private fun makeGraphData(datasets: FunctionGraphDatasetList): Any {
            val datasetsResult = js("[]")
            datasets.xPositions
            datasets.datasets.forEach { ds ->
                val list = js("[]")
                ds.data.forEach { point ->
                    list.push(point)
                }
                datasetsResult.push(
                    jsObject2(
                        "label" to ds.label,
                        "data" to list))
            }
            val labelsList = makeLabelsList(datasets)
            val data = jsObject2(
                "labels" to labelsList,
                "datasets" to datasetsResult,
                "additionalError" to datasets.additionalError?.message)
            return data
        }

        private fun makeLabelsList(datasets: FunctionGraphDatasetList): dynamic {
            val displayDigits = log10(((datasets.xPositions.last() - datasets.xPositions[0])) / datasets.xPositions.size)

            @Suppress("UNUSED_VARIABLE")
            val decimals = if (displayDigits >= 0) 0 else ceil(-displayDigits)

            val result = js("[]")
            datasets.xPositions.forEach { d ->
                @Suppress("UNUSED_VARIABLE")
                val n0 = d
                val s = js("n0.toFixed(decimals)")
                result.push(s)
            }
            return result
        }
    }

    override fun make(instantiation: FunctionInstantiation) = JsPlotFunctionImpl(instantiation)
}
