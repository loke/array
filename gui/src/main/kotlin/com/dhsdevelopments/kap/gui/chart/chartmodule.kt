package com.dhsdevelopments.kap.gui.chart

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.chart.DatasetDouble
import com.dhsdevelopments.kap.chart.computeDatasetsFromAPLValue
import com.dhsdevelopments.kap.chart.computeScatterDatasetFromAPLValue
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.chart.XYChart

class LineChartFunction(val chartType: ChartType<LineChartData>) : APLFunctionDescriptor {
    class LineChartFunctionImpl(val chartType: ChartType<LineChartData>, pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val (horizontalAxisLabels, datasets) = computeDatasetsFromAPLValue(a, pos)
            Platform.runLater {
                ChartController.openWindowWithData(chartType, LineChartData(horizontalAxisLabels, datasets))
            }
            return a
        }
    }

    override fun make(instantiation: FunctionInstantiation) = LineChartFunctionImpl(chartType, instantiation)
}

fun makeXYChartDataFromDatasets(
    labels: Array<String>,
    datasets: Array<DatasetDouble>
): ObservableList<XYChart.Series<String, Number>>? {
    val list = FXCollections.observableArrayList<XYChart.Series<String, Number>>()
    datasets.forEach { dataset ->
        val series = FXCollections.observableArrayList<XYChart.Data<String, Number>>()
        dataset.data.forEachIndexed { i, v ->
            series.add(XYChart.Data(labels[i], v))
        }
        list.add(XYChart.Series(dataset.name, series))
    }
    return list
}

class ScatterChartFunction : APLFunctionDescriptor {
    class ScatterChartFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val dataset = computeScatterDatasetFromAPLValue(a, pos)
            Platform.runLater {
                ChartController.openWindowWithData(ScatterChartType, ScatterData(dataset))
            }
            return a
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ScatterChartFunctionImpl(instantiation)
}

class JavaChartModule : KapModule {
    override val name get() = "javachart"

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("chart")
        engine.registerFunction(ns.internAndExport("line"), LineChartFunction(LineChartType))
        engine.registerFunction(ns.internAndExport("bar"), LineChartFunction(BarChartType))
        engine.registerFunction(ns.internAndExport("pie"), LineChartFunction(PieChartType))
        engine.registerFunction(ns.internAndExport("scatter"), ScatterChartFunction())
        engine.registerFunction(ns.internAndExport("plot"), PlotFunction())
    }
}
