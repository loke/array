package com.dhsdevelopments.kap.gui.objview

import com.dhsdevelopments.kap.APLRational
import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.FormatStyle
import javafx.scene.layout.Pane
import javafx.scene.shape.Line
import javafx.scene.text.Text

object RationalNumberRendererHandler : ObjectRendererHandler {
    override fun canRender(value: APLValue): APLValueRenderer? {
        return if (value is APLRational) {
            return RationalNumberRenderer
        } else {
            null
        }
    }
}

private object RationalNumberRenderer : APLValueRenderer {
    override fun makeNode(value: APLValue) = RationalNumberRendererNode(value as APLRational)
    override fun nodeAsString(value: APLValue) = value.formatted(FormatStyle.PLAIN)
    override fun preferString(value: APLValue) = false
}

private class RationalNumberRendererNode(value: APLRational) : Pane() {
    init {
        val n = value.value
        val lineSpacing = 3.0

        val numText = Text(n.numerator.toString())
        val numBounds = numText.layoutBounds

        val denText = Text(n.denominator.toString())
        val denBounds = denText.layoutBounds

        val paneWidth = kotlin.math.max(numBounds.width, denBounds.width)

        numText.x = (paneWidth - numBounds.width) / 2
        val numYPos = numText.baselineOffset
        numText.y = numYPos

        val lineY = numYPos + lineSpacing

        denText.x = (paneWidth - denBounds.width) / 2
        val denYPos = lineY + denText.baselineOffset + lineSpacing
        denText.y = denYPos

        children.add(numText)
        children.add(Line(0.0, lineY, paneWidth, lineY))
        children.add(denText)

//        width = paneWidth
//        height = denYPos
    }
}
