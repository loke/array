package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.json.*
import kotlin.test.Test
import kotlin.test.assertEquals

class JsonEncoderTest {
    @Test
    fun encodeSimpleNumber() {
        assertEquals("10.05", encodeToString(JsonNumber(10.05)))
    }

    @Test
    fun encodeSimpleString() {
        assertEquals("\"abc\"", encodeToString(JsonString("abc")))
    }

    @Test
    fun encodeSimpleStringWithSpecialChars() {
        assertEquals("\"abc\\ndef\\rghi\"", encodeToString(JsonString("abc\ndef\rghi")))
    }

    @Test
    fun encodeSimpleBooleans() {
        assertEquals("true", encodeToString(JsonBoolean(true)))
        assertEquals("false", encodeToString(JsonBoolean(false)))
    }

    @Test
    fun encodeNull() {
        assertEquals("null", encodeToString(JsonNull))
    }

    @Test
    fun encodeJsonNumericArray() {
        val json = JsonArray((1..8).map(::JsonNumber))
        assertEquals("[1,2,3,4,5,6,7,8]", encodeToString(json))
    }

    @Test
    fun encodeJsonStringArray() {
        val json = JsonArray(listOf(JsonString("a"), JsonString("b"), JsonString("foo"), JsonString("test"), JsonString("test2")))
        assertEquals("[\"a\",\"b\",\"foo\",\"test\",\"test2\"]", encodeToString(json))
    }

    @Test
    fun encodeEmptyArray() {
        assertEquals("[]", encodeToString(JsonArray(emptyList())))
    }

    @Test
    fun encodeSingleElementArray() {
        assertEquals("[1]", encodeToString(JsonArray(listOf(JsonNumber(1)))))
    }

    @Test
    fun encodeNestedArrays() {
        val json = JsonArray(
            listOf(
                JsonArray(listOf(JsonNumber(100))),
                JsonArray(listOf(JsonString("a"), JsonString("b"), JsonString("foo"), JsonString("test"), JsonString("test2")))))
        assertEquals("[[100],[\"a\",\"b\",\"foo\",\"test\",\"test2\"]]", encodeToString(json))
    }

    @Test
    fun encodeSimpleObject() {
        val json = JsonObject(
            listOf(
                "foo" to JsonNumber(1),
                "bar" to JsonString("test")))
        assertEquals("{\"foo\":1,\"bar\":\"test\"}", encodeToString(json))
    }

    @Test
    fun encodeEmptyObject() {
        assertEquals("{}", encodeToString(JsonObject(emptyList())))
    }

    @Test
    fun encodeSingleElementObject() {
        assertEquals("{\"abc\":100}", encodeToString(JsonObject(listOf("abc" to JsonNumber(100)))))
    }

    @Test
    fun encodeNestedObject() {
        val json = JsonObject(
            listOf(
                "foo" to JsonObject(
                    listOf(
                        "a" to JsonString("test"),
                        "b" to JsonNumber(123))),
                "bar" to JsonArray(
                    listOf(
                        JsonString("a"), JsonString("b"), JsonString("c"), JsonString("d"), JsonString("e"), JsonString("f")))))
        assertEquals("{\"foo\":{\"a\":\"test\",\"b\":123},\"bar\":[\"a\",\"b\",\"c\",\"d\",\"e\",\"f\"]}", encodeToString(json))
    }

    @Test
    fun encodeLargeObjectWithStringFields() {
        val pairs = (0 until 100).map { i -> "x${i}" to "result${i}" }
        val json = JsonObject(pairs.map { (k, v) -> k to JsonString(v) })
        val expected = "{" + pairs.joinToString(",") { (k, v) -> "\"${k}\":\"${v}\"" } + "}"
        assertEquals(expected, encodeToString(json))
    }

    @Test
    fun encodeArrayOfObjects() {
        val json = JsonArray(
            listOf(
                JsonObject(
                    listOf(
                        "a" to JsonNumber(100),
                        "b" to JsonNumber(200))),
                JsonObject(
                    listOf(
                        "c" to JsonString("aa"),
                        "d" to JsonString("bb")
                    )),
                JsonObject(
                    listOf(
                        "e" to JsonBoolean(false),
                        "f" to JsonBoolean(true),
                        "g" to JsonString("foo")))))
        assertEquals("[{\"a\":100,\"b\":200},{\"c\":\"aa\",\"d\":\"bb\"},{\"e\":false,\"f\":true,\"g\":\"foo\"}]", encodeToString(json))
    }

    @Test
    fun encodeHighCodepoints() {
        val json = JsonString("\uD835\uDC9Fxyz")
        assertEquals("\"\uD835\uDC9Fxyz\"", encodeToString(json))
    }

    private fun encodeToString(element: JsonElement): String {
        val buf = StringBuilder()
        makeJsonEncoder().encodeJsonToStream(element, AppendableCharConsumer(buf))
        return buf.toString()
    }
}
