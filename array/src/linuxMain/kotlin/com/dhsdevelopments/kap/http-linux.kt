package com.dhsdevelopments.kap

import kotlinx.cinterop.*
import libcurl.*
import platform.posix.size_t

class HttpResultLinux(
    override val code: Long,
    override val content: String,
    override val headers: Map<String, List<String>>
) : HttpResult

@OptIn(ExperimentalForeignApi::class)
class CurlTask {
    val buf = StringBuilder()
    val responseHeaders = ArrayList<Pair<String, String>>()

    fun httpRequest(url: String, methodname: String, postContent: ByteArray?, headers: Map<String, String>?): HttpResultLinux {
        val curl = curl_easy_init() ?: throw IllegalStateException("Initialisation error in libcurl")
        val taskStableRef = StableRef.create(this)
        try {
            memScoped {
                curl_easy_setopt(curl, CURLOPT_URL, url)
                curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L)
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, staticCFunction(::handleWrite))
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, taskStableRef.asCPointer())
                curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, methodname)
                curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, staticCFunction(::handleHeader))
                curl_easy_setopt(curl, CURLOPT_HEADERDATA, taskStableRef.asCPointer())
                if (postContent != null) {
                    val buf = allocArray<ByteVar>(postContent.size)
                    for (i in postContent.indices) {
                        buf[i] = postContent[i]
                    }
                    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, buf)
                    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, postContent.size.toLong())
                }
                val headerList = initHeaders(headers)
                try {
                    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerList)
                    val result = curl_easy_perform(curl)
                    if (result == CURLE_OK) {
                        return processResult(curl)
                    } else {
                        throw Exception("Error handling not implemented")
                    }
                } finally {
                    curl_slist_free_all(headerList)
                }
            }
        } finally {
            curl_easy_cleanup(curl)
            taskStableRef.dispose()
        }
    }

    private fun initHeaders(headers: Map<String, String>?): CPointer<curl_slist>? {
        var list: CPointer<curl_slist>? = null
        if (headers != null) {
            headers.entries.forEach { (key, value) ->
                list = curl_slist_append(list, "${key}: ${value}")
            }
        }
        return list
    }

    private fun escapeString(curl: COpaquePointer, s: String): String {
        val result = curl_easy_escape(curl, s, 0) ?: throw IllegalStateException("Unable to escape string")
        try {
            return result.toKString()
        } finally {
            curl_free(result)
        }
    }

    private fun processResult(curl: COpaquePointer): HttpResultLinux {
        memScoped {
            val codeResult = alloc<UIntVar>()
            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, codeResult)
//
//            val headers = HashMap<String, ArrayList<String>>()
//            var h = curl_easy_nextheader(curl, CURLH_HEADER.toUInt(), -1, null)
//            while (h != null) {
//                val headerRef = h.get(0)
//                val key = headerRef.name
//                val value = headerRef.value
//                if (key == null || value == null) {
//                    println("Invalid values for headers: key=${key}, value=${value}")
//                } else {
//                    headers.getOrPut(key.toKString()) { ArrayList() }.add(value.toKString())
//                }
//                h = curl_easy_nextheader(curl, CURLH_HEADER.toUInt(), -1, h)
//            }
            val headers = HashMap<String, ArrayList<String>>()
            responseHeaders.forEach { (k, v) ->
                headers.getOrPut(k) { ArrayList() }.add(v)
            }
            return HttpResultLinux(codeResult.value.toLong(), buf.toString(), headers)
        }
    }
}

@OptIn(ExperimentalForeignApi::class)
private fun handleWrite(buffer: CPointer<ByteVar>?, size: size_t, nitems: size_t, userdata: COpaquePointer?): size_t {
    if (buffer == null) return 0u
    if (userdata != null) {
        val bytesResult = buffer.readBytes((size * nitems).toInt())
        val data = bytesResult.toKString()
        val curlTask = userdata.asStableRef<CurlTask>().get()
        curlTask.buf.append(data)
    }
    return size * nitems
}

@OptIn(ExperimentalForeignApi::class)
private fun handleHeader(buffer: CPointer<ByteVar>?, size: size_t, nitems: size_t, userdata: COpaquePointer?): size_t {
    if (userdata != null && buffer != null) {
        val nameBuf = ArrayList<Byte>()
        // Ensure that the line ends with crlf
        val bufsizWithCrlf = (size * nitems).toLong()
        val bufsiz = if (bufsizWithCrlf >= 2 && buffer[bufsizWithCrlf - 2] == '\r'.code.toByte() && buffer[bufsizWithCrlf - 1] == '\n'.code.toByte()) {
            bufsizWithCrlf - 2
        } else {
            KapLogger.d {
                memScoped {
                    val newBuffer = allocArray<ByteVar>((size * nitems * 1U).toInt())
                    repeat((size * nitems).toInt()) { i ->
                        newBuffer[i] = buffer[i]
                    }
                    newBuffer[(size * nitems).toInt()] = 0
                    "Header buffer did not end with crlf: ${newBuffer.toKString()}"
                }
            }
            bufsizWithCrlf
        }
        // Iterate over header until a ':' is found
        var i = 0L
        while (i < bufsiz) {
            val b = buffer[i++]
            if (b == ':'.code.toByte()) {
                break
            }
            nameBuf.add(b)
        }
        val name = nameBuf.toByteArray().toKString()
        // Skip whitespace
        while (i < bufsiz && buffer[i] == ' '.code.toByte()) {
            i++
        }
        val valueBuf = ArrayList<Byte>()
        while (i < bufsiz) {
            valueBuf.add(buffer[i++])
        }
        val value = valueBuf.toByteArray().toKString()
        val curlTask = userdata.asStableRef<CurlTask>().get()
        curlTask.responseHeaders.add(Pair(name, value))
    }
    return size * nitems
}

actual fun httpRequest(engine: Engine, httpRequestData: HttpRequestData, pos: Position?): HttpResult {
    val task = CurlTask()
    return when (httpRequestData.method) {
        HttpMethod.GET -> task.httpRequest(httpRequestData.url, "GET", null, httpRequestData.headers)
        HttpMethod.POST -> task.httpRequest(httpRequestData.url, "POST", httpRequestData.data ?: ByteArray(0), httpRequestData.headers)
        HttpMethod.PUT -> task.httpRequest(httpRequestData.url, "PUT", httpRequestData.data ?: ByteArray(0), httpRequestData.headers)
        HttpMethod.DELETE -> task.httpRequest(httpRequestData.url, "DELETE", null, httpRequestData.headers)
        HttpMethod.PATCH -> task.httpRequest(httpRequestData.url, "PATCH", httpRequestData.data ?: ByteArray(0), httpRequestData.headers)
        HttpMethod.HEAD -> task.httpRequest(httpRequestData.url, "HEAD", null, httpRequestData.headers)
        HttpMethod.OPTIONS -> task.httpRequest(httpRequestData.url, "OPTIONS", null, httpRequestData.headers)
    }
}

actual fun httpRequestCallback(
    engine: Engine,
    httpRequestData: HttpRequestData,
    pos: Position?,
    callback: (HttpResult) -> Either<APLValue, Exception>
) {
    TODO("not implemented")
}
