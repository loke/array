package com.dhsdevelopments.kap.clientweb2.webworkerwasmjs

import com.dhsdevelopments.kap.Engine
import com.dhsdevelopments.kap.KapModule

class JsChartModule(val sendMessageFn: (JsAny) -> Unit) : KapModule {
    override val name: String get() = "jschart"

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("chart")
    }
}
