package com.dhsdevelopments.kap.chart

import com.dhsdevelopments.kap.*

class DatasetDouble(val name: String, val data: DoubleArray)

fun computeDatasetsFromAPLValue(a: APLValue, pos: Position? = null): Pair<Array<String>, Array<DatasetDouble>> {
    val labels = a.metadata.labels
    val d = a.dimensions
    val res = when (d.size) {
        1 -> Pair(
            Array(d[0]) { i -> labels?.labels?.get(0)?.get(i)?.title ?: i.toString() },
            arrayOf(DatasetDouble("unnamed", DoubleArray(d[0]) { i -> a.valueAtCoerceToDouble(i, pos) })))
        2 -> {
            val axis0Labels = labels?.labels?.get(0)
            val axis1Labels = labels?.labels?.get(1)
            val labelNames = Array(d[1]) { i -> axis1Labels?.get(i)?.title ?: i.toString() }
            val data = Array(d[0]) { i ->
                val offset = i * d[1]
                val graphLabel = axis0Labels?.get(i)?.title ?: i.toString()
                DatasetDouble(graphLabel, DoubleArray(d[1]) { i2 -> a.valueAtCoerceToDouble(offset + i2, pos) })
            }
            Pair(labelNames, data)
        }
        else -> throwAPLException(InvalidDimensionsException("chart data must be 1- or 2-dimensional", pos))
    }
    return res
}

class ScatterSeries(val name: String, val points: List<XYPoint>) {
    class XYPoint(val x: Double, val y: Double, val label: String? = null)
}

fun computeScatterDatasetFromAPLValue(a: APLValue, pos: Position? = null): List<ScatterSeries> {
    val d = a.dimensions
    if (d.size != 2) {
        throwAPLException(InvalidDimensionsException("Scatter plot data must be a 2-dimensional array", pos))
    }

    val labels = a.metadata.labels?.labels?.get(0)

    val nRows = d[0]
    val nCols = d[1]
    val result = when (nCols) {
        2 -> {
            val res = (0 until nRows).map { i ->
                val x = a.valueAtCoerceToDouble(i * 2, pos)
                val y = a.valueAtCoerceToDouble(i * 2 + 1, pos)
                ScatterSeries.XYPoint(x, y, labels?.get(i)?.title)
            }
            listOf(ScatterSeries("default", res))
        }
        3 -> {
            val groupMap = HashMap<APLValue.APLValueKey, Pair<String, MutableList<ScatterSeries.XYPoint>>>()
            (0 until nRows).forEach { i ->
                val x = a.valueAtCoerceToDouble(i * 3, pos)
                val y = a.valueAtCoerceToDouble(i * 3 + 1, pos)
                val groupValue = a.valueAt(i * 3 + 2)
                val (_, l) = groupMap.getOrPut(groupValue.makeTypeQualifiedKey()) { Pair(groupValue.formatted(FormatStyle.PLAIN), ArrayList()) }
                l.add(ScatterSeries.XYPoint(x, y, labels?.get(i)?.title))
            }
            groupMap.values.map { (k, v) -> ScatterSeries(k, v) }
        }
        else -> throwAPLException(InvalidDimensionsException("Scatter plot data must have 2 or 3 columns", pos))
    }
    return result
}

class SurfaceSeries(val width: Int, val height: Int, val data: DoubleArray)

fun computeSurfaceDatasetFromAPLValue(a: APLValue, pos: Position? = null): SurfaceSeries {
    val d = a.dimensions
    if (d.size != 2) {
        throwAPLException(InvalidDimensionsException("chart data must be 2-dimensional", pos))
    }
    val nRows = d[0]
    val nCols = d[1]
    return SurfaceSeries(nCols, nRows, DoubleArray(nRows * nCols) { i -> a.valueAtCoerceToDouble(i, pos) })
}
