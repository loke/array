package com.dhsdevelopments.kap

import kotlin.test.*

class ListTest : APLTest() {
    @Test
    fun parseList() {
        val result = parseAPLExpression("1;2;3")
        assertListContent(listOf(1, 2, 3), result)
    }

    @Test
    fun parseListWithStatements() {
        val result = parseAPLExpression("1+2;2+3;3 ◊ 5+1+1;6+1+1;7;8")
        assertListContent(listOf(7, 8, 7, 8), result)
    }

    @Test
    fun parseWithExpressions() {
        val result = parseAPLExpression("1+2;3+4")
        assertListContent(listOf(3, 7), result)
    }

    @Test
    fun fromList() {
        parseAPLExpression("fromList (1;2;3)").let { result ->
            assertDimension(dimensionsOfSize(3), result)
            assertArrayContent(arrayOf(1, 2, 3), result)
        }
    }

    @Test
    fun fromListWithSymbol() {
        parseAPLExpression("⌷ (1;2;3)").let { result ->
            assertDimension(dimensionsOfSize(3), result)
            assertArrayContent(arrayOf(1, 2, 3), result)
        }
    }

    @Test
    fun toList() {
        parseAPLExpression("toList 1 2 3").let { result ->
            assertListContent(listOf(1, 2, 3), result)
        }
    }

    @Test
    fun toListWithScalarNumber() {
        parseAPLExpression("toList 1").let { result ->
            assertListContent(listOf(1), result)
        }
    }

    @Test
    fun toListWithEnclosedValue() {
        parseAPLExpression("toList ⊂\"foo\"").let { result ->
            assertListContent(listOf("foo"), result)
        }
    }

    @Test
    fun toListWithSymbol() {
        parseAPLExpression("≬ 1 2 3").let { result ->
            assertListContent(listOf(1, 2, 3), result)
        }
    }

    @Test
    fun toListWithSymbolEmpty() {
        parseAPLExpression("≬ ⍬").let { result ->
            assertListContent(listOf(), result)
        }
    }

    @Test
    fun toListWithSymbolScalar() {
        parseAPLExpression("≬ 1").let { result ->
            assertListContent(listOf(1), result)
        }
    }

    @Test
    fun toListWithListArgument() {
        parseAPLExpression("toList (1;2;3)").let { result ->
            assertIs<APLList>(result)
            assertListContent(listOf(1, 2, 3), result.listElement(0))
        }
    }

    @Test
    fun reshapeList() {
        parseAPLExpression("2 2 ⍴ (1;2)").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            repeat(4) { i ->
                assertListContent(listOf(1, 2), result.valueAt(i))
            }
        }
    }

    @Test
    fun testCollapseList() {
        val (res, out) = parseAPLExpressionWithOutput("comp toList (io:print¨ 2 3) 6", collapse = false)
        assertIs<APLList>(res)
        assertEquals("23", out)
    }

    @Test
    fun functionExpressionsInListShouldFail0() {
        assertFailsWith<ParseException> {
            parseAPLExpression("+ ; 3 ; 4 ; 5")
        }
    }

    @Test
    fun functionExpressionsInListShouldFail1() {
        assertFailsWith<ParseException> {
            parseAPLExpression("1 ; + ; 2")
        }
    }

    @Test
    fun testEnsureListSize() {
        val list = APLList(listOf(APLLONG_0, APLLONG_0))
        assertEquals(2, list.ensureListSize(null, 3, 1).size)
        assertEquals(2, list.ensureListSize(null, 2, null).size)
        assertFailsWith<APLEvalException> {
            list.ensureListSize(null, 10, 3)
        }
        assertFailsWith<APLEvalException> {
            list.ensureListSize(null, 1, null)
        }
        assertFailsWith<APLEvalException> {
            list.ensureListSize(null, 3, null)
        }
    }

    @Test
    fun testUnderToList() {
        val src =
            """
            |addOneToListElements ⇐ { toList 1+fromList ⍵ }
            |{ addOneToListElements ⍵ }⍢toList 100 200
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assert1DArray(arrayOf(101, 201), result)
        }
    }

    @Test
    fun testUnderFromList() {
        parseAPLExpression("(1+)⍢fromList (10 ; 20 ; 30)").let { result ->
            assertIs<APLList>(result)
            assertEquals(3, result.listSize())
            assertSimpleNumber(11, result.listElement(0))
            assertSimpleNumber(21, result.listElement(1))
            assertSimpleNumber(31, result.listElement(2))
        }
    }

    @Test
    fun toListInverse() {
        parseAPLExpression("toList˝ (1;2;3)").let { result ->
            assert1DArray(arrayOf(1, 2, 3), result)
        }
    }

    @Test
    fun toListInverseWithSymbol() {
        parseAPLExpression("≬˝ (1;2;3)").let { result ->
            assert1DArray(arrayOf(1, 2, 3), result)
        }
    }

    @Test
    fun toListInverseWithInvalidArgumentType() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpression("toList˝ 1 2 3")
        }
    }

    @Test
    fun toListInverseWithSymbolAndArrayArgument() {
        assertFailsWith<APLIllegalArgumentException> {
            parseAPLExpression("≬˝ 1 2 3")
        }
    }

    @Test
    fun fromListInverse() {
        parseAPLExpression("fromList˝ 1 2 3").let { result ->
            assertListContent(listOf(1, 2, 3), result)
        }
    }

    @Test
    fun fromListInverseWithSymbol() {
        parseAPLExpression("⌷˝ 1 2 3").let { result ->
            assertListContent(listOf(1, 2, 3), result)
        }
    }

    @Test
    fun fromListInverseWithSymbolAndListArgument() {
        parseAPLExpression("⌷˝ (1;2;3)").let { result ->
            assertIs<APLList>(result)
            assertListContent(listOf(1, 2, 3), result.listElement(0))
        }
    }

    @Test
    fun fromListInverseWithSymbolAndScalarArgument() {
        parseAPLExpression("⌷˝ 1").let { result ->
            assertListContent(listOf(1), result)
        }
    }

    @Test
    fun fromListInverseWithListArgument() {
        parseAPLExpression("fromList˝ (1;2;3)").let { result ->
            assertIs<APLList>(result)
            assertListContent(listOf(1, 2, 3), result.listElement(0))
        }
    }

    private fun assertListContent(expected: List<Any>, list: APLValue) {
        assertTrue(list is APLList, "actual type: ${list::class.simpleName}")
        assertEquals(expected.size, list.listSize())
        expected.forEachIndexed { index, item ->
            assertAPLValue(item, list.listElement(index))
        }
    }
}
