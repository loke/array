package com.dhsdevelopments.kap.edit

interface EditorEngineConnector {
    fun findParenCandidates(src: String, index: Int, callback: (List<Int>) -> Unit)
}
