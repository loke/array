package com.dhsdevelopments.kap.cryptography

import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.IncompatibleTypeException
import com.dhsdevelopments.kap.dimensionsOfSize
import com.dhsdevelopments.kap.iterateMembersWithPosition
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertIs

class HashTest : CryptographyTest() {
    @Test
    fun digestNames() {
        fun checkName(kw: String, digestName: String) {
            evalCryptography("crypto:makeDigest :${kw}").let { result ->
                assertIs<DigestKapValue>(result)
                assertEquals(digestName, result.value.id.name)
            }
        }
        checkName("SHA256", "SHA-256")
        checkName("SHA512", "SHA-512")
        checkName("MD5", "MD5")
        checkName("SHA1", "SHA-1")
    }

    @Test
    fun sha256EmptyString() {
        evalCryptography("d ← crypto:makeDigest :SHA256 ⋄ d⍠:encode \"\"").let { result ->
            assertBytes("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", result)
        }
    }

    @Test
    fun sha256TestString() {
        evalCryptography("d ← crypto:makeDigest :SHA256 ⋄ d⍠:encode \"Test string foo bar\"").let { result ->
            assertBytes("bf18ac6fbe4b708e79137f93549314d8a1a5ba5ea13f1d919bfa9d79dc4d2a9a", result)
        }
    }

    @Test
    fun sha512TestString() {
        evalCryptography("d ← crypto:makeDigest :SHA512 ⋄ d⍠:encode \"A different hash test\"").let { result ->
            assertBytes(
                "6f44cdfbc8b9e4735598691972811ee6e4f8205c0f2332327638b04726b20ca4b2911f0ee5f73550983df81131ca12dfe46b0552708158d46463098c8d3f5b4f",
                result)
        }
    }

    @Test
    fun sha256WithBinaryArray() {
        evalCryptography("d ← crypto:makeDigest :SHA256 ⋄ d⍠:encode \"Foo test string\"-@\\0").let { result ->
            assertBytes("05cb8372ff1a4ae07f7e4c19c4dd07109d678060951277e786d4d6d4e0d4f87b", result)
        }
    }

    @Test
    fun sha256WithTooLargeNumber() {
        assertFailsWith<IncompatibleTypeException> {
            evalCryptography("d ← crypto:makeDigest :SHA256 ⋄ d⍠:encode 123456")
        }
    }

    private fun assertBytes(expected: String, result: APLValue) {
        val bytes = hexToBytes(expected)
        assertDimension(dimensionsOfSize(bytes.size), result)
        result.iterateMembersWithPosition { v, i ->
            assertEquals(bytes[i], v.ensureNumber().asInt())
        }
    }

    @OptIn(ExperimentalStdlibApi::class, ExperimentalUnsignedTypes::class)
    private fun hexToBytes(s: String): List<Int> {
        return s.hexToUByteArray().map(UByte::toInt)
    }
}
