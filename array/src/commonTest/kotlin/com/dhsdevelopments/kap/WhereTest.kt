package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertFailsWith

@Suppress("UNUSED_CHANGED_VALUE")
class WhereTest : APLTest() {
    @Test
    fun simpleWhere() {
        parseAPLExpression("⍸ 0 0 1 1 0 0 1 1 1 1").let { result ->
            assertArrayContent(arrayOf(2, 3, 6, 7, 8, 9), result)
        }
    }

    @Test
    fun multiDimensionalWhere() {
        fun assertNumbers(a: Long, b: Long, value: APLValue) {
            assertDimension(dimensionsOfSize(2), value)
            assertSimpleNumber(a, value.valueAt(0))
            assertSimpleNumber(b, value.valueAt(1))
        }

        parseAPLExpression("⍸4 5⍴0 0 1\n").let { result ->
            assertDimension(dimensionsOfSize(6), result)
            assertNumbers(0, 2, result.valueAt(0))
            assertNumbers(1, 0, result.valueAt(1))
            assertNumbers(1, 3, result.valueAt(2))
            assertNumbers(2, 1, result.valueAt(3))
            assertNumbers(2, 4, result.valueAt(4))
            assertNumbers(3, 2, result.valueAt(5))
        }
    }

    @Test
    fun nonBooleanValues() {
        fun assertNumbers(a: Long, b: Long, value: APLValue) {
            assertDimension(dimensionsOfSize(2), value)
            assertSimpleNumber(a, value.valueAt(0))
            assertSimpleNumber(b, value.valueAt(1))
        }

        // 0 0  0 0  0 0  1 0  1 0  1 0  2 0  2 0  2 0  3 0  3 0  3 0
        parseAPLExpression("⍸ 4 5 ⍴ 3 0 0 0 0").let { result ->
            assertDimension(dimensionsOfSize(12), result)
            var i = 0
            assertNumbers(0, 0, result.valueAt(i++))
            assertNumbers(0, 0, result.valueAt(i++))
            assertNumbers(0, 0, result.valueAt(i++))
            assertNumbers(1, 0, result.valueAt(i++))
            assertNumbers(1, 0, result.valueAt(i++))
            assertNumbers(1, 0, result.valueAt(i++))
            assertNumbers(2, 0, result.valueAt(i++))
            assertNumbers(2, 0, result.valueAt(i++))
            assertNumbers(2, 0, result.valueAt(i++))
            assertNumbers(3, 0, result.valueAt(i++))
            assertNumbers(3, 0, result.valueAt(i++))
            assertNumbers(3, 0, result.valueAt(i++))
        }
    }

    @Test
    fun invalidFormat() {
        assertFailsWith<APLEvalException> {
            parseAPLExpression("⍸ 4 5 ⍴ 1 1 (2 2 ⍴ 1 0)")
        }
    }

    @Test
    fun scalarArgument() {
        assertAPLNull(parseAPLExpression("⍸ 1"))
    }

    @Test
    fun scalarArgumentWrongType() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("⍸@a")
        }
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("⍸ map 2 2 ⍴ 1 2 3 4")
        }
    }

    @Test
    fun inverseWhereTest0() {
        parseAPLExpression("⍸˝ 4 5").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0, 1, 1), result)
        }
    }

    @Test
    fun inverseWhereTest1() {
        parseAPLExpression("⍸˝ 2 2 4").let { result ->
            assert1DArray(arrayOf(0, 0, 2, 0, 1), result)
        }
    }

    @Test
    fun inverseWhereTest2() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("⍸˝ 2 1")
        }
    }

    @Test
    fun inverseWhereTest3() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("⍸˝ ¯1 2")
        }
    }

    @Test
    fun inverseWhereTest4() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("⍸˝ 5")
        }
    }

    @Test
    fun inverseWhereTest5() {
        parseAPLExpression("⍸˝ (1 2) (2 3)").let { result ->
            assertDimension(dimensionsOfSize(3, 4), result)
            assertArrayContent(arrayOf(0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1), result)
        }
    }

    @Test
    fun inverseWhereTest6() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("⍸˝ (1 2) (0 3)")
        }
    }

    @Test
    fun inverseWhereTest7() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("⍸˝ (1 2) (1 0)")
        }
    }

    @Test
    fun inverseWhereTest8() {
        parseAPLExpression("⍸˝ (1 2) (1 2) (5 2)").let { result ->
            assertDimension(dimensionsOfSize(6, 3), result)
            assertArrayContent(arrayOf(0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1), result)
        }
    }

    @Test
    fun inverseWhereTest9() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("⍸˝ (1 2) 1")
        }
    }

    @Test
    fun inverseWhereTest10() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("⍸˝ 2 2⍴1 2 3 4")
        }
    }

    @Test
    fun inverseWhereTest11() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("⍸˝ (2 2⍴1 0 0 0) (2 2⍴1 0 0 0)")
        }
    }

    @Test
    fun inverseWhereTest12() {
        assertAPLNull(parseAPLExpression("⍸˝ ⍬"))
    }

    @Test
    fun inverseWhereTest13() {
        parseAPLExpression("⍸˝ ⍸ 2 2⍴0 1 1 0").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 1, 1, 0), result)
        }
    }

    @Test
    fun inverseWhereTest14() {
        parseAPLExpression("⍸˝ (1 2) (2 1)").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(arrayOf(0, 0, 0, 0, 0, 1, 0, 1, 0), result)
        }
    }
}
