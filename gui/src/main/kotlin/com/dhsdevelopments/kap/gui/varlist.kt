package com.dhsdevelopments.kap.gui

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.gui.arrayedit.ArrayEditor
import com.dhsdevelopments.kap.gui.arrayedit.exportToCode
import com.dhsdevelopments.kap.gui.arrayedit.exportToCsv
import com.dhsdevelopments.kap.gui.arrayedit.exportToExcel
import com.dhsdevelopments.kap.rendertext.renderStringValue
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.layout.BorderPane
import javafx.util.Callback
import org.controlsfx.control.tableview2.FilteredTableColumn
import org.controlsfx.control.tableview2.FilteredTableView
import org.controlsfx.control.tableview2.filter.popupfilter.PopupStringFilter
import kotlin.math.min


class VariableListController(val client: Client) {
    lateinit var borderPane: BorderPane
    lateinit var table: FilteredTableView<VarListEntryWrapper>

    val node get() = borderPane
    val content: ObservableList<VarListEntryWrapper> = FXCollections.observableArrayList()

    init {
        val loader = FXMLLoader(VariableListController::class.java.getResource("varlist.fxml"))
        loader.setController(this)
        loader.load<Parent>()

//        val expanderColumn: TableRowExpanderColumn<ValueWrapper> =
//            TableRowExpanderColumn<ValueWrapper> { dataFeatures -> createEditor(dataFeatures.value.valueInt) }

        val cellFactory = VarListCellFactory(client)

        val namespaceNameColumn = FilteredTableColumn<VarListEntryWrapper, String>("Namespace")
        namespaceNameColumn.cellValueFactory = PropertyValueFactory("namespaceName")
        PopupStringFilter(namespaceNameColumn).let { f ->
            namespaceNameColumn.setOnFilterAction { f.showPopup() }
        }
        namespaceNameColumn.cellFactory = cellFactory

        val symbolNameColumn = FilteredTableColumn<VarListEntryWrapper, String>("Name")
        symbolNameColumn.cellValueFactory = PropertyValueFactory("symbolName")
        PopupStringFilter(symbolNameColumn).let { f ->
            symbolNameColumn.setOnFilterAction { f.showPopup() }
        }
        symbolNameColumn.cellFactory = cellFactory

        val valueColumn = TableColumn<VarListEntryWrapper, String>("Value")
        valueColumn.cellValueFactory = PropertyValueFactory("value")
        valueColumn.cellFactory = cellFactory

        val descriptionColumn = TableColumn<VarListEntryWrapper, String>("Description")
        descriptionColumn.cellValueFactory = PropertyValueFactory("description")
        descriptionColumn.cellFactory = VarListCellFactory(client)

        table.columns.setAll(/*expanderColumn,*/ namespaceNameColumn, symbolNameColumn, valueColumn, descriptionColumn)
        table.selectionModel.selectionMode = SelectionMode.MULTIPLE

        content.setAll(loadVariableContent(findVariablesFromEnvironment(client.engine)))
        client.calculationQueue.addTaskCompletedHandler { engine ->
            val updated = findVariablesFromEnvironment(engine)
            Platform.runLater {
                content.clear()
                content.addAll(loadVariableContent(updated))
            }
        }

        table.items = content
    }

    private fun findVariablesFromEnvironment(engine: Engine): List<Pair<Symbol, APLValue?>> {
        val rootStackFrame = engine.rootStackFrame
        val namespace = engine.currentNamespace.name
        val result = ArrayList<Pair<Symbol, APLValue?>>()
        val bindings = engine.rootEnvironment.bindings
        val storageSize = rootStackFrame.storageList.size
        for (i in 0 until bindings.size) {
            val binding = bindings[i]
            val sym = binding.name
            if (sym.namespace == namespace) {
                val v = if (i < storageSize) {
                    rootStackFrame.storageList[i].value()
                } else {
                    null
                }
                result.add(Pair(sym, v))
            }
        }
        return result
    }

    private fun loadVariableContent(updated: List<Pair<Symbol, APLValue?>>): ArrayList<VarListEntryWrapper> {
        val result = ArrayList<VarListEntryWrapper>()
        updated.forEach { (k, v) ->
            if (k.namespace != NamespaceList.ANONYMOUS_SYMBOL_NAMESPACE_NAME && v != null) {
                result.add(VarListEntryWrapper(this, k, v))
            }
        }
        return result
    }

    private fun createEditor(value: APLValue): Node {
        return Label("foo")
    }

    private val descriptions = HashMap<Symbol, String>()

    fun descriptionForSymbol(symbol: Symbol) = descriptions[symbol] ?: ""
}

class VarListCellFactory(val client: Client) : Callback<TableColumn<VarListEntryWrapper, String>, TableCell<VarListEntryWrapper, String>> {
    override fun call(param: TableColumn<VarListEntryWrapper, String>?): TableCell<VarListEntryWrapper, String> {
        return object : TableCell<VarListEntryWrapper, String>() {
            init {
                contextMenu = ContextMenu(
                    MenuItem("Open in Array Editor").apply { onAction = EventHandler { ArrayEditor.open(client, tableRow.item.valueInt, tableRow.item.sym) } },
                    MenuItem("Export to Excel").apply { onAction = EventHandler { exportToExcel(client, selectedObjects()) } },
                    MenuItem("Export to CSV").apply { onAction = EventHandler { exportToCsv(client, tableRow.item.valueInt) } },
                    MenuItem("Export to Code").apply { onAction = EventHandler { exportToCode(client, selectedObjects()) } })
            }

            private fun selectedObjects(): List<ValueWithName> {
                val items = tableView.selectionModel.selectedItems
                return if (items.isEmpty()) {
                    val item = tableRow.item
                    listOf(ValueWithName(item.valueInt, item.sym))
                } else {
                    items.map { item -> ValueWithName(item.valueInt, item.sym) }
                }
            }

            override fun updateItem(item: String?, empty: Boolean) {
                super.updateItem(item, empty)
                text = if (empty) null else item
                graphic = null
            }
        }
    }
}

data class ValueWithName(val value: APLValue, val name: Symbol? = null, val description: String? = null)

class VarListEntryWrapper(val varListController: VariableListController, val sym: Symbol, val valueInt: APLValue) {
    @Suppress("unused")
    val namespaceName
        get() = sym.namespace.name

    @Suppress("unused")
    val symbolName
        get() = sym.symbolName

    val value
        get() = valueInt.dimensions.let { d ->
            renderSimple(valueInt)
                ?: when (d.size) {
                    0 -> valueInt.toString()
                    1 -> StringBuilder().let { buf ->
                        (0 until min(5, d[0])).forEach { i ->
                            if (i > 0) {
                                buf.append(" ")
                            }
                            val v = valueInt.valueAt(i)
                            buf.append(renderSimple(valueInt.valueAt(i)) ?: "(dimensions: ${v.dimensions})")
                        }
                        if (d[0] >= 5) {
                            buf.append(" (${d[0]} elements)")
                        }
                        buf.toString()
                    }
                    else -> "(array: ${d})"
                }
        }

    @Suppress("unused")
    val description get() = varListController.descriptionForSymbol(sym)

    private fun renderSimple(value: APLValue): String? {
        value.dimensions
        return when {
            isNullValue(value) -> "⍬"
            value is APLSingleValue -> value.formatted(FormatStyle.PLAIN)
            value.isStringValue() -> renderStringValue(value, FormatStyle.PRETTY)
            else -> null
        }
    }
}
