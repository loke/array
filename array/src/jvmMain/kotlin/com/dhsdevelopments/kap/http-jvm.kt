package com.dhsdevelopments.kap

import java.io.IOException
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpRequest.BodyPublishers
import java.net.http.HttpResponse
import java.time.Duration

class HttpResultJvm(
    override val code: Long,
    override val content: String,
    override val headers: Map<String, List<String>>
) : HttpResult

private fun makeHttpClient(): HttpClient {
    return HttpClient.newBuilder()
        .followRedirects(HttpClient.Redirect.NORMAL)
        .build()
}

actual fun httpRequest(engine: Engine, httpRequestData: HttpRequestData, pos: Position?): HttpResult {
    fun makeContent() = if (httpRequestData.data == null) BodyPublishers.noBody() else BodyPublishers.ofByteArray(httpRequestData.data)

    val b = HttpRequest.newBuilder()
        .uri(URI(httpRequestData.url))
        .timeout(Duration.ofMinutes(1))

    when (httpRequestData.method) {
        HttpMethod.GET -> {
            require(httpRequestData.data == null)
            b.GET()
        }
        HttpMethod.POST -> {
            b.POST(makeContent())
        }
        HttpMethod.PUT -> {
            require(httpRequestData.data != null)
            b.PUT(makeContent())
        }
        HttpMethod.DELETE -> {
            require(httpRequestData.data == null)
            b.DELETE()
        }
        HttpMethod.PATCH -> {
            b.method("PATCH", BodyPublishers.noBody())
        }
        HttpMethod.HEAD -> {
            require(httpRequestData.data == null)
            b.method("HEAD", BodyPublishers.noBody())
        }
        HttpMethod.OPTIONS -> {
            require(httpRequestData.data == null)
            b.method("OPTIONS", BodyPublishers.noBody())
        }
    }

    httpRequestData.headers.forEach { (k, v) ->
        b.header(k, v)
    }

    val httpRequest = b.build()
    try {
        val result = makeHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString())
        return HttpResultJvm(result.statusCode().toLong(), result.body(), result.headers().map())
    } catch (e: IOException) {
        throwTagCatch(engine, "ioError", APLNullValue, "Error when connecting to HTTP server: ${e.message}", pos, e)
    }
}

actual fun httpRequestCallback(
    engine: Engine,
    httpRequestData: HttpRequestData,
    pos: Position?,
    callback: (HttpResult) -> Either<APLValue, Exception>
) {
    val jvmNative = engine.nativeData as JvmNativeData
    val pub = jvmNative.findAsyncJobPublisher(pos)
    val thread = object : Thread("http request thread") {
        override fun run() {
            val httpResult = httpRequest(engine, httpRequestData, pos)
            val job = AsyncJob { callback(httpResult) }
            pub(job)
        }
    }
    thread.start()
}
