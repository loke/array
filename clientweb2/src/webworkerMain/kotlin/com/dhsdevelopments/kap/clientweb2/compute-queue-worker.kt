package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.clientweb2.http.HttpManagerImpl
import com.dhsdevelopments.kap.completions.symbolCompletionCandidates
import com.dhsdevelopments.kap.csv.CsvParseException
import com.dhsdevelopments.kap.csv.readCsv
import com.dhsdevelopments.kap.edit.parenCandidates
import kotlinx.serialization.json.Json
import org.w3c.dom.DedicatedWorkerGlobalScope
import org.w3c.xhr.XMLHttpRequest

external val self: DedicatedWorkerGlobalScope

fun main() {
    loadLibraries()
}

var numOutstandingRequests = 0

fun loadLibraries() {
    loadLibFiles(
        "standard-lib/standard-lib.kap",
        "standard-lib/structure.kap",
        "standard-lib/base-functions.kap",
        "standard-lib/math.kap",
        "standard-lib/math-kap.kap",
        "standard-lib/io.kap",
        "standard-lib/http.kap",
        "standard-lib/output.kap",
        "standard-lib/output3.kap",
        "standard-lib/time.kap",
        "standard-lib/regex.kap",
        "standard-lib/util.kap",
        "standard-lib/map.kap")
}

private fun loadLibFiles(vararg names: String) {
    numOutstandingRequests = names.size
    names.forEach { name ->
        val http = XMLHttpRequest()
        http.open("GET", name)
        http.onload = {
            if (http.readyState == 4.toShort() && http.status == 200.toShort()) {
                registeredFilesRoot.registerFile(name, http.responseText.encodeToByteArray())
            } else {
                console.log("Error loading library file: ${name}. Code: ${http.status}")
            }
            if (--numOutstandingRequests == 0) {
                initQueue()
            }
        }
        http.send()
    }
}

fun initQueue() {
    val engine = Engine()
    engine.addLibrarySearchPath("standard-lib")
    val sendMessageFn = { msg: dynamic -> self.postMessage(msg) }
    engine.jsNativeData().sendMessageFn = sendMessageFn
    engine.standardOutput = CharacterOutput { s ->
        val output = jsObject2(
            "messageType" to OUTPUT_DESCRIPTOR_TYPE,
            "text" to s)
        sendMessageFn(output)
    }
    engine.jsNativeData().httpManager = HttpManagerImpl()
    if (engine.jsNativeData().jsTransferQueue != null) {
        engine.jsNativeData().keyboardInput = JsKeyboardInput(engine)
    }
    engine.addModule(JsChartModule(sendMessageFn))
    engine.addModule(JsGuiModule(sendMessageFn))
    engine.addModule(JsAudioModule(sendMessageFn))
    engine.parseAndEval(StringSourceLocation("use(\"standard-lib.kap\")"))
    self.onmessage = { event ->
        val msg = event.data.asDynamic()
        if (msg.messageType) {
            when (msg.messageType) {
                WorkerRequestType.ENGINE_REQUEST -> processEngineRequest(engine, msg)
                else -> throw IllegalStateException("Unexpected message type: ${msg.type}")
            }
        } else {
            when (val request = Json.decodeFromString<Request>(event.data as String)) {
                is EvalRequest -> processEvalRequest(engine, request)
                is ImportRequest -> processImportRequest(engine, request)
                is ImportCsvRequest -> processImportCsvRequest(engine, request)
                is TokeniseRequest -> processTokeniseRequest(engine, request)
                is CompletionRequest -> processCompletionRequest(engine, request)
            }
        }
    }
    val message: dynamic = makeEngineStartedDescriptor(engine)
    self.postMessage(message)
}

private fun makeEngineStartedDescriptor(engine: Engine): dynamic {
    val jsNativeData = engine.nativeData as JsNativeData
    return jsObject2(
        "messageType" to ENGINE_STARTED_TYPE,
        "text" to "started",
        "sharedBuffers" to jsNativeData.jsTransferQueue?.sharedBuffers())
}

private fun processEvalRequest(engine: Engine, request: EvalRequest) {
    engine.clearInterrupted()
    val sourceLocation = StringSourceLocation(request.src)
    val result = try {
        engine.parseAndEvalWithPostProcessing(sourceLocation) { context, result, time ->
            val collapsed = result.collapse()
            when (request.resultType) {
                ResultType.FORMATTED_PRETTY -> {
                    val responseStr = if (isEmptyValue(collapsed)) {
                        ""
                    } else {
                        formatResultToStrings(renderResult(context, collapsed)).joinToString("\n")
                    }
                    StringResponse(responseStr, time.inWholeMilliseconds)
                }
                ResultType.FORMATTED_READABLE -> StringResponse(collapsed.formatted(FormatStyle.READABLE), time.inWholeMilliseconds)
                ResultType.JS -> DataResponse(formatValueToJs(collapsed), time.inWholeMilliseconds)
            }
        }
    } catch (e: APLGenericException) {
        makeEvalExceptionDescriptor(request.requestId, e, sourceLocation)
    } catch (e: Exception) {
        ExceptionDescriptor(e.message ?: "empty")
    }
    self.postMessage(Json.encodeToString(result))
}

private fun processImportRequest(engine: Engine, request: ImportRequest) {
    importIntoVariable(engine, request.varname) {
        formatJsToValue(request.data)
    }
}

fun processImportCsvRequest(engine: Engine, request: ImportCsvRequest) {
    importIntoVariable(engine, request.varname) {
        try {
            readCsv(makeStringCharacterProvider(request.data))
        } catch (e: CsvParseException) {
            throw ImportFailedException("Error importing CSV: ${e.message}")
        }
    }
}

class ImportFailedException(val responseMessage: String) : Exception("Import function failed: ${responseMessage}")

fun processTokeniseRequest(engine: Engine, request: TokeniseRequest) {
    val engineCopy = engine.copyToSyntaxChecker()
    var resp: dynamic = null
    try {
        val result = js("[]")
        val tokeniser = TokenGenerator(engineCopy, StringSourceLocation(request.src))
        try {
            while (true) {
                val (token, pos) = tokeniser.nextTokenOrSpace()
                if (token is EndOfFile) break
                val res = when (token) {
                    is Symbol -> TokenTypes.SYMBOL
                    is ParsedLong -> TokenTypes.NUMBER
                    is ParsedBigInt -> TokenTypes.NUMBER
                    is ParsedDouble -> TokenTypes.NUMBER
                    is ParsedComplex -> TokenTypes.NUMBER
                    is StringToken -> TokenTypes.STRING
                    is ParsedCharacter -> TokenTypes.CHAR
                    is Comment -> TokenTypes.COMMENT
                    else -> null
                }
                if (res != null) {
                    val tokenInfo = jsObject2(
                        "type" to res,
                        "p" to jsObject2(
                            "line" to pos.line,
                            "col" to pos.col,
                            "endLine" to pos.computedEndLine,
                            "endCol" to pos.computedEndCol))
                    result.push(tokenInfo)
                }
            }
        } catch (_: ParseException) {
            // If there is a parse error, just skip the remainder of the file
        }
        resp = result
    } finally {
        val message = jsObject2(
            "messageType" to TOKENISE_RESULT_TYPE,
            "requestId" to request.requestId,
            "tokens" to resp)
        self.postMessage(message)
        engineCopy.close()
    }
}

private fun importIntoVariable(engine: Engine, varname: String, fn: () -> APLValue) {
    val sym = engine.currentNamespace.internSymbol(varname)
    val env = engine.rootEnvironment
    val binding = env.findBinding(sym) ?: env.bindLocal(sym)
    engine.withThreadLocalAssigned {
        engine.recomputeRootFrame()
        val importResult = try {
            engine.rootStackFrame.storageList[binding.storage.index].updateValue(fn())
            ImportResult("Data imported into: ${varname}")
        } catch (e: ImportFailedException) {
            ImportResult(e.responseMessage)
        }
        self.postMessage(Json.encodeToString(importResult as ResponseMessage))
    }
}

private fun makeEvalExceptionDescriptor(requestId: String, exception: APLGenericException, sourceLocation: SourceLocation): EvalExceptionDescriptor {
    var found: Position? = null
    if (exception is ParseException) {
        found = exception.pos
    } else {
        val p0 = exception.pos
        if (p0 != null && p0.source === sourceLocation) {
            found = p0
        } else if (exception is APLEvalException) {
            val callStack = exception.stack
            if (callStack != null) {
                for (e in callStack.frames.asReversed()) {
                    val p1 = e.primaryPos
                    if (p1 != null && p1.source === sourceLocation) {
                        found = p1
                        break
                    }
                }
            }
        }
    }
    return EvalExceptionDescriptor(
        requestId,
        exception.formattedError(),
        makePosDescriptor(exception.pos),
        makePosDescriptor(found),
        exception.extendedDescription)
}

private fun makePosDescriptor(pos: Position?): PosDescriptor? {
    return if (pos == null) {
        null
    } else {
        PosDescriptor(pos.line, pos.col, pos.computedEndLine, pos.computedEndCol, pos.callerName)
    }
}

private fun processCompletionRequest(engine: Engine, request: CompletionRequest) {
    val (candidates, word) = symbolCompletionCandidates(engine, request.src, request.index)
    val candidatesJson = js("[]")
    candidates.forEach { c ->
        candidatesJson.push(
            jsObject2(
                "text" to c.text,
                "description" to c.description))
    }

    val message = jsObject2(
        "messageType" to COMPLETION_RESULT_TYPE,
        "requestId" to request.requestId,
        "candidates" to candidatesJson,
        "word" to word)
    self.postMessage(message)
}

//private fun processParenCandidatesRequest(engine: Engine, data: ParenCandidatesRequestData) {
//    val candidates = parenCandidates(engine, data.src, data.index)
//    val message = ParenCandidatesResultData().apply {
//        messageType = ENGINE_RESPONSE
//        name = WorkerRequestType.WRAP_WITH_PAREN
//        requestId = data.requestId!!
//        candidatesList = candidates.toIntArray()
//    }
//    self.postMessage(message)
//}

private fun processEngineRequest(engine: Engine, engineRequest: dynamic) {
    val result = when (engineRequest.name) {
        EngineRequestType.WRAP_WITH_PAREN -> processParenCandidatesRequest(engine, engineRequest.data)
        EngineRequestType.COMMAND -> processExecuteCommand(engine, engineRequest.data)
        else -> throw IllegalStateException("Unexpected engine request name: ${engineRequest.name}")
    }
    val response = makeEngineResponse(engineRequest.requestId, engineRequest.name, result)
    self.postMessage(response)
}

private fun processParenCandidatesRequest(engine: Engine, requestData: dynamic): dynamic {
    val candidates = parenCandidates(engine, requestData.src, requestData.index)
    return makeParenCandidatesResultData(candidates.toIntArray())
}

private fun processExecuteCommand(engine: Engine, requestData: dynamic): dynamic {
    val cmd = requestData.cmd
    KapLogger.i { "Processing command: '${cmd}'" }
    return try {
        engine.commandManager.processCommandString(cmd)
        jsObject2("success" to true)
    } catch (e: CommandException) {
        jsObject2("success" to false, "message" to "Error: ${e.message}")
    }
}

fun makeEngineResponse(requestId: String, name: String, data: Any?): dynamic {
    return jsObject2(
        "messageType" to ENGINE_RESPONSE,
        "requestId" to requestId,
        "name" to name,
        "data" to data)
}

fun makeParenCandidatesResultData(candidateList: IntArray): dynamic {
    return jsObject2(
        "candidatesList" to candidateList)
}
