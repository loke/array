package com.dhsdevelopments.kap.repl

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.net.networkApi
import com.dhsdevelopments.kap.options.ArgParser
import com.dhsdevelopments.kap.options.InvalidOption
import com.dhsdevelopments.kap.options.Option
import com.dhsdevelopments.kap.repl.rideprotocol.RideRepl
import com.dhsdevelopments.kap.replserver.ReplServer
import com.dhsdevelopments.kap.replserver.TcpReplServer

const val DEFAULT_PROMPT_CHAR = "⊢"

abstract class GenericReplBuilder() {
    private val options =
        mutableListOf(
            Option("help", "Print a summary of options", shortOption = "h"),
            Option("lib-path", "Location of the Kap standard library", requireArg = "path", shortOption = "p", multiple = true),
            Option("load", "File to load", "file", shortOption = "l", multiple = true),
            Option("no-repl", "Don't start the REPL", shortOption = "n"),
            Option("no-standard-lib", "Don't load standard-lib"),
            Option("list-modules", "List available modules"),
            Option("tty", "Use the text-based REPL", shortOption = "t"),
            Option("no-lineeditor", "Use plain text input", multiple = false),
            Option("mod", "Name of module to initialise", requireArg = "name", shortOption = "m", multiple = true),
            Option("mod-option", "Module option in the form: module-name:module-param:value", requireArg = "option", shortOption = "M", multiple = true),
            Option("eval", "Evaluate expression", requireArg = "expr", shortOption = "E", multiple = true),
            Option("server", "Start repl server on the specified port", requireArg = "port"),
            Option("annotate", "Annotated output for editor integration"))

    open fun initBuild() = Unit

    private val outputSink = object : Appendable {
        override fun append(value: Char): Appendable {
            outputString(value.toString())
            return this
        }

        override fun append(value: CharSequence?): Appendable {
            outputString(value.toString())
            return this
        }

        override fun append(value: CharSequence?, startIndex: Int, endIndex: Int): Appendable {
            outputString(value.toString().substring(startIndex, endIndex))
            return this
        }
    }

    private val moduleBuilders = ArrayList<KapModuleBuilder>()
    var disableLineeditor: Boolean = false

    fun moduleBuildersList(): List<KapModuleBuilder> = moduleBuilders

    fun addOptions(extraOptions: List<Option>) {
        options.addAll(extraOptions)
    }

    fun addModuleBuilders(newModuleBuilders: List<KapModuleBuilder>) {
        moduleBuilders.addAll(newModuleBuilders)
    }

    var replKeyboardInput: KeyboardInput? = null

    var exceptionTransformer: ReplExceptionTransformer? = null

    @Throws(ReplFailedException::class)
    fun build(args: Array<String>): AbstractGenericRepl? {
        initBuild()

        if (hasGui()) {
            options.add(Option("gui", "Run the GUI", shortOption = "g"))
        }

        if (getEnvValue("RIDE_INIT") != null) {
            options.add(Option("ride", "Enable Ride integration"))
        }

        val argParser = ArgParser(*options.toTypedArray())

        val argResult = try {
            argParser.parse(args)
        } catch (e: InvalidOption) {
            println("Error: ${e.message}")
            argParser.printHelp(outputSink)
            return null
        }

        if (argResult.containsKey("help")) {
            argParser.printHelp(outputSink)
            return null
        }

        if (argResult.containsKey("list-modules")) {
            println("Modules:")
            moduleBuilders.forEach { builder ->
                println("${builder.name} - ${builder.description}")
                val params = builder.configurationParameters()
                if (params.isNotEmpty()) {
                    println("  Configuration parameters:")
                    params.forEach { param ->
                        println("    ${param.name}: ${param.type} - ${param.description}${(if (param.required) " (required)" else "")}")
                    }
                }
            }
            return null
        }

        val serverPort = processServerPort(argResult)
        disableLineeditor = argResult.containsKey("no-lineeditor")
        val tagOutput = argResult.containsKey("annotate")

        processArgs(argResult)

        return if (argResult.containsKey("ride")) {
            RideRepl.make(this, argResult, exceptionTransformer)
        } else {
            GenericRepl(this, argResult, serverPort, tagOutput, exceptionTransformer)
        }
    }

    private fun processServerPort(argResult: Map<String, List<String?>>): Int? {
        val serverPortArgs = argResult["server"]
        return if (!serverPortArgs.isNullOrEmpty()) {
            if (argResult.containsKey("no-repl")) {
                throw ReplFailedException("--server cannot be used with --no-repl")
            }
            val portAsString = serverPortArgs.first()!!
            val n = try {
                portAsString.toInt()
            } catch (_: NumberFormatException) {
                throw ReplFailedException("Server port number is not an integer: ${portAsString}")
            }
            if (n <= 0 || n >= 65536) {
                throw ReplFailedException("Invalid server port number: ${n}")
            }
            n
        } else {
            null
        }
    }

    open fun hasGui(): Boolean = false
    open fun guiIsDefault(): Boolean = false
    open fun outputString(s: String): Unit = print(s)
    open fun processArgs(argResult: Map<String, List<String?>>) {}
    open fun makeKbInput(repl: AbstractGenericRepl): KeyboardInput = makeKeyboardInput(repl.engine) ?: error("Can't create keyboard input")
}

abstract class AbstractGenericRepl(
    val replBuilder: GenericReplBuilder,
    val argResult: Map<String, List<String?>>,
    val serverPort: Int? = null,
    val tagOutput: Boolean = false,
    val exceptionTransformer: ReplExceptionTransformer? = null
) {
    val engine = Engine()
    var initialised: Boolean = false
        private set
    var startupFilesLoaded: Boolean = false
        private set
    var keyboardInput: KeyboardInput? = null
    val useGui: Boolean = when {
        !replBuilder.hasGui() -> false
        argResult.containsKey("tty") && argResult.containsKey("gui") -> throw ReplFailedException("TTY and GUI selected at the same time")
        argResult.containsKey("tty") -> false
        argResult.containsKey("gui") -> true
        else -> replBuilder.guiIsDefault()
    }

    private var server: ReplServer? = null

    fun init() {
        require(!initialised) { error("REPL already initialised") }
        keyboardInput = replBuilder.makeKbInput(this)
        initModuleBuilders()
        initLibPath()
        loadStdLibrary()
        startServer()
        initialised = true
    }

    fun hasStartupFiles(): Boolean {
        if (!argResult["load"].isNullOrEmpty()) return true
        if (!argResult["eval"].isNullOrEmpty()) return true
        return false
    }

    fun loadStartupFiles() {
        require(initialised) { "REPL not initialised" }
        require(!startupFilesLoaded) { "Startup files has already been loaded" }
        try {
            loadFiles()
            processEvalStrings()
        } finally {
            startupFilesLoaded = true
        }
    }

    fun initAndLoadStartupFiles() {
        init()
        loadStartupFiles()
    }

    fun inhibitRepl() = argResult.containsKey("no-repl")

    private fun initModuleBuilders() {
        val buildersMap = replBuilder.moduleBuildersList().associate { m -> m.name to m }
        val modOptions = argResult["mod-option"]
        argResult["mod"]?.forEach { moduleName ->
            val builder = buildersMap[moduleName] ?: throw ReplFailedException("Module not found: ${moduleName}")
            val params = builder.configurationParameters()
            val paramValues = HashMap<String, Any>()
            if (modOptions != null) {
                params.forEach { param ->
                    val prefix = "${moduleName}:${param.name}:"
                    modOptions
                        .filterNotNull()
                        .filter { s -> s.startsWith(prefix) }
                        .map { s -> s.drop(prefix.length) }
                        .forEach { value ->
                            val v0 = when (param.type) {
                                ParameterInfo.ParameterType.STRING -> value
                                ParameterInfo.ParameterType.INT -> value.toInt()
                            }
                            if (paramValues.containsKey(param.name)) {
                                throw ReplFailedException("Module parameter 'value' was specified multiple times")
                            }
                            paramValues[param.name] = v0
                        }
                }
            }
            try {
                val module = builder.makeModuleWithParams(paramValues)
                engine.addModule(module)
            } catch (e: ModuleBuilderInitFailedException) {
                throw ReplFailedException("Failed to initialise module: ${moduleName}: ${e.message}", e)
            }
        }
    }

    private fun initLibPath() {
        argResult["lib-path"]?.forEach { libPath ->
            if (libPath != null) {
                val libPathType = fileType(libPath)
                if (libPathType != null && libPathType === FileNameType.DIRECTORY) {
                    engine.addLibrarySearchPath(libPath)
                } else {
                    println("Warning: ${libPath} is not a directory, ignoring")
                }
            }
        }
    }

    private fun loadStdLibrary() {
        if (!argResult.containsKey("no-standard-lib")) {
            try {
                engine.parseAndEval(StringSourceLocation("use(\"standard-lib.kap\")"))
            } catch (e: APLGenericException) {
                throw ReplFailedException("Error loading standard library", e)
            }
        }
    }

    private fun loadFiles() {
        argResult["load"]?.forEach { file ->
            requireNotNull(file)
            loadFile(file)
        }
    }

    private fun processEvalStrings() {
        argResult["eval"]?.forEach { expr ->
            requireNotNull(expr)
            evalExpression(expr)
        }
    }

    private fun loadFile(file: String) {
        val sourceLocation = if (file == "-") {
            StdinSourceLocation(engine)
        } else {
            val loadFileType = fileType(file)
            when (loadFileType) {
                null -> throw ReplFailedException("File does not exist: ${file}")
                FileNameType.FILE -> FileSourceLocation(file)
                else -> throw ReplFailedException("Not a file: ${file}")
            }
        }
        try {
            engine.parseAndEval(sourceLocation).collapse()
        } catch (e: APLGenericException) {
            throw ReplFailedException("Error loading file: ${file}", e)
        }
    }

    protected fun evalExpression(expr: String): FormattedEvalResult {
        engine.clearInterrupted()
        return engine.parseAndEvalWithFormat(StringSourceLocation(expr))
    }

    private fun startServer() {
        if (serverPort != null) {
            val nwapi = networkApi()
            if (nwapi == null) {
                KapLogger.w { "Network api not available" }
            } else {
                val serverInst = TcpReplServer(serverPort, nwapi, engine)
                serverInst.start()
                server = serverInst
            }
        }
    }

    fun close() {
        server?.stop()
        engine.close()
    }

    abstract fun mainLoop()

    fun interruptEvaluation() {
        engine.interruptEvaluation()
    }

    abstract fun makeDefaultStdout(): CharacterOutput
}

class GenericRepl(
    replBuilder: GenericReplBuilder,
    argResult: Map<String, List<String?>>,
    serverPort: Int? = null,
    tagOutput: Boolean = false,
    exceptionTransformer: ReplExceptionTransformer? = null
) : AbstractGenericRepl(replBuilder, argResult, serverPort, tagOutput, exceptionTransformer) {

    override fun mainLoop() {
        require(initialised) { "REPL has not been initialised" }
        val kb = keyboardInput ?: throw ReplFailedException("No keyboard reader available")
        val prompt = "${DEFAULT_PROMPT_CHAR} "
        while (true) {
            val line = kb.readString(makeTaggedString(prompt, "prompt")) ?: break
            if (line.isNotBlank()) {
                val cmgr = engine.commandManager
                val cmd = cmgr.processPrefix(line)
                if (cmd != null) {
                    try {
                        cmgr.processCommandString(cmd)
                    } catch (e: CommandException) {
                        outputTagged("Error: ${e.message}\n", "error")
                    }
                } else {
                    try {
                        val result = if (exceptionTransformer == null) {
                            evalExpression(line)
                        } else {
                            exceptionTransformer.callAndHandleExceptions { evalExpression(line) }
                        }
                        if (!isEmptyValue(result.result)) {
                            val resultAsString = formatResultToString(result)
                            outputTagged("${resultAsString}\n", "result")
                        }
                    } catch (e: APLGenericException) {
                        outputTagged(
                            "${e.formattedError()}\n",
                            "error",
                            e.pos?.let { pos -> "${pos.line}:${pos.col}:${pos.computedEndLine}:${pos.computedEndCol}" })
                    }
                }
            }
        }
    }

    private fun formatResultToString(result: FormattedEvalResult): String {
        return buildString {
            for (s in result.strings) {
                append(s)
                append("\n")
            }
        }
    }

    fun outputTagged(content: String, tag: String, arg: String? = null) {
        replBuilder.outputString(makeTaggedString(content, tag, arg))
    }

    private fun makeTaggedString(content: String, tag: String, arg: String? = null): String {
        if (tagOutput) {
            val buf = StringBuilder()
            buf.append(tag)
            if (arg != null && arg != "") {
                buf.append("|")
                buf.append(arg)
            }
            buf.append("{")
            content.forEach { ch ->
                val escaped = when (ch) {
                    '}' -> "\\}"
                    '\\' -> "\\\\"
                    else -> ch
                }
                buf.append(escaped)
            }
            buf.append("}")
            return buf.toString()
        } else {
            return content
        }
    }

    override fun makeDefaultStdout(): CharacterOutput {
        return CharacterOutput { s -> outputTagged(s, "out") }
    }
}

class ReplFailedException(message: String, cause: Exception? = null) : Exception(message, cause) {
    fun formatted(): String {
        val buf = StringBuilder()
        buf.append(message)
        cause?.let { c ->
            buf.append(":\n")
            if (c is APLGenericException) {
                buf.append(c.formattedError())
            } else {
                buf.append(c.message)
            }
        }
        return buf.toString()
    }
}

class StdinSourceLocation(val engine: Engine) : SourceLocation {
    override fun sourceText(): String {
        TODO("not implemented")
    }

    override fun open(): CharacterProvider {
        return engine.standardInput
    }
}

fun interface ReplExceptionTransformer {
    fun callAndHandleExceptions(fn: () -> FormattedEvalResult): FormattedEvalResult
}
