package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs

class PrototypeValuesTest : APLTest() {
    @Test
    fun prototypeWithTake() {
        parseAPLExpression("3↑5 int:proto 1 2").let { result ->
            assert1DArray(arrayOf(1, 2, 5), result)
        }
    }

    @Test
    fun prototypeWithTakeMultipleElements() {
        parseAPLExpression("6↑5 int:proto 1 2").let { result ->
            assert1DArray(arrayOf(1, 2, 5, 5, 5, 5), result)
        }
    }

    @Test
    fun takeSmallDimension() {
        parseAPLExpression("3↑5 int:proto 1 2 3 4 5 6").let { result ->
            assert1DArray(arrayOf(1, 2, 3), result)
        }
    }

    @Test
    fun takeWithArrayProto() {
        parseAPLExpression("5↑ (3 2 ⍴ 10+⍳6) int:proto 1 2").let { result ->
            assertDimension(dimensionsOfSize(5), result)
            assertArrayContent(
                arrayOf(
                    1,
                    2,
                    InnerArray(dimensionsOfSize(3, 2), arrayOf(10, 11, 12, 13, 14, 15)),
                    InnerArray(dimensionsOfSize(3, 2), arrayOf(10, 11, 12, 13, 14, 15)),
                    InnerArray(dimensionsOfSize(3, 2), arrayOf(10, 11, 12, 13, 14, 15))),
                result)
        }
    }

    @Test
    fun takeRowWithProto() {
        parseAPLExpression("1 3 ↑ 10000 int:proto 100+2 2 ⍴ ⍳4").let { result ->
            assertDimension(dimensionsOfSize(1, 3), result)
            assertArrayContent(arrayOf(100, 101, 10000), result)
        }
    }

    @Test
    fun takeWithProtoFromVariable() {
        parseAPLExpression("a ← 9 int:proto 1 2 ⋄ 3 ↑ a").let { result ->
            assert1DArray(arrayOf(1, 2, 9), result)
        }
    }

    @Test
    fun takeWithProtoAsFunctionArgument() {
        parseAPLExpression("∇ foo (x) { 3↑x } ⋄ foo 9 int:proto 1 2").let { result ->
            assert1DArray(arrayOf(1, 2, 9), result)
        }
    }

    @Test
    fun takeWithProtoFromReturnedValue() {
        parseAPLExpression("∇ foo (x) { 9 int:proto x,2 } ⋄ 6 ↑ foo 4 5 6").let { result ->
            assert1DArray(arrayOf(4, 5, 6, 2, 9, 9), result)
        }
    }

    @Test
    fun characterProto0() {
        parseAPLExpression("5 ↑ @\\s int:proto \"bar\"").let { result ->
            assertString("bar  ", result)
        }
    }

    @Test
    fun characterProto1() {
        parseAPLExpression("5 ↑ @X int:proto \"bar\"").let { result ->
            assertString("barXX", result)
        }
    }

    @Test
    fun prototypesArePreservedWhileResizing() {
        parseAPLExpression("3 3 ↑ 2 2 ⍴ 9 int:proto 1 2 3 4").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(arrayOf(1, 2, 9, 3, 4, 9, 9, 9, 9), result)
        }
    }

    @Test
    fun prototypesArePreservedWhileResizingEmptyValue() {
        parseAPLExpression("3 ⍴ 9 int:proto 0⍴1").let { result ->
            assert1DArray(arrayOf(9, 9, 9), result)
        }
    }

    @Test
    fun resizeToSamePreservesPrototypeGenericSameSize() {
        parseAPLExpression("2 2 ⍴ 9 int:proto 2 2 ⍴ 1 2.1 3 4").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, InnerDouble(2.1), 3, 4), result)
            val defaultValue = result.metadata.defaultValue
            assertIs<APLLong>(defaultValue)
            assertEquals(9, defaultValue.value)
        }
    }

    @Test
    fun resizeToSamePreservesPrototypeGenericDifferentSize() {
        parseAPLExpression("2 2 ⍴ 9 int:proto 2 2 ⍴ 1 2.1 3 4 5").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, InnerDouble(2.1), 3, 4), result)
            val defaultValue = result.metadata.defaultValue
            assertIs<APLLong>(defaultValue)
            assertEquals(9, defaultValue.value)
        }
    }

    @Test
    fun resizeToSamePreservesPrototypeLongSameSize() {
        parseAPLExpression("2 2 ⍴ 9 int:proto 2 2 ⍴ 1 2 3 4").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, 2, 3, 4), result)
            val defaultValue = result.metadata.defaultValue
            assertIs<APLLong>(defaultValue)
            assertEquals(9, defaultValue.value)
        }
    }

    @Test
    fun resizeToSamePreservesPrototypeLongDifferentSize() {
        parseAPLExpression("2 2 ⍴ 9 int:proto 2 2 ⍴ 1 2 3 4 5").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, 2, 3, 4), result)
            val defaultValue = result.metadata.defaultValue
            assertIs<APLLong>(defaultValue)
            assertEquals(9, defaultValue.value)
        }
    }

    @Test
    fun resizeToSamePreservesPrototypeLongIotaDifferentSize() {
        parseAPLExpression("2 2 ⍴ 9 int:proto 2 2 ⍴ ⍳100").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 1, 2, 3), result)
            val defaultValue = result.metadata.defaultValue
            assertIs<APLLong>(defaultValue)
            assertEquals(9, defaultValue.value)
        }
    }
}
