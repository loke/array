package com.dhsdevelopments.kap.ffi

import com.dhsdevelopments.kap.APLTest
import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.StringSourceLocation
import com.dhsdevelopments.kap.use
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class JvmNativeTest : APLTest() {
    @Test
    fun putsTest() {
        ffiManager().use { mgr ->
            val lib = mgr.loadLibrary("libc.so.6")
            val fn = lib.findFunction("puts", mgr.makeInputArgInt(), listOf(mgr.makeInputArgString()))
            assertNotNull(fn)
            val res = fn.call(listOf("test message here"))
            assertEquals(18, res.result)
        }
    }

    @Test
    fun moduleTest() {
        parseAPLExpressionWithFfiModule("((ffi:loadLibrary \"libc.so.6\") ffi:findFunction (\"puts\" ; :int ; :string)) ffi:callFunction \"test string via ffi call\"").let { result ->
            assertSimpleNumber(25, result)
        }
    }

    @Test
    fun structTest() {
        ffiManager().use { mgr ->
            val lib = mgr.loadLibrary("libc.so.6")
            val structTm = mgr.defineStruct(
                mgr.makeInputArgInt(), // tm_sec
                mgr.makeInputArgInt(), // tm_min
                mgr.makeInputArgInt(), // tm_hour
                mgr.makeInputArgInt(), // tm_mday
                mgr.makeInputArgInt(), // tm_mon
                mgr.makeInputArgInt(), // tm_year
                mgr.makeInputArgInt(), // tm_wday
                mgr.makeInputArgInt(), // tm_yday
                mgr.makeInputArgInt(), // tm_isdst
                mgr.makeInputArgInt(), // tm_gmtoff
                mgr.makeInputArgString()) // tm_zone

            val structTimeval = mgr.defineStruct(
                mgr.makeInputArgLong(), // tv_sec
                mgr.makeInputArgLong()) // tv_usec

            val gettimeofdayFn = lib.findFunction("gettimeofday", mgr.makeInputArgInt(), listOf(mgr.makeInputArgPtr(), mgr.makeInputArgPtr()))
            assertNotNull(gettimeofdayFn)
            val buf = mgr.allocBytes(structTimeval.size)
            val res = gettimeofdayFn.call(listOf(buf.pointerToOffset(0), mgr.makeNullPtr()))
            assertEquals(0, res.result)
            val sec = structTimeval.derivePtr(buf.pointerToOffset(0), 0).readLongAtOffset(0)
            val usec = structTimeval.derivePtr(buf.pointerToOffset(0), 1).readLongAtOffset(0)
            println("sec = ${sec}, usec = ${usec}")
        }
    }

    @Ignore
    @Test
    fun callStruct() {
        ffiManager().use { mgr ->
            val lib = mgr.loadLibrary("foo.so")
            val structFoo = mgr.defineStruct(mgr.makeInputArgLong(), mgr.makeInputArgLong())
            val fn = lib.findFunction("bar", mgr.makeInputArgInt(), listOf(mgr.makeInputArgStruct(structFoo)))
            assertNotNull(fn)
            val buf = mgr.allocBytes(structFoo.size)
            structFoo.derivePtr(buf.pointerToOffset(0), 0).writeLongAtOffset(0, 10L)
            structFoo.derivePtr(buf.pointerToOffset(0), 1).writeLongAtOffset(0, 21L)
            val res = fn.call(listOf(Array<Long>(2) { i -> if (i == 0) 9L else 220L }))
            println("result = ${res}")
        }
    }

    @Ignore
    @Test
    fun returnStruct() {
        ffiManager().use { mgr ->
            val lib = mgr.loadLibrary("foo.so")
            val structFoo = mgr.defineStruct(mgr.makeInputArgInt(), mgr.makeInputArgInt(), mgr.makeInputArgLong(), mgr.makeInputArgLong())
            val fn = lib.findFunction("retFoo", mgr.makeInputArgStruct(structFoo), emptyList())
            assertNotNull(fn)
            val res = fn.call(emptyList())
            println("res = ${res}")
        }
    }

    @Test
    fun returnString() {
        val src =
            """
            |buf ← ffi:allocBytes 100 
            |buf ffi:copyInto ("testfoo"-@\0),0
            |lib ← ffi:loadLibrary "libc.so.6"
            |fn ← lib ffi:findFunction ("puts" ; :int ; :ptr)
            |result ← fn ffi:callFunction buf
            |ffi:free buf
            |result
            """.trimMargin()
        parseAPLExpressionWithFfiModule(src).let { result ->
            assertSimpleNumber(8, result)
        }
    }

    private fun parseAPLExpressionWithFfiModule(expr: String, withStandardLib: Boolean = false, collapse: Boolean = true, numTasks: Int? = null): APLValue {
        val engine = makeEngine(numTasks)
        val module = FfiModule.make() ?: error("Unable to load module")
        engine.addModule(module)
        engine.addLibrarySearchPath("standard-lib")
        if (withStandardLib) {
            engine.parseAndEval(StringSourceLocation("use(\"standard-lib.kap\")"))
        }
        return engine.parseAndEval(StringSourceLocation(expr), collapseResult = collapse)
    }
}
