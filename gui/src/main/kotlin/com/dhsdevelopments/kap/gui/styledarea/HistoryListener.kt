package com.dhsdevelopments.kap.gui.styledarea

interface HistoryListener {
    fun prevHistory()
    fun nextHistory()
}
