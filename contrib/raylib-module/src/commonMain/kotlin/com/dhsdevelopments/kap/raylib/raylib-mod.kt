package com.dhsdevelopments.kap.raylib

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.makeAPLNumberAsBoolean
import com.dhsdevelopments.kap.ffi.*
import com.dhsdevelopments.kap.log.SimpleLogger

object FfiLogger : SimpleLogger("raylib-module")

abstract class RaylibInitialisationException(message: String) : Exception(message)
class RaylibFunctionNotImplementedException(message: String) : RaylibInitialisationException(message)
class RaylibTypeNotFoundException(message: String, val typeName: String) : RaylibInitialisationException(message)

expect fun parseDefaultRaylibStructs(): List<RaylibApiRoot>

data class ParamDetail(val index: Int, val isOut: Boolean)
data class ArgSpecialisation(val params: List<ParamDetail>) {
    companion object {
        val DEFAULT = ArgSpecialisation(emptyList())
    }
}

private val argSpecialisationMap = mapOf(
    "UpdateCamera" to ArgSpecialisation(listOf(ParamDetail(0, isOut = true))),
    "UpdateCameraPro" to ArgSpecialisation(listOf(ParamDetail(0, isOut = true))))

// Raylib json files downloaded from: https://github.com/raysan5/raylib/tree/master/parser
class RaylibModule : KapModule {
    override val name get() = "raylib-mod"

    lateinit var engine: Engine
    lateinit var mgr: FfiManager
    lateinit var lib: FfiLibrary

    private val raylibStructs = HashMap<String, RaylibStructDefinition>()
    var jsonFiles: List<RaylibApiRoot>? = null
    val aliases = HashMap<String, String>()

    fun resolveAlias(name: String): String {
        var curr = name
        while (true) {
            val aliasName = aliases[curr] ?: return curr
            curr = aliasName
        }
    }

    override fun init(engine: Engine) {
        this.engine = engine
        mgr = try {
            ffiManager()
        } catch (e: FfiNotAvailableException) {
            throw ModuleInitFailedException("FFI manager not available", e)
        }

        lib = try {
            mgr.loadLibrary("libraylib.so")
        } catch (e: LoadLibraryFailedException) {
            throw ModuleInitFailedException("Failed to load raylib library: ${e.message}", e)
        }

        val ns = engine.makeNamespace("raylib")

        val files = parseRaylibStructs()

        files.forEach { json ->
            json.aliases?.forEach { aliasDef ->
                aliases[aliasDef.name] = aliasDef.type
            }
        }

        files.forEach { json ->
            json.structs?.forEach { structDef ->
                try {
                    val fields = structDef.fields.map { f ->
                        RaylibStructFieldDefinition(makeRaylibType(f.type), f.name)
                    }
                    val raylibStructDefinition = RaylibStructDefinition(this, structDef.name, fields)
                    raylibStructs[structDef.name] = raylibStructDefinition
                    engine.registerFunction(ns.internAndExport("init${structDef.name}"), MakeStructRaylibFunction(this, raylibStructDefinition))
                } catch (e: RaylibInitialisationException) {
                    FfiLogger.d { "Skipped struct ${structDef.name}: ${e.message}" }
                }
            }
        }

        files.forEach { json ->
            json.functions?.forEach { fn ->
                try {
                    val functionArgs = makeRaylibTypes(fn)
                    val returnArgType = makeRaylibType(resolveAlias(fn.returnType))
                    val ffiFunction = lib.findFunction(fn.name, returnArgType.type, functionArgs.map(RaylibValueType<*>::type))
                    if (ffiFunction == null) {
                        FfiLogger.w { "Function not found: ${fn.name}" }
                    } else {
                        val spec = argSpecialisationMap[fn.name] ?: ArgSpecialisation.DEFAULT
                        engine.registerFunction(ns.internAndExport(fn.name), RaylibAPLFunction(this, fn.name, ffiFunction, returnArgType, functionArgs, spec))
                    }
                } catch (e: RaylibInitialisationException) {
                    FfiLogger.d { "Skipped function ${fn.name}: ${e.message}" }
                }
            }
        }

        updateGlobalVariables(engine, ns, files)

        engine.classManager.registerSupplementarySystemClass(ns.internAndExport("ffiPointer"), FfiPointerKapClass)
        raylibStructs.forEach { (_, v) ->
            engine.classManager.registerSupplementarySystemClass(ns.internAndExport(v.kapClassAsStruct.name), v.kapClassAsStruct)
            engine.classManager.registerSupplementarySystemClass(ns.internAndExport(v.kapClassAsPointer.name), v.kapClassAsPointer)
        }
    }

    private fun updateGlobalVariables(engine: Engine, ns: Namespace, files: List<RaylibApiRoot>) {
        val assignments = ArrayList<Pair<EnvironmentBinding, () -> APLValue>>()

        fun assignValue(name: String, callback: () -> APLValue) {
            if (ns.findSymbol(name) != null) {
                KapLogger.w { "When declaring global variable, symbol already exists: ${name}" }
            }
            val sym = ns.internAndExport(name)
            if (engine.rootEnvironment.findBinding(sym) == null) {
                assignments.add(Pair(engine.rootEnvironment.bindLocal(sym), callback))
            }
        }

        files.forEach { json ->
            json.defines?.forEach { def ->
                when (def.type) {
                    "STRING" -> assignValue(def.name) { APLString.make(def.value) }
                    "FLOAT" -> assignValue(def.name) { def.value.toDouble().makeAPLNumber() }
                    "INT" -> assignValue(def.name) { def.value.toInt().makeAPLNumber() }
                    "COLOR" -> assignValue(def.name) { parseColour(def.value) }
                }
            }
            json.enums?.forEach { enum ->
                enum.values.forEach { enumValue ->
                    assignValue(enumValue.name) { enumValue.value.makeAPLNumber() }
                }
            }
        }

        engine.withThreadLocalAssigned {
            engine.recomputeRootFrame()
            val stack = currentStorageStack()
            assignments.forEach { (binding, callback) ->
                val value = callback()
                stack.findStorage(StackStorageRef(binding)).updateValue(value)
            }
        }
    }

    private fun parseColour(s: String): APLValue {
        val structDef = raylibStructs["Color"]
        requireNotNull(structDef) { "Struct Color not found" }
        val res = COLOUR_MATCH_REGEX.find(s)
        requireNotNull(res) { "Failed to parse colour definition: ${s}" }
        val r = res.groups.get(1)!!.value.toInt().makeAPLNumber()
        val g = res.groups.get(2)!!.value.toInt().makeAPLNumber()
        val b = res.groups.get(3)!!.value.toInt().makeAPLNumber()
        val a = res.groups.get(4)!!.value.toInt().makeAPLNumber()
        return RaylibStructValue(arrayOf(r, g, b, a), structDef)
    }

    private fun parseRaylibStructs(): List<RaylibApiRoot> {
        val s = jsonFiles
        if (s != null) {
            return s
        }
        return parseDefaultRaylibStructs()
    }

    private fun makeRaylibTypes(fnDefinition: RaylibFunction): List<RaylibValueType<*>> {
        val params = fnDefinition.params
        return if (params == null) {
            emptyList()
        } else {
            params.map { param ->
                makeRaylibType(resolveAlias(param.type))
            }
        }
    }

    private fun makeRaylibType(param: String) = when (param) {
        "bool" -> RaylibValueType.makeArgBoolean(mgr)
        "char", "unsigned char" -> RaylibValueType.makeArgChar(mgr)
        "int", "unsigned int" -> RaylibValueType.makeArgInt(mgr)
        "float" -> RaylibValueType.makeArgFloat(mgr)
        "double" -> RaylibValueType.makeArgDouble(mgr)
        "const char *" -> RaylibValueType.makeArgString(mgr)
        "void" -> RaylibValueType.makeArgVoid(mgr)
        "void *" -> RaylibValueType.makeArgOpaquePointer(mgr)
//        "Rectangle *", "GlyphInfo *" -> makeTypedPointer(param)
        else -> makeStructType(mgr, param)
    }

//    private fun makeTypedPointer(param: String): RaylibValueType<FfiPointer> {
//
//    }

    private fun makeStructType(mgr: FfiManager, param: String): RaylibValueType<*> {
        val matchResult = "^([A-Za-z0-9_]+) \\*$".toRegex().find(param)
        if (matchResult != null) {
            val s = matchResult.groups.get(1)!!.value
            val str = raylibStructs[resolveAlias(s)] ?: throw RaylibTypeNotFoundException("Type not found: pointer ${s}", param)
            if (s in listOf("Rectangle", "GlyphInfo")) {
                return RaylibValueType.makeArgPointerToList(mgr, str)
            } else {
                return RaylibValueType.makeArgPointerToStruct(mgr, str)
            }
        } else {
            val str = raylibStructs[resolveAlias(param)] ?: throw RaylibTypeNotFoundException("Type not found: ${param}", param)
            return RaylibValueType.makeArgStruct(mgr, str)
        }
    }

    companion object {
        val COLOUR_MATCH_REGEX = "^CLITERAL\\(Color\\) *\\{ *(\\d+) *, *(\\d+) *, *(\\d+) *, *(\\d+) *} *$".toRegex()
    }
}

data class RaylibStructFieldDefinition<T>(val type: RaylibValueType<T>, val name: String)

abstract class RaylibValueType<T>(val type: InputArgType<T>) {
    abstract fun convertInput(value: APLValue, pos: Position): Any
    abstract fun convertToKap(value: Any?): APLValue

    abstract fun makeDefaultValue(): APLValue
    open fun isVoid(): Boolean = false

    open fun deriveOutputArg(): RaylibValueType<T> = error("Cannot derive output type: ${this::class.simpleName}")

    companion object {
        fun makeArgBoolean(mgr: FfiManager): RaylibValueType<Boolean> {
            return object : RaylibValueType<Boolean>(mgr.makeInputArgBoolean()) {
                override fun convertInput(value: APLValue, pos: Position) = value.asBoolean(pos)
                override fun convertToKap(value: Any?) = (value as Boolean).makeAPLNumberAsBoolean()
                override fun makeDefaultValue() = APLLONG_0
            }
        }

        fun makeArgChar(mgr: FfiManager): RaylibValueType<Byte> {
            return object : RaylibValueType<Byte>(mgr.makeInputArgChar()) {
                override fun convertInput(value: APLValue, pos: Position): Byte {
                    return value.ensureNumber(pos).asLong(pos).let { v ->
                        if (v < 0 || v > 255) {
                            throwAPLException(APLIllegalArgumentException("Value does not fit in a byte: ${v}", pos))
                        } else {
                            v.toByte()
                        }
                    }
                }

                override fun convertToKap(value: Any?) = (value as Byte).toInt().makeAPLNumber()
                override fun makeDefaultValue() = APLLONG_0
            }
        }

        fun makeArgInt(mgr: FfiManager): RaylibValueType<Int> {
            return object : RaylibValueType<Int>(mgr.makeInputArgInt()) {
                override fun convertInput(value: APLValue, pos: Position) = value.ensureNumber(pos).asInt(pos)
                override fun convertToKap(value: Any?) = (value as Int).makeAPLNumber()
                override fun makeDefaultValue() = APLLONG_0
            }
        }

        fun makeArgFloat(mgr: FfiManager): RaylibValueType<Float> {
            return object : RaylibValueType<Float>(mgr.makeInputArgFloat()) {
                override fun convertInput(value: APLValue, pos: Position) = value.ensureNumber(pos).asDouble(pos).toFloat()
                override fun convertToKap(value: Any?) = (value as Float).toDouble().makeAPLNumber()
                override fun makeDefaultValue() = APLDOUBLE_0
            }
        }

        fun makeArgDouble(mgr: FfiManager): RaylibValueType<Double> {
            return object : RaylibValueType<Double>(mgr.makeInputArgDouble()) {
                override fun convertInput(value: APLValue, pos: Position) = value.ensureNumber(pos).asDouble(pos)
                override fun convertToKap(value: Any?) = (value as Double).makeAPLNumber()
                override fun makeDefaultValue() = APLDOUBLE_0
            }
        }

        fun makeArgString(mgr: FfiManager): RaylibValueType<String?> {
            return object : RaylibValueType<String?>(mgr.makeInputArgString()) {
                override fun convertInput(value: APLValue, pos: Position) = value.toStringValue(pos)
                override fun convertToKap(value: Any?) = APLString.make(value as String)
                override fun makeDefaultValue() = APLString.EMPTY_STRING
            }
        }

        fun makeArgVoid(mgr: FfiManager): RaylibValueType<FfiManager.VoidWrapper> {
            return object : RaylibValueType<FfiManager.VoidWrapper>(mgr.makeInputArgTypeVoid()) {
                override fun convertInput(value: APLValue, pos: Position) = error("Attempt to convert void arg")
                override fun convertToKap(value: Any?) = APLNilValue
                override fun makeDefaultValue() = APLNilValue
                override fun isVoid() = true
            }
        }

        fun makeArgStruct(mgr: FfiManager, str: RaylibStructDefinition): RaylibValueType<StructResult> {
            return StructResultRaylibValueType(mgr, str)
        }

        class StructResultRaylibValueType(
            mgr: FfiManager,
            private val str: RaylibStructDefinition
        ) : RaylibValueType<StructResult>(mgr.makeInputArgStruct(str.structDefinition)) {
            override fun convertInput(value: APLValue, pos: Position): Any {
                if (value.dimensions.size != 1 || value.dimensions[0] != str.fieldList.size) {
                    throwAPLException(
                        InvalidDimensionsException(
                            "While encoding struct of type ${str.name}: Expected 1-dimensional array of ${str.fieldList.size} elements. Got dimensions: ${value.dimensions}",
                            pos))
                }
                val value0 = value.collapse()
                val valueList = Array(str.fieldList.size) { i ->
                    val field = str.fieldList[i].second
                    val v = value0.valueAt(i)
                    field.type.convertInput(v, pos)
                }
                return str.structDefinition.makeStructResult(valueList)
            }

            override fun convertToKap(value: Any?): APLValue {
                val structResult = value as StructResult
                val elements = Array(str.fieldList.size) { i ->
                    str.fieldList[i].second.type.convertToKap(structResult.getAtIndex(i))
                }
                return RaylibStructValue(elements, str)
            }

            override fun makeDefaultValue(): APLValue {
                val array = Array(str.fieldList.size) { i ->
                    val field = str.fieldList[i].second
                    field.type.makeDefaultValue()
                }
                return RaylibStructValue(array, str)
            }
        }

        fun makeArgPointerToStruct(mgr: FfiManager, str: RaylibStructDefinition): RaylibValueType<StructResult> {
            return PointerToStructResultRaylibValueType(mgr, str)
        }

        fun makeArgOpaquePointer(mgr: FfiManager): RaylibValueType<FfiPointer> {
            return object : RaylibValueType<FfiPointer>(mgr.makeInputArgPtr()) {
                override fun convertInput(value: APLValue, pos: Position): Any {
                    if (value !is FfiPointerKapValue) {
                        throwAPLException(APLEvalException("Expected a pointer. Got: ${value::class.simpleName}"))
                    }
                    return value.value
                }

                override fun convertToKap(value: Any?) = FfiPointerKapValue(value as FfiPointer)
                override fun makeDefaultValue() = FfiPointerKapValue(mgr.makeNullPtr())
            }
        }

        fun makeArgPointerToList(mgr: FfiManager, str: RaylibStructDefinition): RaylibValueType<FfiPointer> {
            return PointerToListRaylibType(mgr, str)
        }

        class PointerToStructResultRaylibValueType private constructor(
            private val mgr: FfiManager,
            private val str: RaylibStructDefinition,
            forOutput: Boolean
        ) : RaylibValueType<StructResult>(mgr.makeInputArgPointerToArg(mgr.makeInputArgStruct(str.structDefinition))) {
            constructor(mgr: FfiManager, str: RaylibStructDefinition) : this(mgr, str, false)

            private val structResultValueType by lazy { StructResultRaylibValueType(mgr, str) }

            override fun deriveOutputArg(): RaylibValueType<StructResult> {
                return PointerToStructResultRaylibValueType(mgr, str, true)
            }

            override fun convertInput(value: APLValue, pos: Position): Any {
                return structResultValueType.convertInput(value, pos)
            }

            override fun convertToKap(value: Any?): APLValue {
                return structResultValueType.convertToKap(value)
            }

            override fun makeDefaultValue(): APLValue {
                return structResultValueType.makeDefaultValue()
            }
        }

        class PointerToListRaylibType(
            private val mgr: FfiManager,
            private val str: RaylibStructDefinition
        ) : RaylibValueType<FfiPointer>(mgr.makeInputArgPtr()) {
            override fun convertInput(value: APLValue, pos: Position): Any {
                if (value !is RaylibStructPointerWrapper) {
                    throwAPLException(APLEvalException("Expected a pointer to struct. Got: ${value::class.simpleName}"))
                }
                return value.ptr
            }

            override fun convertToKap(value: Any?) = RaylibStructPointerWrapper(str, value as FfiPointer)
            override fun makeDefaultValue() = RaylibStructPointerWrapper(str, mgr.makeNullPtr())
        }
    }
}

class RaylibStructPointerWrapper(val type: RaylibStructDefinition, val ptr: FfiPointer) : APLSingleValue() {
    override fun formatted(style: FormatStyle) = "[${type.name} pointer]"
    override val kapClass get() = type.kapClassAsPointer
    override fun typeQualifiedHashCode() = ptr.hashCode()
}

class RaylibAPLFunction(
    val module: RaylibModule,
    val name: String,
    val fnName: FfiFunctionDescriptor<*>,
    val returnArgType: RaylibValueType<*>,
    val functionArgs: List<RaylibValueType<*>>,
    spec: ArgSpecialisation
) : APLFunctionDescriptor {
    private val returnProcessor: (FfiFunctionDescriptor.CallResult<*>) -> APLValue
    private val outputFlags = spec.params.filter { it.isOut }.map { it.index }.toHashSet()

    init {
        val valueConverters = ArrayList<(FfiFunctionDescriptor.CallResult<*>) -> APLValue>()
        if (!returnArgType.isVoid()) {
            valueConverters.add { returnValue -> returnArgType.convertToKap(returnValue.result) }
        }
        functionArgs.forEachIndexed { i, a ->
            val index = i
            val arg = a
            if (outputFlags.contains(index)) {
                valueConverters.add { returnValue -> arg.convertToKap(returnValue.outputs!![index]) }
            }
        }
        returnProcessor = when (valueConverters.size) {
            0 -> { _ -> APLNilValue }
            1 -> { resultValue -> valueConverters[0](resultValue) }
            else -> { resultValue -> APLList(valueConverters.map { converter -> converter(resultValue) }) }
        }
    }

    inner class RaylibAPLFunctionImpl(instantiation: FunctionInstantiation) : NoAxisAPLFunction(instantiation) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val a0 = a.listify()
            if (a0.listSize() != functionArgs.size) {
                throwAPLException(
                    APLIllegalArgumentException(
                        "Function expects a list of ${functionArgs.size} elements. Argument has size ${a0.listSize()}",
                        pos))
            }
            val args = functionArgs.mapIndexed { i, arg ->
                val v = a0.listElement(i)
                arg.convertInput(v, pos)
            }
            val res = fnName.call(args, outputFlags)
            return returnProcessor(res)
        }

        override val name1Arg get() = name
    }

    override fun make(instantiation: FunctionInstantiation) = RaylibAPLFunctionImpl(instantiation)
}

class RaylibStructDefinition(
    module: RaylibModule,
    val name: String,
    fields: List<RaylibStructFieldDefinition<*>>
) {
    val fieldList: List<Pair<Symbol, RaylibStructFieldDefinition<*>>>
    val structDefinition: FfiStructDefinition

    val kapClassAsStruct = RaylibStructClass("struct${name}")
    val kapClassAsPointer = RaylibPointerClass("ptr${name}")

    init {
        structDefinition = module.mgr.defineStruct(*fields.map { it.type.type }.toTypedArray())
        fieldList = fields.map { f ->
            val kw = module.engine.keywordNamespace.internSymbol(f.name)
            Pair(kw, f)
        }
    }
}

class RaylibStructClass(override val name: String) : ModuleClass()
class RaylibPointerClass(override val name: String) : ModuleClass()

class RaylibStructValue(val elements: Array<APLValue>, val raylibStructDefinition: RaylibStructDefinition) : APLArray() {
    override val dimensions = dimensionsOfSize(elements.size)
    override fun valueAt(p: Int) = elements[p]
    override val metadata by lazy { RaylibStructValueMetadata() }

    inner class RaylibStructValueMetadata : APLValueMetadata {
        override val labels by lazy {
            val labelsList = raylibStructDefinition.fieldList.map { (name, _) -> AxisLabel(name.symbolName) }
            DimensionLabels(listOf(labelsList))
        }
    }
}

class MakeStructRaylibFunction(val module: RaylibModule, val raylibStructDefinition: RaylibStructDefinition) : APLFunctionDescriptor {
    inner class MakeStructRaylibFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val resArray = arrayOfNulls<APLValue>(raylibStructDefinition.fieldList.size)
            fillInResArray(a, resArray)

            for (i in resArray.indices) {
                if (resArray[i] == null) {
                    resArray[i] = raylibStructDefinition.fieldList[i].second.type.makeDefaultValue()
                }
            }

            return RaylibStructValue(resArray.requireNoNulls(), raylibStructDefinition)
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val a0 = a.arrayify()
            if (a0.dimensions.size != 1 || a0.dimensions[0] != raylibStructDefinition.fieldList.size) {
                throwAPLException(APLIllegalArgumentException("Left argument does not match struct definition", pos))
            }
            val resArray = Array<APLValue?>(a0.dimensions.contentSize()) { i ->
                a0.valueAt(i)
            }
            fillInResArray(b, resArray)
            return RaylibStructValue(resArray.requireNoNulls(), raylibStructDefinition)
        }

        private fun fillInResArray(a: APLValue, resArray: Array<APLValue?>) {
            val a0 = a.arrayify()
            if (a0.dimensions.size != 1 || a0.dimensions[0] % 2 != 0) {
                throwAPLException(APLIllegalArgumentException("Argument must be a list of key/value pairs", pos))
            }

            for (i in 0 until a0.dimensions[0] step 2) {
                val key = a0.valueAt(i).ensureSymbol(pos)

                var structDef: RaylibStructFieldDefinition<*>? = null
                var index = 0
                for ((kw, rlStruct) in raylibStructDefinition.fieldList) {
                    if (kw == key.value) {
                        structDef = rlStruct
                        break
                    }
                    index++
                }

                if (structDef == null) {
                    throwAPLException(
                        APLIllegalArgumentException(
                            "Key ${key.value.symbolName} not in structure definition. Expected one of: ${
                                raylibStructDefinition.fieldList.map { it.first.symbolName }.joinToString(", ")
                            }", pos))
                }

                val value = a0.valueAt(i + 1)
                resArray[index] = value
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = MakeStructRaylibFunctionImpl(instantiation)
}
