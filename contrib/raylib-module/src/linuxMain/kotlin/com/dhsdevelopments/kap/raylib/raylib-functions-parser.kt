package com.dhsdevelopments.kap.raylib

import com.dhsdevelopments.kap.ModuleBuilderInitFailedException
import jansson.*
import kotlinx.cinterop.*

@OptIn(ExperimentalForeignApi::class)
fun parseRaylibFile(s: String): RaylibApiRoot {
    memScoped {
        val error = allocArray<json_error_t>(1)
        val root = json_loads(s, 0U, error) ?: throw ModuleBuilderInitFailedException("Error parsing json")
        try {
            if (jsonTypeof(root) != json_type.JSON_OBJECT) {
                throw ModuleBuilderInitFailedException("The toplevel value must be an object")
            }
            return parseToplevelObject(root)
        } finally {
            json_decref(root)
        }
    }
}

@OptIn(ExperimentalForeignApi::class)
fun parseToplevelObject(root: CPointer<json_t>): RaylibApiRoot {
    val apiRoot = RaylibApiRoot()
    iterateJsonObject(root) { key, value ->
        when (key) {
//            "defines" -> parseDefines(apiRoot)
            "structs" -> parseStructs(apiRoot, value)
            "aliases" -> parseAliases(apiRoot, value)
//            "enums" -> parseEnums(apiRoot)
//            "callbacks" -> parseCallbacks(apiRoot)
            "functions" -> parseFunctions(apiRoot, value)
        }
    }
    return apiRoot
}

@OptIn(ExperimentalForeignApi::class)
fun parseAliases(apiRoot: RaylibApiRoot, value: CPointer<json_t>) {
    if (apiRoot.aliases != null) {
        throw ModuleBuilderInitFailedException("Aliases element as already been parsed")
    }
    val res = ArrayList<RaylibAlias>()
    iterateJsonArray(value) { aliasDef ->
        val aliasData = RaylibAlias()
        iterateJsonObject(aliasDef) { key, v0 ->
            when (key) {
                "name" -> aliasData.name = jsonToString(v0)
                "type" -> aliasData.type = jsonToString(v0)
                "description" -> aliasData.description = jsonToString(v0)
            }
        }
        res.add(aliasData)
    }
    apiRoot.aliases = res
}

@OptIn(ExperimentalForeignApi::class)
fun parseStructs(apiRoot: RaylibApiRoot, value: CPointer<json_t>) {
    if (apiRoot.structs != null) {
        throw ModuleBuilderInitFailedException("Structs element as already been parsed")
    }
    val res = ArrayList<RaylibStruct>()
    iterateJsonArray(value) { s ->
        val structData = RaylibStruct()
        iterateJsonObject(s) { key, v0 ->
            when (key) {
                "name" -> structData.name = jsonToString(v0)
                "description" -> structData.description = jsonToString(v0)
                "fields" -> structData.fields = parseStructFields(v0)
            }
        }
        res.add(structData)
    }
    apiRoot.structs = res
}

@OptIn(ExperimentalForeignApi::class)
fun parseStructFields(value: CPointer<json_t>): List<RaylibStructField> {
    val content = ArrayList<RaylibStructField>()
    iterateJsonArray(value) { v0 ->
        val field = RaylibStructField()
        iterateJsonObject(v0) { key, fieldData ->
            when (key) {
                "type" -> field.type = jsonToString(fieldData)
                "name" -> field.name = jsonToString(fieldData)
                "description" -> field.description = jsonToString(fieldData)
            }
        }
        content.add(field)
    }
    return content
}

@OptIn(ExperimentalForeignApi::class)
fun parseFunctions(apiRoot: RaylibApiRoot, value: CPointer<json_t>) {
    if (apiRoot.functions != null) {
        throw ModuleBuilderInitFailedException("Functions element has already been parsed")
    }
    val res = ArrayList<RaylibFunction>()
    iterateJsonArray(value) { f ->
        val fn = RaylibFunction()
        iterateJsonObject(f) { key, v0 ->
            when (key) {
                "name" -> fn.name = jsonToString(v0)
                "description" -> fn.description = jsonToString(v0)
                "returnType" -> fn.returnType = jsonToString(v0)
                "params" -> fn.params = parseParams(v0)
            }
        }
        res.add(fn)
    }
    apiRoot.functions = res
}

@OptIn(ExperimentalForeignApi::class)
fun parseParams(value: CPointer<json_t>): List<RaylibFunctionParam>? {
    val content = ArrayList<RaylibFunctionParam>()
    iterateJsonArray(value) { v0 ->
        val p = RaylibFunctionParam()
        iterateJsonObject(v0) { key, paramValue ->
            when (key) {
                "type" -> p.type = jsonToString(paramValue)
                "name" -> p.name = jsonToString(paramValue)
            }
        }
        content.add(p)
    }
    return content
}

@OptIn(ExperimentalForeignApi::class)
private inline fun iterateJsonObject(obj: CPointer<json_t>, fn: (key: String, value: CPointer<json_t>) -> Unit) {
    if (jsonTypeof(obj) != json_type.JSON_OBJECT) {
        throw ModuleBuilderInitFailedException("Expected object")
    }
    var iterator = json_object_iter(obj)
    while (iterator != null) {
        val key = json_object_iter_key(iterator) ?: throw ModuleBuilderInitFailedException("Null key")
        val value = json_object_iter_value(iterator) ?: throw ModuleBuilderInitFailedException("Null value from iterator")
        fn(key.toKString(), value)
        iterator = json_object_iter_next(obj, iterator)
    }
}

@OptIn(ExperimentalForeignApi::class)
private inline fun iterateJsonArray(obj: CPointer<json_t>, fn: (value: CPointer<json_t>) -> Unit) {
    if (jsonTypeof(obj) != json_type.JSON_ARRAY) {
        throw ModuleBuilderInitFailedException("Expected array")
    }
    val size = json_array_size(obj)
    if (size > Int.MAX_VALUE.toUInt()) {
        throw ModuleBuilderInitFailedException("Array too large: ${size}")
    }
    for (i in 0 until size.toInt()) {
        val v = json_array_get(obj, i.toULong()) ?: throw ModuleBuilderInitFailedException("Null value in array")
        fn(v)
    }
}

@OptIn(ExperimentalForeignApi::class)
fun jsonToString(value: CPointer<json_t>): String {
    val s = json_string_value(value) ?: throw ModuleBuilderInitFailedException("Got null when string was expected")
    return s.toKString()
}
