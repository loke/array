package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.repl.GenericReplBuilder
import java.util.*

abstract class JvmReplBuilder : GenericReplBuilder() {
    override fun initBuild() {
        super.initBuild()
        ServiceLoader.load(KapModuleBuilder::class.java).let { loader ->
            addModuleBuilders(loader.toList())
        }
    }
}
