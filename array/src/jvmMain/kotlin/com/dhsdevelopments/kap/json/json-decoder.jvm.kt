package com.dhsdevelopments.kap.json

import com.google.gson.Strictness
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import java.io.IOException
import java.io.StringReader

class JvmJsonDecoder : JsonDecoder {
    override fun decodeJsonFromString(source: String): JsonElement {
        val jsonReader = JsonReader(StringReader(source))
        jsonReader.strictness = Strictness.STRICT
        try {
            return parseNextElement(jsonReader)
        } catch (e: IOException) {
            throw JsonParseException("Error parsing json: ${e.message}", e)
        }
    }

    private fun parseNextElement(jsonReader: JsonReader): JsonElement {
        return when (val token = jsonReader.peek()) {
            JsonToken.STRING -> JsonString(jsonReader.nextString())
            JsonToken.NUMBER -> JsonNumber(jsonReader.nextDouble())
            JsonToken.BOOLEAN -> JsonBoolean(jsonReader.nextBoolean())
            JsonToken.NULL -> {
                jsonReader.nextNull()
                JsonNull
            }
            JsonToken.BEGIN_ARRAY -> parseJsonArray(jsonReader)
            JsonToken.BEGIN_OBJECT -> parseJsonObject(jsonReader)
            else -> throw JsonParseException("Unexpected token: ${token}")
        }
    }

    private fun parseJsonArray(jsonReader: JsonReader): JsonArray {
        jsonReader.beginArray()
        val result = ArrayList<JsonElement>()
        while (true) {
            if (jsonReader.peek() == JsonToken.END_ARRAY) {
                break
            }
            result.add(parseNextElement(jsonReader))
        }
        jsonReader.endArray()
        return JsonArray(result)
    }

    private fun parseJsonObject(jsonReader: JsonReader): JsonObject {
        jsonReader.beginObject()
        val result = ArrayList<Pair<String, JsonElement>>()
        while (true) {
            if (jsonReader.peek() == JsonToken.END_OBJECT) {
                break
            }
            val key = jsonReader.nextName()
            val value = parseNextElement(jsonReader)
            result.add(Pair(key, value))
        }
        jsonReader.endObject()
        return JsonObject(result)
    }
}

actual fun makeJsonDecoder(): JsonDecoder = JvmJsonDecoder()
