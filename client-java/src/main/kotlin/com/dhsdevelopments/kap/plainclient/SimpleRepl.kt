package com.dhsdevelopments.kap.plainclient

import com.dhsdevelopments.kap.JvmReplBuilder
import com.dhsdevelopments.kap.KeyboardInput
import com.dhsdevelopments.kap.editor.JlineKeyboardInput
import com.dhsdevelopments.kap.ffi.FfiModule
import com.dhsdevelopments.kap.findInstallPath
import com.dhsdevelopments.kap.makeKeyboardInput
import com.dhsdevelopments.kap.repl.AbstractGenericRepl
import com.dhsdevelopments.kap.repl.JvmReplExceptionTransformer
import com.dhsdevelopments.kap.repl.ReplFailedException
import kotlin.system.exitProcess

class SimpleRepl {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val builder = SimpleReplBuilder()
            builder.exceptionTransformer = JvmReplExceptionTransformer()
            val repl = builder.build(args) ?: return
            try {
                repl.engine.standardOutput = repl.makeDefaultStdout()
                engineInit(repl)
                repl.engine.commandManager.registerQuitHandler {
                    repl.close()
                    exitProcess(0)
                }
                repl.init()
                initModules(repl)
                repl.loadStartupFiles()
                if (!repl.inhibitRepl()) {
                    repl.mainLoop()
                }
            } catch (e: ReplFailedException) {
                println("REPL error: ${e.message}")
            } finally {
                repl.close()
            }
        }

        private fun engineInit(repl: AbstractGenericRepl) {
            findInstallPath()?.let { appPath ->
                repl.engine.addLibrarySearchPath("${appPath}/standard-lib")
            }
        }

        private fun initModules(repl: AbstractGenericRepl) {
            repl.engine.addModule(FfiModule.make()!!)
        }
    }
}

class SimpleReplBuilder : JvmReplBuilder() {
    override fun makeKbInput(repl: AbstractGenericRepl): KeyboardInput {
        return if (disableLineeditor) {
            makeKeyboardInput(repl.engine) ?: error("Can't create keyboard input")
        } else {
            val input = JlineKeyboardInput(repl)
            input.registerSignalHandler { repl.interruptEvaluation() }
            input
        }
    }
}
