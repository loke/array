package com.dhsdevelopments.kap.cryptography

import com.dhsdevelopments.kap.*
import dev.whyoleg.cryptography.CryptographyProvider
import dev.whyoleg.cryptography.CryptographyProviderApi

class CryptographyException(message: String, pos: Position? = null) : APLEvalException(message, pos)

class CryptographyModule : KapModule {
    override val name get() = "cryptography"

    lateinit var provider: CryptographyProvider

    lateinit var digestClass: DigestKapClass

    @OptIn(CryptographyProviderApi::class)
    override fun init(engine: Engine) {
        provider = CryptographyProvider.Registry.registeredProviders.firstOrNull()
            ?: throw ModuleInitSkipped("No cryptography providers registered, cryptography functions not available")

        val ns = engine.makeNamespace("crypto")
        digestClass = DigestKapClass(engine)
        engine.classManager.registerSupplementarySystemClass(ns.internSymbol("digest"), digestClass)
        engine.registerFunction(ns.internAndExport("makeDigest"), MakeDigestFunction(this))
    }
}
