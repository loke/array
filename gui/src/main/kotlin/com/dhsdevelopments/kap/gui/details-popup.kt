package com.dhsdevelopments.kap.gui

import com.dhsdevelopments.kap.gui.styledarea.NodeEditorContent
import com.dhsdevelopments.kap.gui.styledarea.TextStyle
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.layout.Pane
import javafx.scene.layout.VBox
import javafx.scene.text.Text
import javafx.stage.Popup
import org.fxmisc.richtext.TextExt
import java.util.*

class DetailsPopupManager(val client: Client) {
    private val detailsContent = DetailsContentPanel()
    private val popup = Popup().apply { content.add(detailsContent) }
    private var timerTask: TimerTask? = null

    fun showDetailsPopup(text: String, xPos: Double, yPos: Double) {
        hideDetailsPopup()
        val task = DelayDetailsPopupTask(text, xPos, yPos)
        timerTask = task
        client.timer.schedule(task, 500L)
    }

    fun hideDetailsPopup() {
        popup.hide()
        timerTask?.let { task ->
            task.cancel()
            timerTask = null
        }
    }

    inner class DelayDetailsPopupTask(val detailsText: String, val x: Double, val y: Double) : TimerTask() {
        override fun run() {
            Platform.runLater {
                if (timerTask == this) {
                    detailsContent.text.text = detailsText
                    popup.show(client.stage, x, y)
                }
            }
        }
    }
}

class DetailsContentPanel() : Pane() {
    val text = Text().apply {
        styleClass.add("error-details-popup-description")
        wrappingWidth = 600.0
    }

    init {
        stylesheets.add("/com/dhsdevelopments/kap/gui/interactor.css")
        styleClass.add("error-details-popup-panel")
        styleClass.add("popup-panel")

        val vbox = VBox()
        vbox.children.add(text)
        children.add(vbox)
    }
}

class DetailsButtonEditorContentEntry(val client: Client, val detailsText: String) : NodeEditorContent("[Details]") {
    override fun createNode(renderContext: ClientRenderContext, style: TextStyle): Node {
        return TextExt("[Details]").apply {
//            styleClass.add("error-details-button")
            onMouseEntered = EventHandler { event -> showPopup(event.screenX + 5.0, event.screenY + 5.0) }
            onMouseExited = EventHandler { client.detailsPopupManager.hideDetailsPopup() }
            style.styleContent(this)
        }
    }

    private fun showPopup(x: Double, y: Double) {
        client.detailsPopupManager.showDetailsPopup(detailsText, x, y)
    }
}
