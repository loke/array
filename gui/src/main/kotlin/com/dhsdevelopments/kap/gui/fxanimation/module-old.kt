package com.dhsdevelopments.kap.gui.fxanimation

import com.dhsdevelopments.kap.*
import javafx.animation.Animation
import javafx.animation.FadeTransition
import javafx.animation.ParallelTransition
import javafx.animation.PathTransition
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.geometry.Point2D
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Pane
import javafx.scene.shape.LineTo
import javafx.scene.shape.MoveTo
import javafx.scene.shape.Path
import javafx.scene.text.Font
import javafx.scene.text.Text
import javafx.stage.Stage
import javafx.util.Duration

class AnimationContent {
    private val stage = Stage()
    val pane = Pane()
    private var currObjects: AnimatedArrayElements = AnimatedArrayElements(emptyMap())

    val font: Font

    init {
        font = Font.font("serif", 20.0)

        stage.title = "Animation"
        val border = BorderPane().apply {
            center = pane
        }
        val scene = Scene(border, 800.0, 800.0)
        stage.scene = scene
        stage.show()
    }

    fun close() {
        stage.close()
    }

    fun clear() {
        currObjects = AnimatedArrayElements(emptyMap())
        pane.children.clear()
    }

    fun add2DArray(objects: Map<APLValue.APLValueKey, AnimationElement>) {
        val oldObjects = currObjects
        val newObjects = AnimatedArrayElements(objects)
        currObjects = newObjects
        val animation = newObjects.createAnimation(oldObjects)
        pane.children.removeAll(oldObjects.objects.values.mapNotNull { o -> o.primaryNode })
        animation.play()
    }

//    private fun iterateWithLayout(v: APLValue, fn: (element: APLValue, x: Double, y: Double, w: Double, h: Double) -> Unit) {
//        require(v.dimensions.size == 2) { "Argument is not 2-dimensional. Got: ${v.dimensions}" }
//        val numCols = v.dimensions[1]
//        val numRows = v.dimensions[0]
//        val nodeWidth = pane.width / numCols
//        val nodeHeight = pane.height / numRows
//        v.iterateMembersWithPosition { element, i ->
//            fn(element, (i % numCols) * nodeWidth, (i / numCols) * nodeHeight, nodeWidth, nodeHeight)
//        }
//    }
//
//    fun add2DArray(v: APLValue) {
//        pane.children.clear()
//        objects.clear()
//        iterateWithLayout(v) { element, x, y, w, h ->
//            val key = element.makeTypeQualifiedKey()
//            val anim = AnimationElement(this, element, x, y, w, h)
//            objects[key] = anim
//            pane.children.add(anim.dispNode)
//        }
//    }
//
//    fun update2DArray(v: APLValue) {
//        val animations = ArrayList<Animation>()
//        iterateWithLayout(v) { element, x, y, w, h ->
//            val key = element.makeTypeQualifiedKey()
//            val prevObj = objects[key]
//            if (prevObj != null) {
//                val anim = prevObj.update(x, y, w, h)
//                if (anim != null) {
//                    animations.add(anim)
//                }
//            } else {
//                val anim = AnimationElement(this, element, x, y, w, h)
//                objects[key] = anim
//                pane.children.add(anim.dispNode)
//            }
//        }
//        if (animations.isNotEmpty()) {
//            val transition = ParallelTransition()
//            transition.children.addAll(animations)
//            transition.play()
//        }
//    }
}

//class AnimationElement(val owner: AnimationContent, element: APLValue, x: Double, y: Double, w: Double, h: Double) {
//    val dispNode: Node
//
//    init {
//        dispNode = Text(element.formatted()).apply {
//            font = owner.font
//        }
//        val newPosition = computePosition(x, y, w, h)
//        dispNode.layoutX = newPosition.x
//        dispNode.layoutY = newPosition.y
//    }
//
//    private fun computePosition(x: Double, y: Double, w: Double, h: Double): Point2D {
//        val bounds = dispNode.layoutBounds
//        return Point2D(x + (w - bounds.width) / 2, y + (h - bounds.height) / 2)
//    }
//
//    fun update(x: Double, y: Double, w: Double, h: Double): Animation? {
//        val newPosition = computePosition(x, y, w, h)
//        val oldX = dispNode.layoutX
//        val oldY = dispNode.layoutY
//        dispNode.layoutX = newPosition.x
//        dispNode.layoutY = newPosition.y
//        return if (oldX != newPosition.x || oldY != newPosition.y) {
//            PathTransition().apply {
//                node = dispNode
//                duration = Duration.seconds(2.0)
//                path = Path().apply {
//                    elements.add(MoveTo(oldX - newPosition.x, oldY - newPosition.y))
//                    elements.add(LineTo(0.0, 0.0))
//                }
//            }
//        } else {
//            null
//        }
//    }
//}

class AnimatedArrayElements(val objects: Map<APLValue.APLValueKey, AnimationElement>) {
    fun createAnimation(oldObjects: AnimatedArrayElements): Animation {
        val parallelTransition = ParallelTransition()
        objects.asSequence().map { v -> v.value }.forEach { element ->
            val transition = element.animateElement(oldObjects)
            parallelTransition.children.add(transition)
        }
        return parallelTransition
    }
}

private val DEFAULT_DURATION = Duration.seconds(2.0)

class AnimationElement(
    val destX: Double,
    val destY: Double,
    val key: APLValue.APLValueKey,
    val value: APLValue,
    val origin: List<APLValue.APLValueKey>
) {
    private lateinit var owner: AnimationContent
    var primaryNode: Node? = null

    fun initOwner(content: AnimationContent) {
        owner = content
    }

    private fun makeTextNode(): Pair<Text, Point2D> {
        val node = Text(value.formatted(FormatStyle.PLAIN))
        node.font = owner.font
        val bounds = computePos(node, destX, destY)
        node.layoutX = bounds.x
        node.layoutY = bounds.y
        owner.pane.children.add(node)
        return Pair(node, bounds)
    }

    private fun computePos(node: Text, x: Double, y: Double): Point2D {
        val bounds = node.layoutBounds
        val margin = 50.0
        return Point2D(
            (x * (owner.pane.width - margin * 2)) - (bounds.width / 2) + margin,
            (y * (owner.pane.height - margin * 2)) - (bounds.height / 2) + margin + node.baselineOffset)
    }

    fun animateElement(oldObjects: AnimatedArrayElements): Animation {
        val oldElements = origin.mapNotNull { originElement -> oldObjects.objects[originElement] }
        return if (oldElements.isEmpty()) {
            FadeTransition().apply {
                val (newNode, _) = makeTextNode()
                node = newNode
                primaryNode = node
                duration = DEFAULT_DURATION
                fromValue = 0.0
                toValue = 1.0
            }
        } else {
            val parallelTransition = ParallelTransition()
            val nodesToRemove = ArrayList<Node>()
            oldElements.forEach { e ->
                val (newNode, newNodePos) = makeTextNode()
                nodesToRemove.add(newNode)
                val bounds = computePos(newNode, e.destX, e.destY)
                val rx = bounds.x - newNodePos.x
                val ry = bounds.y - newNodePos.y
                if (rx == 0.0 && ry == 0.0) {
                    //parallelTransition.children.add(PauseTransition(DEFAULT_DURATION))
                } else {
                    val transition = PathTransition().apply {
                        node = newNode
                        duration = DEFAULT_DURATION
                        path = Path().apply {
                            elements.add(MoveTo(bounds.x - newNodePos.x, bounds.y - newNodePos.y))
                            elements.add(LineTo(0.0, 0.0))
                        }
                    }
                    parallelTransition.children.add(transition)
                }
            }
            primaryNode = nodesToRemove.removeFirst()
            parallelTransition.onFinished = EventHandler { owner.pane.children.removeAll(nodesToRemove) }
            parallelTransition
        }
    }
}

class FxAnimationModule : KapModule {
    override val name get() = "fx-animation"

    private var currContent: AnimationContent? = null

    override fun init(engine: Engine) {
        val ns = engine.makeNamespace("anim")
        engine.registerFunction(ns.internAndExport("add"), AddArrayFunction(this))
        engine.registerFunction(ns.internAndExport("clear"), ClearArrayFunction(this))
    }

    override fun close() {
        Platform.runLater {
            currContent?.close()
        }
    }

    fun withAnimationContent(fn: (AnimationContent) -> Unit) {
        Platform.runLater {
            val content = currContent ?: AnimationContent().also { content ->
                currContent = content
            }
            fn(content)
        }
    }
}

class AddArrayFunction(val module: FxAnimationModule) : APLFunctionDescriptor {
    inner class AddArrayFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val a0 = a.collapse().listify().ensureListSize(pos, 3, 1)
            val values = a0[0]
            if (values.dimensions.size != 2) {
                throwAPLException(
                    InvalidDimensionsException(
                        "Only 2-dimensional arrays can be rendered. Input dimensions: ${values.dimensions}",
                        pos))
            }

            val keys = if (a0.size < 2) {
                values
            } else {
                val keys0 = a0[1]
                if (keys0 is APLNilValue) {
                    values
                } else {
                    if (!keys0.dimensions.compareEquals(values.dimensions)) {
                        throwAPLException(
                            InvalidDimensionsException(
                                "Dimensions does not match. Keys array: ${keys0.dimensions}, values array: ${values.dimensions}",
                                pos))
                    }
                    keys0
                }
            }

            val origins = if (a0.size < 3) {
                keys
            } else {
                val origins0 = a0[2]
                if (origins0 is APLNilValue) {
                    keys
                } else {
                    if (!origins0.dimensions.compareEquals(values.dimensions)) {
                        throwAPLException(
                            InvalidDimensionsException(
                                "Dimensions does not match. Origin array: ${origins0.dimensions}, values array: ${values.dimensions}",
                                pos))
                    }
                    origins0
                }
            }

            val multipliers = values.dimensions.multipliers()

            val animObjects = HashMap<APLValue.APLValueKey, AnimationElement>()
            values.iterateMembersWithPosition { value, i ->
                val key = keys.valueAt(i).makeNonTypeQualifiedKey()
                val originElements = origins.valueAt(i).membersSequence().map(APLValue::makeNonTypeQualifiedKey).toList()
                val p = multipliers.positionFromIndex(i)
                require(p.size == 2)
                val x = p[1].toDouble() / (values.dimensions[1] - 1)
                val y = p[0].toDouble() / (values.dimensions[0] - 1)
                val old = animObjects.put(key, AnimationElement(x, y, key, value, originElements))
                if (old != null) {
                    throwAPLException(APLEvalException("Duplicate elements with key ${key.value.formatted(FormatStyle.PLAIN)}", pos))
                }
            }

            module.withAnimationContent { content ->
                animObjects.values.forEach { e -> e.initOwner(content) }
                content.add2DArray(animObjects)
            }
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = AddArrayFunctionImpl(instantiation)
}

class ClearArrayFunction(val module: FxAnimationModule) : APLFunctionDescriptor {
    inner class ClearArrayFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            module.withAnimationContent { content ->
                content.clear()
            }
            return APLNullValue
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ClearArrayFunctionImpl(instantiation)
}
