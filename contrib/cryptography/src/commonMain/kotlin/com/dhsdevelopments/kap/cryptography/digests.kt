package com.dhsdevelopments.kap.cryptography

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.builtins.KotlinObjectWrappedValue
import com.dhsdevelopments.kap.builtins.ensureType
import dev.whyoleg.cryptography.CryptographyAlgorithmId
import dev.whyoleg.cryptography.DelicateCryptographyApi
import dev.whyoleg.cryptography.algorithms.*

abstract class KotlinObjectWrappedValueClass : ModuleClass()

class DigestKapClass(engine: Engine) : KotlinObjectWrappedValueClass() {
    override val name get() = "digest"

    private val encodeSym = engine.keywordNamespace.internSymbol("encode")

    private val encodeMethod = KapMethod { context, objRef, a, pos ->
        objRef as DigestKapValue
        val hasher = objRef.value.hasher()
        val result = hasher.hashBlocking(a.asByteArray(pos))
        APLArrayByte(dimensionsOfSize(result.size), result)
    }

    override fun resolveMethod(engine: Engine, ref: APLValue, methodName: Symbol, pos: Position): KapMethod {
        return when (methodName) {
            encodeSym -> encodeMethod
            else -> throwAPLException(MethodNotFoundException(methodName, pos))
        }
    }
}

class DigestKapValue(module: CryptographyModule, digest: Digest) : KotlinObjectWrappedValue<Digest>(digest) {
    override fun formatted(style: FormatStyle) = "[digest: ${value.id.name}]"
    override val kapClass = module.digestClass

    companion object {
        fun ensureValue(a: APLValue, pos: Position): Digest = ensureType(a, "digest", pos)
    }
}

class MakeDigestFunction(val module: CryptographyModule) : APLFunctionDescriptor {
    inner class MakeHasherFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        val nameToAlgorithm: Map<Symbol, CryptographyAlgorithmId<Digest>>

        init {
            fun makeKw(name: String) = pos.engine.internSymbol(name, pos.engine.keywordNamespace)
            @OptIn(DelicateCryptographyApi::class)
            nameToAlgorithm = mapOf(
                makeKw("SHA256") to SHA256,
                makeKw("SHA512") to SHA512,
                makeKw("SHA1") to SHA1,
                makeKw("MD5") to MD5)
        }

        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val name = a.ensureSymbol(pos).value
            val algorithm = nameToAlgorithm[name] ?: throwAPLException(CryptographyException("Invalid algorithm: ${name.nameWithNamespace}", pos))
            val digest = module.provider.get(algorithm)
            return DigestKapValue(module, digest)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = MakeHasherFunctionImpl(instantiation)
}
