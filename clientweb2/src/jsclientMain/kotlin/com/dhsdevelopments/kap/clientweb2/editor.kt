package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.edit.*
import com.dhsdevelopments.kap.jsObject2
import com.dhsdevelopments.kap.keyboard.ExtendedCharsKeyboardInput
import com.dhsdevelopments.kap.setTimeout
import kotlinx.dom.addClass
import kotlinx.dom.removeClass
import kotlinx.serialization.json.Json
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLElement
import org.w3c.dom.Worker
import org.w3c.dom.events.KeyboardEvent
import kotlin.js.Promise

@Suppress("ClassName")
@JsModule("@codemirror/view")
@JsNonModule
external object codemirrorView {
    class EditorView(config: dynamic) {
        var state: codemirrorState.EditorState
        fun dispatch(transaction: dynamic)
        fun focus()

        companion object {
            fun theme(config: dynamic): dynamic
            val updateListener: dynamic
            val decorations: dynamic
        }
    }

    class Decoration : codemirrorState.RangeValue {
        companion object {
            val none: Decoration
            fun mark(spec: dynamic): Decoration
            fun set(of: Array<codemirrorState.Range<Decoration>>, sort: Boolean?): codemirrorState.RangeSet<Decoration>
        }
    }

    class ViewUpdate {
        var changes: dynamic
    }

    interface PluginValue {
        var update: ((ViewUpdate) -> Unit)?
        var destroy: (() -> Unit)?
    }

    class ViewPlugin<T : PluginValue> {
        var extension: dynamic /*Extension*/
    }

    interface PluginSpec<T : PluginValue> {
        var provide: ((ViewPlugin<T>) -> dynamic /*Extension*/)?
        var decorations: ((T) -> codemirrorState.RangeSet<Decoration>)?
    }

    object keymap {
        fun of(config: dynamic): dynamic
    }
}

@Suppress("ClassName")
@JsModule("@codemirror/state")
@JsNonModule
external object codemirrorState {
    class EditorState {
        var doc: dynamic
        var selection: dynamic
        var extensions: dynamic

        fun <T> field(f: StateField<T>, require: Boolean): T?

        companion object {
            fun create(config: dynamic): EditorState
        }
    }

    class RangeSetBuilder<T : RangeValue> {
        fun add(from: Int, to: Int, value: T)
        fun finish(): RangeSet<T>
    }

    open class RangeValue {
        fun range(from: Int, to: Int?): Range<RangeValue>
    }

    class Range<T : RangeValue> {
        var from: Int
        var to: Int
        var value: T
    }

    class RangeSet<T : RangeValue> {
        var size: Int
        fun map(changes: dynamic): RangeSet<T>
        fun update(updateSpec: dynamic): RangeSet<T>

        companion object {
            val empty: RangeSet<*>
        }
    }

    class StateEffect {
        var value: dynamic

        companion object {
            val appendConfig: StateEffectType
            fun define(config: dynamic): StateEffectType
        }
    }

    class StateEffectType {
        fun of(value: dynamic): StateEffect
    }

    class StateField<T> {
        companion object {
            fun <T> define(config: dynamic): StateField<T>
        }
    }
}

val codemirrorCommands = js("require('@codemirror/commands')")
val codemirror = js("require('codemirror')")

fun insertCharIntoEditorView(view: codemirrorView.EditorView, ch: String) {
    val ranges = view.state.selection.ranges
    if (ranges.length > 0) {
        val l = js("[]")
        for (i in 0 until ranges.length) {
            val range = ranges[i]
            val changeReq = jsObject2(
                "from" to range.from,
                "to" to range.to,
                "insert" to ch)
            l.push(changeReq)
        }
        val selectionReq = jsObject2(
            "anchor" to ranges[0].from + ch.length)
        val updateReq = jsObject2(
            "changes" to l,
            "selection" to selectionReq,
            "scrollIntoView" to true)
        view.dispatch(updateReq)
    }
}

class EditorViewHolder(val view: codemirrorView.EditorView) {
    val editableText: EditableText = JsClientEditableText()

    fun updateContent(text: String) {
        val change = jsObject2(
            "from" to 0,
            "to" to view.state.doc.length,
            "insert" to text)
        val transaction = jsObject2(
            "changes" to change)
        view.dispatch(transaction)
    }

    fun focus() {
        view.focus()
    }

    fun insertCharAndCommit(s: String) {
        insertCharIntoEditorView(view, s)
    }

    private inner class JsClientEditableText : SimpleTextTrackingEditableText() {
        private fun addChange(fromIndex: Int, toIndex: Int, insert: String) {
            val prevPos = view.state.selection.main.head as Int
            val origWidth = toIndex - fromIndex
            val newPos = when {
                prevPos >= toIndex -> prevPos + (insert.length - origWidth)
                else -> prevPos
            }
            val transaction = jsObject2(
                "changes" to jsObject2("from" to fromIndex, "to" to toIndex, "insert" to insert),
                "selection" to jsObject2("anchor" to newPos, "head" to newPos))
            view.dispatch(transaction)
        }

        override fun text(start: Int, end: Int?): String {
            val s = view.state.doc.toString()
            return s.substring(start, end ?: s.length)
        }

        override fun cursorPosition(): Int {
            return view.state.selection.main.head
        }

        override fun insert(text: String, index: Int) {
            addChange(index, index, text)
        }

        override fun remove(numChars: Int, index: Int) {
            addChange(index, index + numChars, "")
        }

        override fun commit() {
        }
    }
}

fun addEditor(worker: Worker, topElement: HTMLDivElement, prefixConfiguration: PrefixConfiguration, returnCallback: (String) -> Unit): EditorViewHolder {
    val theme = codemirrorView.EditorView.theme(makeThemeConfig())

    @Suppress("UNUSED_VARIABLE")
    val codemirrorViewReference = codemirrorView

    val updateFn = { u: dynamic -> processDocumentChange(worker, u) }

    fun autocompleteWrapper(context: codemirrorAutocomplete.CompletionContext): Promise<codemirrorAutocomplete.CompletionResult?>? {
        return resolveCompletion(context, worker)
    }

    val autocompletionConfig = jsObject2(
        "activateOnTyping" to false,
        "override" to arrayOf(::autocompleteWrapper))
    val autocompletionExtension = codemirrorAutocomplete.autocompletion(autocompletionConfig)

    val engineConnector = JsEngineConnector(worker)

    val startStateConfig = jsObject2(
        "doc" to "",
        "extensions" to arrayOf(
            theme,
            codemirrorView.keymap.of(makeKapKeymap(engineConnector, prefixConfiguration, returnCallback)),
            codemirrorView.EditorView.updateListener.of(updateFn),
            codemirror.basicSetup,
            autocompletionExtension
        ))
    val startState = codemirrorState.EditorState.create(startStateConfig)
    val config = jsObject2(
        "state" to startState,
        "parent" to topElement)
    val view = codemirrorView.EditorView(config)
    return EditorViewHolder(view)
}

private var outstandingTokenisationRequest: TokenisationRequestInfo? = null
private var waitingTokenisationRequest: TokenisationRequestInfo? = null

class TokenisationRequestInfo(val worker: Worker, val view: codemirrorView.EditorView, val src: String) {
    val requestId = makeEditorTokenisationRequestId()
    var valid: Boolean = true

    fun start() {
        worker.postMessage(Json.encodeToString<Request>(TokeniseRequest(requestId, src)))
    }

    companion object {
        private var currentId = 0L
        private fun makeEditorTokenisationRequestId(): String {
            val newId = currentId++
            return "${EDITOR_REQUEST_PREFIX}-${newId}"
        }

        private const val EDITOR_REQUEST_PREFIX = "editor-token"

        fun isRequestIdEditorRequest(requestId: String) = requestId.startsWith(EDITOR_REQUEST_PREFIX)
    }
}

private fun processDocumentChange(worker: Worker, update: dynamic) {
    if (update.changedRanges.length > 0) {
        val req = TokenisationRequestInfo(worker, update.view, update.view.state.doc.toString())
        val oldReq = outstandingTokenisationRequest
        if (oldReq == null) {
            val waitingReq = waitingTokenisationRequest
            if (waitingReq == null) {
                setTimeout({
                    require(outstandingTokenisationRequest == null) { "Hit timeout with a non-null request" }
                    val w = waitingTokenisationRequest
                    require(w != null) { "Timeout fired with no waiting request" }
                    outstandingTokenisationRequest = w
                    waitingTokenisationRequest = null
                    w.start()
                }, 250.0)
            }
            waitingTokenisationRequest = req
        } else {
            oldReq.valid = false
        }
        waitingTokenisationRequest = req
    }
}

fun processTokeniseResult(msg: dynamic) {
    val oldReq = outstandingTokenisationRequest
    require(oldReq != null) { "Got tokenisation result without outstanding request" }
    val next = waitingTokenisationRequest
    if (next != null) {
        waitingTokenisationRequest = null
        outstandingTokenisationRequest = next
        next.start()
    } else {
        outstandingTokenisationRequest = null
        if (oldReq.valid) {
            updateDecorationForTokenisationResult(oldReq, msg.tokens as Array<dynamic>)
        }
    }
}

val tokenTypeToStyle = Array(TokenTypes.NUM_TYPES) { i ->
    when (i) {
        TokenTypes.SYMBOL -> "kap-highlight-symbol"
        TokenTypes.NUMBER -> "kap-highlight-number"
        TokenTypes.STRING -> "kap-highlight-string"
        TokenTypes.COMMENT -> "kap-highlight-comment"
        TokenTypes.CHAR -> "kap-highlight-char"
        else -> error("Undefined token type index: ${i}")
    }
}

class DecorationList {
    val decorations = initDecorationList()

    private fun initDecorationList(): Array<codemirrorView.Decoration?> {
        fun makeDecoration(className: String): codemirrorView.Decoration {
            val conf = jsObject2(
                "inclusiveStart" to true,
                "inclusiveEnd" to false,
                "class" to className)
            return codemirrorView.Decoration.mark(conf)
        }

        return tokenTypeToStyle.map { style -> makeDecoration(style) }.toTypedArray()
    }

    fun findDecoration(type: Int): codemirrorView.Decoration? {
        if (type < 0 || type >= decorations.size) {
            return null
        }
        return decorations[type]
    }
}

private val tokenHighlightStateField = run {
    fun processUpdate(value: codemirrorState.RangeSet<codemirrorView.Decoration>, transaction: dynamic): dynamic {
        val rangeSetUpdates = ArrayList<dynamic>()
        repeat(transaction.effects.length) { i ->
            val effect = transaction.effects[i]
            if (effect.`is`(tokenHighlightStateEffect)) {
                val config = jsObject2(
                    "add" to arrayOf(effect.value.type.range(effect.value.from, effect.value.to)))
                //mapped = mapped.update(config)
                rangeSetUpdates.add(config)
            }
        }

        var mapped = value.map(transaction.changes)
        // Since an update either replaces all highlights, or does nothing, if rangeSetUpdates is not empty,
        // that means that this is a full update, and we can remove all existing highlights
        val hasUpdates = rangeSetUpdates.isNotEmpty()
        if (hasUpdates) {
            mapped = mapped.update(jsObject2(
                "filter" to { from: Int, to: Int, value: codemirrorView.Decoration -> false }))
        }

        rangeSetUpdates.forEach { conf ->
            mapped = mapped.update(conf)
        }

        return mapped
    }

    val conf = jsObject2(
        "create" to {
            codemirrorView.Decoration.none
        },
        "update" to { value: codemirrorState.RangeSet<codemirrorView.Decoration>, transaction: dynamic ->
            processUpdate(value, transaction)
        },
        "provide" to { value: dynamic ->
            codemirrorView.EditorView.decorations.from(value)
        })
    codemirrorState.StateField.define<codemirrorState.RangeSet<codemirrorView.Decoration>>(conf)
}

private val tokenHighlightStateEffect = codemirrorState.StateEffect.define(
    jsObject2(
        "map" to { value: dynamic, change: dynamic ->
            jsObject2(
                "from" to change.mapPos(value.from),
                "to" to change.mapPos(value.to),
                "type" to value.type)
        }))

private fun updateDecorationForTokenisationResult(req: TokenisationRequestInfo, tokens: Array<dynamic>) {
    if (tokens.isEmpty()) {
        return
    }

    val dl = DecorationList()

    val doc = req.view.state.doc
    var currIndex = 0
    val indexes = IntArray(doc.lines) { i ->
        currIndex.also {
            currIndex += (doc.line(i + 1).length as Int) + 1
        }
    }

    val effectList = js("[]")

    tokens.forEach { token ->
        val tokenType = token.type
        val pos = token.p
        val decoration = dl.findDecoration(tokenType)
        if (decoration != null) {
            val start = indexes[pos.line] + (pos.col as Int)
            val end = indexes[pos.endLine] + (pos.endCol as Int)
            val stateEffect = tokenHighlightStateEffect.of(
                jsObject2(
                    "from" to start,
                    "to" to end,
                    "type" to decoration))
            effectList.push(stateEffect)
        }
    }

    if (req.view.state.field(tokenHighlightStateField, false) == null) {
        val stateEffect = codemirrorState.StateEffect.appendConfig.of(arrayOf(tokenHighlightStateField))
        effectList.push(stateEffect)
    }

    req.view.dispatch(jsObject2("effects" to effectList))
}

private fun makeThemeConfig(): dynamic {
    val themeConfig = js("{\"&\":{fontSize:\"18px\"}, \".cm-content\":{fontFamily:\"Iosevka Fixed, monospace\"}}")
    return themeConfig
}

private fun makeKeyBinding(ch: String, fn: (dynamic, KeyboardEvent) -> Boolean): dynamic {
    return jsObject2(
        "key" to ch,
        "run" to fn)
}

class PrefixConfiguration(val keyboardInput: ExtendedCharsKeyboardInput, var prefixKey: String) {
    var prefixSym: String? = recomputePrefixSym()
        private set

    var prefixStateActive = false
        private set

    fun updatePrefixState(newValue: Boolean) {
        prefixStateActive = newValue
        val element = findElement<HTMLElement>("lang-bar")
        if (newValue) {
            element.addClass("prefix-active")
        } else {
            element.removeClass("prefix-active")
        }
    }

    private fun recomputePrefixSym(): String? {
        val prefixSymEntry = keyboardInput
            .keymap
            .entries
            .find { (descriptor, _) -> descriptor.character == prefixKey }
        return prefixSymEntry?.value
    }

    fun updatePrefix(s: String) {
        prefixKey = s
        prefixSym = recomputePrefixSym()
        prefixStateActive = false
    }
}

private fun makeKapKeymap(engineConnector: EditorEngineConnector, prefixConfiguration: PrefixConfiguration, returnCallback: (String) -> Unit): dynamic {
    val keyList = js("[]")

    keyList.push(jsObject2(
        "any" to { view: dynamic, event: KeyboardEvent ->
            if (event.key === prefixConfiguration.prefixKey) {
                prefixTyped(view, prefixConfiguration)
            } else {
                false
            }
        }))

    prefixConfiguration.keyboardInput.keymap.forEach { (descriptor, text) ->
        keyList.push(makeKeyBinding(descriptor.character, makeInsertCharFunction(prefixConfiguration) { text }))
    }

    keyList.push(makeKeyBinding("Space", makeInsertCharFunction(prefixConfiguration) { prefixConfiguration.prefixKey }))
    keyList.push(makeKeyBinding("Alt-(", makeEditorActionFunction(WrapWithParen(engineConnector))))

    keyList.push(makeKeyBinding("Enter") { view: dynamic, event: KeyboardEvent ->
        returnCallback(view.state.doc.toString())
        true
    })

    keyList.push(makeKeyBinding("Shift-Enter") { view: dynamic, event: KeyboardEvent ->
        insertCharIntoEditorView(view, "\n")
        true
    })

    keyList.push(jsObject2(
        "any" to { view: dynamic, event: KeyboardEvent ->
            if (!listOf("Shift", "Control", "Alt", "Meta").contains(event.key)) {
                prefixConfiguration.updatePrefixState(false)
            }
            false
        }))

    return keyList
}

private fun makeInsertCharFunction(prefixConfiguration: PrefixConfiguration, charGetter: () -> String): (dynamic, KeyboardEvent) -> Boolean {
    return { view, event ->
        if (prefixConfiguration.prefixStateActive) {
            prefixConfiguration.updatePrefixState(false)
            insertCharIntoEditorView(view, charGetter())
            true
        } else {
            false
        }
    }
}

private fun prefixTyped(view: dynamic, prefixConfiguration: PrefixConfiguration): Boolean {
    if (prefixConfiguration.prefixStateActive) {
        prefixConfiguration.updatePrefixState(false)
        val prefixCharSym = prefixConfiguration.prefixSym
        if (prefixCharSym != null) {
            insertCharIntoEditorView(view, prefixCharSym)
        }
    } else {
        prefixConfiguration.updatePrefixState(true)
    }
    return true
}

private fun makeEditorActionFunction(action: EditorAction): (dynamic, KeyboardEvent) -> Boolean {
    return { view, event ->
        val editor = editorGlobal
        if (editor == null) {
            println("Editor is not initialised")
        } else {
            println("running action")
            action.run(editor.editableText)
        }
        true
    }
}
