package com.dhsdevelopments.kap

class KapClassNotFound(name: Symbol, pos: Position? = null) : APLEvalException("Class not found: ${name.nameWithNamespace}", pos)
class KapMethodNotFound(name: Symbol, obj: APLValue, pos: Position? = null) :
    APLEvalException("Method does exist: ${name.nameWithNamespace}. Object: ${obj.formatted(FormatStyle.PLAIN)}")

interface KapClass {
    val name: String

    fun resolveMethod(engine: Engine, ref: APLValue, methodName: Symbol, pos: Position): KapMethod =
        throwAPLException(APLEvalException("Invalid target object", pos))
}

abstract class ModuleClass : KapClass

open class SystemClass(override val name: String) : KapClass {
    companion object {
        val INTEGER = SystemClass("integer")
        val FLOAT = SystemClass("float")
        val COMPLEX = SystemClass("complex")
        val CHAR = SystemClass("char")
        val ARRAY = SystemClass("array")
        val SYMBOL = SystemClass("symbol")
        val LAMBDA_FN = SystemClass("function")
        val LIST = SystemClass("list")
        val MAP = MapClass()
        val RATIONAL = SystemClass("rational")
        val OBJECT = SystemClass("object")
        val TIMESTAMP = SystemClass("timestamp")
        val NIL = SystemClass("null")
        val INTERNAL = SystemClass("internal")

        val SYSTEM_CLASS_LIST =
            listOf(
                INTEGER, FLOAT, COMPLEX, CHAR, ARRAY, SYMBOL, LAMBDA_FN,
                LIST, MAP, RATIONAL, OBJECT, TIMESTAMP, NIL, INTERNAL)
    }
}

class UserDefinedClass(val sym: Symbol) : KapClass {
    override val name get() = sym.nameWithNamespace
}

class ClassManager(val engine: Engine) {
    private val registeredClasses: MutableMap<Symbol, KapClass> = HashMap()
    private val classToSymbol: MutableMap<KapClass, Symbol> = HashMap()

    init {
        SystemClass.SYSTEM_CLASS_LIST.forEach { cl ->
            val sym = engine.coreNamespace.internSymbol(cl.name)
            registeredClasses[sym] = cl
            classToSymbol[cl] = sym
        }
    }

    fun findClass(sym: Symbol) = registeredClasses[sym]

    fun registerSupplementarySystemClass(sym: Symbol, cl: ModuleClass) {
        registerClass(sym, cl)
    }

    fun registerUserClass(cl: UserDefinedClass) {
        registerClass(cl.sym, cl)
    }

    private fun registerClass(sym: Symbol, cl: KapClass) {
        registeredClasses[sym] = cl
        classToSymbol[cl] = sym
    }

    fun nameForClass(cl: KapClass): Symbol {
        return classToSymbol[cl] ?: throw IllegalStateException("No symbol found for class: ${cl.name}")
    }

    fun init() {
        val ns = engine.makeNamespace("objects")
        engine.registerFunction(engine.internSymbol("defclass", ns), DefclassFunctionDescriptor())
        engine.registerFunction(engine.internSymbol("make", ns), MakeClassInstanceFunctionDescriptor())
        engine.registerFunction(engine.internSymbol("classof", ns), TagOfFunctionDescriptor())
        engine.registerFunction(engine.internSymbol("extract", ns), ExtractFunctionDescriptor())
    }
}

class DefclassFunctionDescriptor : APLFunctionDescriptor {
    class DefclassFunctionImpl(instantiation: FunctionInstantiation) : NoAxisAPLFunction(instantiation) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val a0 = a.listify()
            val name = a0.listElement(0).ensureSymbol(pos)
            context.engine.classManager.registerUserClass(UserDefinedClass(name.value))
            return name
        }
    }

    override fun make(instantiation: FunctionInstantiation) = DefclassFunctionImpl(instantiation)
}

class MakeClassInstanceFunctionDescriptor : APLFunctionDescriptor {
    class MakeClassInstanceFunctionImpl(instantiation: FunctionInstantiation) : NoAxisAPLFunction(instantiation) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
            val className = a.ensureSymbol(pos).value
            val cl = context.engine.classManager.findClass(className) ?: throwAPLException(KapClassNotFound(className, pos))
            return TypedAPLValue(b, cl)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = MakeClassInstanceFunctionImpl(instantiation)
}

class TagOfFunctionDescriptor : APLFunctionDescriptor {
    class TagOfFunctionImpl(instantiation: FunctionInstantiation) : NoAxisAPLFunction(instantiation) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            return APLSymbol(context.engine.classManager.nameForClass(a.kapClass))
        }
    }

    override fun make(instantiation: FunctionInstantiation) = TagOfFunctionImpl(instantiation)
}

class ExtractFunctionDescriptor : APLFunctionDescriptor {
    class ExtractFunctionImpl(instantiation: FunctionInstantiation) : NoAxisAPLFunction(instantiation) {
        private fun ensureTypedAPLValue(a: APLValue): TypedAPLValue {
            val a0 = a.unwrapDeferredValue()
            if (a0 !is TypedAPLValue) {
                throwAPLException(IncompatibleTypeException("Value is not a class instance: ${a.formatted(FormatStyle.PLAIN)}", pos))
            }
            return a0
        }

        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            return ensureTypedAPLValue(a).delegate
        }

        override fun evalWithStructuralUnder1Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue): APLValue {
            val a0 = ensureTypedAPLValue(a)
            val result = baseFn.eval1Arg(context, a0.delegate, null)
            return TypedAPLValue(result, a0.kapClass)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ExtractFunctionImpl(instantiation)
}

class TypedAPLValue(val delegate: APLValue, override val kapClass: KapClass) : APLSingleValue() {
    override fun formatted(style: FormatStyle) = "instance"

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Boolean {
        TODO("Not yet implemented")
    }

    override fun typeQualifiedHashCode(): Int {
        TODO("not implemented")
    }
}
