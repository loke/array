package com.dhsdevelopments.kap.gui.display

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.gui.Client
import com.dhsdevelopments.kap.gui.ClientRenderContext
import com.dhsdevelopments.kap.gui.EvalExpressionResult
import com.dhsdevelopments.kap.gui.arrayedit.ArrayEditor
import com.dhsdevelopments.kap.gui.settings.ConfigProvider
import com.dhsdevelopments.kap.gui.styledarea.EditorContent
import com.dhsdevelopments.kap.gui.styledarea.ParStyle
import com.dhsdevelopments.kap.gui.styledarea.StringEditorContentEntry
import com.dhsdevelopments.kap.gui.styledarea.TextStyle
import com.dhsdevelopments.kap.rendertext.renderStringValueOptionalQuotes
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.geometry.HPos
import javafx.geometry.Insets
import javafx.geometry.Side
import javafx.scene.Node
import javafx.scene.control.ContextMenu
import javafx.scene.control.Label
import javafx.scene.control.MenuItem
import javafx.scene.input.Clipboard
import javafx.scene.input.DataFormat
import javafx.scene.input.MouseButton
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.text.Text
import org.fxmisc.richtext.model.ReadOnlyStyledDocument
import org.fxmisc.richtext.model.ReadOnlyStyledDocumentBuilder
import org.fxmisc.richtext.model.StyledSegment
import org.fxmisc.richtext.model.TextOps
import java.util.*
import kotlin.reflect.KClass

var currStyleId = 0

fun makeKapValueDoc(
    config: ConfigProvider,
    segOps: TextOps<EditorContent, TextStyle>,
    value: EvalExpressionResult,
    parStyle: ParStyle,
    clearableStyleContent: TextStyle.ClearableStyleContent
): ReadOnlyStyledDocument<ParStyle, EditorContent, TextStyle> {
    val baseStyle = TextStyle(config, TextStyle.Type.RESULT, contentRefs = clearableStyleContent)
    val newDoc = ReadOnlyStyledDocumentBuilder(segOps, parStyle)
    val formatted = value.formattedEvalResult

    val styleNamePrefix = "kap-value-output-${currStyleId++}-"
    val cellValueMap = IdentityHashMap<FormattedEvalResult.MetadataEntry, String>()
    var cssClassNameIndex = 0

    formatted.strings.forEachIndexed { i, s ->
        val res = ArrayList<StyledSegment<EditorContent, TextStyle>>()
        val rowCells = arrayOfNulls<FormattedEvalResult.MetadataEntry>(s.length)
        formatted.metadataList
            .asSequence()
            .filter { m -> i >= m.row && i < m.row + m.height }
            .forEach { m ->
                for (x in m.col until m.col + m.width) {
                    rowCells[x] = m
                }
            }

        var start = 0
        var x = 0
        var curr: FormattedEvalResult.MetadataEntry? = null

        fun collectString() {
            if (x > start) {
                val currCopy = curr
                val newStyle = if (currCopy == null) {
                    baseStyle
                } else {
                    val cssClassName = cellValueMap.getOrPut(currCopy) { "${styleNamePrefix}${cssClassNameIndex++}" }
                    val index = clearableStyleContent.addValue(currCopy.value)
                    baseStyle.copyWithInnerValue(index, cssClassName)
                }
                res.add(StyledSegment(EditorContent.makeString(s.substring(start, x)), newStyle))
            }
        }

        while (x < rowCells.size) {
            val metadata = rowCells[x]
            if (rowCells[x] != curr) {
                collectString()
                start = x
                curr = metadata
            }
            x++
        }
        collectString()

        newDoc.addParagraph(res)
    }
    return newDoc.addParagraph(EditorContent.makeBlank(), baseStyle).build()
}

////////////////////////////////////////////////////////////////////////////////////

interface ValueRenderer {
    val value: APLValue
    val text: String
    fun renderValue(): Region
    fun addToMenu(contextMenu: ContextMenu) {}

    companion object {
        private val renderers = hashMapOf<KClass<out APLValue>, (APLValue) -> ValueRenderer>(
            //Pair(APLMap::class, { APLMapRenderer(it as APLMap) })
        )

        fun makeValueRenderer(client: Client, value: APLValue): ValueRenderer {
            val renderer = renderers[value::class]
            return if (renderer != null) {
                renderer(value)
            } else {
                Array2ValueRenderer(client, value)
            }
        }

        fun makeContent(client: Client, value: APLValue): EditorContent {
            return Array2ContentEntry(makeValueRenderer(client, value))
        }
    }
}

class Array2ValueRenderer(private val client: Client, override val value: APLValue) : ValueRenderer {
    override val text = possiblyOversizeDescription(value)
    override fun renderValue() = makeArrayNode(client, value)

    override fun addToMenu(contextMenu: ContextMenu) {
        val item = MenuItem("Open in editor").apply { onAction = EventHandler { Platform.runLater { ArrayEditor.open(client, value) } } }
        contextMenu.items.add(item)
    }
}

private class Array2ContentEntry(val renderer: ValueRenderer) : EditorContent {
    override fun length() = renderer.text.length

    override fun createNode(renderContext: ClientRenderContext, style: TextStyle): Node {
        val node = renderer.renderValue()
        val contextMenu = ContextMenu(
            MenuItem("Copy as string").apply { onAction = EventHandler { copyAsString() } },
            MenuItem("Copy as code").apply { onAction = EventHandler { copyAsCode() } },
            MenuItem("Copy as HTML").apply { onAction = EventHandler { copyAsHtml() } })
        renderer.addToMenu(contextMenu)
        node.setOnMouseClicked { event ->
            if (event.button == MouseButton.SECONDARY) {
                contextMenu.show(node, Side.RIGHT, -(node.width - event.x), event.y)
            }
        }
        return node
    }

    private fun copyAsString() {
        val clipboard = Clipboard.getSystemClipboard()
        clipboard.setContent(mapOf(DataFormat.PLAIN_TEXT to renderer.value.formatted(FormatStyle.PRETTY)))
    }

    private fun copyAsCode() {
        val clipboard = Clipboard.getSystemClipboard()
        clipboard.setContent(mapOf(DataFormat.PLAIN_TEXT to renderer.value.formatted(FormatStyle.READABLE)))
    }

    private fun copyAsHtml() {
        val buf = StringBuilder()
        renderer.value.asHtml(buf)
        val result = buf.toString()
        val clipboard = Clipboard.getSystemClipboard()
        clipboard.setContent(
            mapOf(
                DataFormat.HTML to result,
                DataFormat.PLAIN_TEXT to result))
    }

    override fun joinSegment(nextSeg: EditorContent): Optional<EditorContent> = Optional.empty()
    override fun realGetText() = renderer.text
    override fun realCharAt(index: Int) = throw IllegalStateException("Can't get character array element")

    override fun realSubsequence(start: Int, end: Int): EditorContent {
        val text = renderer.text
        return if (start == 0 && end == text.length) {
            this
        } else {
            StringEditorContentEntry(text.substring(start, end))
        }
    }
}

private fun makeArrayNode(client: Client, value: APLValue): Region {
    fun renderAsString(): Region {
        return Label(value.formatted(FormatStyle.PRETTY)).apply { styleClass.add("kapresult") }
    }

    val d = value.dimensions
    val node = when {
        isOversize(value) -> renderOversizeValue(value)
        d.size == 1 && d[0] == 0 -> renderAsString()
        value.isStringValue() -> makeStringDisp(value)
        d.size == 1 -> makeArray1(client, value)
        d.size == 2 -> makeArray2(client, value)
        else -> renderAsString()
    }
    return node
}

private fun possiblyOversizeDescription(value: APLValue) =
    if (isOversize(value)) oversizeDescription(value) else value.formatted(FormatStyle.PRETTY)

private fun isOversize(value: APLValue) = value.dimensions.asList().any { it > 100 }

private fun oversizeDescription(value: APLValue) = "Oversized array: ${value.dimensions}"

private fun renderOversizeValue(value: APLValue): Region {
    val text = Label(oversizeDescription(value))
    text.border = Border(BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(1.0)))
    return text
}

private fun makeArray1(client: Client, value: APLValue): Region {
    val grid = GridPane()
    grid.border = Border(BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(1.0)))
    value.iterateMembersWithPosition { v, i ->
        val label = makeArrayNode(client, v)
        GridPane.setRowIndex(label, 0)
        GridPane.setColumnIndex(label, i)
        GridPane.setMargin(label, Insets(3.0, 3.0, 3.0, 3.0))
        GridPane.setHalignment(label, HPos.RIGHT)
        grid.children.add(label)
    }
    return grid
}

private fun makeArray2(client: Client, value: APLValue): Region {
    val dimensions = value.dimensions
    val grid = GridPane()
    grid.border = Border(BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(1.0)))

    val labelState = DimensionLabels.computeLabelledAxis(value)

    val numRows = dimensions[0]
    val numCols = dimensions[1]
    val rowOffset = if (labelState[1]) 1 else 0
    val colOffset = if (labelState[0]) 1 else 0
    dimensions.multipliers()

    if (labelState[1]) {
        value.metadata.labels?.let { labels ->
            labels.labels[1]?.let { axis1Labels ->
                axis1Labels.forEachIndexed { i, axisLabel ->
                    if (axisLabel != null) {
                        val label = Text(axisLabel.title).apply {
                            styleClass.add("kapresult-header")
                        }
                        GridPane.setRowIndex(label, 0)
                        GridPane.setColumnIndex(label, i + colOffset)
                        GridPane.setMargin(label, Insets(3.0, 3.0, 3.0, 3.0))
                        GridPane.setHalignment(label, HPos.CENTER)
                        grid.children.add(label)
                    }
                }
            }
        }
    }

    repeat(numRows) { rowIndex ->
        if (labelState[0]) {
            value.metadata.labels?.let { labels ->
                labels.labels[0]?.let { axis0Labels ->
                    val axisLabel = axis0Labels[rowIndex]
                    if (axisLabel != null) {
                        val rowLabel = Text(axisLabel.title).apply {
                            styleClass.add("kapresult-header")
                        }
                        GridPane.setRowIndex(rowLabel, rowIndex + rowOffset)
                        GridPane.setColumnIndex(rowLabel, 0)
                        GridPane.setMargin(rowLabel, Insets(3.0, 3.0, 3.0, 3.0))
                        GridPane.setHalignment(rowLabel, HPos.LEFT)
                        grid.children.add(rowLabel)
                    }
                }
            }
        }
        repeat(numCols) { colIndex ->
            val label = makeArrayNode(client, value.valueAt(dimensions.indexFromPosition(intArrayOf(rowIndex, colIndex))))
            GridPane.setRowIndex(label, rowIndex + rowOffset)
            GridPane.setColumnIndex(label, colIndex + colOffset)
            GridPane.setMargin(label, Insets(3.0, 3.0, 3.0, 3.0))
            GridPane.setHalignment(label, HPos.RIGHT)
            grid.children.add(label)
        }
    }

    return grid
}

private fun makeStringDisp(value: APLValue): Region {
    return Label(renderStringValueOptionalQuotes(value, true)).apply { styleClass.add("kapresult") }
}
