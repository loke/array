package com.dhsdevelopments.kap.gui.fxanimation

import javafx.animation.*
import javafx.geometry.Insets
import javafx.geometry.Point2D
import javafx.scene.Scene
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.shape.LineTo
import javafx.scene.shape.MoveTo
import javafx.scene.shape.Path
import javafx.scene.text.Font
import javafx.scene.text.Text
import javafx.stage.Stage
import javafx.util.Duration
import kotlin.math.max
import kotlin.math.min


class AnimationPanel {
    private val stage = Stage()
    val pane = Pane()
    val font: Font = Font.font("serif", 20.0)

    init {
        stage.title = "Animation"
        val border = BorderPane().apply {
            center = pane
        }
        pane.background = Background(BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY))
        val scene = Scene(border, 800.0, 800.0)
        stage.scene = scene
    }

    fun close() {
        stage.close()
    }

    fun show() {
        stage.show()
    }

    fun drawContent(elements: List<TextElement>) {
        pane.children.clear()
//        pane.children.add(Line(0.0, pane.height / 2, pane.width, pane.height / 2))
//        pane.children.add(Line(pane.width / 2, 0.0, pane.width / 2, pane.height))
        val transitions = ArrayList<Animation>()
        elements.forEach { e ->
            e.makeNode(this)?.let { t ->
                transitions.add(t)
            }
        }
        if (transitions.isNotEmpty()) {
            val parallelTransition = ParallelTransition()
            parallelTransition.children.addAll(transitions)
            parallelTransition.play()
        }
    }
}

private val DEFAULT_DURATION = Duration.seconds(2.0)
private const val DEFAULT_FONT_SIZE = 20.0

class TextElement(val text: String, val x: Double, val y: Double) {
    var translateFrom: Pair<Double, Double>? = null
    var fadeTime: Double? = null
    var alignment: Alignment = Alignment.Centre
    var fontName: String? = null
    var fontSize: Double = 1.0
    var delay: Double = 0.0
    var colour: ColourDefinition? = null

    //animation:run ⍮"Foopalletestgj" (1r2 1r2) :translate (0.8r 0.2r) :align :right
    fun makeNode(panel: AnimationPanel): Animation? {
        val textNode = Text(text)
        colour?.let { c ->
            textNode.fill = c.makeColour()
        }
        val w = panel.pane.width
        val h = panel.pane.height
        textNode.font = Font.font(fontName ?: "serif", DEFAULT_FONT_SIZE * fontSize)
        val newPosition = alignment.computePosition(textNode, x * w, y * h)
        textNode.x = newPosition.x
        textNode.y = newPosition.y
        panel.pane.children.add(textNode)

        val l = ArrayList<Animation>()
        translateFrom?.let { (tx, ty) ->
            val txTrans = alignment.computePosition(textNode, tx * w, ty * h)
            textNode.x = txTrans.x
            textNode.y = txTrans.y
            // The PathTransition function uses the centre of the node for positioning. We need to compensate for this.
            val bounds = textNode.layoutBounds
            val tx = txTrans.x + (bounds.centerX - bounds.minX)
            val ty = txTrans.y - (bounds.centerY - bounds.minY) + (bounds.height - textNode.baselineOffset)
            val nx = newPosition.x + (bounds.centerX - bounds.minX)
            val ny = newPosition.y - (bounds.centerY - bounds.minY) + (bounds.height - textNode.baselineOffset)
            if (tx != nx || ty != ny) {
                l.add(PathTransition().apply {
                    node = textNode
                    duration = DEFAULT_DURATION
                    path = Path().apply {
                        elements.add(MoveTo(tx, ty))
                        elements.add(LineTo(nx, ny))
                    }
                })
            }
        }
        fadeTime?.let { f ->
            l.add(FadeTransition().apply {
                node = textNode
                duration = Duration.millis(DEFAULT_DURATION.toMillis() * f)
                fromValue = 0.0
                toValue = 1.0
            })
        }
        return if (l.isEmpty()) {
            null
        } else {
            val p = ParallelTransition().apply { children.addAll(l) }
            if (delay != 0.0) {
                SequentialTransition(PauseTransition(Duration.seconds(delay)), p)
            } else {
                p
            }
        }
    }

    sealed class Alignment {
        abstract fun computePosition(node: Text, x: Double, y: Double): Point2D

        object Left : Alignment() {
            override fun computePosition(node: Text, x: Double, y: Double) = Point2D(x, y)
        }

        object Centre : Alignment() {
            override fun computePosition(node: Text, x: Double, y: Double) = Point2D(x - node.layoutBounds.width / 2, y)
        }

        object Right : Alignment() {
            override fun computePosition(node: Text, x: Double, y: Double) = Point2D(x - node.layoutBounds.width, y)
        }
    }

    class ColourDefinition(red: Double, green: Double, blue: Double) {
        val red = fixupValue(red)
        val green = fixupValue(green)
        val blue = fixupValue(blue)

        fun makeColour() = Color(red, green, blue, 1.0)

        private fun fixupValue(v: Double) = max(0.0, min(1.0, v))
    }
}
