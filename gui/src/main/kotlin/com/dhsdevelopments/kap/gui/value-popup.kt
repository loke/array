package com.dhsdevelopments.kap.gui

import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.toStringValueOrNull
import javafx.scene.Scene
import javafx.scene.control.TextArea
import javafx.scene.layout.BorderPane
import javafx.stage.Stage

class ValuePopup(client: Client) {
    private val stage = Stage()
    private val contentArea: TextArea

    init {
        stage.initOwner(client.stage)
        val borderPane = BorderPane()
        borderPane.stylesheets.add("/com/dhsdevelopments/kap/gui/value-popup.css")
        borderPane.styleClass.add("value-popup")
        contentArea = TextArea().apply {
            styleClass.add("value-popup-value")
            isEditable = false
            isWrapText = true
        }
        borderPane.center = contentArea
        stage.scene = Scene(borderPane, 1000.0, 800.0)
    }

    fun updateContent(s: String) {
        contentArea.text = s
    }

    fun show() {
        stage.show()
    }

    companion object {
        fun displayValueInPopup(client: Client, value: APLValue) {
            val window = ValuePopup(client)
            window.updateContent(value.toStringValueOrNull() ?: "cannot render")
            window.show()
        }
    }
}
