package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.OptimisationFlags.Companion.OPTIMISATION_FLAG_2ARG_LONG_LONG
import com.dhsdevelopments.kap.complex.Complex
import com.dhsdevelopments.mpbignum.BigInt
import com.dhsdevelopments.mpbignum.LongExpressionOverflow
import com.dhsdevelopments.mpbignum.Rational
import kotlin.math.max

class EqualsAPLFunction : APLFunctionDescriptor {
    class EqualsAPLFunctionImpl(pos: FunctionInstantiation) : MathCombineAPLFunction(pos, resultType2Arg = ArrayMemberType.BOOLEAN) {
        override fun combine2Arg(a: APLSingleValue, b: APLSingleValue) = if (opLong(a, b)) APLLONG_1 else APLLONG_0

        @Suppress("NOTHING_TO_INLINE")
        private inline fun opLong(a: APLSingleValue, b: APLSingleValue) = a.numericCompareEquals(b, pos)

        override fun identityValue() = APLLONG_1
        override val optimisationFlags get() = OptimisationFlags(OPTIMISATION_FLAG_2ARG_LONG_LONG)
        override fun combine2ArgLongToLong(a: Long, b: Long) = if (a == b) 1L else 0L
        override fun combine2ArgGenericToLong(a: APLSingleValue, b: APLSingleValue): Long = if (opLong(a, b)) 1 else 0

        override fun deriveBitwise() = BitwiseXnorFunction()

        override val name2Arg get() = "equals"
    }

    override fun make(instantiation: FunctionInstantiation) = EqualsAPLFunctionImpl(instantiation)
}

class NotEqualsAPLFunction : APLFunctionDescriptor {
    class NotEqualsAPLFunctionImpl(pos: FunctionInstantiation) : MathCombineAPLFunction(pos, resultType2Arg = ArrayMemberType.BOOLEAN) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            ensureAxisNull(axis)
            val a1 = a.arrayify().collapse()
            val rank = a1.rank
            val value0 = if (rank == 1) {
                a1
            } else {
                AxisMultiDimensionEnclosedValue(a1, rank - 1)
            }

            val map = HashSet<APLValue.APLValueKey>()
            val result = BooleanArray(value0.size) { i ->
                val v = value0.valueAt(i)
                val added = map.add(v.makeTypeQualifiedKey())
                added
            }

            return APLArrayBoolean(dimensionsOfSize(result.size), result)
        }

        override fun combine2Arg(a: APLSingleValue, b: APLSingleValue) = if (opLong(a, b)) APLLONG_1 else APLLONG_0

        @Suppress("NOTHING_TO_INLINE")
        private inline fun opLong(a: APLSingleValue, b: APLSingleValue) = !a.numericCompareEquals(b, pos)

        override fun identityValue() = APLLONG_0
        override val optimisationFlags get() = OptimisationFlags(OPTIMISATION_FLAG_2ARG_LONG_LONG)
        override fun combine2ArgLongToLong(a: Long, b: Long) = if (a != b) 1L else 0L
        override fun combine2ArgGenericToLong(a: APLSingleValue, b: APLSingleValue): Long = if (opLong(a, b)) 1 else 0

        override fun deriveBitwise() = BitwiseXorFunction()

        override val name2Arg get() = "not equals"
    }

    override fun make(instantiation: FunctionInstantiation) = NotEqualsAPLFunctionImpl(instantiation)
}

class LessThanAPLFunction : APLFunctionDescriptor {
    class LessThanAPLFunctionImpl(pos: FunctionInstantiation) : MathCombineAPLFunction(pos, resultType2Arg = ArrayMemberType.BOOLEAN) {
        override fun combine2Arg(a: APLSingleValue, b: APLSingleValue): APLValue {
            return opLong(a, b).makeAPLNumberAsBoolean()
        }

        private fun opLong(a: APLSingleValue, b: APLSingleValue): Boolean {
            return a.numericCompare(b) < 0
        }

        override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            if (axis != null) {
                throwAPLException(AxisNotSupported(pos))
            }
            val aDimensions = a.dimensions
            val dimensionsArray = IntArray(aDimensions.size + 1) { i ->
                if (i == 0) {
                    1
                } else {
                    aDimensions[i - 1]
                }
            }

            val oldMetadata = a.metadata
            val resized = ResizedArrayImpls.makeResizedArray(Dimensions(dimensionsArray), a)
            return if (oldMetadata.isDefault) {
                resized
            } else {
                MetadataOverrideArray(resized, LessThanResultMetadata(oldMetadata))
            }
        }

        override fun identityValue() = APLLONG_0

        override val optimisationFlags get() = OptimisationFlags(OPTIMISATION_FLAG_2ARG_LONG_LONG)
        override fun combine2ArgLongToLong(a: Long, b: Long) = if (a < b) 1L else 0L
        override fun combine2ArgGenericToLong(a: APLSingleValue, b: APLSingleValue): Long =
            if (opLong(a, b)) 1 else 0

        override fun deriveBitwise() = BitwiseLessThan()

        override val name1Arg get() = "promote"
        override val name2Arg get() = "less than"

        class LessThanResultMetadata(val sourceMetadata: APLValueMetadata) : APLValueMetadata {
            override val labels by lazy { computeLabels() }
            override val defaultValue get() = APLValueMetadata.DefaultMetadata.defaultValue

            private fun computeLabels() = sourceMetadata.labels?.insertAxis(0)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = LessThanAPLFunctionImpl(instantiation)
}

class GreaterThanAPLFunction : APLFunctionDescriptor {
    class GreaterThanAPLFunctionImpl(pos: FunctionInstantiation) : MathCombineAPLFunction(pos, resultType2Arg = ArrayMemberType.BOOLEAN) {
        override fun combine2Arg(a: APLSingleValue, b: APLSingleValue): APLValue {
            return opLong(a, b).makeAPLNumberAsBoolean()
        }

        private fun opLong(a: APLSingleValue, b: APLSingleValue): Boolean {
            return a.numericCompare(b) > 0
        }

        override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
            if (axis != null) {
                throwAPLException(AxisNotSupported(pos))
            }
            val aDimensions = a.dimensions
            return if (aDimensions.size <= 1) {
                a
            } else {
                val dimensionsArray = IntArray(aDimensions.size - 1) { i ->
                    if (i == 0) {
                        aDimensions[0] * aDimensions[1]
                    } else {
                        aDimensions[i + 1]
                    }
                }

                val oldMetadata = a.metadata
                val resized = ResizedArrayImpls.makeResizedArray(Dimensions(dimensionsArray), a)
                if (oldMetadata.isDefault) {
                    resized
                } else {
                    MetadataOverrideArray(resized, GreaterThanResultMetadata(oldMetadata))
                }
            }
        }

        override fun identityValue() = APLLONG_0
        override val optimisationFlags get() = OptimisationFlags(OPTIMISATION_FLAG_2ARG_LONG_LONG)
        override fun combine2ArgLongToLong(a: Long, b: Long) = if (a > b) 1L else 0L
        override fun combine2ArgGenericToLong(a: APLSingleValue, b: APLSingleValue): Long =
            if (opLong(a, b)) 1 else 0

        override fun deriveBitwise() = BitwiseGreaterThan()

        override val name1Arg get() = "demote"
        override val name2Arg get() = "greater than"

        private class GreaterThanResultMetadata(val oldMetdata: APLValueMetadata) : APLValueMetadata {
            override val labels by lazy { computeLabels() }
            override val defaultValue get() = APLValueMetadata.DefaultMetadata.defaultValue

            private fun computeLabels(): DimensionLabels? {
                val oldLabels = oldMetdata.labels
                return if (oldLabels == null) {
                    null
                } else {
                    require(oldLabels.labels.size >= 2)
                    val newLabelList = ArrayList<List<AxisLabel?>?>()
                    newLabelList.add(null) // New combined axis does not have any names
                    repeat(oldLabels.labels.size - 2) { i ->
                        newLabelList.add(oldLabels.labels[i + 2])
                    }
                    DimensionLabels(newLabelList)
                }
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = GreaterThanAPLFunctionImpl(instantiation)
}

class LessThanEqualAPLFunction : APLFunctionDescriptor {
    class LessThanEqualAPLFunctionImpl(pos: FunctionInstantiation) : MathCombineAPLFunction(pos, resultType2Arg = ArrayMemberType.BOOLEAN) {
        override fun combine2Arg(a: APLSingleValue, b: APLSingleValue): APLValue {
            return opLong(a, b).makeAPLNumberAsBoolean()
        }

        private fun opLong(a: APLSingleValue, b: APLSingleValue): Boolean {
            return a.numericCompare(b) <= 0
        }

        override fun identityValue() = APLLONG_1
        override val optimisationFlags get() = OptimisationFlags(OPTIMISATION_FLAG_2ARG_LONG_LONG)
        override fun combine2ArgLongToLong(a: Long, b: Long) = if (a <= b) 1L else 0L
        override fun combine2ArgGenericToLong(a: APLSingleValue, b: APLSingleValue): Long =
            if (opLong(a, b)) 1 else 0

        override fun deriveBitwise() = BitwiseLessThanOrEqual()

        override val name2Arg get() = "less than or equals"
    }

    override fun make(instantiation: FunctionInstantiation) = LessThanEqualAPLFunctionImpl(instantiation)
}

class GreaterThanEqualAPLFunction : APLFunctionDescriptor {
    class GreaterThanEqualAPLFunctionImpl(pos: FunctionInstantiation) : MathCombineAPLFunction(pos, resultType2Arg = ArrayMemberType.BOOLEAN) {
        override fun combine2Arg(a: APLSingleValue, b: APLSingleValue): APLValue {
            return opLong(a, b).makeAPLNumberAsBoolean()
        }

        private fun opLong(a: APLSingleValue, b: APLSingleValue): Boolean {
            return a.numericCompare(b) >= 0
        }

        override fun identityValue() = APLLONG_1
        override val optimisationFlags get() = OptimisationFlags(OPTIMISATION_FLAG_2ARG_LONG_LONG)
        override fun combine2ArgLongToLong(a: Long, b: Long) = if (a >= b) 1L else 0L
        override fun combine2ArgGenericToLong(a: APLSingleValue, b: APLSingleValue): Long =
            if (opLong(a, b)) 1 else 0

        override fun deriveBitwise() = BitwiseGreaterThanOrEqual()

        override val name2Arg get() = "greater than or equals"
    }

    override fun make(instantiation: FunctionInstantiation) = GreaterThanEqualAPLFunctionImpl(instantiation)
}

fun Boolean.makeAPLNumberAsBoolean(): APLValue {
    return if (this) APLLONG_1 else APLLONG_0
}

object NumericRelationErrors {
    fun throwIncompatibleSingleArg(a: APLValue, pos: Position): Nothing {
        throwAPLException(IncompatibleTypeException("Function does not support arguments of type: ${a.kapClass.name}", pos))
    }

    fun throwIncompatibleArg(a: APLValue, b: APLValue, pos: Position?): Nothing {
        throwAPLException(IncompatibleTypeException("Incompatible argument types. Left arg: ${a.kapClass.name}, Right arg: ${b.kapClass.name}", pos))
    }

    fun throwBigintNotSupported(pos: Position): Nothing {
        throwAPLException(IncompatibleTypeException("Function does not support bigint arguments", pos))
    }

    fun throwCharNotSupported(pos: Position): Nothing {
        throwAPLException(IncompatibleTypeException("Function does not support char arguments", pos))
    }

    fun throwRationalNotSupported(pos: Position): Nothing {
        throwAPLException(IncompatibleTypeException("Function does not support rational arguments", pos))
    }
}

inline fun numericRelationOperation(
    pos: Position,
    a: APLSingleValue,
    b: APLSingleValue,
    fnLong: (Long, Long) -> APLValue,
    fnDouble: (Double, Double) -> APLValue,
    fnComplex: (Complex, Complex) -> APLValue,
    fnChar: ((Int, Int) -> APLValue) = { _, _ -> NumericRelationErrors.throwCharNotSupported(pos) },
    fnOther: ((aOther: APLValue, bOther: APLValue) -> APLValue) = { x, y -> NumericRelationErrors.throwIncompatibleArg(x, y, pos) },
    fnBigint: ((aBigint: BigInt, bBigint: BigInt) -> APLValue) = { _, _ -> NumericRelationErrors.throwBigintNotSupported(pos) },
    fnRational: ((aRational: Rational, bRational: Rational) -> APLValue) = { _, _ -> NumericRelationErrors.throwRationalNotSupported(pos) }
): APLValue {
    numericRelationOperation2(
        pos,
        a,
        b,
        { x, y ->
            return try {
                fnLong(x, y)
            } catch (e: LongExpressionOverflow) {
                e.result.makeAPLNumber()
            }
        },
        { x, y -> return fnDouble(x, y) },
        { x, y -> return fnComplex(x, y) },
        { x, y -> return fnChar(x, y) },
        { x, y -> return fnOther(x, y) },
        { x, y -> return fnBigint(x, y) },
        { x, y -> return fnRational(x, y) })
}

inline fun numericRelationOperation2(
    pos: Position,
    a: APLSingleValue,
    b: APLSingleValue,
    fnLong: (Long, Long) -> Nothing,
    fnDouble: (Double, Double) -> Nothing,
    fnComplex: (Complex, Complex) -> Nothing,
    fnChar: ((Int, Int) -> Nothing) = { _, _ -> NumericRelationErrors.throwCharNotSupported(pos) },
    fnOther: ((aOther: APLValue, bOther: APLValue) -> Nothing) = { x, y -> NumericRelationErrors.throwIncompatibleArg(x, y, pos) },
    fnBigint: ((aBigint: BigInt, bBigint: BigInt) -> Nothing) = { _, _ -> NumericRelationErrors.throwBigintNotSupported(pos) },
    fnRational: ((aRational: Rational, bRational: Rational) -> Nothing) = { _, _ -> NumericRelationErrors.throwRationalNotSupported(pos) }
): Nothing {
    when {
        a is APLNumber && b is APLNumber -> {
            when {
                a is APLLong && b is APLLong -> fnLong(a.value, b.value)
                a is APLComplex || b is APLComplex -> fnComplex(a.asComplex(), b.asComplex())
                a is APLDouble || b is APLDouble -> fnDouble(a.asDouble(), b.asDouble())
                a is APLRational || b is APLRational -> fnRational(a.asRational(pos), b.asRational(pos))
                a is APLBigInt || b is APLBigInt -> fnBigint(a.asBigInt(pos), b.asBigInt(pos))
                else -> NumericRelationErrors.throwIncompatibleArg(a, b, pos)
            }
        }
        a is APLChar && b is APLChar -> {
            fnChar(a.value, b.value)
        }
        else -> fnOther(a, b)
    }
}

inline fun singleArgNumericRelationOperation(
    pos: Position,
    a: APLSingleValue,
    fnLong: (Long) -> APLValue,
    fnDouble: (Double) -> APLValue,
    fnComplex: (Complex) -> APLValue,
    fnChar: ((Int) -> APLValue) = { _ -> throwAPLException(IncompatibleTypeException("Function does not support character argument", pos)) },
    fnBigInt: ((BigInt) -> APLValue) = { _ -> throwAPLException(IncompatibleTypeException("Function does not support bigint argument", pos)) },
    fnRational: ((Rational) -> APLValue) = { _ -> throwAPLException(IncompatibleTypeException("Function does not support rational argument", pos)) }
): APLValue {
    return when (a) {
        is APLLong -> fnLong(a.asLong(pos))
        is APLDouble -> fnDouble(a.asDouble(pos))
        is APLComplex -> fnComplex(a.asComplex())
        is APLChar -> fnChar(a.value)
        is APLBigInt -> fnBigInt(a.value)
        is APLRational -> fnRational(a.value)
        else -> NumericRelationErrors.throwIncompatibleSingleArg(a, pos)
    }
}

class CompareObjectsFunction : APLFunctionDescriptor {
    class CompareObjectsFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            return a.compareTotalOrdering(b, pos = pos, typeDiscrimination = true).makeAPLNumber()
        }
    }

    override fun make(instantiation: FunctionInstantiation) = CompareObjectsFunctionImpl(instantiation)
}

class CompareFunction : APLFunctionDescriptor {
    class CompareFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            fun recurse(v: APLValue): Int {
                val v0 = v.unwrapDeferredValue()
                val d = v0.dimensions
                return when {
                    v0 is APLSingleValue -> 0
                    d.size == 0 -> recurse(v0.disclose()) + 1
                    d.contentSize() == 0 -> 1
                    else -> {
                        var first = true
                        var currentSize = 0
                        v0.iterateMembers { inner ->
                            val size = recurse(inner)
                            if (first) {
                                currentSize = size
                                first = false
                            } else {
                                currentSize = max(currentSize, size)
                            }
                        }
                        currentSize + 1
                    }
                }
            }
            return recurse(a).makeAPLNumber()
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            return a.compareEqualsTotalOrdering(b, pos = pos, typeDiscrimination = false).makeAPLNumberAsBoolean()
        }

        override val name1Arg get() = "depth"
        override val name2Arg get() = "compare"
    }

    override fun make(instantiation: FunctionInstantiation) = CompareFunctionImpl(instantiation)
}

class CompareNotEqualFunction : APLFunctionDescriptor {
    class CompareNotEqualFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val dimensions = a.dimensions
            val ret = if (dimensions.size == 0) 1 else dimensions[0]
            return ret.makeAPLNumber()
        }

        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            return (!a.compareEqualsTotalOrdering(b, pos = pos, typeDiscrimination = false)).makeAPLNumberAsBoolean()
        }

        override val name1Arg get() = "size"
        override val name2Arg get() = "compare not equals"
    }

    override fun make(instantiation: FunctionInstantiation) = CompareNotEqualFunctionImpl(instantiation)
}
