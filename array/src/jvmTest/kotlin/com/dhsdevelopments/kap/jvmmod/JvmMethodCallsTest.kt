package com.dhsdevelopments.kap.jvmmod

import com.dhsdevelopments.kap.APLTest
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame
import kotlin.test.assertTrue

class Foo {
    @Suppress("unused")
    fun standardTest(): String {
        return "some result"
    }

    companion object {
        @Suppress("unused")
        @JvmStatic
        fun staticTest(arg: String): String {
            val result = "message from method: ${arg}"
            return result
        }

        @Suppress("unused")
        @JvmStatic
        fun throwExceptionsTest(): String {
            throw RuntimeException("Test exception")
        }
    }
}

class JvmMethodCallsTest : APLTest() {
    @Test
    fun findClass() {
        parseAPLExpression("jvm:findClass \"java.lang.String\"").let { result ->
            assertTrue(result is JvmInstanceValue)
            assertSame(String::class.java, result.instance)
        }
    }

    @Test
    fun findMethod() {
        parseAPLExpression("jvm:findMethod ((jvm:findClass \"com.dhsdevelopments.kap.jvmmod.Foo\") ; \"staticTest\" ; jvm:findClass \"java.lang.String\")").let { result ->
            assertTrue(result is JvmInstanceValue)
            val expected = Foo::class.java.getMethod("staticTest", String::class.java)
            assertEquals(expected, result.instance)
        }
    }

    @Test
    fun callMethod() {
        val result = parseAPLExpression(
            """                        
            |fooClass ← jvm:findClass "com.dhsdevelopments.kap.jvmmod.Foo"
            |constructor ← jvm:findConstructor fooClass 
            |method ← jvm:findMethod (fooClass ; "standardTest")
            |a ← jvm:createInstance constructor
            |jvm:callMethod (method ; a)
            """.trimMargin())
        assertTrue(result is JvmInstanceValue)
        val instance = result.instance
        assertTrue(instance is String)
        assertEquals("some result", instance)
    }

    @Test
    fun callStatic() {
        val result = parseAPLExpression(
            """
            |fooClass ← jvm:findClass "com.dhsdevelopments.kap.jvmmod.Foo"
            |method ← jvm:findMethod (fooClass ; "staticTest" ; jvm:findClass "java.lang.String")
            |jvm:callMethod (method ; null ; "qwe")
            """.trimMargin())
        assertTrue(result is JvmInstanceValue)
        val instance = result.instance
        assertTrue(instance is String)
        assertEquals("message from method: qwe", instance)
    }

    @Test
    fun catchException() {
        val result = parseAPLExpression(
            """
            |fooClass ← jvm:findClass "com.dhsdevelopments.kap.jvmmod.Foo"
            |method ← jvm:findMethod (fooClass ; "throwExceptionsTest")
            |{ jvm:callMethod (method ; null) } catch 'kap:jvmMethodCallException λ{ 1 }
            """.trimMargin()
        )
        assertSimpleNumber(1, result)
    }
}
