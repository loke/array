package com.dhsdevelopments.kap.gui

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.ffi.FfiModule
import com.dhsdevelopments.kap.gui.arrayedit.ArrayEditor
import com.dhsdevelopments.kap.gui.chart.JavaChartModule
import com.dhsdevelopments.kap.gui.fxanimation.FxAnimationModule2
import com.dhsdevelopments.kap.gui.graphics.GuiModule
import com.dhsdevelopments.kap.gui.objview.ObjectViewerRegistry
import com.dhsdevelopments.kap.gui.report2.ReportClient
import com.dhsdevelopments.kap.gui.settings.ConfigProvider
import com.dhsdevelopments.kap.gui.settings.ConfigProviderImpl
import com.dhsdevelopments.kap.gui.settings.loadSettings
import com.dhsdevelopments.kap.gui.settings.saveSettings
import com.dhsdevelopments.kap.gui.styledarea.TextStyle
import com.dhsdevelopments.kap.gui.syms.LanguageBar
import com.dhsdevelopments.kap.gui.viewer.StructureViewer
import com.dhsdevelopments.kap.keyboard.ExtendedCharsKeyboardInput
import com.dhsdevelopments.kap.repl.AbstractGenericRepl
import com.dhsdevelopments.kap.repl.JvmReplExceptionTransformer
import com.panemu.tiwulfx.control.dock.DetachableTab
import javafx.application.Application.launch
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.scene.text.Font
import javafx.stage.DirectoryChooser
import javafx.stage.FileChooser
import javafx.stage.Stage
import java.io.File
import java.nio.file.Path
import java.util.*
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract
import kotlin.io.path.isDirectory
import kotlin.system.exitProcess

class Client(val clientApplication: ApplicationServices, val stage: Stage, val repl: AbstractGenericRepl) {
    val engine: Engine get() = repl.engine
    val renderContext: ClientRenderContext = ClientRenderContextImpl()
    val resultList: ResultList3
    val calculationQueue: CalculationQueue
    val sourceEditors = ArrayList<SourceEditor>()
    val timer = Timer("Client timer", true)
    private var inputFont: Font
    private val functionListWindow: FunctionListController
    private val varListWindow: VariableListController
    private val keyboardHelpWindow: KeyboardHelpWindow
    private val horizSplitPane: SplitPane
    private var leftDtPane: BorderedDetatchablePane? = null
    private var rightDtPane: BorderedDetatchablePane? = null
    private var bottomDtPane: DetachablePaneWrapper
    private val aboutWindow: AboutWindow
    private val directoryTextField = TextField()
    private val breakButton = Button("Stop")
    private val stackTraceWindow: StackTrace
    private val configProviderImpl: ConfigProviderImpl

    val config: ConfigProvider get() = configProviderImpl
    val objectViewerRegistry = ObjectViewerRegistry()
    val detailsPopupManager = DetailsPopupManager(this)

    init {
        initGuiModules(repl)

        configProviderImpl = ConfigProviderImpl(loadSettings())

        engine.standardOutput = SendToMainCharacterOutput()
        calculationQueue = CalculationQueue(engine)
        (engine.nativeData as JvmNativeData).let { nativeData ->
            nativeData.asyncJobPublisher = { result -> pushComputeResultToQueue(result) }
            nativeData.keyboardInput = FxClientKeyboardInput()
        }

        registerClientCommands()

        val fontFiles = listOf("iosevka-fixed-regular.ttf", "iosevka-fixed-bold.ttf")
        fontFiles.forEach { fileName ->
            javaClass.getResourceAsStream("fonts/${fileName}").use { contentIn ->
                Font.loadFont(contentIn, 18.0) ?: throw IllegalStateException("Unable to load font: ${fileName}")
            }
        }

        inputFont = Font(config.settings().fontFamilyWithDefault(), config.settings().fontSizeWithDefault().toDouble())

        resultList = ResultList3(this)

        stage.title = "Kap"

        horizSplitPane = SplitPane().apply {
            orientation = Orientation.HORIZONTAL
            items.add(BorderPane().also { p ->
                p.center = resultList.getNode()
            })
        }

        bottomDtPane = DetachablePaneWrapper()
        val vertSplitPane = SplitPane().apply {
            orientation = Orientation.VERTICAL
            items.add(horizSplitPane)
            items.add(BorderPane().apply { center = bottomDtPane.pane })
            setDividerPosition(0, 0.75)
        }

        val border = BorderPane().apply {
            top = makeTopBar()
            center = vertSplitPane
        }

        stackTraceWindow = StackTrace.makeStackTraceWindow(this)
        bottomDtPane.pane.tabs.add(DetachableTab("Stack trace", stackTraceWindow.borderPane).apply { isClosable = false })

        functionListWindow = FunctionListController(engine)
        bottomDtPane.pane.tabs.add(DetachableTab("Function list", functionListWindow.node).apply { isClosable = false })

        varListWindow = VariableListController(this)
        bottomDtPane.pane.tabs.add(DetachableTab("Variable list", varListWindow.node).apply { isClosable = false })

        keyboardHelpWindow = KeyboardHelpWindow(renderContext)
        aboutWindow = AboutWindow()

        updateWorkingDirectory(config.settings().directory ?: currentDirectory())

        calculationQueue.start()
        stage.onCloseRequest = EventHandler { stopRequest() }
        if (repl.hasStartupFiles()) {
            calculationQueue.pushJobToQueue {
                repl.loadStartupFiles()
            }
        }

        stage.scene = Scene(border, 1500.0, 1000.0)
        stage.show()

        resultList.requestFocus()
    }

    private fun registerClientCommands() {
        repl.engine.commandManager.registerQuitHandler {
            Platform.runLater {
                close()
            }
        }
    }

    private fun viewLeftPane() {
        val leftDtPaneCopy = leftDtPane
        if (leftDtPaneCopy == null) {
            val pane = BorderedDetatchablePane()
            horizSplitPane.items.add(0, pane)
            leftDtPane = pane
        } else {
            bottomDtPane.pane.tabs.addAll(leftDtPaneCopy.dtPane.pane.tabs)
            horizSplitPane.items.remove(leftDtPaneCopy)
            leftDtPane = null
        }
    }

    private fun viewRightPane() {
        val rightDtPaneCopy = rightDtPane
        if (rightDtPaneCopy == null) {
            val pane = BorderedDetatchablePane()
            horizSplitPane.items.add(pane)
            rightDtPane = pane
        } else {
            bottomDtPane.pane.tabs.addAll(rightDtPaneCopy.dtPane.pane.tabs)
            horizSplitPane.items.remove(rightDtPaneCopy)
            rightDtPane = null
        }
    }

    private fun pushComputeResultToQueue(job: AsyncJob) {
        calculationQueue.pushJobToQueue { engine ->
            val res = engine.withThreadLocalAssigned {
                job.callback()
            }
            when (res) {
                is Either.Left -> {
                    // Drop the result
                }
                is Either.Right -> {
                    Platform.runLater {
                        resultList.addExceptionResult(res.value)
                    }
                }
            }
        }
    }

    private fun makeTopBar(): VBox {
        val vbox = VBox()
        vbox.children.add(makeMenuBar())
        vbox.children.add(makeToolBar())
        vbox.children.add(LanguageBar(this))
        return vbox
    }

    private fun makeMenuBar(): MenuBar {
        return MenuBar().apply {
            val fileMenu = Menu("File").apply {
                id = "fileMenu"
                items.add(MenuItem("New").apply {
                    id = "newMenuAction"
                    onAction = EventHandler { openNewFile() }
                })
                items.add(MenuItem("Open").apply {
                    id = "openMenuAction"
                    onAction = EventHandler { selectAndEditFile() }
                })
                items.add(MenuItem("Settings").apply {
                    id = "settingsMenuAction"
                    onAction = EventHandler { openSettingsWindow() }
                })
                items.add(MenuItem("Close").apply {
                    id = "closeMenuAction"
                    onAction = EventHandler { close() }
                })
            }
            menus.add(fileMenu)

            val interpreterMenu = Menu("History").apply {
                id = "interpreterMenu"
                items.add(MenuItem("Clear log").apply {
                    id = "clearLogMenuAction"
                    onAction = EventHandler { clearLog() }
                })
            }
            menus.add(interpreterMenu)

            val windowMenu = Menu("Window").apply {
                id = "windowMenu"
                items.add(MenuItem("Array Editor").apply {
                    id = "arrayEditorMenuAction"
                    onAction = EventHandler { Platform.runLater { ArrayEditor.open(this@Client) } }
                })
                items.add(MenuItem("Structure Viewer").apply {
                    id = "structureViewerMenuAction"
                    onAction = EventHandler { Platform.runLater { StructureViewer.open(this@Client) } }
                })
                items.add(MenuItem("Create Report").apply {
                    id = "createReportMenuAction"
                    onAction = EventHandler { createReport() }
                })
                items.add(Menu("Pane").apply {
                    id = "paneMenu"
                    items.add(MenuItem("View Left Pane").apply {
                        id = "leftPaneAction"
                        onAction = EventHandler { viewLeftPane() }
                    })
                    items.add(MenuItem("View Right Pane").apply {
                        id = "rightPaneAction"
                        onAction = EventHandler { viewRightPane() }
                    })
                })
            }
            menus.add(windowMenu)

            val helpMenu = Menu("Help").apply {
                id = "helpMenu"
                items.add(MenuItem("About").apply {
                    id = "aboutMenuAction"
                    onAction = EventHandler { aboutWindow.show() }
                })
                items.add(MenuItem("Kap website").apply {
                    id = "kapWebsiteMenuAction"
                    onAction = EventHandler {
                        clientApplication.showDocument("https://kapdemo.dhsdevelopments.com/")
                    }
                })
                items.add(MenuItem("Keyboard").apply {
                    id = "keyboardMenuAction"
                    onAction = EventHandler { keyboardHelpWindow.show() }
                })
            }
            menus.add(helpMenu)
        }
    }

    private fun initGuiModules(repl: AbstractGenericRepl) {
        val engine = repl.engine
        engine.addModule(GuiModule())
        engine.addModule(JavaChartModule())
        engine.addModule(FxAnimationModule2())
//        engine.addModule(RaylibModule())
    }

    private fun close() {
        if (stopRequest()) {
            stage.close()
        }
    }

    private fun makeToolBar(): ToolBar {
        return ToolBar(makeWorkingDirectoryButton(), makeBreakButton())
    }

    private fun makeWorkingDirectoryButton(): Node {
        val hbox = HBox()
        hbox.alignment = Pos.BASELINE_LEFT

        val label = Label("Working directory:")
        label.onMouseClicked = EventHandler { selectWorkingDirectory() }
        hbox.children.add(label)
        HBox.setMargin(label, Insets(0.0, 5.0, 0.0, 5.0))

        directoryTextField.prefColumnCount = 20
        hbox.children.add(directoryTextField)

        directoryTextField.onAction = EventHandler {
            val name = directoryTextField.text.trim()
            val path = Path.of(name)
            if (path.isDirectory()) {
                updateWorkingDirectory(name)
            } else {
                directoryTextField.text = engine.workingDirectory ?: currentDirectory()
                displayErrorWithStage(stage, "Path is not a directory: ${name}")
            }
        }

        engine.addWorkingDirectoryListener { newName ->
            Platform.runLater {
                directoryTextField.text = newName ?: currentDirectory()
            }
        }

        return hbox
    }

    private fun makeBreakButton(): Node {
        breakButton.onMouseClicked = EventHandler { interruptEvaluation() }
        return breakButton
    }

    private fun interruptEvaluation() {
        engine.interruptEvaluation()
    }

    private fun selectWorkingDirectory() {
        val fileSelector = DirectoryChooser().apply {
            title = "Select working directory"
            val dirString = config.settings().directory
            if (dirString != null) {
                val dir = File(dirString)
                if (dir.isDirectory) {
                    initialDirectory = dir
                }
            }
        }
        val res = fileSelector.showDialog(stage)
        if (res != null) {
            if (!res.isDirectory) {
                val dialog = Dialog<ButtonType>().apply {
                    title = "Not a valid directory"
                    contentText = "The selected directory is not a directory, or is not readable."
                    dialogPane.buttonTypes.apply {
                        add(ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE))
                    }
                }
                dialog.showAndWait()
            } else {
                updateWorkingDirectory(res.path)
            }
        }
    }

    private fun updateWorkingDirectory(dir: String) {
        engine.workingDirectory = dir
        configProviderImpl.updateSettings(config.settings().copy(directory = dir))
        directoryTextField.text = dir
    }

    private fun clearLog() {
        resultList.clearLog()
    }

    private fun openNewFile() {
        createEditor().show()
    }

    private fun createEditor(): SourceEditor {
        return SourceEditor(this)
    }

    fun selectFile(forSave: Boolean = false, nameHeader: String = "Open Kap file", extensions: List<Pair<String, List<String>>>? = null): File? {
        val extensions = extensions ?: listOf(Pair("Kap files", listOf("kap")))
        require(extensions.isNotEmpty())
        val fileSelector = FileChooser().apply {
            val filters =
                extensions.map { (name, extList) ->
                    FileChooser.ExtensionFilter(name, extList.map { e -> "*.${e}" })
                }

            extensionFilters.addAll(filters + listOf(FileChooser.ExtensionFilter("All files", "*")))

            title = nameHeader
            selectedExtensionFilter = filters.first()
            val dir = config.settings().recentPath
            if (dir != null) {
                val file = File(dir)
                if (file.isDirectory) {
                    initialDirectory = file
                }
            }
        }
        val file = if (forSave) {
            val resFile = fileSelector.showSaveDialog(stage)
            val extName = extensions.first().second.first()
            if (!resFile.exists() && resFile.extension != extName) {
                File("${resFile.path}.${extName}")
            } else {
                resFile
            }
        } else {
            fileSelector.showOpenDialog(stage)
        }
        if (file != null) {
            configProviderImpl.updateSettings(config.settings().copy(recentPath = file.parent))
        }
        return file
    }

    private fun selectAndEditFile() {
        selectFile()?.let { file ->
            val editor = createEditor()
            editor.setFile(file)
            editor.show()
        }
    }

    private fun createReport() {
        ReportClient.open(this)
    }

    private fun openSettingsWindow() {
        val dialog = SettingsDialog(stage, config.settings())
        dialog.showAndWait().ifPresent { result ->
            configProviderImpl.updateSettings(result)
            saveSettings(config.settings())
        }
    }


    fun evalSource(source: SourceLocation, preserveNamespace: Boolean = false) {
        resultList.updateInputStateDisabled()
        calculationQueue.pushRequest(source, preserveNamespace = preserveNamespace) { result ->
            Platform.runLater {
                displayResult(result)
                resultList.updateInputStateRepl()
            }
        }
    }

    // Suppress here because of this issue https://youtrack.jetbrains.com/issue/KTIJ-20744
    @Suppress("USELESS_IS_CHECK")
    private fun displayResult(result: Either<EvalExpressionResult, Exception>) {
        when (result) {
            is Either.Left -> resultList.addResult(result.value)
            is Either.Right -> {
                val ex = result.value
                resultList.addExceptionResult(ex)
                if (ex is APLGenericException) {
                    if (ex.pos != null) {
                        val pos = ex.pos
                        if (pos != null) {
                            highlightSourceLocation(pos, ex.message ?: "no error message")
                        }
                    }
                    if (ex is APLEvalException) {
                        stackTraceWindow.updateException(ex)
                        highlightErrorInReplFromStackTrace(ex)
                    }
                    if (ex is WrappedException) {
                        ex.cause.printStackTrace()
                    }
                }
            }
        }
    }

    private fun highlightErrorInReplFromStackTrace(ex: APLEvalException) {
        if (maybeHighlightErrorInRepl(ex.pos)) {
            return
        }
        val stack = ex.stack
        if (stack != null) {
            stack.frames.asReversed().forEach { entry ->
                if (maybeHighlightErrorInRepl(entry.primaryPos)) {
                    return
                }
            }
        }
    }

    private fun maybeHighlightErrorInRepl(p: Position?): Boolean {
        if (p != null) {
            val s = p.source
            if (s is REPLSourceLocation) {
                highlightErrorInRepl(s, p)
                return true
            }
        }
        return false
    }

    fun highlightSourceLocation(pos: Position, message: String? = null) {
        when (val sourceLocation = pos.source) {
            is SourceEditor.EditorSourceLocation -> {
                sourceLocation.editor?.let { editor ->
                    sourceEditors.forEach { e ->
                        if (e === editor) {
                            editor.highlightError(pos, message)
                        }
                    }
                }
            }
//            is REPLSourceLocation -> {
//                highlightErrorInRepl(sourceLocation, pos)
//            }
        }
    }

    private fun highlightErrorInRepl(sourceLocation: REPLSourceLocation, pos: Position) {
        resultList.updateStyle(
            sourceLocation.tag,
            pos.line,
            pos.col,
            pos.computedEndLine,
            pos.computedEndCol,
            TextStyle(config, TextStyle.Type.SINGLE_CHAR_HIGHLIGHT))
    }

    fun stopRequest(): Boolean {
        val editorsCopy = ArrayList(sourceEditors)
        editorsCopy.forEach { editor ->
            if (!editor.closeMaybeSave(true)) {
                return false
            }
        }
        saveSettings(config.settings())
        calculationQueue.stop()
        repl.close()
        return true
    }

    private inner class ClientRenderContextImpl : ClientRenderContext {
        private val extendedInput = ExtendedCharsKeyboardInput()

        override fun engine() = engine
        override fun font() = inputFont
        override fun extendedInput() = extendedInput
    }

    private inner class SendToMainCharacterOutput : CharacterOutput {
        override fun writeString(s: String) {
            Platform.runLater {
                resultList.addOutput(s)
            }
        }
    }

    @OptIn(ExperimentalContracts::class)
    @Suppress("WRONG_INVOCATION_KIND")
    fun withErrorDialog(name: String, details: String? = null, fn: () -> Unit) {
        contract {
            callsInPlace(fn, InvocationKind.EXACTLY_ONCE)
        }
        try {
            fn()
        } catch (e: Exception) {
            e.printStackTrace()
            val dialog = Alert(Alert.AlertType.ERROR)
            dialog.initOwner(stage)
            dialog.title = "Error: ${name}"
            if (details != null) {
                dialog.dialogPane.contentText = "${details}: ${e.message}"
            }
            dialog.showAndWait()
        }
    }

    inner class FxClientKeyboardInput : KeyboardInput {
        override fun readString(prompt: String): String? {
            require(!Platform.isFxApplicationThread())
            Platform.runLater {
                resultList.updateInputStatePrompted(prompt)
                resultList.showBottomParagraphAtTop()
            }
            return resultList.promptedInputQueue.take()
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val builder = FxClientGenericReplBuilder()
            builder.exceptionTransformer = JvmReplExceptionTransformer()
            val repl = builder.build(args) ?: return
            if (repl.useGui && !repl.inhibitRepl()) {
                startGui(repl, args)
            } else {
                try {
                    repl.engine.standardOutput = repl.makeDefaultStdout()
                    engineInit(repl)
                    repl.loadStartupFiles()
                    repl.engine.commandManager.registerQuitHandler {
                        repl.close()
                        exitProcess(0)
                    }
                    if (!repl.inhibitRepl()) {
                        repl.mainLoop()
                    }
                } finally {
                    repl.close()
                }
            }
        }

        var replGlobal: AbstractGenericRepl? = null
            private set(newValue) {
                require(field == null) { "Attempt to change replGlobal" }
                require(newValue != null) { "Attempt to set replGlobal to null" }
                field = newValue
            }

        private fun startGui(repl: AbstractGenericRepl, args: Array<String>) {
            var done = false
            try {
                engineInit(repl)
                done = true
            } finally {
                if (!done) {
                    repl.close()
                }
            }

            require(done) { "Try/finally block finished but 'done' was false" }
            replGlobal = repl
            launch(ClientApplication::class.java, *args)
        }

        private fun engineInit(repl: AbstractGenericRepl) {
            val installPath = findInstallPath()
            if (installPath == null) {
                repl.engine.addLibrarySearchPath("../array/standard-lib")
            } else {
                repl.engine.addLibrarySearchPath("${installPath}/standard-lib")
            }

            repl.init()

            initStandardModules(repl)

            //        engine.addLibrarySearchPath("../array/standard-lib")
//        extraPaths?.forEach(engine::addLibrarySearchPath)
//        initModules()
//        engine.parseAndEval(StringSourceLocation("use(\"standard-lib.kap\")"))
//
//        engine.standardOutput = SendToMainCharacterOutput()

        }

        private fun initStandardModules(repl: AbstractGenericRepl) {
            val engine = repl.engine
            engine.addModule(FfiModule.make()!!)
        }
    }

    /**
     * Wrapper for a detachable pane that lives in a SplitPane and should not be resizable with the parent
     */
    private class BorderedDetatchablePane : BorderPane() {
        val dtPane = DetachablePaneWrapper()

        init {
            SplitPane.setResizableWithParent(this, false)
            center = dtPane.pane
        }
    }
}
