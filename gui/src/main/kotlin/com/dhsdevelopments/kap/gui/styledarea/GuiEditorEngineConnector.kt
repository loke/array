package com.dhsdevelopments.kap.gui.styledarea

import com.dhsdevelopments.kap.edit.EditorEngineConnector
import com.dhsdevelopments.kap.edit.parenCandidates
import com.dhsdevelopments.kap.gui.Client
import javafx.application.Platform

class GuiEditorEngineConnector(private val client: Client) : EditorEngineConnector {
    override fun findParenCandidates(src: String, index: Int, callback: (List<Int>) -> Unit) {
        client.calculationQueue.pushJobToQueue { engine ->
            val res = parenCandidates(engine, src, index)
            Platform.runLater {
                callback(res)
            }
        }
    }
}
