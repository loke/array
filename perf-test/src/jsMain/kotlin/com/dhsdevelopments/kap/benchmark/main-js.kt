package com.dhsdevelopments.kap.benchmark

import com.dhsdevelopments.kap.RegisteredEntry
import com.dhsdevelopments.kap.registeredFilesRoot

/*
Running jsNodeRun:
Running tests: js
primes: avg=7610.3, median=7595, min=7535, max=7700, stddev=1.0000203429011993
var lookup scope: avg=5933.8, median=5948, min=5882, max=5992, stddev=1.0000169689073763
contrib bench: avg=1441.5, median=1454.5, min=1427, max=1467, stddev=1.0000327846161194
simple sum: avg=1161.6, median=1169, min=1152, max=1179, stddev=1.0000278063243908
multiple call: avg=555.6, median=557, min=550, max=560, stddev=1.0000126986874875

With overflow bignum:
Running tests: js
primes: avg=9697.8, median=9868.5, min=9582, max=10031, stddev=1.0000777406036818
var lookup scope: avg=9947.8, median=9916.5, min=9913, max=9995, stddev=1.000002908076898
contrib bench: avg=1954.6, median=1950.5, min=1948, max=1962, stddev=1.000001392501081
simple sum: avg=14390.3, median=14443, min=14347, max=14486, stddev=1.0000032538222676
multiple call: avg=1298.9, median=1300, min=1295, max=1301, stddev=1.0000008564780307

Remove extra try/catch in addExact:
Running tests: js
primes: avg=9093.8, median=9212, min=8941, max=9213, stddev=1.0000605477393347
var lookup scope: avg=9686.8, median=9747.5, min=9581, max=9815, stddev=1.0000338024938362
contrib bench: avg=1920.5, median=1919.5, min=1905, max=1935, stddev=1.0000128309618421
simple sum: avg=14367.5, median=14368, min=14270, max=14511, stddev=1.0000096568479195
multiple call: avg=1325.4, median=1318, min=1317, max=1337, stddev=1.0000112256252656

Before updated tracking of type specialisation:
Running tests: js
primes: avg=7465.2, median=7481.5, min=7412, max=7498, stddev=1.0000065796516198
var lookup scope: avg=645, median=644.5, min=643, max=646, stddev=1.0000009614802179
contrib bench: avg=1493.4, median=1495, min=1489, max=1503, stddev=1.0000031924713346
simple sum: avg=5087.1, median=5103, min=5064, max=5112, stddev=1.000005357503938
multiple call: avg=818.4, median=818.5, min=814, max=821, stddev=1.0000039117329698
formatter: avg=254.7, median=252, min=247, max=260, stddev=1.0001264716642058
decode: avg=2174.1, median=2169, min=2144, max=2197, stddev=1.0000418982373112

After updated tracking of type specialisation:
Running tests: js
Running tests: js
primes: avg=6463.2, median=6526, min=6383, max=6641, stddev=1.0000656522868572
var lookup scope: avg=644.7, median=637, min=635, max=677, stddev=1.000161678078281
contrib bench: avg=1364.7, median=1357.5, min=1352, max=1387, stddev=1.000027601010795
simple sum: avg=5072.9, median=5027.5, min=5018, max=5259, stddev=1.0001059597116542
multiple call: avg=856, median=851, min=846, max=874, stddev=1.000065642224971
formatter: avg=257.7, median=252.5, min=250, max=263, stddev=1.0001521508710032
decode: avg=2156.3, median=2130, min=2121, max=2302, stddev=1.0002817257648802
*/

var jsFilesystem: dynamic = js("require('fs')")

fun main() {
    loadFs()
    runAllTests("js", "standard-lib", "benchmark-reports", "unnamed")
}

fun loadFs() {
    fun readFileRecurse(fsDir: String, dir: RegisteredEntry.Directory) {
        val files = jsFilesystem.readdirSync(fsDir) as Array<String>
        files.forEach { name ->
            val newName = "${fsDir}/${name}"
            val result = jsFilesystem.statSync(newName)
            when {
                result.isDirectory() -> readFileRecurse(newName, dir.createDirectory(name, false))
                result.isFile() -> {
                    val content = jsFilesystem.readFileSync(newName)
                    dir.registerFile(name, content)
                }
            }
        }
    }

    fun initDirectory(fsDir: String, base: String) {
        readFileRecurse("${fsDir}/${base}", registeredFilesRoot.createDirectory(base, errorIfExists = false))
    }

    initDirectory("../../../../array", "standard-lib")
    initDirectory("../../../../array", "test-data")
}
