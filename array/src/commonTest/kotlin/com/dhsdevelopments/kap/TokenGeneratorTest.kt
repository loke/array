package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.complex.Complex
import com.dhsdevelopments.mpbignum.BigInt
import com.dhsdevelopments.mpbignum.Rational
import com.dhsdevelopments.mpbignum.make
import com.dhsdevelopments.mpbignum.of
import kotlin.test.*

class TokenGeneratorTest : APLTest() {
    @Test
    fun testSimpleToken() {
        val gen = makeGenerator("foo")
        val token = gen.nextToken()
        assertTokenIsSymbol(gen, token, "foo", gen.engine.initialNamespace.name)
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun testMultipleTokens() {
        val gen = makeGenerator("foo bar test abc test")
        val expectedTokens = arrayOf("foo", "bar", "test", "abc", "test")
        expectedTokens.forEach { name ->
            val token = gen.nextToken()
            assertTokenIsSymbol(gen, token, name, gen.engine.initialNamespace.name)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun testMultipleTokensSpecialChars() {
        val gen = makeGenerator("foo bar test abc test ∆ ⍙ a∆ a⍙ ∆b ⍙b x∆y x⍙y")
        val expectedTokens = arrayOf("foo", "bar", "test", "abc", "test", "∆", "⍙", "a∆", "a⍙", "∆b", "⍙b", "x∆y", "x⍙y")
        expectedTokens.forEach { name ->
            val token = gen.nextToken()
            assertTokenIsSymbol(gen, token, name, gen.engine.initialNamespace.name)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun testArgSymbols() {
        val gen = makeGenerator("⍵ ⍺ ⍺x ⍵x qz⍺x⍵zq")
        assertTokenIsSymbol(gen, gen.nextToken(), "⍵", gen.engine.coreNamespace.name)
        assertTokenIsSymbol(gen, gen.nextToken(), "⍺", gen.engine.coreNamespace.name)
        assertTokenIsSymbol(gen, gen.nextToken(), "⍺", gen.engine.coreNamespace.name)
        assertTokenIsSymbol(gen, gen.nextToken(), "x", gen.engine.initialNamespace.name)
        assertTokenIsSymbol(gen, gen.nextToken(), "⍵", gen.engine.coreNamespace.name)
        assertTokenIsSymbol(gen, gen.nextToken(), "x", gen.engine.initialNamespace.name)
        assertTokenIsSymbol(gen, gen.nextToken(), "qz", gen.engine.initialNamespace.name)
        assertTokenIsSymbol(gen, gen.nextToken(), "⍺", gen.engine.coreNamespace.name)
        assertTokenIsSymbol(gen, gen.nextToken(), "x", gen.engine.initialNamespace.name)
        assertTokenIsSymbol(gen, gen.nextToken(), "⍵", gen.engine.coreNamespace.name)
        assertTokenIsSymbol(gen, gen.nextToken(), "zq", gen.engine.initialNamespace.name)
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun syntacticalTokens() {
        val gen = makeGenerator("( ) { } [ ] ← ◊ ⋄ ∇ ⇐ ⍬ λ ⍞ ; « » .")
        assertIs<OpenParen>(gen.nextToken())
        assertIs<CloseParen>(gen.nextToken())
        assertIs<OpenFnDef>(gen.nextToken())
        assertIs<CloseFnDef>(gen.nextToken())
        assertIs<OpenBracket>(gen.nextToken())
        assertIs<CloseBracket>(gen.nextToken())
        assertIs<LeftArrow>(gen.nextToken())
        assertIs<StatementSeparator>(gen.nextToken())
        assertIs<StatementSeparator>(gen.nextToken())
        assertIs<FnDefSym>(gen.nextToken())
        assertIs<FnDefArrow>(gen.nextToken())
        assertIs<APLNullSym>(gen.nextToken())
        assertIs<LambdaToken>(gen.nextToken())
        assertIs<ApplyToken>(gen.nextToken())
        assertIs<ListSeparator>(gen.nextToken())
        assertIs<LeftForkToken>(gen.nextToken())
        assertIs<RightForkToken>(gen.nextToken())
        assertIs<MemberDereferenceToken>(gen.nextToken())
        assertIs<EndOfFile>(gen.nextToken())
    }

    @Test
    fun testMultipleSpaces() {
        val gen = makeGenerator("     foo       bar     test        ")
        val expectedTokens = arrayOf("foo", "bar", "test")
        expectedTokens.forEach { name ->
            val token = gen.nextToken()
            assertTokenIsSymbol(gen, token, name, gen.engine.initialNamespace.name)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun testNewline() {
        val gen = makeGenerator("foo\nbar test")
        assertTokenIsSymbol(gen, gen.nextToken(), "foo")
        assertSame(Newline, gen.nextToken())
        assertTokenIsSymbol(gen, gen.nextToken(), "bar")
        assertTokenIsSymbol(gen, gen.nextToken(), "test")
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun newlinePara() {
        val gen = makeGenerator("foo\nbar\nabc")
        assertTokenIsSymbol(gen, gen.nextToken(), "foo", gen.engine.initialNamespace.name)
        assertSame(Newline, gen.nextToken())
        assertTokenIsSymbol(gen, gen.nextToken(), "bar", gen.engine.initialNamespace.name)
        assertSame(Newline, gen.nextToken())
        assertTokenIsSymbol(gen, gen.nextToken(), "abc", gen.engine.initialNamespace.name)
    }

    @Test
    fun singleCharFunction() {
        val gen = makeGenerator("+-,,")
        val expectedTokens = arrayOf("+", "-", ",", ",")
        expectedTokens.forEach { name ->
            val token = gen.nextToken()
            val ns = gen.engine.findNamespace("kap")
            assertNotNull(ns)
            assertTokenIsSymbol(gen, token, name, ns.name)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun parseNumbers() {
        val gen = makeGenerator("10 20 1 ¯1 ¯10")
        val expectedTokens = arrayOf(10, 20, 1, -1, -10)
        expectedTokens.forEach { value ->
            val token = gen.nextToken()
            assertTrue(token is ParsedLong)
            assertEquals(value.toLong(), token.value)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun parseNumberTypes() {
        val gen = makeGenerator("1 2 1.2 2.0 9. ¯2.3 ¯2. 0 0.0")
        assertInteger(1, gen.nextToken())
        assertInteger(2, gen.nextToken())
        assertDouble(Pair(1.11119, 1.20001), gen.nextToken())
        assertDouble(Pair(1.99999, 2.00001), gen.nextToken())
        assertDouble(Pair(8.99999, 9.00001), gen.nextToken())
        assertDouble(Pair(-2.30001, -2.29999), gen.nextToken())
        assertDouble(Pair(-2.00001, -1.99999), gen.nextToken())
        assertInteger(0, gen.nextToken())
        assertDouble(Pair(-0.00001, 0.00001), gen.nextToken())
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun parseDoubleWithScientificNotation() {
        val gen = makeGenerator("1e4 ¯1e2 1e20 2.1e10 1e¯30 ¯2.3e¯30")
        assertExactDouble(1e4, gen.nextToken())
        assertExactDouble(-1e2, gen.nextToken())
        assertExactDouble(1e20, gen.nextToken())
        assertExactDouble(2.1e10, gen.nextToken())
        assertExactDouble(1e-30, gen.nextToken())
        assertExactDouble(-2.3e-30, gen.nextToken())
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun parseHexNumbers() {
        val gen = makeGenerator(
            """
            |0x0 0x1 0x000001 0x000000000000000000000000000000000001 0x123456789abcdef1234567890
            |0xffffffff 0x100000000 0x7fffffff 0x80000000 0x7fffffffffffffff 0x8000000000000000
            |¯0x1234abcdef ¯0xabcdefabcdefabcdef1234567890            
            """.trimMargin().replace('\n', ' '))
        assertInteger(0, gen.nextToken())
        assertInteger(1, gen.nextToken())
        assertInteger(1, gen.nextToken())
        assertInteger(1, gen.nextToken())
        assertBigInt(BigInt.of("123456789abcdef1234567890", 16), gen.nextToken())
        assertInteger(0xffffffffL, gen.nextToken())
        assertInteger(0x100000000L, gen.nextToken())
        assertInteger(0x7fffffffL, gen.nextToken())
        assertInteger(0x80000000L, gen.nextToken())
        assertInteger(0x7fffffffffffffffL, gen.nextToken())
        assertBigInt(BigInt.of("8000000000000000", 16), gen.nextToken())
        assertInteger(-0x1234abcdefL, gen.nextToken())
        assertBigInt(BigInt.of("-abcdefabcdefabcdef1234567890", 16), gen.nextToken())
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun parseBinaryNumbers() {
        val gen = makeGenerator(
            """
            |0b0 0b1 0b00001 0b1000 0b1000000000000000000000000000000000000000000000000000000000000000000000
            |0b11111111111111111 0b111111111111111111111111111111111111111111111111111111111111111111111111
            |0b000001000000000000000000000011111000000000
            |0b00000000000000000000000000000000000000000000000000000000000000000000000000000000001
            |¯0b111111111111111111111111111111111111111111111111111111111000000000000000000000000000000000000000
            |¯0b1 ¯0b111111 ¯0b00001100 ¯0b100000000000000000000000000000000000000000000000000000000000000000000000
            """.trimMargin().replace('\n', ' '))
        assertInteger(0, gen.nextToken())
        assertInteger(1, gen.nextToken())
        assertInteger(1, gen.nextToken())
        assertInteger(8, gen.nextToken())
        assertBigInt(BigInt.of("590295810358705651712"), gen.nextToken())
        assertInteger(131071, gen.nextToken())
        assertBigInt(BigInt.of("4722366482869645213695"), gen.nextToken())
        assertInteger(68719492608, gen.nextToken())
        assertInteger(1, gen.nextToken())
        assertBigInt(BigInt.of("-79228162514264337043788136448"), gen.nextToken())
        assertInteger(-1, gen.nextToken())
        assertInteger(-63, gen.nextToken())
        assertInteger(-12, gen.nextToken())
        assertBigInt(BigInt.of("-2361183241434822606848"), gen.nextToken())
        assertSame(EndOfFile, gen.nextToken())
    }

    private fun assertExactDouble(expected: Double, token: Token) {
        assertTrue(token is ParsedDouble)
        assertEquals(expected, token.value)
    }

    private fun assertDouble(expected: Pair<Double, Double>, token: Token) {
        assertTrue(token is ParsedDouble)
        assertTrue(expected.first <= token.value)
        assertTrue(expected.second >= token.value)
    }

    private fun assertInteger(expected: Long, token: Token) {
        assertTrue(token is ParsedLong)
        assertEquals(expected, token.value)
    }

    private fun assertBigInt(expected: BigInt, token: Token) {
        assertTrue(token is ParsedBigInt, "Argument was not a bigint. Type: ${token::class.simpleName}")
        assertEquals(expected, token.value, "Expected result: ${expected}, actual: ${token.value}")
    }

    @Test
    fun parseInvalidNumbers() {
        assertFailsWith<IllegalNumberFormat> {
            val gen = makeGenerator("2000a")
            gen.nextToken()
        }
    }

    @Test
    fun parseComments0() {
        val gen = makeGenerator("foo ⍝ test comment")
        val token = gen.nextToken()
        assertTokenIsSymbol(gen, token, "foo", gen.engine.initialNamespace.name)
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun parseComments1() {
        val gen = makeGenerator("foo bar ⍝ abc\nqw we")
        gen.nextToken().let { token -> assertTokenIsSymbol(gen, token, "foo") }
        gen.nextToken().let { token -> assertTokenIsSymbol(gen, token, "bar") }
        gen.nextToken().let { token -> assertSame(Newline, token) }
        gen.nextToken().let { token -> assertTokenIsSymbol(gen, token, "qw") }
        gen.nextToken().let { token -> assertTokenIsSymbol(gen, token, "we") }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun parseComments2() {
        val gen = makeGenerator("foo bar ⍝ abc\n⍝ blah\nqw we")
        gen.nextToken().let { token -> assertTokenIsSymbol(gen, token, "foo") }
        gen.nextToken().let { token -> assertTokenIsSymbol(gen, token, "bar") }
        gen.nextToken().let { token -> assertSame(Newline, token) }
        gen.nextToken().let { token -> assertSame(Newline, token) }
        gen.nextToken().let { token -> assertTokenIsSymbol(gen, token, "qw") }
        gen.nextToken().let { token -> assertTokenIsSymbol(gen, token, "we") }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun parseComments3() {
        val gen = makeGenerator("foo bar ⍝ abc\nhello ⍝ blah\nqw we")
        fun nextToken() = gen.nextTokenOrSpace().token
        nextToken().let { token -> assertTokenIsSymbol(gen, token, "foo") }
        nextToken().let { token -> assertSame(Whitespace, token) }
        nextToken().let { token -> assertTokenIsSymbol(gen, token, "bar") }
        nextToken().let { token -> assertSame(Whitespace, token) }
        nextToken().let { token -> assertSame(Comment, token) }
        nextToken().let { token -> assertSame(Newline, token) }
        nextToken().let { token -> assertTokenIsSymbol(gen, token, "hello") }
        nextToken().let { token -> assertSame(Whitespace, token) }
        nextToken().let { token -> assertSame(Comment, token) }
        nextToken().let { token -> assertSame(Newline, token) }
        nextToken().let { token -> assertTokenIsSymbol(gen, token, "qw") }
        nextToken().let { token -> assertSame(Whitespace, token) }
        nextToken().let { token -> assertTokenIsSymbol(gen, token, "we") }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun testStrings() {
        val gen = makeGenerator("\"foo\" \"embedded\\\"quote\"")
        gen.nextToken().let { token ->
            assertTrue(token is StringToken)
            assertEquals("foo", token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is StringToken)
            assertEquals("embedded\"quote", token.value)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun testUnterminatedString() {
        val gen = makeGenerator("\"bar")
        assertFailsWith<ParseException> {
            gen.nextToken()
        }
    }

    @Test
    fun testSymbolsWithNumbers() {
        val gen = makeGenerator("a1 a2 a3b aa2233")
        gen.nextToken().let { token ->
            assertTokenIsSymbol(gen, token, "a1", gen.engine.initialNamespace.name)
        }
        gen.nextToken().let { token ->
            assertTokenIsSymbol(gen, token, "a2", gen.engine.initialNamespace.name)
        }
        gen.nextToken().let { token ->
            assertTokenIsSymbol(gen, token, "a3b", gen.engine.initialNamespace.name)
        }
        gen.nextToken().let { token ->
            assertTokenIsSymbol(gen, token, "aa2233", gen.engine.initialNamespace.name)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun testCharacters() {
        val gen = makeGenerator("@a @b @1 @2")
        assertTokenIsCharacter('a'.code, gen.nextToken())
        assertTokenIsCharacter('b'.code, gen.nextToken())
        assertTokenIsCharacter('1'.code, gen.nextToken())
        assertTokenIsCharacter('2'.code, gen.nextToken())
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun specialCharacters() {
        val gen = makeGenerator("@\\n @\\r @\\\\ @\\e @\\u014E @\\0 @\\s @\\t")
        assertTokenIsCharacter('\n'.code, gen.nextToken())
        assertTokenIsCharacter('\r'.code, gen.nextToken())
        assertTokenIsCharacter('\\'.code, gen.nextToken())
        assertTokenIsCharacter(27, gen.nextToken())
        assertTokenIsCharacter(0x014e, gen.nextToken())
        assertTokenIsCharacter(0, gen.nextToken())
        assertTokenIsCharacter(' '.code, gen.nextToken())
        assertTokenIsCharacter('\t'.code, gen.nextToken())
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun specialCharactersNoUnicodeNames() {
        val gen = makeGenerator("@\\n @\\\\ @\\e @\\u014E")
        assertTokenIsCharacter('\n'.code, gen.nextToken())
        assertTokenIsCharacter('\\'.code, gen.nextToken())
        assertTokenIsCharacter(27, gen.nextToken())
        assertTokenIsCharacter(0x014e, gen.nextToken())
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun unicodeNames() {
        if (backendSupportsUnicodeNames) {
            val gen =
                makeGenerator("@\\SPACE @\\GREATER-THAN_OR_EQUIVALENT_TO @\\MATHEMATICAL_BOLD_SMALL_E @\\SINGLE_LOW-9_QUOTATION_MARK @\\TAI_THAM_SIGN_TONE-2 @\\COMBINING_LOW_LINE")
            assertTokenIsCharacter(' '.code, gen.nextToken())
            assertTokenIsCharacter(0x2273, gen.nextToken())
            assertTokenIsCharacter(0x1D41E, gen.nextToken())
            assertTokenIsCharacter(0x201A, gen.nextToken())
            assertTokenIsCharacter(0x1A76, gen.nextToken())
            assertTokenIsCharacter(0x332, gen.nextToken())
            assertSame(EndOfFile, gen.nextToken())
        }
    }

    @Test
    fun specialCharactersIllegalSyntax0() {
        assertFailsWith<ParseException> {
            makeGenerator("@\\q").nextToken()
        }
    }

    @Test
    fun specialCharactersIllegalSyntax1() {
        assertFailsWith<ParseException> {
            makeGenerator("@\\SOME_TEXT").nextToken()
        }
    }

    @Test
    fun specialCharactersIllegalSyntax2() {
        assertFailsWith<ParseException> {
            makeGenerator("@\\u12ab17").nextToken()
        }
    }

    @Test
    fun specialCharactersIllegalSyntax3() {
        assertFailsWith<ParseException> {
            makeGenerator("@\\u").nextToken()
        }
    }

    @Test
    fun specialCharactersIllegalSyntax4() {
        assertFailsWith<ParseException> {
            makeGenerator("@").nextToken()
        }
    }

    @Test
    fun specialCharactersIllegalSyntax5() {
        assertFailsWith<ParseException> {
            makeGenerator("@\\").nextToken()
        }
    }

    @Test
    fun testSymbolsInStrings() {
        val gen = makeGenerator("\"a\"  \"foo@bar\"  ")
        gen.nextToken().let { token ->
            assertTrue(token is StringToken)
            assertEquals("a", token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is StringToken)
            assertEquals("foo@bar", token.value)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    private fun assertTokenIsCharacter(expected: Int, token: Token) {
        assertTrue(token is ParsedCharacter, "actual type was: ${token}")
        assertEquals(expected, token.value)
    }

    @Test
    fun complexNumbers() {
        val gen = makeGenerator("1j2 0j2 2j0 1J2 0J2 ¯1j2 1j¯2 ¯1j¯2")
        assertComplex(Complex(1.0, 2.0), gen.nextToken())
        assertComplex(Complex(0.0, 2.0), gen.nextToken())
        assertComplex(Complex(2.0, 0.0), gen.nextToken())
        assertComplex(Complex(1.0, 2.0), gen.nextToken())
        assertComplex(Complex(0.0, 2.0), gen.nextToken())
        assertComplex(Complex(-1.0, 2.0), gen.nextToken())
        assertComplex(Complex(1.0, -2.0), gen.nextToken())
        assertComplex(Complex(-1.0, -2.0), gen.nextToken())
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun complexWithDecimals() {
        val gen = makeGenerator("1.2j2.1 ¯1.1j6 2j¯3.1 1e8j9 4j2e3 ¯1.2e20j¯2.1e19 1.1e¯20j3.3e¯21 ¯2e¯4j¯3.1e¯10 ¯2.3e¯10j1e30")
        assertComplex(Complex(1.2, 2.1), gen.nextToken())
        assertComplex(Complex(-1.1, 6.0), gen.nextToken())
        assertComplex(Complex(2.0, -3.1), gen.nextToken())
        assertComplex(Complex(1e8, 9.0), gen.nextToken())
        assertComplex(Complex(4.0, 2e3), gen.nextToken())
        assertComplex(Complex(-1.2e20, -2.1e19), gen.nextToken())
        assertComplex(Complex(1.1e-20, 3.3e-21), gen.nextToken())
        assertComplex(Complex(-2e-4, -3.1e-10), gen.nextToken())
        assertComplex(Complex(-2.3e-10, 1e30), gen.nextToken())
        assertSame(EndOfFile, gen.nextToken())
    }

    private fun assertComplex(expected: Complex, token: Token) {
        assertTrue(token is ParsedComplex)
        assertEquals(expected, token.value)
    }

    @Test
    fun testParserPosition() {
        val gen = makeGenerator("foo bar 10 1.2\nx y")
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "foo", gen.engine.initialNamespace.name)
            assertEquals(0, pos.line)
            assertEquals(0, pos.col)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "bar", gen.engine.initialNamespace.name)
            assertEquals(0, pos.line)
            assertEquals(4, pos.col)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTrue(token is ParsedLong)
            assertEquals(10, token.value)
            assertEquals(0, pos.line)
            assertEquals(8, pos.col)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTrue(token is ParsedDouble)
            val value = token.value
            assertTrue(1.11119 <= value)
            assertTrue(2.00001 >= value)
            assertEquals(0, pos.line)
            assertEquals(11, pos.col)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertSame(Newline, token)
            assertEquals(0, pos.line)
            assertEquals(14, pos.col)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "x", gen.engine.initialNamespace.name)
            assertEquals(1, pos.line)
            assertEquals(0, pos.col)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "y", gen.engine.initialNamespace.name)
            assertEquals(1, pos.line)
            assertEquals(2, pos.col)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun nonBmpStandaloneChars() {
        val gen = makeGenerator("@\uD835\uDC9F @b")
        gen.nextToken().let { token ->
            assertTrue(token is ParsedCharacter)
            assertEquals(0x1d49f, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedCharacter)
            assertEquals('b'.code, token.value)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun continuationCharacter() {
        val gen = makeGenerator(
            """
            |1 2 `
            |3 `
            |4
            """.trimMargin())
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(1, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(2, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(3, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(4, token.value)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun continuationCharacterIllegalPosition() {
        val gen = makeGenerator(
            """
            |1 `4 5
            |6
            """.trimMargin())
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(1, token.value)
        }
        assertFailsWith<ParseException> {
            gen.nextToken()
        }
    }

    @Test
    fun parsePlainSymbol() {
        val res = TokenGenerator.parseStringToSymbol("foo:ab")
        assertNotNull(res)
        assertEquals("foo", res.first)
        assertEquals("ab", res.second)
    }

    @Test
    fun parsePlainSymbolNoNamespace() {
        val res = TokenGenerator.parseStringToSymbol("ab")
        assertNotNull(res)
        assertNull(res.first)
        assertEquals("ab", res.second)
    }

    @Test
    fun parseIllegalNames() {
        assertNull(TokenGenerator.parseStringToSymbol("  foo"))
        assertNull(TokenGenerator.parseStringToSymbol("foo  "))
        assertNull(TokenGenerator.parseStringToSymbol("  foo  "))
        assertNull(TokenGenerator.parseStringToSymbol("  foo:ab"))
        assertNull(TokenGenerator.parseStringToSymbol("foo:ab  "))
        assertNull(TokenGenerator.parseStringToSymbol("  foo:ab  "))
        assertNull(TokenGenerator.parseStringToSymbol("foo abcdef"))
        assertNull(TokenGenerator.parseStringToSymbol("12"))
        assertNull(TokenGenerator.parseStringToSymbol("12foo"))
        assertNull(TokenGenerator.parseStringToSymbol("12:foo"))
        assertNull(TokenGenerator.parseStringToSymbol("foo:99"))
        assertNull(TokenGenerator.parseStringToSymbol("99:77"))
        assertNull(TokenGenerator.parseStringToSymbol("foo:99ab"))
        assertNull(TokenGenerator.parseStringToSymbol("aa,ww"))
        assertNull(TokenGenerator.parseStringToSymbol("aa:"))
        assertNull(TokenGenerator.parseStringToSymbol("aa::"))
        assertNull(TokenGenerator.parseStringToSymbol("1a1"))
    }

    @Test
    fun parseKeyword() {
        val res = TokenGenerator.parseStringToSymbol(":foo")
        assertNotNull(res)
        assertEquals("keyword", res.first)
        assertEquals("foo", res.second)
    }

    @Test
    fun pushBackTokenTest() {
        val gen = makeGenerator("aa abc abcd")
        gen.nextToken().let { token ->
            assertTokenIsSymbol(gen, token, "aa", gen.engine.initialNamespace.name)
        }
        val a = gen.nextTokenWithPosition()
        assertTokenIsSymbol(gen, a.token, "abc", gen.engine.initialNamespace.name)
        assertEquals(0, a.pos.line)
        assertEquals(3, a.pos.col)
        assertEquals(6, a.pos.computedEndCol)
        gen.pushBackToken(a)
        val (b, bPos) = gen.nextTokenWithPosition()
        assertTokenIsSymbol(gen, b, "abc", gen.engine.initialNamespace.name)
        assertEquals(0, bPos.line)
        assertEquals(3, bPos.col)
        assertEquals(6, bPos.computedEndCol)
        gen.nextTokenWithPosition().let { (c, cPos) ->
            assertTokenIsSymbol(gen, c, "abcd", gen.engine.initialNamespace.name)
            assertEquals(0, cPos.line)
            assertEquals(7, cPos.col)
            assertEquals(11, cPos.computedEndCol)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun integerTokenPosition() {
        val gen = makeGenerator("aa abc 1234 abcd")
        gen.nextToken().let { token ->
            assertTokenIsSymbol(gen, token, "aa", gen.engine.initialNamespace.name)
        }
        gen.nextToken().let { token ->
            assertTokenIsSymbol(gen, token, "abc", gen.engine.initialNamespace.name)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTrue(token is ParsedLong)
            assertEquals(1234, token.value)
            assertEquals(0, pos.line)
            assertEquals(7, pos.col)
            assertEquals(11, pos.computedEndCol)
        }
        gen.nextToken().let { token ->
            assertTokenIsSymbol(gen, token, "abcd", gen.engine.initialNamespace.name)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun multilineTokenPosition() {
        val gen = makeGenerator(
            """
            |aa bb
            |cccc dddd
            """.trimMargin())
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "aa", gen.engine.initialNamespace.name)
            assertPosition(0, 0, 0, 2, pos)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "bb", gen.engine.initialNamespace.name)
            assertPosition(0, 3, 0, 5, pos)
        }
        assertSame(Newline, gen.nextToken())
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "cccc", gen.engine.initialNamespace.name)
            assertPosition(1, 0, 1, 4, pos)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "dddd", gen.engine.initialNamespace.name)
            assertPosition(1, 5, 1, 9, pos)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun multilinePositionWithContinuationChars() {
        val gen = makeGenerator(
            """
            |aa bb `
            |cc dd
            """.trimMargin())
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "aa", gen.engine.initialNamespace.name)
            assertPosition(0, 0, 0, 2, pos)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "bb", gen.engine.initialNamespace.name)
            assertPosition(0, 3, 0, 5, pos)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "cc", gen.engine.initialNamespace.name)
            assertPosition(1, 0, 1, 2, pos)
        }
        gen.nextTokenWithPosition().let { (token, pos) ->
            assertTokenIsSymbol(gen, token, "dd", gen.engine.initialNamespace.name)
            assertPosition(1, 3, 1, 5, pos)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun parseBigInt() {
        val gen =
            makeGenerator(
                "1234567891234567891234567890 ¯22222234567891234567891234567890 9223372036854775807 " +
                        "9223372036854775808 ¯9223372036854775808 ¯9223372036854775809")
        gen.nextToken().let { token ->
            assertTrue(token is ParsedBigInt)
            assertEquals(BigInt.of("1234567891234567891234567890"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedBigInt)
            assertEquals(BigInt.of("-22222234567891234567891234567890"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(0x7FFFFFFFFFFFFFFF, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedBigInt)
            assertEquals(BigInt.of("9223372036854775808"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals((-0x7FFFFFFFFFFFFFFF) - 1, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedBigInt)
            assertEquals(BigInt.of("-9223372036854775809"), token.value)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun parsedRationals() {
        val gen = makeGenerator(
            "1r1 0r1 2r3 123456789012345678901234567890r11 " +
                    "¯1r1 ¯0r1 ¯1234r0005 ¯4r2 12345678901234567890123456789000r1000 " +
                    "000010r000300 0000r100")
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(1L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(0L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make(2, 3), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make("123456789012345678901234567890", "11"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(-1L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(0L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make(-1234, 5), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(-2L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedBigInt)
            assertEquals(BigInt.of("12345678901234567890123456789"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make(10, 300), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(0L, token.value)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun parsedRationalDivisionByZero() {
        fun ensureTokenFails(s: String) {
            val gen = makeGenerator(s)
            assertFailsWith<IllegalNumberFormat> {
                gen.nextToken()
            }
        }
        ensureTokenFails("0r0")
        ensureTokenFails("0000r0000")
        ensureTokenFails("1r0")
        ensureTokenFails("10000000r000000")
        ensureTokenFails("¯0r0")
        ensureTokenFails("¯1r0")
        ensureTokenFails("15r0")
        ensureTokenFails("123456789012345678901234567890r0")
        ensureTokenFails("¯138834r0")
        ensureTokenFails("¯123456789012345678901234567890r0")
    }

    @Test
    fun parsedDecimalRationals() {
        val gen = makeGenerator(
            "0r 0.0r 1r 1.0r 1.5r 10.1r 0.3r 12345678901234567890123456789012345678r 12345678901234567890123456789012345678.1234r " +
                    "¯1r ¯1234.123r ¯0r ¯1234567890123456789012345678901234567890.1234r 00001234.1r 000012r 123456.1200000000r " +
                    "000012.00003400000000r 0000.2r 0000.0200r")
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(0L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(0L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(1L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(1L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make(3L, 2L), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make(101L, 10L), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make(3L, 10L), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedBigInt)
            assertEquals(BigInt.of("12345678901234567890123456789012345678"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make("123456789012345678901234567890123456781234", "10000"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(-1L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make("-1234123", "1000"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(0L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make("-12345678901234567890123456789012345678901234", "10000"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make("12341", "10"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedLong)
            assertEquals(12L, token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make("12345612", "100"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make("12000034", "1000000"), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make(2, 10), token.value)
        }
        gen.nextToken().let { token ->
            assertTrue(token is ParsedRational)
            assertEquals(Rational.make(2, 100), token.value)
        }
        assertSame(EndOfFile, gen.nextToken())
    }

    @Test
    fun testPushBack() {
        val gen = makeGenerator("123 45 7890")
        gen.nextTokenOrSpace().let { (token, pos) ->
            assertTrue(token is ParsedLong)
            assertEquals(123L, token.value)
            assertEquals(0, pos.col)
            assertEquals(3, pos.endCol)
        }
        assertEquals(3, gen.currentPos.col)
        gen.nextTokenOrSpace().let { (token, pos) ->
            assertTrue(token is Whitespace)
            assertEquals(3, pos.col)
            assertEquals(4, pos.endCol)
        }
        assertEquals(4, gen.currentPos.col)
        gen.nextTokenOrSpace().let { v ->
            val (token, pos) = v
            assertTrue(token is ParsedLong)
            assertEquals(45L, token.value)
            assertEquals(4, pos.col)
            assertEquals(6, pos.endCol)
            gen.pushBackToken(v)
        }
        assertEquals(4, gen.currentPos.col)
        gen.nextTokenOrSpace().let { v ->
            val (token, pos) = v
            assertTrue(token is ParsedLong)
            assertEquals(45L, token.value)
            assertEquals(4, pos.col)
            assertEquals(6, pos.endCol)
        }
        assertEquals(6, gen.currentPos.col)
        gen.nextTokenOrSpace().let { (token, pos) ->
            assertTrue(token is Whitespace)
            assertEquals(6, pos.col)
            assertEquals(7, pos.endCol)
        }
        gen.nextTokenOrSpace().let { (token, pos) ->
            assertTrue(token is ParsedLong)
            assertEquals(7890L, token.value)
            assertEquals(7, pos.col)
            assertEquals(11, pos.endCol)
        }
        assertEquals(11, gen.currentPos.col)
        gen.nextTokenOrSpace().let { (token, pos) ->
            assertSame(EndOfFile, token)
            assertEquals(11, pos.col)
            assertEquals(11, pos.endCol)
        }
    }

    @Test
    fun importOfSelfShouldNotBeAllowed() {
        val engine = makeEngine()
        val fooNs = engine.makeNamespace("foo")
        assertEquals(1, fooNs.imports.size)
        assertEquals(NamespaceList.CORE_NAMESPACE_NAME, fooNs.imports[0])
        fooNs.addImport(fooNs.name)
        assertEquals(1, fooNs.imports.size)
        assertEquals(NamespaceList.CORE_NAMESPACE_NAME, fooNs.imports[0])
    }

    @Test
    fun exportOfNonexistentSymbolShouldFail() {
        val engine = makeEngine()
        val fooNs = engine.makeNamespace("foo")
        val barNs = engine.makeNamespace("bar")
        assertFailsWith<IllegalArgumentException> {
            fooNs.exportIfInterned(barNs.internSymbol("abc"))
        }
    }

    @Test
    fun nullTokens() {
        val gen = makeGenerator("null 3⦻⦻4")
        assertSame(NilToken, gen.nextToken())
        gen.nextToken().let { token ->
            assertIs<ParsedLong>(token)
            assertEquals(3, token.value)
        }
        assertSame(NilToken, gen.nextToken())
        assertSame(NilToken, gen.nextToken())
        gen.nextToken().let { token ->
            assertIs<ParsedLong>(token)
            assertEquals(4, token.value)
        }
        assertIs<EndOfFile>(gen.nextToken())
    }

    @Test
    fun incompleteCharacter() {
        val gen = makeGenerator("a@")
        gen.nextToken().let { token ->
            assertTokenIsSymbol(gen, token, "a", gen.engine.initialNamespace.name)
        }
        assertFailsWith<ParseException> {
            gen.nextToken()
        }
    }

    @Test
    fun invalidCharacterInHexChar() {
        val gen = makeGenerator("@\\uxyz")
        assertFailsWith<ParseException> {
            gen.nextToken()
        }
    }

    private fun makeGenerator(content: String): TokenGenerator {
        val engine = makeEngine()
        return TokenGenerator(engine, StringSourceLocation(content))
    }

    private fun assertTokenIsSymbol(gen: TokenGenerator, token: Token, name: String, namespaceName: NamespaceName? = null) {
        val ns = if (namespaceName != null) {
            val v = gen.engine.findNamespaceFromName(namespaceName)
            assertNotNull(v)
            v
        } else {
            gen.engine.initialNamespace
        }
        assertTrue(token is Symbol, "Token was: ${token}")
        assertEquals(gen.engine.internSymbol(name, ns), token)
        assertEquals(name, token.symbolName)
        assertEquals(ns.name, token.namespace)
    }

    private fun assertPosition(line: Int, col: Int, endLine: Int?, endCol: Int?, pos: Position) {
        assertEquals(line, pos.line)
        assertEquals(col, pos.col)
        assertEquals(endLine, pos.endLine)
        assertEquals(endCol, pos.endCol)
    }
}
