package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertFalse

class EnvVarTestJvm {
    @Test
    fun testPath() {
        if (!getEnvValueSupported) {
            return
        }
        val path = getEnvValue("PATH")
        assertFalse(path.isNullOrBlank())
    }
}
