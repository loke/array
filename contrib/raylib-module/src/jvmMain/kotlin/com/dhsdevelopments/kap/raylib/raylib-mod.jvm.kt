package com.dhsdevelopments.kap.raylib

import com.dhsdevelopments.kap.ModuleInitFailedException
import com.google.gson.Gson
import java.io.InputStreamReader

actual fun parseDefaultRaylibStructs(): List<RaylibApiRoot> {
    val jsonFiles = listOf("raylib_api.json", "raymath.json", "rlgl.json").map { file ->
        RaylibModule::class.java.getResourceAsStream(file).use { inStream ->
            if (inStream == null) {
                throw ModuleInitFailedException("Can't find ${file} in package")
            }
            Gson().fromJson(InputStreamReader(inStream, Charsets.UTF_8), RaylibApiRoot::class.java)
        }
    }
    return jsonFiles
}
