package com.dhsdevelopments.kap.json

import com.dhsdevelopments.kap.*

actual val backendSupportsJson = true

external object JSON {
    fun parse(input: String): JsAny?
}

actual fun parseJsonToAPL(input: CharacterProvider): APLValue {
    val buf = StringBuilder()
    input.lines().forEach { line ->
        buf.append(line)
        buf.append("\n")
    }
    val json = JSON.parse(buf.toString())
    return parseEntry(json)
}

private fun parseEntry(value: JsAny?): APLValue {
    val res = when (value) {
        null -> APLNilValue
        is JsArray<*> -> parseArray(value.unsafeCast())
        is JsString -> APLString.make(value.toString())
        is JsNumber -> value.toDouble().makeAPLNumber()
        is JsBoolean -> if (value.toBoolean()) APLLONG_1 else APLLONG_0
        else -> parseObject(value)
    }
    return res
}

private fun parseArray(value: JsArray<JsAny>): APLValue {
    val content = ArrayList<APLValue>()
    repeat(value.length) { i ->
        content.add(parseEntry(value[i]))
    }
    return APLArrayList(dimensionsOfSize(content.size), content)
}

private fun jsObjectKeys(value: JsAny): JsArray<JsAny> = js("Object.keys(value)")
private fun jsLookupObjectValue(value: JsAny, key: JsString): JsAny? = js("value[key]")

private fun parseObject(value: JsAny): APLValue {
    val content = ArrayList<Pair<APLValue.APLValueKey, APLValue>>()
    val keyArray = jsObjectKeys(value)
    repeat(keyArray.length) { i ->
        val k = keyArray[i]
        if (k !is JsString) {
            throw JsonParseException("Key is not a string: ${k}")
        }
        content.add(APLString.make(k.toString()).makeKey() to parseEntry(jsLookupObjectValue(value, k)))
    }
    return APLMap(ImmutableMap2.makeFromContent(content))
}
