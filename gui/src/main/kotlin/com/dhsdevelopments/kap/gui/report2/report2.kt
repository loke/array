package com.dhsdevelopments.kap.gui.report2

import com.dhsdevelopments.kap.KapLogger
import com.dhsdevelopments.kap.gui.Client
import com.dhsdevelopments.kap.gui.arrayedit.ArrayEditor
import javafx.collections.ListChangeListener
import javafx.event.EventHandler
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.stage.Stage

class ReportClient {
    lateinit var variableList: ListView<DisplayedValue>
    lateinit var table: ReportSpreadsheetView

    lateinit var zoomSlider: Slider
    lateinit var zoomOutButton: Button
    lateinit var zoomInButton: Button
    lateinit var zoomResetButton: Button

    lateinit var stage: Stage
    lateinit var client: Client

    private fun initClient(client: Client, stage: Stage) {
        this.client = client
        this.stage = stage
        table.initClient(this)
    }

    fun initialize() {
        variableList.selectionModel.selectedIndices.addListener(ListChangeListener {
            val sel = variableList.selectionModel.selectedItems
            if (sel.isNotEmpty()) {
                val item = sel.first()
                val cols = table.columns
                table.selectionModel.apply {
                    clearSelection()
                    selectRange(
                        item.row,
                        cols[item.col],
                        item.row + item.displayedRowCount - 1,
                        cols[item.col + item.displayedColCount - 1])
                }
            }
        })

        variableList.setCellFactory { listView ->
            val cell = ListCell<DisplayedValue>()
            val menu = ContextMenu(
                MenuItem("Open editor").apply {
                    onAction = EventHandler {
                        val v0 = cell.item.value
                        if (v0 != null) {
                            ArrayEditor.open(client, v0, cell.item.name)
                        } else {
                            KapLogger.w { "When opening array editor, v0 was null" }
                        }
                    }
                }
            )
            cell.itemProperty().addListener { _, _, newValue ->
                cell.text = newValue?.name?.nameWithNamespace ?: ""
            }
            cell.emptyProperty().addListener { _, _, isEmpty ->
                if (isEmpty) {
                    cell.contextMenu = null
                } else {
                    cell.contextMenu = menu
                }
            }
            cell
        }

        initZoomControls()
    }

    private fun initZoomControls() {
        zoomSlider.min = 0.25
        zoomSlider.max = 2.0
        zoomSlider.value = 1.0
        zoomSlider.blockIncrement = 0.1
        zoomSlider.valueProperty().addListener { _, _, newValue ->
            table.zoomFactor = newValue.toDouble()
        }

        zoomOutButton.onAction = EventHandler { zoomSlider.decrement() }
        zoomInButton.onAction = EventHandler { zoomSlider.increment() }
        zoomResetButton.onAction = EventHandler { zoomSlider.value = 1.0 }
    }

    companion object {
        fun open(client: Client) {
            val loader = FXMLLoader(ReportClient::class.java.getResource("report2.fxml"))
            val root: Parent = loader.load()
            val controller: ReportClient = loader.getController()
            val stage = Stage()
            controller.initClient(client, stage)

            val scene = Scene(root, 1000.0, 800.0)
            stage.title = "Report"
            stage.scene = scene
            stage.show()
        }
    }
}
