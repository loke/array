package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.jsObject2
import kotlinx.browser.document
import kotlinx.serialization.json.Json
import org.w3c.dom.Worker
import kotlin.js.Promise

@Suppress("ClassName")
@JsModule("@codemirror/autocomplete")
@JsNonModule
external object codemirrorAutocomplete {
    class CompletionContext {
        var state: codemirrorState.EditorState
        var pos: Int
        var explicit: Boolean
        var view: codemirrorView.EditorView?
    }

    interface CompletionResult {
        val from: Int
        val to: Int?
        val options: Array<Completion>
    }

    interface Completion {
        val label: String
        val displayLabel: String?
        val detail: String?
        val type: String?
    }

    fun autocompletion(config: dynamic): dynamic
}

//fun initAutocomplete(worker: Worker) {
//    fun resolver(context: codemirrorAutocomplete.CompletionContext): Promise<codemirrorAutocomplete.CompletionResult?>? {
//        return resolveCompletion(context, worker)
//    }
//
//    val autocompletionConfig = jsObject2("override" to arrayOf(::resolver))
//    val autocompletionExtension = codemirrorAutocomplete.autocompletion(autocompletionConfig)
//    return autocompletionExtension
//}

class CompletionRequestInfo(
    val worker: Worker,
    val content: String,
    val index: Int,
    val resolve: (codemirrorAutocomplete.CompletionResult) -> Unit,
    val reject: (Throwable) -> Unit
) {
    val id = makeRequestIdString()

    fun start() {
        println("requesting completions for: '${content}'/${index}")
        worker.postMessage(Json.encodeToString<Request>(CompletionRequest(id, content, index)))
    }

    fun processResponse(candidates: dynamic, word: String) {
        println("got candidates: ${candidates.length}")
        val startIndex = index - word.length
        val candidateListRes = js("[]")
        repeat(candidates.length) { i ->
            val candidate = candidates[i]
            val text = candidate.text as String
            val description = candidate.description as String
            val e = jsObject2(
                "label" to text,
                "displayLabel" to text,
                "detail" to description,
                "type" to "typename")
            candidateListRes.push(e)
        }

        val res = jsObject2(
            "from" to startIndex,
            "to" to index,
            "options" to candidateListRes)

        resolve(res)
    }

    companion object {
        private var requestId = 0

        private fun makeRequestIdString(): String {
            val newId = requestId++
            return "completion-request-${newId}"
        }
    }
}

private val outstandingCompletionRequests = ArrayDeque<CompletionRequestInfo>()

fun resolveCompletion(context: codemirrorAutocomplete.CompletionContext, worker: Worker): Promise<codemirrorAutocomplete.CompletionResult?>? {
    return Promise<codemirrorAutocomplete.CompletionResult> { resolve, reject ->
        val content = context.state.doc.toString()
        val req = CompletionRequestInfo(worker, content, context.pos, resolve, reject)
        val requestsIsEmpty = outstandingCompletionRequests.isEmpty()
        outstandingCompletionRequests.add(req)
        if (requestsIsEmpty) {
            req.start()
        }

//
//        val res = object : codemirrorAutocomplete.CompletionResult {
//            override val from get() = 0
//            override val to get() = 1
//            override val options: Array<codemirrorAutocomplete.Completion>
//                get() = arrayOf(object : codemirrorAutocomplete.Completion {
//                    override val label: String get() = "foo"
//                    override val displayLabel: String? get() = "blappe"
//                    override val detail: String? get() = "bloppe"
//                    override val type: String? get() = "typename"
//                })
//        }
//        resolve(res)
    }
}

fun processCompletionResult(msg: dynamic) {
    require(outstandingCompletionRequests.isNotEmpty()) { "Got completion result when queue was empty" }
    val req = outstandingCompletionRequests.removeFirst()
    require(req.id == msg.requestId) { "Request id does not match. queue request=${req.id}, incoming=${msg.requestId}" }
    document.asDynamic().foo = msg
    req.processResponse(msg.candidates, msg.word)
    outstandingCompletionRequests.firstOrNull()?.start()
}
