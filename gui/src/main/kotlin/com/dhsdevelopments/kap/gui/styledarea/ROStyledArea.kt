package com.dhsdevelopments.kap.gui.styledarea

import com.dhsdevelopments.kap.KapLogger
import com.dhsdevelopments.kap.gui.Client
import com.dhsdevelopments.kap.gui.DetailsButtonEditorContentEntry
import com.dhsdevelopments.kap.gui.EvalExpressionResult
import com.dhsdevelopments.kap.gui.display.makeKapValueDoc
import com.dhsdevelopments.kap.gui.settings.ConfigProvider
import com.dhsdevelopments.kap.gui.settings.ReturnBehaviour
import com.dhsdevelopments.kap.isEmptyValue
import com.dhsdevelopments.kap.repl.DEFAULT_PROMPT_CHAR
import javafx.application.Platform
import javafx.scene.Node
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCombination
import javafx.scene.text.TextFlow
import org.fxmisc.richtext.model.ReadOnlyStyledDocumentBuilder
import org.fxmisc.richtext.model.StyledDocument
import org.fxmisc.richtext.model.StyledSegment
import org.fxmisc.richtext.model.TextOps
import org.fxmisc.wellbehaved.event.EventPattern
import org.fxmisc.wellbehaved.event.InputMap
import java.util.function.BiConsumer
import java.util.function.Function
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract
import kotlin.math.min

interface CommandListener {
    fun valid(text: String): Boolean
    fun handle(text: String)
}

enum class InputMode {
    DISABLED, REPL_INPUT, PROMPTED_INPUT
}

@OptIn(ExperimentalContracts::class)
class ROStyledArea(
    override val client: Client,
    applyParagraphStyle: BiConsumer<TextFlow, ParStyle>,
    styledTextOps: TextOps<EditorContent, TextStyle>,
    nodeFactory: Function<StyledSegment<EditorContent, TextStyle>, Node>,
    configProvider: ConfigProvider
) : KapEditorStyledArea<ParStyle, EditorContent, TextStyle>(
    ParStyle(),
    applyParagraphStyle,
    TextStyle(client.config),
    styledTextOps,
    nodeFactory, configProvider
) {
    private var updatesEnabled = false
    private var commandListener: CommandListener? = null
    private var promptedInputCommandListener: CommandListener? = null
    private val historyListeners = ArrayList<HistoryListener>()
    private var inputState: InputMode = InputMode.DISABLED
    private val inputEnabled get() = inputState != InputMode.DISABLED

    override val editableText = ROStyledAreaEditableText(this)

    init {
        undoManager = null
        updateInputStateRepl()
        configProvider.addConfigUpdateListener { oldSettings, newSettings ->
            if (oldSettings.fontSizeWithDefault() != newSettings.fontSizeWithDefault()) {
                KapLogger.v { "updating font size" }
                layout()
            }
        }
    }

    fun updateInputStateDisabled() {
        require(inputState != InputMode.DISABLED)
        if (inputState == InputMode.REPL_INPUT) {
            prevText = if (client.config.settings().newlineBehaviour == ReturnBehaviour.PRESERVE) {
                currentInput()
            } else {
                null
            }
        }
        removePrompt()
        inputState = InputMode.DISABLED
    }

    fun updateInputStateRepl() {
        require(inputState == InputMode.DISABLED)
        displayPrompt()
        inputState = InputMode.REPL_INPUT
        val p = prevText
        if (p != null) {
            replaceInputTextAndMoveCursor(p)
            prevText = null
        }
    }

    fun updateInputStatePrompted(prompt: String) {
        require(inputState == InputMode.DISABLED)
        displayPrompt(prompt)
        inputState = InputMode.PROMPTED_INPUT
    }

    override fun addInputMappings(entries: MutableList<InputMap<*>>) {
        entries.add(InputMap.consume(EventPattern.keyPressed(KeyCode.ENTER), { sendCurrentContent() }))
        entries.add(InputMap.consume(EventPattern.keyPressed(KeyCode.ENTER, KeyCombination.CONTROL_DOWN), { insertNewline() }))

        // History navigation
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.UP), { atEditboxStart() }, { prevHistory() }))
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.DOWN), { atEditboxEnd() }, { nextHistory() }))
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.P, KeyCombination.CONTROL_DOWN), { atEditboxStart() }, { prevHistory() }))
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.N, KeyCombination.CONTROL_DOWN), { atEditboxEnd() }, { nextHistory() }))

        // Cursor movement
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.HOME), { atEditbox() }, { moveToBeginningOfInput() }))
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.HOME, KeyCombination.SHIFT_DOWN), { atEditbox() }, { moveToBeginningOfInput() }))

        // Emacs-style cursor movement
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.F, KeyCombination.CONTROL_DOWN), { true }, { caretSelectionBind.moveToNextChar() }))
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.B, KeyCombination.CONTROL_DOWN), { true }, { caretSelectionBind.moveToPrevChar() }))
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.A, KeyCombination.CONTROL_DOWN), { atEditbox() }, { moveToBeginningOfInput() }))
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.E, KeyCombination.CONTROL_DOWN), { atEditbox() }, { moveToEndOfInput() }))
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.K, KeyCombination.CONTROL_DOWN), { atEditbox() }, { deleteToEnd() }))
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.D, KeyCombination.CONTROL_DOWN), { atEditbox() }, { deleteNextChar() }))
    }

    private fun displayPrompt(promptText: String = DEFAULT_PROMPT_CHAR) {
        withUpdateEnabledNoPreserveCursor {
            val inputDocument = ReadOnlyStyledDocumentBuilder(segOps, ParStyle(ParStyle.ParStyleType.NORMAL))
                .addParagraph(
                    listOf(
                        StyledSegment(EditorContent.makeString(promptText), TextStyle(client.config, TextStyle.Type.PROMPT)),
                        StyledSegment(EditorContent.makeString(" "), TextStyle(client.config, TextStyle.Type.PROMPT, promptTag = true))))
                .build()
            insert(document.length(), inputDocument)
        }
    }

    private var prevText: InputText? = null

    fun removePrompt() {
        val pos = findInputStartEnd()
        withUpdateEnabled(preserveCursor = false) {
            deleteText(pos.promptStartPos, pos.inputEnd)
        }
    }

    fun clearLog() {
        val pos = findInputStartEnd()
        withUpdateEnabled {
            deleteText(0, pos.promptStartPos)
        }
    }

    fun setCommandListener(listener: CommandListener) {
        commandListener = listener
    }

    fun setPromptedInputCommandListener(listener: CommandListener) {
        promptedInputCommandListener = listener
    }

    private fun atEditboxStart(): Boolean {
        if (!inputEnabled) return false
        val inputPosition = findInputStartEnd()
        val pos = caretPosition
        return pos >= inputPosition.inputStart && pos <= inputPosition.inputEnd
    }

    private fun atEditboxEnd(): Boolean {
        if (!inputEnabled) return false
        val inputPosition = findInputStartEnd()
        val pos = caretPosition
        return pos >= inputPosition.inputStart && pos <= inputPosition.inputEnd
    }

    override fun atEditbox(): Boolean {
        return inputEnabled
    }

    private fun isAtInput(start: Int, end: Int): Boolean {
        return if (start == end && document.getStyleAtPosition(start).promptTag) {
            true
        } else {
            val spans = document.getStyleSpans(start, end)
            val firstNonInputSpan = spans.find { span ->
                span.style.type != TextStyle.Type.INPUT
            }
            firstNonInputSpan == null
        }
    }

    fun addHistoryListener(historyListener: HistoryListener) {
        historyListeners.add(historyListener)
    }

    private fun insertNewline() {
        insertText(caretPosition, "\n")
    }

    private fun prevHistory() {
        if (inputState == InputMode.REPL_INPUT) {
            historyListeners.forEach { it.prevHistory() }
        }
    }

    private fun nextHistory() {
        if (inputState == InputMode.REPL_INPUT) {
            historyListeners.forEach { it.nextHistory() }
        }
    }

    fun findInputStartEnd(): InputPositions {
        require(inputEnabled) { "Input is not enabled" }
        var pos = document.length() - 1
        while (pos >= 0) {
            val style = document.getStyleOfChar(pos)
            if (style.promptTag) {
                break
            }
            pos--
        }
        require(pos >= 0)

        val inputStartPos = pos + 1
        while (pos >= 0) {
            val style = document.getStyleOfChar(pos)
            if (style.type != TextStyle.Type.PROMPT) {
                break
            }
            pos--
        }

        val promptStartPos = pos + 1
        return InputPositions(promptStartPos, inputStartPos, document.length())
    }

    private fun findTextInsertionPoint(): Int {
        return if (inputEnabled) {
            findInputStartEnd().promptStartPos
        } else {
            document.length()
        }
    }

    private fun sendCurrentContent() {
        val inputPosition = findInputStartEnd()
        val text = document.subSequence(inputPosition.inputStart, inputPosition.inputEnd).text
        when (inputState) {
            InputMode.REPL_INPUT -> commandListener?.handle(text)
            InputMode.PROMPTED_INPUT -> promptedInputCommandListener?.handle(text)
            InputMode.DISABLED -> error("Attempt to command when input is disabled")
        }
    }

    fun currentInput(): InputText {
        val inputPosition = findInputStartEnd()
        val text = document.subSequence(inputPosition.inputStart, inputPosition.inputEnd).text

        val sel = caretSelectionBind.underlyingSelection
        val selStart = sel.startPosition
        val selEnd = sel.endPosition
        val selectionStartOffset = selStart - inputPosition.inputStart
        val selectionEndOffset = selEnd - inputPosition.inputStart

        return InputText(text, selectionStartOffset, selectionEndOffset)
    }

    fun <T> withUpdateEnabled(preserveCursor: Boolean = true, fn: () -> T): T {
        contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
        val oldInputText = if (preserveCursor && inputEnabled) currentInput() else null
        val oldEnabled = updatesEnabled
        updatesEnabled = true
        try {
            return fn()
        } finally {
            updatesEnabled = oldEnabled
            if (oldInputText != null) {
                val inputPosition = findInputStartEnd()
                caretSelectionBind.moveTo(min(inputPosition.inputStart + oldInputText.selectionStartOffset, inputPosition.inputEnd))
            }
        }
    }

    fun <T> withUpdateEnabledNoPreserveCursor(fn: () -> T): T {
        return withUpdateEnabled(preserveCursor = false, fn)
    }

    fun appendTextEnd(
        text: String,
        style: TextStyle,
        parStyle: ParStyle? = null
    ): StyledDocument<ParStyle, EditorContent, TextStyle> {
        val builder = ReadOnlyStyledDocumentBuilder(segOps, parStyle ?: ParStyle())
        text.split("\n").forEach { part ->
            builder.addParagraph(EditorContent.makeString(part), style)
        }
        val inputPos = findTextInsertionPoint()
        val doc = builder.build()
        withUpdateEnabled {
            insert(inputPos, doc)
        }
        showBottomParagraphAtTop()
        return doc
    }

    fun appendExpressionResultEnd(
        value: EvalExpressionResult,
        parStyle: ParStyle = ParStyle(),
        clearableStyleContent: TextStyle.ClearableStyleContent
    ) {
        withUpdateEnabled {
            if (!isEmptyValue(value.formattedEvalResult.result)) {
                val newDoc = makeKapValueDoc(client.config, segOps, value, parStyle, clearableStyleContent)
                insert(findTextInsertionPoint(), newDoc)
            }
            if (client.config.settings().displayTiming) {
                val timingDoc = ReadOnlyStyledDocumentBuilder(segOps, parStyle)
                timingDoc.addParagraph(
                    listOf(
                        StyledSegment(
                            EditorContent.makeString("${value.formattedEvalResult.evaluationTime}\n"),
                            TextStyle(client.config, TextStyle.Type.TIMING))),
                    ParStyle(ParStyle.ParStyleType.TIMING))
                insert(findTextInsertionPoint(), timingDoc.build())
            }
        }
        Platform.runLater {
            showBottomParagraphAtTop()
        }
    }

    fun appendErrorMessage(text: String, details: String? = null) {
        withUpdateEnabled {
            val inputPos = findTextInsertionPoint()
            val content = mutableListOf<StyledSegment<EditorContent, TextStyle>>(
                StyledSegment(
                    EditorContent.makeString(text),
                    TextStyle(client.config, TextStyle.Type.ERROR)))
            if (details != null) {
                content.add(StyledSegment(EditorContent.makeString(" "), TextStyle(client.config, TextStyle.Type.ERROR)))
                content.add(StyledSegment(DetailsButtonEditorContentEntry(client, details), TextStyle(client.config, TextStyle.Type.DETAILS_BUTTON)))
            }
            val newDoc = ReadOnlyStyledDocumentBuilder(segOps, ParStyle())
                .addParagraph(content)
                .addParagraph(EditorContent.makeBlank(), TextStyle(client.config, TextStyle.Type.ERROR))
                .build()
            insert(inputPos, newDoc)
        }
        showBottomParagraphAtTop()
    }

    fun appendOutputEnd(text: String) {
        withUpdateEnabled {
            val textStyle = TextStyle(client.config, TextStyle.Type.OUTPUT)
            val builder = ReadOnlyStyledDocumentBuilder(segOps, ParStyle(ParStyle.ParStyleType.OUTPUT))
            text.split("\n").forEach { part -> builder.addParagraph(EditorContent.makeString(part), textStyle) }

            val p = findTextInsertionPoint()
            // Input position at the beginning of the buffer
            val newPos = if (p == 0) {
                builder.addParagraph(EditorContent.makeBlank(), textStyle)
                p
            } else {
                val style = document.getParagraphStyleAtPosition(p - 1)
                if (style.type == ParStyle.ParStyleType.OUTPUT) {
                    p - 1
                } else {
                    builder.addParagraph(EditorContent.makeBlank(), textStyle)
                    p
                }
            }
            insert(newPos, builder.build())
        }
        showBottomParagraphAtTop()
    }

    fun insertInitialText(s: String) {
        withUpdateEnabled {
            val textStyle = TextStyle(client.config, TextStyle.Type.DEFAULT)
            val builder = ReadOnlyStyledDocumentBuilder(segOps, ParStyle(ParStyle.ParStyleType.NORMAL))
            s.split("\n").forEach { part ->
                builder.addParagraph(EditorContent.makeString(part), textStyle)
            }
            repeat(2) {
                builder.addParagraph(EditorContent.makeBlank(), textStyle)
            }
            insert(0, builder.build())
        }
    }

    override fun replace(start: Int, end: Int, replacement: StyledDocument<ParStyle, EditorContent, TextStyle>) {
        when {
            updatesEnabled -> super.replace(start, end, replacement)
            isAtInput(start, end) -> super.replace(start, end, makeInputStyle(replacement.text))
            inputEnabled && replacement.length() > 0 -> {
                // If we get here, the user tried to insert text outside the editable area.
                // When this happens, we simply insert the text at the end instead.
                val p = findInputStartEnd().inputEnd
                super.replace(p, p, makeInputStyle(replacement.text))
            }
        }
    }

    private fun makeInputStyle(s: String): StyledDocument<ParStyle, EditorContent, TextStyle> {
        return ReadOnlyStyledDocumentBuilder(segOps, ParStyle())
            .addParagraph(EditorContent.makeString(s), TextStyle(client.config, type = TextStyle.Type.INPUT))
            .build()
    }

    private fun replaceInputTextInternal(s: String) {
        val inputPos = findInputStartEnd()
        withUpdateEnabledNoPreserveCursor {
            deleteText(inputPos.inputStart, inputPos.inputEnd)
            replace(inputPos.inputStart, inputPos.inputStart, makeInputStyle(s))
        }
    }

    fun replaceInputText(s: String) {
        replaceInputTextInternal(s)
        moveToEndOfInput()
    }

    fun replaceInputTextAndMoveCursor(p: InputText) {
        replaceInputTextInternal(p.text)
        val pos = findInputStartEnd()
        caretSelectionBind.selectRange(p.selectionStartOffset + pos.inputStart, p.selectionEndOffset + pos.inputStart)
    }

    private fun moveToBeginningOfInput() {
        val inputPosition = findInputStartEnd()
        caretSelectionBind.moveTo(inputPosition.inputStart)
    }

    private fun moveToEndOfInput() {
        val inputPosition = findInputStartEnd()
        caretSelectionBind.moveTo(inputPosition.inputEnd)
    }

    private fun deleteToEnd() {
        val inputPosition = findInputStartEnd()
        withUpdateEnabled {
            deleteText(caretSelectionBind.position, inputPosition.inputEnd)
        }
    }

    class InputPositions(val promptStartPos: Int, val inputStart: Int, val inputEnd: Int) {
        override fun toString() = "InputPositions(promptStartPos=$promptStartPos, inputStart=$inputStart, inputEnd=$inputEnd)"
    }

    class InputText(val text: String, val selectionStartOffset: Int, val selectionEndOffset: Int) {
        override fun toString() = "InputText(text='$text', selectionStartOffset=$selectionStartOffset, selectionEndOffset=$selectionEndOffset)"
    }
}
