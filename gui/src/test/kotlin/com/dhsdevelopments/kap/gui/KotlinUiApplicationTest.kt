package com.dhsdevelopments.kap.gui

import javafx.application.Application
import javafx.stage.Stage
import org.testfx.api.FxRobot
import org.testfx.api.FxToolkit
import org.testfx.framework.junit.ApplicationAdapter
import org.testfx.framework.junit.ApplicationFixture
import kotlin.test.AfterTest
import kotlin.test.BeforeTest

abstract class KotlinUiApplicationTest : FxRobot(), ApplicationFixture {
    @BeforeTest
    fun internalBefore() {
        FxToolkit.registerPrimaryStage()
        FxToolkit.setupApplication { ApplicationAdapter(this) }
    }

    @AfterTest
    @Throws(Exception::class)
    fun internalAfter() {
        FxToolkit.cleanupAfterTest(this, ApplicationAdapter(this))
    }

    @Throws(Exception::class)
    override fun init() {
    }

    @Throws(Exception::class)
    override fun start(stage: Stage) {
    }

    @Throws(Exception::class)
    override fun stop() {
    }

    companion object {
        @JvmStatic
        fun launch(appClass: Class<out Application>, vararg appArgs: String): Application {
            FxToolkit.registerPrimaryStage()
            return FxToolkit.setupApplication(appClass, *appArgs)
        }
    }
}
