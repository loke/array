package com.dhsdevelopments.kap.gui.syms

import com.dhsdevelopments.kap.asCodepointList
import com.dhsdevelopments.kap.charToString
import com.dhsdevelopments.kap.gui.Client
import com.dhsdevelopments.kap.gui.styledarea.KapEditorStyledArea
import com.dhsdevelopments.kap.keyboard.SYMBOL_BAR_LAYOUT
import com.dhsdevelopments.kap.keyboard.SYMBOL_DOC_LIST
import com.dhsdevelopments.kap.keyboard.SymbolDoc
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane
import javafx.scene.layout.Pane
import javafx.stage.Popup
import java.util.*

class LanguageBar(val client: Client) : Pane() {
    private val flowPane = FlowPane().apply {
        styleClass.add("langbar")
        BorderPane.setAlignment(this, Pos.CENTER)
    }

    private val functionHelpContent = FunctionHelp(client.config)
    private val functionHelpPopup = Popup().apply { content.add(functionHelpContent) }
    private var timerTask: TimerTask? = null

    init {
        stylesheets.add("/com/dhsdevelopments/kap/gui/interactor.css")
        updateButtons()
        children.add(flowPane)
        widthProperty().addListener { _, _, newValue ->
            flowPane.prefWrapLength = newValue.toDouble()
        }
    }

    private fun hideFunctionHelpPopup() {
        if (functionHelpPopup.isShowing) {
            functionHelpPopup.hide()
        }
        val task = timerTask
        if (task != null) {
            task.cancel()
            timerTask = null
        }
    }

    private fun updateButtons() {
        SYMBOL_BAR_LAYOUT.asCodepointList().forEach { sym ->
            if (sym == ' '.code) {
                flowPane.children.add(Pane().apply { prefWidth = 25.0 })
            } else {
                val s = charToString(sym)
                val clickableLabel = Label(s)
                clickableLabel.styleClass.add("langbar-char")
                clickableLabel.padding = Insets(5.0, 2.0, 5.0, 2.0)
                clickableLabel.onMouseClicked = EventHandler {
                    val focused = scene.focusOwner
                    if (focused is KapEditorStyledArea<*, *, *>) {
                        focused.replaceSelectionAndDisplay(s)
                    }
                }
                clickableLabel.onMouseEntered = EventHandler { event ->
                    SYMBOL_DOC_LIST[s]?.let { doc ->
                        hideFunctionHelpPopup()
                        val x = event.screenX + 10.0
                        val y = event.screenY + 10.0
                        val task = DelayPopupTask(s, doc, x, y)
                        timerTask = task
                        client.timer.schedule(task, 500L)
                    }
                }
                clickableLabel.onMouseExited = EventHandler { hideFunctionHelpPopup() }
                flowPane.children.add(clickableLabel)
            }
        }
    }

    inner class DelayPopupTask(private val s: String, private val doc: SymbolDoc, private val xPos: Double, private val yPos: Double) : TimerTask() {
        override fun run() {
            Platform.runLater {
                if (timerTask === this) {
                    functionHelpContent.updateDoc(s, doc)
                    functionHelpPopup.show(this@LanguageBar, xPos, yPos)
                    timerTask = null
                }
            }
        }
    }
}
