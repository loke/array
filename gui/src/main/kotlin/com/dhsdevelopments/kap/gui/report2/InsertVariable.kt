package com.dhsdevelopments.kap.gui.report2

import com.dhsdevelopments.kap.Namespace
import com.dhsdevelopments.kap.StackStorageRef
import com.dhsdevelopments.kap.currentStorageStack
import com.dhsdevelopments.kap.gui.arrayedit.ArrayEditor
import com.dhsdevelopments.kap.gui.displayErrorWithStage
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.event.EventHandler
import javafx.fxml.FXMLLoader
import javafx.geometry.Insets
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.ChoiceBox
import javafx.scene.control.TextField
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Pane
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.util.StringConverter
import org.controlsfx.control.textfield.TextFields

class InsertVariable {
    lateinit var root: Pane
    lateinit var displaySettingsWrapper: BorderPane
    lateinit var displayTypeChoiceBox: ChoiceBox<DisplayType>
    lateinit var stage: Stage
    lateinit var selection: ArrayEditor.SelectedArea

    lateinit var nameField: TextField
    lateinit var ok: Button
    lateinit var cancel: Button

    lateinit var client: ReportClient

    private var currentConfigurator: DisplayConfigurator? = null

    fun initClient(client: ReportClient, stage: Stage, selection: ArrayEditor.SelectedArea) {
        this.client = client
        this.stage = stage
        this.selection = selection

        initButtonListeners()

        TextFields.bindAutoCompletion(nameField) { request ->
            matchSymbolsAndNamespaces(request.userText)
        }

        val typeList = FXCollections.observableArrayList(
            ArrayDisplayType,
            LineChartDisplayType,
            SegmentedBarDisplayType)
        displayTypeChoiceBox.items = typeList
        displayTypeChoiceBox.converter = DisplayTypeConverter
        displayTypeChoiceBox.selectionModel.select(ArrayDisplayType)
        displayTypeChoiceBox.selectionModel.selectedIndexProperty().addListener { observable, oldValue, newValue ->
            displayTypeChoiceBox.selectionModel.selectedItem?.let { type -> updateSettingsForType(typeList[newValue.toInt()]) }
        }
        updateSettingsForType(ArrayDisplayType)
    }

    private fun initButtonListeners() {
        ok.onAction = EventHandler {
            val name = nameField.text.trim()
            if (name.isNotEmpty()) {
                client.table.reportClient.client.calculationQueue.pushJobToQueue { engine ->
                    val sym = engine.currentNamespace.findSymbol(name, includePrivate = true)
                    if (sym == null) {
                        Platform.runLater {
                            displayErrorWithStage(stage, "Error", "Symbol not defined: ${name}")
                        }
                    } else {
                        val configurator = currentConfigurator
                        require(configurator != null) { "No configurator assigned. This is an internal error." }
                        engine.rootEnvironment.findBinding(sym)?.let { binding ->
                            engine.withThreadLocalAssigned {
                                val storage = currentStorageStack().findStorage(StackStorageRef(binding))
                                val value = storage.value()
                                if (value == null) {
                                    Platform.runLater {
                                        displayErrorWithStage(stage, "Error", "Variable is not assigned: ${name}")
                                    }
                                } else {
                                    val collapsed = value.collapse()
                                    configurator.saveToResult(engine, storage, collapsed, sym)
                                }
                            }
                        }
                    }
                }
            }
            stage.close()
        }

        cancel.onAction = EventHandler {
            stage.close()
        }
    }

    private fun updateSettingsForType(type: DisplayType) {
        val typeConfigurator = type.makeConfigurator(client, selection)
        currentConfigurator = typeConfigurator
        displaySettingsWrapper.center = typeConfigurator.settingsPanel().also { panel ->
            BorderPane.setMargin(panel, Insets(5.0, 5.0, 5.0, 5.0))
        }
        stage.sizeToScene()
    }

    private fun matchSymbolsAndNamespaces(s: String): List<String> {
        val engine = client.client.engine
        val indexOfColon = s.indexOf(':')
        if (indexOfColon == -1) {
            return matchSymbolsInNamespace(engine.currentNamespace, s, false)
        } else {
            val namespaceName = s.substring(0 until indexOfColon)
            val ns = if (namespaceName.isBlank()) engine.keywordNamespace else engine.findNamespace(namespaceName)
            if (ns == null) {
                return emptyList()
            }
            return matchSymbolsInNamespace(ns, s.substring(indexOfColon + 1), true)
        }
    }

    private fun matchSymbolsInNamespace(ns: Namespace, s: String, includeNsName: Boolean): List<String> {
        val sLower = s.lowercase()
        return ns
            .allSymbols()
            .filter { sym -> sym.symbolName.lowercase().startsWith(sLower) }
            .sortedBy { v -> v.symbolName.lowercase() }
            .map { sym ->
                if (includeNsName) {
                    sym.nameWithNamespace
                } else {
                    sym.symbolName
                }
            }
    }

    companion object {
        fun open(client: ReportClient) {
            val selection = ArrayEditor.SelectedArea.computeSelectedArea(client.table.selectionModel.selectedCells)
            if (selection == null || selection.width == 0 || selection.height == 0) {
                return
            }

            val stage = Stage(StageStyle.UTILITY).apply {
                initOwner(client.stage)
                initModality(Modality.WINDOW_MODAL)
                title = "Insert variable"
            }

            val loader = FXMLLoader(InsertVariable::class.java.getResource("insert-variable.fxml"))
            val root = loader.load<Parent>()
            val controller = loader.getController<InsertVariable>()
            stage.scene = Scene(root)
            controller.initClient(client, stage, selection)

            stage.show()
            stage.requestFocus()
            controller.nameField.requestFocus()
        }
    }

    object DisplayTypeConverter : StringConverter<DisplayType>() {
        override fun toString(obj: DisplayType): String {
            return obj.description
        }

        override fun fromString(string: String?): DisplayType {
            TODO("not implemented")
        }
    }
}
