# Kap

An implementation of an APL-like language in Kotlin. The implementation is a Kotlin Multiplatform project, and can be
compiled to Java, natively and to Javascript.

## Summary

Kap is an array-based language that is inspired by APL. The language provides a powerful set of tools that allows the
programmer to work with arrays of data instead of element by element. This allows the code to be incredibly compact, and
often reduces what would otherwise be tens or even hundreds of lines of code into single line.

Features that sets Kap apart from APL include:

- Local variable scopes including closures
- First class functions
- Custom syntax extensions (this allows features like `if` and `while` to be implemented as part of the standard library
  rather than being part of the language syntax)
- Lazy evaluation
- Parallel evaluation
- Structural under (inspired by BQN)
- Bigint and rational arithmetic
- Simple sound and graphic API
- Line/bar/pie charts

The main website for the project is: https://kapdemo.dhsdevelopments.com/

## What the language looks like

As a language based on APL, it uses non-ASCII symbols with most fundamental functions being a single character. This may
make the language seem complicated, but it's actually rather simple once one learns the general syntax.

The first line of code in the following example assigns a string to the variable A, and the second line splits the
string at instances of `-`, returning an array of arrays (a string in Kap is nothing more than a one-dimensional array
of characters):

```
A ← "this-is-a-demo"
A ⊂⍨ A≠@-
```

Typing the above code into the REPL will display the following result:

```
┌→─────────────────────┐
│"this" "is" "a" "demo"│
└──────────────────────┘
```

The box around the result indicates that the result is an array. In this case it is a one-dimensional array with 4
elements, each of which is an array of characters (i.e. string).

More examples can be found on the [Kap examples](https://kapdemo.dhsdevelopments.com/examples.html) web page.

## Build dependencies

The Java version of Kap has been tested on Linux, Macos and Windows. The only dependency is Java 21.

## Running the JavaFX based UI

The most feature-complete way to run Kap is to use the JVM client. It provides a nicer user interface, an editor,
highlighting of the location of errors, ability to draw graphics, and many other features.

To start the client, simply run:

```
./gradlew gui:run
```          

## Running the Linux native version

To build the native Linux version, several dependencies needs to be installed. The development packages for the
following libraries needs to be available:

- Libedit
- Libcurl
- ICU
- Jansson
- GMP
- Ncurses
- Alsa
- Libffi
- OpenSSL

### Fedora Linux packages

On Fedora, the necessary packages can be installed using:

```
dnf install java-21-openjdk-devel jansson-devel libicu-devel libffi-devel alsa-lib-devel ncurses-devel libedit-devel gmp-devel libicu-devel libxcrypt-compat libcurl-devel openssl-devel
```

### Ubuntu packages

On Ubuntu, use the following command to install the dependencies:

```
apt-get install openjdk-21-jdk libicu-dev libcurl4-openssl-dev libjansson-dev libgmp-dev libedit-dev libasound2-dev libffi-dev libssl-dev
```

### Building the Linux version

To build the native Linux binary, run the following command:

```
./gradlew text-client:linuxMainBinaries
``` 

When running the Linux binary, the path to the standard library needs to be passed to the executable:

```
text-client/build/bin/linux/releaseExecutable/text-client.kexe --lib-path=array/standard-lib
```

There is currently no support for native compilation on Windows or OSX. As long as the dependencies are available, it
should be possible to make it work. Help appreciated if anyone is interested in working on it.

## Web client

The application also compiles to Javascript, and it's possible to run it in a browser. Note that some functionality is
missing from the Javascript version. For example, network and file operations are currently not implemented.

You can try the web UI here: https://kapdemo.dhsdevelopments.com/clientweb2/

## Documentation

Work on the documentation is in progress, but it's still limited.

There is a tutorial which is incomplete: https://kapdemo.dhsdevelopments.com/tutorial.html

The following document explains the differences between Kap and APL, which is useful for readers who already are
familiar with APL: https://kapdemo.dhsdevelopments.com/kap-comparison.html

The reference documentation can be found here: https://kapdemo.dhsdevelopments.com/reference.html

Anyone interested in learning more can ask questions on the Matrix channel.

## Contributions

The main repository for this project is available from Codeberg: https://codeberg.org/loke/array

For discussions about this project, feel free to join
the [Matrix channel](https://matrix.to/#/#kap:dhsdevelopments.com):
`#kap:dhsdevelopments.com`.
