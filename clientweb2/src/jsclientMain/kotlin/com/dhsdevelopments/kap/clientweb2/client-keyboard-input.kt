package com.dhsdevelopments.kap.clientweb2

import com.dhsdevelopments.kap.JsTransferQueue
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.HTMLSpanElement

fun processReadLine(msg: dynamic) {
    val queue = jsTransferQueue
    require(queue != null) { "jsTransferQueue is null" }

    queue.waitAndUpdateStateValue(0, JsTransferQueue.STATE_NONE, JsTransferQueue.STATE_READ_LINE_RESULT) {
        val prompt = msg.prompt as String
        displayInputElement(prompt) { res ->
            queue.sendArray(res.encodeToByteArray()) {
                queue.updateState(JsTransferQueue.STATE_NONE)
            }
        }
    }
}

private var currentInputCallback: ((String) -> Unit)? = null

fun initResponseInputField() {
    val responseInputField = findElement<HTMLInputElement>("response-input-field")
    responseInputField.onkeypress = { event ->
        if (event.key == "Enter") {
            sendResponseInputResult()
        }
    }
}

fun sendResponseInputResult() {
    val cb = currentInputCallback
    require(cb != null) { "Attempting to send response without an installed callback" }
    val field = findElement<HTMLInputElement>("response-input-field")
    val text = field.value
    cb(text)
    displayCodeEditorElement()
}

private fun displayInputElement(prompt: String, fn: (String) -> Unit) {
    require(currentInputCallback == null) { "Attempt to display input element while waiting for input" }
    findElement<HTMLElement>("response-input-wrapper").hidden = false
    findElement<HTMLSpanElement>("response-input-prompt").innerText = prompt
    findElement<HTMLElement>("code-editor-wrapper").hidden = true
    findElement<HTMLInputElement>("response-input-field").let { field ->
        field.value = ""
        field.focus()
    }
    currentInputCallback = fn
}

private fun displayCodeEditorElement() {
    findElement<HTMLElement>("response-input-wrapper").hidden = true
    findElement<HTMLSpanElement>("response-input-prompt").innerText = ""
    findElement<HTMLElement>("code-editor-wrapper").hidden = false
    editorGlobal?.focus()
    currentInputCallback = null
}
