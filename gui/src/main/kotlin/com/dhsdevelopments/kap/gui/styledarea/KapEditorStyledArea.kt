package com.dhsdevelopments.kap.gui.styledarea

import com.dhsdevelopments.kap.completions.SymbolCompletionCandidate
import com.dhsdevelopments.kap.completions.symbolCompletionCandidates
import com.dhsdevelopments.kap.edit.EditableText
import com.dhsdevelopments.kap.edit.EditorAction
import com.dhsdevelopments.kap.edit.EditorEngineConnector
import com.dhsdevelopments.kap.edit.WrapWithParen
import com.dhsdevelopments.kap.gui.Client
import com.dhsdevelopments.kap.gui.settings.ConfigProvider
import com.dhsdevelopments.kap.keyboard.ExtendedCharsKeyboardInput
import javafx.application.Platform
import javafx.event.Event
import javafx.geometry.Point2D
import javafx.scene.Node
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCombination
import javafx.scene.input.KeyEvent
import javafx.scene.text.TextFlow
import org.fxmisc.richtext.GenericStyledArea
import org.fxmisc.richtext.model.GenericEditableStyledDocument
import org.fxmisc.richtext.model.StyledSegment
import org.fxmisc.richtext.model.TextOps
import org.fxmisc.wellbehaved.event.EventPattern
import org.fxmisc.wellbehaved.event.InputHandler
import org.fxmisc.wellbehaved.event.InputMap
import org.fxmisc.wellbehaved.event.Nodes
import org.reactfx.util.FxTimer
import java.time.Duration
import java.util.function.BiConsumer
import java.util.function.Function

open class KapEditorStyledArea<PS, SEG, S>(
    parStyle: PS,
    applyParagraphStyle: BiConsumer<TextFlow, PS>,
    textStyle: S,
    segmentOps: TextOps<SEG, S>,
    nodeFactory: Function<StyledSegment<SEG, S>, Node>,
    configProvider: ConfigProvider
) : GenericStyledArea<PS, SEG, S>(
    parStyle,
    applyParagraphStyle,
    textStyle,
    GenericEditableStyledDocument(parStyle, textStyle, segmentOps),
    segmentOps,
    false,
    nodeFactory
) {
    private var defaultKeymap: InputMap<*> = Nodes.getInputMap(this)
    private var prefixActive = false
    private val extendedInput = ExtendedCharsKeyboardInput()
    private var currentInputMap: InputMap<out Event>? = null

    init {
        stylesheets.add("/com/dhsdevelopments/kap/gui/interactor.css")
        styleClass.addAll("editcontent", "kapfont")
        updateKeymap(configProvider.settings().keyPrefix)
        configProvider.addConfigUpdateListener { oldConfig, newConfig ->
            if (oldConfig.keyPrefix != newConfig.keyPrefix) {
                updateKeymap(newConfig.keyPrefix)
            }
        }
    }

    open val client: Client? = null
    open fun atEditbox(): Boolean = true
    open val editableText: EditableText = GuiEditableText(this)

    fun updateKeymap(keyPrefix: String) {
        val entries = ArrayList<InputMap<out Event>>()

        // Keymap
        extendedInput.keymap.forEach { e ->
            val modifiers = if (e.key.shift) arrayOf(KeyCombination.ALT_DOWN, KeyCombination.SHIFT_DOWN) else arrayOf(KeyCombination.ALT_DOWN)
            val v = InputMap.consume(EventPattern.keyTyped(e.key.character, *modifiers), { replaceSelectionAndDisplay(e.value) })
            entries.add(v)
        }
        entries.add(InputMap.consume(EventPattern.keyTyped(" ", KeyCombination.ALT_DOWN), { replaceSelectionAndDisplay(" ") }))
        addInputMappings(entries)

        // Prefix input
        entries.add(makePrefixInputKeymap(keyPrefix))

        // Kap-specific actions
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.Q, KeyCombination.CONTROL_DOWN), { atEditbox() }, { wrapWithParen() }))
        entries.add(InputMap.consumeWhen(EventPattern.keyPressed(KeyCode.SPACE, KeyCombination.CONTROL_DOWN), { atEditbox() }, { completeSymbol() }))

        entries.add(defaultKeymap)
        if (currentInputMap != null) {
            Nodes.removeInputMap(this, currentInputMap)
        }
        currentInputMap = InputMap.sequence(*entries.toTypedArray())
        Nodes.pushInputMap(this, currentInputMap)
    }

    open fun addInputMappings(entries: MutableList<InputMap<out Event>>) {}

    private fun makePrefixInputKeymap(prefixChar: String): InputMap<out Event> {
        fun disableAndAdd(s: String) {
            prefixActive = false
            replaceSelectionAndDisplay(s)
        }

        fun processKey(event: KeyEvent) {
            val charMapping = extendedInput.keymap[ExtendedCharsKeyboardInput.KeyDescriptor(
                event.character,
                event.isShiftDown)]
            if (charMapping == null) {
                disableAndAdd(prefixChar)
            } else {
                disableAndAdd(charMapping)
            }
        }

        fun emptyKeyModifiers(event: KeyEvent): Boolean {
            return !(event.isAltDown || event.isShiftDown || event.isControlDown || event.isMetaDown)
        }

        val entries = ArrayList<InputMap<out Event>>()
        entries.add(InputMap.consume(EventPattern.keyTyped(prefixChar)) { event ->
            if (!prefixActive) {
                prefixActive = true
            } else {
                processKey(event)
            }
        })
        entries.add(InputMap.process(EventPattern.keyTyped()) { event ->
            when {
                !prefixActive -> {
                    InputHandler.Result.PROCEED
                }
                event.character == " " && emptyKeyModifiers(event) -> {
                    prefixActive = false
                    replaceSelectionAndDisplay(prefixChar)
                    InputHandler.Result.CONSUME
                }
                else -> {
                    prefixActive = false
                    val charMapping = extendedInput.keymap[ExtendedCharsKeyboardInput.KeyDescriptor(
                        event.character,
                        event.isShiftDown)]
                    if (charMapping == null) {
                        InputHandler.Result.PROCEED
                    } else {
                        replaceSelectionAndDisplay(charMapping)
                        InputHandler.Result.CONSUME
                    }
                }
            }
        })
        return InputMap.sequence(*entries.toTypedArray())
    }

    private var outstandingScroll = 0

    fun showBottomParagraphAtTop() {
        outstandingScroll++
        FxTimer.runLater(Duration.ofMillis(100)) {
            if (--outstandingScroll == 0) {
                showParagraphAtTop(document.paragraphs.size - 1)
                scrollXToPixel(0.0)
            }
        }
    }

    fun replaceSelectionAndDisplay(s: String) {
        replaceSelection(s)
        undoManager.preventMerge()
    }

    fun clearStyles() {
        repeat(paragraphs.size) { i ->
            setStyle(i, initialTextStyle)
        }
    }

    fun setStyleForRange(startLine: Int, startCol: Int, endLine: Int, endCol: Int, textStyle: S) {
        val numLines = endLine - startLine + 1
        when {
            numLines == 1 -> {
                setStyle(startLine, startCol, endCol, textStyle)
            }
            numLines > 1 -> {
                setStyle(startLine, startCol, paragraphs[startLine].length(), textStyle)
                repeat(numLines - 2) { i ->
                    val rowIndex = startLine + i + 1
                    setStyle(rowIndex, 0, paragraphs[rowIndex].length(), textStyle)
                }
                setStyle(endLine, 0, endCol, textStyle)
            }
        }
    }

    private var engineConnectorImpl: EditorEngineConnector? = null

    private fun editorEngineConnector(): EditorEngineConnector? {
        val c = engineConnectorImpl
        if (c != null) {
            return c
        }
        val clientCopy = client ?: return null
        val res = GuiEditorEngineConnector(clientCopy)
        engineConnectorImpl = res
        return res
    }

    private var wrapWithParenImpl: EditorAction? = null

    private fun wrapWithParenAction(): EditorAction? {
        val w = wrapWithParenImpl
        if (w != null) {
            return w
        }
        val engineConnector = editorEngineConnector() ?: return null
        val action = WrapWithParen(engineConnector)
        wrapWithParenImpl = action
        return action
    }

    fun wrapWithParen() {
        val action = wrapWithParenAction() ?: return
        action.run(editableText)
    }

    fun completeSymbol() {
        val c = client ?: return
        val text = editableText.text()
        val index = editableText.cursorPosition()
        val state = editableText.markState()

        val displayPos = caretBounds.let { b ->
            val b0 = b.get()
            if (b.isPresent) {
                Point2D(b0.maxX + 10, b0.maxY + 10)
            } else {
                null
            }
        }

        c.calculationQueue.pushJobToQueue { engine ->
            val (candidates, prefix) = symbolCompletionCandidates(engine, text, index)

            fun completionCallback(candidate: SymbolCompletionCandidate) {
                if (!editableText.stateChangedSinceLastMark(state)) {
                    val s = candidate.text
                    val startIndex = editableText.cursorPosition() - prefix.length
                    editableText.remove(prefix.length, startIndex)
                    editableText.insert(s, startIndex)
                    editableText.commit()
                }
            }

            Platform.runLater {
                if (!editableText.stateChangedSinceLastMark(state)) {
                    when {
                        candidates.size == 1 -> completionCallback(candidates[0])
                        candidates.size > 1 -> CompletionDialog.open(this, displayPos, candidates, ::completionCallback)
                    }
                }
            }
        }
    }
}
