package com.dhsdevelopments.kap.clientweb2.webworkerwasmjs

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.clientweb2.*
import com.dhsdevelopments.kap.csv.CsvParseException
import com.dhsdevelopments.kap.csv.readCsv
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.w3c.dom.DedicatedWorkerGlobalScope
import org.w3c.xhr.XMLHttpRequest

external val self: DedicatedWorkerGlobalScope

fun main() {
    loadLibraries()
}

var numOutstandingRequests = 0

fun loadLibraries() {
    loadLibFiles(
        "standard-lib/standard-lib.kap",
        "standard-lib/structure.kap",
        "standard-lib/base-functions.kap",
        "standard-lib/math.kap",
        "standard-lib/math-kap.kap",
        "standard-lib/io.kap",
        "standard-lib/http.kap",
        "standard-lib/output.kap",
        "standard-lib/output3.kap",
        "standard-lib/time.kap",
        "standard-lib/regex.kap",
        "standard-lib/util.kap",
        "standard-lib/map.kap")
}

private fun loadLibFiles(vararg names: String) {
    numOutstandingRequests = names.size
    names.forEach { name ->
        val http = XMLHttpRequest()
        http.open("GET", name)
        http.onload = {
            if (http.readyState == 4.toShort() && http.status == 200.toShort()) {
                registeredFilesRoot.registerFile(name, http.responseText.encodeToByteArray())
            } else {
                KapLogger.e { "Error loading library file: ${name}. Code: ${http.status}" }
            }
            if (--numOutstandingRequests == 0) {
                initQueue()
            }
        }
        http.send()
    }
}

fun initQueue() {
    val engine = Engine()
    engine.addLibrarySearchPath("standard-lib")
    val sendMessageFn = { msg: JsAny -> self.postMessage(msg) }
    engine.standardOutput = CharacterOutput { s ->
        val output = makeJsObject(
            "messageType" to OUTPUT_DESCRIPTOR_TYPE.toJsString(),
            "text" to s.toJsString())
        sendMessageFn(output)
    }
    engine.addModule(JsChartModule(sendMessageFn))
    engine.addModule(JsGuiModule(sendMessageFn))
    engine.addModule(JsAudioModule(sendMessageFn))
    engine.parseAndEval(StringSourceLocation("use(\"standard-lib.kap\")"))
    self.onmessage = { event ->
        when (val request = Json.decodeFromString<Request>(event.data.toString())) {
            is EvalRequest -> processEvalRequest(engine, request)
            is ImportRequest -> processImportRequest(engine, request)
            is ImportCsvRequest -> processImportCsvRequest(engine, request)
            is TokeniseRequest -> processTokeniseRequest(engine, request)
        }
    }
    val message = makeEngineStartedDescriptor(engine)
    self.postMessage(message)
}

private fun makeEngineStartedDescriptor(engine: Engine): JsAny {
    val jsNativeData = engine.nativeData as JsNativeData
    return makeJsObject(
        "messageType" to ENGINE_STARTED_TYPE.toJsString(),
        "text" to "started".toJsString(),
        "breakData" to jsNativeData.breakBufferData)
}

private fun processEvalRequest(engine: Engine, request: EvalRequest) {
    engine.clearInterrupted()
    val sourceLocation = StringSourceLocation(request.src)
    val result = try {
        engine.parseAndEvalWithPostProcessing(sourceLocation) { context, result ->
            val collapsed = result.collapse()
            when (request.resultType) {
                ResultType.FORMATTED_PRETTY -> StringResponse(formatResultToStrings(renderResult(context, collapsed)).joinToString("\n"))
                ResultType.FORMATTED_READABLE -> StringResponse(collapsed.formatted(FormatStyle.READABLE))
                ResultType.JS -> DataResponse(formatValueToJs(collapsed))
            }
        }
    } catch (e: APLGenericException) {
        makeEvalExceptionDescriptor(request.requestId, e, sourceLocation)
    } catch (e: Exception) {
        ExceptionDescriptor(e.message ?: "empty")
    }
    self.postMessage(Json.encodeToString(result).toJsString())
}

private fun processImportRequest(engine: Engine, request: ImportRequest) {
    importIntoVariable(engine, request.varname) {
        formatJsToValue(request.data)
    }
}

fun processImportCsvRequest(engine: Engine, request: ImportCsvRequest) {
    importIntoVariable(engine, request.varname) {
        try {
            readCsv(StringCharacterProvider(request.data))
        } catch (e: CsvParseException) {
            throw ImportFailedException("Error importing CSV: ${e.message}")
        }
    }
}

class ImportFailedException(val responseMessage: String) : Exception("Import function failed: ${responseMessage}")

fun processTokeniseRequest(engine: Engine, request: TokeniseRequest) {
    val engineCopy = engine.copyToSyntaxChecker()
    var resp: JsArray<JsAny>? = null
    try {
        val result = JsArray<JsAny>()
        val tokeniser = TokenGenerator(engineCopy, StringSourceLocation(request.src))
        try {
            while (true) {
                val (token, pos) = tokeniser.nextTokenOrSpace()
                if (token is EndOfFile) break
                val res = when (token) {
                    is Symbol -> TokenTypes.SYMBOL
                    is ParsedLong -> TokenTypes.NUMBER
                    is ParsedBigInt -> TokenTypes.NUMBER
                    is ParsedDouble -> TokenTypes.NUMBER
                    is ParsedComplex -> TokenTypes.NUMBER
                    is StringToken -> TokenTypes.STRING
                    is ParsedCharacter -> TokenTypes.CHAR
                    is Comment -> TokenTypes.COMMENT
                    else -> null
                }
                if (res != null) {
                    val tokenInfo = makeJsObject(
                        "type" to res.toJsNumber(),
                        "p" to makeJsObject(
                            "line" to pos.line.toJsNumber(),
                            "col" to pos.col.toJsNumber(),
                            "endLine" to pos.computedEndLine.toJsNumber(),
                            "endCol" to pos.computedEndCol.toJsNumber()))
                    result.set(result.length, tokenInfo)
                }
            }
        } catch (e: ParseException) {
            // If there is a parse error, just skip the remainder of the file
        }
        resp = result
    } finally {
        val message = makeJsObject(
            "messageType" to TOKENISE_RESULT_TYPE.toJsString(),
            "requestId" to request.requestId.toJsString(),
            "tokens" to resp)
        self.postMessage(message)
        engineCopy.close()
    }
}

private fun importIntoVariable(engine: Engine, varname: String, fn: () -> APLValue) {
    val sym = engine.currentNamespace.internSymbol(varname)
    val env = engine.rootEnvironment
    val binding = env.findBinding(sym) ?: env.bindLocal(sym)
    engine.withThreadLocalAssigned {
        engine.recomputeRootFrame()
        val importResult = try {
            engine.rootStackFrame.storageList[binding.storage.index].updateValue(fn())
            ImportResult("Data imported into: ${varname}")
        } catch (e: ImportFailedException) {
            ImportResult(e.responseMessage)
        }
        self.postMessage(Json.encodeToString(importResult as ResponseMessage).toJsString())
    }
}

private fun makeEvalExceptionDescriptor(requestId: String, exception: APLGenericException, sourceLocation: SourceLocation): EvalExceptionDescriptor {
    var found: Position? = null
    if (exception is ParseException) {
        found = exception.pos
    } else {
        val p0 = exception.pos
        if (p0 != null && p0.source === sourceLocation) {
            found = p0
        } else if (exception is APLEvalException) {
            val callStack = exception.callStack
            if (callStack != null) {
                for (e in callStack.asReversed()) {
                    val p1 = e.pos
                    if (p1 != null && p1.source === sourceLocation) {
                        found = p1
                        break
                    }
                }
            }
        }
    }
    return EvalExceptionDescriptor(requestId, exception.formattedError(), makePosDescriptor(exception.pos), makePosDescriptor(found))
}

private fun makePosDescriptor(pos: Position?): PosDescriptor? {
    return if (pos == null) {
        null
    } else {
        PosDescriptor(pos.line, pos.col, pos.computedEndLine, pos.computedEndCol, pos.callerName)
    }
}

private fun makeEmptyJsObject(): JsAny = js("{}")

@Suppress("UNUSED_PARAMETER")
private fun updateJsObject(obj: JsAny, key: String, value: JsAny?): Unit = js("obj[key] = value")

fun makeJsObject(vararg elements: Pair<String, JsAny?>): JsAny {
    val res = makeEmptyJsObject()
    elements.forEach { (k, v) ->
        updateJsObject(res, k, v)
    }
    return res
}

@Suppress("UNUSED_PARAMETER")
private fun internalMakeJsArray(size: Int): JsAny = js("new Array(size)")
fun <T : JsAny> makeJsArrayOfSize(size: Int): JsArray<T> {
    return internalMakeJsArray(size).unsafeCast()
}
