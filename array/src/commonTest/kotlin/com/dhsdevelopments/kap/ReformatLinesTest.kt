package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals

class ReformatLinesTest {
    @Test
    fun plainNewlinesAreRemoved() {
        val s = "foo\nbar".reformatLines()
        assertEquals("foo bar", s)
    }

    @Test
    fun doubleNewlinesArePreserved() {
        val s = "foo\n\nbar".reformatLines()
        assertEquals("foo\n\nbar", s)
    }

    @Test
    fun simpleStringIsReturnedUnchanged() {
        val s = "foo".reformatLines()
        assertEquals("foo", s)
    }

    @Test
    fun testWithMultiline() {
        val s =
            """
                foo
                bar
                
                testing
                abc
                test
            """.reformatLines()
        assertEquals("foo bar\n\ntesting abc test", s)
    }
}
