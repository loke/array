@file:OptIn(ExperimentalStdlibApi::class)

package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.builtins.ResizedArrayImpls
import com.dhsdevelopments.kap.builtins.compareAPLArrays
import com.dhsdevelopments.kap.dates.APLTimestamp
import com.dhsdevelopments.kap.rendertext.*
import com.dhsdevelopments.mpbignum.BigIntConstants
import com.dhsdevelopments.mpbignum.LongExpressionOverflow
import com.dhsdevelopments.mpbignum.toDouble
import kotlin.jvm.JvmInline

enum class FormatStyle {
    PLAIN,
    READABLE,
    PRETTY
}

class AxisLabel(val title: String) {
    override fun toString() = "AxisLabel[title=${title}]"
}

class DimensionLabels(val labels: List<List<AxisLabel?>?>) {
    fun computeLabelledAxes(): BooleanArray {
        val result = BooleanArray(labels.size) { i ->
            val labelList = labels[i]
            if (labelList == null) {
                false
            } else {
                var found = false
                for (element in labelList) {
                    if (element != null) {
                        found = true
                        break
                    }
                }
                found
            }
        }
        return result
    }

    fun addElementsToAxis(axis: Int, index: Int, n: Int): DimensionLabels {
        return if (labels[axis] == null) {
            this
        } else {
            val res = labels.indices.map { i ->
                val oldAxisLabels = labels[i]
                if (oldAxisLabels == null) {
                    null
                } else {
                    (0 until oldAxisLabels.size + n).map { i2 ->
                        when {
                            i2 < index -> oldAxisLabels[i2]
                            i2 < index + n -> null
                            else -> oldAxisLabels[i2 - n]
                        }
                    }
                }
            }
            DimensionLabels(res)
        }
    }

    fun removeElementsFromAxis(axis: Int, index: Int, n: Int): DimensionLabels {
        return if (labels[axis] == null) {
            this
        } else {
            val res = labels.indices.map { i ->
                val oldAxisLabels = labels[i]
                if (oldAxisLabels == null) {
                    null
                } else {
                    (0 until oldAxisLabels.size - n).map { i2 ->
                        oldAxisLabels[if (i2 < index) i2 else i2 + n]
                    }
                }
            }
            DimensionLabels(res)
        }
    }

    fun insertAxis(axis: Int): DimensionLabels {
        require(axis >= 0 && axis <= labels.size)
        val res = ArrayList<List<AxisLabel?>?>()
        repeat(labels.size + 1) { i ->
            val l = when {
                i < axis -> labels[i]
                i > axis -> labels[i - 1]
                else -> null
            }
            res.add(l)
        }
        return DimensionLabels(res)
    }

    fun deleteAxis(axis: Int): DimensionLabels {
        require(axis >= 0 && axis < labels.size)
        val res = ArrayList<List<AxisLabel?>?>()
        repeat(labels.size - 1) { i ->
            val l = if (i < axis) {
                labels[i]
            } else {
                labels[i - 1]
            }
            res.add(l)
        }
        return DimensionLabels(res)
    }

    fun formatForDebug(): String {
        val buf = StringBuilder()
        buf.append("dimensions = ")
        buf.append(labels.size)
        buf.append("\n")
        labels.indices.forEach { i ->
            buf.append("${i}:")
            val l = labels[i]
            if (l == null) {
                buf.append(" null")
            } else {
                buf.append(" (size: ")
                buf.append(l.size)
                buf.append(")")
                l.forEach { v ->
                    buf.append(" ")
                    buf.append(v?.title)
                }
            }
            buf.append("\n")
        }
        return buf.toString()
    }

    companion object {
        fun computeLabelledAxis(value: APLValue): BooleanArray {
            val labels = value.metadata.labels
            return labels?.computeLabelledAxes() ?: BooleanArray(value.dimensions.size) { false }
        }

        @Suppress("unused")
        fun makeEmpty(dimensions: Dimensions): DimensionLabels {
            val result = ArrayList<List<AxisLabel?>?>(dimensions.size)
            repeat(dimensions.size) {
                result.add(null)
            }
            return DimensionLabels(result)
        }

        fun makeDerived(dimensions: Dimensions, oldLabels: DimensionLabels?, newLabels: List<List<AxisLabel?>?>): DimensionLabels {
            require(newLabels.size == dimensions.size)
            val oldLabelsList = oldLabels?.labels
            val result = ArrayList<List<AxisLabel?>?>(dimensions.size)
            repeat(dimensions.size) { i ->
                val newLabelsList = newLabels[i]
                val v = when {
                    newLabelsList != null -> {
                        require(newLabelsList.size == dimensions[i]) { "newLabelsList does not have correct size" }
                        newLabelsList
                    }
                    oldLabelsList != null -> oldLabelsList[i]
                    else -> null
                }
                result.add(v)
            }
            return DimensionLabels(result)
        }

        fun make(newLabels: List<List<AxisLabel?>?>): DimensionLabels? {
            return if (newLabels.any { it != null }) {
                DimensionLabels(newLabels)
            } else {
                null
            }
        }
    }
}

/**
 * Given two arrays [a] and [b] which both have a member specialisation type of `LONG` (i.e. [ArrayMemberType.isLong]
 * must return true for both arguments), fetch the elements at [aIndex] and [bIndex] in the arrays [a] and [b] respectively.
 * If both values are `Long`, call [optFun] on the two arguments and return the result. If either of the values
 * overflowed to bigint, then convert the arguments to [APLValue] and call [fallbackFun] on the arguments. The return
 * value must be either a Long or a BigInt. If it is a long, this value is returned. If it's a Bigint, a [LongExpressionOverflow]
 * is thrown with this value.
 */
inline fun eval2ArgLongFnOnIndexes(
    a: APLValue, aIndex: Int, b: APLValue, bIndex: Int, optFun: (Long, Long) -> Long, fallbackFun: (APLValue, APLValue) -> APLValue
): Long {
    val a0 = try {
        a.valueAtLong(aIndex)
    } catch (e: LongExpressionOverflow) {
        val a1 = e.result.makeAPLNumber()
        val b1 = b.valueAt(bIndex)
        val res = fallbackFun(a1, b1).unwrapDeferredValue()
        when (res) {
            is APLLong -> return res.value
            is APLBigInt -> throw LongExpressionOverflow(res.value)
            else -> error("Unexpected return value from fallback function")
        }
    }
    val b0 = try {
        b.valueAtLong(bIndex)
    } catch (e: LongExpressionOverflow) {
        val a1 = a0.makeAPLNumber()
        val b1 = b.valueAt(bIndex)
        val res = fallbackFun(a1, b1).unwrapDeferredValue()
        when (res) {
            is APLLong -> return res.value
            is APLBigInt -> throw LongExpressionOverflow(res.value)
            else -> error("Unexpected return value from fallback function")
        }
    }
    return optFun(a0, b0)
}

/**
 * Object describing what is known about members of an array. If a given flag is set, it means that
 * it is known that every element in the array matches this flag. See the descriptions for the individual
 * status functions for information about the assumtions that can be made when that flag is set.
 */
@JvmInline
value class ArrayMemberType(private val flags: Int) {
    fun intersection(uType: ArrayMemberType): ArrayMemberType {
        return ArrayMemberType(flags and uType.flags)
    }

    fun union(uType: ArrayMemberType): ArrayMemberType {
        return ArrayMemberType(flags or uType.flags)
    }

    /**
     * Type that indicates that all elements are integers. When this flag is set, the function [APLValue.valueAtLong] must either
     * return a [Long], or throw a [LongExpressionOverflow] with the value in case of overflow.
     */
    val isLong get() = flags and LONG_BIT == LONG_BIT

    /**
     * Type that indicates that all elements are doubles. When this flag is set, the function [APLValue.valueAtDouble] may
     * be called in the array.
     */
    val isDouble get() = flags and DOUBLE_BIT == DOUBLE_BIT

    /**
     * Extension of the 'LONG' type that restricts the return value to 0 or 1. When this flag is set, it is not
     * necessary to catch [LongExpressionOverflow] when fetching values using [APLValue.valueAtLong].
     */
    val isBoolean get() = flags and BOOLEAN_BIT == BOOLEAN_BIT

    /**
     * Type that indicates that all elements are char.
     */
    val isChar get() = flags and CHAR_BIT == CHAR_BIT

    /**
     * Check whether there are no type specialisations for this array.
     */
    val isGeneric get() = flags == 0

    /**
     * Type that indicates that all elements in the array are atoms. In other words, if this value is true,
     * the array consequently have depth 0.
     */
    val isAtom get() = flags and ATOM_BIT == ATOM_BIT

    override fun toString(): String {
        val propList = ArrayList<String>()
        if (isLong) propList.add("long")
        if (isDouble) propList.add("double")
        if (isBoolean) propList.add("boolean")
        if (isChar) propList.add("char")
        if (isAtom) propList.add("atom")
        return if (propList.isEmpty()) "generic" else propList.joinToString(",")
    }

    companion object {
        private const val LONG_BIT = 0x1
        private const val DOUBLE_BIT = 0x2
        private const val BOOLEAN_BIT = 0x4
        private const val ATOM_BIT = 0x8
        private const val CHAR_BIT = 0x10

        val LONG = ArrayMemberType(LONG_BIT or ATOM_BIT)
        val DOUBLE = ArrayMemberType(DOUBLE_BIT or ATOM_BIT)
        val GENERIC = ArrayMemberType(0)
        val BOOLEAN = ArrayMemberType(LONG_BIT or BOOLEAN_BIT or ATOM_BIT)
        val ATOM = ArrayMemberType(ATOM_BIT)
        val CHAR = ArrayMemberType(CHAR_BIT or ATOM_BIT)
        val ALL = ArrayMemberType(LONG_BIT or DOUBLE_BIT or BOOLEAN_BIT or CHAR_BIT or ATOM_BIT)
    }
}

abstract class APLValue {
    abstract val dimensions: Dimensions
    abstract val rank: Int
    abstract val specialisedType: ArrayMemberType
    abstract val specialisedTypeAsMember: ArrayMemberType
    val isDepth0 get() = specialisedType.isAtom
    open val typeDescription get() = kapClass.name

    abstract fun valueAt(p: Int): APLValue

    /**
     * Get the long value at position [p].
     *
     * Note: This function may throw [LongExpressionOverflow] which must be handled.
     */
    abstract fun valueAtLong(p: Int): Long
    abstract fun valueAtDouble(p: Int): Double

    /**
     * Get the value at position [p] and convert it to long.
     *
     * Note: This function may throw [LongExpressionOverflow] which must be handled.
     */
    abstract fun valueAtCoerceToLong(p: Int, pos: Position?): Long
    abstract fun valueAtCoerceToInt(p: Int, pos: Position?): Int
    abstract fun valueAtCoerceToDouble(p: Int, pos: Position?): Double

    abstract val size: Int

    abstract fun formatted(style: FormatStyle = FormatStyle.PRETTY): String
    abstract fun formattedAsCodeRequiresParens(): Boolean

    abstract fun collapseInt(withDiscard: Boolean = false): APLValue
    abstract fun collapseFirstLevel(): APLValue

    fun isScalar(): Boolean = rank == 0
    fun arrayify() = if (rank == 0) ResizedArrayImpls.resizedSingleValue(size1Dimensions(), this.disclose()) else this

    abstract fun unwrapDeferredValue(): APLValue
    abstract fun compareEqualsTotalOrdering(reference: APLValue, pos: Position? = null, typeDiscrimination: Boolean = true): Boolean
    abstract val kapClass: KapClass
    abstract val isAtomic: Boolean

    abstract fun compareTotalOrdering(reference: APLValue, pos: Position? = null, typeDiscrimination: Boolean = true): Int

    abstract fun disclose(): APLValue

    abstract val metadata: APLValueMetadata

    fun labelsAsStrings(axis: Int): List<String> {
        val l = metadata.labels?.labels?.get(axis)?.mapIndexed { i, label -> label?.title ?: i.toString() }
        return l ?: (0 until dimensions[axis]).map(Int::toString)
    }

    fun collapse(withDiscard: Boolean = false): APLValue {
        val m = metadata.collapse()
        val v = collapseInt(withDiscard = withDiscard)
        return when {
            v === this -> this
            m.isDefault || withDiscard -> v
            else -> MetadataOverrideArray(v, m)
        }
    }

    /**
     * Return a value which can be used as a hash key when storing references to this object in Kotlin maps.
     * The key must follow the standard equals/hashCode conventions with respect to the object which it
     * represents.
     *
     * This the key returned will follow strict type discrimination. I.e. a floating point number will always
     * be different from a rational number, even if the two numbers reflect the same value.
     *
     * In other words, if two instances of [APLValue] are to be considered equivalent, then the objects returned
     * by this method should be the same when compared using [equals] and return the same value from [hashCode].
     */
    fun makeTypeQualifiedKey(): APLValueKey = APLValueKey.CmpKey(this)

    /**
     * Return a value which can be used as a hash key when storing references to this object in Kotlin maps.
     * The key must follow the standard equals/hashCode conventions with respect to the object which it
     * represents.
     *
     * This the key returned will follow numeric equivalence rules, i.e. -0.0=0.0, 0.5=1/2, etc.
     *
     * In other words, if two instances of [APLValue] are to be considered equivalent, then the objects returned
     * by this method should be the same when compared using [equals] and return the same value from [hashCode].
     */
    fun makeNonTypeQualifiedKey(): APLValueKey = APLValueKey.NumericKey(this)

    fun singleValueOrError(): APLValue {
        return when {
            rank == 0 -> this
            size == 1 -> valueAt(0)
            else -> throw IllegalStateException("Expected a single element in array, found ${size} elements")
        }
    }

    fun ensureNumber(pos: Position? = null): APLNumber {
        return ensureNumberOrNull() ?: throwAPLException(IncompatibleTypeException("Wanted a value of type number. Got: ${typeDescription}", pos))
    }

    abstract fun ensureNumberOrNull(): APLNumber?
    abstract fun ensureSymbol(pos: Position? = null): APLSymbol
    abstract fun ensureList(pos: Position? = null): APLList
    abstract fun ensureMap(pos: Position? = null): APLMap
    abstract fun ensureChar(pos: Position? = null): APLChar

    fun toIntArray(pos: Position): IntArray {
        return IntArray(size) { i -> valueAtCoerceToInt(i, pos) }
    }

    abstract fun asBoolean(pos: Position? = null): Boolean
    abstract fun asHtml(buf: Appendable)

    open fun membersSequence(): Sequence<APLValue> {
        val v = unwrapDeferredValue()
        return if (v.isAtomic) {
            sequenceOf(v)
        } else {
            Sequence {
                val length = v.size
                var index = 0
                object : Iterator<APLValue> {
                    override fun hasNext() = index < length
                    override fun next() = v.valueAt(index++)
                }
            }
        }
    }

    companion object {
        val typeSortOrder = arrayOf(
            APLLong::class,
            APLBigInt::class,
            APLRational::class,
            APLDouble::class,
            APLComplex::class,
            APLChar::class,
            APLSymbol::class,
            APLNilValue::class,
            APLArray::class,
            APLMap::class,
            APLList::class,
            APLTimestamp::class)

        fun typeToPosition(ref: APLValue): Int? {
            typeSortOrder.forEachIndexed { i, cl ->
                if (cl.isInstance(ref)) {
                    return i
                }
            }
            return null
        }
    }

    interface APLValueKey {
        val value: APLValue

        class CmpKey(override val value: APLValue) : APLValueKey {
            override fun equals(other: Any?) = other is APLValueKey && value.compareEqualsTotalOrdering(other.value)
            override fun hashCode() = value.typeQualifiedHashCode()
        }

        class NumericKey(override val value: APLValue) : APLValueKey {
            override fun equals(other: Any?) = other is APLValueKey && value.compareEqualsTotalOrdering(other.value, typeDiscrimination = false)
            override fun hashCode() = value.nonTypeQualifiedHashCode()
        }
    }

    abstract fun typeQualifiedHashCode(): Int
    abstract fun nonTypeQualifiedHashCode(): Int
}

abstract class AbstractAPLValue : APLValue() {
    override val rank: Int get() = dimensions.size
    override val specialisedType: ArrayMemberType get() = ArrayMemberType.GENERIC
    override val specialisedTypeAsMember get() = ArrayMemberType.GENERIC

    override fun unwrapDeferredValue(): APLValue {
        return if (dimensions.isEmpty()) EnclosedAPLValue.make(valueAt(0).unwrapDeferredValue()) else this
    }

    override fun valueAtLong(p: Int): Long = error("valueAtLong not implemented for class ${this::class.simpleName}")
    override fun valueAtDouble(p: Int): Double = error("valueAtDouble not implemented for class ${this::class.simpleName}")

    override fun valueAtCoerceToInt(p: Int, pos: Position?): Int {
        val l = if (specialisedType.isLong) {
            valueAtLong(p)
        } else {
            valueAt(p).ensureNumber(pos).asLong(pos)
        }
        if (l < Int.MIN_VALUE || l > Int.MAX_VALUE) {
            throwAPLException(IntMagnitudeException(l, pos))
        }
        return l.toInt()
    }

    override fun valueAtCoerceToLong(p: Int, pos: Position?): Long {
        val st = specialisedType
        return when {
            st.isLong -> valueAtLong(p)
            st.isDouble -> valueAtDouble(p).let { v ->
                if (v >= Long.MIN_VALUE && v <= Long.MAX_VALUE) {
                    v.toLong()
                } else {
                    throwAPLException(KapOverflowException("Value does not fit in a long: ${v}"))
                }
            }
            else -> valueAt(p).ensureNumber(pos).asLong(pos)
        }
    }

    override fun valueAtCoerceToDouble(p: Int, pos: Position?): Double {
        val st = specialisedType
        return when {
            st.isLong -> {
                try {
                    valueAtLong(p).toDouble()
                } catch (e: LongExpressionOverflow) {
                    e.result.toDouble()
                }
            }
            st.isDouble -> valueAtDouble(p)
            else -> valueAt(p).ensureNumber(pos).asDouble(pos)
        }
    }

    override val size: Int get() = dimensions.contentSize()

    override fun formattedAsCodeRequiresParens() = true

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) =
        compareTotalOrdering(reference, pos = null, typeDiscrimination = typeDiscrimination) == 0

    override fun compareTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Int {
        throwAPLException(
            IncompatibleTypeException(
                "Comparison not implemented for objects of type ${kapClass.name} to ${reference.kapClass.name}",
                pos))
    }

    override val metadata: APLValueMetadata get() = APLValueMetadata.DefaultMetadata

    override fun ensureNumberOrNull(): APLNumber? {
        val v = unwrapDeferredValue()
        return if (v === this) {
            null
        } else {
            v.ensureNumberOrNull()
        }
    }

    override fun ensureSymbol(pos: Position?): APLSymbol {
        val v = unwrapDeferredValue()
        if (v === this) {
            throwAPLException(IncompatibleTypeException("Value $this is not a symbol (type=${kapClass.name})", pos))
        } else {
            return v.ensureSymbol(pos)
        }
    }

    override fun ensureList(pos: Position?): APLList {
        val v = unwrapDeferredValue()
        if (v === this) {
            throwAPLException(IncompatibleTypeException("Value $this is not a list (type=${kapClass.name})", pos))
        } else {
            return v.ensureList(pos)
        }
    }

    override fun ensureMap(pos: Position?): APLMap {
        val v = unwrapDeferredValue()
        if (v === this) {
            throwAPLException(IncompatibleTypeException("Value $this is not a map (type=${kapClass.name})", pos))
        } else {
            return v.ensureMap(pos)
        }
    }

    override fun ensureChar(pos: Position?): APLChar {
        val v = unwrapDeferredValue()
        if (v === this) {
            throwAPLException(IncompatibleTypeException("Value $this is not a char (type=${kapClass.name})", pos))
        } else {
            return v.ensureChar(pos)
        }
    }

    override fun asBoolean(pos: Position?): Boolean {
        val v = unwrapDeferredValue()
        return if (v === this) {
            true
        } else {
            v.asBoolean(pos)
        }
    }

    override fun asHtml(buf: Appendable) {
        buf.append("<pre>")
        escapeHtml(formatted(FormatStyle.PRETTY), buf)
        buf.append("</pre>")
    }
}

fun APLValue.listify(): APLList {
    val v = unwrapDeferredValue()
    return if (v is APLList) {
        v
    } else {
        APLList(listOf(v))
    }
}

/**
 * Converts the content of this value to a byte array according to the following rules:
 *
 *   - If the array is not a scalar or vector, throw an exception
 *   - If the size of the array is zero, return a zero-length [ByteArray]
 *   - If each value in the array is an integer in the range 0-255, create a byte array with the corresponding values
 *   - If each value in the array is a character, encode the string as UTF-8
 *   - Otherwise, throw an exception
 */
fun APLValue.asByteArray(pos: Position? = null): ByteArray {
    val v = this.collapse().arrayify()
    if (v.dimensions.size != 1) {
        throwAPLException(InvalidDimensionsException("Value must be a scalar or a one-dimensional array", pos))
    }
    val size = v.dimensions[0]
    if (size == 0) {
        return byteArrayOf()
    } else {
        return when (val firstValue = v.valueAt(0)) {
            is APLNumber -> {
                ByteArray(size) { i ->
                    val valueInt = (if (i == 0) firstValue else v.valueAt(i)).ensureNumber(pos).asInt(pos)
                    if (valueInt < 0 || valueInt > 255) {
                        throwAPLException(IncompatibleTypeException("Element at index ${i} in array is not a byte: ${valueInt}", pos))
                    }
                    valueInt.toByte()
                }
            }
            is APLChar -> {
                v.toStringValue(pos).encodeToByteArray()
            }
            else -> {
                throwAPLException(IncompatibleTypeException("Value cannot be converted to byte array", pos))
            }
        }
    }
}

inline fun APLValue.iterateMembers(fn: (APLValue) -> Unit) {
    when {
        isAtomic -> fn(this)
        else -> for (i in 0 until size) {
            fn(valueAt(i))
        }
    }
}

inline fun APLValue.iterateMembersWithPosition(fn: (APLValue, Int) -> Unit) {
    when {
        isAtomic -> fn(this, 0)
        else -> for (i in 0 until size) {
            fn(valueAt(i), i)
        }
    }
}

abstract class APLSingleValue : AbstractAPLValue() {
    override val dimensions get() = emptyDimensions()
    override val isAtomic get() = true
    override fun valueAt(p: Int) =
        if (p == 0) this else throwAPLException(APLIndexOutOfBoundsException("Reading at non-zero index ${p} from scalar"))

    override val size get() = 1
    override val rank get() = 0
    override fun collapseInt(withDiscard: Boolean): APLValue = this
    override fun collapseFirstLevel() = this
    override fun disclose() = this
    override val specialisedType get() = ArrayMemberType.ATOM
    override val specialisedTypeAsMember get() = ArrayMemberType.ATOM

    open fun numericCompare(reference: APLValue, pos: Position? = null, typeDiscrimination: Boolean = false): Int =
        throwAPLException(NumericComparisonNotSupported("Cannot compare values of type: ${kapClass.name} to ${reference.kapClass.name}", pos))

    open val numericCompareValid: Boolean get() = false

    override fun unwrapDeferredValue() = this

    private fun throwOrderingNotSupportedException(cl: KapClass, pos: Position?): Nothing =
        throwAPLException(APLEvalException("Objects of type ${cl.name} cannot be compared", pos))

    open fun numericCompareEquals(reference: APLSingleValue, pos: Position?) =
        if (reference is APLComplex) {
            APLComplex.compareEqualsComplexToSingleValue(reference, this, pos)
        } else if (this.numericCompareValid && reference.numericCompareValid) {
            numericCompare(reference, typeDiscrimination = false) == 0
        } else {
            compareTotalOrdering(reference, pos, typeDiscrimination = false) == 0
        }

    override fun compareTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Int {
        val other = reference.unwrapDeferredValue()
        return if (other is APLSingleValue && numericCompareValid && other.numericCompareValid) {
            numericCompare(other, pos, typeDiscrimination)
        } else if (this::class == other::class) {
            compareSameType(other, pos)
        } else {
            val pos0 = typeToPosition(this) ?: throwOrderingNotSupportedException(this.kapClass, pos)
            val pos1 = typeToPosition(other) ?: throwOrderingNotSupportedException(other.kapClass, pos)
            require(pos0 != pos1)
            return if (pos0 < pos1) -1 else 1
        }
    }

    open fun compareSameType(reference: APLValue, pos: Position?): Int = throwOrderingNotSupportedException(this.kapClass, pos)

    override fun nonTypeQualifiedHashCode() = typeQualifiedHashCode()
}

abstract class APLArray : AbstractAPLValue() {
    override val kapClass get() = SystemClass.ARRAY
    override val isAtomic get() = false
    override val typeDescription get() = "array ${dimensions}"

    override fun collapseInt(withDiscard: Boolean): APLValue {
        val v = unwrapDeferredValue()
        return if (withDiscard) {
            when {
                v.rank == 0 -> v.valueAt(0).collapse(withDiscard = true)
                else -> v.iterateMembers { v0 -> v0.collapse(withDiscard = true) }
            }
            UnusedResultAPLValue
        } else {
            when {
                v.rank == 0 -> EnclosedAPLValue.make(v.valueAt(0).collapse())
                else -> CollapsedArrayImpl.make(v)
            }
        }
    }

    override fun collapseFirstLevel(): APLValue {
        return APLArrayImpl(dimensions, Array(size, this::valueAt))
    }

    override fun formatted(style: FormatStyle) =
        when (style) {
            FormatStyle.PLAIN -> formatArrayAsPlain(this)
            FormatStyle.PRETTY -> encloseInBox(this, FormatStyle.PRETTY)
            FormatStyle.READABLE -> arrayToAPLFormat(this)
        }

    override fun formattedAsCodeRequiresParens() = !isStringValue()

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Boolean {
        val u = this.unwrapDeferredValue()
        if (u is APLSingleValue) {
            return u.compareEqualsTotalOrdering(reference, pos)
        } else {
            val uRef = reference.unwrapDeferredValue()
            if (!u.dimensions.compareEquals(uRef.dimensions)) {
                return false
            }
            for (i in 0 until u.size) {
                val o1 = u.valueAt(i)
                val o2 = uRef.valueAt(i)
                if (!o1.compareEqualsTotalOrdering(o2, pos)) {
                    return false
                }
            }
            return true
        }
    }

    override fun compareTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Int {
        return compareAPLArrays(this, reference, pos, typeDiscrimination)
    }

    override fun disclose() = if (dimensions.size == 0) valueAt(0) else this

    private inline fun computeHashCode(fn: (APLValue) -> Int): Int {
        var curr = 0
        dimensions.forEach { dim ->
            curr = (curr * 63) xor dim
        }
        membersSequence().forEach { v ->
            curr = (curr * 63) xor fn(v)
        }
        return curr
    }

    override fun typeQualifiedHashCode() = computeHashCode(APLValue::typeQualifiedHashCode)
    override fun nonTypeQualifiedHashCode() = computeHashCode(APLValue::nonTypeQualifiedHashCode)

    override fun asHtml(buf: Appendable) {
        val d = dimensions
        when {
            d.size == 1 && isStringValue() -> {
                buf.append("<span style=\"color: #00c000;\">")
                escapeHtml(toStringValue(), buf)
                buf.append("</span>")
            }
            d.size == 2 -> {
                array2DAsHtml(this, buf)
            }
            else -> {
                super.asHtml(buf)
            }
        }
    }
}

class APLMap(val content: ImmutableMap2<APLValueKey, APLValue>) : APLSingleValue() {
    override val kapClass get() = SystemClass.MAP
    override val dimensions = emptyDimensions()

    override fun formatted(style: FormatStyle): String {
        return when (style) {
            FormatStyle.PLAIN -> "map[size=${content.size}]"
            FormatStyle.READABLE -> formatMapReadable()
            FormatStyle.PRETTY -> formatMapPretty()
        }
    }

    private fun formatMapReadable(): String {
        val buf = StringBuilder()
        buf.append("map ${content.size} 2⍴")
        var first = true
        content.forEach { (k, v) ->
            if (first) {
                first = false
            } else {
                buf.append(" ")
            }
            maybeWrapInParens(buf, k.value)
            buf.append(" ")
            maybeWrapInParens(buf, v)
        }
        return buf.toString()
    }

    private fun formatMapPretty(): String {
        val buf = StringBuilder()
        buf.append("map(size=${content.size})\n")
        val s = String2D(aplMapToArray().formatted(FormatStyle.PRETTY))
        repeat(s.height()) { i ->
            buf.append("  ")
            s.row(i).forEach { ch ->
                buf.append(ch)
            }
            buf.append("\n")
        }
        return buf.toString()
    }

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Boolean {
        if (reference !is APLMap) {
            return false
        }
        if (content.size != reference.content.size) {
            return false
        }
        content.forEach { (key, value) ->
            val v = reference.content[key] ?: return false
            // NOTE: The typeDiscrimination argument is intentionally not passed to this function.
            // This is in order to ensure that maps are always type discriminated.
            if (!value.compareEqualsTotalOrdering(v, pos)) {
                return false
            }
        }
        return true
    }

    override fun typeQualifiedHashCode(): Int {
        var curr = 0
        content.forEach { (key, value) ->
            curr = (curr * 63) xor key.value.typeQualifiedHashCode()
            curr = (curr * 63) xor value.typeQualifiedHashCode()
        }
        return curr
    }

    override fun nonTypeQualifiedHashCode() = typeQualifiedHashCode()

    override fun ensureMap(pos: Position?) = this

    fun lookupValue(key: APLValueKey): APLValue? {
        return content[key]
    }

    @Suppress("unused")
    fun updateValue(key: APLValueKey, value: APLValue): APLMap {
        return APLMap(content.copyAndPut(key, value))
    }

    fun updateValues(elements: List<Pair<APLValueKey, APLValue>>): APLValue {
        return when (elements.size) {
            0 -> this
            1 -> elements[0].let { (key, value) -> APLMap(content.copyAndPut(key, value)) }
            else -> APLMap(content.copyAndPutMultiple(*elements.map { v -> Pair(v.first, v.second) }.toTypedArray()))
        }
    }

    fun elementCount(): Int {
        return content.size
    }

    fun removeValues(toRemove: ArrayList<APLValue>): APLMap {
        return APLMap(content.copyWithoutMultiple(toRemove.map { v -> v.makeTypeQualifiedKey() }.toTypedArray()))
    }

    fun aplMapToArray(): APLValue {
        val res = ArrayList<APLValue>()
        content.forEach { (key, value) ->
            res.add(key.value)
            res.add(value)
        }
        return APLArrayList(dimensionsOfSize(res.size / 2, 2), res)
    }
}

open class APLList(val elements: List<APLValue>) : APLSingleValue() {
    override val kapClass get() = SystemClass.LIST
    override val dimensions get() = emptyDimensions()

    override fun formattedAsCodeRequiresParens() = true

    override fun formatted(style: FormatStyle) =
        when (style) {
            FormatStyle.PLAIN -> elements.joinToString(separator = " ; ") { v -> v.formatted(FormatStyle.PLAIN) }
            FormatStyle.PRETTY -> elements.joinToString(separator = "\n; value\n") { v -> v.formatted(FormatStyle.PRETTY) }
            FormatStyle.READABLE -> elements.joinToString(separator = " ; ") { v -> v.formatted(FormatStyle.READABLE) }
        }

    override fun collapseInt(withDiscard: Boolean): APLValue {
        return if (withDiscard) {
            elements.forEach { v ->
                v.collapse(true)
            }
            UnusedResultAPLValue
        } else {
            val res = elements.map { v ->
                v.collapse(false)
            }
            CollapsedAPLList(res)
        }
    }

    override fun ensureList(pos: Position?) = this

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Boolean {
        if (reference !is APLList) {
            return false
        }
        if (elements.size != reference.elements.size) {
            return false
        }
        elements.indices.forEach { i ->
            if (!listElement(i).compareEqualsTotalOrdering(reference.listElement(i), pos, typeDiscrimination)) {
                return false
            }
        }
        return true
    }

    override fun typeQualifiedHashCode() = computeHashCodeFromList(elements, APLValue::typeQualifiedHashCode)
    override fun nonTypeQualifiedHashCode() = computeHashCodeFromList(elements, APLValue::nonTypeQualifiedHashCode)

    private inline fun computeHashCodeFromList(values: Collection<APLValue>, fn: (APLValue) -> Int): Int {
        var curr = 0
        values.forEach { v ->
            curr = (curr * 63) xor fn(v)
        }
        return curr
    }

    fun listSize() = elements.size
    fun listElement(index: Int, pos: Position? = null) =
        if (index >= 0 && index < elements.size) {
            elements[index]
        } else {
            throwAPLException(ListOutOfBounds("Attempt to access element ${index} from list. Size: ${elements.size}", pos))
        }

    override fun compareSameType(reference: APLValue, pos: Position?): Int {
        val other = reference as APLList
        return if (elements.size == other.elements.size) {
            repeat(elements.size) { i ->
                val res = this.elements[i].compareTotalOrdering(other.elements[i])
                if (res != 0) {
                    return res
                }
            }
            return 0
        } else {
            elements.size.compareTo(other.elements.size)
        }
    }

    fun ensureListSize(pos: Position?, maxArgs: Int? = null, minArgs: Int? = null): Array<APLValue> {
        if (maxArgs != null) {
            fun throwElementCountException(): Nothing {
                val buf = StringBuilder()
                buf.append("Expected a list of ")
                if (minArgs == null) {
                    buf.append(maxArgs)
                } else {
                    buf.append(minArgs)
                    buf.append("-")
                    buf.append(maxArgs)
                }
                buf.append(" elements, got: ")
                buf.append(listSize())
                throwAPLException(APLEvalException(buf.toString(), pos))
            }

            if (minArgs == null) {
                if (listSize() != maxArgs) {
                    throwElementCountException()
                }
            } else {
                if (listSize() > maxArgs || listSize() < minArgs) {
                    throwElementCountException()
                }
            }
        }
        return elements.toTypedArray()
    }
}

class CollapsedAPLList(elements: List<APLValue>) : APLList(elements) {
    override fun collapseInt(withDiscard: Boolean) = this
}

private fun arrayToAPLFormat(value: APLArray): String {
    val v = value.collapse()
    val labelsDefinition = makeReadableLabelsDefinitionIfExists(v.metadata)
    val s = if (v.isStringValue()) {
        renderStringValue(v, FormatStyle.READABLE)
    } else {
        arrayToAPLFormatStandard(v as APLArray)
    }
    return if (labelsDefinition == null) {
        s
    } else {
        "${labelsDefinition}${s}"
    }
}

fun makeReadableLabelsDefinitionIfExists(metadata: APLValueMetadata): String? {
    val labels = metadata.labels
    return if (labels != null) {
        val appender = StringBuilder()
        labels.labels.forEachIndexed { i, labelsList ->
            if (labelsList != null && labelsList.any { l -> l != null }) {
                labelsList.forEach { label ->
                    if (label == null) {
                        appender.append("null ")
                    } else {
                        appender.append("\"")
                        appender.append(label.title.replace("\"", "\\\""))
                        appender.append("\" ")
                    }
                }
                appender.append("labels[")
                appender.append(i.toString())
                appender.append("] ")
            }
        }
        appender.toString()
    } else {
        null
    }
}

private fun arrayToAPLFormatStandard(value: APLArray): String {
    return buildString {
        val dimensions = value.dimensions
        if (dimensions.size == 0) {
            append("⊂")
            append(value.valueAt(0).formatted(FormatStyle.READABLE))
        } else {
            append(dimensions[0])
            repeat(dimensions.size - 1) { i ->
                append(" ")
                append(dimensions[i + 1])
            }
            append("⍴")
            when (value.size) {
                0 -> {
                    append("1")
                }
                1 -> {
                    val a = value.valueAt(0)
                    if (!a.isAtomic) {
                        append("⊂")
                    }
                    append(a.formatted(FormatStyle.READABLE))
                }
                else -> {
                    for (i in 0 until value.size) {
                        val a = value.valueAt(i)
                        if (i > 0) {
                            append(" ")
                        }
                        maybeWrapInParens(this, a)
                    }
                }
            }
        }
    }
}

fun isNullValue(value: APLValue): Boolean {
    val dimensions = value.dimensions
    return dimensions.size == 1 && dimensions[0] == 0
}

fun APLValue.isStringValue(): Boolean {
    val dimensions = this.dimensions
    if (dimensions.size != 1) {
        return false
    }
    if (specialisedType.isChar) {
        return true
    }
    iterateMembers { v ->
        if (v !is APLChar) {
            return false
        }
    }
    return true
}

fun APLValue.toStringValueOrNull(): String? {
    val dimensions = this.dimensions
    if (dimensions.size != 1) {
        return null
    }

    val buf = StringBuilder()
    for (i in 0 until this.size) {
        val v = this.valueAt(i)
        if (v !is APLChar) {
            return null
        }
        buf.append(v.asString())
    }

    return buf.toString()
}

inline fun APLValue.toStringValue(pos: Position? = null, message: (() -> String?) = { null }): String {
    val result = this.toStringValueOrNull()
    if (result == null) {
        val m = message()
        val messagePrefix = if (m == null) "" else "${m}: "
        throwAPLException(IncompatibleTypeException("${messagePrefix}Argument is not a string", pos))
    }
    return result
}

class ConstantArray(
    override val dimensions: Dimensions,
    value: APLValue
) : APLArray() {

    private val valueInternal = value.unwrapDeferredValue()

    override val specialisedType get() = valueInternal.specialisedTypeAsMember

    override fun valueAt(p: Int) = valueInternal
    override fun valueAtLong(p: Int) = valueInternal.ensureNumber().asLong()
    override fun valueAtDouble(p: Int) = valueInternal.ensureNumber().asDouble()
}

object EmptyValue : APLArray() {
    override val dimensions = emptyListDimensions()

    override fun valueAt(p: Int): APLValue {
        throwAPLException(APLIndexOutOfBoundsException("Attempt to read value from the empty value"))
    }

    override val metadata = EmptyValueMetadata()

    class EmptyValueMetadata : APLValueMetadata {
        override val defaultValue get() = APLNilValue
    }
}

object UnusedResultAPLValue : APLValue() {
    private fun raiseError(): Nothing = error { "Attempt to call unused value" }

    override val kapClass get() = raiseError()
    override val dimensions get() = raiseError()
    override val rank get() = raiseError()
    override val specialisedType get() = raiseError()
    override val specialisedTypeAsMember get() = raiseError()
    override val isAtomic get() = raiseError()
    override fun compareTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) = raiseError()
    override fun valueAt(p: Int) = raiseError()
    override fun valueAtLong(p: Int) = raiseError()
    override fun valueAtDouble(p: Int) = raiseError()
    override fun valueAtCoerceToInt(p: Int, pos: Position?) = raiseError()
    override fun valueAtCoerceToLong(p: Int, pos: Position?) = raiseError()
    override fun valueAtCoerceToDouble(p: Int, pos: Position?) = raiseError()
    override val size get() = raiseError()
    override fun formatted(style: FormatStyle) = raiseError()
    override fun formattedAsCodeRequiresParens() = raiseError()
    override fun collapseInt(withDiscard: Boolean) = raiseError()
    override fun collapseFirstLevel() = raiseError()
    override fun unwrapDeferredValue() = raiseError()
    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) = raiseError()
    override fun disclose() = raiseError()
    override val metadata get() = raiseError()
    override fun ensureNumberOrNull() = raiseError()
    override fun ensureSymbol(pos: Position?) = raiseError()
    override fun ensureList(pos: Position?) = raiseError()
    override fun ensureMap(pos: Position?) = raiseError()
    override fun ensureChar(pos: Position?) = raiseError()
    override fun asBoolean(pos: Position?) = raiseError()
    override fun asHtml(buf: Appendable) = raiseError()
    override fun typeQualifiedHashCode() = raiseError()
    override fun nonTypeQualifiedHashCode() = raiseError()
}

open class APLArrayImpl(
    override val dimensions: Dimensions,
    private val values: Array<APLValue>,
    override val specialisedType: ArrayMemberType = ArrayMemberType.GENERIC
) : APLArray() {

    override fun toString() = "APLArrayImpl[${dimensions}, ${ArrayUtils.toString(values)}]"
    override val size get() = values.size

    override fun valueAt(p: Int) = values[p]

    override fun valueAtLong(p: Int): Long {
        return when (val v = valueAt(p).collapse()) {
            is APLLong -> v.value
            is APLBigInt -> throw LongExpressionOverflow(v.value)
            else -> error("Unexpected type in APLArrayImpl: ${v::class.simpleName}")
        }
    }

    override fun valueAtDouble(p: Int): Double {
        require(specialisedType.isDouble)
        return valueAt(p).ensureNumber().asDouble()
    }

    companion object {
        inline fun make(dimensions: Dimensions, fn: (index: Int) -> APLValue): APLArrayImpl {
            var st = ArrayMemberType.ALL
            val content = Array(dimensions.contentSize()) { index ->
                fn(index).also { res ->
                    st = st.intersection(res.specialisedTypeAsMember)
                }
            }
            return APLArrayImpl(dimensions, content, st)
        }
    }

    // Possible implementation of array specialisation that could extend APLArrayImpl.make
    //    private fun makeSpecialisedArray(d: Dimensions, v: APLValue, startPos: Int, stepSize: Int): APLArray {
//        val type = v.specialisedType
//        var curr = startPos
//        return when {
//            type.isLong -> {
//                val size = d.contentSize()
//                val longArray = LongArray(size)
//                var i = 0
//                try {
//                    repeat(size) {
//                        longArray[i] = v.valueAtLong(curr)
//                        curr += stepSize
//                        i++
//                    }
//                    APLArrayLong(d, longArray)
//                } catch (e: LongExpressionOverflow) {
//                    val vArray = Array<APLValue>(size) { i2 ->
//                        when {
//                            i2 < i -> longArray[i2].makeAPLNumber()
//                            i2 == i -> e.result.makeAPLNumber()
//                            else -> v.valueAt(curr).also { curr += stepSize }
//                        }
//                    }
//                    APLArrayImpl(d, vArray)
//                }
//            }
//            type.isDouble -> {
//                APLArrayDouble(d, DoubleArray(d.contentSize()) { v.valueAtDouble(curr).also { curr += stepSize } })
//            }
//            else -> {
//                APLArrayImpl.make(d) { v.valueAt(curr).also { curr += stepSize } }
//            }
//        }
//    }
}

class CollapsedArrayImpl(
    dimensions: Dimensions,
    values: Array<APLValue>,
    override val specialisedType: ArrayMemberType
) : APLArrayImpl(dimensions, values) {
    override fun collapseInt(withDiscard: Boolean) = this

    companion object {
        fun make(orig: APLValue): APLValue {
            val d = orig.dimensions
            val dSize = orig.size
            if (dSize == 0) {
                return APLEmptyArray(d)
            }
            val st = orig.specialisedType
            return when {
                st.isBoolean -> APLArrayBoolean(d, BooleanArray(dSize) { index ->
                    try {
                        when (orig.valueAtLong(index)) {
                            0L -> false
                            1L -> true
                            else -> error("Unexpected value in boolean array")
                        }
                    } catch (e: LongExpressionOverflow) {
                        when (e.result) {
                            BigIntConstants.ZERO -> false
                            BigIntConstants.ONE -> true
                            else -> error("Unexpected value in boolean array")
                        }
                    }
                })
                st.isLong -> APLArrayLong.makeWithOverflowCheck(d, orig)
                st.isDouble -> APLArrayDouble(d, DoubleArray(dSize) { index -> orig.valueAtDouble(index) })
                else -> {
                    var innerType = ArrayMemberType.ALL
                    val content = Array(dSize) { index ->
                        orig.valueAt(index).collapse().also { res ->
                            innerType = innerType.intersection(res.specialisedTypeAsMember)
                        }
                    }
                    CollapsedArrayImpl(d, content, innerType)
                }
            }
        }
    }
}

//x←?100000000 5⍴2 ⋄ ({+/x}⍣10) 0 ⋄ 1
private fun APLArrayLong.Companion.makeWithOverflowCheck(d: Dimensions, orig: APLValue): APLValue {
    require(orig.specialisedType.isLong)
    val result = LongArray(d.contentSize())
    var booleanCheckPattern = 0L
    var i = 0
    try {
        repeat(result.size) {
            val v = orig.valueAtLong(i)
            booleanCheckPattern = booleanCheckPattern or v
            result[i] = v
            i++
        }
        return APLArrayLong(d, result, booleanCheckPattern == 0L || booleanCheckPattern == 1L)
    } catch (e: LongExpressionOverflow) {
        val array = Array(result.size) { i0 ->
            if (i0 < i) {
                result[i0].makeAPLNumber()
            } else if (i0 == i) {
                APLBigInt(e.result)
            } else {
                orig.valueAt(i0)
            }
        }
        return APLArrayImpl(d, array, ArrayMemberType.ATOM) // The datatype isn't long anymore, but both longs and bigints are atoms
    }
}

class APLArrayList(
    override val dimensions: Dimensions,
    private val values: List<APLValue>,
    override val specialisedType: ArrayMemberType = computeSpecialisedType(values)
) : APLArray() {
    override fun valueAt(p: Int) = values[p]
    override fun toString() = values.toString()
    override val size get() = values.size

    // TODO: This implementation exists in order for this class to preserve type specialisation.
    //       However, it still boxes objects internally, so ideally this class should not be used
    //       if it is known all elements are of a certain type.
    //       The only use of type specialisation for this is in dyadic ~
    override fun valueAtLong(p: Int) = values[p].ensureNumber().asLong()
    override fun valueAtDouble(p: Int) = values[p].ensureNumber().asDouble()

    companion object {
        private fun computeSpecialisedType(values: List<APLValue>): ArrayMemberType {
            var st = ArrayMemberType.ALL
            for (v in values) {
                st = st.intersection(v.specialisedTypeAsMember)
                if (st.isGeneric) {
                    break
                }
            }
            return st
        }
    }
}

class APLEmptyArray(override val dimensions: Dimensions) : APLArray() {
    init {
        require(dimensions.size != 0) { "Empty arrays must not be rank 0" }
        require(dimensions.contentSize() == 0) { "Empty arrays cannot have a size" }
    }

    private fun throwError(): Nothing = throwAPLException(APLIndexOutOfBoundsException("Cannot get value from empty array", null))
    override val specialisedType get() = ArrayMemberType.ALL
    override fun valueAt(p: Int) = throwError()
    override fun valueAtDouble(p: Int) = throwError()
}

class EnclosedAPLValue private constructor(val value: APLValue) : APLArray() {
    override val dimensions: Dimensions
        get() = emptyDimensions()

    override fun valueAt(p: Int): APLValue {
        if (p != 0) {
            throwAPLException(APLIndexOutOfBoundsException("Attempt to read from a non-zero index"))
        }
        return value
    }

    override fun disclose() = value

    override fun unwrapDeferredValue(): APLValue {
        val v = value.unwrapDeferredValue()
        return if (value === v) {
            this
        } else if (value is APLSingleValue) {
            // This can happen if value was a deferred value
            v
        } else {
            EnclosedAPLValue(v)
        }
    }

    companion object {
        fun make(value: APLValue): APLValue {
            return if (value is APLSingleValue) {
                value.unwrapDeferredValue()
            } else {
                EnclosedAPLValue(value)
            }
        }
    }
}

class APLChar(val value: Int) : APLSingleValue() {
    init {
        if (value < 0) {
            throw IllegalArgumentException("Char values cannot be negative")
        }
    }

    override val specialisedTypeAsMember get() = ArrayMemberType.CHAR

    override val kapClass: KapClass get() = SystemClass.CHAR

    fun asString() = charToString(value)

    override fun formattedAsCodeRequiresParens() = false

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) =
        reference.unwrapDeferredValue().let { v -> v is APLChar && value == v.value }

    override fun numericCompare(reference: APLValue, pos: Position?, typeDiscrimination: Boolean): Int {
        return if (reference is APLChar) {
            value.compareTo(reference.value)
        } else {
            super.numericCompare(reference, pos, typeDiscrimination)
        }
    }

    override fun compareSameType(reference: APLValue, pos: Position?): Int {
        return numericCompare(reference, pos)
    }

    override fun ensureChar(pos: Position?) = this

    override fun toString() = "APLChar['${asString()}' 0x${value.toString(16)}]"

    override fun typeQualifiedHashCode() = value.hashCode()

    @OptIn(ExperimentalStdlibApi::class)
    override fun formatted(style: FormatStyle) = when (style) {
        FormatStyle.PLAIN -> charToString(value)
        FormatStyle.PRETTY -> "@${charToString(value)}"
        FormatStyle.READABLE -> if (value in 33..126 && value != 92) "@${charToString(value)}" else "@\\u${value.toHexString(charHexFormat)}"
    }

    override fun asHtml(buf: Appendable) {
        escapeHtml(formatted(FormatStyle.READABLE), buf)
    }

    companion object {
        private val charHexFormat = HexFormat {
            upperCase = true
            number {
                removeLeadingZeros = true
            }
        }

        fun fromLong(value: Long, pos: Position): APLChar {
            if (value < 0) {
                throwAPLException(APLEvalException("Codepoints cannot be negative: ${value}", pos))
            }
            if (value > Int.MAX_VALUE) {
                throwAPLException(APLEvalException("Invalid codepoint: ${value}", pos))
            }
            return APLChar(value.toInt())
        }
    }
}

class APLString(val content: IntArray) : APLArray() {
    private constructor(string: String) : this(string.asCodepointList().toIntArray())

    override val dimensions = dimensionsOfSize(content.size)
    override fun valueAt(p: Int) = APLChar(content[p])

    override fun collapseInt(withDiscard: Boolean) = this
    override val specialisedType get() = ArrayMemberType.CHAR

    override fun toString() = "APLString[value=[${content.joinToString(transform = ::charToString)}]]"

    companion object {
        val EMPTY_STRING = APLString("")

        fun make(s: String) = if (s.isEmpty()) EMPTY_STRING else APLString(s)
    }
}

object APLNullValue : APLArray() {
    override val dimensions get() = emptyListDimensions()
    override val specialisedType get() = ArrayMemberType.ALL
    override fun valueAt(p: Int) = throwAPLException(APLIndexOutOfBoundsException("Attempt to read a value from the null value"))
}

object APLNilValue : APLSingleValue() {
    override val kapClass get() = SystemClass.NIL
    override fun formatted(style: FormatStyle) = "null"
    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) = reference === this

    override fun typeQualifiedHashCode() = 0
}

class APLSymbol(val value: Symbol) : APLSingleValue() {
    override val kapClass get() = SystemClass.SYMBOL

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) =
        reference is APLSymbol && value == reference.value

    override fun compareSameType(reference: APLValue, pos: Position?): Int {
        return value.compareTo((reference as APLSymbol).value)
    }

    override fun ensureSymbol(pos: Position?) = this

    override fun typeQualifiedHashCode() = value.hashCode()

    override fun formatted(style: FormatStyle) =
        when (style) {
            FormatStyle.PLAIN -> value.nameWithNamespace
            FormatStyle.PRETTY -> value.nameWithNamespace
            FormatStyle.READABLE -> {
                val quotePrefix = if (value.namespace == NamespaceList.KEYWORD_NAMESPACE_NAME) "" else "'"
                "${quotePrefix}${value.nameWithNamespace}"
            }
        }

    override fun asHtml(buf: Appendable) {
        escapeHtml(formatted(FormatStyle.PLAIN), buf)
    }
}

/**
 * This class represents a closure. It wraps a function and a context to use when calling the closure.
 *
 * @param fn the function that is wrapped by the closure
 * @param savedFrame the saved stack frame to use when calling the function
 */
class LambdaValue(private val fn: APLFunction, private val savedFrame: StorageStack.StorageStackFrame) : APLSingleValue() {
    override val kapClass get() = SystemClass.LAMBDA_FN

    override fun formatted(style: FormatStyle) =
        when (style) {
            FormatStyle.PLAIN -> "function"
            FormatStyle.READABLE -> throw IllegalArgumentException("Functions can't be printed in readable form")
            FormatStyle.PRETTY -> "function"
        }

    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) = this === reference

    override fun typeQualifiedHashCode() = fn.hashCode()

    fun makeClosure(): APLFunction {
        return object : APLFunction(fn.instantiation, listOf(fn)) {
            override fun eval1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
                withSavedStackFrame(savedFrame) {
                    return fn.eval1Arg(context, a, axis)
                }
            }

            override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
                withSavedStackFrame(savedFrame) {
                    return fn.eval2Arg(context, a, b, axis)
                }
            }

            override fun evalInverse1Arg(context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
                withSavedStackFrame(savedFrame) {
                    return fn.evalInverse1Arg(context, a, axis)
                }
            }

            override fun evalInverse2ArgB(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
                withSavedStackFrame(savedFrame) {
                    return fn.evalInverse2ArgB(context, a, b, axis)
                }
            }

            override fun evalInverse2ArgA(context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
                withSavedStackFrame(savedFrame) {
                    return fn.evalInverse2ArgA(context, a, b, axis)
                }
            }

            override fun evalWithStructuralUnder1Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, axis: APLValue?): APLValue {
                withSavedStackFrame(savedFrame) {
                    return fn.evalWithStructuralUnder1Arg(baseFn, context, a, axis)
                }
            }

            override fun evalWithStructuralUnder2Arg(baseFn: APLFunction, context: RuntimeContext, a: APLValue, b: APLValue, axis: APLValue?): APLValue {
                withSavedStackFrame(savedFrame) {
                    return fn.evalWithStructuralUnder2Arg(baseFn, context, a, b, axis)
                }
            }

            override fun identityValue() = fn.identityValue()
        }
    }

    companion object {
        fun fromValue(a: APLValue, pos: Position): LambdaValue {
            val a0 = a.unwrapDeferredValue()
            if (a0 !is LambdaValue) {
                throwAPLException(IncompatibleTypeException("Expected a lambda value, got: ${a0.kapClass.name}", pos))
            }
            return a0
        }
    }
}

class IntArrayValue(
    srcDimensions: Dimensions,
    val values: IntArray
) : APLArray() {

    override val specialisedType get() = ArrayMemberType.LONG
    override val dimensions = srcDimensions
    override fun valueAt(p: Int) = values[p].makeAPLNumber()
    override fun valueAtLong(p: Int) = values[p].toLong()

    fun intValueAt(p: Int) = values[p]

    init {
        require(values.isNotEmpty()) { "IntArrayValue with size 0" }
    }

    companion object {
        inline fun fromAPLValue(src: APLValue, pos: Position? = null, convert: (Int) -> Int = { it }): IntArrayValue {
            return if (src is IntArrayValue) {
                error("foo")
                src
            } else {
                val values = IntArray(src.size) { i -> convert(src.valueAtCoerceToInt(i, pos)) }
                IntArrayValue(src.dimensions, values)
            }
        }
    }
}

class APLArrayLong(
    override val dimensions: Dimensions,
    val values: LongArray,
    isBoolean: Boolean = false
) : APLArray() {
    override val specialisedType = if (isBoolean) ArrayMemberType.BOOLEAN else ArrayMemberType.LONG
    override fun valueAt(p: Int) = values[p].makeAPLNumber()
    override fun valueAtLong(p: Int) = values[p]
    override fun collapseInt(withDiscard: Boolean) = this
    override val size get() = values.size

    companion object
}

class APLArrayDouble(
    override val dimensions: Dimensions,
    val values: DoubleArray
) : APLArray() {
    override val specialisedType get() = ArrayMemberType.DOUBLE
    override fun valueAt(p: Int) = values[p].makeAPLNumber()
    override fun valueAtDouble(p: Int) = values[p]
    override fun collapseInt(withDiscard: Boolean) = this
    override val size get() = values.size
}

class APLArrayBoolean(
    override val dimensions: Dimensions,
    val values: BooleanArray
) : APLArray() {
    override val specialisedType get() = ArrayMemberType.BOOLEAN
    override fun valueAt(p: Int) = if (values[p]) APLLONG_1 else APLLONG_0
    override fun valueAtLong(p: Int) = if (values[p]) 1L else 0L
    override fun collapseInt(withDiscard: Boolean) = this
    override val size get() = values.size
}

class APLArrayByte(
    override val dimensions: Dimensions,
    val values: ByteArray
) : APLArray() {
    override val specialisedType get() = ArrayMemberType.LONG
    override fun valueAt(p: Int) = (values[p].toLong() and 0xFF).makeAPLNumber()
    override fun valueAtLong(p: Int) = values[p].toLong() and 0xFF
    override fun collapseInt(withDiscard: Boolean) = this
    override val size get() = values.size
}

class APLArrayInt(
    override val dimensions: Dimensions,
    val values: IntArray
) : APLArray() {
    override val specialisedType get() = ArrayMemberType.LONG
    override fun valueAt(p: Int) = values[p].makeAPLNumber()
    override fun valueAtCoerceToInt(p: Int, pos: Position?) = values[p]
    override fun valueAtLong(p: Int) = values[p].toLong()
    override fun collapseInt(withDiscard: Boolean) = this
    override val size get() = values.size
}

/**
 * Wraps an array of longs and presents it as an array of doubles.
 */
class LongToDoubleArrayProjection(value: APLValue, val pos: Position) : DelegatedValue(value) {
    init {
        require(value.specialisedType.isLong)
    }

    override val specialisedType get() = value.specialisedType.union(ArrayMemberType.DOUBLE)

    override fun valueAtDouble(p: Int): Double {
        return try {
            valueAtLong(p).toDouble()
        } catch (e: LongExpressionOverflow) {
            e.result.toDouble()
        }
    }

    override fun valueAtCoerceToDouble(p: Int, pos: Position?) = valueAtDouble(p)
}

abstract class AbstractDelegatedValue : APLValue() {
    abstract val value: APLValue
    override val kapClass get() = value.kapClass
    override val dimensions get() = value.dimensions
    override val isAtomic get() = value.isAtomic
    override val rank: Int get() = value.rank
    override val specialisedType: ArrayMemberType get() = value.specialisedType
    override val specialisedTypeAsMember get() = value.specialisedTypeAsMember
    override fun valueAt(p: Int) = value.valueAt(p)
    override fun valueAtLong(p: Int) = value.valueAtLong(p)
    override fun valueAtDouble(p: Int) = value.valueAtDouble(p)
    override fun valueAtCoerceToInt(p: Int, pos: Position?) = value.valueAtCoerceToInt(p, pos)
    override fun valueAtCoerceToLong(p: Int, pos: Position?) = value.valueAtCoerceToLong(p, pos)
    override fun valueAtCoerceToDouble(p: Int, pos: Position?) = value.valueAtCoerceToDouble(p, pos)
    override val size: Int get() = value.size
    override fun formatted(style: FormatStyle) = value.formatted(style)
    override fun collapseInt(withDiscard: Boolean) = value.collapseInt(withDiscard = withDiscard)
    override fun collapseFirstLevel() = value.collapseFirstLevel()
    override val metadata get() = value.metadata
    override fun unwrapDeferredValue() = value.unwrapDeferredValue()
    override fun compareEqualsTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) = value.compareEqualsTotalOrdering(reference, pos)
    override fun compareTotalOrdering(reference: APLValue, pos: Position?, typeDiscrimination: Boolean) = value.compareTotalOrdering(reference, pos)
    override fun disclose() = value.disclose()
    override fun ensureSymbol(pos: Position?) = value.ensureSymbol(pos)
    override fun ensureList(pos: Position?) = value.ensureList(pos)
    override fun ensureMap(pos: Position?) = value.ensureMap(pos)
    override fun ensureChar(pos: Position?) = value.ensureChar(pos)
    override fun asBoolean(pos: Position?) = value.asBoolean(pos)
    override fun formattedAsCodeRequiresParens() = value.formattedAsCodeRequiresParens()
    override fun ensureNumberOrNull() = value.ensureNumberOrNull()
    override fun asHtml(buf: Appendable) = value.asHtml(buf)
    override fun typeQualifiedHashCode() = value.typeQualifiedHashCode()
    override fun nonTypeQualifiedHashCode() = value.nonTypeQualifiedHashCode()
}

open class DelegatedValue(delegate: APLValue) : AbstractDelegatedValue() {
    override val value = delegate
}
