package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertFailsWith

class MemberDereferenceTest : APLTest() {
    @Test
    fun memberDereferenceMap() {
        parseAPLExpression("foo ← map 'test 1 'abc 2\nfoo.test").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun memberDereferenceMapExpression() {
        parseAPLExpression("foo ← map 'test 1 'abc 2\nfoo.('test)").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun memberDereferenceMapConstantInvalidValue() {
        assertFailsWith<KeyNotFoundException> {
            parseAPLExpression("foo ← map 'test 1 'abc 2\nfoo.bar")
        }
    }

    @Test
    fun memberDereferenceMapExpressionInvalidValue() {
        assertFailsWith<KeyNotFoundException> {
            parseAPLExpression("foo ← map 'test 1 'abc 2\nfoo.('bar)")
        }
    }

    @Test
    fun memberDereferenceMissingKeyShouldThrow() {
        assertFailsWith<APLEvalException> {
            parseAPLExpression("foo ← 1 2\nfoo.bar")
        }
    }

    @Test
    fun memberDereferenceFromExpression() {
        parseAPLExpression("findMap ⇐ { map 'foo 10 'bar 20 }\n(findMap 0).foo").let { result ->
            assertSimpleNumber(10, result)
        }
    }

    @Test
    fun memberDereferenceFromExpressionString() {
        parseAPLExpression("foo ← map \"test\" 1 \"abc\" 2\nfoo.(\"test\")").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun memberDereferenceWithNoLeftArg() {
        assertFailsWith<ParseException> {
            parseAPLExpression(".foo")
        }
    }

    @Test
    fun memberDerefenceWithExpression() {
        parseAPLExpression("a ← map 10 \"abc\" 20 \"def\" 30 \"ghi\"\na.(2+8)").let { result ->
            assertString("abc", result)
        }
    }

    @Test
    fun memberDereferenceSequence() {
        parseAPLExpression("a ← map :a 1 :b (map :c (map :d 10 :e 20) :f 30 :g 40) :h 50\na.:b.:c.:d").let { result ->
            assertSimpleNumber(10, result)
        }
    }

    @Test
    fun memberDereferenceSequenceMissingElement() {
        assertFailsWith<KeyNotFoundException> {
            parseAPLExpression("a ← map :a 1 :b (map :c (map :d 10 :e 20) :f 30 :g 40) :h 50\na.:b.:c.:abc")
        }
    }

    @Test
    fun memberDereferenceSequenceMissingFirstElement() {
        assertFailsWith<KeyNotFoundException> {
            parseAPLExpression("a ← map :a 1 :b (map :c (map :d 10 :e 20) :f 30 :g 40) :h 50\na.:x.:c.:d")
        }
    }

    @Test
    fun memberDereferenceSequenceExpressionElement() {
        parseAPLExpression("a ← map 10 (map 100 200 300 400) 20 30\na.(10).(300)").let { result ->
            assertSimpleNumber(400, result)
        }
    }

    @Test
    fun memberDereferenceWithExplicitIntegerKeyShouldFail() {
        assertFailsWith<ParseException> {
            parseAPLExpression("a ← map 10 (map 100 200 300 400) 20 30\na.20")
        }
    }

    @Test
    fun memberDereferenceWithExplicitFunctionNameKey() {
        parseAPLExpression("a ← map 'map 10 'math:sin 20\na.map").let { result ->
            assertSimpleNumber(10, result)
        }
        parseAPLExpression("a ← map 'map 10 'math:sin 20\na.math:sin").let { result ->
            assertSimpleNumber(20, result)
        }
    }

    @Test
    fun memberDerferenceWithStranding() {
        parseAPLExpression("a ← map 'foo 10 'bar 20\na.foo a.bar").let { result ->
            assert1DArray(arrayOf(10, 20), result)
        }
    }

    @Test
    fun arrayDereferenceSimple() {
        parseAPLExpression("(10 20 30).(0)").let { result ->
            assertSimpleNumber(10, result)
        }
    }

    @Test
    fun multiDimensionalArrayDereference() {
        parseAPLExpression("(3 4 ⍴ ⍳12).(2 3)").let { result ->
            assertSimpleNumber(11, result)
        }
    }

    @Test
    fun arrayDereferenceNegativeIndex() {
        parseAPLExpression("a←(⍕¨100+⍳10) ⋄ {a.(⍵)}¨ (⍳10)-10").let { result ->
            assertDimension(dimensionsOfSize(10), result)
            for (i in 0 until 10) {
                assertString((i + 100).toString(), result.valueAt(i), "String at index ${i}")
            }
        }
    }

    @Test
    fun multiDimensionalArrayDereferenceNegativeIndex() {
        parseAPLExpression("(5 4 ⍴ ⍳20).(¯2 2)").let { result ->
            assertSimpleNumber(14, result)
        }
    }

    @Test
    fun recursiveArrayIndexDereference() {
        parseAPLExpression("((10 (20 30) 40 50) 60).(0).(1).(1)").let { result ->
            assertSimpleNumber(30, result)
        }
    }

    @Test
    fun recursiveDereferenceMixedTypes() {
        parseAPLExpression("(10 (map 'foo (30 40 50) 'bar (60 70 80)) 20).(1).foo.(2)").let { result ->
            assertSimpleNumber(50, result)
        }
    }

    @Test
    fun recursiveDereferenceWithMultipleDimensionsAndMixedTypes() {
        parseAPLExpression("(10 (map 'foo (2 3 ⍴ 30+10×⍳6) 'bar (60 70 80)) 20).(1).foo.(1 2)").let { result ->
            assertSimpleNumber(80, result)
        }
    }

    @Test
    fun recursiveDereferenceWithMultipleDimensionsAndMixedTypesAndComputedValues() {
        parseAPLExpression("(10 (map 'foo (2 3 ⍴ 30+10×⍳6) 'bar (60 70 80)) 20).(1).foo.(1+⍳2)").let { result ->
            assertSimpleNumber(80, result)
        }
    }

    @Test
    fun recursiveMixedTypesWithIntegerKeys() {
        parseAPLExpression("(10 20 30 (map 100 (\"abc\" \"def\") 200 300) 40).(3).(100).(1)").let { result ->
            assertString("def", result)
        }
    }

    @Test
    fun indexDereferenceIndexOutOfRange0() {
        assertFailsWith<APLIndexOutOfBoundsException> {
            parseAPLExpression("(10 20 30).(3)")
        }
    }

    @Test
    fun indexDereferenceIndexOutOfRange1() {
        assertFailsWith<APLIndexOutOfBoundsException> {
            parseAPLExpression("(10 20 30).(¯4)")
        }
    }

    @Test
    fun indexDereferenceIndexOutOfRange2() {
        assertFailsWith<APLIndexOutOfBoundsException> {
            parseAPLExpression("(3 3 ⍴ 10×⍳9).(2 3)")
        }
    }

    @Test
    fun indexDereferenceIndexOutOfRange3() {
        assertFailsWith<APLIndexOutOfBoundsException> {
            parseAPLExpression("(3 3 ⍴ 10×⍳9).(2 ¯4)")
        }
    }

    @Test
    fun indexDereferenceInvalidDimensions0() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("(3 3 ⍴ 10×⍳9).(0 0 0)")
        }
    }

    @Test
    fun indexDereferenceInvalidDimensions1() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("(3 3 ⍴ 10×⍳9).(0)")
        }
    }

    @Test
    fun indexDereferenceInvalidDimensions2() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("(10×⍳9).(0 0)")
        }
    }

    @Test
    fun indexDereferenceInvalidDimensionsAndInvalidIndex() {
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression("(3 3 ⍴ 10×⍳9).(0 3 0)")
        }
    }

    @Test
    fun indexDereferenceFromScalar() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("(123).(0)")
        }
    }

    @Test
    fun listDereference0() {
        parseAPLExpression("(10;20;30).(0)").let { result ->
            assertSimpleNumber(10, result)
        }
    }

    @Test
    fun listDereference1() {
        parseAPLExpression("(10;\"foo\";30).(1)").let { result ->
            assertString("foo", result)
        }
    }

    @Test
    fun listDereferenceIndexOutOfBoundsIndexTooLarge() {
        assertFailsWith<ListOutOfBounds> {
            parseAPLExpression("(10;\"foo\";30).(3)")
        }
    }

    @Test
    fun listDereferenceIndexOutOfBoundsIndexNegative() {
        assertFailsWith<ListOutOfBounds> {
            parseAPLExpression("(10;\"foo\";30).(¯1)")
        }
    }

    @Test
    fun arrayDereferenceFromSymbolShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("(1 2 3).foo")
        }
    }

    @Test
    fun listDereferenceFromLSymbolShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("(1;2;3).foo")
        }
    }

    @Test
    fun arrayDereferenceFromStringShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("(1 2 3).(\"foo\")")
        }
    }

    @Test
    fun listDereferenceFromLStringShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("(1;2;3).(\"foo\")")
        }
    }

    @Test
    fun scalarDereferenceFromIntegerShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("(1).(0)")
        }
    }

    @Test
    fun scalarDereferenceFromSymbolShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("(1).foo")
        }
    }

    @Test
    fun scalarDereferenceFromStringShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("(1).(\"foo\")")
        }
    }

    @Test
    fun listDereferenceFromComplexShouldFail() {
        assertFailsWith<NumberComplexException> {
            parseAPLExpression("(10;\"foo\";30).(1j1)")
        }
    }

    @Test
    fun arrayDereferenceFromFloat() {
        parseAPLExpression("(10 20 30).(1.0)").let { result ->
            assertSimpleNumber(20, result)
        }
    }

    @Test
    fun listDereferenceFromFloat() {
        parseAPLExpression("(10;20;30).(1.0)").let { result ->
            assertSimpleNumber(20, result)
        }
    }

    @Test
    fun dereferenceMixedSymbolArrayList0() {
        parseAPLExpression("(map 'foo (10 20 (30;40;\"abc\")) 'bar 10).foo.(2).(0)").let { result ->
            assertSimpleNumber(30, result)
        }
    }

    @Test
    fun dereferenceMixedSymbolArrayList1() {
        parseAPLExpression("(map 'foo (10 (30;40;\"abc\") 20) 'bar 10).foo.(1).(2)").let { result ->
            assertString("abc", result)
        }
    }

    @Test
    fun dereferenceLabelledArray() {
        parseAPLExpression("(\"foo\" \"bar\" labels 10 20).(0)").let { result ->
            assertSimpleNumber(10, result)
        }
    }
}
