package com.dhsdevelopments.kap.json

import com.dhsdevelopments.kap.*

actual val backendSupportsJson = true

actual fun parseJsonToAPL(input: CharacterProvider): APLValue {
    val buf = StringBuilder()
    input.lines().forEach { line ->
        buf.append(line)
        buf.append("\n")
    }
    val json = try {
        JSON.parse<Any>(buf.toString())
    } catch (e: Throwable) {
        val formatted = buildString {
            append("Error parsing JSON")
            val message = e.message
            if (message != null) {
                append(": ")
                append(message)
            }
        }
        throw JsonParseException(formatted)
    }
    return parseEntry(json)
}

private fun parseEntry(value: Any?): APLValue {
    return when (value) {
        null -> APLNilValue
        is Array<*> -> parseArray(value)
        is String -> APLString.make(value)
        is Double -> value.makeAPLNumber()
        is Boolean -> if (value) APLLONG_1 else APLLONG_0
        else -> parseObject(value.asDynamic())
    }
}

private fun parseArray(value: Array<*>): APLValue {
    val content = ArrayList<APLValue>()
    value.forEach { m ->
        content.add(parseEntry(m))
    }
    return APLArrayList(dimensionsOfSize(content.size), content)
}

private fun parseObject(value: dynamic): APLValue {
    val content = ArrayList<Pair<APLValue.APLValueKey, APLValue>>()
    val keysFn = js("Object.keys")
    val keyArray = keysFn(value)
    keyArray.forEach { k ->
        if (k !is String) {
            throw JsonParseException("Key is not a string: ${k}")
        }
        content.add(APLString.make(k.unsafeCast<String>()).makeTypeQualifiedKey() to parseEntry(value[k]))
    }
    return APLMap(ImmutableMap2.makeFromContent(content))
}
