package com.dhsdevelopments.kap

import kotlin.test.*

class NilTest : APLTest() {
    @Test
    fun nilBooleanValue() {
        parseAPLExpression("null and 5").let { result ->
            assertSimpleNumber(5, result)
        }
        parseAPLExpression("null or 5").let { result ->
            assertIs<APLNilValue>(result)
        }
    }

    @Test
    fun nilDimensions() {
        parseAPLExpression("⍴ null").let { result ->
            assertAPLNull(result)
        }
    }

    @Test
    fun nilType() {
        parseAPLExpression2("typeof null").let { (result, engine) ->
            val sym = engine.coreNamespace.findSymbol("null", includePrivate = true)
            assertNotNull(sym)
            assertIs<APLSymbol>(result)
            assertSame(sym, result.value)
        }
    }

    @Test
    fun compareNil() {
        assertSimpleNumber(1, parseAPLExpression("null≡null"))
        assertSimpleNumber(0, parseAPLExpression("1≡null"))
        assertSimpleNumber(0, parseAPLExpression("(0⍴0)≡null"))
        assertSimpleNumber(0, parseAPLExpression("(1 1)≡null"))
    }

    @Test
    fun dimensionsNil() {
        parseAPLExpression("⍴ null").let { result ->
            assertDimension(emptyListDimensions(), result)
        }
    }

    @Test
    fun nilKeyInMap() {
        parseAPLExpression("a ← map null 1 :abc 2 ⋄ a mapGet null").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun mathsWithNilShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression2("null - 8")
        }
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression2("null ÷ 8")
        }
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression2("null - null")
        }
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression2("null ÷ null")
        }
    }

    @Test
    fun scalarFunctionsWithNull() {
        assertSimpleNumber(0, parseAPLExpression("null + null"))
        assertSimpleNumber(1, parseAPLExpression("null × null"))
    }

    @Test
    fun addWithNull() {
        parseAPLExpression("⦻ 2 + 1 ⦻").let { result ->
            assert1DArray(arrayOf(1, 2), result)
        }
    }

    @Test
    fun mulWithNull() {
        parseAPLExpression("⦻ 3 × 4 ⦻").let { result ->
            assert1DArray(arrayOf(4, 3), result)
        }
    }

    @Test
    fun subtractNull() {
        parseAPLExpression("2 3 - ⦻").let { result ->
            assert1DArray(arrayOf(2, 3), result)
        }
    }

    @Test
    fun divideNull() {
        parseAPLExpression("2 3 ÷ ⦻").let { result ->
            assert1DArray(arrayOf(2, 3), result)
        }
    }

    @Test
    fun addCharAndNull() {
        parseAPLExpression("\"ab\" + null").let { result ->
            assertString("ab", result)
        }
    }

    @Test
    fun subCharAndNull() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("\"ab\" - null")
        }
    }

    @Test
    fun minWithNil() {
        parseAPLExpression("⦻ 2 ⦻ ¯1234⌊10 ⦻ ¯12 ⦻").let { result ->
            assert1DArray(arrayOf(10, 2, -12, -1234), result)
        }
    }

    @Test
    fun maxWithNil() {
        parseAPLExpression("⦻ 1 ⦻ ¯9⌈4 ⦻ ¯44 ⦻").let { result ->
            assert1DArray(arrayOf(4, 1, -44, -9), result)
        }
    }

    @Test
    fun minMaxWithNilShouldFail() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("null⌊null")
        }
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("null⌈null")
        }
    }
}
