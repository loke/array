package com.dhsdevelopments.kap.clientweb2

import kotlinx.browser.document
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.w3c.dom.Worker

object RequestIdGenerator {
    private var currentValue = 0L

    fun nextValue(): String {
        val v = currentValue++
        return v.toString()
    }
}

class CommandDescriptor(val requestId: String, val text: String)

fun sendCommand(worker: Worker, command: CommandDescriptor) {
    val rendererSelectorValue: dynamic = document.getElementById("experimental-render")
    worker.postMessage(
        Json.encodeToString(
            EvalRequest(
                command.requestId,
                command.text,
                if (rendererSelectorValue.checked) ResultType.JS else ResultType.FORMATTED_PRETTY) as Request))
}
