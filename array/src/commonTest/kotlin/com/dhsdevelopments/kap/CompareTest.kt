package com.dhsdevelopments.kap

import kotlin.test.Test

class CompareTest : APLTest() {
    @Test
    fun testComparators() {
        testFunction(arrayOf(1, 0, 0, 1, 0, 0), "<")
        testFunction(arrayOf(0, 1, 0, 0, 1, 0), ">")
        testFunction(arrayOf(1, 0, 1, 1, 0, 1), "≤")
        testFunction(arrayOf(0, 1, 1, 0, 1, 1), "≥")
        testFunction(arrayOf(0, 0, 1, 0, 0, 1), "=")
        testFunction(arrayOf(1, 1, 0, 1, 1, 0), "≠")
    }

    private fun testFunction(expected: Array<Long>, name: String) {
        assertSimpleNumber(expected[0], parseAPLExpression("1${name}2"))
        assertSimpleNumber(expected[0], parseAPLExpression("100000000000000000000000000${name}200000000000000000000000000"))
        assertSimpleNumber(expected[0], parseAPLExpression("(1÷3)${name}(2÷3)"))
        assertSimpleNumber(expected[1], parseAPLExpression("2${name}1"))
        assertSimpleNumber(expected[1], parseAPLExpression("200000000000000000000000000${name}100000000000000000000000000"))
        assertSimpleNumber(expected[1], parseAPLExpression("(2÷3)${name}(1÷3)"))
        assertSimpleNumber(expected[2], parseAPLExpression("2${name}2"))
        assertSimpleNumber(expected[2], parseAPLExpression("200000000000000000000000000${name}200000000000000000000000000"))
        assertSimpleNumber(expected[2], parseAPLExpression("(2÷3)${name}(2÷3)"))
        assertSimpleNumber(expected[2], parseAPLExpression("0.0${name}0.0"))
        assertSimpleNumber(expected[3], parseAPLExpression("0${name}1"))
        assertSimpleNumber(expected[4], parseAPLExpression("1${name}0"))
        assertSimpleNumber(expected[4], parseAPLExpression("1${name}0"))
        assertSimpleNumber(expected[5], parseAPLExpression("0${name}0"))
    }

    @Test
    fun testComparatorsInexact() {
        testFunctionsInexact(arrayOf(0, 0, 1, 1), "=")
        testFunctionsInexact(arrayOf(1, 1, 0, 0), "≠")
        testFunctionsInexact(arrayOf(0, 1, 0, 0), "<")
        testFunctionsInexact(arrayOf(1, 0, 0, 0), ">")
        testFunctionsInexact(arrayOf(0, 1, 1, 1), "≤")
        testFunctionsInexact(arrayOf(1, 0, 1, 1), "≥")
    }

    private fun testFunctionsInexact(expected: Array<Long>, name: String) {
        assertSimpleNumber(expected[0], parseAPLExpression("(1÷3)${name}1.0÷3.0"), "${name}, rational to double inexact")
        assertSimpleNumber(expected[1], parseAPLExpression("(1.0÷3.0)${name}1÷3"), "${name}, double to rational inexact")
        assertSimpleNumber(expected[2], parseAPLExpression("(1÷2)${name}1.0÷2.0"), "${name}, rational to double exact")
        assertSimpleNumber(expected[3], parseAPLExpression("(1.0÷2.0)${name}1÷2"), "${name}, double to rational exact")
    }

    @Test
    fun compareNumbersWithAllTypes() {
        fun compareFunction(name: String, expected: Array<Long>) {
            for (value in listOf("5", "5.0", "5.1", "5j0", "5.1j0", "(int:asBigint 5)", "(9÷2)")) {
                parseAPLExpression("${value} ${name} 6 6.0 6.1 6.1j0 (int:asBigint 1000) (1000000÷3) 1 1.0 1.1 1.1j0 (int:asBigint 1) (10÷3)").let { result ->
                    assert1DArray(expected, result)
                }
            }
        }
        compareFunction("=", arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
        compareFunction("≠", arrayOf(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1))
        compareFunction("<", arrayOf(1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0))
        compareFunction(">", arrayOf(0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1))
        compareFunction("≤", arrayOf(1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0))
        compareFunction("≥", arrayOf(0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1))
    }

    @Test
    fun comparePositiveInfinity0() {
        parseAPLExpression("(1.0÷0.0) > 3 3.0 3.1 2j0 10000000000000000000000000000000000000000000000000 (1000000000000000000000000000000000000000000000000÷3)").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 1, 1, 1), result)
        }

        parseAPLExpression("(1.0÷0.0) < 3 3.0 3.1 2j0 10000000000000000000000000000000000000000000000000 (1000000000000000000000000000000000000000000000000÷3)").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0, 0, 0), result)
        }
    }

    @Test
    fun comparePositiveInfinity1() {
        parseAPLExpression("3 3.0 3.1 2j0 10000000000000000000000000000000000000000000000000 (1000000000000000000000000000000000000000000000000÷3) > (1.0÷0.0)").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0, 0, 0), result)
        }
        parseAPLExpression("3 3.0 3.1 2j0 10000000000000000000000000000000000000000000000000 (1000000000000000000000000000000000000000000000000÷3) < (1.0÷0.0)").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 1, 1, 1), result)
        }
    }

    @Test
    fun compareNegativeInfinity0() {
        parseAPLExpression("(- 1.0÷0.0) > 3 3.0 3.1 2j0 10000000000000000000000000000000000000000000000000 (1000000000000000000000000000000000000000000000000÷3)").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0, 0, 0), result)
        }
        parseAPLExpression("(- 1.0÷0.0) < 3 3.0 3.1 2j0 10000000000000000000000000000000000000000000000000 (1000000000000000000000000000000000000000000000000÷3)").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 1, 1, 1), result)
        }
    }

    @Test
    fun compareNegativeInfinity1() {
        parseAPLExpression("3 3.0 3.1 2j0 10000000000000000000000000000000000000000000000000 (1000000000000000000000000000000000000000000000000÷3) > (- 1.0÷0.0)").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 1, 1, 1), result)
        }
        parseAPLExpression("3 3.0 3.1 2j0 10000000000000000000000000000000000000000000000000 (1000000000000000000000000000000000000000000000000÷3) < (- 1.0÷0.0)").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0, 0, 0), result)
        }
    }

    @Test
    fun compareEqualsInfinity() {
        fun testEqualsAndSame(a: String, b: String, expected: Long) {
            assertSimpleNumber(expected, parseAPLExpression("${a}=${b}"))
            assertSimpleNumber(1 - expected, parseAPLExpression("${a}≠${b}"))
        }
        testEqualsAndSame("(1.0÷0.0)", "(1.0÷0.0)", 1)
        testEqualsAndSame("(¯1.0÷0.0)", "(¯1.0÷0.0)", 1)
        testEqualsAndSame("(¯1.0÷0.0)", "(1.0÷0.0)", 0)
    }

    @Test
    fun compareSameInfinity() {
        fun testEqualsAndSame(a: String, b: String, expected: Long) {
            assertSimpleNumber(expected, parseAPLExpression("${a}≡${b}"))
            assertSimpleNumber(1 - expected, parseAPLExpression("${a}≢${b}"))
        }
        testEqualsAndSame("(1.0÷0.0)", "(1.0÷0.0)", 1)
        testEqualsAndSame("(¯1.0÷0.0)", "(¯1.0÷0.0)", 1)
        testEqualsAndSame("(¯1.0÷0.0)", "(1.0÷0.0)", 0)
    }

    @Test
    fun compareEqualsChar() {
        parseAPLExpression("@a @a 1 @a @a = 1 @b @b @a 1.1").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 1, 0), result)
        }
        parseAPLExpression("@a @a 1 @a @a ≠ 1 @b @b @a 1.1").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 0, 1), result)
        }
    }

    @Test
    fun compareLessGreaterChar() {
        parseAPLExpression("@e @b @c < @a @b @f").let { result ->
            assert1DArray(arrayOf(0, 0, 1), result)
        }
        parseAPLExpression("@e @b @c > @a @b @f").let { result ->
            assert1DArray(arrayOf(1, 0, 0), result)
        }
        parseAPLExpression("@e @b @c ≤ @a @b @f").let { result ->
            assert1DArray(arrayOf(0, 1, 1), result)
        }
        parseAPLExpression("@e @b @c ≥ @a @b @f").let { result ->
            assert1DArray(arrayOf(1, 1, 0), result)
        }
    }

    @Test
    fun compareEqualsLists() {
        parseAPLExpression("(1;2) (1;2) (2;1;3) (1;2) 3 = (2;1) (1;2) (2;1) 3 (1;2)").let { result ->
            assert1DArray(arrayOf(0, 1, 0, 0, 0), result)
        }
        parseAPLExpression("(1;2) (1;2) (2;1;3) (1;2) 3 ≠ (2;1) (1;2) (2;1) 3 (1;2)").let { result ->
            assert1DArray(arrayOf(1, 0, 1, 1, 1), result)
        }
    }

    @Test
    fun testIdenticalSimple() {
        assertSimpleNumber(1, parseAPLExpression("10≡10"))
        assertSimpleNumber(0, parseAPLExpression("10≡100"))
        assertSimpleNumber(1, parseAPLExpression("10 11≡10 11"))
        assertSimpleNumber(1, parseAPLExpression("11 12 13 14 15 16 17≡10+1 2 3 4 5 6 7"))
        assertSimpleNumber(0, parseAPLExpression("1 2 3≡1 2"))
        assertSimpleNumber(1, parseAPLExpression("(1 2) (3 4 (5 6)) ≡ (1 2) (3 4 (5 6))"))
        assertSimpleNumber(0, parseAPLExpression("(1 2) (3 4 (5 6)) ≡ (1 2) (3 4 (@a 6))"))
        assertSimpleNumber(1, parseAPLExpression("@a ≡ @a"))
        assertSimpleNumber(1, parseAPLExpression("1.2 ≡ 1.2"))
        assertSimpleNumber(0, parseAPLExpression("2J4 ≡ 2J3"))
        assertSimpleNumber(0, parseAPLExpression("(2⋆1000) ≡ 2.0"))
        assertSimpleNumber(0, parseAPLExpression("2.0 ≡ (2⋆1000)"))
        assertSimpleNumber(0, parseAPLExpression("(2⋆1000) ≡ 2j5"))
        assertSimpleNumber(0, parseAPLExpression("2j5 ≡ (2⋆1000)"))
        assertSimpleNumber(0, parseAPLExpression("(2⋆600) ≡ (2⋆1000)"))
        assertSimpleNumber(1, parseAPLExpression("(2⋆1000) ≡ (2⋆1000)"))
        assertSimpleNumber(0, parseAPLExpression("(2⋆1000) ≡ (÷10)"))
        assertSimpleNumber(0, parseAPLExpression("(÷10) ≡ (2⋆1000)"))
    }

    @Test
    fun testIdenticalLazyValueLong() {
        assertSimpleNumber(1, parseAPLExpression("0≡0⌷0 1"))
    }

    @Test
    fun testIdenticalLazyValueDouble() {
        assertSimpleNumber(1, parseAPLExpression("0.0≡0⌷0.0 1.0"))
    }

    @Test
    fun testIdenticalLazyValueComplex() {
        assertSimpleNumber(1, parseAPLExpression("3J1≡0⌷3J1 4J1"))
    }

    @Test
    fun testIdenticalLazyValueChar() {
        assertSimpleNumber(1, parseAPLExpression("@a≡0⌷@a @b"))
    }

    @Test
    fun testNotIdentical() {
        assertSimpleNumber(0, parseAPLExpression("10≢10"))
        assertSimpleNumber(1, parseAPLExpression("10≢100"))
        assertSimpleNumber(0, parseAPLExpression("10 11≢4+6 7"))
        assertSimpleNumber(0, parseAPLExpression("11 12 13 14 15 16 17≢10+1 2 3 4 5 6 7"))
        assertSimpleNumber(1, parseAPLExpression("1 2 3≢1 2"))
        assertSimpleNumber(0, parseAPLExpression("\"foo\"≢\"foo\""))
        assertSimpleNumber(1, parseAPLExpression("(2 4 ⍴ 1 2 3 4 5 6 7 8) ≢ (4 2 ⍴ 1 2 3 4 5 6 7 8)"))
        assertSimpleNumber(1, parseAPLExpression("2 ≢ 1⍴2"))
        assertSimpleNumber(0, parseAPLExpression("'foo ≢ 'foo"))
        assertSimpleNumber(1, parseAPLExpression("@a ≢ @b"))
        assertSimpleNumber(0, parseAPLExpression("(1;2;3) ≢ (1;2;3)"))
        assertSimpleNumber(1, parseAPLExpression("(2⋆1000) ≢ 2.0"))
        assertSimpleNumber(1, parseAPLExpression("2.0 ≢ (2⋆1000)"))
        assertSimpleNumber(1, parseAPLExpression("(2⋆1000) ≢ 2j5"))
        assertSimpleNumber(1, parseAPLExpression("2j5 ≢ (2⋆1000)"))
        assertSimpleNumber(1, parseAPLExpression("(2⋆600) ≢ (2⋆1000)"))
        assertSimpleNumber(0, parseAPLExpression("(2⋆1000) ≢ (2⋆1000)"))
        assertSimpleNumber(1, parseAPLExpression("(2⋆1000) ≢ (÷10)"))
        assertSimpleNumber(1, parseAPLExpression("(÷10) ≢ (2⋆1000)"))
    }

    @Test
    fun testNotIdenticalLazyValueLong() {
        assertSimpleNumber(0, parseAPLExpression("0≢0⌷0 1"))
    }

    @Test
    fun testNotIdenticalLazyValueDouble() {
        assertSimpleNumber(0, parseAPLExpression("0.0≢0⌷0.0 1.0"))
    }

    @Test
    fun testNotIdenticalLazyValueComplex() {
        assertSimpleNumber(0, parseAPLExpression("3J1≢0⌷3J1 4J1"))
    }

    @Test
    fun testNotIdenticalLazyValueChar() {
        assertSimpleNumber(0, parseAPLExpression("@a≢0⌷@a @b"))
    }

    @Test
    fun compareEqualsNonNumeric() {
        assertSimpleNumber(1, parseAPLExpression("@a = @a"))
        assertSimpleNumber(0, parseAPLExpression("@a = @b"))
        assertSimpleNumber(1, parseAPLExpression("('foo) = 'foo"))
        assertSimpleNumber(0, parseAPLExpression("('foo) = 'bar"))
    }

    @Test
    fun compareNotEqualsNonNumeric() {
        assertSimpleNumber(1, parseAPLExpression("'foo ≠ 'foox"))
        assertSimpleNumber(0, parseAPLExpression("'foo ≠ 'foo"))
        assertSimpleNumber(0, parseAPLExpression("@b ≠ @b"))
        assertSimpleNumber(1, parseAPLExpression("@a ≠ @b"))
        assertSimpleNumber(0, parseAPLExpression("(1;2;3) ≠ (1;2;3)"))
        assertSimpleNumber(1, parseAPLExpression("(1;2;3) ≠ (1;2;3;4)"))
    }

    @Test
    fun oneArgumentNotIdenticalTest() {
        assertSimpleNumber(0, parseAPLExpression("≢0⍴0"))
        assertSimpleNumber(1, parseAPLExpression("≢4"))
        assertSimpleNumber(4, parseAPLExpression("≢1 2 3 4"))
        assertSimpleNumber(1, parseAPLExpression("≢,4"))
        assertSimpleNumber(2, parseAPLExpression("≢2 3 ⍴ ⍳100"))
        assertSimpleNumber(8, parseAPLExpression("≢8 3 4 ⍴ ⍳100"))
        assertSimpleNumber(2, parseAPLExpression("≢(2 2 ⍴ ⍳4) (2 2 ⍴ ⍳4)"))
        assertSimpleNumber(5, parseAPLExpression("≢(1 2) (3 4) (5 6) (7 8) (9 10)"))
    }

    @Test
    fun oneArgumentIdenticalTest0() {
        assertSimpleNumber(1, parseAPLExpression("≡1 2 3"))
        assertSimpleNumber(0, parseAPLExpression("≡1"))
        assertSimpleNumber(2, parseAPLExpression("≡(1 2 3) (4 5 6)"))
        assertSimpleNumber(2, parseAPLExpression("≡(1 2 3) (10 11)"))
        assertSimpleNumber(2, parseAPLExpression("≡(1 2 3) (4 5 6) (100 200) (3000 4000)"))
        assertSimpleNumber(3, parseAPLExpression("≡(1 2 3) (4 5 6 (10 11))"))
        assertSimpleNumber(3, parseAPLExpression("≡((1 2) (3 4)) ((10 20) (30 40))"))
        assertSimpleNumber(1, parseAPLExpression("≡\"foo\""))
        assertSimpleNumber(2, parseAPLExpression("≡(1 2 ⍬)"))
    }

    @Test
    fun oneArgumentIdenticalWithHashMap() {
        val result = parseAPLExpression(
            """
            |a ← map 2 2 ⍴ 1 2 3 4
            |≡a
            """.trimMargin())
        assertSimpleNumber(0, result)
    }

    @Test
    fun oneArgumentIdenticalWithComplex() {
        assertSimpleNumber(0, parseAPLExpression("≡1J3"))
    }

    @Test
    fun oneArgumentIdenticalWithEnclosedArg0() {
        assertSimpleNumber(2, parseAPLExpression("≡⊂1 2 3"))
    }

    @Test
    fun oneArgumentIdenticalWithEnclosedArg1() {
        assertSimpleNumber(3, parseAPLExpression("≡⊂(1 2 3) (4 5 6)"))
    }

    @Test
    fun oneArgumentIdenticalWithPlusReduceEach() {
        // This test ensures that the for/each result object computes depth properly
        parseAPLExpression("≡ +/¨ ⍳4 5 6").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun oneArgumentIdenticalWithPlusReduceEachExplicitCollapse() {
        parseAPLExpression("≡ comp +/¨ ⍳4 5 6").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun compareIntegers() {
        assertSimpleNumber(1, parseAPLExpression("3 cmp 1"))
        assertSimpleNumber(-1, parseAPLExpression("10 cmp 110"))
        assertSimpleNumber(0, parseAPLExpression("1 cmp 1"))
        assertSimpleNumber(0, parseAPLExpression("¯3 cmp ¯3"))
    }

    @Test
    fun compareDouble() {
        assertSimpleNumber(1, parseAPLExpression("3.0 cmp 1.0"))
        assertSimpleNumber(-1, parseAPLExpression("10.0 cmp 110.0"))
        assertSimpleNumber(0, parseAPLExpression("1.0 cmp 1.0"))
        assertSimpleNumber(0, parseAPLExpression("¯3.0 cmp ¯3.0"))
        assertSimpleNumber(0, parseAPLExpression("(1.0÷0.0) cmp (1.0÷0.0)"))
        assertSimpleNumber(0, parseAPLExpression("(¯1.0÷0.0) cmp (¯1.0÷0.0)"))
        assertSimpleNumber(-1, parseAPLExpression("4.0 cmp (1.0÷0.0)"))
    }

    @Test
    fun compareIntWithDouble() {
        assertSimpleNumber(-1, parseAPLExpression("2.0 cmp 4"))
        assertSimpleNumber(-1, parseAPLExpression("2 cmp 4.0"))
        assertSimpleNumber(1, parseAPLExpression("9.0 cmp 1"))
        assertSimpleNumber(1, parseAPLExpression("9 cmp 1.0"))
        assertSimpleNumber(1, parseAPLExpression("9 cmp 9.0"))
        assertSimpleNumber(-1, parseAPLExpression("9.0 cmp 9"))
        assertSimpleNumber(-1, parseAPLExpression("4 cmp (1.0÷0.0)"))
        assertSimpleNumber(1, parseAPLExpression("(1.0÷0.0) cmp 4"))
    }

    @Test
    fun compareChars() {
        assertSimpleNumber(-1, parseAPLExpression("@a cmp @b"))
        assertSimpleNumber(1, parseAPLExpression("@b cmp @a"))
        assertSimpleNumber(0, parseAPLExpression("@b cmp @b"))
        assertSimpleNumber(1, parseAPLExpression("@a cmp @A"))
        assertSimpleNumber(0, parseAPLExpression("@a cmp @a"))
    }

    @Test
    fun compareStrings() {
        assertSimpleNumber(-1, parseAPLExpression("\"abc\" cmp \"bcd\""))
        assertSimpleNumber(-1, parseAPLExpression("\"ABC\" cmp \"abc\""))
        assertSimpleNumber(0, parseAPLExpression("⍬ cmp ⍬"))
        assertSimpleNumber(0, parseAPLExpression("\"a\" cmp \"a\""))
        assertSimpleNumber(-1, parseAPLExpression("\"abc\" cmp \"abcc\""))
    }

    @Test
    fun compareNestedStrings() {
        assertSimpleNumber(1, parseAPLExpression("\"abc\" \"def\" cmp \"abc\" \"cde\""))
        assertSimpleNumber(-1, parseAPLExpression("\"abc\" \"def\" cmp \"abc\" \"ghi\""))
    }

    @Test
    fun compareIntAndChar() {
        assertSimpleNumber(-1, parseAPLExpression("1 cmp @c"))
        assertSimpleNumber(1, parseAPLExpression("@c cmp 1"))
    }

    @Test
    fun compareIntAndComplex() {
        assertSimpleNumber(-1, parseAPLExpression("1 cmp 2J1"))
        assertSimpleNumber(-1, parseAPLExpression("3 cmp 2J1"))
        assertSimpleNumber(1, parseAPLExpression("2J1 cmp 3"))
        assertSimpleNumber(1, parseAPLExpression("3J1 cmp 3"))
        assertSimpleNumber(-1, parseAPLExpression("1J0 cmp 1"))
        assertSimpleNumber(1, parseAPLExpression("1 cmp 1J0"))
    }

    @Test
    fun compareDoubleAndComplex() {
        assertSimpleNumber(-1, parseAPLExpression("2.1 cmp 2.2J5"))
        assertSimpleNumber(-1, parseAPLExpression("3.1 cmp 2.2J5"))
        assertSimpleNumber(1, parseAPLExpression("2.2J1 cmp 2.0"))
        assertSimpleNumber(1, parseAPLExpression("1J1 cmp 2.0"))
        assertSimpleNumber(0, parseAPLExpression("2J0 cmp 2.0"))
        assertSimpleNumber(0, parseAPLExpression("2.0 cmp 2J0"))
    }

    @Test
    fun compareLists() {
        assertSimpleNumber(-1, parseAPLExpression("(1;2;3) cmp (1;2;4)"))
        assertSimpleNumber(1, parseAPLExpression("(1;2;4) cmp (1;2;3)"))
        assertSimpleNumber(0, parseAPLExpression("(1;2;3) cmp (1;2;3)"))
        assertSimpleNumber(-1, parseAPLExpression("(10;11;12) cmp (1;2;3;4)"))
        assertSimpleNumber(1, parseAPLExpression("(1;2;3;4) cmp (10;11;12)"))
    }

    @Test
    fun compareListAndNumbers() {
        assertSimpleNumber(1, parseAPLExpression("(1;2;3;4) cmp 1"))
        assertSimpleNumber(-1, parseAPLExpression("1 cmp (1;2;3;4)"))
    }

    @Test
    fun compareCharAndBigint() {
        assertSimpleNumber(-1, parseAPLExpression("100000000000000000000000000 cmp @a"))
        assertSimpleNumber(1, parseAPLExpression("@a cmp 100000000000000000000000000"))
    }

    @Test
    fun compareCharAndRational() {
        assertSimpleNumber(-1, parseAPLExpression("(1÷3) cmp @a"))
        assertSimpleNumber(1, parseAPLExpression("@a cmp (1÷3)"))
    }

    @Test
    fun compareIntAndRational() {
        assertSimpleNumber(-1, parseAPLExpression("(1÷3) cmp 1"))
        assertSimpleNumber(1, parseAPLExpression("1 cmp (1÷3)"))
    }

    @Test
    fun compareBigIntAndRational() {
        assertSimpleNumber(-1, parseAPLExpression("(100000000000000000000000001÷3) cmp 100000000000000000000000000"))
        assertSimpleNumber(1, parseAPLExpression("100000000000000000000000000 cmp (100000000000000000000000001÷3)"))
    }

    @Test
    fun compareBigInt() {
        assertSimpleNumber(-1, parseAPLExpression("100000000000000000000000000 cmp 200000000000000000000000000"))
        assertSimpleNumber(-1, parseAPLExpression("200000000000000000000000000 cmp 200000000000000000000000001"))
        assertSimpleNumber(1, parseAPLExpression("200000000000000000000000000 cmp 100000000000000000000000000"))
        assertSimpleNumber(1, parseAPLExpression("200000000000000000000000001 cmp 200000000000000000000000000"))
        assertSimpleNumber(0, parseAPLExpression("200000000000000000000000000 cmp 200000000000000000000000000"))
        assertSimpleNumber(0, parseAPLExpression("299999999999999999999999999 cmp 299999999999999999999999999"))
    }

    @Test
    fun compareRational() {
        assertSimpleNumber(-1, parseAPLExpression("(100000000000000000000000000÷3) cmp (200000000000000000000000000÷3)"))
        assertSimpleNumber(-1, parseAPLExpression("(200000000000000000000000000÷3) cmp (200000000000000000000000001÷3)"))
        assertSimpleNumber(1, parseAPLExpression("(200000000000000000000000000÷3) cmp (100000000000000000000000000÷3)"))
        assertSimpleNumber(1, parseAPLExpression("(200000000000000000000000001÷3) cmp (200000000000000000000000000÷3)"))
        assertSimpleNumber(0, parseAPLExpression("(200000000000000000000000001÷3) cmp (200000000000000000000000001÷3)"))
    }

    @Test
    fun compareBigIntToDouble() {
        assertSimpleNumber(1, parseAPLExpression("2.1 cmp (int:asBigint 2)"))
        assertSimpleNumber(-1, parseAPLExpression("(int:asBigint 2) cmp 2.1"))
        assertSimpleNumber(1, parseAPLExpression("(int:asBigint 2) cmp 2.0"))
    }

    @Test
    fun compareRationalToDouble() {
        assertSimpleNumber(1, parseAPLExpression("1.6 cmp (3÷2)"))
        assertSimpleNumber(-1, parseAPLExpression("(3÷2) cmp 1.6"))
        assertSimpleNumber(-1, parseAPLExpression("1.5 cmp (3÷2)"))
    }

    @Test
    fun compareBigIntAndChar() {
        assertSimpleNumber(-1, parseAPLExpression("100000000000000000000000000 cmp @c"))
        assertSimpleNumber(1, parseAPLExpression("@c cmp 100000000000000000000000000"))
    }

    @Test
    fun compareRationalAndChar() {
        assertSimpleNumber(-1, parseAPLExpression("(500000000000000000000000000÷3) cmp @c"))
        assertSimpleNumber(1, parseAPLExpression("@c cmp (500000000000000000000000000÷3)"))
    }

    @Test
    fun compareIntegerWithComplex() {
        assertSimpleNumber(-1, parseAPLExpression("2 cmp 4j7"))
        assertSimpleNumber(1, parseAPLExpression("4j7 cmp 2"))
    }

    @Test
    fun compareArrayWithComplex() {
        assertSimpleNumber(1, parseAPLExpression("1 2 3 4 5 6 cmp 4j7"))
        assertSimpleNumber(-1, parseAPLExpression("4j7 cmp 1 2 3 4 5 6"))
    }

    @Test
    fun compareEnclosedArrays() {
        assertSimpleNumber(-1, parseAPLExpression("(⊂1 2) cmp (⊂2 3)"))
    }

    @Test
    fun compareEnclosedWithArray() {
        assertSimpleNumber(-1, parseAPLExpression("(⊂1 2) cmp 2 3"))
    }

    @Test
    fun compareArrayWithEnclosed() {
        assertSimpleNumber(1, parseAPLExpression("2 3 cmp (⊂1 2)"))
    }

    @Test
    fun compareEnclosedArrayWithChar() {
        assertSimpleNumber(1, parseAPLExpression("(⊂1 2) cmp @a"))
        assertSimpleNumber(-1, parseAPLExpression("@a cmp (⊂1 2)"))
    }

    @Test
    fun compareBigIntArrays() {
        assertSimpleNumber(
            0,
            parseAPLExpression("100000000000000000000000000 100000000000000000000000000 cmp 100000000000000000000000000 100000000000000000000000000"))
        assertSimpleNumber(
            -1,
            parseAPLExpression("10000000000000000000000000 100000000000000000000000000 cmp 100000000000000000000000000 100000000000000000000000000"))
        assertSimpleNumber(
            1,
            parseAPLExpression("100000000000000000000000000 100000000000000000000000000 cmp 10000000000000000000000000 100000000000000000000000000"))
    }

    @Test
    fun compareMap0() {
        val src =
            """
            |a ← map :a 1 :b 10 :c 3
            |b ← map :a 1 :b 10
            |a ≡ b
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertSimpleNumber(0, result)
        }
    }

    @Test
    fun compareMap1() {
        val src =
            """
            |a ← map :a 1 :b 10
            |b ← map :b 10 :a 1
            |a ≡ b
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun compareMapWithInteger() {
        assertSimpleNumber(0, parseAPLExpression("1 ≡ (map :a 1 :b 10)"))
        assertSimpleNumber(0, parseAPLExpression("(map :a 1 :b 10) ≡ 1"))
    }

    @Test
    fun compareMapWithArray() {
        assertSimpleNumber(0, parseAPLExpression("1 2 3 4 ≡ (map :a 1 :b 10)"))
        assertSimpleNumber(0, parseAPLExpression("(map :a 1 :b 10) ≡ 1 2 3 4"))
    }

    @Test
    fun compareSameRationalToLong() {
        assertSimpleNumber(0, parseAPLExpression("(1÷2) ≡ 123"))
        assertSimpleNumber(1, parseAPLExpression("(1÷2) ≢ 123"))
    }

    @Test
    fun compareSameRationalToDouble() {
        assertSimpleNumber(1, parseAPLExpression("(1÷2) ≡ 0.5"))
        assertSimpleNumber(0, parseAPLExpression("(1÷2) ≡ 0.2"))
        assertSimpleNumber(0, parseAPLExpression("(1÷2) ≢ 0.5"))
        assertSimpleNumber(1, parseAPLExpression("(1÷2) ≢ 0.2"))
    }

    @Test
    fun compareSameRationalToDoubleNonExact() {
        assertSimpleNumber(0, parseAPLExpression("(1÷3) ≡ 1.0÷3.0"))
        assertSimpleNumber(1, parseAPLExpression("(1÷3) ≢ 1.0÷3.0"))
    }

    @Test
    fun compareSameRationalToComplex() {
        assertSimpleNumber(0, parseAPLExpression("(1÷2) ≡ 8j2"))
        assertSimpleNumber(1, parseAPLExpression("(1÷2) ≢ 8j2"))
    }

    @Test
    fun compareSameRationalToBigint() {
        assertSimpleNumber(0, parseAPLExpression("(1÷2) ≡ 10⋆40"))
        assertSimpleNumber(1, parseAPLExpression("(1÷2) ≢ 10⋆40"))
    }

    @Test
    fun compareSameRationalToRational() {
        assertSimpleNumber(1, parseAPLExpression("(1÷2) ≡ (1÷2)"))
        assertSimpleNumber(0, parseAPLExpression("(1÷2) ≡ (1÷7)"))
        assertSimpleNumber(0, parseAPLExpression("(1÷2) ≢ (1÷2)"))
        assertSimpleNumber(1, parseAPLExpression("(1÷2) ≢ (1÷7)"))
    }

    @Test
    fun compareSameRationalToNonNumber() {
        assertSimpleNumber(0, parseAPLExpression("(1÷2) ≡ (1;2)"))
        assertSimpleNumber(1, parseAPLExpression("(1÷2) ≢ (1;2)"))
    }

    @Test
    fun compareEqualsRationalToNonFiniteFloat() {
        assertSimpleNumber(0, parseAPLExpression("(1÷2)≡(1.0÷0.0)"))
        assertSimpleNumber(0, parseAPLExpression("(1.0÷0.0)≡(1÷2)"))
        assertSimpleNumber(1, parseAPLExpression("(1÷2)≢(1.0÷0.0)"))
        assertSimpleNumber(1, parseAPLExpression("(1.0÷0.0)≢(1÷2)"))
    }

    @Test
    fun compareNegativeZero() {
        assertSimpleNumber(1, parseAPLExpression("¯0.0 = 0.0"))
        assertSimpleNumber(1, parseAPLExpression("0.0 = ¯0.0"))
        assertSimpleNumber(0, parseAPLExpression("¯0.0 ≠ 0.0"))
        assertSimpleNumber(0, parseAPLExpression("0.0 ≠ ¯0.0"))
        assertSimpleNumber(0, parseAPLExpression("¯0.0 < 0.0"))
        assertSimpleNumber(0, parseAPLExpression("0.0 < ¯0.0"))
        assertSimpleNumber(0, parseAPLExpression("¯0.0 > 0.0"))
        assertSimpleNumber(0, parseAPLExpression("0.0 > ¯0.0"))
        assertSimpleNumber(1, parseAPLExpression("¯0.0 ≤ 0.0"))
        assertSimpleNumber(1, parseAPLExpression("0.0 ≤ ¯0.0"))
        assertSimpleNumber(1, parseAPLExpression("¯0.0 ≥ 0.0"))
        assertSimpleNumber(1, parseAPLExpression("0.0 ≥ ¯0.0"))
    }

    @Test
    fun compareNegativeZeroAndBigint() {
        assertSimpleNumber(1, parseAPLExpression("¯0.0 = (int:asBigint 0)"))
        assertSimpleNumber(1, parseAPLExpression("(int:asBigint 0) = ¯0.0"))
        assertSimpleNumber(0, parseAPLExpression("¯0.0 ≠ (int:asBigint 0)"))
        assertSimpleNumber(0, parseAPLExpression("(int:asBigint 0) ≠ ¯0.0"))
        assertSimpleNumber(0, parseAPLExpression("¯0.0 < (int:asBigint 0)"))
        assertSimpleNumber(0, parseAPLExpression("(int:asBigint 0) < ¯0.0"))
        assertSimpleNumber(0, parseAPLExpression("¯0.0 > (int:asBigint 0)"))
        assertSimpleNumber(0, parseAPLExpression("(int:asBigint 0) > ¯0.0"))
        assertSimpleNumber(1, parseAPLExpression("¯0.0 ≤ (int:asBigint 0)"))
        assertSimpleNumber(1, parseAPLExpression("(int:asBigint 0) ≤ ¯0.0"))
        assertSimpleNumber(1, parseAPLExpression("¯0.0 ≥ (int:asBigint 0)"))
        assertSimpleNumber(1, parseAPLExpression("(int:asBigint 0) ≥ ¯0.0"))
    }

    @Test
    fun compareNegativeZeroWithSpecialisedArray() {
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 = 0.0 ¯0.0 ¯0.0 0.0").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 1), result)
        }
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 ≠ 0.0 ¯0.0 ¯0.0 0.0").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0), result)
        }
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 < 0.0 ¯0.0 ¯0.0 0.0").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0), result)
        }
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 > 0.0 ¯0.0 ¯0.0 0.0").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0), result)
        }
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 ≤ 0.0 ¯0.0 ¯0.0 0.0").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 1), result)
        }
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 ≥ 0.0 ¯0.0 ¯0.0 0.0").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 1), result)
        }
    }

    @Test
    fun compareNegativeZeroWithGenericArray() {
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 0 = 0.0 ¯0.0 ¯0.0 0.0 0").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 1, 1), result)
        }
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 0 ≠ 0.0 ¯0.0 ¯0.0 0.0 0").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0, 0), result)
        }
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 0 < 0.0 ¯0.0 ¯0.0 0.0 0").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0, 0), result)
        }
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 0 > 0.0 ¯0.0 ¯0.0 0.0 0").let { result ->
            assert1DArray(arrayOf(0, 0, 0, 0, 0), result)
        }
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 0 ≤ 0.0 ¯0.0 ¯0.0 0.0 0").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 1, 1), result)
        }
        parseAPLExpression("¯0.0 0.0 ¯0.0 0.0 0 ≥ 0.0 ¯0.0 ¯0.0 0.0 0").let { result ->
            assert1DArray(arrayOf(1, 1, 1, 1, 1), result)
        }
    }

    @Test
    fun compareSameNegativeZero() {
        assertSimpleNumber(1, parseAPLExpression("¯0.0 ≡ 0.0"))
        assertSimpleNumber(1, parseAPLExpression("0.0 ≡ ¯0.0"))
        assertSimpleNumber(0, parseAPLExpression("¯0.0 ≢ 0.0"))
        assertSimpleNumber(0, parseAPLExpression("0.0 ≢ ¯0.0"))
    }

    @Test
    fun compareSameNegativeZeroBigint() {
        assertSimpleNumber(1, parseAPLExpression("¯0.0 ≡ int:asBigint 0"))
        assertSimpleNumber(1, parseAPLExpression("(int:asBigint 0) ≡ ¯0.0"))
        assertSimpleNumber(0, parseAPLExpression("¯0.0 ≢ (int:asBigint 0)"))
        assertSimpleNumber(0, parseAPLExpression("(int:asBigint 0) ≢ ¯0.0"))
    }

    @Test
    fun compareCmpNegativeZero() {
        assertSimpleNumber(-1, parseAPLExpression("¯0.0 cmp 0.0"))
        assertSimpleNumber(1, parseAPLExpression("0.0 cmp ¯0.0"))
        assertSimpleNumber(0, parseAPLExpression("0.0 cmp 0.0"))
        assertSimpleNumber(0, parseAPLExpression("¯0.0 cmp ¯0.0"))
    }

    @Test
    fun compareSymbols() {
        assertSimpleNumber(1, parseAPLExpression(":a = :a"))
        assertSimpleNumber(0, parseAPLExpression(":a ≠ :a"))
        assertSimpleNumber(0, parseAPLExpression(":b = :a"))
        assertSimpleNumber(1, parseAPLExpression(":b ≠ :a"))
    }

    @Test
    fun compareSameSymbols() {
        assertSimpleNumber(1, parseAPLExpression(":a ≡ :a"))
        assertSimpleNumber(0, parseAPLExpression(":a ≢ :a"))
        assertSimpleNumber(0, parseAPLExpression(":b ≡ :a"))
        assertSimpleNumber(1, parseAPLExpression(":b ≢ :a"))
    }

    @Test
    fun symbolCompareTotalOrdering() {
        assertSimpleNumber(-1, parseAPLExpression(":s cmp :x"))
        assertSimpleNumber(1, parseAPLExpression(":x cmp :s"))
        assertSimpleNumber(0, parseAPLExpression(":v cmp :v"))
    }

    @Test
    fun compareListsNoTypeDiscrimination() {
        assertSimpleNumber(1, parseAPLExpression("(1 ; 0.5) ≡ (1 ; 1÷2)"))
        assertSimpleNumber(0, parseAPLExpression("(1 ; 0.5) ≢ (1 ; 1÷2)"))

        assertSimpleNumber(0, parseAPLExpression("(2 ; 0.5) ≡ (1 ; 1÷2)"))
        assertSimpleNumber(1, parseAPLExpression("(2 ; 0.5) ≢ (1 ; 1÷2)"))

        assertSimpleNumber(0, parseAPLExpression("(1 ; 0.49999) ≡ (1 ; 1÷2)"))
        assertSimpleNumber(1, parseAPLExpression("(1 ; 0.49999) ≢ (1 ; 1÷2)"))

        assertSimpleNumber(1, parseAPLExpression("(1 ; ¯0.0) ≡ (1 ; 0.0)"))
        assertSimpleNumber(0, parseAPLExpression("(1 ; ¯0.0) ≢ (1 ; 0.0)"))

        assertSimpleNumber(0, parseAPLExpression("(2 ; ¯0.0) ≡ (1 ; 0.0)"))
        assertSimpleNumber(1, parseAPLExpression("(2 ; ¯0.0) ≢ (1 ; 0.0)"))
    }
}
