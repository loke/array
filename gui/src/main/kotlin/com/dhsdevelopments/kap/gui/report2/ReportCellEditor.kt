package com.dhsdevelopments.kap.gui.report2

import javafx.event.EventHandler
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import org.controlsfx.control.spreadsheet.SpreadsheetCellEditor
import org.controlsfx.control.spreadsheet.SpreadsheetView

class ReportCellEditor(view: SpreadsheetView) : SpreadsheetCellEditor(view) {
    private val textField = TextField()

    override fun startEdit(value: Any?, format: String, vararg options: Any) {
        require(value is LabelCellContent) { "Value is not LabelCellContent, got: ${value}" }
        textField.text = value.text
        attachEnterEscapeEventHandler()
        textField.requestFocus()
        textField.selectAll()
    }

    override fun getControlValue(): String {
        return textField.text
    }

    override fun end() {
        textField.onKeyPressed = null
    }

    override fun getEditor(): TextField {
        return textField
    }

    private fun attachEnterEscapeEventHandler() {
        textField.onKeyPressed = EventHandler { event ->
            if (event.code === KeyCode.ENTER) {
                endEdit(true)
            } else if (event.code === KeyCode.ESCAPE) {
                endEdit(false)
            }
        }
    }
}
