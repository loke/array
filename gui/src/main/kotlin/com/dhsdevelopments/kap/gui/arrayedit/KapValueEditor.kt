package com.dhsdevelopments.kap.gui.arrayedit

import com.dhsdevelopments.kap.FormatStyle
import javafx.event.EventHandler
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import org.controlsfx.control.spreadsheet.SpreadsheetCellEditor
import org.controlsfx.control.spreadsheet.SpreadsheetView


class KapValueEditor(view: SpreadsheetView?) : SpreadsheetCellEditor(view) {
    private val textField = TextField()

    override fun startEdit(value: Any?, format: String, vararg options: Any) {
        if (value is APLValueSpreadsheetCell) {
            textField.text = value.value.formatted(FormatStyle.READABLE)
        }
        attachEnterEscapeEventHandler()
        textField.requestFocus()
        textField.selectAll()
    }

    override fun getControlValue(): String {
        return textField.text
    }

    override fun end() {
        textField.onKeyPressed = null
    }

    override fun getEditor(): TextField {
        return textField
    }

    private fun attachEnterEscapeEventHandler() {
        textField.onKeyPressed = EventHandler { event ->
            if (event.code === KeyCode.ENTER) {
                endEdit(true)
            } else if (event.code === KeyCode.ESCAPE) {
                endEdit(false)
            }
        }
    }
}
