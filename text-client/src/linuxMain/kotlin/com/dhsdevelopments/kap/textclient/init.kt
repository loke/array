@file:OptIn(ExperimentalForeignApi::class)

package com.dhsdevelopments.kap.textclient

import com.dhsdevelopments.kap.KeyboardInput
import com.dhsdevelopments.kap.KeyboardInputNative
import com.dhsdevelopments.kap.cryptography.CryptographyModule
import com.dhsdevelopments.kap.ffi.FfiModule
import com.dhsdevelopments.kap.raylib.LinuxRaylibModuleBuilder
import com.dhsdevelopments.kap.repl.AbstractGenericRepl
import com.dhsdevelopments.kap.repl.GenericReplBuilder
import com.dhsdevelopments.kap.repl.ReplFailedException
import kotlinx.cinterop.*
import ncurses.OK
import ncurses.setupterm
import platform.posix.*

private var replInst: AbstractGenericRepl? = null
var consoleInit: Boolean = false

@OptIn(ExperimentalForeignApi::class)
fun main(args: Array<String>) {
    setlocale(LC_ALL, "")
    initConsole()

    val builder = LinuxReplBuilder()
    builder.addModuleBuilders(listOf(LinuxRaylibModuleBuilder()))
    val repl = builder.build(args) ?: return
    try {
        replInst = repl
        repl.engine.standardOutput = repl.makeDefaultStdout()
        repl.initAndLoadStartupFiles()
        signal(SIGINT, staticCFunction(::sigHandler))

        repl.engine.addModule(CryptographyModule())
        repl.engine.addModule(TerminalModule())
        repl.engine.addModule(LinuxAudioModule())
        FfiModule.make()?.let { module ->
            repl.engine.addModule(module)
        }

        repl.engine.commandManager.registerQuitHandler {
            repl.close()
            exit(0)
        }

        if (!repl.inhibitRepl()) {
            repl.mainLoop()
        }
    } catch (e: ReplFailedException) {
        println("REPL error: ${e.message}")
    } finally {
        repl.close()
    }
}

private fun sigHandler(@Suppress("UNUSED_PARAMETER") n: Int) {
    replInst?.engine?.interruptEvaluation()
}

@OptIn(ExperimentalForeignApi::class)
private fun initConsole() {
    memScoped {
        val ret = allocArray<IntVar>(1)
        val termValue = getenv("TERM")
        if (termValue != null) {
            if (setupterm(termValue.toKString(), 1, ret) == OK) {
                consoleInit = true
            }
        }
    }
}

private class LinuxReplBuilder : GenericReplBuilder() {
    override fun makeKbInput(repl: AbstractGenericRepl): KeyboardInput {
        return if (disableLineeditor) {
            KeyboardInputNative()
        } else {
            LibinputKeyboardInput()
        }
    }
}
