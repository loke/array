package com.dhsdevelopments.kap.gui2.report

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.gui2.Gui2Client
import java.awt.BorderLayout
import java.awt.Dimension
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.JScrollPane

class ReportFrame(val client: Gui2Client) : JFrame("Report editor") {
    private val reportTable: ReportTable

    init {
        val panel = JPanel(BorderLayout())
        panel.add(JLabel("Foo"), BorderLayout.NORTH)

        reportTable = ReportTable(this, client)

        val scrollPane = JScrollPane(reportTable)
        scrollPane.preferredSize = Dimension(800, 800)
        scrollPane.horizontalScrollBarPolicy = JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS
        scrollPane.verticalScrollBarPolicy = JScrollPane.VERTICAL_SCROLLBAR_ALWAYS
        panel.add(scrollPane, BorderLayout.CENTER)

        contentPane = panel
    }

    fun addDisplayedValue(engine: Engine, storage: VariableHolder, value: APLValue, rowIndex: Int, colIndex: Int) {
        KapLogger.d { "displaying:\n${value.formatted(FormatStyle.PRETTY)}" }
        reportTable.reportTableModel.addDisplayedValue(engine, storage, value, rowIndex, colIndex)
    }
}
