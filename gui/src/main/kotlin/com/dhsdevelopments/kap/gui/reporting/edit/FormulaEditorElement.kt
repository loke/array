package com.dhsdevelopments.kap.gui.reporting.edit

import com.dhsdevelopments.kap.FormatStyle
import com.dhsdevelopments.kap.gui.reporting.Formula
import com.dhsdevelopments.kap.gui.reporting.ReportingClient
import javafx.scene.control.Label
import javafx.scene.layout.VBox

class FormulaEditorElement(client: ReportingClient, formula: Formula) : VBox() {
    val label = Label()

    init {
        children.add(label)

        client.registerVariableListener(formula.name) { value ->
            label.text = value.formatted(FormatStyle.PRETTY)
        }
    }
}
