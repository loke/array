package com.dhsdevelopments.kap.gui.chart

import com.dhsdevelopments.kap.chart.DatasetDouble
import com.dhsdevelopments.kap.chart.ScatterSeries
import javafx.collections.FXCollections
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.chart.*
import javafx.scene.layout.BorderPane
import javafx.stage.Stage

interface ChartType<T> {
    fun make(): Chart<T>
}

interface Chart<T> {
    val component: Node
    fun updateData(datasets: T)
}

object LineChartType : ChartType<LineChartData> {
    override fun make() = LineChartHolder()
}

class LineChartData(val labels: Array<String>, val datasets: Array<DatasetDouble>)

class LineChartHolder : Chart<LineChartData> {
    override val component = LineChart(CategoryAxis(), NumberAxis())

    override fun updateData(datasets: LineChartData) {
        component.data = makeXYChartDataFromDatasets(datasets.labels, datasets.datasets)
    }
}

object BarChartType : ChartType<LineChartData> {
    override fun make() = BarChartHolder()
}

class BarChartHolder : Chart<LineChartData> {
    override val component = BarChart(CategoryAxis(), NumberAxis())

    override fun updateData(datasets: LineChartData) {
        component.data = makeXYChartDataFromDatasets(datasets.labels, datasets.datasets)
    }
}

object PieChartType : ChartType<LineChartData> {
    override fun make() = PieChartHolder()
}

class PieChartHolder : Chart<LineChartData> {
    override val component = PieChart()

    override fun updateData(datasets: LineChartData) {
        if (datasets.datasets.isNotEmpty()) {
            val valuesList = datasets.datasets[0].data
            val list = FXCollections.observableArrayList<PieChart.Data>()
            for (i in 0 until valuesList.size) {
                list.add(PieChart.Data(datasets.labels[i], valuesList[i]))
            }
            component.data = list
        }
    }
}

class ScatterData(val series: List<ScatterSeries>)

object ScatterChartType : ChartType<ScatterData> {
    override fun make(): Chart<ScatterData> {
        return ScatterChartHolder()
    }
}

class ScatterChartHolder : Chart<ScatterData> {
    override val component = ScatterChart(NumberAxis(), NumberAxis())

    override fun updateData(datasets: ScatterData) {
        val list = FXCollections.observableArrayList<XYChart.Series<Number, Number>>()
        datasets.series.forEach { d ->
            val series = XYChart.Series<Number, Number>()
            series.name = d.name
            d.points.forEach { point ->
                series.data.add(XYChart.Data(point.x, point.y, point.label))
            }
            list.add(series)
        }
        component.data = list
    }
}

class ChartController<T>(val chart: Chart<T>) {
    val borderPane = BorderPane(chart.component)

    fun updateData(datasets: T) {
        chart.updateData(datasets)
    }

    companion object {
        private fun <T> make(type: ChartType<T>): ChartController<T> {
            return ChartController(type.make())
        }

        fun <T> openWindowWithData(type: ChartType<T>, datasets: T) {
            val controller = make(type)
            controller.updateData(datasets)
            val stage = Stage()
            stage.title = "Chart"
            stage.scene = Scene(controller.borderPane)
            stage.show()
        }
    }
}
