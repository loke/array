package com.dhsdevelopments.kap.gui.syms

import com.dhsdevelopments.kap.gui.settings.ConfigProvider
import com.dhsdevelopments.kap.keyboard.ExtendedCharsKeyboardInput
import com.dhsdevelopments.kap.keyboard.SymbolDoc
import javafx.scene.control.Label
import javafx.scene.layout.Pane
import javafx.scene.layout.VBox

class FunctionHelp(val config: ConfigProvider) : Pane() {
    private val headingLabel = Label("").apply { styleClass.add("function-help-heading") }
    private val descriptionLabel = Label("").apply { styleClass.add("function-help-doc") }

    private val keyboardInput = ExtendedCharsKeyboardInput()

    init {
        stylesheets.add("/com/dhsdevelopments/kap/gui/interactor.css")
        styleClass.add("function-help-panel")
        styleClass.add("popup-panel")
        val vbox = VBox()
        vbox.children.add(headingLabel)
        vbox.children.add(descriptionLabel)
        children.add(vbox)
    }

    fun updateDoc(sym: String, doc: SymbolDoc) {
        headingLabel.text = sym

        val buf = StringBuilder()
        var first = true

        fun addRow(s: String) {
            if (first) {
                first = false
            } else {
                buf.append("\n")
            }
            buf.append(s)
        }

        doc.monadicName?.let { n ->
            addRow("Monadic: ${n}")
        }
        doc.dyadicName?.let { n ->
            addRow("Dyadic: ${n}")
        }
        doc.monadicOperator?.let { n ->
            addRow("Monadic operator: ${n}")
        }
        doc.dyadicOperator?.let { n ->
            addRow("Dyadic operator: ${n}")
        }
        doc.specialDescription?.let { n ->
            addRow(n)
        }

        keyboardInput.keymap2[sym]?.let { keySequence ->
            val prefix = config.settings().keyPrefix
            addRow("\nKeyboard: ${prefix} ${keySequence.character}")
        }

        descriptionLabel.text = buf.toString()

        requestParentLayout()
    }
}
