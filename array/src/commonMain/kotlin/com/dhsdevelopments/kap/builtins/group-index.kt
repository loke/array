package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*

class GroupFunction : APLFunctionDescriptor {
    class GroupFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val a = a.arrayify()
            val b = b.arrayify()
            val aDimensions = a.dimensions
            if (aDimensions.size != 1) {
                throwAPLException(InvalidDimensionsException("Left argument should be rank 1", pos))
            }
            val bDimensions = b.dimensions
            if (aDimensions[0] != bDimensions[0]) {
                throwAPLException(InvalidDimensionsException("Size of left argument must match the size of the major axis in the left argument", pos))
            }

            val b0 = if (bDimensions.size > 1) {
                AxisMultiDimensionEnclosedValue(b, bDimensions.size - 1)
            } else {
                b
            }

            val res = ArrayList<MutableList<APLValue>?>()
            repeat(aDimensions[0]) { i ->
                val index = a.valueAtCoerceToInt(i, pos)
                if (index >= 0) {
                    if (index >= res.size) {
                        repeat(index - res.size + 1) {
                            res.add(null)
                        }
                    }
                    val l = res[index] ?: ArrayList<APLValue>().also { res[index] = it }
                    l.add(b0.valueAt(i))
                }
            }
            val content = if (bDimensions.size == 1) {
                res.map { v -> if (v == null) APLNullValue else APLArrayList(dimensionsOfSize(v.size), v) }
            } else {
                var emptyElement: APLValue? = null
                res.map { v ->
                    if (v == null) {
                        emptyElement ?: APLArrayImpl(bDimensions.remove(0).insert(0, 0), emptyArray()).also { emptyElement = it }
                    } else {
                        ConcatenatedArraysValue(bDimensions.remove(0).insert(0, v.size), v)
                    }
                }
            }
            return APLArrayList(dimensionsOfSize(content.size), content)
        }

        private class ConcatenatedArraysValue(override val dimensions: Dimensions, val content: List<APLValue>) : APLArray() {
            private val elementDimensions = dimensions.remove(0)
            private val elementSize = elementDimensions.contentSize()

            override fun valueAt(p: Int): APLValue {
                val contentIndex = p / elementSize
                val innerIndex = p % elementSize
                val v = content[contentIndex]
                return v.valueAt(innerIndex)
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = GroupFunctionImpl(instantiation)
}
