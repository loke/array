package com.dhsdevelopments.kap.sql

import com.dhsdevelopments.kap.APLTest
import com.dhsdevelopments.kap.SystemClass
import com.dhsdevelopments.kap.dates.APLTimestamp
import com.dhsdevelopments.kap.dimensionsOfSize
import com.dhsdevelopments.kap.iterateMembersWithPosition
import org.junit.Test
import kotlin.test.*

class SQLTest : APLTest() {
    @Test
    fun connectTest() {
        parseAPLExpression("sql:connect \"jdbc:sqlite::memory:\"").let { result ->
            assertSame(SystemClass.Companion.INTERNAL, result.kapClass)
        }
    }

    @Test
    fun simpleQuery() {
        val result = parseAPLExpression(
            """
            |c ← sql:connect "jdbc:sqlite::memory:"
            |c sql:update "create table foo (a int primary key, b varchar(20))"
            |c sql:update "insert into foo values (1,'foo')"
            |c sql:update "insert into foo values (2,'testing')"
            |c sql:update "insert into foo values (3,'xx')"
            |result ← c sql:query "select * from foo order by a"
            |close c
            |result
            """.trimMargin())
        assertDimension(dimensionsOfSize(3, 2), result)
        assertSimpleNumber(1, result.valueAt(0))
        assertString("foo", result.valueAt(1))
        assertSimpleNumber(2, result.valueAt(2))
        assertString("testing", result.valueAt(3))
        assertSimpleNumber(3, result.valueAt(4))
        assertString("xx", result.valueAt(5))
    }

    @Test
    fun updatePrepared0() {
        val result = parseAPLExpression(
            """
            |c ← sql:connect "jdbc:sqlite::memory:"
            |c sql:update "create table foo (a int primary key, b varchar(20))"
            |statement ← c sql:prepare "insert into foo values (?, ?)"
            |statement sql:updatePrepared 1 "foo"
            |statement sql:updatePrepared 2 "bar"
            |statement sql:updatePrepared 3 "test message"
            |result ← c sql:query "select * from foo order by a"
            |close statement
            |close c
            |result
            """.trimMargin())
        assertDimension(dimensionsOfSize(3, 2), result)
        assertSimpleNumber(1, result.valueAt(0))
        assertString("foo", result.valueAt(1))
        assertSimpleNumber(2, result.valueAt(2))
        assertString("bar", result.valueAt(3))
        assertSimpleNumber(3, result.valueAt(4))
        assertString("test message", result.valueAt(5))
    }

    @Test
    fun updatePrepared1() {
        val result = parseAPLExpression(
            """
            |c ← sql:connect "jdbc:sqlite::memory:"
            |c sql:update "create table foo (a int primary key, b varchar(20))"
            |statement ← c sql:prepare "insert into foo values (?, ?)"
            |statement sql:updatePrepared 3 2 ⍴ 1 "foo" 2 "bar" 3 "test message"
            |result ← c sql:query "select * from foo order by a"
            |close statement
            |close c
            |result
            """.trimMargin())
        assertDimension(dimensionsOfSize(3, 2), result)
        assertSimpleNumber(1, result.valueAt(0))
        assertString("foo", result.valueAt(1))
        assertSimpleNumber(2, result.valueAt(2))
        assertString("bar", result.valueAt(3))
        assertSimpleNumber(3, result.valueAt(4))
        assertString("test message", result.valueAt(5))
    }

    @Test
    fun preparedQuery() {
        val result = parseAPLExpression(
            """
            |c ← sql:connect "jdbc:sqlite::memory:"
            |c sql:update "create table foo (a int primary key, b varchar(20))"
            |c sql:update "insert into foo values (1,'foo')"
            |c sql:update "insert into foo values (2,'testing')"
            |c sql:update "insert into foo values (3,'xx')"
            |c sql:update "insert into foo values (4,'testing2')"
            |c sql:update "insert into foo values (5,'testing-found')"
            |statement ← c sql:prepare "select a, b from foo where a = ?"
            |result ← statement sql:queryPrepared ,5
            |close statement
            |close c
            |result
            """.trimMargin())
        assertDimension(dimensionsOfSize(1, 2), result)
        assertSimpleNumber(5, result.valueAt(0))
        assertString("testing-found", result.valueAt(1))
    }

    // p ← db sql:prepare "insert into foo values (?,?,?)"
    // p sql:updatePrepared (⍪100+⍳n) , (⍪{"foo",⍕⍵}¨⍳n) , ?n⍴10000

    // db ← sql:connect "jdbc:sqlite:/home/elias/foo.db"
    //Connection(url=jdbc:sqlite:/home/elias/foo.db)
    //n ← 500
    //500
    //p ← db sql:prepare "insert into foo values (?,?,?)"
    //PreparedStatement(insert into foo values (?,?,?)
    // parameters=null)
    //p sql:updatePrepared (⍪80000+⍳n) , (⍪{"foo",⍕⍵}¨⍳n) , ?n⍴10000
    //⍬
    //time:measureTime { p sql:updatePrepared (⍪90000+⍳n) , (⍪{"foo",⍕⍵}¨⍳n) , ?n⍴10000 }
    //Total time: 11.57

    @Test
    fun preparedUpdateWithMultiDimensionalArg() {
        val src =
            """
            |c ← sql:connect "jdbc:sqlite::memory:"
            |c sql:update "create table foo (a int primary key, b int)"
            |statement ← c sql:prepare "insert into foo (a,b) values (?,?)"
            |res ← statement sql:updatePrepared 3 2 ⍴ 1 11 2 22 3 33
            |res2 ← c sql:query "select a,b from foo order by a"
            |close statement
            |close c
            |res res2
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2), result)
            result.valueAt(0).let { v ->
                assert1DArray(arrayOf(1, 1, 1), v)
            }
            result.valueAt(1).let { v ->
                assertDimension(dimensionsOfSize(3, 2), v)
                assertArrayContent(arrayOf(1, 11, 2, 22, 3, 33), v)
            }
        }
    }

    @Test
    fun insertNull() {
        val src =
            """
            |c ← sql:connect "jdbc:sqlite::memory:"
            |c sql:update "create table foo (a int primary key, b int null)"
            |c sql:update "insert into foo (a,b) values (1,2)"
            |c sql:update "insert into foo (a,b) values (2,null)"
            |res ← c sql:query "select a,b from foo order by a"
            |close c
            |res
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, 2, 2, InnerAPLNil()), result)
        }
    }

    @Test
    fun insertNullWithPreparedStatement() {
        val src =
            """
            |c ← sql:connect "jdbc:sqlite::memory:"
            |c sql:update "create table foo (a int primary key, b int null)"
            |statement ← c sql:prepare "insert into foo (a,b) values (?,?)"
            |statement sql:updatePrepared 4 2 ⍴ 1 2 3 null 5 null 7 8
            |res ← c sql:query "select a,b from foo order by a"
            |close c
            |res
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(4, 2), result)
            assertArrayContent(arrayOf(1, 2, 3, InnerAPLNil(), 5, InnerAPLNil(), 7, 8), result)
        }
    }

    @Test
    fun preparedQueryFailWithIncorrectDimension() {
        assertFailsWith<SQLAPLException> {
            parseAPLExpression(
                """
                |c ← sql:connect "jdbc:sqlite::memory:"
                |c sql:update "create table foo (a int primary key, b varchar(20))"
                |c sql:update "insert into foo values (1,'foo')"
                |c sql:update "insert into foo values (2,'testing')"
                |c sql:update "insert into foo values (3,'xx')"
                |c sql:update "insert into foo values (4,'testing2')"
                |c sql:update "insert into foo values (5,'testing-found')"
                |statement ← c sql:prepare "select a, b from foo where a = ?"
                |result ← statement sql:queryPrepared 5
                |close statement
                |close c
                |result
            """.trimMargin())
        }
    }

    @Test
    fun invalidConnectString() {
        assertFailsWith<SQLAPLException> {
            parseAPLExpression("sql:connect \"jdbc:foo::memory:\"")
        }
    }

    @Test
    fun invalidQuery() {
        assertFailsWith<SQLAPLException> {
            parseAPLExpression(
                """
                |c ← close atLeave sql:connect "jdbc:sqlite::memory:"
                |c sql:query "select * from bar"
                """.trimMargin())
        }
    }

    @Test
    fun invalidPreparedQuery() {
        assertFailsWith<SQLAPLException> {
            parseAPLExpression(
                """
                |c ← close atLeave sql:connect "jdbc:sqlite::memory:"
                |statement ← c sql:prepare "select a, b from foo where a = ?"
                |result ← statement sql:queryPrepared ,5
                """.trimMargin())
        }
    }

    @Test
    fun invalidPreparedUpdate() {
        assertFailsWith<SQLAPLException> {
            parseAPLExpression(
                """
                |c ← close atLeave sql:connect "jdbc:sqlite::memory:"
                |statement ← c sql:prepare "update foo set a = 1 where a = ?"
                |result ← statement sql:updatePrepared ,5
                """.trimMargin())
        }
    }

    @Test
    fun labelsTest() {
        val src =
            """
            |c ← close atLeave sql:connect "jdbc:sqlite::memory:"
            |c sql:update "create table foo (fooId int primary key, name varchar(200) not null)"
            |c sql:update "insert into foo (fooId, name) values (10, 'some text')"
            |c sql:query "select fooId,name from foo"
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            val metadata = result.metadata
            val labels = metadata.labels
            assertNotNull(labels)
            val labelList = labels.labels
            assertEquals(2, labelList.size)
            assertNull(labelList[0])
            val axisLabels = labelList[1]
            assertNotNull(axisLabels)
            assertEquals(2, axisLabels.size)
            axisLabels[0].let { l ->
                assertNotNull(l)
                assertEquals("fooId", l.title)
            }
            axisLabels[1].let { l ->
                assertNotNull(l)
                assertEquals("name", l.title)
            }
            assertDimension(dimensionsOfSize(1, 2), result)
            assertArrayContent(arrayOf<Any>(10, "some text"), result)
        }
    }

    @Test
    fun selectBooleanTestSQLite() {
        // Note that this test doesn't properly test for booleans returned from the database.
        // This is because SQLite always returns boolean values as integers.
        // See the selectBooleanTestH2 for an alternative test
        val src =
            """
            |c ← close atLeave sql:connect "jdbc:sqlite::memory:"
            |c sql:update "create table foo (fooId int primary key, b boolean)"
            |c sql:update "insert into foo (fooId, b) values (1, true)"
            |c sql:update "insert into foo (fooId, b) values (2, false)"
            |c sql:update "insert into foo (fooId, b) values (3, null)"
            |c sql:query "select fooId,b from foo order by fooId"
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(3, 2), result)
            assertArrayContent(arrayOf(1, 1, 2, 0, 3, InnerAPLNil()), result)
        }
    }

    @Test
    fun selectBooleanTestH2() {
        val src =
            """
            |c ← close atLeave sql:connect "jdbc:h2:mem:"
            |c sql:update "create table foo (fooId int primary key, b boolean)"
            |s ← c sql:prepare "insert into foo (fooId, b) values (?, ?)"
            |s sql:updatePrepared 3 2 ⍴ 1 1 2 0 3 null
            |c sql:query "select fooId,b from foo order by fooId"
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(3, 2), result)
            assertArrayContent(arrayOf(1, 1, 2, 0, 3, InnerAPLNil()), result)
        }
    }

    @Test
    fun readWriteBlob() {
        val src =
            """
            |c ← close atLeave sql:connect "jdbc:h2:mem:"
            |c sql:update "create table foo (id int primary key, content blob not null)"
            |s ← c sql:prepare "insert into foo (id, content) values (?, ?)"
            |s sql:updatePrepared 1 (⍳256)
            |c sql:query "select * from foo"
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(1, 2), result)
            assertSimpleNumber(1, result.valueAt(0))
            val b = result.valueAt(1)
            assertDimension(dimensionsOfSize(256), b)
            b.iterateMembersWithPosition { aplValue, i ->
                assertSimpleNumber(i.toLong(), aplValue)
            }
        }
    }

    @Test
    fun readWriteBinary() {
        val src =
            """
            |c ← close atLeave sql:connect "jdbc:h2:mem:"
            |c sql:update "create table foo (id int primary key, content binary(256) not null)"
            |s ← c sql:prepare "insert into foo (id, content) values (?, ?)"
            |s sql:updatePrepared 1 (⍳256)
            |c sql:query "select * from foo"
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(1, 2), result)
            assertSimpleNumber(1, result.valueAt(0))
            val b = result.valueAt(1)
            assertDimension(dimensionsOfSize(256), b)
            b.iterateMembersWithPosition { aplValue, i ->
                assertSimpleNumber(i.toLong(), aplValue)
            }
        }
    }

    @Test
    fun readWriteClob() {
        val src =
            """
            |c ← close atLeave sql:connect "jdbc:h2:mem:"
            |c sql:update "create table foo (id int primary key, content clob not null)"
            |s ← c sql:prepare "insert into foo (id, content) values (?, ?)"
            |s sql:updatePrepared 1 "test string"
            |c sql:query "select * from foo"
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(1, 2), result)
            assertSimpleNumber(1, result.valueAt(0))
            assertString("test string", result.valueAt(1))
        }
    }

    // Ignore for now, since the actual millisend timestamp returned depends on timezone
    @Ignore
    @Test
    fun readDate() {
        val src =
            """
            |c ← close atLeave sql:connect "jdbc:h2:mem:"
            |c sql:update "create table foo (id int primary key, content date not null)"
            |s ← c sql:prepare "insert into foo (id, content) values (?, ?)"
            |s sql:updatePrepared 1 (time:toTimestamp 1723910400000)
            |c sql:query "select * from foo"
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(1, 2), result)
            assertSimpleNumber(1, result.valueAt(0))
            val v = result.valueAt(1)
            assertIs<APLTimestamp>(v)
            assertEquals(1723910400000, v.time.toEpochMilliseconds())
        }
    }

    @Test
    fun readTime() {
        val src =
            """
            |c ← close atLeave sql:connect "jdbc:h2:mem:"
            |c sql:update "create table foo (id int primary key, content time not null)"
            |s ← c sql:prepare "insert into foo (id, content) values (?, ?)"
            |s sql:updatePrepared 1 (time:toTimestamp 50400000)
            |c sql:query "select * from foo"
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(1, 2), result)
            assertSimpleNumber(1, result.valueAt(0))
            val v = result.valueAt(1)
            assertIs<APLTimestamp>(v)
            assertEquals(50400000, v.time.toEpochMilliseconds())
        }
    }
}