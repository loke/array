package com.dhsdevelopments.kap

import com.dhsdevelopments.kap.json.JsonDecodeException
import com.dhsdevelopments.kap.json.backendSupportsJson
import kotlin.test.*

class JsonTest : APLTest() {
    @Test
    fun readJson() {
        unless(backendSupportsJson) { return }

        parseAPLExpression("json:read \"test-data/json-test.json\"").let { result ->
            assertTrue(result is APLMap)
            assertEquals(7, result.elementCount())
            assertAPLValue(InnerDoubleOrLong(1.0), result.lookupValue(APLString.make("foo").makeTypeQualifiedKey())!!)
            result.lookupValue(APLString.make("someArray").makeTypeQualifiedKey()).let { inner ->
                assertDimension(dimensionsOfSize(5), inner!!)
                assertAPLValue(InnerDoubleOrLong(1.0), inner.valueAt(0))
                assertAPLValue(InnerDoubleOrLong(2.0), inner.valueAt(1))
                assertAPLValue(InnerDoubleOrLong(3.0), inner.valueAt(2))
                assertAPLValue(InnerDoubleOrLong(4.0), inner.valueAt(3))
                inner.valueAt(4).let { internalList ->
                    assertDimension(dimensionsOfSize(2), internalList)
                    assertArrayContent(arrayOf(InnerDoubleOrLong(5.0), InnerDoubleOrLong(6.0)), internalList)
                }
            }
            assertString("foo test", result.lookupValue(APLString.make("someString").makeTypeQualifiedKey())!!)
            assertSimpleNumber(1, result.lookupValue(APLString.make("booleanValue").makeTypeQualifiedKey())!!)
            assertSimpleNumber(0, result.lookupValue(APLString.make("booleanValue2").makeTypeQualifiedKey())!!)
            result.lookupValue(APLString.make("recursiveMap").makeTypeQualifiedKey()).let { inner ->
                assertTrue(inner is APLMap)
                assertAPLValue(InnerDoubleOrLong(1.0), inner.lookupValue(APLString.make("a").makeTypeQualifiedKey())!!)
                assertAPLValue(InnerDoubleOrLong(2.0), inner.lookupValue(APLString.make("b").makeTypeQualifiedKey())!!)
            }
            assertAPLNil(result.lookupValue(APLString.make("nullValue0").makeTypeQualifiedKey())!!)
        }
    }

    @Test
    fun testWeirdMastodonData() {
        unless(backendSupportsJson) { return }
        parseAPLExpression("json:read \"test-data/mastodon-format.json\"")
    }

    @Test
    fun readJsonFromString() {
        unless(backendSupportsJson) { return }

        parseAPLExpression("json:readString \"{\\\"a\\\":10,\\\"b\\\":\\\"c\\\"}\"").let { result ->
            assertTrue(result is APLMap)
            assertEquals(2, result.elementCount())
            assertAPLValue(InnerDoubleOrLong(10.0), result.lookupValue(APLString.make("a").makeTypeQualifiedKey())!!)
            assertString("c", result.lookupValue(APLString.make("b").makeTypeQualifiedKey())!!)
        }
    }

    @Test
    fun readJsonInteger() {
        unless(backendSupportsJson) { return }

        parseAPLExpression("json:readString \"12\"").let { result ->
            val n = result.ensureNumber()
            when (n) {
                is APLDouble -> assertEquals(12.0, n.value)
                is APLLong -> assertEquals(12L, n.value)
                else -> fail("Unexpected type: ${n::class}")
            }
        }
    }

    @Test
    fun readJsonString() {
        unless(backendSupportsJson) { return }

        parseAPLExpression("json:readString \"\\\"ab\\\"\"").let { result ->
            assertString("ab", result)
        }
    }

    @Test
    fun readJsonBoolean() {
        unless(backendSupportsJson) { return }

        parseAPLExpression("json:readString \"true\"").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun readJsonNull() {
        unless(backendSupportsJson) { return }

        parseAPLExpression("json:readString \"null\"").let { result ->
            assertAPLNil(result)
        }
    }

    @Test
    fun invalidJsonSyntaxEmptyElementInMap() {
        unless(backendSupportsJson) { return }

        assertFailsWith<JsonDecodeException> {
            parseAPLExpression(
                """
            |json:readString "{ \"a\": 1 , , }"
            """.trimMargin())
        }
    }

    @Test
    fun invalidJsonSyntaxUnterminatedString() {
        unless(backendSupportsJson) { return }

        assertFailsWith<JsonDecodeException> {
            parseAPLExpression(
                """
                |json:readString "[ 1, 2, \"foo ]"
                """.trimMargin())
        }
    }

    @Test
    fun extractJsonFieldUsingDereferenceDoubleResult() {
        unless(backendSupportsJson) { return }
        val src =
            """
            |a ← json:readString "[10, 20, {\"foo\": [100,200,300], \"bar\": \"test\"}]"
            |a.(2).("foo").(2)
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertTrue(result.ensureNumber().asLong() == 300L)
        }
    }

    @Test
    fun extractJsonFieldUsingDereferenceStringResult() {
        unless(backendSupportsJson) { return }
        val src =
            """
            |a ← json:readString "[10, 20, {\"foo\": [100,200,300], \"bar\": \"test\"}]"
            |a.(2).("bar")
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertString("test", result)
        }
    }
}
