package com.dhsdevelopments.kap

import kotlin.test.*

class SymbolTest : APLTest() {
    @Test
    fun testIntern() {
        val engine = makeEngine()
        val symbol1 = engine.internSymbol("symbol1")
        val symbol2 = engine.internSymbol("symbol2")
        val symbol3 = engine.internSymbol("symbol1")
        assertNotSame(symbol1, symbol2)
        assertNotSame(symbol2, symbol3)
        assertSame(symbol1, symbol3)
    }

    @Test
    fun compareWithNamespace() {
        val engine = makeEngine()
        val nsName0 = NamespaceName("n0")
        val nsName1 = NamespaceName("n1")
        val n0 = Namespace(nsName0)
        val n1 = Namespace(nsName1)
        val sym0 = engine.internSymbol("abc", n0)
        val sym1 = engine.internSymbol("abc", n1)
        assertNotEquals(sym0, sym1)
    }

    @Test
    fun compareToWithNamespace() {
        val engine = makeEngine()
        val nsName0 = NamespaceName("n0")
        val nsName1 = NamespaceName("n1")
        val n0 = Namespace(nsName0)
        val n1 = Namespace(nsName1)
        val sym0 = engine.internSymbol("abc", n0)
        val sym1 = engine.internSymbol("abc", n1)
        val d = sym0.compareTo(sym1)
        assertEquals(-1, d)
    }

    @Test
    fun compareDifferentNamespacesWithSameName() {
        val nsName = NamespaceName("n0")
        val ns0 = Namespace(nsName)
        val ns1 = Namespace(nsName)
        val sym0 = ns0.internSymbol("foo")
        val sym1 = ns1.internSymbol("foo")
        assertEquals("foo", sym0.symbolName)
        assertEquals("foo", sym1.symbolName)
        assertEquals("n0", sym0.namespace.name)
        assertEquals("n0", sym1.namespace.name)
        // The namespace names are the same, but the actual symbols are different, since they were interned in different namespaces
        assertTrue(sym0.namespace == sym1.namespace)
        assertTrue(sym0.namespace === sym1.namespace)
        assertTrue(sym0 != sym1)
        assertTrue(sym0 !== sym1)
    }

    @Test
    fun copiedNamespacesPreserveSymbolEquality() {
        val nsName = NamespaceName("n0")
        val ns0 = Namespace(nsName)
        val sym0 = ns0.internSymbol("foo")
        val ns1 = ns0.copy()
        assertSame(sym0, ns1.findSymbol("foo", true))
        val barSym0 = ns0.internSymbol("bar")
        assertNull(ns1.findSymbol("bar", true))
        val barSym1 = ns1.internSymbol("bar")
        assertTrue(barSym0 != barSym1)
        assertTrue(barSym0 !== barSym1)
    }

    @Test
    fun testParseSymbol() {
        val engine = makeEngine()
        val result = engine.parseAndEval(StringSourceLocation("'foo"))
        assertSame(engine.internSymbol("foo"), result.ensureSymbol().value)
    }

    @Test
    fun keywordSymbol() {
        val engine = makeEngine()
        val result = engine.parseAndEval(StringSourceLocation(":foo"))
        assertSame(engine.makeNamespace("keyword").internSymbol("foo"), result.ensureSymbol().value)
    }

    @Test
    fun findSymbolInEngine() {
        val engine = makeEngine()
        val result = engine.parseAndEval(StringSourceLocation(":foo"))
        assertIs<APLSymbol>(result)
        val foundSym = engine.findSymbolWithNamespace("keyword:foo")
        assertNotNull(foundSym)
        assertSame(result.value, foundSym)
    }

    @Test
    fun keywordsRenderWithoutNamespace() {
        parseAPLExpression("⍕:abcdef").let { result ->
            assertString(":abcdef", result)
        }
    }

    @Test
    fun regularSymbolsRenderWithNamespace() {
        parseAPLExpression("⍕'abcdef").let { result ->
            assertString("default:abcdef", result)
        }
    }

    @Test
    fun internSymbolTest() {
        val engine = makeEngine()
        val result = engine.parseAndEval(StringSourceLocation("\"def\" int:intern \"abc\""))
        assertIs<APLSymbol>(result)
        val ns = engine.findNamespace("def")
        assertNotNull(ns)
        val expectedSym = ns.findSymbol("abc", includePrivate = true)
        assertNotNull(expectedSym)
        assertSame(expectedSym, result.value)
    }

    @Test
    fun symbolNameTest() {
        parseAPLExpression("int:symbolName 'foo:test").let { result ->
            assert1DArray(arrayOf("test", "foo"), result)
        }
    }
}
