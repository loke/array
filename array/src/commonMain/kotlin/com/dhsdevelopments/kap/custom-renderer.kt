package com.dhsdevelopments.kap

import kotlin.time.Duration

class CustomRendererParameter(val engine: Engine) : SystemParameterProvider {
    override fun lookupValue(): APLValue {
        return engine.customRenderer ?: APLNullValue
    }

    override fun updateValue(newValue: APLValue, pos: Position) {
        val v = newValue.collapse()
        val res = when {
            v.dimensions.isNullDimensions() -> null
            v is LambdaValue -> v
            else -> throwAPLException(APLIllegalArgumentException("Argument must be a lambda value", pos))
        }
        engine.customRenderer = res
    }
}

fun formatResult(value: APLValue, addMetadataFn: ((Int, Int, APLList) -> Unit?)? = null, fn: (String) -> Unit) {
    val d = value.dimensions
    when (d.size) {
        2 -> {
            // This is a two-dimensional array of characters
            var i = 0
            repeat(d[0]) { rowIndex ->
                val buf = StringBuilder()
                repeat(d[1]) { colIndex ->
                    val code = when (val v = value.valueAt(i++)) {
                        is APLChar -> v.value
                        is APLList -> {
                            if (addMetadataFn != null) {
                                addMetadataFn(rowIndex, colIndex, v)
                            }
                            v.listElement(0).ensureChar().value
                        }
                        else -> error("Expected char or list. Got: ${v}")
                    }
                    buf.append(charToString(code))
                }
                fn(buf.toString())
            }
        }
        1 -> {
            // This is a one-dimensional array of strings
            repeat(d[0]) { i ->
                val s = value.valueAt(i).toStringValue()
                fn(s)
            }
        }
        else -> {
            throw IllegalArgumentException("Invalid result format: ${value.dimensions}")
        }
    }
}

fun renderResult(context: RuntimeContext, result: APLValue): APLValue {
    val rendererFn = context.engine.customRenderer
    return if (rendererFn == null) {
        val parts = result.formatted(FormatStyle.PRETTY).split("\n")
        APLArrayImpl(dimensionsOfSize(parts.size), Array(parts.size) { i -> APLString(parts[i].asCodepointList().toIntArray()) })
    } else {
        try {
            rendererFn.makeClosure().eval1Arg(context, result, null)
        } catch (e: APLEvalException) {
            throwAPLException(APLEvalException("Error while rendering result: ${e.message}", e.pos, e))
        }
    }
}

fun formatResultToStrings(value: APLValue): List<String> {
    val result = ArrayList<String>()
    formatResult(value, null, result::add)
    return result
}

/**
 * Returns true if the value is 'empty', i.e. if the result is not supposed to be displayed in the REPL.
 */
fun isEmptyValue(v: APLValue) = v.dimensions.isNullDimensions() && v.metadata.defaultValue === APLNilValue

class FormattedEvalResult(val result: APLValue, renderedResult: APLValue, val evaluationTime: Duration) {
    val strings: List<String>
    val metadataList: List<MetadataEntry>

    init {
        val list = ArrayList<String>()
        metadataList = ArrayList()

        var currVclip: Pair<Int, Int>? = null
        var currHclip: Pair<Int, Int>? = null

        fun processMetadataEntryField(row: Int, col: Int, definition: APLValue) {
            val d = definition.dimensions
            if (d.size != 1 || d[0] == 0) {
                KapLogger.w { "Metadata should be a 1-dimensional array with at least one element. Got dimensions: ${d}" }
                return
            }
            val sym = definition.valueAt(0).ensureSymbol().value
            if (sym.namespace != NamespaceList.KEYWORD_NAMESPACE_NAME) {
                KapLogger.w { "Metadata key should be a keyword: ${sym.nameWithNamespace}" }
                return
            }
            when (sym.symbolName) {
                "data" -> {
                    if (d[0] != 3) {
                        KapLogger.w { "Metadata type :data should have three elements. Got: ${d}" }
                        return
                    }
                    val coords = definition.valueAt(1)
                    val height = coords.valueAtCoerceToInt(0, null)
                    val width = coords.valueAtCoerceToInt(1, null)
                    val v = definition.valueAt(2)
                    metadataList.add(MetadataEntry(row, col, height, width, v))
                }
                "vclip" -> {
                    if (d[0] != 3) {
                        KapLogger.w { "Metadata type :vclip should have three elements. Got: ${d}" }
                        return
                    }
                    currVclip = Pair(row, definition.valueAtCoerceToInt(1, null))
                }
                "hclip" -> {
                    if (d[0] != 3) {
                        KapLogger.w { "Metadata type :hclip should have three elements. Got: ${d}" }
                        return
                    }
                    currHclip = Pair(col, definition.valueAtCoerceToInt(2, null))
                }
            }
        }

        fun processMetadataEntry(row: Int, col: Int, list: APLList) {
            for (i in 1 until list.listSize()) {
                val definition = list.listElement(i)
                processMetadataEntryField(row, col, definition)
            }
        }

        formatResult(renderedResult, ::processMetadataEntry, list::add)
        strings = list

        if (currHclip != null || currVclip != null) {
            val vc = currVclip
            val hc = currHclip
            metadataList.indices.forEach { i ->
                val md = metadataList[i]
                val hAdjusted = if (vc != null && md.row < vc.first && md.row + md.height > vc.first) vc.first - md.row else null
                val wAdjusted = if (hc != null && md.col < hc.first && md.col + md.width > hc.first) hc.first - md.col else null
                if (hAdjusted != null || wAdjusted != null) {
                    metadataList[i] = md.copyWithNewSize(hAdjusted, wAdjusted)
                }
            }
        }
    }

    class MetadataEntry(val row: Int, val col: Int, val height: Int, val width: Int, val value: APLValue) {
        fun copyWithNewSize(newHeight: Int?, newWidth: Int?): MetadataEntry {
            return MetadataEntry(row, col, newHeight ?: height, newWidth ?: width, value)
        }

        override fun toString(): String {
            return "MetadataEntry(row=${row}, col=${col}, height=${height}, width=${width}, value=${value})"
        }
    }
}
