package com.dhsdevelopments.kap.gui.report2

import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.Engine
import com.dhsdevelopments.kap.Symbol
import com.dhsdevelopments.kap.VariableHolder
import com.dhsdevelopments.kap.chart.DatasetDouble
import com.dhsdevelopments.kap.chart.computeDatasetsFromAPLValue
import com.dhsdevelopments.kap.gui.arrayedit.ArrayEditor
import com.dhsdevelopments.kap.gui.chart.makeXYChartDataFromDatasets
import javafx.application.Platform
import javafx.scene.chart.CategoryAxis
import javafx.scene.chart.LineChart
import javafx.scene.chart.NumberAxis
import javafx.scene.control.ToggleButton
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import org.controlsfx.control.SegmentedButton
import org.controlsfx.control.spreadsheet.SpreadsheetCell

object LineChartDisplayType : DisplayType {
    override val description get() = "Chart"
    override fun makeConfigurator(client: ReportClient, selection: ArrayEditor.SelectedArea) = LineChartDisplayConfigurator(client, selection)
}

class LineChartDisplayConfigurator(val client: ReportClient, val selection: ArrayEditor.SelectedArea) : DisplayConfigurator {
    private val panel: VBox

    init {
        panel = VBox()
        panel.children.add(SegmentedButton(ToggleButton("Bar"), ToggleButton("Line"), ToggleButton("Pie")))
    }

    override fun settingsPanel() = panel

    override fun saveToResult(engine: Engine, storage: VariableHolder, value: APLValue, sym: Symbol) {
        val displayedValue = LineChartDisplayedValue(client, engine, storage, sym, selection.row, selection.column, value)
        displayedValue.processUpdateAndSetSpan(value, selection)
        Platform.runLater {
            client.variableList.items.add(displayedValue)
        }
    }
}

class LineChartDisplayedValue(
    reportClient: ReportClient,
    engine: Engine,
    storage: VariableHolder,
    name: Symbol,
    row: Int,
    col: Int,
    value: APLValue? = null
) : DisplayedValue(reportClient, engine, storage, name, row, col, value, nRows = 1, nCols = 1) {
    override val displayedRowCount get() = nRows
    override val displayedColCount get() = nCols

    override fun processUpdate(v0: APLValue) {
        processUpdateAndSetSpan(v0, null)
    }

    fun processUpdateAndSetSpan(v0: APLValue, selection: ArrayEditor.SelectedArea?) {
        val (labels, datasets) = computeDatasetsFromAPLValue(v0)
        Platform.runLater {
            value = v0
            updateContentForValue(labels, datasets)
            if (selection != null) {
                reportClient.table.setSpanFromSelection(selection)
            }
        }
    }

    private fun updateContentForValue(labels: Array<String>, datasets: Array<DatasetDouble>) {
        val cellContent = if (!initialised) {
            val gridRow = reportClient.table.grid.rows[row]
            val content = LineChartCellContent(this)
            val cell = LineChartCellType.createCell(content, row, col)
            gridRow[col] = cell
            reportClient.table.grid.rows[row] = gridRow
            initialised = true
            content
        } else {
            val cell = reportClient.table.grid.rows[row][col]
            val item = cell.item
            require(item is LineChartCellContent)
            item
        }
        cellContent.chart.data = makeXYChartDataFromDatasets(labels, datasets)
    }
}

class LineChartCellContent(owner: LineChartDisplayedValue) : LinkedVariableCellContent(owner) {
    val chart: LineChart<String, Number>
    val panel: BorderPane

    init {
        chart = LineChart(CategoryAxis(), NumberAxis())
        panel = BorderPane()
        panel.center = chart
    }

    override fun cellContentString() = ""
}

object LineChartCellType : ReportSpreadsheetCellType<LineChartCellContent>(), CellSpannable {
    fun createCell(content: LineChartCellContent, row: Int, col: Int): SpreadsheetCell {
        val cell = createCellWithType(content, row, col, LineChartCellType)
        cell.graphic = content.panel
        return cell
    }

    override fun initCell(spreadsheetCellBase: SpreadsheetCell) {
    }

    override fun convertValue(value: Any?): LineChartCellContent? {
        return when (value) {
            null -> null
            is LineChartCellContent -> value
            else -> throw IllegalArgumentException("Cannot convert type: ${value}")
        }
    }
}
