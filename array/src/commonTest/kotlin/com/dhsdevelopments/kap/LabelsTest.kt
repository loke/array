package com.dhsdevelopments.kap

import kotlin.test.*

class LabelsTest : APLTest() {
    @Test
    fun simpleLabelsTest() {
        parseAPLExpression("\"foo\" \"bar\" labels[1] 2 2 ⍴ 1 2 3 4").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, 2, 3, 4), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val a = labels.labels
            assertEquals(2, a.size)
            assertNull(a[0])
            val labelsList = a[1]
            assertLabelList(listOf("foo", "bar"), labelsList)
        }
    }

    @Test
    fun preserveLabelsForTranspose() {
        parseAPLExpression("⍉ \"a\" \"b\" labels[0] \"q\" \"w\" \"e\" labels[1] 2 3 ⍴ ⍳100").let { result ->
            assertDimension(dimensionsOfSize(3, 2), result)
            assertArrayContent(arrayOf(0, 3, 1, 4, 2, 5), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val a = labels.labels
            assertEquals(2, a.size)
            assertLabelList(listOf("q", "w", "e"), a[0])
            assertLabelList(listOf("a", "b"), a[1])
        }
    }

    @Test
    fun rotateMonadicLabels() {
        parseAPLExpression("⌽ \"a\" \"b\" \"c\" \"d\" \"e\" \"f\" labels[0] ⍳6").let { result ->
            assertDimension(dimensionsOfSize(6), result)
            assertArrayContent(arrayOf(5, 4, 3, 2, 1, 0), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val a = labels.labels
            assertEquals(1, a.size)
            assertLabelList(listOf("f", "e", "d", "c", "b", "a"), a[0])
        }
    }

    @Test
    fun rotateMonadic2D() {
        parseAPLExpression("⌽ \"aa\" \"bb\" \"cc\" labels[1] \"a\" \"b\" \"c\" labels[0] 3 3 ⍴ ⍳9").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(arrayOf(2, 1, 0, 5, 4, 3, 8, 7, 6), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val a = labels.labels
            assertEquals(2, a.size)
            assertLabelList(listOf("a", "b", "c"), a[0])
            assertLabelList(listOf("cc", "bb", "aa"), a[1])
        }
    }

    @Test
    fun rotateDyadicLabels() {
        parseAPLExpression("2 ⌽ \"a\" \"b\" \"c\" \"d\" \"e\" \"f\" labels[0] ⍳6").let { result ->
            assert1DArray(arrayOf(2, 3, 4, 5, 0, 1), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertEquals(1, labels.labels.size)
            assertLabelList(listOf("c", "d", "e", "f", "a", "b"), labels.labels[0])
        }
    }

    @Test
    fun rotateDyadicHorizontalMonadic2D() {
        parseAPLExpression("2 ⌽ \"aa\" \"bb\" \"cc\" \"dd\" labels[1] \"a\" \"b\" \"c\" \"d\" labels[0] 4 4 ⍴ ⍳9").let { result ->
            assertDimension(dimensionsOfSize(4, 4), result)
            assertArrayContent(arrayOf(2, 3, 0, 1, 6, 7, 4, 5, 1, 2, 8, 0, 5, 6, 3, 4), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val a = labels.labels
            assertEquals(2, a.size)
            assertLabelList(listOf("a", "b", "c", "d"), a[0])
            assertLabelList(listOf("cc", "dd", "aa", "bb"), a[1])
        }
    }

    @Test
    fun rotateDyadicVerticalMonadic2D() {
        parseAPLExpression("2 ⊖ \"aa\" \"bb\" \"cc\" \"dd\" \"ee\" \"ff\" labels[1] \"a\" \"b\" \"c\" \"d\" \"e\" \"f\" labels[0] 6 6 ⍴ ⍳36").let { result ->
            assertDimension(dimensionsOfSize(6, 6), result)
            assertArrayContent(
                arrayOf(
                    12, 13, 14, 15, 16, 17,
                    18, 19, 20, 21, 22, 23,
                    24, 25, 26, 27, 28, 29,
                    30, 31, 32, 33, 34, 35,
                    0, 1, 2, 3, 4, 5,
                    6, 7, 8, 9, 10, 11),
                result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val a = labels.labels
            assertEquals(2, a.size)
            assertLabelList(listOf("c", "d", "e", "f", "a", "b"), a[0])
            assertLabelList(listOf("aa", "bb", "cc", "dd", "ee", "ff"), a[1])
        }
    }

    @Test
    fun rotateDifferentAmountsShouldClearLabelsHorizontal() {
        parseAPLExpression("0 1 ⌽ \"aa\" \"bb\" labels[1] \"a\" \"b\" labels[0] 2 2 ⍴ ⍳4").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 1, 3, 2), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertLabelList(listOf("a", "b"), labels.labels[0])
            assertNull(labels.labels[1])
        }
    }

    @Test
    fun rotateDifferentAmountsShouldClearLabelsVertical() {
        parseAPLExpression("0 1 ⊖ \"aa\" \"bb\" labels[1] \"a\" \"b\" labels[0] 2 2 ⍴ ⍳4").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 3, 2, 1), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertNull(labels.labels[0])
            assertLabelList(listOf("aa", "bb"), labels.labels[1])
        }
    }

    @Test
    fun rotateMultiArgSameAmountsShouldClearLabelsHorizontal() {
        parseAPLExpression("1 1 ⌽ \"aa\" \"bb\" labels[1] \"a\" \"b\" labels[0] 2 2 ⍴ ⍳4").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, 0, 3, 2), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertLabelList(listOf("a", "b"), labels.labels[0])
            assertNull(labels.labels[1])
        }
    }

    @Test
    fun rotateMultiArgSameAmountsShouldClearLabelsVertical() {
        parseAPLExpression("1 1 ⊖ \"aa\" \"bb\" labels[1] \"a\" \"b\" labels[0] 2 2 ⍴ ⍳4").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(2, 3, 0, 1), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertNull(labels.labels[0])
            assertLabelList(listOf("aa", "bb"), labels.labels[1])
        }
    }

    @Test
    fun concatenationTwoLabels() {
        parseAPLExpression("(\"a\" \"b\" labels[1] 2 2 ⍴ ⍳4) ,[1] (\"aa\" \"bb\" labels[1] 2 2 ⍴ 100+⍳4)").let { result ->
            assertDimension(dimensionsOfSize(2, 4), result)
            assertArrayContent(arrayOf(0, 1, 100, 101, 2, 3, 102, 103), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val a = labels.labels
            assertEquals(2, a.size)
            assertNull(a[0])
            assertLabelList(listOf("a", "b", "aa", "bb"), a[1])
        }
    }

    /**
     * Mismatched labels on the non-concatenated axis should be dropped
     */
    @Test
    fun concatenationConflictingAxisNames() {
        parseAPLExpression("(\"a\" \"b\" labels[1] 2 2 ⍴ ⍳4) ,[0] (\"aa\" \"bb\" labels[1] 2 2 ⍴ 100+⍳4)").let { result ->
            assertDimension(dimensionsOfSize(4, 2), result)
            assertArrayContent(arrayOf(0, 1, 2, 3, 100, 101, 102, 103), result)
            val labels = result.metadata.labels
            assertNull(labels)
        }
    }

    /**
     * If some labels are matching, then we currently don't preserve them. Instead, this results in a null entry.
     */
    @Test
    fun concatenatePartiallyMatchedAxisNames() {
        parseAPLExpression("(\"a\" \"b\" \"c\" labels[1] 2 3 ⍴ ⍳6) ,[0] (\"aa\" \"b\" \"c\" labels[1] 2 3 ⍴ 100+⍳6)").let { result ->
            assertDimension(dimensionsOfSize(4, 3), result)
            assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5, 100, 101, 102, 103, 104, 105), result)
            val labels = result.metadata.labels
            assertNull(labels)
        }
    }

    /**
     * If all labels match, the labels should be preserved
     */
    @Test
    fun concatenateMatchedAxisNames() {
        parseAPLExpression("(\"a\" \"b\" \"c\" labels[1] 2 3 ⍴ ⍳6) ,[0] (\"a\" \"b\" \"c\" labels[1] 2 3 ⍴ 100+⍳6)").let { result ->
            assertDimension(dimensionsOfSize(4, 3), result)
            assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5, 100, 101, 102, 103, 104, 105), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertNull(labels.labels[0])
            val axis1Labels = labels.labels[1]
            assertNotNull(axis1Labels)
            assertEquals(3, axis1Labels.size)
            assertEquals("a", axis1Labels[0]?.title)
            assertEquals("b", axis1Labels[1]?.title)
            assertEquals("c", axis1Labels[2]?.title)
        }
    }

    @Test
    fun concatenation1D() {
        parseAPLExpression("(\"a\" \"b\" labels[0] 1 2) , (\"aa\" \"bb\" labels[0] 100 101)").let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(1, 2, 100, 101), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val a = labels.labels
            assertEquals(1, a.size)
            assertLabelList(listOf("a", "b", "aa", "bb"), a[0])
        }
    }

    @Test
    fun readLabelsRank1() {
        parseAPLExpression("labels \"foo\" \"bar\" labels 1 2").let { result ->
            assert1DArray(arrayOf("foo", "bar"), result)
        }
    }

    @Test
    fun readLabelsRank2DefaultAxis() {
        parseAPLExpression("labels \"foo\" \"bar\" labels 2 2 ⍴ ⍳4").let { result ->
            assert1DArray(arrayOf("foo", "bar"), result)
        }
    }

    @Test
    fun readLabelsRank2Explicit0() {
        parseAPLExpression("labels[0] \"foo\" \"bar\" labels[0] 2 2 ⍴ ⍳4").let { result ->
            assert1DArray(arrayOf("foo", "bar"), result)
        }
    }

    @Test
    fun readLabelsRank2Explicit10() {
        parseAPLExpression("labels[1] \"foo\" \"bar\" labels[1] 2 2 ⍴ ⍳4").let { result ->
            assert1DArray(arrayOf("foo", "bar"), result)
        }
    }

    @Test
    fun emptyLabels() {
        parseAPLExpression("labels 1 2 3 4").let { result ->
            assert1DArray(arrayOf("", "", "", ""), result)
        }
    }

    @Test
    fun emptyLabelAxes() {
        parseAPLExpression("labels[0] \"foo\" \"bar\" \"xyz\" labels[1] 2 3 ⍴ ⍳4").let { result ->
            assert1DArray(arrayOf("", ""), result)
        }
    }

    @Test
    fun readLabelsInvalidAxis() {
        assertFailsWith<IllegalAxisException> {
            parseAPLExpression("labels[1] \"foo\" \"bar\" labels 1 2")
        }
    }

    @Test
    fun labelsArePreservedWithBracketAxisIndexing0() {
        val src =
            """
            |foo ← "a" "b" "c" labels[1] 2 3 ⍴ ⍳6
            |foo[;0 1]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 1, 3, 4), result)
            assertLabelList(listOf("a", "b"), result.metadata.labels?.labels?.get(1))
        }
    }

    @Test
    fun labelsArePreservedWithBracketIndexing1() {
        val src =
            """
            |foo ← "a" "b" "c" "d" labels[0] 4 3 ⍴ ⍳12
            |foo[0 1;]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 3), result)
            assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5), result)
            assertLabelList(listOf("a", "b"), result.metadata.labels?.labels?.get(0))
        }
    }

    @Test
    fun labelsArePreservedWithBracketIndexing2() {
        val src =
            """
            |foo ← "a" "b" "c" "d" labels[0] 4 3 ⍴ ⍳12
            |foo[2 0;]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 3), result)
            assertArrayContent(arrayOf(6, 7, 8, 0, 1, 2), result)
            assertLabelList(listOf("c", "a"), result.metadata.labels?.labels?.get(0))
        }
    }

    @Test
    fun labelsArePreservedWithBracketIndexing3() {
        val src =
            """
            |foo ← "a" "b" "c" "d" labels[0] 4 3 ⍴ ⍳12
            |foo[2 0;2 0]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(8, 6, 2, 0), result)
            assertLabelList(listOf("c", "a"), result.metadata.labels?.labels?.get(0))
        }
    }

    @Test
    fun labelsArePreservedWithBracketIndexing4() {
        val src =
            """
            |foo ← "a" "b" "c" "d" labels[0] 4 3 ⍴ ⍳12
            |foo[2 0;0]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2), result)
            assertArrayContent(arrayOf(6, 0), result)
            assertLabelList(listOf("c", "a"), result.metadata.labels?.labels?.get(0))
        }
    }

    @Test
    fun labelsArePreservedWithBracketIndexing5() {
        val src =
            """
            |foo ← "a" "b" "c" labels[1] 4 3 ⍴ ⍳12
            |foo[2;1 0]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2), result)
            assertArrayContent(arrayOf(7, 6), result)
            assertLabelList(listOf("b", "a"), result.metadata.labels?.labels?.get(0))
        }
    }

    @Test
    fun labelsArePreservedWithBracketIndexing6() {
        val src =
            """
            |foo ← "a" "b" "c" labels[1] 4 3 ⍴ ⍳12
            |foo[;2 0 0 0]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(4, 4), result)
            assertArrayContent(arrayOf(2, 0, 0, 0, 5, 3, 3, 3, 8, 6, 6, 6, 11, 9, 9, 9), result)
            assertLabelList(listOf("c", "a", "a", "a"), result.metadata.labels?.labels?.get(1))
        }
    }

    @Test
    fun axisLookupWithMultiDimensionalIndex() {
        val src =
            """
            |foo ← "a" "b" "c" labels[1] 4 3 2 ⍴ ⍳24
            |foo[3;2 2 ⍴ 0 1 1 1;1]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(19, 21, 21, 21), result)
            assertNull(result.metadata.labels)
        }
    }

    @Test
    fun labelsArePreservedWithQuadIndexing() {
        val src =
            """
            |foo ← "a" "b" "c" "d" labels[0] 4 3 ⍴ ⍳12
            |(2 0) (2 0) ⌷ foo
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(8, 6, 2, 0), result)
            assertLabelList(listOf("c", "a"), result.metadata.labels?.labels?.get(0))
        }
    }

    @Test
    fun labelsAsStringsTest() {
        parseAPLExpression("\"a\" \"b\" labels 1 2").let { result ->
            assert1DArray(arrayOf(1, 2), result)
            val labels = result.labelsAsStrings(0)
            assertEquals(listOf("a", "b"), labels)
        }
    }

    @Test
    fun computeLabelledAxisTest() {
        parseAPLExpression("\"a\" \"b\" labels[0] 2 2⍴ 1 2 3 4").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, 2, 3, 4), result)
            val labelsList = result.metadata.labels
            assertNotNull(labelsList)
            val v = labelsList.computeLabelledAxes()
            assertEquals(2, v.size)
            assertTrue(v[0])
            assertFalse(v[1])
        }
    }

    @Test
    fun labelsArePreservedWithIncreaseDimension() {
        val src =
            """
            |a ← "a" "b" labels[0] 2 2⍴ 1 2 3 4
            |b ← "c" "d" labels[1] a
            |< b
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(1, 2, 2), result)
            assertArrayContent(arrayOf(1, 2, 3, 4), result)
            val labelsList = result.metadata.labels
            assertNotNull(labelsList)
            assertNull(labelsList.labels[0])
            labelsList.labels[1].let { axisLabels ->
                assertNotNull(axisLabels)
                assertEquals(2, axisLabels.size)
                axisLabels[0].let { l ->
                    assertNotNull(l)
                    assertEquals("a", l.title)
                }
                axisLabels[1].let { l ->
                    assertNotNull(l)
                    assertEquals("b", l.title)
                }
            }
            labelsList.labels[2].let { axisLabels ->
                assertNotNull(axisLabels)
                assertEquals(2, axisLabels.size)
                axisLabels[0].let { l ->
                    assertNotNull(l)
                    assertEquals("c", l.title)
                }
                axisLabels[1].let { l ->
                    assertNotNull(l)
                    assertEquals("d", l.title)
                }
            }
        }
    }

    @Test
    fun labelsArePreservedWithDecreaseDimension() {
        val src =
            """
            |a ← "a" "b" labels[0] 2 2 2 2 ⍴ ⍳16
            |b ← "c" "d" labels[2] a
            |c ← "e" "f" labels[3] b
            |> c
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(4, 2, 2), result)
            assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15), result)
            val labelsList = result.metadata.labels
            assertNotNull(labelsList)
            assertEquals(3, labelsList.labels.size)
            assertNull(labelsList.labels[0])
            labelsList.labels[1].let { axisLabels ->
                assertNotNull(axisLabels)
                assertEquals(2, axisLabels.size)
                axisLabels[0].let { l ->
                    assertNotNull(l)
                    assertEquals("c", l.title)
                }
                axisLabels[1].let { l ->
                    assertNotNull(l)
                    assertEquals("d", l.title)
                }
            }
            labelsList.labels[2].let { axisLabels ->
                assertNotNull(axisLabels)
                assertEquals(2, axisLabels.size)
                axisLabels[0].let { l ->
                    assertNotNull(l)
                    assertEquals("e", l.title)
                }
                axisLabels[1].let { l ->
                    assertNotNull(l)
                    assertEquals("f", l.title)
                }
            }
        }
    }

    @Test
    fun labelsArePreservedWithReorderedAxis() {
        val src =
            """
            |a ← "d" "e" "f" labels[1] "a" "b" "c" labels[0] 3 3 ⍴ ⍳9
            |1 0 ⍉ a
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(arrayOf(0, 3, 6, 1, 4, 7, 2, 5, 8), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val labelList = labels.labels
            assertEquals(2, labelList.size)
            assertLabelList(listOf("d", "e", "f"), labelList[0])
            assertLabelList(listOf("a", "b", "c"), labelList[1])
        }
    }

    @Test
    fun updatedLabelsPreservesOtherAxis() {
        val src =
            """
            |a ← "d" "e" "f" labels[1] "a" "b" "c" labels[0] 3 3 ⍴ ⍳9
            |b ← "g" "h" "i" labels[0] a
            |b
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val labelList = labels.labels
            assertEquals(2, labelList.size)
            assertLabelList(listOf("g", "h", "i"), labelList[0])
            assertLabelList(listOf("d", "e", "f"), labelList[1])
        }
    }

    @Test
    fun dropPreservesLabels() {
        val src =
            """
            |a ← "a" "b" "c" "d" "e" labels 1 2 3 4 5
            |b ← 2↓a
            |b
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assert1DArray(arrayOf(3, 4, 5), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val labelList = labels.labels
            assertEquals(1, labelList.size)
            assertLabelList(listOf("c", "d", "e"), labelList[0])
        }
    }

    @Test
    fun takeFromStartPreservesLabels() {
        val src =
            """
            |a ← "a" "b" "c" "d" "e" labels 1 2 3 4 5
            |b ← 3↑a
            |b
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assert1DArray(arrayOf(1, 2, 3), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val labelList = labels.labels
            assertEquals(1, labelList.size)
            assertLabelList(listOf("a", "b", "c"), labelList[0])
        }
    }

    @Test
    fun takeFromEndPreservesLabels() {
        val src =
            """
            |a ← "a" "b" "c" "d" "e" labels 1 2 3 4 5
            |b ← ¯3↑a
            |b
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assert1DArray(arrayOf(3, 4, 5), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val labelList = labels.labels
            assertEquals(1, labelList.size)
            assertLabelList(listOf("c", "d", "e"), labelList[0])
        }
    }

    @Test
    fun takeMultidimensionalPreservesLabels() {
        val src =
            """
            |a ← "d" "e" "f" labels[1] "a" "b" "c" labels[0] 3 3 ⍴ ⍳9
            |b ← 1 1↑a
            |b
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(1, 1), result)
            assertArrayContent(arrayOf(0), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val labelList = labels.labels
            assertEquals(2, labelList.size)
            assertLabelList(listOf("a"), labelList[0])
            assertLabelList(listOf("d"), labelList[1])
        }
    }

    @Test
    fun monadicDropPreservesLabels() {
        val src =
            """
            |a ← "a" "b" "c" "d" "e" labels 1 2 3 4 5
            |b ← ↓a
            |b
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assert1DArray(arrayOf(2, 3, 4, 5), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val labelList = labels.labels
            assertEquals(1, labelList.size)
            assertLabelList(listOf("b", "c", "d", "e"), labelList[0])
        }
    }

    @Test
    fun insertingElementPreservesOtherLabels() {
        val src =
            """
            |a ← "a" "b" "c" "d" "e" labels 1 2 3 4 5
            |b ← (9,)⍢(1↓) a
            |b
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assert1DArray(arrayOf(1, 9, 2, 3, 4, 5), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            val labelList = labels.labels
            assertEquals(1, labelList.size)
            assertLabelList(listOf("a", null, "b", "c", "d", "e"), labelList[0])
        }
    }

    @Test
    fun insertingIntoHigherDimensionalArraysPreservesLabels() {
        val src =
            """
            |a ← "d" "e" "f" "g" labels[0] "a" "b" "c" labels[1] 4 3 ⍴ ⍳12
            |{("x" "y" "z" labels[0] 3 3 ⍴ 100+⍳9)⍪⍵}⍢(1 0↓) a
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(7, 3), result)
            assertArrayContent(
                arrayOf(
                    0, 1, 2, 100, 101, 102, 103, 104, 105, 106, 107, 108,
                    3, 4, 5, 6, 7, 8, 9, 10, 11), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertEquals(2, labels.labels.size)
            labels.labels[0].let { l ->
                assertNotNull(l)
                assertLabelList(listOf("d", "x", "y", "z", "e", "f", "g"), l)
            }
            labels.labels[1].let { l ->
                assertNotNull(l)
                assertLabelList(listOf("a", "b", "c"), l)
            }
        }
    }

    @Test
    fun takeWithFillPreservesLabels() {
        val src =
            """
            |foo ← "a" "b" "c" labels ⍳3
            |4↑foo
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assert1DArray(arrayOf(0, 1, 2, 0), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertEquals(1, labels.labels.size)
            labels.labels[0].let { l ->
                assertNotNull(l)
                assertLabelList(listOf("a", "b", "c", null), l)
            }
        }
    }

    @Test
    fun takeWithFillNegativeArgumentPreservesLabels() {
        val src =
            """
            |foo ← "a" "b" "c" labels ⍳3
            |¯4↑foo
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assert1DArray(arrayOf(0, 0, 1, 2), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertEquals(1, labels.labels.size)
            labels.labels[0].let { l ->
                assertNotNull(l)
                assertLabelList(listOf(null, "a", "b", "c"), l)
            }
        }
    }

    @Test
    fun forEachOnLabels() {
        val src =
            """
            |a ← "e" "f" "g" labels[1] "a" "b" "c" "d" labels[0] 4 3 ⍴ ⍳12
            |b ← {10+⍵}¨ a
            |b
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(4, 3), result)
            assertArrayContent(arrayOf(10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertEquals(2, labels.labels.size)
            labels.labels[0].let { l ->
                assertNotNull(l)
                assertLabelList(listOf("a", "b", "c", "d"), l)
            }
            labels.labels[1].let { l ->
                assertNotNull(l)
                assertLabelList(listOf("e", "f", "g"), l)
            }
        }
    }

    @Test
    fun selectColumnsByLabels() {
        parseAPLExpression("\"b\" \"c\" { ⍉(⊂⍺ ⍳⍨ labels ⍵) ⌷ ⍉⍵ } \"a\" \"b\" \"c\" \"d\" labels 2 4 ⍴ ⍳8").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(1, 2, 5, 6), result)
            val labels = result.metadata.labels
            assertNotNull(labels)
            assertEquals(2, labels.labels.size)
            assertNull(labels.labels[0])
            val axis1Labels = labels.labels[1]
            assertLabelList(listOf("b", "c"), axis1Labels)
        }
    }

    @Test
    fun selectColumnsByLabelsWithDiscard() {
        parseAPLExpression("\"b\" \"c\" { ⍉(⊂⍺ ⍳⍨ labels ⍵) ⌷ ⍉⍵ } \"a\" \"b\" \"c\" \"d\" labels 2 4 ⍴ ⍳8", collapse = false).collapse(true)
    }

    @Test
    fun labelledSubarray0() {
        val src =
            """
            |↑ ("a" "b" labels 2 2 ⍴ ⍳4) 1             
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 1, 2, 3), result)
            assertLabels(listOf(null, listOf("a", "b")), result)
        }
    }

    @Test
    fun labelledSubarray1() {
        val src =
            """
            |a ← ("a" "b" labels 2 2 ⍴ ⍳4)
            |b ← a 1
            |c ← a b
            |↑c
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(0, 1, 2, 3), result)
            assertLabels(listOf(null, listOf("a", "b")), result)
        }
    }

    @Test
    fun labelledSubarray2() {
        val src =
            """
            |("a" "b" labels 2 2 ⍴ ⍳4) 1             
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2), result)
            result.valueAt(0).let { v ->
                assertDimension(dimensionsOfSize(2, 2), v)
                assertArrayContent(arrayOf(0, 1, 2, 3), v)
                assertLabels(listOf(null, listOf("a", "b")), v)
            }
            assertSimpleNumber(1, result.valueAt(1))
        }
    }

    @Test
    fun labelledArrayInList() {
        val src =
            """
            |"a" "b" labels 2 2 ⍴ ⍳4 ; 123
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertIs<APLList>(result)
            assertEquals(2, result.listSize())
            result.listElement(0).let { v ->
                assertDimension(dimensionsOfSize(2, 2), v)
                assertArrayContent(arrayOf(0, 1, 2, 3), v)
                assertLabels(listOf(null, listOf("a", "b")), v)
            }
            assertSimpleNumber(123, result.listElement(1))
        }
    }

    @Test
    fun labelsArePreservedWhenIndexing0() {
        val src =
            """
            |x ← "a" "b" "c" "d" labels[0] "e" "f" "g" "h" labels[1] 4 4 ⍴ ⍳16
            |x[;3]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(4), result)
            assertArrayContent(arrayOf(3, 7, 11, 15), result)
            assertLabels(listOf(listOf("a", "b", "c", "d")), result)
        }
    }

    @Test
    fun labelsArePreservedWhenIndexing1() {
        val src =
            """
            |x ← "a" "b" "c" "d" labels[0] "e" "f" "g" "h" labels[1] 4 4 ⍴ ⍳16
            |x[;2 1 0]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(4, 3), result)
            assertArrayContent(arrayOf(2, 1, 0, 6, 5, 4, 10, 9, 8, 14, 13, 12), result)
            assertLabels(listOf(listOf("a", "b", "c", "d"), listOf("g", "f", "e")), result)
        }
    }

    @Test
    fun indexSingleElementFromLabelledArray0() {
        val src =
            """
            |x ← "a" "b" "c" "d" labels ⍳4
            |x[2]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertSimpleNumber(2, result)
        }
    }

    @Test
    fun indexSingleElementFromLabelledArray1() {
        val src =
            """
            |x ← "a" "b" "c" "d" labels[0] "e" "f" "g" "h" labels[1] 4 4 ⍴ ⍳16
            |x[2;1]
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertSimpleNumber(9, result)
        }
    }

    @Test
    fun replicateFirstAxisPreservesOtherAxisLabels() {
        val src =
            """
            |x ← "a" "b" "c" "d" labels[0] "e" "f" "g" "h" labels[1] 4 4 ⍴ ⍳16
            |1 0 0 1 ⌿ x
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 4), result)
            assertArrayContent(arrayOf(0, 1, 2, 3, 12, 13, 14, 15), result)
            assertLabels(
                listOf(
                    listOf("a", "d"),
                    listOf("e", "f", "g", "h")
                ),
                result)
        }
    }

    @Test
    fun replicateLastAxisPreservesOtherAxisLabels() {
        val src =
            """
            |x ← "a" "b" "c" "d" labels[0] "e" "f" "g" "h" labels[1] 4 4 ⍴ ⍳16
            |1 1 1 0 / x
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(4, 3), result)
            assertArrayContent(arrayOf(0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14), result)
            assertLabels(
                listOf(
                    listOf("a", "b", "c", "d"),
                    listOf("e", "f", "g")
                ),
                result)
        }
    }

    @Test
    fun replicateFirstAxisReplicatedRowsDoNotPreserveLabels() {
        val src =
            """
            |x ← "a" "b" "c" "d" labels[0] "e" "f" "g" "h" labels[1] 4 4 ⍴ ⍳16
            |1 1 2 1 ⌿ x
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(5, 4), result)
            assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 8, 9, 10, 11, 12, 13, 14, 15), result)
            assertLabels(
                listOf(
                    listOf("a", "b", null, null, "d"),
                    listOf("e", "f", "g", "h")
                ),
                result)
        }
    }

    @Test
    fun replicateLastAxisReplicatedRowsDoNotPreserveLabels() {
        val src =
            """
            |x ← "a" "b" "c" "d" labels[0] "e" "f" "g" "h" labels[1] 4 4 ⍴ ⍳16
            |1 2 1 0 / x
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(4, 4), result)
            assertArrayContent(arrayOf(0, 1, 1, 2, 4, 5, 5, 6, 8, 9, 9, 10, 12, 13, 13, 14), result)
            assertLabels(
                listOf(
                    listOf("a", "b", "c", "d"),
                    listOf("e", null, null, "g")
                ),
                result)
        }
    }

    /**
     * This test was added after a JVM exception was detected in the below expression
     */
    @Test
    fun labelsLookupWithSingleDimensionArg0() {
        parseAPLExpression("a ← \"a1\" \"a2\" labels 1 2 ⋄ \"a1\" s:col a", withStandardLib = true).let { result ->
            assertSimpleNumber(1, result)
        }
    }

    /**
     * Simplified version of the above test
     */
    @Test
    fun labelsLookupWithSingleDimensionArg1() {
        parseAPLExpression("a ← \"a1\" \"a2\" labels 1 2 ⋄ 0 ⌷ a").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun nullLabels() {
        parseAPLExpression("\"foo\" null labels 100 200").let { result ->
            assert1DArray(arrayOf(100, 200), result)
            assertLabels(
                listOf(
                    listOf("foo", null)),
                result)
        }
    }

    @Test
    fun nullLabelsWithMultidimensionalArray() {
        parseAPLExpression("\"a\" null \"test\" labels[0] null \"abc\" \"def\" labels[1] 3 3 ⍴ ⍳9").let { result ->
            assertDimension(dimensionsOfSize(3, 3), result)
            assertLabels(
                listOf(
                    listOf("a", null, "test"),
                    listOf(null, "abc", "def")),
                result)
        }
    }
}
