package com.dhsdevelopments.kap.completions

import com.dhsdevelopments.kap.*

class SymbolCompletionCandidate(val sym: Symbol, val text: String, val description: String) {
    override fun toString() = "SymbolCompletionCandidate[symbol=${sym}, text='${text}', description='${description}']"
}

fun symbolCompletionCandidates(engine: Engine, text: String, index: Int): Pair<List<SymbolCompletionCandidate>, String> {
    val word = completionWordAtIndex(engine, text, index) ?: return Pair(emptyList(), "")
    return Pair(symbolCompletionCandidatesForWord(engine, word), word)
}

fun symbolCompletionCandidatesForWord(engine: Engine, word: String): List<SymbolCompletionCandidate> {
    val i = word.indexOf(':')
    val candidates = ArrayList<SymbolCompletionCandidate>()
    when (i) {
        -1 -> {
            candidates.addAll(
                engine
                    .currentNamespace
                    .allSymbols()
                    .matchSymbols(word) { sym -> makeCandidate(engine, sym, sym.symbolName) })
            candidates.addAll(engine.currentNamespace.imports.flatMap { nsName ->
                engine
                    .findNamespaceFromName(nsName)
                    .allSymbols(true)
                    .matchSymbols(word) { sym -> makeCandidate(engine, sym, sym.symbolName) }
            })
        }
        0 -> {
            // find keywords
            val prefix = word.substring(1)
            candidates.addAll(
                engine
                    .keywordNamespace
                    .allSymbols()
                    .matchSymbols(prefix) { sym -> makeCandidate(engine, sym, ":${sym.symbolName}") }
            )
        }
        else -> {
            val ns = engine.findNamespace(word.substring(0, i))
            if (ns != null) {
                val prefix = word.substring(i + 1)
                candidates.addAll(
                    ns
                        .allSymbols(true)
                        .matchSymbols(prefix) { sym -> makeCandidate(engine, sym, sym.nameWithNamespace) })
            }
        }
    }
    return candidates
}

private inline fun Set<Symbol>.matchSymbols(prefix: String, fn: (Symbol) -> SymbolCompletionCandidate): List<SymbolCompletionCandidate> {
    return filter { sym -> sym.symbolName.startsWith(prefix) }.sorted().map { sym -> fn(sym) }
}

private fun makeCandidate(engine: Engine, sym: Symbol, expandedName: String): SymbolCompletionCandidate {
    val desc = buildString {
        when {
            engine.syntaxRulesForSymbol(sym) != null -> append("syntax")
            engine.getOperator(sym) != null -> append("operator")
            engine.getFunction(sym) != null -> append("function")
            else -> append("symbol")
        }
    }
    return SymbolCompletionCandidate(sym, expandedName, desc)
}

fun completionWordAtIndex(engine: Engine, text: String, index: Int): String? {
    if (index == 0) {
        return ""
    }
    if (index < 0 || index > text.length) {
        throw IndexOutOfBoundsException("Attempt to find word at illegal index. index=${index}, text='${text}'")
    }
    var p = index
    val res = ArrayList<String>()
    while (p > 0) {
        val ref = mpCodepointBefore(text, p) ?: return null
        val ch = ref.codepoint
        if (engine.isCharSymbolDelimiter(ch)) break
        res.add(charToString(ch))
        p = ref.leftIndex
    }
    return res.reversed().joinToString(separator = "")
}

private data class CodePointRef(val codepoint: Int, val leftIndex: Int)

private fun mpCodepointBefore(text: String, index: Int): CodePointRef? {
    if (index <= 0 || index > text.length) {
        throw IndexOutOfBoundsException("Invalid index: ${index}, length: ${text.length}")
    }
    val low = text[index - 1]
    return when {
        low.isHighSurrogate() -> {
            KapLogger.e { "Got high surrogate at index ${index}" }
            null
        }
        low.isLowSurrogate() -> {
            when (index) {
                0 -> error("This should be checked in the initial if statement")
                1 -> {
                    KapLogger.e { "First character in string is a low surrogate: '${text}'" }
                    null
                }
                else -> {
                    val high = text[index - 2]
                    if (!high.isHighSurrogate()) {
                        KapLogger.w { "low surrogate not preceded by a high surrogate at index: ${index - 2}" }
                        null
                    } else {
                        CodePointRef(makeCharFromSurrogatePair(high, low), index - 2)
                    }
                }
            }
        }
        else -> CodePointRef(low.code, index - 1)
    }
}
