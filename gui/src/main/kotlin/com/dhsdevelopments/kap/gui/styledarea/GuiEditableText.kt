package com.dhsdevelopments.kap.gui.styledarea

import com.dhsdevelopments.kap.edit.SimpleTextTrackingEditableText

class GuiEditableText<PS, SEG, S>(val styledArea: KapEditorStyledArea<PS, SEG, S>) : SimpleTextTrackingEditableText() {
    override fun text(start: Int, end: Int?): String {
        return styledArea.document.getText(start, end ?: styledArea.document.length())
    }

    override fun cursorPosition(): Int {
        return styledArea.caretPosition
    }

    override fun insert(text: String, index: Int) {
        val start = styledArea.caretPosition
        styledArea.insertText(index, text)
        val newPos = if (index <= start) {
            start + text.length
        } else {
            start
        }
        styledArea.caretSelectionBind.selectRange(newPos, newPos)
    }

    override fun remove(numChars: Int, index: Int) {
        val oldCursor = styledArea.caretPosition
        styledArea.deleteText(index, index + numChars)
        val i = when {
            oldCursor < index -> oldCursor
            oldCursor > index + numChars -> oldCursor - numChars
            else -> index
        }
        styledArea.caretSelectionBind.selectRange(i, i)
    }

    override fun commit() {
    }
}
