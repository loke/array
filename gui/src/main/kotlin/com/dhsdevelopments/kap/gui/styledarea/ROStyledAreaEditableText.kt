package com.dhsdevelopments.kap.gui.styledarea

import com.dhsdevelopments.kap.edit.SimpleTextTrackingEditableText

class ROStyledAreaEditableText(val styledArea: ROStyledArea) : SimpleTextTrackingEditableText() {
    override fun text(start: Int, end: Int?): String {
        val p = styledArea.findInputStartEnd()
        val endIndex = if (end == null) p.inputEnd else p.inputStart + end
        return styledArea.document.getText(p.inputStart + start, endIndex)
    }

    override fun cursorPosition(): Int {
        val (p, index) = findBoundsAndCursor()
        return index - p.inputStart
    }

    override fun insert(text: String, index: Int) {
        val (p, cursorPos) = findBoundsAndCursor()
        styledArea.insertText(p.inputStart + index, text)
        val newPos = if (p.inputStart + index <= cursorPos) cursorPos + text.length else cursorPos
        styledArea.caretSelectionBind.selectRange(newPos, newPos)
    }

    override fun remove(numChars: Int, index: Int) {
        val (p, cursorPos) = findBoundsAndCursor()
        val oldCursor = cursorPos - p.inputStart
        val docPos = p.inputStart + index
        styledArea.deleteText(docPos, docPos + numChars)
        val i = when {
            oldCursor < index -> oldCursor
            oldCursor > index + numChars -> oldCursor - numChars
            else -> index
        }
        val newPos = p.inputStart + i
        styledArea.caretSelectionBind.selectRange(newPos, newPos)
    }

    override fun commit() {
    }

    private fun findBoundsAndCursor(): Pair<ROStyledArea.InputPositions, Int> {
        val p = styledArea.findInputStartEnd()
        val index = styledArea.caretPosition
        if (index < p.inputStart || index > p.inputEnd) {
            throw IllegalStateException("Cursor position is outside editable range")
        }
        return Pair(p, index)
    }
}
