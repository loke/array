@file:OptIn(ExperimentalNativeApi::class, ExperimentalForeignApi::class)

package com.dhsdevelopments.kap

import kotlinx.cinterop.*
import platform.posix.getenv
import platform.posix.nanosleep
import platform.posix.timespec
import kotlin.experimental.ExperimentalNativeApi
import kotlin.native.ref.WeakReference

actual fun sleepMillis(engine: Engine, time: Long) {
    memScoped {
        val tms = alloc<timespec>()
        tms.tv_sec = time / 1000
        tms.tv_nsec = time.rem(1000) * 1000L * 1000L
        nanosleep(tms.ptr, null)
    }
}

actual fun sleepMillisCallback(engine: Engine, time: Long, pos: Position, callback: () -> Either<APLValue, Exception>) {
    TODO("not implemented")
}

actual fun Double.formatDouble() = this.toString()

actual fun toRegexpWithException(string: String, options: Set<RegexOption>): Regex {
    return try {
        string.toRegex(options)
    } catch (e: Exception) {
        throw RegexpParseException("Error parsing regexp: \"${string}\"", e)
    }
}

actual fun numCores() = 1

actual fun makeBackgroundDispatcher(numThreads: Int): MPThreadPoolExecutor {
    return SingleThreadedThreadPoolExecutor()
}

class LinuxWeakRef<T : Any>(ref: T) : MPWeakReference<T> {
    private val instance = WeakReference(ref)

    override val value: T? get() = instance.value
}

actual fun <T : Any> MPWeakReference.Companion.make(ref: T): MPWeakReference<T> {
    return LinuxWeakRef(ref)
}

actual fun makeTimerHandler(engine: Engine): TimerHandler? = null

actual fun getEnvValue(name: String): String? {
    return getenv(name)?.toKString()
}

actual val getEnvValueSupported: Boolean get() = true

class LinuxNativeData : NativeData {
    override fun close() {}
}

actual fun makeNativeData(): NativeData = LinuxNativeData()

@Suppress("NOTHING_TO_INLINE")
actual inline fun nativeUpdateBreakPending(engine: Engine, state: Boolean) {
}

@Suppress("NOTHING_TO_INLINE")
actual inline fun nativeBreakPending(engine: Engine): Boolean = false

actual fun findInstallPath(): String? = null

actual val brokenNegativeZero: Boolean = false
