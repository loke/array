package com.dhsdevelopments.kap

import kotlin.test.*

class MapTest : APLTest() {
    @Test
    fun simpleMap() {
        parseAPLExpression("map 3 2 ⍴ \"foo\" 1 \"bar\" 2 \"a\" 3").let { result ->
            assertIs<APLMap>(result)
            assertEquals(3, result.elementCount())
            assertSimpleNumber(1, result.lookupValue(APLString.make("foo").makeTypeQualifiedKey())!!)
            assertSimpleNumber(2, result.lookupValue(APLString.make("bar").makeTypeQualifiedKey())!!)
            assertSimpleNumber(3, result.lookupValue(APLString.make("a").makeTypeQualifiedKey())!!)
        }
    }

    @Test
    fun oneElementMap() {
        parseAPLExpression("map \"foo\" 1").let { result ->
            assertIs<APLMap>(result)
            assertEquals(1, result.elementCount())
            assertSimpleNumber(1, result.lookupValue(APLString.make("foo").makeTypeQualifiedKey())!!)
        }
    }

    @Test
    fun multiElementMapFromVector() {
        parseAPLExpression("map \"foo\" 1 \"test\" 2").let { result ->
            assertIs<APLMap>(result)
            assertEquals(2, result.elementCount())
            assertSimpleNumber(1, result.lookupValue(APLString.make("foo").makeTypeQualifiedKey())!!)
            assertSimpleNumber(2, result.lookupValue(APLString.make("test").makeTypeQualifiedKey())!!)
        }
    }

    @Test
    fun missingElementsReturnsNull() {
        parseAPLExpression("map 3 2 ⍴ \"foo\" 1 \"bar\" 2 \"a\" 3").let { result ->
            assertIs<APLMap>(result)
            assertEquals(3, result.elementCount())
            assertSimpleNumber(1, result.lookupValue(APLString.make("foo").makeTypeQualifiedKey())!!)
            assertSimpleNumber(2, result.lookupValue(APLString.make("bar").makeTypeQualifiedKey())!!)
            assertSimpleNumber(3, result.lookupValue(APLString.make("a").makeTypeQualifiedKey())!!)
            assertNull(result.lookupValue(APLString.make("x").makeTypeQualifiedKey()))
        }
    }

    @Test
    fun mixedKeyTypes() {
        parseAPLExpression("map 3 2 ⍴ 1 2 @a 3 (3 2 ⍴ 1 2 3 4 5 6) 4").let { result ->
            assertIs<APLMap>(result)
            assertEquals(3, result.elementCount())
            assertSimpleNumber(2, result.lookupValue(1.makeAPLNumber().makeTypeQualifiedKey())!!)
            assertSimpleNumber(3, result.lookupValue(APLChar('a'.code).makeTypeQualifiedKey())!!)
            assertSimpleNumber(
                4,
                result.lookupValue(
                    APLArrayImpl(
                        dimensionsOfSize(3, 2),
                        arrayOf(1, 2, 3, 4, 5, 6).map { v -> v.makeAPLNumber() }.toTypedArray()).makeTypeQualifiedKey())!!)
        }
    }

    @Test
    fun stringValues() {
        parseAPLExpression("map 2 2 ⍴ \"foo\" \"a\" \"bar\" \"b\"").let { result ->
            assertIs<APLMap>(result)
            assertEquals(2, result.elementCount())
            assertString("a", result.lookupValue(APLString.make("foo").makeTypeQualifiedKey())!!)
            assertString("b", result.lookupValue(APLString.make("bar").makeTypeQualifiedKey())!!)
        }
    }

    @Test
    fun lookupTest() {
        val result = parseAPLExpression("(map 4 2 ⍴ 1 2 3 4 \"foo\" \"a\" \"bar\" \"b\") mapGet 1")
        assertSimpleNumber(2, result)
    }

    @Test
    fun updateTest() {
        val result = parseAPLExpression(
            """
            |a ← map 2 2 ⍴ "foo" "abc" "bar" "bcd"
            |b ← a mapPut "foo" "update"
            |↑b mapGet ⊂"foo"
        """.trimMargin())
        assertString("update", result)
    }

    @Test
    fun insertTest() {
        val result = parseAPLExpression(
            """
            |a ← map 2 2 ⍴ "foo" "abc" "bar" "bcd"
            |b ← a mapPut "foo2" "insert"
            |↑b mapGet ⊂"foo2"
        """.trimMargin())
        assertString("insert", result)
    }

    @Test
    fun removeTest() {
        val result = parseAPLExpression(
            """
            |a ← map 2 2 ⍴ "foo" "abc" "bar" "bcd"
            |b ← a mapRemove ⊂"foo"
            |↑b mapGet ⊂"foo"
        """.trimMargin())
        assertAPLNil(result)
    }

    @Test
    fun putAndRemove() {
        val result = parseAPLExpression(
            """
            |a ← map 2 2 ⍴ "foo" "abc" "bar" "bcd"
            |b ← a mapPut "foo2" "added"
            |c ← b mapRemove ⊂"foo2"
            |↑c mapGet ⊂"foo2"
        """.trimMargin())
        assertAPLNil(result)
    }

    @Test
    fun putMultiple() {
        val result = parseAPLExpression(
            """
            |a ← map 2 2 ⍴ "foo" "abc" "bar" "bcd"
            |b ← a mapPut 2 2 ⍴ "foo2" "added2" "foo3" "added3"
            |b mapGet "bar" "foo2" "foo3"
        """.trimMargin())
        assertDimension(dimensionsOfSize(3), result)
        assertString("bcd", result.valueAt(0))
        assertString("added2", result.valueAt(1))
        assertString("added3", result.valueAt(2))
    }

    @Test
    fun mapArraySyntaxGetSimple() {
        val result = parseAPLExpression(
            """
            |a ← map 2 2 ⍴ "foo" "abc" "bar" "bcd"
            |a[⊂"foo"]
            """.trimMargin())
        assertDimension(emptyDimensions(), result)
        val v = result.valueAt(0)
        assertString("abc", v)
    }

    @Test
    fun mapArraySyntaxNotFound() {
        val result = parseAPLExpression(
            """
            |a ← map 2 2 ⍴ "foo" "abc" "bar" "bcd"
            |a[⊂"abcde"]
            """.trimMargin())
        assertAPLNil(result)
    }

    @Test
    fun readKeyValues() {
        parseAPLExpression("a ← map 2 2 ⍴ \"foo\" \"abc\" \"bar\" \"bcd\" ◊ mapToArray a").let { result ->
            fun findKeyAndValue(v: APLValue, findKey: String, findValue: String) {
                for (i in 0 until v.dimensions[0]) {
                    val key = v.valueAt(v.dimensions.indexFromPosition(intArrayOf(i, 0))).toStringValue()
                    val value = v.valueAt(v.dimensions.indexFromPosition(intArrayOf(i, 1))).toStringValue()
                    if (key == findKey) {
                        assertEquals(findValue, value, "Value for key does not match")
                        return
                    }
                }
                fail("Could not find key: '${findKey}'")
            }

            assertDimension(dimensionsOfSize(2, 2), result)
            findKeyAndValue(result, "foo", "abc")
            findKeyAndValue(result, "bar", "bcd")
        }
    }

    @Test
    fun mapKeysWithStringsSimpleTest() {
        parseAPLExpression("mapKeys map \"a\" 1 \"b\" 2").let { result ->
            assertDimension(dimensionsOfSize(2), result)
            val expected = listOf("a", "b")
            val resultAsList = ArrayList<String>()
            result.iterateMembers { m ->
                resultAsList.add(m.toStringValue())
            }
            assertEquals(expected, resultAsList.sorted())
        }
    }

    @Test
    fun mapKeysWithEmptyMap() {
        parseAPLExpression("mapKeys (map 1 2) mapRemove 1").let { result ->
            assertAPLNull(result)
        }
    }

    @Test
    fun mapKeysOnIllegalType() {
        assertFailsWith<IncompatibleTypeException> {
            parseAPLExpression("mapKeys 1")
        }
    }

    @Test
    fun mapKeysDyadic() {
        assertFailsWith<Unimplemented2ArgException> {
            parseAPLExpression("1 mapKeys (map 1 2 3 4)")
        }
    }

    @Test
    fun listAsKey() {
        parseAPLExpression("a ← map (:a ; :b) 10 (:a ; :b ; :c) 20 ⋄ a[(:a ; :b ; :c)]").let { result ->
            assertSimpleNumber(20, result)
        }
    }

    @Test
    fun complexAsKey() {
        parseAPLExpression("a ← map 7j6 1 8j6 2 ⋄ a[7j6]").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun rationalAsKey() {
        parseAPLExpression("a ← map (4÷5) 1 (6÷7) 2 ⋄ a[4÷5]").let { result ->
            assertSimpleNumber(1, result)
        }
    }

    @Test
    fun indexedLookupWithMultipleResults() {
        parseAPLExpression("a ← map \"foo\" 3 \"bar\" 4 ⋄ a[\"foo\" \"bar\"]").let { result ->
            assert1DArray(arrayOf(3, 4), result)
        }
    }

    @Test
    fun indexedLookupWithMultipleResultsMissingValues() {
        parseAPLExpression("a ← map \"foo\" 3 \"bar\" 4 ⋄ a[\"foo\" \"bar\" \"abc\"]").let { result ->
            assert1DArray(arrayOf(3, 4, InnerAPLNil()), result)
        }
    }

    @Test
    fun indexedLookupWithMultipleResultsMultiDimensions() {
        parseAPLExpression("a ← map \"foo\" 3 \"bar\" 4 \"abc\" 5 \"def\" 6 \"ghi\" 7 ⋄ a[2 2 ⍴ \"foo\" \"bar\" \"abc\" \"def\"]").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(3, 4, 5, 6), result)
        }
    }

    @Test
    fun indexedLookupWithAtomicKeys() {
        parseAPLExpression("a ← map 10 100 20 200 30 300 ⋄ a[30 20]").let { result ->
            assert1DArray(arrayOf(300, 200), result)
        }
    }

    @Test
    fun indexedLookupWithAtomicKeysMultiDimensions() {
        parseAPLExpression("a ← map 10 100 20 200 30 300 40 400 50 500 60 600 70 700 ⋄ a[2 2 ⍴ 30 20 10 70]").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(300, 200, 100, 700), result)
        }
    }

    @Test
    fun indexedLookupWithAtomicKeysMultiDimensionsMissingValues() {
        parseAPLExpression("a ← map 10 100 20 200 30 300 40 400 50 500 60 600 70 700 ⋄ a[2 2 ⍴ 30 20 11 71]").let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(300, 200, InnerAPLNil(), InnerAPLNil()), result)
        }
    }

    @Test
    fun numericEquivalenceForKeys() {
        val src =
            """
            |a ← map 1 10 1.1 20 (int:asBigint 3) 30 (int:asRational 4) 40 (10000.0÷7.0) 50 3.5 60 5.3 70
            |a mapGet¨ 1.0 1 1.1 1.10000000000001 3 3.0 (int:asBigint 3) (int:asRational 3) 4 4.0 (int:asBigint 4) (int:asRational 4) (10000.0÷7.0) (6282923587291429÷4398046511104) 3.5 (7÷2) 5.3 (5÷3) 
            """.trimMargin()
        val result = parseAPLExpression(src)
        assert1DArray(
            arrayOf(
                InnerAPLNil(), 10, 20, InnerAPLNil(), 30, InnerAPLNil(), 30, 30, 40, InnerAPLNil(),
                40, 40, 50, InnerAPLNil(), 60, InnerAPLNil(), 70, InnerAPLNil()), result)
    }

    @Test
    fun testKeyEquivalence() {
        val a = APLLong(1).makeTypeQualifiedKey() as APLValue.APLValueKey
        val b = APLDouble(1.0).makeTypeQualifiedKey() as APLValue.APLValueKey
        val c = a == b
        assertFalse(c)
    }

    @Test
    fun testKeyComparisonStrings() {
        val a = APLString.make("foo").makeTypeQualifiedKey()
        val b = APLString.make("foo").makeTypeQualifiedKey()
        val c = a == b
        assertTrue(c)
    }

    @Test
    fun infinitesAsKeys() {
        val src =
            """
            |a ← map (1.0÷0.0) 10 (¯1.0÷0.0) 20
            |a mapGet¨ (1.0÷0.0) (¯1.0÷0.0) 0 ¯1 (2⋆250) 0.00000001
            """.trimMargin()
        val result = parseAPLExpression(src)
        assert1DArray(arrayOf(10, 20, InnerAPLNil(), InnerAPLNil(), InnerAPLNil(), InnerAPLNil()), result)
    }

    @Test
    fun negativeZeroAsKey() {
        val src =
            """
            |a ← map ¯0.0 1 0.0 2
            |(a mapGet ¯0.0) (a mapGet 0.0)
            """.trimMargin()
        val result = parseAPLExpression(src)
        assert1DArray(arrayOf(1, 2), result)
    }

    @Test
    fun mapAppendTest() {
        val src =
            """
            |a ← map 1 (⍮"foo") 2 (⍮"bar") 3 (1 2 3 4)
            |a mapAppend 1 "test" 3 10 5 11
            """.trimMargin()
        val result = parseAPLExpression(src, withStandardLib = true)
        assertIs<APLMap>(result)
        assertEquals(4, result.elementCount())
        assert1DArray(arrayOf("foo", "test"), result.lookupValue(1.makeAPLNumber().makeTypeQualifiedKey()))
        assert1DArray(arrayOf("bar"), result.lookupValue(2.makeAPLNumber().makeTypeQualifiedKey()))
        assert1DArray(arrayOf(1, 2, 3, 4, 10), result.lookupValue(3.makeAPLNumber().makeTypeQualifiedKey()))
        assert1DArray(arrayOf(11), result.lookupValue(5.makeAPLNumber().makeTypeQualifiedKey()))
    }

    @Test
    fun mapAppendTest2DArg() {
        val src =
            """
            |a ← map 1 (20 21) 3 (22 23)
            |a mapAppend 3 2 ⍴ 1 100 2 200 3 300
            """.trimMargin()
        val result = parseAPLExpression(src, withStandardLib = true)
        assertIs<APLMap>(result)
        assertEquals(3, result.elementCount())
        assert1DArray(arrayOf(20, 21, 100), result.lookupValue(1.makeAPLNumber().makeTypeQualifiedKey()))
        assert1DArray(arrayOf(200), result.lookupValue(2.makeAPLNumber().makeTypeQualifiedKey()))
        assert1DArray(arrayOf(22, 23, 300), result.lookupValue(3.makeAPLNumber().makeTypeQualifiedKey()))
    }

    @Test
    fun mapAppendWithInvalidDimension0() {
        val src =
            """
            |a ← map 1 (⍮"foo") 2 (⍮"bar") 3 (1 2 3 4)
            |a mapAppend 1 "test" 3 10 5
            """.trimMargin()
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression(src, withStandardLib = true)
        }
    }

    @Test
    fun mapAppendWithInvalidDimension1() {
        val src =
            """
            |a ← map 1 (⍮"foo") 2 (⍮"bar") 3 (1 2 3 4)
            |a mapAppend 4 4 ⍴ 1 "test" 3 10 5 11 101 102 103 104 105 106 107 108 109 110
            """.trimMargin()
        assertFailsWith<InvalidDimensionsException> {
            parseAPLExpression(src, withStandardLib = true)
        }
    }
}
