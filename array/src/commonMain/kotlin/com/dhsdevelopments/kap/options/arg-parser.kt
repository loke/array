package com.dhsdevelopments.kap.options

import com.dhsdevelopments.kap.maxValueBy

class InvalidOptionDefinition(message: String) : Exception(message)
open class OptionException(message: String) : Exception(message)
class InvalidOption(name: String) : OptionException("Invalid arg: ${name}")
class OptionSyntaxException(message: String) : OptionException(message)

class Option(
    val name: String,
    val description: String? = null,
    val requireArg: String? = null,
    val shortOption: String? = null,
    val multiple: Boolean = false
) {
    override fun toString(): String {
        return "Option(name='${name}', description=${description}, requireArg=${requireArg}, shortOption=${shortOption}, multiple=${multiple})"
    }
}

class OptionResult(
    val option: Option,
    val arg: String?)

private val LONG_OPTION_ARG_PATTERN = "^--([a-zA-Z0-9-]+)=(.*)$".toRegex()
private val LONG_OPTION_NO_ARG_PATTERN = "^--([a-zA-Z0-9-]+)$".toRegex()
private val SHORT_OPTION_PATTERN = "^-([a-zA-Z0-9]+)$".toRegex()

class ArgParser(vararg options: Option) {
    val definedOptions: Map<String, Option>

    init {
        definedOptions = HashMap()
        options.forEach { option ->
            if (definedOptions.containsKey(option.name)) {
                throw InvalidOptionDefinition("Duplicated option name found: ${option.name}")
            }
            val s = option.shortOption
            if (s != null) {
                if (definedOptions.values.find { v -> v.shortOption == s } != null) {
                    throw InvalidOptionDefinition("Duplicated short option found: ${option.name}")
                }
            }
            definedOptions[option.name] = option
        }
    }

    fun parse(args: Array<String>): Map<String, List<String?>> {
        val parseResults = HashMap<String, MutableList<String?>>()

        fun putResult(option: Option, parameter: String?) {
            val prev = parseResults[option.name]
            if (prev == null) {
                parseResults[option.name] = mutableListOf(parameter)
            } else if (!option.multiple) {
                throw OptionSyntaxException("Multiple values for ${option.name} not allowed")
            } else {
                prev.add(parameter)
            }
        }

        var i = 0
        while (i < args.size) {
            val arg = args[i++]
            val argResult = LONG_OPTION_ARG_PATTERN.matchEntire(arg)
            if (argResult != null) {
                val option = lookup(arg, argResult.groups.get(1)!!.value, argResult.groups.get(2)!!.value)
                putResult(option.option, option.arg)
            } else {
                val longNoArgResult = LONG_OPTION_NO_ARG_PATTERN.matchEntire(arg)
                if (longNoArgResult != null) {
                    val option = lookup(arg, longNoArgResult.groups.get(1)!!.value, null)
                    putResult(option.option, option.arg)
                } else {
                    val shortResult = SHORT_OPTION_PATTERN.matchEntire(arg)
                    if (shortResult != null) {
                        val shortArgString = shortResult.groups.get(1)!!.value
                        shortArgString.forEach { ch ->
                            val option = lookupSingleChar(ch.toString())
                            if (option.requireArg != null) {
                                if (i >= args.size) {
                                    throw OptionSyntaxException("Missing argument to \"${option.shortOption}\" parameter")
                                }
                                val parameter = args[i++]
                                putResult(option, parameter)
                            } else {
                                putResult(option, null)
                            }
                        }
                    } else {
                        throw InvalidOption(arg)
                    }
                }
            }
        }
        return parseResults
    }

    fun printHelp(outputSink: Appendable) {
        outputSink.append("Options:")
        outputSink.append("\n")
        val descList = definedOptions.keys.sorted().map { key ->
            val option = definedOptions[key]!!
            val description = option.description
            val buf = StringBuilder()
            buf.append("  --")
            buf.append(option.name)
            if (option.requireArg != null) {
                buf.append("=")
                buf.append(option.requireArg)
            }
            if (option.shortOption != null) {
                buf.append(", -")
                buf.append(option.shortOption)
                if (option.requireArg != null) {
                    buf.append(" ")
                    buf.append(option.requireArg)
                }
            }
            Pair(buf.toString(), description ?: "")
        }
        val descCol = descList.maxValueBy { it.first.length }
        descList.forEach { (option, description) ->
            outputSink.append(option)
            outputSink.append(" ".repeat(descCol + 1 - option.length))
            outputSink.append(description)
            outputSink.append("\n")
        }
    }

    private fun lookup(originalArg: String, name: String, arg: String?): OptionResult {
        val option = definedOptions[name] ?: throw InvalidOption(originalArg)
        if ((option.requireArg != null && arg == null) || (option.requireArg == null && arg != null)) {
            throw InvalidOption(originalArg)
        }
        return OptionResult(option, arg)
    }

    private fun lookupSingleChar(s: String): Option {
        definedOptions.values.forEach { option ->
            if (option.shortOption != null && option.shortOption == s) {
                return option
            }
        }
        throw InvalidOption(s)
    }
}
