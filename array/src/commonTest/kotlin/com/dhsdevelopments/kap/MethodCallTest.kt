package com.dhsdevelopments.kap

import kotlin.test.Ignore
import kotlin.test.Test

class MethodCallTest : APLTest() {
    @Test
    fun simpleCall() {
        val src =
            """
            |a ← map 'value 10 `
            |        'kap:methods (map 'valuePlusN λ{ ⍺.value + ⍵ })
            |a⍠valuePlusN 200
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertSimpleNumber(210, result)
        }
    }

    @Test
    fun methodCallWithUpdatedFields() {
        val src =
            """
            |a ← map 'value 10 `
            |        'kap:methods (map 'valuePlusN λ{ ⍺.value + ⍵ })
            |a ← a mapPut 'value 20
            |a⍠valuePlusN 100
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertSimpleNumber(120, result)
        }
    }

    // Ignored since .computeClosure() isn't properly implemented for MethodCallFunction
    @Ignore
    @Test
    fun methodCallWithCapturedValue() {
        val src =
            """
            |a ← map 'value 10 `
            |        'kap:methods (map 'valuePlusN λ{ ⍺.value + ⍵ })
            |b ← λ(a⍠valuePlusN)
            |a ← a mapPut 'value 20
            |⍞b 100
            """.trimMargin()
        parseAPLExpression(src).let { result ->
            assertSimpleNumber(110, result)
        }
    }
}
