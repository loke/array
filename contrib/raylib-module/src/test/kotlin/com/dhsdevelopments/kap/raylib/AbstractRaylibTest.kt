package com.dhsdevelopments.kap.raylib

import com.dhsdevelopments.kap.*
import kotlin.test.AfterTest

abstract class AbstractRaylibTest {
    private val registeredEngines = ArrayList<Engine>()

    @AfterTest
    fun teardown() {
        val copy = ArrayList(registeredEngines)
        registeredEngines.clear()
        copy.forEach(Engine::close)
    }

    fun makeEngine(numComputeEngines: Int? = null): Engine {
        return Engine(numComputeEngines).also { engine -> registerEngine(engine) }
    }

    fun registerEngine(engine: Engine) {
        registeredEngines.add(engine)
    }

    fun parseAndTestWithGeneric(expr: String, withStandardLib: Boolean = false, collapse: Boolean = true, numTasks: Int? = null, callback: (APLValue) -> Unit) {
        parseAPLExpression(expr.replace("{GENERIC}", ""), withStandardLib = withStandardLib, collapse = collapse, numTasks = numTasks).let { result ->
            callback(result)
        }
        parseAPLExpression(
            expr.replace("{GENERIC}", "int:ensureGeneric"),
            withStandardLib = withStandardLib,
            collapse = collapse,
            numTasks = numTasks).let { result ->
            callback(result)
        }
    }

    fun parseAPLExpression(expr: String, withStandardLib: Boolean = false, collapse: Boolean = true, numTasks: Int? = null): APLValue {
        return parseAPLExpression2(expr, withStandardLib, collapse, numTasks).first
    }

    fun parseAPLExpression2(
        expr: String,
        withStandardLib: Boolean = false,
        collapse: Boolean = true,
        numTasks: Int? = null
    ): Pair<APLValue, Engine> {
        val engine = makeEngine(numTasks)
        engine.standardOutput = CharacterOutput { s -> print(s) }
        engine.addLibrarySearchPath("../../array/standard-lib")
        if (withStandardLib) {
            engine.parseAndEval(StringSourceLocation("use(\"standard-lib.kap\")"))
        }
        val result = engine.parseAndEval(StringSourceLocation(expr), collapseResult = collapse)
        return Pair(result, engine)
    }

    fun parseAPLExpressionWithOutput(
        expr: String,
        withStandardLib: Boolean = false,
        collapse: Boolean = true,
        numTasks: Int? = null
    ): Pair<APLValue, String> {
        val engine = makeEngine(numTasks)
        engine.addLibrarySearchPath("standard-lib")
        if (withStandardLib) {
            engine.parseAndEval(StringSourceLocation("use(\"standard-lib.kap\")"))
        }
        val output = StringBuilderOutput()
        engine.standardOutput = output
        val result = engine.parseAndEval(StringSourceLocation(expr))
        engine.withThreadLocalAssigned {
            return Pair(if (collapse) result.collapse() else result, output.buf.toString())
        }
    }
}
