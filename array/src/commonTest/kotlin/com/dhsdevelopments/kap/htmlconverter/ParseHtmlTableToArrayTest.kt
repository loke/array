package com.dhsdevelopments.kap.htmlconverter

import com.dhsdevelopments.kap.APLTest
import com.dhsdevelopments.kap.NearDouble
import com.dhsdevelopments.kap.dimensionsOfSize
import com.fleeksoft.ksoup.Ksoup
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class ParseHtmlTableToArrayTest : APLTest() {
    @Test
    fun simpleTableTest() {
        val html = """
            <html>
              <head>
                <title>Some title</title>
              </head>
              <body>
                <div>Some extra text</div>
                <div>
                  Some more text here
                  <div>
                    <table>
                      <tr>
                        <td>1,234</td>
                        <td>2,123,421.112</td>
                      </tr>
                      <tr>
                        <td>3[1]</td>
                        <td>4</td>
                      </tr>
                    </table>
                  </div>
                </div>
              </body>
            </html> 
        """.trimIndent()
        val doc = Ksoup.parse(html)
        val tableData = htmlTableToArray(doc)
        assertNotNull(tableData)
        assertDimension(dimensionsOfSize(2, 2), tableData)
        assertArrayContent(arrayOf(1234, NearDouble(2123421.112), 3, 4), tableData)
    }

    @Test
    fun parseMultipleTables() {
        val html = """
            <table>
            <tr><td>1</td><td>2</td></tr>
            <tr><td>3</td><td>4</td></tr>
            <tr><td>5</td><td>123456789012345678901234567890123456789012345678901234567890</td></tr>
            <tr><td>-123456789012345678901234567890123456789012345678901234567890</td><td>0</td></tr>
            </table>
            <table><thead><th>header1</th><th>header2</th></thead>
            <tbody><tr><td>1.2345</td><td>some text</td></tr>
            <tr><td>1 2 3</td><td>a1234</td>
        """.trimIndent()
        val doc = Ksoup.parse(html)
        htmlTableToArray(doc, 0).let { tableData ->
            assertNotNull(tableData)
            assertDimension(dimensionsOfSize(4, 2), tableData)
            assertArrayContent(
                arrayOf(
                    1, 2, 3, 4, 5, InnerBigIntOrLong("123456789012345678901234567890123456789012345678901234567890"),
                    InnerBigIntOrLong("-123456789012345678901234567890123456789012345678901234567890"), 0),
                tableData)
        }
        htmlTableToArray(doc, 1).let { tableData ->
            assertNotNull(tableData)
            assertDimension(dimensionsOfSize(2, 2), tableData)
            assertArrayContent(arrayOf(NearDouble(1.2345), "some text", 1, "a1234"), tableData)
        }
        assertNull(htmlTableToArray(doc, -1))
        assertNull(htmlTableToArray(doc, 2))
    }

    @Test
    fun parseTableWithHeaders() {
        val html = """
            <table>
            <thead>
            <tr><th>header0</th><th>header1</th></tr>
            </thead>
            <tbody>
            <tr><td>0</td><td>1</td></tr>
            <tr><td>2</td><td>3</td></tr>
            <tr><td>4</td><td>5</td></tr>
            </tbody>
            </table>
        """.trimIndent()
        val doc = Ksoup.parse(html)
        htmlTableToArray(doc, 0).let { tableData ->
            assertNotNull(tableData)
            assertDimension(dimensionsOfSize(3, 2), tableData)
            assertArrayContent(arrayOf(0, 1, 2, 3, 4, 5), tableData)

            val labels = tableData.metadata.labels?.labels
            assertNotNull(labels)
            assertEquals(2, labels.size)
            val axisLabels = labels[1]
            assertNotNull(axisLabels)
            assertEquals(2, axisLabels.size)
            assertEquals("header0", axisLabels[0]?.title)
            assertEquals("header1", axisLabels[1]?.title)
        }
    }
}
