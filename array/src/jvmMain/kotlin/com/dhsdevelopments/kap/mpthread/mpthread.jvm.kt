@file:OptIn(ExperimentalContracts::class)

package com.dhsdevelopments.kap.mpthread

import com.dhsdevelopments.kap.Either
import java.util.concurrent.atomic.AtomicReferenceArray
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract
import kotlin.reflect.KClass

private class JvmThread(private val fn: (MPThread) -> Any?) : MPThread {
    private var threadRef: Thread? = null
    private var result: Either<Any?, Throwable>? = null

    private val thread: Thread get() = threadRef ?: throw IllegalStateException("Thread has not been assigned yet")

    fun assignThread(newThread: Thread) {
        require(threadRef == null)
        threadRef = newThread
    }


    override fun join() = thread.join()
    override fun isStopped() = !thread.isAlive

    override fun completionData(): Either<Any?, Throwable> {
        val d = result
        requireNotNull(d) { "Result is not assigned" }
        return d
    }

    private fun start() {
        val thread = Thread {
            try {
                val res = fn(this)
                result = Either.Left(res)
            } catch (e: Exception) {
                result = Either.Right(e)
            }
        }
        assignThread(thread)
        thread.start()
    }

    companion object {
        fun createNewThread(fn: (MPThread) -> Any?): JvmThread {
            val jvmThread = JvmThread(fn)
            jvmThread.start()
            return jvmThread
        }
    }
}

actual fun startThread(fn: (MPThread) -> Any?): MPThread {
    return JvmThread.createNewThread(fn)
}

actual val backendSupportsThreading: Boolean get() = true

class JvmMPAtomicRefArray<T>(size: Int) : MPAtomicRefArray<T> {
    private val content = AtomicReferenceArray<T>(size)

    override operator fun get(index: Int): T? = content[index]

    override fun compareAndExchange(index: Int, expected: T?, newValue: T?): T? {
        return content.compareAndExchange(index, expected, newValue)
    }
}

actual fun <T> makeAtomicRefArray(size: Int): MPAtomicRefArray<T> {
    return JvmMPAtomicRefArray(size)
}

actual fun <T : Any> makeMPThreadLocalBackend(type: KClass<T>): MPThreadLocal<T> {
    return object : MPThreadLocal<T> {
        val tl = object : ThreadLocal<T?>() {
            override fun initialValue(): T? = null
        }

        override var value: T?
            get() = tl.get()
            set(newValue) = tl.set(newValue)
    }
}

actual class MPLock actual constructor(recursive: Boolean) {
    val impl: Lock = ReentrantLock()

    actual fun makeCondVar(): MPCondVar {
        return JvmCondVar(impl.newCondition())
    }
}

class JvmCondVar(val impl: Condition) : MPCondVar {
    override fun waitUpdate() {
        impl.await()
    }

    override fun signal() {
        impl.signal()
    }

    override fun signalAll() {
        impl.signalAll()
    }
}

@OptIn(ExperimentalContracts::class)
actual inline fun <T> MPLock.withLocked(fn: () -> T): T {
    contract { callsInPlace(fn, InvocationKind.EXACTLY_ONCE) }
    val lock = impl
    lock.lock()
    try {
        return fn()
    } finally {
        lock.unlock()
    }
}
