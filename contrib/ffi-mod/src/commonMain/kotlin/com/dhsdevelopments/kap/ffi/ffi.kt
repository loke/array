package com.dhsdevelopments.kap.ffi

import com.dhsdevelopments.kap.NativeCloseable

class FfiNotAvailableException : Exception("FFI is not available")
class LoadLibraryFailedException(message: String, cause: Throwable? = null) : Exception(message, cause)
class GenericFfiException(message: String, cause: Throwable? = null) : Exception(message, cause)

interface FfiManager : NativeCloseable {
    /**
     * Load the given library.
     *
     * @return an instance ot [FfiLibrary]
     * @throws LoadLibraryFailedException if the library could not be loaded
     */
    fun loadLibrary(file: String): FfiLibrary

    /**
     * Allocate a buffer of size [size].
     *
     * @return a buffer object
     */
    fun allocBytes(size: Long): FfiBuffer

    /**
     * Defines the memory layout of a struct.
     */
    fun defineStruct(vararg fields: InputArgType<*>): FfiStructDefinition

    /**
     * Close the ffi manager and release all resources. As this function will free all memory that is used
     * by the ffi manager, it is important that no objects are used after this function has been called.
     */
    override fun close()

    fun makeInputArgChar(): InputArgType<Byte>
    fun makeInputArgInt(): InputArgType<Int>
    fun makeInputArgLong(): InputArgType<Long>
    fun makeInputArgFloat(): InputArgType<Float>
    fun makeInputArgDouble(): InputArgType<Double>
    fun makeInputArgString(): InputArgType<String?>
    fun makeInputArgPtr(): InputArgType<FfiPointer>
    fun makeInputArgBoolean(): InputArgType<Boolean>
    fun makeInputArgStruct(structDefinition: FfiStructDefinition): InputArgType<StructResult>
    fun <T> makeInputArgPointerToArg(type: InputArgType<T>): InputArgType<T>
    fun makeInputArgTypeVoid(): InputArgType<VoidWrapper>

    fun makeNullPtr(): FfiPointer

    object VoidWrapper
}

interface StructResult {
    fun getAtIndex(index: Int): Any
    fun size(): Int
}

interface FfiStructDefinition {
    val size: Long
    fun derivePtr(base: FfiPointer, index: Int): FfiPointer
    fun makeStructResult(values: Array<Any>): StructResult
}

expect fun ffiManager(): FfiManager

interface InputArgType<T>
//interface OutputArgType<out T>

interface FfiLibrary {
    /**
     * The name of the library (if the library was loaded using [FfiManager.loadLibrary], it is the name that was used in that function)
     */
    val name: String

    /**
     * Looks up a function and returns an [FfiFunctionDescriptor] that represents that function.
     *
     * @return an instance of [FfiFunctionDescriptor], or `null` if the function could not be found
     */
    fun <R> findFunction(name: String, returnType: InputArgType<R>, argTypes: List<InputArgType<*>>): FfiFunctionDescriptor<R>?
}

interface FfiFunctionDescriptor<R> {
    val returnType: InputArgType<R>
    val argTypes: List<InputArgType<*>>

    fun call(args: List<Any>, outputParams: Set<Int>? = null): CallResult<R>

    data class CallResult<T>(val result: T, val outputs: List<Any?>?)
}

interface FfiPointer {
    fun readLongAtOffset(index: Long): Long

    /**
     * Write a long value at offset [index] into the array.
     * Note that the JVM and the Linux implementations behave differently. The Linux version doesn't
     * multiply the index by the size of a long, while the JVM version does.
     * This will need to be fixed ones this function is actually used.
     */
    fun writeLongAtOffset(index: Long, value: Long)
    fun readByteAtOffset(index: Long): Byte
    fun writeByteAtOffset(index: Long, value: Byte)

    fun copyBytesToArray(offset: Long, buf: ByteArray, bufPosition: Int, size: Int)
    fun copyBytesFromArray(offset: Long, buf: ByteArray, bufPosition: Int, size: Int)
}

/**
 * A memory buffer allocated by [FfiManager.allocBytes].
 */
interface FfiBuffer : NativeCloseable {
    /**
     * Return a pointer to the byte at [offset]
     */
    fun pointerToOffset(offset: Long): FfiPointer

    /**
     * Free the memory buffer. After this function has been called, no further operations on the object is allowed.
     */
    override fun close()
}
