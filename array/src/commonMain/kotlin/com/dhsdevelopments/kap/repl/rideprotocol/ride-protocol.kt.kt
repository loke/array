@file:OptIn(ExperimentalStdlibApi::class)

package com.dhsdevelopments.kap.repl.rideprotocol

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.json.*
import com.dhsdevelopments.kap.keyboard.SYMBOL_BAR_LAYOUT
import com.dhsdevelopments.kap.keyboard.SYMBOL_DOC_LIST
import com.dhsdevelopments.kap.keyboard.SymbolDoc
import com.dhsdevelopments.kap.net.NetworkApi
import com.dhsdevelopments.kap.net.TcpConnection
import com.dhsdevelopments.kap.net.networkApi
import com.dhsdevelopments.kap.repl.AbstractGenericRepl
import com.dhsdevelopments.kap.repl.GenericReplBuilder
import com.dhsdevelopments.kap.repl.ReplExceptionTransformer
import com.dhsdevelopments.kap.repl.ReplFailedException

class RideConnectionException(message: String, cause: Throwable? = null) : Exception(message, cause)
class RideConnectionProtocolException(message: String, cause: Throwable? = null) : Exception(message, cause)

class RideConnection(conn: TcpConnection) {
    private val output: ByteConsumer = conn.byteConsumer
    private val input: ByteProvider = conn.byteProvider
    private val jsonEncoder = makeJsonEncoder()
    private val jsonDecoder = makeJsonDecoder()

    fun initialHandshake() {
        println("Starting handshake")

        writeBlock("SupportedProtocols=2")
        writeBlock("UsingProtocol=2")

        val s0 = readBlock()
        if (s0 != "SupportedProtocols=2") {
            throw RideConnectionException("Remote server does not support protocol 2")
        }

        val s1 = readBlock()
        if (s1 != "UsingProtocol=2") {
            throw RideConnectionException("Remote server is not using protocol 2")
        }

//        writeJson(JsonArray(listOf(JsonString("Identify"), JsonObject(listOf("apiVersion" to JsonNumber(1), "identity" to JsonNumber(2))))))
    }

    private val protocolTag = "RIDE".encodeToByteArray()

    fun writeJson(json: JsonElement) {
        val buf = StringBuilder()
        val consumer = AppendableCharConsumer(buf)
        jsonEncoder.encodeJsonToStream(json, consumer)
        consumer.close()
        writeBlock(buf.toString())
    }

    private fun writeBlock(s: String) {
        println("Writing block: +${s}+")
        val contentByteArray = s.encodeToByteArray()
        val len = contentByteArray.size + 8
        output.writeNetworkOrderInt(len)
        output.writeBlock(protocolTag)
        output.writeBlock(contentByteArray)
        output.flush()
    }

    fun readJson(): JsonElement {
        val s = readBlock()
        return jsonDecoder.decodeJsonFromString(s)
    }

    private fun readBlock(): String {
        val buf = ByteArray(8)
        println("Waiting for block")
        readBufWithCheck(buf)
        val len = ((buf[0].toInt() and 0xff) shl 24) or
                ((buf[1].toInt() and 0xff) shl 16) or
                ((buf[2].toInt() shl 8) and 0xff) or
                (buf[3].toInt() and 0xff)
        repeat(4) { i ->
            if (buf[i + 4] != protocolTag[i]) {
                throw RideConnectionProtocolException("Unexpected protocol header: [${buf.joinToString(", ") { v -> v.toUByte().toHexString() }}]")
            }
        }
        val buf2 = ByteArray(len - 8)
        readBufWithCheck(buf2)
        val s = buf2.decodeToString()
        println("Received string: +${s}+")
        return s
    }

    private fun readBufWithCheck(buf: ByteArray) {
        val res = input.readBlockComplete(buf)
        if (!res) {
            throw RideConnectionProtocolException("Connection closed")
        }
    }

    fun setPromptType(type: Int) {
        writeJson(JsonArray(listOf(JsonString("SetPromptType"), JsonObject(listOf("type" to JsonNumber(type))))))
    }

    fun echoInput(s: String) {
        val args = JsonObject(listOf("input" to JsonString(s), "group" to JsonNumber(0)))
        writeJson(JsonArray(listOf(JsonString("EchoInput"), args)))
    }

    fun appendSessionOutput(s: String, type: Int) {
        val args = JsonObject(listOf("result" to JsonString(s), "type" to JsonNumber(type), "group" to JsonNumber(0)))
        writeJson(JsonArray(listOf(JsonString("AppendSessionOutput"), args)))
    }
}

interface RideCommandHandler {
    fun handleCommand(cmdArray: List<JsonElement>, connection: RideConnection)
}

class RideRepl private constructor(
    val host: String,
    val port: Int,
    val nwApi: NetworkApi,
    replBuilder: GenericReplBuilder,
    argResult: Map<String, List<String?>>,
    exceptionTransformer: ReplExceptionTransformer?,
) : AbstractGenericRepl(replBuilder, argResult, null, false, exceptionTransformer) {

    val computeQueue = RideComputeQueue(engine)

    private val commandHandlers = mapOf(
        "Identify" to IdentifyCommandHandler(),
        "GetLanguageBar" to GetLanguageBarCommandHandler(),
        "Execute" to ExecuteCommandHandler())

    override fun mainLoop() {
        computeQueue.start()
        try {
            nwApi.connect(host, port).use { conn ->
                println("Connected to server: ${conn}")

                val rideConnection = RideConnection(conn)
                rideConnection.initialHandshake()
                println("Handshake completed")

                rideConnection.setPromptType(1)

                var stopped = false
                while (!stopped) {
                    val cmdArray = rideConnection.readJson().ensureArray()
                    val cmd = cmdArray[0].ensureString()
                    val handler = commandHandlers[cmd]
                    if (handler != null) {
                        handler.handleCommand(cmdArray, rideConnection)
                    } else if (cmd == "Exit") {
                        println("Connection close request from remote server")
                        stopped = true
                    } else {
                        println("Unknown command: ${cmd}")
                    }
                }
            }
        } finally {
            computeQueue.stop()
        }
    }

    private fun JsonElement.ensureString() = castJsonType<JsonString>(this).value
    private fun JsonElement.ensureArray() = castJsonType<JsonArray>(this).values
    private fun JsonElement.ensureObject() = castJsonType<JsonObject>(this)

    private inline fun <reified T> castJsonType(element: JsonElement): T {
        if (element !is T) {
            throw RideConnectionProtocolException("Message was not an array")
        }
        return element
    }

    override fun makeDefaultStdout(): CharacterOutput {
        return CharacterOutput { s -> print(s) }
    }

    companion object {
        fun make(
            builder: GenericReplBuilder,
            argResult: Map<String, List<String?>>,
            exceptionTransformer: ReplExceptionTransformer?
        ): AbstractGenericRepl? {
            val rideInitEnv = getEnvValue("RIDE_INIT") ?: throw IllegalStateException("RIDE_INIT was null")
            val matchResult = "^([A-Z]+):([^:]+):([0-9]+)$".toRegex().matchEntire(rideInitEnv)
            if (matchResult == null) {
                throw ReplFailedException("Invalid format for RIDE_INIT variable: '${rideInitEnv}'")
            }
            val command = matchResult.groups.get(1)!!
            if (command.value != "CONNECT") {
                throw ReplFailedException("Interpreter currently only supports the CONNECT command. Got: '${rideInitEnv}'")
            }
            val host = matchResult.groups.get(2)!!.value
            val port = matchResult.groups.get(3)!!.value
            val portInt = try {
                port.toInt()
            } catch (e: NumberFormatException) {
                throw ReplFailedException("Invalud port number specification in environment: '${rideInitEnv}'", e)
            }
            val nwApi = networkApi()
            if (nwApi == null) {
                throw ReplFailedException("Network API not available")
            }
            return RideRepl(host, portInt, nwApi, builder, argResult, exceptionTransformer)
        }
    }

    inner class IdentifyCommandHandler : RideCommandHandler {
        override fun handleCommand(cmdArray: List<JsonElement>, connection: RideConnection) {
            val response = JsonObject(
                listOf(
                    "apiVersion" to JsonNumber(1),
                    "Port" to JsonNumber(0),
                    "IPAddress" to JsonString(""),
                    "Vendor" to JsonString("DHS Developments"),
                    "Language" to JsonString("Kap"),
                    "version" to JsonString("0.1"),
                    "Machine" to JsonString(nwApi.hostname()),
                    "arch" to JsonString("jvm"),
                    "Project" to JsonString("none"))
            )
//            connection.writeJson(JsonArray(listOf(JsonString("ReplyIdentify"), response)))
            connection.writeJson(JsonArray(listOf(JsonString("Identify"), response)))
        }
    }

    class GetLanguageBarCommandHandler : RideCommandHandler {
        override fun handleCommand(cmdArray: List<JsonElement>, connection: RideConnection) {
            val result = ArrayList<JsonElement>()

            SYMBOL_BAR_LAYOUT.forEach { sym ->
                if (sym != ' ') {
                    val symAsString = sym.toString()
                    val symName = "Name: ${symAsString}"
                    val desc = SYMBOL_DOC_LIST[symAsString]
                    val helpText = if (desc == null) {
                        JsonArray(emptyList())
                    } else {
                        makeDescription(desc)
                    }
                    val element = JsonObject(listOf("name" to JsonString(symName), "avchar" to JsonString(symAsString), "helptext" to helpText))
                    result.add(element)
                }
            }

            connection.writeJson(JsonArray(listOf(JsonString("ReplyGetLanguageBar"), JsonObject(listOf("entries" to JsonArray(result))))))
        }

        private fun makeDescription(desc: SymbolDoc): JsonArray {
            val result = ArrayList<JsonString>()
            desc.monadicName?.let { n ->
                result.add(JsonString("Monadic: ${n}"))
            }
            desc.dyadicName?.let { n ->
                result.add(JsonString("Dyadic: ${n}"))
            }
            desc.monadicOperator?.let { n ->
                result.add(JsonString("Monadic operator: ${n}"))
            }
            desc.dyadicOperator?.let { n ->
                result.add(JsonString("Dyadic operator: ${n}"))
            }
            desc.specialDescription?.let { n ->
                result.add(JsonString(n))
            }
            return JsonArray(result)
        }
    }

    inner class ExecuteCommandHandler : RideCommandHandler {
        override fun handleCommand(cmdArray: List<JsonElement>, connection: RideConnection) {
            val text = cmdArray[1].ensureObject().lookup("text")
            if (text == null) {
                throw RideConnectionProtocolException("Got Execute command without text argument")
            }
            val string = text.ensureString()
            val src = StringSourceLocation(string)

            connection.echoInput(string)
            connection.setPromptType(0)

            println("Sending request")
            computeQueue.addRequest { engine ->
                println("Will eval: +${src}+")
                try {
                    val res = engine.parseAndEvalWithFormat(src)
                    connection.appendSessionOutput(res.strings.joinToString("\n"), 2)
                } catch (e: APLGenericException) {
                    connection.appendSessionOutput(e.formattedError(), 5)
                }
                connection.setPromptType(1)
            }
        }
    }
}
