package com.dhsdevelopments.mpbignum

import kotlin.math.floor
import kotlin.math.sqrt
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class IsqrtTest {
    @Test
    fun simpleSqrt() {
        assertEquals(BigInt.of(0), BigInt.of(0).isqrt())
        assertEquals(BigInt.of(1), BigInt.of(1).isqrt())
        assertEquals(BigInt.of(2), BigInt.of(4).isqrt())
        assertEquals(BigInt.of(4), BigInt.of(16).isqrt())
    }

    @Test
    fun simpleSqrtRounded() {
        assertEquals(BigInt.of(1), BigInt.of(2).isqrt())
        assertEquals(BigInt.of(1), BigInt.of(3).isqrt())
        assertEquals(BigInt.of(8), BigInt.of(67).isqrt())
    }

    @Test
    fun sqrtRange() {
        for (i in 2..100000) {
            assertEquals(BigInt.of(floor(sqrt(i.toDouble()))), BigInt.of(i).isqrt())
        }
    }

    @Test
    fun largeSqrt() {
        assertEquals(
            BigInt.of("3513641828820144253111222381699882939"),
            BigInt.of("12345678901234567890123456789012345678901234567890123456789012345678901111").isqrt())
        BigInt.of("12345678901111111111111111111111111111111111111111111111111111").let { n ->
            assertEquals(n, n.pow(2).isqrt())
        }
        BigInt.of("12345678901111111111111111111111111111111111111111111111111111").let { n ->
            assertEquals(n, (n.pow(2) + 1).isqrt())
        }
    }

    @Test
    fun sqrtNegativeShouldFail0() {
        assertFailsWith<ArithmeticException> {
            BigInt.of(-1).isqrt()
        }
        assertFailsWith<ArithmeticException> {
            BigInt.of("-123456").isqrt()
        }
        assertFailsWith<ArithmeticException> {
            BigInt.of("-100000000000000000000000000000000000000000000000000000000000000").isqrt()
        }
    }
}
