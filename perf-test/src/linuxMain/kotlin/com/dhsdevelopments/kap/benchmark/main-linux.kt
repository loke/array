package com.dhsdevelopments.kap.benchmark

/*
Debug executable:
Running tests: linux
primes: avg=9104.8, median=10024.0, min=8634, max=10438, stddev=1.0020157531761062

Running tests: linux
primes: avg=3142.6, median=3299.0, min=2249, max=3422, stddev=1.0062743352550496
var lookup scope: avg=18161.9, median=16816.0, min=11784, max=24409, stddev=1.0269002758703367
contrib bench: avg=2773.9, median=3604.0, min=1423, max=3605, stddev=1.0645374633681086
simple sum: avg=1072.1, median=1068.5, min=1059, max=1104, stddev=1.0000740751386223
multiple call: avg=985.7, median=997.5, min=966, max=999, stddev=1.0000765765823796

With overflow bignum:
Running tests: linux
primes: avg=3152.6, median=3174.0, min=2784, max=3444, stddev=1.0014892310457526
var lookup scope: avg=22971.6, median=23170.5, min=22073, max=23538, stddev=1.0001781115142003
contrib bench: avg=3258.2, median=2582.0, min=1749, max=3456, stddev=1.0118615065207681
simple sum: avg=1272.0, median=1271.5, min=1254, max=1302, stddev=1.0000757704374381
multiple call: avg=841.5, median=695.0, min=582, max=1001, stddev=1.0188451691641531

Before updated tracking of type specialisation:
Running tests: linux
primes: avg=786.9, median=788.0, min=785, max=790, stddev=1.0000013646375647
var lookup scope: avg=502.8, median=507.5, min=491, max=511, stddev=1.0000647901955315
contrib bench: avg=778.4, median=778.5, min=769, max=785, stddev=1.0000221483843748
simple sum: avg=1273.9, median=1274.0, min=1271, max=1286, stddev=1.000005450369912
multiple call: avg=375.1, median=375.0, min=362, max=394, stddev=1.0005183091377732
formatter: avg=138.8, median=138.5, min=137, max=141, stddev=1.0000404861872194
decode: avg=353.9, median=367.5, min=336, max=388, stddev=1.0008064521784283

Before updated tracking of type specialisation:
Running tests: linux
primes: avg=786.9, median=788.0, min=785, max=790, stddev=1.0000013646375647
var lookup scope: avg=502.8, median=507.5, min=491, max=511, stddev=1.0000647901955315
contrib bench: avg=778.4, median=778.5, min=769, max=785, stddev=1.0000221483843748
simple sum: avg=1273.9, median=1274.0, min=1271, max=1286, stddev=1.000005450369912
multiple call: avg=375.1, median=375.0, min=362, max=394, stddev=1.0005183091377732
formatter: avg=138.8, median=138.5, min=137, max=141, stddev=1.0000404861872194
decode: avg=353.9, median=367.5, min=336, max=388, stddev=1.0008064521784283

After updated tracking of type specialisation:
Running tests: linux
primes: avg=763.1, median=766.0, min=757, max=768, stddev=1.0000086635598946
var lookup scope: avg=494.8, median=490.5, min=484, max=505, stddev=1.00009099887953
contrib bench: avg=806.5, median=837.0, min=781, max=884, stddev=1.0006131255187236
simple sum: avg=1287.6, median=1286.5, min=1278, max=1300, stddev=1.0000143673509811
multiple call: avg=392.9, median=388.5, min=375, max=409, stddev=1.0005701863856158
formatter: avg=141.4, median=142.0, min=138, max=144, stddev=1.0000710189266333
decode: avg=350.0, median=353.0, min=336, max=362, stddev=1.0003346378862936
*/

fun main() {
    runAllTests("linux", "../array/standard-lib", "../benchmark-reports", "unnamed")
}
