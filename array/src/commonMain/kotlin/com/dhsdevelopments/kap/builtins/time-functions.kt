package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*
import com.dhsdevelopments.kap.dates.APLTimestamp
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

class SleepFunction : APLFunctionDescriptor {
    class SleepFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val sleepTimeSeconds = a.ensureNumber(pos).asDouble()
            if (sleepTimeSeconds > 0) {
                try {
                    sleepMillis(context.engine, (sleepTimeSeconds * 1000).toLong())
                } catch (e: SleepNotSupportedException) {
                    throwAPLException(APLEvalException("Backend does not support the sleep operation", pos))
                }
            }
            context.engine.checkInterrupted(pos)
            return sleepTimeSeconds.makeAPLNumber()
        }

        override fun callAsync1Arg(
            context: RuntimeContext,
            a: APLValue,
            axis: APLValue?,
            callback: (context: RuntimeContext, APLValue) -> Either<APLValue, Exception>
        ) {
            val sleepTimeSeconds = a.ensureNumber(pos).asDouble()
            sleepMillisCallback(context.engine, (sleepTimeSeconds * 1000).toLong(), pos) {
                callback(context, sleepTimeSeconds.makeAPLNumber())
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = SleepFunctionImpl(instantiation)
}

class TimeMillisFunction : APLFunctionDescriptor {
    class TimeMillisFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            unless(a.ensureNumber(pos).asInt(pos) == 0) {
                throwAPLException(APLIllegalArgumentException("Argument to timeMillis must be 0", pos))
            }
            return Clock.System.now().toEpochMilliseconds().makeAPLNumber()
        }

        override val name1Arg get() = "timeMillis"
    }

    override fun make(instantiation: FunctionInstantiation) = TimeMillisFunctionImpl(instantiation)
}

class MakeTimerFunction : APLFunctionDescriptor {
    class MakeTimerFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval2Arg(context: RuntimeContext, a: APLValue, b: APLValue): APLValue {
            val delays = b.arrayify().toIntArray(pos)
            val callbacks = a.arrayify().membersSequence().map { v ->
                if (v is LambdaValue) {
                    v
                } else {
                    throwAPLException(APLIllegalArgumentException("Left argument must be a function or a list of functions", pos))
                }
            }.toList()
            return context.engine.makeTimer(delays, callbacks, pos)
        }
    }

    override fun make(instantiation: FunctionInstantiation) = MakeTimerFunctionImpl(instantiation)
}

class ToTimestampFunction : APLFunctionDescriptor {
    class ToTimestampFunctionImpl(pos: FunctionInstantiation) : MathCombineAPLFunction(pos) {
        override fun combine1Arg(a: APLSingleValue): APLValue {
            val millis = a.ensureNumber(pos).asLong(pos)
            return APLTimestamp(Instant.fromEpochMilliseconds(millis))
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ToTimestampFunctionImpl(instantiation)
}

private fun ensureTimestamp(a: APLValue, pos: Position): Instant {
    val a0 = a.unwrapDeferredValue()
    if (a0 !is APLTimestamp) {
        throwAPLException(IncompatibleTypeException("Argument is not a timestamp", pos))
    }
    return a0.time
}

class FromTimestampFunction : APLFunctionDescriptor {
    class FromTimestampFunctionImpl(pos: FunctionInstantiation) : MathCombineAPLFunction(pos) {
        override fun combine1Arg(a: APLSingleValue): APLValue {
            return ensureTimestamp(a, pos).toEpochMilliseconds().makeAPLNumber()
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FromTimestampFunctionImpl(instantiation)
}

class FormatTimestampFunction : APLFunctionDescriptor {
    class FormatTimestampFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            return APLString.make(ensureTimestamp(a, pos).toString())
        }
    }

    override fun make(instantiation: FunctionInstantiation) = FormatTimestampFunctionImpl(instantiation)
}

class ParseTimestampFunction : APLFunctionDescriptor {
    class ParseTimestampFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            val s = a.toStringValue(pos)
            return APLTimestamp(Instant.parse(s))
        }
    }

    override fun make(instantiation: FunctionInstantiation) = ParseTimestampFunctionImpl(instantiation)
}
