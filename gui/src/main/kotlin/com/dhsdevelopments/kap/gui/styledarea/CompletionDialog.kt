package com.dhsdevelopments.kap.gui.styledarea

import com.dhsdevelopments.kap.completions.SymbolCompletionCandidate
import javafx.geometry.Point2D
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.stage.StageStyle

class CompletionDialog(val result: List<SymbolCompletionCandidate>, val selectionCallback: (SymbolCompletionCandidate) -> Unit) : BorderPane() {
    private val vbox: VBox
    private var selectedItem = -1

    init {
        require(result.isNotEmpty())
        stylesheets.add("/com/dhsdevelopments/kap/gui/completion-dialog.css")
        styleClass.add("completion-dialog")
        vbox = VBox()
        vbox.isFillWidth = true
        result.forEach { candidate ->
            val label = Label(candidate.sym.nameWithNamespace)
            label.styleClass.add("completion-label")
            vbox.children.add(label)
        }

        selectItem(0)

        center = vbox
    }

    private fun registerEventFilter(scene: Scene) {
        scene.addEventFilter(KeyEvent.KEY_PRESSED) { event ->
            val handled = when {
                event.code == KeyCode.DOWN -> moveSelection(1)
                event.code == KeyCode.N && event.isControlDown -> moveSelection(1)
                event.code == KeyCode.UP -> moveSelection(-1)
                event.code == KeyCode.P && event.isControlDown -> moveSelection(-1)
                event.code == KeyCode.ENTER || event.code == KeyCode.TAB -> selectCurrent()
                event.code == KeyCode.ESCAPE -> close()
                else -> false
            }
            if (handled) {
                event.consume()
            }
        }
    }

    private fun selectItem(i: Int) {
        if (selectedItem != i) {
            if (selectedItem >= 0) {
                vbox.children[selectedItem].styleClass.remove(SELECTED_ITEM_CLASS_NAME)
            }
            vbox.children[i].styleClass.add(SELECTED_ITEM_CLASS_NAME)
            selectedItem = i
        }
    }

    private fun moveSelection(direction: Int): Boolean {
        val newIndex = (selectedItem + direction) % result.size
        selectItem(newIndex)
        return true
    }

    private fun selectCurrent(): Boolean {
        val item = result[selectedItem]
        selectionCallback(item)
        close()
        return true
    }

    private fun close(): Boolean {
        this.scene.window.hide()
        return true
    }

    companion object {
        private const val SELECTED_ITEM_CLASS_NAME = "completion-label-selected"

        fun open(
            owner: Node,
            displayPos: Point2D?,
            result: List<SymbolCompletionCandidate>,
            selectionCallback: (SymbolCompletionCandidate) -> Unit): CompletionDialog {
            val stage = Stage()
            stage.initModality(Modality.APPLICATION_MODAL)
            stage.initStyle(StageStyle.UNDECORATED)
            stage.initOwner(owner.scene.window)
            val dialog = CompletionDialog(result, selectionCallback)
            val scene = Scene(dialog)
            stage.scene = scene
            dialog.registerEventFilter(scene)
            if (displayPos != null) {
                stage.x = displayPos.x
                stage.y = displayPos.y
            }
            stage.show()
            return dialog
        }
    }
}
