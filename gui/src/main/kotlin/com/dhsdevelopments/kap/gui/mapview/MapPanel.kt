package com.dhsdevelopments.kap.gui.mapview

import com.dhsdevelopments.kap.APLMap
import com.dhsdevelopments.kap.APLValue
import com.dhsdevelopments.kap.Symbol
import com.dhsdevelopments.kap.gui.Client
import com.dhsdevelopments.kap.gui.objview.ActionDescriptor
import com.dhsdevelopments.kap.gui.objview.ObjectEditorHandler
import javafx.beans.property.ReadOnlyObjectPropertyBase
import javafx.collections.FXCollections
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.control.TableCell
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.layout.BorderPane
import javafx.stage.Stage
import javafx.util.Callback

class MapPanel(val client: Client, val content: APLMap) : BorderPane() {
    private val table = TableView<MapEntry>()

    init {
        val label = Label("Foo test")
        top = label
        center = table

        table.items = FXCollections.observableArrayList(content.content.entries.map { (k, v) -> MapEntry(k.value, v) })

        val keyColumn = TableColumn<MapEntry, APLValue>("Key").apply {
            cellValueFactory = PropertyValueFactory("key")
            cellFactory = TableColumnKapValueCellFactory(client)
        }
        val valueColumn = TableColumn<MapEntry, APLValue>("Value").apply {
            cellValueFactory = PropertyValueFactory("value")
            cellFactory = TableColumnKapValueCellFactory(client)
        }

        table.columns.setAll(keyColumn, valueColumn)
    }

    companion object {
        fun open(client: Client, value: APLMap) {
            val stage = Stage()
            val editor = MapPanel(client, value)
            val scene = Scene(editor, 800.0, 400.0)
            stage.title = "Map viewer"
            stage.scene = scene
            stage.show()
        }

        val PROVIDERS = buildObjectViewHandlerList()

        private fun buildObjectViewHandlerList(): List<ObjectEditorHandler> {
            val viewHandler = object : ObjectEditorHandler {
                override fun name(value: APLValue) = "Open in map viewer"

                override fun canEdit(value: APLValue): ActionDescriptor? {
                    return if (value.unwrapDeferredValue() is APLMap) {
                        object : ActionDescriptor {
                            override val menuTitle get() = "Open in map viewer"

                            override fun perform(client: Client, value: APLValue, name: Symbol?) {
                                open(client, value.ensureMap())
                            }
                        }
                    } else {
                        null
                    }
                }
            }
            return listOf(viewHandler)
        }
    }
}

class MapEntry(val k: APLValue, val v: APLValue) {
    private val keyP = object : ReadOnlyObjectPropertyBase<APLValue>() {
        override fun get() = k
        override fun getBean() = this
        override fun getName() = "key"
    }

    @Suppress("unused")
    fun keyProperty() = keyP

    private val valueP = object : ReadOnlyObjectPropertyBase<APLValue>() {
        override fun get() = v
        override fun getBean() = this
        override fun getName() = "value"
    }

    @Suppress("unused")
    fun valueProperty() = valueP
}

private class APLValueTableCell(val client: Client) : TableCell<MapEntry, APLValue>() {
    override fun updateItem(item: APLValue?, empty: Boolean) {
        super.updateItem(item, empty)
        if (empty) {
            text = null
            graphic = null
        } else {
            item!!
            text = null
            graphic = client.objectViewerRegistry.findRendererProvider(item).makeNode(item)
        }
    }
}

private class TableColumnKapValueCellFactory(val client: Client) : Callback<TableColumn<MapEntry, APLValue>, TableCell<MapEntry, APLValue>> {
    override fun call(param: TableColumn<MapEntry, APLValue>): TableCell<MapEntry, APLValue> {
        return APLValueTableCell(client)
    }
}
