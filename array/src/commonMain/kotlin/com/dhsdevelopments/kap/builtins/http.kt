package com.dhsdevelopments.kap.builtins

import com.dhsdevelopments.kap.*

private fun ensureHeaderArray(headerArg: APLValue, pos: Position): Map<String, String> {
    unless(
        (headerArg.rank == 2 && headerArg.dimensions[1] == 2) ||
                (headerArg.rank == 1 && headerArg.dimensions[0] % 2 == 0)
    ) {
        throwAPLException(APLIllegalArgumentException("Headers list should be a rank-2 array with 2 columns", pos))
    }
    val result = HashMap<String, String>()
    for (i in 0 until headerArg.size / 2) {
        val key = headerArg.valueAt(i * 2).toStringValue(pos)
        val value = headerArg.valueAt(i * 2 + 1).toStringValue(pos)
        result[key] = value
    }
    return result
}

private fun parseHttpArgs(a: APLValue, method: HttpMethod, acceptsData: Boolean, pos: Position): HttpRequestData {
    val elements = a.listify()
    val size = elements.listSize()

    val (min, max) = if (acceptsData) Pair(2, 3) else Pair(1, 2)
    if (size < min || size > max) {
        throwAPLException(APLIllegalArgumentException("Function requires ${min}-${max} arguments, got ${size}", pos))
    }

    var i = 0
    val url = elements.listElement(i++, pos).toStringValue(pos)

    val headers = if (i < size) {
        ensureHeaderArray(elements.listElement(i++, pos), pos)
    } else {
        emptyMap()
    }

    val data = if (i < size && acceptsData) {
        elements.listElement(i++, pos).asByteArray(pos)
    } else {
        null
    }

    return HttpRequestData(url, method, data, headers)
}

private fun makeAPLListFromHttpResult(result: HttpResult): APLList {
    val headers = result.headers.map { (key, values) ->
        APLString.make(key).makeTypeQualifiedKey() to APLArrayImpl(
            dimensionsOfSize(values.size),
            values.map(APLString::make).toTypedArray())
    }
    return APLList(
        listOf(
            result.code.makeAPLNumber(),
            APLString.make(result.content),
            APLMap(ImmutableMap2.makeFromContent(headers))))
}

private fun parseArgsAndCallHttp(context: RuntimeContext, a: APLValue, method: HttpMethod, acceptsData: Boolean, pos: Position): APLValue {
    val httpRequestData = parseHttpArgs(a, method, acceptsData, pos)
    val result = httpRequest(context.engine, httpRequestData, pos)
    return makeAPLListFromHttpResult(result)
}

class HttpRequestFunction(val method: HttpMethod, val acceptsData: Boolean) : APLFunctionDescriptor {
    inner class HttpRequestFunctionImpl(pos: FunctionInstantiation) : NoAxisAPLFunction(pos) {
        override fun eval1Arg(context: RuntimeContext, a: APLValue): APLValue {
            return parseArgsAndCallHttp(context, a, method, acceptsData, pos)
        }

        override fun callAsync1Arg(
            context: RuntimeContext,
            a: APLValue,
            axis: APLValue?,
            callback: (context: RuntimeContext, APLValue) -> Either<APLValue, Exception>
        ) {
            val httpRequestData = parseHttpArgs(a, method, acceptsData, pos)
            httpRequestCallback(context.engine, httpRequestData, pos) { result ->
                callback(context, makeAPLListFromHttpResult(result))
            }
        }
    }

    override fun make(instantiation: FunctionInstantiation) = HttpRequestFunctionImpl(instantiation)
}
