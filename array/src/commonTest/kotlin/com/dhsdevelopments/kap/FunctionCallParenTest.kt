package com.dhsdevelopments.kap

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs

class FunctionCallParenTest : APLTest() {
    @Test
    fun simpleFunctionCall() {
        parseAPLExpression("∇ foo (a;b;c) { a+b+c }\nfoo ⟦1;2;3⟧").let { result ->
            assertSimpleNumber(6, result)
        }
    }

    @Test
    fun functionCallInStrandedAarray() {
        parseAPLExpression("∇ foo (a;b;c) { a+b+c }\n10 20 foo ⟦1;2;3⟧ 40 50").let { result ->
            assert1DArray(arrayOf(10, 20, 6, 40, 50), result)
        }
    }

    @Test
    fun functionCallWithEmptyArgument() {
        parseAPLExpression("∇ foo { if(isLocallyBound '⍺) { throw \"got left argument\" } else {⍵} } \nfoo⟦⟧", withStandardLib = true).let { result ->
            assertIs<APLList>(result)
            assertEquals(0, result.listSize())
        }
    }

    @Test
    fun stringResultWithStrand() {
        parseAPLExpression("∇ foo (a;b;c) { \"test\" }\n10 20 foo ⟦1;2;3⟧ 40 50").let { result ->
            assert1DArray(arrayOf<Any>(10, 20, "test", 40, 50), result)
        }
    }

    @Test
    fun multipleCallsInExpression() {
        parseAPLExpression("∇ foo (a;b;c) { a+b+c }\nfoo ⟦1;2;3⟧ + foo ⟦10;11;12⟧").let { result ->
            assertSimpleNumber(39, result)
        }
    }

    @Test
    fun nestedFunctionCalls() {
        parseAPLExpression("∇ foo (a;b;c) { a+b+c }\nfoo ⟦1;2;foo ⟦10;11;12⟧⟧").let { result ->
            assertSimpleNumber(36, result)
        }
    }

    @Test
    fun functionCallWithTacit() {
        parseAPLExpression("∇ foo (a) { 1+a }\n (foo-)⟦10⟧").let { result ->
            assertSimpleNumber(-9, result)
        }
    }

    @Test
    fun functionCallWithOperator() {
        parseAPLExpression("+/⟦1 2 3 4⟧").let { result ->
            assertSimpleNumber(10, result)
        }
    }

    @Test
    fun functionCallWithOperatorAndOuterFunctionCall0() {
        parseAPLExpression("+/⟦1 2 3 4⟧ + 100").let { result ->
            assertSimpleNumber(110, result)
        }
    }

    @Test
    fun functionCallWithOperatorAndOuterFunctionCall1() {
        parseAPLExpression("200 + +/⟦1 2 3 4⟧ + 100").let { result ->
            assertSimpleNumber(310, result)
        }
    }

    @Test
    fun functionCallWithEmptyArgs() {
        parseAPLExpression("∇ foo (a) { ⍴ fromList a } ⋄ foo⟦⟧").let { result ->
            assert1DArray(arrayOf(0), result)
        }
    }

    @Test
    fun functionCallWithEmptyArgsValueResult() {
        parseAPLExpression("∇ foo (a) { 10 (⍴ fromList a) } ⋄ foo⟦⟧").let { result ->
            assert1DArray(arrayOf(10, Inner1D(arrayOf(0))), result)
        }
    }
}
