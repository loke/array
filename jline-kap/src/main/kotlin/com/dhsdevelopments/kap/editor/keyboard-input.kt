package com.dhsdevelopments.kap.editor

import com.dhsdevelopments.kap.Engine
import com.dhsdevelopments.kap.KeyboardInput
import com.dhsdevelopments.kap.completions.symbolCompletionCandidatesForWord
import com.dhsdevelopments.kap.edit.*
import com.dhsdevelopments.kap.editor.JlineKeyboardInput.Companion.KAP_SYMBOL_MAP
import com.dhsdevelopments.kap.keyboard.ExtendedCharsKeyboardInput
import com.dhsdevelopments.kap.repl.AbstractGenericRepl
import org.jline.keymap.KeyMap
import org.jline.reader.*
import org.jline.reader.impl.DefaultParser
import org.jline.terminal.Terminal
import org.jline.terminal.TerminalBuilder

class JlineKeyboardInput(repl: AbstractGenericRepl) : KeyboardInput {
    private val terminal = TerminalBuilder.builder().build()
    private val reader: LineReader

    init {
        val kapParser = KapParser(repl.engine)

//        val registry = KapSystemRegistry(kapParser, terminal, null, null)
//        registry.setCommandRegistries(kapCommandRegistry)

        reader = LineReaderBuilder.builder()
            .terminal(terminal)
            .parser(kapParser)
            .completer(KapSymbolCompleter(repl.engine))
            .option(LineReader.Option.USE_FORWARD_SLASH, true)
            .build()

        val layout = ExtendedCharsKeyboardInput()
        val prefix = "`"
        val state = InputStateWrapper(repl.engine, reader, layout)

        val connector = JlineEditorEngineConnector(state)

        overrideEditingCommands(state)

        reader.widgets[CHARPREFIX_WIDGET_NAME] = PrefixWidget(state)
        reader.widgets[WRAP_WITH_PAREN_WIDGET_NAME] = EditorActionWidget(WrapWithParen(connector), state)

        reader.keyMaps[LineReader.MAIN]!!.bind(Reference(CHARPREFIX_WIDGET_NAME), prefix)
        reader.keyMaps[LineReader.MAIN]!!.bind(Reference(WRAP_WITH_PAREN_WIDGET_NAME), KeyMap.ctrl('q'))
        reader.keyMaps[KAP_SYMBOL_MAP] = makeKapSymbolKeymap(state, prefix, layout)
    }

    private fun overrideEditingCommands(state: InputStateWrapper) {
        val fns = listOf(
            LineReader.ACCEPT_LINE, LineReader.BACKWARD_DELETE_CHAR, LineReader.BACKWARD_DELETE_WORD,
            LineReader.DELETE_CHAR, LineReader.EXPAND_OR_COMPLETE, LineReader.SELF_INSERT,
            LineReader.KILL_LINE, LineReader.KILL_WHOLE_LINE)
        for (name in fns) {
            val builtinWidget = reader.builtinWidgets[name]
            requireNotNull(builtinWidget)
            val wrapper = ModificationTrackerWidget(builtinWidget, state)
            reader.widgets.put(name, wrapper)
        }
    }

    private fun makeKapSymbolKeymap(state: InputStateWrapper, prefix: String, layout: ExtendedCharsKeyboardInput): KeyMap<Binding> {
        val map = KeyMap<Binding>()
        layout.keymap.forEach { entry ->
            map.bind(InsertSymbolWidget(state, entry.value), entry.key.character)
        }
        map.bind(InsertSymbolWidget(state, prefix))
        val resetMapWidget = ResetMapWidget(state)
//        map.bind(resetMapWidget, "\u001B") //esc
        map.bind(resetMapWidget, "\u0008") //bs
        map.bind(resetMapWidget, "\u007F") //del
        return map
    }

    override fun readString(prompt: String): String? {
        try {
            while (true) {
                try {
                    return reader.readLine(prompt)
                } catch (_: UserInterruptException) {
                    // Don't log anything, just restart the input
                }
            }
        } catch (_: EndOfFileException) {
            return null
        }
    }

    fun registerSignalHandler(handler: () -> Unit) {
        reader.terminal.handle(Terminal.Signal.INT) { handler() }
    }

    override fun close() {
        terminal.close()
    }

    companion object {
        const val CHARPREFIX_WIDGET_NAME = "charprefix"
        const val WRAP_WITH_PAREN_WIDGET_NAME = "wrap-with-paren"
        const val KAP_SYMBOL_MAP = "kap"
    }
}

private class ModificationTrackerWidget(val widget: Widget, val state: InputStateWrapper) : Widget {
    override fun apply(): Boolean {
        state.editableText.markEdited()
        return widget.apply()
    }
}

class KapSymbolCompleter(val engine: Engine) : Completer {
    override fun complete(reader: LineReader, line: ParsedLine, candidates: MutableList<Candidate>) {
        val word = line.word()
        val list = symbolCompletionCandidatesForWord(engine, word)
        candidates.addAll(list.map { c -> Candidate(c.text, c.text, c.sym.namespace.name, c.description, null, null, true) })
    }
}

internal class KapParser(val engine: Engine) : DefaultParser() {
    init {
        escapeChars = charArrayOf()
        quoteChars = charArrayOf('"')
        lineCommentDelims = arrayOf("⍝")
    }

    override fun isDelimiter(buffer: CharSequence, pos: Int): Boolean {
        val ch = buffer.toString().codePointAt(pos)
        return engine.isCharSymbolDelimiter(ch)
    }
}

internal class InputStateWrapper(val engine: Engine, val reader: LineReader, val layout: ExtendedCharsKeyboardInput) {
    val editableText = JlineEditableText(reader)

    fun writeAndResetKeymap(sym: String) {
        reader.buffer.write(sym)
        resetKeymap()
    }

    fun resetKeymap() {
        reader.keyMap = LineReader.MAIN
    }
}

internal class JlineEditableText(val reader: LineReader) : SimpleTextTrackingEditableText() {
    override fun text(start: Int, end: Int?): String {
        val buffer = reader.buffer
        return buffer.substring(start, end ?: buffer.length())
    }

    override fun cursorPosition(): Int {
        return reader.buffer.cursor()
    }

    override fun insert(text: String, index: Int) {
        val buffer = reader.buffer
        val oldCursor = buffer.cursor()
        buffer.cursor(index)
        buffer.write(text)
        buffer.cursor(if (oldCursor >= index) oldCursor + text.length else oldCursor)
    }

    override fun remove(numChars: Int, index: Int) {
        val buffer = reader.buffer
        val oldCursor = buffer.cursor()
        buffer.cursor(index)
        buffer.delete(numChars)
        val i = when {
            oldCursor < index -> oldCursor
            oldCursor > index + numChars -> oldCursor - numChars
            else -> index
        }
        buffer.cursor(i)
    }

    override fun commit() {}
}

internal class PrefixWidget(internal val state: InputStateWrapper) : Widget {
    override fun apply(): Boolean {
        state.reader.keyMap = KAP_SYMBOL_MAP
        return true
    }
}

internal class InsertSymbolWidget(internal val state: InputStateWrapper, val sym: String) : Widget {
    override fun apply(): Boolean {
        state.writeAndResetKeymap(sym)
        return true
    }
}

internal class ResetMapWidget(internal val state: InputStateWrapper) : Widget {
    override fun apply(): Boolean {
        state.resetKeymap()
        return true
    }
}

internal class JlineEditorEngineConnector(val state: InputStateWrapper) : EditorEngineConnector {
    override fun findParenCandidates(src: String, index: Int, callback: (List<Int>) -> Unit) {
        val res = parenCandidates(state.engine, src, index)
        callback(res)
    }
}

internal class EditorActionWidget(val editorAction: EditorAction, val state: InputStateWrapper) : Widget {
    override fun apply(): Boolean {
        editorAction.run(state.editableText)
        return true
    }
}
