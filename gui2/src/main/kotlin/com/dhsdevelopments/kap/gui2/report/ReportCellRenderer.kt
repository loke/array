package com.dhsdevelopments.kap.gui2.report

import java.awt.Component
import javax.swing.JLabel
import javax.swing.JTable
import javax.swing.UIManager
import javax.swing.table.DefaultTableCellRenderer

class ReportCellRenderer : DefaultTableCellRenderer() {
    private var defaultLabel: JLabel? = null

    override fun getTableCellRendererComponent(table: JTable?, value: Any, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int): Component {
        require(value is ReportCell)
        val label = defaultLabel ?: JLabel().also { l -> defaultLabel = l }
        value.formatLabel(label)
        label.isOpaque = true
        if (table != null) {
            label.foreground = if (isSelected) table.selectionForeground else table.foreground
            label.background = if (isSelected) table.selectionBackground else table.background
        }
        label.border = if (hasFocus) UIManager.getBorder("Table.focusCellHighlightBorder") else noFocusBorder

        return label
    }
}
