package com.dhsdevelopments.kap.htmlconverter

import com.dhsdevelopments.kap.APLTest
import com.dhsdevelopments.kap.builtins.HtmlParserException
import com.dhsdevelopments.kap.dimensionsOfSize
import kotlin.test.Test
import kotlin.test.assertFailsWith

class ParseHtmlFunctionTest : APLTest() {
    @Test
    fun simpleHtmlMonadicCall() {
        val src = "io:fromHtmlTable \"<html><body>Foo<table>" +
                "<thead><th>Foo</th><th>Bar</th></thead><tbody>" +
                "<tr><td>1</td><td>10</td></tr>" +
                "<tr><td>abctest</td><td>123foo</td></tr>" +
                "<tr><td>2</td><td>3</td></tr>" +
                "</tbody></table></body></html>\""
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(3, 2), result)
            assertArrayContent(arrayOf<Any>(1, 10, "abctest", 123, 2, 3), result)
        }
    }

    @Test
    fun simpleHtmlDyadicCall() {
        val src = "1 io:fromHtmlTable \"<html><body>Foo" +
                "<table>" +
                "<tr><td>1</td><td>10</td></tr>" +
                "<tr><td>abctest</td><td>123foo</td></tr>" +
                "<tr><td>2</td><td>3</td></tr>" +
                "</table>" +
                "<table>" +
                "<tr><td>10</td><td>11</td></tr>" +
                "<tr><td>12</td><td>13</td></tr>" +
                "</table>" +
                "</body></html>\""
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(10, 11, 12, 13), result)
        }
    }

    @Test
    fun noTableInHtmlContent() {
        assertFailsWith<HtmlParserException> {
            parseAPLExpression("io:fromHtmlTable \"foo\"")
        }
    }

    @Test
    fun invalidTableIndex() {
        assertFailsWith<HtmlParserException> {
            val src = "1 io:fromHtmlTable \"<html><body>Foo<table>" +
                    "<thead><th>Foo</th><th>Bar</th></thead><tbody>" +
                    "<tr><td>1</td><td>10</td></tr>" +
                    "<tr><td>abctest</td><td>123foo</td></tr>" +
                    "<tr><td>2</td><td>3</td></tr>" +
                    "</tbody></table></body></html>\""
            parseAPLExpression(src)
        }
    }

    @Test
    fun tableWithHeaders() {
        val src = "1 io:fromHtmlTable \"<html><body>Foo" +
                "<table>" +
                "<tbody>" +
                "<tr><td>1</td><td>10</td></tr>" +
                "<tr><td>abctest</td><td>123foo</td></tr>" +
                "<tr><td>2</td><td>3</td></tr>" +
                "</tbody>" +
                "</table>" +
                "<table>" +
                "<thead><tr><th>abc</th><th>def</th></tr></thead>" +
                "<tbody>" +
                "<tr><td>10</td><td>11</td></tr>" +
                "<tr><td>12</td><td>13</td></tr>" +
                "</thead>" +
                "</table>" +
                "</body></html>\""
        parseAPLExpression(src).let { result ->
            assertDimension(dimensionsOfSize(2, 2), result)
            assertArrayContent(arrayOf(10, 11, 12, 13), result)
            assertLabels(listOf(null, listOf("abc", "def")), result)
        }
    }
}
