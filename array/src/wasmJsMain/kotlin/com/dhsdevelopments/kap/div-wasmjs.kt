package com.dhsdevelopments.kap

import org.khronos.webgl.Uint8Array
import kotlin.reflect.KClass

external fun setTimeout(callback: () -> Unit, time: Double)

actual fun sleepMillis(time: Long) {
    TODO("not implemented")
}

actual fun sleepMillisCallback(engine: Engine, time: Long, pos: Position, callback: () -> Either<APLValue, Exception>) {
    setTimeout({ callback() }, time.toDouble())
}

class JsAtomicRefArray<T>(size: Int) : MPAtomicRefArray<T> {
    private val content: MutableList<T?>

    init {
        content = ArrayList()
        repeat(size) {
            content.add(null)
        }
    }

    override fun get(index: Int) = content[index]

    override fun compareAndExchange(index: Int, expected: T?, newValue: T?): T? {
        val v = content[index]
        if (v == expected) {
            content[index] = newValue
        }
        return v
    }
}

actual fun <T> makeAtomicRefArray(size: Int): MPAtomicRefArray<T> {
    return JsAtomicRefArray(size)
}

actual fun <T : Any> makeMPThreadLocalBackend(type: KClass<T>): MPThreadLocal<T> {
    return object : MPThreadLocal<T> {
        override var value: T? = null
    }
}

private val NON_SCIENTIFIC_INTEGER_REGEX = "^-?[0-9]+$".toRegex()

actual fun Double.formatDouble(): String {
    val s = this.toString()
    return if (NON_SCIENTIFIC_INTEGER_REGEX.matches(s)) {
        "${s}.0"
    } else {
        s
    }
}

actual fun toRegexpWithException(string: String, options: Set<RegexOption>): Regex {
    return try {
        string.toRegex(options)
    } catch (e: Throwable) {
        throw RegexpParseException("Error parsing regexp: \"${string}\"", e)
    }
}

actual class MPLock

actual inline fun <T> MPLock.withLocked(fn: () -> T): T {
    return fn()
}

actual class MPReadWriteLock

actual inline fun <T> MPReadWriteLock.withReadLock(fn: () -> T): T {
    return fn()
}

actual inline fun <T> MPReadWriteLock.withWriteLock(fn: () -> T): T {
    return fn()
}

actual fun numCores() = 1

actual fun makeBackgroundDispatcher(numThreads: Int): MPThreadPoolExecutor {
    return SingleThreadedThreadPoolExecutor()
}

private fun makeWeakRef(ref: JsReference<*>): JsAny = js("new WeakRef(ref)")
private fun derefRef(ref: JsAny): JsReference<*> = js(
    """
        (function(inner) {
            var a = ref.deref();
            if(a) {
                return a;
            } else {
                return null;
            }
        })(ref)
    """)

class JsWeakReference<T : Any>(ref: T) : MPWeakReference<T> {
    private val instance = makeWeakRef(ref.toJsReference())

    override val value: T? get() = derefRef(instance).get() as T?
}

actual fun <T : Any> MPWeakReference.Companion.make(ref: T): MPWeakReference<T> {
    return JsWeakReference(ref)
}

actual fun makeTimerHandler(engine: Engine): TimerHandler? = null

external class SharedArrayBuffer(size: Int) : JsAny

private fun crossOriginIsolatedAndDefined(): Boolean = js("typeof(crossOriginIsolated) !== \"undefined\" && crossOriginIsolated")
private fun makeUint8ArrayFromBuffer(buffer: SharedArrayBuffer): Uint8Array = js("new Uint8Array(buffer)")
fun storeAtomic(array: Uint8Array, offset: Int, value: Int): Unit = js("Atomics.store(array, offset, value)")
fun loadAtomic(array: Uint8Array, offset: Int): Int = js("Atomics.load(array, offset)")

class JsNativeData : NativeData {
    val breakBufferData: SharedArrayBuffer?
    val breakBufferArray: Uint8Array?

    init {
        if (crossOriginIsolatedAndDefined()) {
            breakBufferData = SharedArrayBuffer(1)
            breakBufferArray = makeUint8ArrayFromBuffer(breakBufferData)
            storeAtomic(breakBufferArray, 0, 0)
        } else {
            breakBufferData = null
            breakBufferArray = null
        }
    }

    override fun close() {}
}

actual fun makeNativeData(): NativeData = JsNativeData()

@Suppress("NOTHING_TO_INLINE")
actual inline fun nativeUpdateBreakPending(engine: Engine, state: Boolean) {
    val jsNativeData = engine.nativeData as JsNativeData
    val b = jsNativeData.breakBufferArray
    if (b != null) {
        val newState = if (state) 1 else 0
        storeAtomic(b, 0, newState)
    }
}

@Suppress("NOTHING_TO_INLINE")
actual inline fun nativeBreakPending(engine: Engine): Boolean {
    val jsNativeData = engine.nativeData as JsNativeData
    val b = jsNativeData.breakBufferArray
    return if (b != null) {
        loadAtomic(b, 0) == 1
    } else {
        false
    }
}

actual fun findInstallPath(): String? = null

// This disables test cases that are affected by a bug in the wasmjs compiler
// https://youtrack.jetbrains.com/issue/KT-58681/K-Wasm-division-remainder-has-a-wrong-sign
actual val brokenNegativeZero: Boolean = true
